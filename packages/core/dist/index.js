// TODO: перенести общие типы в core
var ESignSoftware;
(function (ESignSoftware) {
    ESignSoftware["CPro"] = "cpro";
    ESignSoftware["NCA"] = "nca";
    ESignSoftware["Fake"] = "fake";
})(ESignSoftware || (ESignSoftware = {}));
var ESignStrategy;
(function (ESignStrategy) {
    ESignStrategy["Qualified"] = "qualified";
    ESignStrategy["Simple"] = "simple";
    ESignStrategy["Mock"] = "mock";
})(ESignStrategy || (ESignStrategy = {}));
//

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    if (typeof b !== "function" && b !== null)
        throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spreadArray(to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

function isFunction(value) {
    return typeof value === 'function';
}

function createErrorClass(createImpl) {
    var _super = function (instance) {
        Error.call(instance);
        instance.stack = new Error().stack;
    };
    var ctorFunc = createImpl(_super);
    ctorFunc.prototype = Object.create(Error.prototype);
    ctorFunc.prototype.constructor = ctorFunc;
    return ctorFunc;
}

var UnsubscriptionError = createErrorClass(function (_super) {
    return function UnsubscriptionErrorImpl(errors) {
        _super(this);
        this.message = errors
            ? errors.length + " errors occurred during unsubscription:\n" + errors.map(function (err, i) { return i + 1 + ") " + err.toString(); }).join('\n  ')
            : '';
        this.name = 'UnsubscriptionError';
        this.errors = errors;
    };
});

function arrRemove(arr, item) {
    if (arr) {
        var index = arr.indexOf(item);
        0 <= index && arr.splice(index, 1);
    }
}

var Subscription = (function () {
    function Subscription(initialTeardown) {
        this.initialTeardown = initialTeardown;
        this.closed = false;
        this._parentage = null;
        this._finalizers = null;
    }
    Subscription.prototype.unsubscribe = function () {
        var e_1, _a, e_2, _b;
        var errors;
        if (!this.closed) {
            this.closed = true;
            var _parentage = this._parentage;
            if (_parentage) {
                this._parentage = null;
                if (Array.isArray(_parentage)) {
                    try {
                        for (var _parentage_1 = __values(_parentage), _parentage_1_1 = _parentage_1.next(); !_parentage_1_1.done; _parentage_1_1 = _parentage_1.next()) {
                            var parent_1 = _parentage_1_1.value;
                            parent_1.remove(this);
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_parentage_1_1 && !_parentage_1_1.done && (_a = _parentage_1.return)) _a.call(_parentage_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                }
                else {
                    _parentage.remove(this);
                }
            }
            var initialFinalizer = this.initialTeardown;
            if (isFunction(initialFinalizer)) {
                try {
                    initialFinalizer();
                }
                catch (e) {
                    errors = e instanceof UnsubscriptionError ? e.errors : [e];
                }
            }
            var _finalizers = this._finalizers;
            if (_finalizers) {
                this._finalizers = null;
                try {
                    for (var _finalizers_1 = __values(_finalizers), _finalizers_1_1 = _finalizers_1.next(); !_finalizers_1_1.done; _finalizers_1_1 = _finalizers_1.next()) {
                        var finalizer = _finalizers_1_1.value;
                        try {
                            execFinalizer(finalizer);
                        }
                        catch (err) {
                            errors = errors !== null && errors !== void 0 ? errors : [];
                            if (err instanceof UnsubscriptionError) {
                                errors = __spreadArray(__spreadArray([], __read(errors)), __read(err.errors));
                            }
                            else {
                                errors.push(err);
                            }
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_finalizers_1_1 && !_finalizers_1_1.done && (_b = _finalizers_1.return)) _b.call(_finalizers_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            if (errors) {
                throw new UnsubscriptionError(errors);
            }
        }
    };
    Subscription.prototype.add = function (teardown) {
        var _a;
        if (teardown && teardown !== this) {
            if (this.closed) {
                execFinalizer(teardown);
            }
            else {
                if (teardown instanceof Subscription) {
                    if (teardown.closed || teardown._hasParent(this)) {
                        return;
                    }
                    teardown._addParent(this);
                }
                (this._finalizers = (_a = this._finalizers) !== null && _a !== void 0 ? _a : []).push(teardown);
            }
        }
    };
    Subscription.prototype._hasParent = function (parent) {
        var _parentage = this._parentage;
        return _parentage === parent || (Array.isArray(_parentage) && _parentage.includes(parent));
    };
    Subscription.prototype._addParent = function (parent) {
        var _parentage = this._parentage;
        this._parentage = Array.isArray(_parentage) ? (_parentage.push(parent), _parentage) : _parentage ? [_parentage, parent] : parent;
    };
    Subscription.prototype._removeParent = function (parent) {
        var _parentage = this._parentage;
        if (_parentage === parent) {
            this._parentage = null;
        }
        else if (Array.isArray(_parentage)) {
            arrRemove(_parentage, parent);
        }
    };
    Subscription.prototype.remove = function (teardown) {
        var _finalizers = this._finalizers;
        _finalizers && arrRemove(_finalizers, teardown);
        if (teardown instanceof Subscription) {
            teardown._removeParent(this);
        }
    };
    Subscription.EMPTY = (function () {
        var empty = new Subscription();
        empty.closed = true;
        return empty;
    })();
    return Subscription;
}());
var EMPTY_SUBSCRIPTION = Subscription.EMPTY;
function isSubscription(value) {
    return (value instanceof Subscription ||
        (value && 'closed' in value && isFunction(value.remove) && isFunction(value.add) && isFunction(value.unsubscribe)));
}
function execFinalizer(finalizer) {
    if (isFunction(finalizer)) {
        finalizer();
    }
    else {
        finalizer.unsubscribe();
    }
}

var config = {
    onUnhandledError: null,
    onStoppedNotification: null,
    Promise: undefined,
    useDeprecatedSynchronousErrorHandling: false,
    useDeprecatedNextContext: false,
};

var timeoutProvider = {
    setTimeout: function (handler, timeout) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var delegate = timeoutProvider.delegate;
        if (delegate === null || delegate === void 0 ? void 0 : delegate.setTimeout) {
            return delegate.setTimeout.apply(delegate, __spreadArray([handler, timeout], __read(args)));
        }
        return setTimeout.apply(void 0, __spreadArray([handler, timeout], __read(args)));
    },
    clearTimeout: function (handle) {
        var delegate = timeoutProvider.delegate;
        return ((delegate === null || delegate === void 0 ? void 0 : delegate.clearTimeout) || clearTimeout)(handle);
    },
    delegate: undefined,
};

function reportUnhandledError(err) {
    timeoutProvider.setTimeout(function () {
        {
            throw err;
        }
    });
}

function noop() { }

function errorContext(cb) {
    {
        cb();
    }
}

var Subscriber = (function (_super) {
    __extends(Subscriber, _super);
    function Subscriber(destination) {
        var _this = _super.call(this) || this;
        _this.isStopped = false;
        if (destination) {
            _this.destination = destination;
            if (isSubscription(destination)) {
                destination.add(_this);
            }
        }
        else {
            _this.destination = EMPTY_OBSERVER;
        }
        return _this;
    }
    Subscriber.create = function (next, error, complete) {
        return new SafeSubscriber(next, error, complete);
    };
    Subscriber.prototype.next = function (value) {
        if (this.isStopped) ;
        else {
            this._next(value);
        }
    };
    Subscriber.prototype.error = function (err) {
        if (this.isStopped) ;
        else {
            this.isStopped = true;
            this._error(err);
        }
    };
    Subscriber.prototype.complete = function () {
        if (this.isStopped) ;
        else {
            this.isStopped = true;
            this._complete();
        }
    };
    Subscriber.prototype.unsubscribe = function () {
        if (!this.closed) {
            this.isStopped = true;
            _super.prototype.unsubscribe.call(this);
            this.destination = null;
        }
    };
    Subscriber.prototype._next = function (value) {
        this.destination.next(value);
    };
    Subscriber.prototype._error = function (err) {
        try {
            this.destination.error(err);
        }
        finally {
            this.unsubscribe();
        }
    };
    Subscriber.prototype._complete = function () {
        try {
            this.destination.complete();
        }
        finally {
            this.unsubscribe();
        }
    };
    return Subscriber;
}(Subscription));
var _bind = Function.prototype.bind;
function bind(fn, thisArg) {
    return _bind.call(fn, thisArg);
}
var ConsumerObserver = (function () {
    function ConsumerObserver(partialObserver) {
        this.partialObserver = partialObserver;
    }
    ConsumerObserver.prototype.next = function (value) {
        var partialObserver = this.partialObserver;
        if (partialObserver.next) {
            try {
                partialObserver.next(value);
            }
            catch (error) {
                handleUnhandledError(error);
            }
        }
    };
    ConsumerObserver.prototype.error = function (err) {
        var partialObserver = this.partialObserver;
        if (partialObserver.error) {
            try {
                partialObserver.error(err);
            }
            catch (error) {
                handleUnhandledError(error);
            }
        }
        else {
            handleUnhandledError(err);
        }
    };
    ConsumerObserver.prototype.complete = function () {
        var partialObserver = this.partialObserver;
        if (partialObserver.complete) {
            try {
                partialObserver.complete();
            }
            catch (error) {
                handleUnhandledError(error);
            }
        }
    };
    return ConsumerObserver;
}());
var SafeSubscriber = (function (_super) {
    __extends(SafeSubscriber, _super);
    function SafeSubscriber(observerOrNext, error, complete) {
        var _this = _super.call(this) || this;
        var partialObserver;
        if (isFunction(observerOrNext) || !observerOrNext) {
            partialObserver = {
                next: (observerOrNext !== null && observerOrNext !== void 0 ? observerOrNext : undefined),
                error: error !== null && error !== void 0 ? error : undefined,
                complete: complete !== null && complete !== void 0 ? complete : undefined,
            };
        }
        else {
            var context_1;
            if (_this && config.useDeprecatedNextContext) {
                context_1 = Object.create(observerOrNext);
                context_1.unsubscribe = function () { return _this.unsubscribe(); };
                partialObserver = {
                    next: observerOrNext.next && bind(observerOrNext.next, context_1),
                    error: observerOrNext.error && bind(observerOrNext.error, context_1),
                    complete: observerOrNext.complete && bind(observerOrNext.complete, context_1),
                };
            }
            else {
                partialObserver = observerOrNext;
            }
        }
        _this.destination = new ConsumerObserver(partialObserver);
        return _this;
    }
    return SafeSubscriber;
}(Subscriber));
function handleUnhandledError(error) {
    {
        reportUnhandledError(error);
    }
}
function defaultErrorHandler(err) {
    throw err;
}
var EMPTY_OBSERVER = {
    closed: true,
    next: noop,
    error: defaultErrorHandler,
    complete: noop,
};

var observable = (function () { return (typeof Symbol === 'function' && Symbol.observable) || '@@observable'; })();

function identity(x) {
    return x;
}

function pipeFromArray(fns) {
    if (fns.length === 0) {
        return identity;
    }
    if (fns.length === 1) {
        return fns[0];
    }
    return function piped(input) {
        return fns.reduce(function (prev, fn) { return fn(prev); }, input);
    };
}

var Observable = (function () {
    function Observable(subscribe) {
        if (subscribe) {
            this._subscribe = subscribe;
        }
    }
    Observable.prototype.lift = function (operator) {
        var observable = new Observable();
        observable.source = this;
        observable.operator = operator;
        return observable;
    };
    Observable.prototype.subscribe = function (observerOrNext, error, complete) {
        var _this = this;
        var subscriber = isSubscriber(observerOrNext) ? observerOrNext : new SafeSubscriber(observerOrNext, error, complete);
        errorContext(function () {
            var _a = _this, operator = _a.operator, source = _a.source;
            subscriber.add(operator
                ?
                    operator.call(subscriber, source)
                : source
                    ?
                        _this._subscribe(subscriber)
                    :
                        _this._trySubscribe(subscriber));
        });
        return subscriber;
    };
    Observable.prototype._trySubscribe = function (sink) {
        try {
            return this._subscribe(sink);
        }
        catch (err) {
            sink.error(err);
        }
    };
    Observable.prototype.forEach = function (next, promiseCtor) {
        var _this = this;
        promiseCtor = getPromiseCtor(promiseCtor);
        return new promiseCtor(function (resolve, reject) {
            var subscriber = new SafeSubscriber({
                next: function (value) {
                    try {
                        next(value);
                    }
                    catch (err) {
                        reject(err);
                        subscriber.unsubscribe();
                    }
                },
                error: reject,
                complete: resolve,
            });
            _this.subscribe(subscriber);
        });
    };
    Observable.prototype._subscribe = function (subscriber) {
        var _a;
        return (_a = this.source) === null || _a === void 0 ? void 0 : _a.subscribe(subscriber);
    };
    Observable.prototype[observable] = function () {
        return this;
    };
    Observable.prototype.pipe = function () {
        var operations = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            operations[_i] = arguments[_i];
        }
        return pipeFromArray(operations)(this);
    };
    Observable.prototype.toPromise = function (promiseCtor) {
        var _this = this;
        promiseCtor = getPromiseCtor(promiseCtor);
        return new promiseCtor(function (resolve, reject) {
            var value;
            _this.subscribe(function (x) { return (value = x); }, function (err) { return reject(err); }, function () { return resolve(value); });
        });
    };
    Observable.create = function (subscribe) {
        return new Observable(subscribe);
    };
    return Observable;
}());
function getPromiseCtor(promiseCtor) {
    var _a;
    return (_a = promiseCtor !== null && promiseCtor !== void 0 ? promiseCtor : config.Promise) !== null && _a !== void 0 ? _a : Promise;
}
function isObserver(value) {
    return value && isFunction(value.next) && isFunction(value.error) && isFunction(value.complete);
}
function isSubscriber(value) {
    return (value && value instanceof Subscriber) || (isObserver(value) && isSubscription(value));
}

function hasLift(source) {
    return isFunction(source === null || source === void 0 ? void 0 : source.lift);
}
function operate(init) {
    return function (source) {
        if (hasLift(source)) {
            return source.lift(function (liftedSource) {
                try {
                    return init(liftedSource, this);
                }
                catch (err) {
                    this.error(err);
                }
            });
        }
        throw new TypeError('Unable to lift unknown Observable type');
    };
}

function createOperatorSubscriber(destination, onNext, onComplete, onError, onFinalize) {
    return new OperatorSubscriber(destination, onNext, onComplete, onError, onFinalize);
}
var OperatorSubscriber = (function (_super) {
    __extends(OperatorSubscriber, _super);
    function OperatorSubscriber(destination, onNext, onComplete, onError, onFinalize, shouldUnsubscribe) {
        var _this = _super.call(this, destination) || this;
        _this.onFinalize = onFinalize;
        _this.shouldUnsubscribe = shouldUnsubscribe;
        _this._next = onNext
            ? function (value) {
                try {
                    onNext(value);
                }
                catch (err) {
                    destination.error(err);
                }
            }
            : _super.prototype._next;
        _this._error = onError
            ? function (err) {
                try {
                    onError(err);
                }
                catch (err) {
                    destination.error(err);
                }
                finally {
                    this.unsubscribe();
                }
            }
            : _super.prototype._error;
        _this._complete = onComplete
            ? function () {
                try {
                    onComplete();
                }
                catch (err) {
                    destination.error(err);
                }
                finally {
                    this.unsubscribe();
                }
            }
            : _super.prototype._complete;
        return _this;
    }
    OperatorSubscriber.prototype.unsubscribe = function () {
        var _a;
        if (!this.shouldUnsubscribe || this.shouldUnsubscribe()) {
            var closed_1 = this.closed;
            _super.prototype.unsubscribe.call(this);
            !closed_1 && ((_a = this.onFinalize) === null || _a === void 0 ? void 0 : _a.call(this));
        }
    };
    return OperatorSubscriber;
}(Subscriber));

var ObjectUnsubscribedError = createErrorClass(function (_super) {
    return function ObjectUnsubscribedErrorImpl() {
        _super(this);
        this.name = 'ObjectUnsubscribedError';
        this.message = 'object unsubscribed';
    };
});

var Subject = (function (_super) {
    __extends(Subject, _super);
    function Subject() {
        var _this = _super.call(this) || this;
        _this.closed = false;
        _this.currentObservers = null;
        _this.observers = [];
        _this.isStopped = false;
        _this.hasError = false;
        _this.thrownError = null;
        return _this;
    }
    Subject.prototype.lift = function (operator) {
        var subject = new AnonymousSubject(this, this);
        subject.operator = operator;
        return subject;
    };
    Subject.prototype._throwIfClosed = function () {
        if (this.closed) {
            throw new ObjectUnsubscribedError();
        }
    };
    Subject.prototype.next = function (value) {
        var _this = this;
        errorContext(function () {
            var e_1, _a;
            _this._throwIfClosed();
            if (!_this.isStopped) {
                if (!_this.currentObservers) {
                    _this.currentObservers = Array.from(_this.observers);
                }
                try {
                    for (var _b = __values(_this.currentObservers), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var observer = _c.value;
                        observer.next(value);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
        });
    };
    Subject.prototype.error = function (err) {
        var _this = this;
        errorContext(function () {
            _this._throwIfClosed();
            if (!_this.isStopped) {
                _this.hasError = _this.isStopped = true;
                _this.thrownError = err;
                var observers = _this.observers;
                while (observers.length) {
                    observers.shift().error(err);
                }
            }
        });
    };
    Subject.prototype.complete = function () {
        var _this = this;
        errorContext(function () {
            _this._throwIfClosed();
            if (!_this.isStopped) {
                _this.isStopped = true;
                var observers = _this.observers;
                while (observers.length) {
                    observers.shift().complete();
                }
            }
        });
    };
    Subject.prototype.unsubscribe = function () {
        this.isStopped = this.closed = true;
        this.observers = this.currentObservers = null;
    };
    Object.defineProperty(Subject.prototype, "observed", {
        get: function () {
            var _a;
            return ((_a = this.observers) === null || _a === void 0 ? void 0 : _a.length) > 0;
        },
        enumerable: false,
        configurable: true
    });
    Subject.prototype._trySubscribe = function (subscriber) {
        this._throwIfClosed();
        return _super.prototype._trySubscribe.call(this, subscriber);
    };
    Subject.prototype._subscribe = function (subscriber) {
        this._throwIfClosed();
        this._checkFinalizedStatuses(subscriber);
        return this._innerSubscribe(subscriber);
    };
    Subject.prototype._innerSubscribe = function (subscriber) {
        var _this = this;
        var _a = this, hasError = _a.hasError, isStopped = _a.isStopped, observers = _a.observers;
        if (hasError || isStopped) {
            return EMPTY_SUBSCRIPTION;
        }
        this.currentObservers = null;
        observers.push(subscriber);
        return new Subscription(function () {
            _this.currentObservers = null;
            arrRemove(observers, subscriber);
        });
    };
    Subject.prototype._checkFinalizedStatuses = function (subscriber) {
        var _a = this, hasError = _a.hasError, thrownError = _a.thrownError, isStopped = _a.isStopped;
        if (hasError) {
            subscriber.error(thrownError);
        }
        else if (isStopped) {
            subscriber.complete();
        }
    };
    Subject.prototype.asObservable = function () {
        var observable = new Observable();
        observable.source = this;
        return observable;
    };
    Subject.create = function (destination, source) {
        return new AnonymousSubject(destination, source);
    };
    return Subject;
}(Observable));
var AnonymousSubject = (function (_super) {
    __extends(AnonymousSubject, _super);
    function AnonymousSubject(destination, source) {
        var _this = _super.call(this) || this;
        _this.destination = destination;
        _this.source = source;
        return _this;
    }
    AnonymousSubject.prototype.next = function (value) {
        var _a, _b;
        (_b = (_a = this.destination) === null || _a === void 0 ? void 0 : _a.next) === null || _b === void 0 ? void 0 : _b.call(_a, value);
    };
    AnonymousSubject.prototype.error = function (err) {
        var _a, _b;
        (_b = (_a = this.destination) === null || _a === void 0 ? void 0 : _a.error) === null || _b === void 0 ? void 0 : _b.call(_a, err);
    };
    AnonymousSubject.prototype.complete = function () {
        var _a, _b;
        (_b = (_a = this.destination) === null || _a === void 0 ? void 0 : _a.complete) === null || _b === void 0 ? void 0 : _b.call(_a);
    };
    AnonymousSubject.prototype._subscribe = function (subscriber) {
        var _a, _b;
        return (_b = (_a = this.source) === null || _a === void 0 ? void 0 : _a.subscribe(subscriber)) !== null && _b !== void 0 ? _b : EMPTY_SUBSCRIPTION;
    };
    return AnonymousSubject;
}(Subject));

var BehaviorSubject = (function (_super) {
    __extends(BehaviorSubject, _super);
    function BehaviorSubject(_value) {
        var _this = _super.call(this) || this;
        _this._value = _value;
        return _this;
    }
    Object.defineProperty(BehaviorSubject.prototype, "value", {
        get: function () {
            return this.getValue();
        },
        enumerable: false,
        configurable: true
    });
    BehaviorSubject.prototype._subscribe = function (subscriber) {
        var subscription = _super.prototype._subscribe.call(this, subscriber);
        !subscription.closed && subscriber.next(this._value);
        return subscription;
    };
    BehaviorSubject.prototype.getValue = function () {
        var _a = this, hasError = _a.hasError, thrownError = _a.thrownError, _value = _a._value;
        if (hasError) {
            throw thrownError;
        }
        this._throwIfClosed();
        return _value;
    };
    BehaviorSubject.prototype.next = function (value) {
        _super.prototype.next.call(this, (this._value = value));
    };
    return BehaviorSubject;
}(Subject));

function fromPromise(promise) {
    return new Observable(function (subscriber) {
        promise
            .then(function (value) {
            if (!subscriber.closed) {
                subscriber.next(value);
                subscriber.complete();
            }
        }, function (err) { return subscriber.error(err); })
            .then(null, reportUnhandledError);
    });
}

function tap(observerOrNext, error, complete) {
    var tapObserver = isFunction(observerOrNext) || error || complete
        ?
            { next: observerOrNext, error: error, complete: complete }
        : observerOrNext;
    return tapObserver
        ? operate(function (source, subscriber) {
            var _a;
            (_a = tapObserver.subscribe) === null || _a === void 0 ? void 0 : _a.call(tapObserver);
            var isUnsub = true;
            source.subscribe(createOperatorSubscriber(subscriber, function (value) {
                var _a;
                (_a = tapObserver.next) === null || _a === void 0 ? void 0 : _a.call(tapObserver, value);
                subscriber.next(value);
            }, function () {
                var _a;
                isUnsub = false;
                (_a = tapObserver.complete) === null || _a === void 0 ? void 0 : _a.call(tapObserver);
                subscriber.complete();
            }, function (err) {
                var _a;
                isUnsub = false;
                (_a = tapObserver.error) === null || _a === void 0 ? void 0 : _a.call(tapObserver, err);
                subscriber.error(err);
            }, function () {
                var _a, _b;
                if (isUnsub) {
                    (_a = tapObserver.unsubscribe) === null || _a === void 0 ? void 0 : _a.call(tapObserver);
                }
                (_b = tapObserver.finalize) === null || _b === void 0 ? void 0 : _b.call(tapObserver);
            }));
        })
        :
            identity;
}

var ECreateSignedDocStatus;
(function (ECreateSignedDocStatus) {
    ECreateSignedDocStatus["IDLE"] = "IDLE";
    ECreateSignedDocStatus["PENDING"] = "PENDING";
    ECreateSignedDocStatus["ERROR"] = "ERROR";
    ECreateSignedDocStatus["DONE"] = "DONE";
})(ECreateSignedDocStatus || (ECreateSignedDocStatus = {}));
class SignedDocStore {
    state; //Observable<TCreateSignedDocState>;
    apiProvider;
    constructor(apiProvider) {
        this.apiProvider = apiProvider;
        this.state = new BehaviorSubject({ status: ECreateSignedDocStatus.IDLE });
    }
    get state$() {
        return this.state.asObservable();
    }
    createSignedDoc(signature, unsigned_text) {
        const ctx = {
            strategy: {
                type: ESignStrategy.Qualified,
                software: ESignSoftware.CPro,
                certificate_identifier: 'blah blah' // fixme: WTF??
            },
            content: {
                type: 'text',
                unsigned_message: unsigned_text
            },
            signature: signature
        };
        this.state.next({ status: ECreateSignedDocStatus.PENDING, pending_message: 'Отправка данных на бек' });
        const sub = fromPromise(this.apiProvider.createSignedDocument(ctx))
            .pipe(tap(console.log)).subscribe({
            next: (result) => this.state.next({ status: ECreateSignedDocStatus.DONE, result: { id: result.doc_id } }),
            error: error => this.state.next({ status: ECreateSignedDocStatus.ERROR, error }),
            complete: () => sub.unsubscribe()
        });
    }
}

class B2BCenterByUuidCreateDocProvider {
    access_token;
    url;
    constructor(access_token, url = document?.location?.href ?? 'https://b2b-center.ru.test/personal/iframe-sign-external/') {
        this.access_token = access_token;
        this.url = url;
    }
    async createSignedDocument(context) {
        try {
            // fixme: uuid fiction
            const response = await fetch(this.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + this.access_token // FIXME тут надо попробовать с cookie
                },
                body: JSON.stringify(context)
            });
            const data = await response.json();
            console.log('B2BCenterByUuidCreateDocProvider create success.', data);
            return { doc_id: data.doc_id ?? 123 };
        }
        catch (error) {
            console.error(error);
            throw error;
        }
    }
}

class CreateSignedDocPresenter {
    store;
    _view;
    constructor() {
        // fixme: DI Provider
        // const provider = new BackendRestJsApiProvider()
        // fixme: ну вот тут уже должна авторизация происходить по куке, зачем проталкивать токен туда сюда то???
        const provider = new B2BCenterByUuidCreateDocProvider('eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIzMjEzZDdkNi05ODMxLTRmZjEtODExMC01NzIyZDZkYjg0OTQiLCJhdWQiOiJjYTcyMzFiZS0yNGY1LTQ0OWItYjkwYS1lNDk1NDJiYTQxN2IiLCJtc3ViIjpudWxsLCJzaWQiOiJlMjQyZTk1Zi05NDViLTQyMGMtODc0My1kMGQxMzRmZjc1YTIiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxOTMzMzUzNCwiZXhwIjoxNzIwMTk3NTM0fQ.AHkYSy8byFqAPVS4GU_FClE2FFYsw-47TwaaAF7qsT8pNPY8JPcznnr6nPk1l6hXGhEIlZp4uxNhpX39Gi92vq9EAR4QgrCG585zvzGp8Pma1n8f4GYeWNr5wB3AoHlPmEbfR4_hkkOw9UXxrVocmPYlIeTtdoKrrwCcs02QHQpf5zoO');
        this.store = new SignedDocStore(provider);
    }
    set view(v) {
        this._view = v;
    }
    get state() {
        return this.store.state$;
    }
    create(sig, unsigned_text) {
        if (this._view) {
            const subs = this.state.subscribe({
                next: (docState) => {
                    const { status } = docState;
                    switch (status) {
                        case ECreateSignedDocStatus.PENDING:
                            this._view?.onCreateSignedDocPending(docState.pending_message);
                            break;
                        case ECreateSignedDocStatus.DONE:
                            this._view?.onCreateSignedDocDone({ doc_id: docState.result.id });
                            break;
                        case ECreateSignedDocStatus.ERROR:
                            this._view?.onCreateSignedDocError(docState.error);
                            break;
                    }
                },
                error: (error) => {
                    this._view?.onCreateSignedDocError(error);
                    subs.unsubscribe();
                },
                complete: () => {
                    subs.unsubscribe();
                }
            });
        }
        console.log('create DOCUMENT');
        this.store.createSignedDoc(sig, unsigned_text);
    }
}

/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */
var SignContentType;
(function (SignContentType) {
    SignContentType["SignContextText"] = "SignContextText";
    SignContentType["SignContextFiles"] = "SignContextFiles";
})(SignContentType || (SignContentType = {}));
var SignContentEncoding;
(function (SignContentEncoding) {
    SignContentEncoding["None"] = "none";
    SignContentEncoding["Base64"] = "base64";
})(SignContentEncoding || (SignContentEncoding = {}));
var SignatureType;
(function (SignatureType) {
    SignatureType["BES"] = "BES";
    SignatureType["XLT1"] = "XLT1";
})(SignatureType || (SignatureType = {}));
var WidgetSignStrategyType;
(function (WidgetSignStrategyType) {
    WidgetSignStrategyType["Base"] = "base";
    WidgetSignStrategyType["Iframe"] = "iframe";
})(WidgetSignStrategyType || (WidgetSignStrategyType = {}));
var ContentType;
(function (ContentType) {
    ContentType["Json"] = "application/json";
    ContentType["FormData"] = "multipart/form-data";
    ContentType["UrlEncoded"] = "application/x-www-form-urlencoded";
    ContentType["Text"] = "text/plain";
})(ContentType || (ContentType = {}));
class HttpClient {
    baseUrl = "http://b2b-center.ru.test/eds/external_operation/v1";
    securityData = null;
    securityWorker;
    abortControllers = new Map();
    customFetch = (...fetchParams) => fetch(...fetchParams);
    baseApiParams = {
        credentials: "same-origin",
        headers: {},
        redirect: "follow",
        referrerPolicy: "no-referrer",
    };
    constructor(apiConfig = {}) {
        Object.assign(this, apiConfig);
    }
    setSecurityData = (data) => {
        this.securityData = data;
    };
    encodeQueryParam(key, value) {
        const encodedKey = encodeURIComponent(key);
        return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
    }
    addQueryParam(query, key) {
        return this.encodeQueryParam(key, query[key]);
    }
    addArrayQueryParam(query, key) {
        const value = query[key];
        return value.map((v) => this.encodeQueryParam(key, v)).join("&");
    }
    toQueryString(rawQuery) {
        const query = rawQuery || {};
        const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
        return keys
            .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
            .join("&");
    }
    addQueryParams(rawQuery) {
        const queryString = this.toQueryString(rawQuery);
        return queryString ? `?${queryString}` : "";
    }
    contentFormatters = {
        [ContentType.Json]: (input) => input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
        [ContentType.Text]: (input) => (input !== null && typeof input !== "string" ? JSON.stringify(input) : input),
        [ContentType.FormData]: (input) => (Array.from(input.keys()) || []).reduce((formData, key) => {
            const property = input.get(key);
            formData.append(key, property instanceof Blob
                ? property
                : typeof property === "object" && property !== null
                    ? JSON.stringify(property)
                    : `${property}`);
            return formData;
        }, new FormData()),
        [ContentType.UrlEncoded]: (input) => this.toQueryString(input),
    };
    mergeRequestParams(params1, params2) {
        return {
            ...this.baseApiParams,
            ...params1,
            ...(params2 || {}),
            headers: {
                ...(this.baseApiParams.headers || {}),
                ...(params1.headers || {}),
                ...((params2 && params2.headers) || {}),
            },
        };
    }
    createAbortSignal = (cancelToken) => {
        if (this.abortControllers.has(cancelToken)) {
            const abortController = this.abortControllers.get(cancelToken);
            if (abortController) {
                return abortController.signal;
            }
            return void 0;
        }
        const abortController = new AbortController();
        this.abortControllers.set(cancelToken, abortController);
        return abortController.signal;
    };
    abortRequest = (cancelToken) => {
        const abortController = this.abortControllers.get(cancelToken);
        if (abortController) {
            abortController.abort();
            this.abortControllers.delete(cancelToken);
        }
    };
    request = async ({ body, secure, path, type, query, format, baseUrl, cancelToken, ...params }) => {
        const secureParams = ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
            this.securityWorker &&
            (await this.securityWorker(this.securityData))) ||
            {};
        const requestParams = this.mergeRequestParams(params, secureParams);
        const queryString = query && this.toQueryString(query);
        const payloadFormatter = this.contentFormatters[type || ContentType.Json];
        const responseFormat = format || requestParams.format;
        return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
            ...requestParams,
            headers: {
                ...(requestParams.headers || {}),
                ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
            },
            signal: (cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal) || null,
            body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
        }).then(async (response) => {
            const r = response.clone();
            r.data = null;
            r.error = null;
            const data = !responseFormat
                ? r
                : await response[responseFormat]()
                    .then((data) => {
                    if (r.ok) {
                        r.data = data;
                    }
                    else {
                        r.error = data;
                    }
                    return r;
                })
                    .catch((e) => {
                    r.error = e;
                    return r;
                });
            if (cancelToken) {
                this.abortControllers.delete(cancelToken);
            }
            if (!response.ok)
                throw data;
            return data;
        });
    };
}
/**
 * @title Схема API для хранилища подписанных документов
 * @version 1.0.1
 * @baseUrl http://b2b-center.ru.test/eds/external_operation/v1
 *
 * Описывает конечные точки для проверки, сохранения подписанного документа ЭП В хранилище.  Так же описаны вспомогательные для этого процесса действия (конфиг для виджета, в зависимости от возможностей пользователя и окружения, формирование архива и визуализации документа).
 */
class Api extends HttpClient {
    api = {
        /**
     * No description
     *
     * @name EdsSignedDocDetail
     * @request GET:/api/eds/signed_doc/{id}
     * @response `200` `{
        doc_id: number,
        user_id?: number,
        firm_id?: number,
        doc_type?: string,
        signature?: string,
        signers?: ({
        serial_number?: string,
        thumbprint?: string,
        issuer?: string,
        owner?: string,
    
    })[],
    
    }` Doc
     */
        edsSignedDocDetail: (id, params = {}) => this.request({
            path: `/api/eds/signed_doc/${id}`,
            method: "GET",
            format: "json",
            ...params,
        }),
        /**
         * No description
         *
         * @name EdsSignConfigTextCreate
         * @request POST:/api/eds/sign_config_text
         * @response `200` `SignConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП текста
         * @response `400` `Error`
         */
        edsSignConfigTextCreate: (data, params = {}) => this.request({
            path: `/api/eds/sign_config_text`,
            method: "POST",
            body: data,
            type: ContentType.Json,
            format: "json",
            ...params,
        }),
        /**
         * No description
         *
         * @name EdsSignConfigFilesCreate
         * @request POST:/api/eds/sign_config_files
         * @response `200` `SignFilesConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП списка файлов из FileStorage
         * @response `400` `Error`
         */
        edsSignConfigFilesCreate: (data, params = {}) => this.request({
            path: `/api/eds/sign_config_files`,
            method: "POST",
            body: data,
            type: ContentType.Json,
            format: "json",
            ...params,
        }),
        /**
         * No description
         *
         * @name EdsSignedDocumentTextCreate
         * @request POST:/api/eds/signed_document_text
         * @response `200` `SignedDocumentCreatedResponse` Created
         * @response `400` `Error`
         */
        edsSignedDocumentTextCreate: (data, params = {}) => this.request({
            path: `/api/eds/signed_document_text`,
            method: "POST",
            body: data,
            type: ContentType.Json,
            format: "json",
            ...params,
        }),
        /**
     * No description
     *
     * @name EdsCreateSignedDocIntentCreate
     * @request POST:/api/eds/create_signed_doc_intent
     * @response `200` `{
        uuid: OperationId,
    
    }` Ok
     * @response `400` `Error`
     */
        edsCreateSignedDocIntentCreate: (data, params = {}) => this.request({
            path: `/api/eds/create_signed_doc_intent`,
            method: "POST",
            body: data,
            type: ContentType.Json,
            format: "json",
            ...params,
        }),
    };
}

// } from '@/api/BackendRestJsApi';
class BackendRestJsApiProvider {
    restOpenApiModel;
    constructor(accessToken) {
        const token = accessToken ??
            // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiXSwianRpIjoiYjJjM2RiMmEtZTlkYS00Yzg0LWIzNGEtODc5YzZjYWJkMWY2IiwiYXVkIjoiZGI3MGMwNDgtNzQzNS00OTE3LWI0ZDEtMTFhNzg0NTE0ZGQ3IiwibXN1YiI6bnVsbCwic2lkIjoiMTlhMzg4NTItOGQ4Mi00ZTJiLThjODItMDA2N2E1YzE3ZDQ0Iiwic3ViIjoiNjAxMzYiLCJpYXQiOjE3MDk1NjM0NjUsImV4cCI6MTcwOTU5OTQ2NX0.AdfdTCEWVWXyTzu680uPZIXVLTkyOOLWINeasWc_Z8iWA81x-VOg_YAK4uB5h5711DkyP3EIG1Tn8DilVKku1zNqAQuH5lBt3Qh26ExB-LXBFg1yzMTHaja19-DIFXRu5A_cyT1Jy1aCdC9Po_BNefexPSpFSrruwAND4MNwjgq74-jR';
            // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiI3OWNmZTA5MS1jNDUyLTRmZWItYmM4NS0wZGI0MDZmMGNiM2QiLCJhdWQiOiJkYjcwYzA0OC03NDM1LTQ5MTctYjRkMS0xMWE3ODQ1MTRkZDciLCJtc3ViIjpudWxsLCJzaWQiOiJmNzk2ZmMyNi05NWVmLTRjNTgtYTBkZS01NmEzM2YxODAzMGEiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxNDY2MTgzMSwiZXhwIjoxNzE0Njk3ODMxfQ.AIFd2-t6QA7Rh64KA2kJaF83WIZNRxM4h5iCq51z9eQIKuwZNn81lxzgTAH5JBzxlvtkyzbrK1LzD0fNPKPmCxWrAXBBrymZjs29eTcxgtJdmNCMF3gEpEYttEmAUKXS-mjAHVLkFO8x0QfE3hZF0F4TMt3942cfq9VgL1ZZMOla3wGY';
            // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIyNzBkMzZkNC0zNWQ1LTQ1NTQtOTFlZi0xZDYzYjQ1ZmFhYmYiLCJhdWQiOiIyM2ZlNGJhOS0xOGRkLTQ0MDUtOWQxNC00MTgxY2Y1YTdlOTgiLCJtc3ViIjpudWxsLCJzaWQiOiI2NTdmNzNkZS04YmMxLTRmNTAtYjEyOC0xYjFjMmQyNmZmMDEiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxNzUxMjcwNSwiZXhwIjoxNzE3OTQ0NzA1fQ.AEYY-0zbBkbAbHEASPEMIVNg1IYmSKXCiILNnt2COwvSgJsKSIJiKpazZcP_jsCOOkc048nCk92-N7cL62reqZfkAWN4fxzgEn8WbshK0y2EHxxxKe-CBLS8nuFV15dX4cU_xMs3Aif_843o2hFpU8hD-Cs_TqkoaBI4u-xGILsZ9Rid'
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIzMjEzZDdkNi05ODMxLTRmZjEtODExMC01NzIyZDZkYjg0OTQiLCJhdWQiOiJjYTcyMzFiZS0yNGY1LTQ0OWItYjkwYS1lNDk1NDJiYTQxN2IiLCJtc3ViIjpudWxsLCJzaWQiOiJlMjQyZTk1Zi05NDViLTQyMGMtODc0My1kMGQxMzRmZjc1YTIiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxOTMzMzUzNCwiZXhwIjoxNzIwMTk3NTM0fQ.AHkYSy8byFqAPVS4GU_FClE2FFYsw-47TwaaAF7qsT8pNPY8JPcznnr6nPk1l6hXGhEIlZp4uxNhpX39Gi92vq9EAR4QgrCG585zvzGp8Pma1n8f4GYeWNr5wB3AoHlPmEbfR4_hkkOw9UXxrVocmPYlIeTtdoKrrwCcs02QHQpf5zoO';
        const resultConfig = {
            baseUrl: 'https://iframe-mvp.b2b.test'
        };
        const headers = { Authorization: `Bearer ${token}` };
        resultConfig.baseApiParams = { headers };
        // this.restOpenApiModel = new RestJsOpenApiModel(resultConfig);
        this.restOpenApiModel = new Api(resultConfig);
    }
    async provideSignConfiguration(ctx) {
        const req = {
            sign_content_type: SignContentType.SignContextText,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const { data, ok, error } = await this.restOpenApiModel.api.edsSignConfigTextCreate(req);
        if (!ok) {
            throw error ?? new Error('При запросе конфига упало с ошибкой');
        }
        const { payload } = data; // FIXME: решить, надо ли тут так или нефиг?
        const { qualified } = payload;
        let qual = undefined;
        if (qualified) {
            const { identifier_type, valid_certificate_identifiers } = qualified;
            qual = { identifier_type, valid_certificate_identifiers };
        }
        const cfg = {
            context: ctx
        };
        if (qual) {
            cfg.qualified = qual;
        }
        return cfg;
    }
    // fixme: переделать на нормальный возврат результата без контекстов
    async createSignedDocumentOld(context) {
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const req = {
            sign_content_type: SignContentType.SignContextText,
            content: (context.content.unsigned_message),
            signature: context.signature,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const { data, ok, error } = await this.restOpenApiModel.api.edsSignedDocumentTextCreate(req);
        if (!ok) {
            const err = new Error(error.title || 'Ошибка при попытке сохранить подписанный текстовый документ в хран');
            context.error = err;
            throw err;
        }
        if (!data.doc_id) {
            // FIXME
            throw new Error('ata.doc_id');
        }
        // FIXME: Херня какая то
        const doc = {
            doc_id: data.doc_id
        };
        context.doc = doc;
    }
    async createSignedDocument(context) {
        // throw new Error('OLOLOLO');
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const req = {
            sign_content_type: SignContentType.SignContextText,
            content: (context.content.unsigned_message),
            signature: context.signature,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const { data, ok, error } = await this.restOpenApiModel.api.edsSignedDocumentTextCreate(req);
        if (!ok) {
            const err = new Error(error.title || 'Ошибка при попытке сохранить подписанный текстовый документ в хран');
            context.error = err;
            throw err;
        }
        if (!data.doc_id) {
            // FIXME
            throw new Error('ata.doc_id');
        }
        // FIXME: Херня какая то
        const doc = {
            doc_id: data.doc_id
        };
        return doc;
    }
    async createSignIframeRequest(context) {
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const requestData = {
            widget_strategy: {
                type: WidgetSignStrategyType.Iframe
            },
            sign_context: {
                type: SignContentType.SignContextText,
                unsigned_message: context.content.unsigned_message,
                encoding: SignContentEncoding.None,
                is_detached: false
            },
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const { data, ok, error } = await this.restOpenApiModel.api.edsCreateSignedDocIntentCreate(requestData);
        if (!ok) {
            throw new Error(error.title);
        }
        return { uuid: data.uuid };
    }
    async fetchSignedDoc(id) {
        const { data, ok, error } = await this.restOpenApiModel.api.edsSignedDocDetail(id);
        if (!ok) {
            throw new Error(error.title);
        }
        console.log('Loaded doc data: ', data);
        return data;
    }
}

export { BackendRestJsApiProvider, CreateSignedDocPresenter, ECreateSignedDocStatus, ESignSoftware, ESignStrategy, SignedDocStore };
//# sourceMappingURL=index.js.map
