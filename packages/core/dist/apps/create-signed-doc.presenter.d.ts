import { TCreateSignedDocState } from '@/apps/signed-doc.store';
import { TSignature, TSignedDocument } from '@/types';
import { Observable } from 'rxjs';
export interface ICreateSignedDocView {
    onCreateSignedDocError: (error: Error) => void;
    onCreateSignedDocPending: (pending_message?: string) => void;
    onCreateSignedDocDone: (document: TSignedDocument) => void;
}
export declare class CreateSignedDocPresenter {
    private readonly store;
    private _view?;
    constructor();
    set view(v: ICreateSignedDocView);
    get state(): Observable<TCreateSignedDocState>;
    create(sig: TSignature, unsigned_text: string): void;
}
