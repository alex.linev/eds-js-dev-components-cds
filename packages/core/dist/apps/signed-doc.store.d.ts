import { BehaviorSubject, Observable } from 'rxjs';
import { IBackendApiCreateDocServiceProvider, TSignature } from '@/types';
export declare enum ECreateSignedDocStatus {
    IDLE = "IDLE",
    PENDING = "PENDING",
    ERROR = "ERROR",
    DONE = "DONE"
}
export type TCreateSignedDocResult = {
    id: number;
};
export type TCreateSignedDocState = {
    status: ECreateSignedDocStatus.IDLE;
} | {
    status: ECreateSignedDocStatus.PENDING;
    pending_message?: string;
} | {
    status: ECreateSignedDocStatus.ERROR;
    error: Error;
} | {
    status: ECreateSignedDocStatus.DONE;
    result: TCreateSignedDocResult;
};
export declare class SignedDocStore {
    readonly state: BehaviorSubject<TCreateSignedDocState>;
    private readonly apiProvider;
    constructor(apiProvider: IBackendApiCreateDocServiceProvider);
    get state$(): Observable<TCreateSignedDocState>;
    createSignedDoc(signature: TSignature, unsigned_text: string): void;
}
