import { IBackendApiCreateDocServiceProvider, TSignContext, TSignedDocument } from '@/types';
export declare class B2BCenterByUuidCreateDocProvider implements IBackendApiCreateDocServiceProvider {
    private readonly access_token;
    private readonly url;
    constructor(access_token: string, url?: string);
    createSignedDocument(context: TSignContext): Promise<TSignedDocument>;
}
