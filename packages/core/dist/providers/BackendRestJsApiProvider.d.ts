import { IBackendApiServiceProvider, TSignConfiguration, TSignContext, TSignedDocument } from '@/types';
export default class BackendRestJsApiProvider implements IBackendApiServiceProvider {
    private readonly restOpenApiModel;
    constructor(accessToken?: string);
    provideSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration>;
    createSignedDocumentOld(context: TSignContext): Promise<void>;
    createSignedDocument(context: TSignContext): Promise<TSignedDocument>;
    createSignIframeRequest(context: TSignContext): Promise<{
        uuid: string;
    }>;
    fetchSignedDoc(id: number): Promise<TSignedDocument>;
}
