/** Описывает ошибку */
export interface Error {
    title: string;
    description?: string;
    api_code: string;
    trace_id?: TraceID;
}
/**
 * @format int64
 * @min 1
 */
export type FileId = number;
export interface UserInfo {
    type?: "bearer_in_header";
}
export declare enum SignContentType {
    SignContextText = "SignContextText",
    SignContextFiles = "SignContextFiles"
}
export declare enum SignContentEncoding {
    None = "none",
    Base64 = "base64"
}
export type TraceID = string;
export type SignedDocumentId = number;
export type Signature = string;
export declare enum SignatureType {
    BES = "BES",
    XLT1 = "XLT1"
}
export type Hash = string;
export type Hashes = Hash[];
export interface FileHash {
    file_id: FileId;
    hash: Hash;
}
export type FileHashList = FileHash[];
/** Данные по зарегистрированным за пользователем сертификатам. На КЗ может быть оба вида сертификата у пользователя одновременно */
export interface QualifiedCertificateConfigItem {
    /** Для плагина КриптоПро используется отпечаток при поиске сертификатов в хранилище, у NCA - БИН */
    identifier_type: "thumbprint" | "bin";
    identifier: string;
    software: "CPro" | "NCA";
}
export interface QualifiedSignConfig {
    valid: QualifiedCertificateConfigItem[];
}
export interface SimpleSignConfig {
    owner: string;
    org_name: string;
}
export interface MockQualifiedConfig {
    user_name?: string;
    owner: string;
    org_name?: string;
    issuer: string;
    serial_number: string;
    thumbprint: string;
    valid_to: string;
    valid_from: string;
    algorithm?: string;
    certificate_body?: string;
}
/** Фейковые данные для заглушки (подпись, сертификат), которые необходимы, чтобы имитировать процесс формирования ЭП */
export interface MockSignConfig {
    signature?: Signature;
    /** Фейковые данные для отображения их как данные сертификата текущего пользователя */
    qualified?: MockQualifiedConfig[];
}
export declare enum WidgetSignStrategyType {
    Base = "base",
    Iframe = "iframe"
}
export interface WidgetSignStrategy {
    type?: WidgetSignStrategyType;
}
export type SignContext = BaseSignContext & (BaseSignContextTypeMapping<SignContentType.SignContextText, SignContextText> | BaseSignContextTypeMapping<SignContentType.SignContextFiles, SignContextFiles>);
export type SignContextText = BaseSignContext & {
    unsigned_message: string;
    encoding: SignContentEncoding;
    is_detached: boolean;
};
export type SignContextFiles = BaseSignContext & {
    file_ids?: FileId[];
};
export interface SignConfigRequest {
    widget_strategy?: WidgetSignStrategy;
    context?: SignContext;
    sign_content_type?: SignContentType;
    user_info?: UserInfo;
}
export type SignFilesConfigRequest = SignConfigRequest & {
    file_ids?: FileId[];
};
export type SignConfigResponse = BaseSignConfigResponse & (BaseSignConfigResponseTypeMapping<"text", any> | BaseSignConfigResponseTypeMapping<"files", any>);
export type SignFilesConfigResponse = SignConfigResponse & {
    type?: "files";
    hashes?: {
        file_id: number;
        hash: Hash;
    }[];
};
export interface SignedTextDocumentCreateRequest {
    content: string;
    sign_content_type: SignContentType;
    signature: Signature;
    user_info: UserInfo;
}
export interface SignedDocumentCreatedResponse {
    doc_id?: SignedDocumentId;
}
export type OperationId = string;
interface BaseSignContext {
    type: SignContentType;
}
type BaseSignContextTypeMapping<Key, Type> = {
    type: Key;
} & Type;
interface BaseSignConfigResponse {
    type: "text";
    qualified?: QualifiedSignConfig;
    simple?: SimpleSignConfig;
    /** Фейковые данные для заглушки (подпись, сертификат), которые необходимы, чтобы имитировать процесс формирования ЭП */
    mock?: MockSignConfig;
}
type BaseSignConfigResponseTypeMapping<Key, Type> = {
    type: Key;
} & Type;
export interface EdsCreateSignedDocIntentCreatePayload {
    sign_context: SignContext;
    widget_strategy: WidgetSignStrategy;
    user_info: UserInfo;
}
export declare namespace Api {
    /**
   * No description
   * @name EdsSignedDocDetail
   * @request GET:/api/eds/signed_doc/{id}
   * @response `200` `{
      doc_id: number,
      user_id?: number,
      firm_id?: number,
      doc_type?: string,
      signature?: string,
      signers?: ({
      serial_number?: string,
      thumbprint?: string,
      issuer?: string,
      owner?: string,
  
  })[],
  
  }` Doc
  */
    namespace EdsSignedDocDetail {
        type RequestParams = {
            id: number;
        };
        type RequestQuery = {};
        type RequestBody = never;
        type RequestHeaders = {};
        type ResponseBody = {
            doc_id: number;
            user_id?: number;
            firm_id?: number;
            doc_type?: string;
            signature?: string;
            signers?: {
                serial_number?: string;
                thumbprint?: string;
                issuer?: string;
                owner?: string;
            }[];
        };
    }
    /**
     * No description
     * @name EdsSignConfigTextCreate
     * @request POST:/api/eds/sign_config_text
     * @response `200` `SignConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП текста
     * @response `400` `Error`
     */
    namespace EdsSignConfigTextCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = SignConfigRequest;
        type RequestHeaders = {};
        type ResponseBody = SignConfigResponse;
    }
    /**
     * No description
     * @name EdsSignConfigFilesCreate
     * @request POST:/api/eds/sign_config_files
     * @response `200` `SignFilesConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП списка файлов из FileStorage
     * @response `400` `Error`
     */
    namespace EdsSignConfigFilesCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = SignFilesConfigRequest;
        type RequestHeaders = {};
        type ResponseBody = SignFilesConfigResponse;
    }
    /**
     * No description
     * @name EdsSignedDocumentTextCreate
     * @request POST:/api/eds/signed_document_text
     * @response `200` `SignedDocumentCreatedResponse` Created
     * @response `400` `Error`
     */
    namespace EdsSignedDocumentTextCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = SignedTextDocumentCreateRequest;
        type RequestHeaders = {};
        type ResponseBody = SignedDocumentCreatedResponse;
    }
    /**
   * No description
   * @name EdsCreateSignedDocIntentCreate
   * @request POST:/api/eds/create_signed_doc_intent
   * @response `200` `{
      uuid: OperationId,
  
  }` Ok
   * @response `400` `Error`
  */
    namespace EdsCreateSignedDocIntentCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = EdsCreateSignedDocIntentCreatePayload;
        type RequestHeaders = {};
        type ResponseBody = {
            uuid: OperationId;
        };
    }
}
export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;
export interface FullRequestParams extends Omit<RequestInit, "body"> {
    /** set parameter to `true` for call `securityWorker` for this request */
    secure?: boolean;
    /** request path */
    path: string;
    /** content type of request body */
    type?: ContentType;
    /** query params */
    query?: QueryParamsType;
    /** format of response (i.e. response.json() -> format: "json") */
    format?: ResponseFormat;
    /** request body */
    body?: unknown;
    /** base url */
    baseUrl?: string;
    /** request cancellation token */
    cancelToken?: CancelToken;
}
export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;
export interface ApiConfig<SecurityDataType = unknown> {
    baseUrl?: string;
    baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
    securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
    customFetch?: typeof fetch;
}
export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
    data: D;
    error: E;
}
type CancelToken = Symbol | string | number;
export declare enum ContentType {
    Json = "application/json",
    FormData = "multipart/form-data",
    UrlEncoded = "application/x-www-form-urlencoded",
    Text = "text/plain"
}
export declare class HttpClient<SecurityDataType = unknown> {
    baseUrl: string;
    private securityData;
    private securityWorker?;
    private abortControllers;
    private customFetch;
    private baseApiParams;
    constructor(apiConfig?: ApiConfig<SecurityDataType>);
    setSecurityData: (data: SecurityDataType | null) => void;
    protected encodeQueryParam(key: string, value: any): string;
    protected addQueryParam(query: QueryParamsType, key: string): string;
    protected addArrayQueryParam(query: QueryParamsType, key: string): any;
    protected toQueryString(rawQuery?: QueryParamsType): string;
    protected addQueryParams(rawQuery?: QueryParamsType): string;
    private contentFormatters;
    protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams;
    protected createAbortSignal: (cancelToken: CancelToken) => AbortSignal | undefined;
    abortRequest: (cancelToken: CancelToken) => void;
    request: <T = any, E = any>({ body, secure, path, type, query, format, baseUrl, cancelToken, ...params }: FullRequestParams) => Promise<HttpResponse<T, E>>;
}
/**
 * @title Схема API для хранилища подписанных документов
 * @version 1.0.1
 * @baseUrl http://b2b-center.ru.test/eds/external_operation/v1
 *
 * Описывает конечные точки для проверки, сохранения подписанного документа ЭП В хранилище.  Так же описаны вспомогательные для этого процесса действия (конфиг для виджета, в зависимости от возможностей пользователя и окружения, формирование архива и визуализации документа).
 */
export declare class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
    api: {
        /**
     * No description
     *
     * @name EdsSignedDocDetail
     * @request GET:/api/eds/signed_doc/{id}
     * @response `200` `{
        doc_id: number,
        user_id?: number,
        firm_id?: number,
        doc_type?: string,
        signature?: string,
        signers?: ({
        serial_number?: string,
        thumbprint?: string,
        issuer?: string,
        owner?: string,
    
    })[],
    
    }` Doc
     */
        edsSignedDocDetail: (id: number, params?: RequestParams) => Promise<HttpResponse<{
            doc_id: number;
            user_id?: number;
            firm_id?: number;
            doc_type?: string;
            signature?: string;
            signers?: {
                serial_number?: string;
                thumbprint?: string;
                issuer?: string;
                owner?: string;
            }[];
        }, any>>;
        /**
         * No description
         *
         * @name EdsSignConfigTextCreate
         * @request POST:/api/eds/sign_config_text
         * @response `200` `SignConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП текста
         * @response `400` `Error`
         */
        edsSignConfigTextCreate: (data: SignConfigRequest, params?: RequestParams) => Promise<HttpResponse<any, Error>>;
        /**
         * No description
         *
         * @name EdsSignConfigFilesCreate
         * @request POST:/api/eds/sign_config_files
         * @response `200` `SignFilesConfigResponse` Конфиг для Js с необходимыми данными для формаирования ЭП списка файлов из FileStorage
         * @response `400` `Error`
         */
        edsSignConfigFilesCreate: (data: SignFilesConfigRequest, params?: RequestParams) => Promise<HttpResponse<any, Error>>;
        /**
         * No description
         *
         * @name EdsSignedDocumentTextCreate
         * @request POST:/api/eds/signed_document_text
         * @response `200` `SignedDocumentCreatedResponse` Created
         * @response `400` `Error`
         */
        edsSignedDocumentTextCreate: (data: SignedTextDocumentCreateRequest, params?: RequestParams) => Promise<HttpResponse<SignedDocumentCreatedResponse, Error>>;
        /**
     * No description
     *
     * @name EdsCreateSignedDocIntentCreate
     * @request POST:/api/eds/create_signed_doc_intent
     * @response `200` `{
        uuid: OperationId,
    
    }` Ok
     * @response `400` `Error`
     */
        edsCreateSignedDocIntentCreate: (data: EdsCreateSignedDocIntentCreatePayload, params?: RequestParams) => Promise<HttpResponse<{
            uuid: OperationId;
        }, Error>>;
    };
}
export {};
