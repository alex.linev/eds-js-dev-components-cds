export type SimplePin = string;
export interface ConfigResponse {
    result?: {
        certificates: string[];
        simple?: {
            owner?: string;
            org_name?: string;
        };
        mock?: {
            signature?: string;
            qualified?: {
                user_name?: string;
                owner?: string;
                org_name?: string;
                issuer?: string;
                serial_number?: string;
                thumbprint?: string;
                valid_from?: string;
                valid_to?: string;
                algorithm?: string;
                certificate_body?: string;
            }[];
        };
    };
}
export interface FileStorageHashRequest {
    file_ids: number[];
}
export interface FileStorageHashResponse {
    result?: {
        file_id?: number;
        hash?: string;
    }[];
}
export interface FileStorageSignedDocRequest {
    signatures?: {
        file_id?: number;
        value?: string;
    }[];
}
export interface FileStorageSimpleSignedDocRequest {
    /** @example [345678,345679] */
    file_ids: number[];
    pin: SimplePin;
}
export interface FileStorageMockSignedDocRequest {
    /** @example [345678,345679] */
    file_ids?: number[];
}
export interface FileStorageSignedDocResponse {
    result?: {
        file_id?: number;
        sign_doc_id?: number;
    }[];
}
export interface FileStorageArchiveRequest {
    /** @example [345678,345679] */
    file_ids: number[];
    /** @example [9876,9877] */
    signed_doc_ids: number[];
}
export interface FileStorageArchiveResponse {
    result?: {
        file_id?: number;
    };
}
export interface FileStorageSignedDocViewResponse {
    result?: {
        file_id?: number;
        sign_doc_id?: number;
        signature?: string;
        signers?: SignerResponse[];
    };
}
export interface SignerResponse {
    serial_number?: string;
    thumbprint?: string;
    owner?: string;
    issuer?: string;
    valid_from?: string;
    valid_to?: string;
    signed_at?: string;
    algo?: string;
}
export interface ErrorResponse {
    error?: {
        code?: string;
        message?: string;
    };
}
export interface FileStorageSignedDocPdfRequest {
    /** @example [50,51,60] */
    signed_doc_ids?: number[];
}
export interface FileStorageSignedDocPdfResponse {
    result?: {
        file_id?: number;
        signed_doc_ids?: number[];
        pdf_file_id?: number;
    }[];
}
export interface TradeInfoWidgetCertificateInfo {
    owner?: string;
    valid_to?: string;
    serial_number?: string;
    expires?: boolean;
}
export interface TradeInfoWidgetResponse {
    qualified?: {
        state: 'has_valid' | 'has_expired' | 'not_found';
        certificates?: TradeInfoWidgetCertificateInfo[] | null;
    };
    simple?: {
        state: 'has_valid' | 'has_expired' | 'not_found';
        info?: {
            valid_to?: string;
            expires?: boolean;
        };
    };
}
export interface EdsJsFileStorageSignedDocListParams {
    /** Идентификатор подписанного документа */
    doc_id: number;
    /** Код проверки того, что пользователь может просматривать этот документ */
    code: string;
}
export interface EdsJsFileStorageSignedDocPdfCreateParams {
    /** Код проверки того, что пользователь может просматривать этот документ */
    code: string;
}
export interface EdsJsFileStorageArchiveCreateParams {
    /** Код проверки того, что пользователь может просматривать этот документ */
    code: string;
}
export declare namespace Eds {
    /**
     * No description
     * @tags Methods
     * @name EdsJsConfigList
     * @summary Получение отпечатков сертификатов
     * @request GET:/eds/eds_js/config/
     * @secure
     * @response `200` `(ConfigResponse | ErrorResponse)` Успешный ответ
     */
    namespace EdsJsConfigList {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = never;
        type RequestHeaders = {};
        type ResponseBody = ConfigResponse | ErrorResponse;
    }
    /**
     * No description
     * @tags Methods
     * @name EdsJsFileStorageHashCreate
     * @summary Получение списка хешей файлов
     * @request POST:/eds/eds_js/file_storage/hash/
     * @secure
     * @response `200` `(FileStorageHashResponse | ErrorResponse)` Успешный ответ
     */
    namespace EdsJsFileStorageHashCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = FileStorageHashRequest;
        type RequestHeaders = {};
        type ResponseBody = FileStorageHashResponse | ErrorResponse;
    }
    /**
     * @description Возвращает данные сертификатов подписантов подписанного документа, подписанный hash и идентификатор файла в FileStorage v2
     * @tags Methods
     * @name EdsJsFileStorageSignedDocList
     * @summary Получить ранее подписанный документ по его идентификатору
     * @request GET:/eds/eds_js/file_storage/signed_doc/
     * @secure
     * @response `200` `(FileStorageSignedDocViewResponse | ErrorResponse)` Документ найден в хранилище и доступен для просмотра по коду доступа
     * @response `404` `void` Документ не найден по переданному идентификатору
     */
    namespace EdsJsFileStorageSignedDocList {
        type RequestParams = {};
        type RequestQuery = {
            doc_id: number;
            code: string;
        };
        type RequestBody = never;
        type RequestHeaders = {};
        type ResponseBody = FileStorageSignedDocViewResponse | ErrorResponse;
    }
    /**
     * No description
     * @tags Methods
     * @name EdsJsFileStorageSignedDocCreate
     * @summary Создание документа и сохранение его в хранилище подписанных документов
     * @request POST:/eds/eds_js/file_storage/signed_doc/
     * @secure
     * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
     */
    namespace EdsJsFileStorageSignedDocCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = FileStorageSignedDocRequest;
        type RequestHeaders = {};
        type ResponseBody = FileStorageSignedDocResponse | ErrorResponse;
    }
    /**
     * No description
     * @tags Methods
     * @name EdsJsFileStorageSignedDocSimpleCreate
     * @summary Создание документа ПЭП и сохранение его в хранилище подписанных документов
     * @request POST:/eds/eds_js/file_storage/signed_doc_simple/
     * @secure
     * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
     */
    namespace EdsJsFileStorageSignedDocSimpleCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = FileStorageSimpleSignedDocRequest;
        type RequestHeaders = {};
        type ResponseBody = FileStorageSignedDocResponse | ErrorResponse;
    }
    /**
     * No description
     * @tags Methods
     * @name EdsJsFileStorageSignedDocMockCreate
     * @summary Создание документа через заглушку и сохранение его в хранилище подписанных документов
     * @request POST:/eds/eds_js/file_storage/signed_doc_mock/
     * @secure
     * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
     */
    namespace EdsJsFileStorageSignedDocMockCreate {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = FileStorageMockSignedDocRequest;
        type RequestHeaders = {};
        type ResponseBody = FileStorageSignedDocResponse | ErrorResponse;
    }
    /**
     * No description
     * @tags Methods
     * @name EdsJsFileStorageSignedDocPdfCreate
     * @summary Создает визуализацию подписанных документов по переданным идентификаторам.
     * @request POST:/eds/eds_js/file_storage/signed_doc_pdf/
     * @secure
     * @response `200` `(FileStorageSignedDocPdfResponse | ErrorResponse)` Pdf визуализации успешно созданы и помещены в FS. Либо объект ErrorResponse ошибки.
     */
    namespace EdsJsFileStorageSignedDocPdfCreate {
        type RequestParams = {};
        type RequestQuery = {
            code: string;
        };
        type RequestBody = FileStorageSignedDocPdfRequest;
        type RequestHeaders = {};
        type ResponseBody = FileStorageSignedDocPdfResponse | ErrorResponse;
    }
    /**
     * No description
     * @tags Methods
     * @name EdsJsFileStorageArchiveCreate
     * @summary Создание архива файлов и подписанных документов
     * @request POST:/eds/eds_js/file_storage/archive/
     * @secure
     * @response `200` `(FileStorageArchiveResponse | ErrorResponse)` Успешный ответ
     */
    namespace EdsJsFileStorageArchiveCreate {
        type RequestParams = {};
        type RequestQuery = {
            code: string;
        };
        type RequestBody = FileStorageArchiveRequest;
        type RequestHeaders = {};
        type ResponseBody = FileStorageArchiveResponse | ErrorResponse;
    }
    /**
     * @description Возвращает информацию о зарегистрированных сертификатах ЭП пользователя и ПЭП для виджета правого блока торговой процедуры.
     * @tags Methods
     * @name TradeInfoWidgetList
     * @summary Данные об ЭП пользователя для виджета торговой процедуры
     * @request GET:/eds/trade_info_widget/
     * @secure
     * @response `200` `TradeInfoWidgetResponse` Для КЭП возвращается состояние и, если есть действующие - их список. Для ПЭП состояние и если есть - ее срок окончания.
     */
    namespace TradeInfoWidgetList {
        type RequestParams = {};
        type RequestQuery = {};
        type RequestBody = never;
        type RequestHeaders = {};
        type ResponseBody = TradeInfoWidgetResponse;
    }
}
export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, 'body' | 'bodyUsed'>;
export interface FullRequestParams extends Omit<RequestInit, 'body'> {
    /** set parameter to `true` for call `securityWorker` for this request */
    secure?: boolean;
    /** request path */
    path: string;
    /** content type of request body */
    type?: ContentType;
    /** query params */
    query?: QueryParamsType;
    /** format of response (i.e. response.json() -> format: "json") */
    format?: ResponseFormat;
    /** request body */
    body?: unknown;
    /** base url */
    baseUrl?: string;
    /** request cancellation token */
    cancelToken?: CancelToken;
}
export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;
export interface ApiConfig<SecurityDataType = unknown> {
    baseUrl?: string;
    baseApiParams?: Omit<RequestParams, 'baseUrl' | 'cancelToken' | 'signal'>;
    securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
    customFetch?: typeof fetch;
}
export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
    data: D;
    error: E;
}
type CancelToken = Symbol | string | number;
export declare enum ContentType {
    Json = "application/json",
    FormData = "multipart/form-data",
    UrlEncoded = "application/x-www-form-urlencoded"
}
export declare class HttpClient<SecurityDataType = unknown> {
    baseUrl: string;
    private securityData;
    private securityWorker?;
    private abortControllers;
    private customFetch;
    private baseApiParams;
    constructor(apiConfig?: ApiConfig<SecurityDataType>);
    setSecurityData: (data: SecurityDataType | null) => void;
    private encodeQueryParam;
    private addQueryParam;
    private addArrayQueryParam;
    protected toQueryString(rawQuery?: QueryParamsType): string;
    protected addQueryParams(rawQuery?: QueryParamsType): string;
    private contentFormatters;
    private mergeRequestParams;
    private createAbortSignal;
    abortRequest: (cancelToken: CancelToken) => void;
    request: <T = any, E = any>({ body, secure, path, type, query, format, baseUrl, cancelToken, ...params }: FullRequestParams) => Promise<HttpResponse<T, E>>;
}
/**
 * @title EDS API
 * @version 1.0
 * @baseUrl https://b2b-center.ru.test/api/openid/v1
 *
 * @author API Team <api-dev-team@b2b-center.ru>
 */
export declare class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
    eds: {
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsConfigList
         * @summary Получение отпечатков сертификатов
         * @request GET:/eds/eds_js/config/
         * @secure
         * @response `200` `(ConfigResponse | ErrorResponse)` Успешный ответ
         */
        edsJsConfigList: (params?: RequestParams) => Promise<HttpResponse<ConfigResponse | ErrorResponse, any>>;
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsFileStorageHashCreate
         * @summary Получение списка хешей файлов
         * @request POST:/eds/eds_js/file_storage/hash/
         * @secure
         * @response `200` `(FileStorageHashResponse | ErrorResponse)` Успешный ответ
         */
        edsJsFileStorageHashCreate: (data: FileStorageHashRequest, params?: RequestParams) => Promise<HttpResponse<FileStorageHashResponse | ErrorResponse, any>>;
        /**
         * @description Возвращает данные сертификатов подписантов подписанного документа, подписанный hash и идентификатор файла в FileStorage v2
         *
         * @tags Methods
         * @name EdsJsFileStorageSignedDocList
         * @summary Получить ранее подписанный документ по его идентификатору
         * @request GET:/eds/eds_js/file_storage/signed_doc/
         * @secure
         * @response `200` `(FileStorageSignedDocViewResponse | ErrorResponse)` Документ найден в хранилище и доступен для просмотра по коду доступа
         * @response `404` `void` Документ не найден по переданному идентификатору
         */
        edsJsFileStorageSignedDocList: (query: EdsJsFileStorageSignedDocListParams, params?: RequestParams) => Promise<HttpResponse<FileStorageSignedDocViewResponse | ErrorResponse, void>>;
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsFileStorageSignedDocCreate
         * @summary Создание документа и сохранение его в хранилище подписанных документов
         * @request POST:/eds/eds_js/file_storage/signed_doc/
         * @secure
         * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
         */
        edsJsFileStorageSignedDocCreate: (data: FileStorageSignedDocRequest, params?: RequestParams) => Promise<HttpResponse<FileStorageSignedDocResponse | ErrorResponse, any>>;
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsFileStorageSignedDocSimpleCreate
         * @summary Создание документа ПЭП и сохранение его в хранилище подписанных документов
         * @request POST:/eds/eds_js/file_storage/signed_doc_simple/
         * @secure
         * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
         */
        edsJsFileStorageSignedDocSimpleCreate: (data: FileStorageSimpleSignedDocRequest, params?: RequestParams) => Promise<HttpResponse<FileStorageSignedDocResponse | ErrorResponse, any>>;
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsFileStorageSignedDocMockCreate
         * @summary Создание документа через заглушку и сохранение его в хранилище подписанных документов
         * @request POST:/eds/eds_js/file_storage/signed_doc_mock/
         * @secure
         * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
         */
        edsJsFileStorageSignedDocMockCreate: (data: FileStorageMockSignedDocRequest, params?: RequestParams) => Promise<HttpResponse<FileStorageSignedDocResponse | ErrorResponse, any>>;
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsFileStorageSignedDocPdfCreate
         * @summary Создает визуализацию подписанных документов по переданным идентификаторам.
         * @request POST:/eds/eds_js/file_storage/signed_doc_pdf/
         * @secure
         * @response `200` `(FileStorageSignedDocPdfResponse | ErrorResponse)` Pdf визуализации успешно созданы и помещены в FS. Либо объект ErrorResponse ошибки.
         */
        edsJsFileStorageSignedDocPdfCreate: (query: EdsJsFileStorageSignedDocPdfCreateParams, data: FileStorageSignedDocPdfRequest, params?: RequestParams) => Promise<HttpResponse<ErrorResponse | FileStorageSignedDocPdfResponse, any>>;
        /**
         * No description
         *
         * @tags Methods
         * @name EdsJsFileStorageArchiveCreate
         * @summary Создание архива файлов и подписанных документов
         * @request POST:/eds/eds_js/file_storage/archive/
         * @secure
         * @response `200` `(FileStorageArchiveResponse | ErrorResponse)` Успешный ответ
         */
        edsJsFileStorageArchiveCreate: (query: EdsJsFileStorageArchiveCreateParams, data: FileStorageArchiveRequest, params?: RequestParams) => Promise<HttpResponse<FileStorageArchiveResponse | ErrorResponse, any>>;
        /**
         * @description Возвращает информацию о зарегистрированных сертификатах ЭП пользователя и ПЭП для виджета правого блока торговой процедуры.
         *
         * @tags Methods
         * @name TradeInfoWidgetList
         * @summary Данные об ЭП пользователя для виджета торговой процедуры
         * @request GET:/eds/trade_info_widget/
         * @secure
         * @response `200` `TradeInfoWidgetResponse` Для КЭП возвращается состояние и, если есть действующие - их список. Для ПЭП состояние и если есть - ее срок окончания.
         */
        tradeInfoWidgetList: (params?: RequestParams) => Promise<HttpResponse<TradeInfoWidgetResponse, any>>;
    };
}
export {};
