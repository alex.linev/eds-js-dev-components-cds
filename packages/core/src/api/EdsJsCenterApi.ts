/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export type SimplePin = string;

export interface ConfigResponse {
  result?: {
    certificates: string[];
    simple?: { owner?: string; org_name?: string };
    mock?: {
      signature?: string;
      qualified?: {
        user_name?: string;
        owner?: string;
        org_name?: string;
        issuer?: string;
        serial_number?: string;
        thumbprint?: string;
        valid_from?: string;
        valid_to?: string;
        algorithm?: string;
        certificate_body?: string;
      }[];
    };
  };
}

export interface FileStorageHashRequest {
  file_ids: number[];
}

export interface FileStorageHashResponse {
  result?: { file_id?: number; hash?: string }[];
}

export interface FileStorageSignedDocRequest {
  signatures?: { file_id?: number; value?: string }[];
}

export interface FileStorageSimpleSignedDocRequest {
  /** @example [345678,345679] */
  file_ids: number[];
  pin: SimplePin;
}

export interface FileStorageMockSignedDocRequest {
  /** @example [345678,345679] */
  file_ids?: number[];
}

export interface FileStorageSignedDocResponse {
  result?: { file_id?: number; sign_doc_id?: number }[];
}

export interface FileStorageArchiveRequest {
  /** @example [345678,345679] */
  file_ids: number[];

  /** @example [9876,9877] */
  signed_doc_ids: number[];
}

export interface FileStorageArchiveResponse {
  result?: { file_id?: number };
}

export interface FileStorageSignedDocViewResponse {
  result?: { file_id?: number; sign_doc_id?: number; signature?: string; signers?: SignerResponse[] };
}

export interface SignerResponse {
  serial_number?: string;
  thumbprint?: string;
  owner?: string;
  issuer?: string;
  valid_from?: string;
  valid_to?: string;
  signed_at?: string;
  algo?: string;
}

export interface ErrorResponse {
  error?: { code?: string; message?: string };
}

export interface FileStorageSignedDocPdfRequest {
  /** @example [50,51,60] */
  signed_doc_ids?: number[];
}

export interface FileStorageSignedDocPdfResponse {
  result?: { file_id?: number; signed_doc_ids?: number[]; pdf_file_id?: number }[];
}

export interface TradeInfoWidgetCertificateInfo {
  owner?: string;
  valid_to?: string;
  serial_number?: string;
  expires?: boolean;
}

export interface TradeInfoWidgetResponse {
  qualified?: {
    state: 'has_valid' | 'has_expired' | 'not_found';
    certificates?: TradeInfoWidgetCertificateInfo[] | null;
  };
  simple?: { state: 'has_valid' | 'has_expired' | 'not_found'; info?: { valid_to?: string; expires?: boolean } };
}

export interface EdsJsFileStorageSignedDocListParams {
  /** Идентификатор подписанного документа */
  doc_id: number;

  /** Код проверки того, что пользователь может просматривать этот документ */
  code: string;
}

export interface EdsJsFileStorageSignedDocPdfCreateParams {
  /** Код проверки того, что пользователь может просматривать этот документ */
  code: string;
}

export interface EdsJsFileStorageArchiveCreateParams {
  /** Код проверки того, что пользователь может просматривать этот документ */
  code: string;
}

export namespace Eds {
  /**
   * No description
   * @tags Methods
   * @name EdsJsConfigList
   * @summary Получение отпечатков сертификатов
   * @request GET:/eds/eds_js/config/
   * @secure
   * @response `200` `(ConfigResponse | ErrorResponse)` Успешный ответ
   */
  export namespace EdsJsConfigList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = ConfigResponse | ErrorResponse;
  }
  /**
   * No description
   * @tags Methods
   * @name EdsJsFileStorageHashCreate
   * @summary Получение списка хешей файлов
   * @request POST:/eds/eds_js/file_storage/hash/
   * @secure
   * @response `200` `(FileStorageHashResponse | ErrorResponse)` Успешный ответ
   */
  export namespace EdsJsFileStorageHashCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = FileStorageHashRequest;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageHashResponse | ErrorResponse;
  }
  /**
   * @description Возвращает данные сертификатов подписантов подписанного документа, подписанный hash и идентификатор файла в FileStorage v2
   * @tags Methods
   * @name EdsJsFileStorageSignedDocList
   * @summary Получить ранее подписанный документ по его идентификатору
   * @request GET:/eds/eds_js/file_storage/signed_doc/
   * @secure
   * @response `200` `(FileStorageSignedDocViewResponse | ErrorResponse)` Документ найден в хранилище и доступен для просмотра по коду доступа
   * @response `404` `void` Документ не найден по переданному идентификатору
   */
  export namespace EdsJsFileStorageSignedDocList {
    export type RequestParams = {};
    export type RequestQuery = { doc_id: number; code: string };
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageSignedDocViewResponse | ErrorResponse;
  }
  /**
   * No description
   * @tags Methods
   * @name EdsJsFileStorageSignedDocCreate
   * @summary Создание документа и сохранение его в хранилище подписанных документов
   * @request POST:/eds/eds_js/file_storage/signed_doc/
   * @secure
   * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
   */
  export namespace EdsJsFileStorageSignedDocCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = FileStorageSignedDocRequest;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageSignedDocResponse | ErrorResponse;
  }
  /**
   * No description
   * @tags Methods
   * @name EdsJsFileStorageSignedDocSimpleCreate
   * @summary Создание документа ПЭП и сохранение его в хранилище подписанных документов
   * @request POST:/eds/eds_js/file_storage/signed_doc_simple/
   * @secure
   * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
   */
  export namespace EdsJsFileStorageSignedDocSimpleCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = FileStorageSimpleSignedDocRequest;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageSignedDocResponse | ErrorResponse;
  }
  /**
   * No description
   * @tags Methods
   * @name EdsJsFileStorageSignedDocMockCreate
   * @summary Создание документа через заглушку и сохранение его в хранилище подписанных документов
   * @request POST:/eds/eds_js/file_storage/signed_doc_mock/
   * @secure
   * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
   */
  export namespace EdsJsFileStorageSignedDocMockCreate {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = FileStorageMockSignedDocRequest;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageSignedDocResponse | ErrorResponse;
  }
  /**
   * No description
   * @tags Methods
   * @name EdsJsFileStorageSignedDocPdfCreate
   * @summary Создает визуализацию подписанных документов по переданным идентификаторам.
   * @request POST:/eds/eds_js/file_storage/signed_doc_pdf/
   * @secure
   * @response `200` `(FileStorageSignedDocPdfResponse | ErrorResponse)` Pdf визуализации успешно созданы и помещены в FS. Либо объект ErrorResponse ошибки.
   */
  export namespace EdsJsFileStorageSignedDocPdfCreate {
    export type RequestParams = {};
    export type RequestQuery = { code: string };
    export type RequestBody = FileStorageSignedDocPdfRequest;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageSignedDocPdfResponse | ErrorResponse;
  }
  /**
   * No description
   * @tags Methods
   * @name EdsJsFileStorageArchiveCreate
   * @summary Создание архива файлов и подписанных документов
   * @request POST:/eds/eds_js/file_storage/archive/
   * @secure
   * @response `200` `(FileStorageArchiveResponse | ErrorResponse)` Успешный ответ
   */
  export namespace EdsJsFileStorageArchiveCreate {
    export type RequestParams = {};
    export type RequestQuery = { code: string };
    export type RequestBody = FileStorageArchiveRequest;
    export type RequestHeaders = {};
    export type ResponseBody = FileStorageArchiveResponse | ErrorResponse;
  }
  /**
   * @description Возвращает информацию о зарегистрированных сертификатах ЭП пользователя и ПЭП для виджета правого блока торговой процедуры.
   * @tags Methods
   * @name TradeInfoWidgetList
   * @summary Данные об ЭП пользователя для виджета торговой процедуры
   * @request GET:/eds/trade_info_widget/
   * @secure
   * @response `200` `TradeInfoWidgetResponse` Для КЭП возвращается состояние и, если есть действующие - их список. Для ПЭП состояние и если есть - ее срок окончания.
   */
  export namespace TradeInfoWidgetList {
    export type RequestParams = {};
    export type RequestQuery = {};
    export type RequestBody = never;
    export type RequestHeaders = {};
    export type ResponseBody = TradeInfoWidgetResponse;
  }
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, 'body' | 'bodyUsed'>;

export interface FullRequestParams extends Omit<RequestInit, 'body'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, 'baseUrl' | 'cancelToken' | 'signal'>;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded'
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = 'https://b2b-center.ru.test/api/openid/v1';
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: 'same-origin',
    headers: {},
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === 'number' ? value : `${value}`)}`;
  }

  private addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  private addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join('&');
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => 'undefined' !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join('&');
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : '';
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === 'object' || typeof input === 'string') ? JSON.stringify(input) : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === 'object' && property !== null
            ? JSON.stringify(property)
            : `${property}`
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input)
  };

  private mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {})
      }
    };
  }

  private createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ''}${path}${queryString ? `?${queryString}` : ''}`, {
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {}),
        ...(requestParams.headers || {})
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : void 0,
      body: typeof body === 'undefined' || body === null ? null : payloadFormatter(body)
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title EDS API
 * @version 1.0
 * @baseUrl https://b2b-center.ru.test/api/openid/v1
 *
 * @author API Team <api-dev-team@b2b-center.ru>
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  eds = {
    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsConfigList
     * @summary Получение отпечатков сертификатов
     * @request GET:/eds/eds_js/config/
     * @secure
     * @response `200` `(ConfigResponse | ErrorResponse)` Успешный ответ
     */
    edsJsConfigList: (params: RequestParams = {}) =>
      this.request<ConfigResponse | ErrorResponse, any>({
        path: `/eds/eds_js/config/`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsFileStorageHashCreate
     * @summary Получение списка хешей файлов
     * @request POST:/eds/eds_js/file_storage/hash/
     * @secure
     * @response `200` `(FileStorageHashResponse | ErrorResponse)` Успешный ответ
     */
    edsJsFileStorageHashCreate: (data: FileStorageHashRequest, params: RequestParams = {}) =>
      this.request<FileStorageHashResponse | ErrorResponse, any>({
        path: `/eds/eds_js/file_storage/hash/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * @description Возвращает данные сертификатов подписантов подписанного документа, подписанный hash и идентификатор файла в FileStorage v2
     *
     * @tags Methods
     * @name EdsJsFileStorageSignedDocList
     * @summary Получить ранее подписанный документ по его идентификатору
     * @request GET:/eds/eds_js/file_storage/signed_doc/
     * @secure
     * @response `200` `(FileStorageSignedDocViewResponse | ErrorResponse)` Документ найден в хранилище и доступен для просмотра по коду доступа
     * @response `404` `void` Документ не найден по переданному идентификатору
     */
    edsJsFileStorageSignedDocList: (query: EdsJsFileStorageSignedDocListParams, params: RequestParams = {}) =>
      this.request<FileStorageSignedDocViewResponse | ErrorResponse, void>({
        path: `/eds/eds_js/file_storage/signed_doc/`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsFileStorageSignedDocCreate
     * @summary Создание документа и сохранение его в хранилище подписанных документов
     * @request POST:/eds/eds_js/file_storage/signed_doc/
     * @secure
     * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
     */
    edsJsFileStorageSignedDocCreate: (data: FileStorageSignedDocRequest, params: RequestParams = {}) =>
      this.request<FileStorageSignedDocResponse | ErrorResponse, any>({
        path: `/eds/eds_js/file_storage/signed_doc/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsFileStorageSignedDocSimpleCreate
     * @summary Создание документа ПЭП и сохранение его в хранилище подписанных документов
     * @request POST:/eds/eds_js/file_storage/signed_doc_simple/
     * @secure
     * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
     */
    edsJsFileStorageSignedDocSimpleCreate: (data: FileStorageSimpleSignedDocRequest, params: RequestParams = {}) =>
      this.request<FileStorageSignedDocResponse | ErrorResponse, any>({
        path: `/eds/eds_js/file_storage/signed_doc_simple/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsFileStorageSignedDocMockCreate
     * @summary Создание документа через заглушку и сохранение его в хранилище подписанных документов
     * @request POST:/eds/eds_js/file_storage/signed_doc_mock/
     * @secure
     * @response `200` `(FileStorageSignedDocResponse | ErrorResponse)` Успешный ответ
     */
    edsJsFileStorageSignedDocMockCreate: (data: FileStorageMockSignedDocRequest, params: RequestParams = {}) =>
      this.request<FileStorageSignedDocResponse | ErrorResponse, any>({
        path: `/eds/eds_js/file_storage/signed_doc_mock/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsFileStorageSignedDocPdfCreate
     * @summary Создает визуализацию подписанных документов по переданным идентификаторам.
     * @request POST:/eds/eds_js/file_storage/signed_doc_pdf/
     * @secure
     * @response `200` `(FileStorageSignedDocPdfResponse | ErrorResponse)` Pdf визуализации успешно созданы и помещены в FS. Либо объект ErrorResponse ошибки.
     */
    edsJsFileStorageSignedDocPdfCreate: (
      query: EdsJsFileStorageSignedDocPdfCreateParams,
      data: FileStorageSignedDocPdfRequest,
      params: RequestParams = {}
    ) =>
      this.request<FileStorageSignedDocPdfResponse | ErrorResponse, any>({
        path: `/eds/eds_js/file_storage/signed_doc_pdf/`,
        method: 'POST',
        query: query,
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * No description
     *
     * @tags Methods
     * @name EdsJsFileStorageArchiveCreate
     * @summary Создание архива файлов и подписанных документов
     * @request POST:/eds/eds_js/file_storage/archive/
     * @secure
     * @response `200` `(FileStorageArchiveResponse | ErrorResponse)` Успешный ответ
     */
    edsJsFileStorageArchiveCreate: (
      query: EdsJsFileStorageArchiveCreateParams,
      data: FileStorageArchiveRequest,
      params: RequestParams = {}
    ) =>
      this.request<FileStorageArchiveResponse | ErrorResponse, any>({
        path: `/eds/eds_js/file_storage/archive/`,
        method: 'POST',
        query: query,
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params
      }),

    /**
     * @description Возвращает информацию о зарегистрированных сертификатах ЭП пользователя и ПЭП для виджета правого блока торговой процедуры.
     *
     * @tags Methods
     * @name TradeInfoWidgetList
     * @summary Данные об ЭП пользователя для виджета торговой процедуры
     * @request GET:/eds/trade_info_widget/
     * @secure
     * @response `200` `TradeInfoWidgetResponse` Для КЭП возвращается состояние и, если есть действующие - их список. Для ПЭП состояние и если есть - ее срок окончания.
     */
    tradeInfoWidgetList: (params: RequestParams = {}) =>
      this.request<TradeInfoWidgetResponse, any>({
        path: `/eds/trade_info_widget/`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params
      })
  };
}
