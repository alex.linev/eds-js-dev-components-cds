import {ECreateSignedDocStatus, SignedDocStore, TCreateSignedDocState} from '@/apps/signed-doc.store';
import BackendRestJsApiProvider from '@/providers/BackendRestJsApiProvider';
import {TSignature, TSignedDocument} from '@/types';
import {Observable} from 'rxjs';
import {B2BCenterByUuidCreateDocProvider} from '@/providers/B2BCenterByUuidCreateDocProvider';

export interface ICreateSignedDocView {
    onCreateSignedDocError: (error: Error) => void;
    onCreateSignedDocPending: (pending_message?: string) => void;
    onCreateSignedDocDone: (document: TSignedDocument) => void;
}

export class CreateSignedDocPresenter {
    private readonly store: SignedDocStore;
    private _view?: ICreateSignedDocView;

    constructor() {
        // fixme: DI Provider
        // const provider = new BackendRestJsApiProvider()
        // fixme: ну вот тут уже должна авторизация происходить по куке, зачем проталкивать токен туда сюда то???
        const provider = new B2BCenterByUuidCreateDocProvider(
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIzMjEzZDdkNi05ODMxLTRmZjEtODExMC01NzIyZDZkYjg0OTQiLCJhdWQiOiJjYTcyMzFiZS0yNGY1LTQ0OWItYjkwYS1lNDk1NDJiYTQxN2IiLCJtc3ViIjpudWxsLCJzaWQiOiJlMjQyZTk1Zi05NDViLTQyMGMtODc0My1kMGQxMzRmZjc1YTIiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxOTMzMzUzNCwiZXhwIjoxNzIwMTk3NTM0fQ.AHkYSy8byFqAPVS4GU_FClE2FFYsw-47TwaaAF7qsT8pNPY8JPcznnr6nPk1l6hXGhEIlZp4uxNhpX39Gi92vq9EAR4QgrCG585zvzGp8Pma1n8f4GYeWNr5wB3AoHlPmEbfR4_hkkOw9UXxrVocmPYlIeTtdoKrrwCcs02QHQpf5zoO'
        )
        this.store = new SignedDocStore(provider);
    }

    set view(v: ICreateSignedDocView) {
        this._view = v;
    }

    get state(): Observable<TCreateSignedDocState> {
        return this.store.state$;
    }

    create(sig: TSignature, unsigned_text: string): void {
        if (this._view) {
            const subs = this.state.subscribe({
                next: (docState: TCreateSignedDocState) => {
                    const {status} = docState;
                    switch (status) {
                        case ECreateSignedDocStatus.PENDING:
                            this._view?.onCreateSignedDocPending(docState.pending_message);
                            break;
                        case ECreateSignedDocStatus.DONE:
                            this._view?.onCreateSignedDocDone({doc_id: docState.result.id});
                            break;
                        case ECreateSignedDocStatus.ERROR:
                            this._view?.onCreateSignedDocError(docState.error);
                            break;
                    }
                },
                error: (error: Error) => {
                    this._view?.onCreateSignedDocError(error);
                    subs.unsubscribe();
                },
                complete: () => {
                    subs.unsubscribe();
                }
            })
        }
        console.log('create DOCUMENT');
        this.store.createSignedDoc(sig, unsigned_text);
    }
}