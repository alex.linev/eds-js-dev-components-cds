import {BehaviorSubject, catchError, mergeMap, Observable, Subscription, tap} from 'rxjs';
import {
    ESignSoftware,
    ESignStrategy,
    IBackendApiCreateDocServiceProvider,
    IBackendApiServiceProvider,
    TSignature,
    TSignContext, TSignedDocument
} from '@/types';
import {fromPromise} from 'rxjs/internal/observable/innerFrom';

export enum ECreateSignedDocStatus {
    IDLE = 'IDLE',
    PENDING = 'PENDING',
    ERROR = 'ERROR',
    DONE = 'DONE',
}

export type TCreateSignedDocResult = {
    id: number;
}

export type TCreateSignedDocState = {
    status: ECreateSignedDocStatus.IDLE;
} | {
    status: ECreateSignedDocStatus.PENDING;
    pending_message?: string;
} | {
    status: ECreateSignedDocStatus.ERROR;
    error: Error;
} | {
    status: ECreateSignedDocStatus.DONE;
    result: TCreateSignedDocResult;
}

export class SignedDocStore {
    public readonly state: BehaviorSubject<TCreateSignedDocState>; //Observable<TCreateSignedDocState>;
    private readonly apiProvider: IBackendApiCreateDocServiceProvider;

    constructor(apiProvider: IBackendApiCreateDocServiceProvider) {
        this.apiProvider = apiProvider;
        this.state = new BehaviorSubject<TCreateSignedDocState>({status: ECreateSignedDocStatus.IDLE});
    }

    get state$(): Observable<TCreateSignedDocState> {
        return this.state.asObservable();
    }

    createSignedDoc(signature: TSignature, unsigned_text: string): void {
        const ctx: TSignContext = {
            strategy: {
                type: ESignStrategy.Qualified,
                software: ESignSoftware.CPro,
                certificate_identifier: 'blah blah' // fixme: WTF??
            },
            content: {
                type: 'text',
                unsigned_message: unsigned_text
            },
            signature: signature
        }
        this.state.next({status: ECreateSignedDocStatus.PENDING, pending_message: 'Отправка данных на бек'});
        const sub: Subscription = fromPromise(this.apiProvider.createSignedDocument(ctx))
            .pipe(tap(console.log)).subscribe({
                next: (result: TSignedDocument) => this.state.next({status: ECreateSignedDocStatus.DONE, result: {id: result.doc_id}}),
                error: error => this.state.next({status: ECreateSignedDocStatus.ERROR, error}),
                complete: () => sub.unsubscribe()
            });
    }
}