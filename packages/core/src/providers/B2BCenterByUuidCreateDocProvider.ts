import {IBackendApiCreateDocServiceProvider, TSignContext, TSignedDocument} from '@/types';

export class B2BCenterByUuidCreateDocProvider implements IBackendApiCreateDocServiceProvider {

    constructor(
        private readonly access_token: string,
        private readonly url: string = document?.location?.href ?? 'https://b2b-center.ru.test/personal/iframe-sign-external/',
    ) {
    }

    async createSignedDocument(context: TSignContext): Promise<TSignedDocument> {
        try {
            // fixme: uuid fiction
            const response = await fetch(this.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + this.access_token // FIXME тут надо попробовать с cookie
                },
                body: JSON.stringify(context)
            });
            const data = await response.json();
            console.log('B2BCenterByUuidCreateDocProvider create success.', data);
            return {doc_id: data.doc_id ?? 123};
        } catch (error) {
            console.error(error);
            throw error;
        }

    }

}