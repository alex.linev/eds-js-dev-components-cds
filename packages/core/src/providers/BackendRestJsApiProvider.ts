import {IBackendApiServiceProvider, TError, TSignConfiguration, TSignContext, TSignedDocument} from '@/types';
import {
    Api as RestJsOpenApiModel,
    EdsCreateSignedDocIntentCreatePayload,
    SignConfigRequest,
    SignContentEncoding,
    SignContentType,
    SignedTextDocumentCreateRequest,
    WidgetSignStrategyType
} from '@/api/Api';
// } from '@/api/BackendRestJsApi';

export default class BackendRestJsApiProvider implements IBackendApiServiceProvider {

    private readonly restOpenApiModel: RestJsOpenApiModel<any>;

    constructor(
        accessToken?: string
    ) {
        const token =
            accessToken ??
            // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiXSwianRpIjoiYjJjM2RiMmEtZTlkYS00Yzg0LWIzNGEtODc5YzZjYWJkMWY2IiwiYXVkIjoiZGI3MGMwNDgtNzQzNS00OTE3LWI0ZDEtMTFhNzg0NTE0ZGQ3IiwibXN1YiI6bnVsbCwic2lkIjoiMTlhMzg4NTItOGQ4Mi00ZTJiLThjODItMDA2N2E1YzE3ZDQ0Iiwic3ViIjoiNjAxMzYiLCJpYXQiOjE3MDk1NjM0NjUsImV4cCI6MTcwOTU5OTQ2NX0.AdfdTCEWVWXyTzu680uPZIXVLTkyOOLWINeasWc_Z8iWA81x-VOg_YAK4uB5h5711DkyP3EIG1Tn8DilVKku1zNqAQuH5lBt3Qh26ExB-LXBFg1yzMTHaja19-DIFXRu5A_cyT1Jy1aCdC9Po_BNefexPSpFSrruwAND4MNwjgq74-jR';
            // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiI3OWNmZTA5MS1jNDUyLTRmZWItYmM4NS0wZGI0MDZmMGNiM2QiLCJhdWQiOiJkYjcwYzA0OC03NDM1LTQ5MTctYjRkMS0xMWE3ODQ1MTRkZDciLCJtc3ViIjpudWxsLCJzaWQiOiJmNzk2ZmMyNi05NWVmLTRjNTgtYTBkZS01NmEzM2YxODAzMGEiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxNDY2MTgzMSwiZXhwIjoxNzE0Njk3ODMxfQ.AIFd2-t6QA7Rh64KA2kJaF83WIZNRxM4h5iCq51z9eQIKuwZNn81lxzgTAH5JBzxlvtkyzbrK1LzD0fNPKPmCxWrAXBBrymZjs29eTcxgtJdmNCMF3gEpEYttEmAUKXS-mjAHVLkFO8x0QfE3hZF0F4TMt3942cfq9VgL1ZZMOla3wGY';
            // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIyNzBkMzZkNC0zNWQ1LTQ1NTQtOTFlZi0xZDYzYjQ1ZmFhYmYiLCJhdWQiOiIyM2ZlNGJhOS0xOGRkLTQ0MDUtOWQxNC00MTgxY2Y1YTdlOTgiLCJtc3ViIjpudWxsLCJzaWQiOiI2NTdmNzNkZS04YmMxLTRmNTAtYjEyOC0xYjFjMmQyNmZmMDEiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxNzUxMjcwNSwiZXhwIjoxNzE3OTQ0NzA1fQ.AEYY-0zbBkbAbHEASPEMIVNg1IYmSKXCiILNnt2COwvSgJsKSIJiKpazZcP_jsCOOkc048nCk92-N7cL62reqZfkAWN4fxzgEn8WbshK0y2EHxxxKe-CBLS8nuFV15dX4cU_xMs3Aif_843o2hFpU8hD-Cs_TqkoaBI4u-xGILsZ9Rid'
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIzMjEzZDdkNi05ODMxLTRmZjEtODExMC01NzIyZDZkYjg0OTQiLCJhdWQiOiJjYTcyMzFiZS0yNGY1LTQ0OWItYjkwYS1lNDk1NDJiYTQxN2IiLCJtc3ViIjpudWxsLCJzaWQiOiJlMjQyZTk1Zi05NDViLTQyMGMtODc0My1kMGQxMzRmZjc1YTIiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxOTMzMzUzNCwiZXhwIjoxNzIwMTk3NTM0fQ.AHkYSy8byFqAPVS4GU_FClE2FFYsw-47TwaaAF7qsT8pNPY8JPcznnr6nPk1l6hXGhEIlZp4uxNhpX39Gi92vq9EAR4QgrCG585zvzGp8Pma1n8f4GYeWNr5wB3AoHlPmEbfR4_hkkOw9UXxrVocmPYlIeTtdoKrrwCcs02QHQpf5zoO';
        const resultConfig = {
            baseUrl: 'https://iframe-mvp.b2b.test'
        } as {
            baseUrl: string;
            baseApiParams: any
        };
        const headers = {Authorization: `Bearer ${token}`};
        resultConfig.baseApiParams = {headers};
        // this.restOpenApiModel = new RestJsOpenApiModel(resultConfig);
        this.restOpenApiModel = new RestJsOpenApiModel(resultConfig);
    }

    async provideSignConfiguration(ctx: TSignContext): Promise<TSignConfiguration> {
        const req: SignConfigRequest = {
            sign_content_type: SignContentType.SignContextText,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const {data, ok, error} = await this.restOpenApiModel.api.edsSignConfigTextCreate(req);
        if (!ok) {
            throw error ?? new Error('При запросе конфига упало с ошибкой');
        }
        const {payload} = data; // FIXME: решить, надо ли тут так или нефиг?
        const {qualified} = payload;
        let qual = undefined;
        if (qualified) {
            const {identifier_type, valid_certificate_identifiers} = qualified;
            qual = {identifier_type, valid_certificate_identifiers};
        }
        const cfg: TSignConfiguration = {
            context: ctx
        };
        if (qual) {
            cfg.qualified = qual;
        }
        return cfg;
    }

    // fixme: переделать на нормальный возврат результата без контекстов
    async createSignedDocumentOld(context: TSignContext): Promise<void> {
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const req: SignedTextDocumentCreateRequest = {
            sign_content_type: SignContentType.SignContextText,
            content: (context.content.unsigned_message) as string,
            signature: context.signature as string,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const {
            data,
            ok,
            error
        } = await this.restOpenApiModel.api.edsSignedDocumentTextCreate(req);
        if (!ok) {
            const err = new Error(error.title || 'Ошибка при попытке сохранить подписанный текстовый документ в хран');
            context.error = err as TError;
            throw err;
        }
        if (!data.doc_id) {
            // FIXME
            throw new Error('ata.doc_id');
        }
        // FIXME: Херня какая то
        const doc: TSignedDocument = {
            doc_id: data.doc_id
        };
        context.doc = doc;
    }

    async createSignedDocument(context: TSignContext): Promise<TSignedDocument> {
        // throw new Error('OLOLOLO');
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const req: SignedTextDocumentCreateRequest = {
            sign_content_type: SignContentType.SignContextText,
            content: (context.content.unsigned_message) as string,
            signature: context.signature as string,
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const {
            data,
            ok,
            error
        } = await this.restOpenApiModel.api.edsSignedDocumentTextCreate(req);
        if (!ok) {
            const err = new Error(error.title || 'Ошибка при попытке сохранить подписанный текстовый документ в хран');
            context.error = err as TError;
            throw err;
        }
        if (!data.doc_id) {
            // FIXME
            throw new Error('ata.doc_id');
        }
        // FIXME: Херня какая то
        const doc: TSignedDocument = {
            doc_id: data.doc_id
        };
        return doc;
    }

    async createSignIframeRequest(context: TSignContext): Promise<{
        uuid: string
    }> {
        if (context.content.type !== 'text') {
            throw new Error('No text');
        }
        const requestData: EdsCreateSignedDocIntentCreatePayload = {
            widget_strategy: {
                type: WidgetSignStrategyType.Iframe
            },
            sign_context: {
                type: SignContentType.SignContextText,
                unsigned_message: context.content.unsigned_message,
                encoding: SignContentEncoding.None,
                is_detached: false
            },
            user_info: {
                type: 'bearer_in_header'
            }
        };
        const {
            data,
            ok,
            error
        } = await this.restOpenApiModel.api.edsCreateSignedDocIntentCreate(requestData);
        if (!ok) {
            throw new Error(error.title)
        }
        return {uuid: data.uuid};
    }

    async fetchSignedDoc(id: number): Promise<TSignedDocument> {
        const {
            data, ok, error
        } = await this.restOpenApiModel.api.edsSignedDocDetail(id);
        if (!ok) {
            throw new Error(error.title);
        }
        console.log('Loaded doc data: ', data);
        return data;
    }
}