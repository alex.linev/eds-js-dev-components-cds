import {createApp} from 'vue'
import App from './App.vue'
import {createRouter, createWebHistory} from 'vue-router';
import {routes} from '@/routes';
// import {
//     createDisplay,
//     CdsGrid, CdsRow, CdsCol, CdsLayout, CdsHeader, CdsMain,
//     CdsToolbar, CdsToolbarTitle
// } from '@central-design-system/components';
import { createCds, components, directives } from '@central-design-system/components';
import '@central-design-system/components/dist/cds.css';

import DefaultLayout from '@/layouts/DefaultLayout.vue'
import BlankLayout from '@/layouts/BlankLayout.vue'

declare global {
    interface Window {
        BASE_URL: string;
    }
}

const cds = createCds({
    components,
    directives,
    // ...options
});

// const routes = [
//     {path: '/', component: Index},
//     {path: '/modal-iframe', component: ModalIframe},
//     {path: '/modal-import', component: ModalImport},
//     {path: '/widget', component: Widget},
// ];
// const BASE_URL = process.env.BASE_URL || '/';
window.BASE_URL = import.meta.env.BASE_URL || '/'
const router = createRouter({
    history: createWebHistory('/'),
    routes
});

const app = createApp(App);
app.use(router);
app.use(cds);

app.component('DefaultLayout', DefaultLayout);
app.component('BlankLayout', BlankLayout);

app.mount('#app')
