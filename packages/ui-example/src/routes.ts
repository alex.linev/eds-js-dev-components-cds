import {RouteRecordRaw} from 'vue-router'
import Widget from '@/pages/Widget.vue';
import ModalIframe from '@/pages/ModalIframe.vue';
import ModalImport from '@/pages/ModalImport.vue';
import Index from '@/pages/Index.vue';
import DocumentView from '@/pages/DocumentView.vue';

export const routes: Array<RouteRecordRaw> = [
    {
        // Стартовая страница
        path: '/',
        name: 'HomeView',
        component: Index,
        meta: {
            layout: 'default'
        }
    }, {
        // Модальное окно c iframe виджетом
        path: '/modal-iframe',
        name: 'ModalIframeView',
        component: ModalIframe,
        meta: {
            layout: 'default'
        }
    }, {
        // Модальное окно c виджетом как компонент
        path: '/modal-import',
        name: 'ModalImportView',
        component: ModalImport,
        meta: {
            layout: 'default'
        }
    }, {
        // Чистая страница для вставки в iframe с содержимым виджета
        path: '/widget',
        name: 'WidgetView',
        component: Widget,
        meta: {
            layout: 'blank'
        }
    }, {
        // Страница просмотра подписанного документа по его id (старый api)
        path: '/document/:id',
        name: 'DocumentView',
        component: DocumentView,
    },
]