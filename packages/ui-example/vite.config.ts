import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import envCompatible from 'vite-plugin-env-compatible';
import alias from '@rollup/plugin-alias';

export default defineConfig({
    plugins: [
        vue(),
        envCompatible,
        alias({
            entries: [
                { find: '@', replacement: '/src' }
            ]
        })
    ]
});
