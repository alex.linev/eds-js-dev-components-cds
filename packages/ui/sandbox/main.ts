import {runApp} from '../dist/edsjs-test-pkg.es.js';
import '../dist/style.css';
import {EdsIframeOptions, getWidget, TSize2d} from 'eds-test-pkg-iframe-client';
// window.addEventListener('--- MESSAGE ---', function(event) {
//     console.log('Received message:', event);
// });
runApp(`#app`);

const iOpts: EdsIframeOptions = {
    url: 'http://localhost:5173/',
    elId: 'iframe-target'
};
const resizeTarget = function (elId: string, size: TSize2d) {
    const el = document.getElementById(elId);
    if (el) {
        el.style.width = `${size.width}px`;
        el.style.height = `${size.height}px`;
    }
}
const w = getWidget(iOpts);
w.connect()
    .then((communicate) => {
        console.log('widget resolved, provided communication object', communicate);
        setTimeout(() => communicate.execCommand({name: 'setup', data: {content: 'Это какое то сообщение'}}), 2000);
        setTimeout(() => communicate.execCommand({name: 'load_certificates'}), 2024);
        communicate.subscribe(
            (eData) => {
                console.log('Received message in widget subscriber:', eData);
                if (eData.e === 'html_updated') {
                    resizeTarget(iOpts.elId, eData.d);
                } else if (eData.e === 'signed') {
                    console.log('DONE')
                    communicate.execCommand({name: 'to_processing', data: {message: "Подпись сформировалась, должен выполниться внешний обработчик"}});
                }
            },
            (e: string) => {console.log('Received error message:', e)}
        )
        console.log('Connected! {}{}{}');
    }).catch((e) => {
    console.log('Connection error', e);
});