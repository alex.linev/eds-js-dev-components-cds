import {resolve} from 'node:path';
import {defineConfig} from 'vite';
import vue from '@vitejs/plugin-vue';
import alias from '@rollup/plugin-alias';
import terser from '@rollup/plugin-terser';

export default defineConfig({
    // If our .vue files have a style, it will be compiled as a single `.css` file under /dist.
    plugins: [
        vue({style: {filename: `style.css`}}),
        alias({
            entries: [
                {find: '@', replacement: '/src'}
            ]
        })
    ],

    build: {
        // Output compiled files to /dist.
        outDir: `./dist`,
        lib: {
            // Set the entry point (file that contains our components exported).
            entry: resolve(__dirname, `src/index.ts`),
            // Name of the library.
            name: `edsjs-test-pkg`,
            // We are building for CJS and ESM, use a function to rename automatically files.
            // Example: my-component-library.esm.js
            fileName: format => `${`edsjs-test-pkg`}.${format}.js`,
        },
        // minify: 'terser',
        minify: false,
        terserOptions: {
            compress: {
                drop_console: false,
                drop_debug: false,
                sourceMap: true,
            }
        },
        rollupOptions: {
            // Vue is provided by the parent project, don't compile Vue source-code inside our library.
            // external: [`vue`],
            output: {globals: {vue: `Vue`}},
            plugins: [
                terser({
                    // ecma: 2020,
                    // module: true,
                    // warnings: true,
                    compress: {
                        drop_console: false,
                        drop_debugger: false
                    }
                })
            ]
        },
    },
});