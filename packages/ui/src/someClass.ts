export interface ISomeClass {
    getName(): string;
}

export default class SomeClass implements ISomeClass {
    getName(): string {
        return 'SomeClass';
    }
}