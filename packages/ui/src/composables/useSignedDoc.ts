import {ref} from 'vue';
import {IBackendApiServiceProvider, TSignedDocument} from 'edsjs-test-pkg-core';

export default function useSignedDoc(provider: IBackendApiServiceProvider) {
    const isSignedDocFetching = ref(false);
    const signedDoc = ref<TSignedDocument | null>(null);
    const signedDocFetchError = ref<Error | null>(null);

    const fetchSignedDoc = async (id: number): Promise<void> => {
        isSignedDocFetching.value = true;
        try {
            const doc = await provider.fetchSignedDoc(id);
            isSignedDocFetching.value = false;
            signedDoc.value = doc;
        } catch (e: unknown) {
            // if (e instanceof Response && 'error' in e && 'title' in e) { //fixme
            //     // const {title, ok} = e.error;
            //     createSignedDocError.value = new Error(((e.error as ).title) || 'create doc error');
            //     isSignedDocCreating.value = false;
            // } else {
            signedDocFetchError.value = new Error('Ошибка при попытке получить подписанный документ');
            isSignedDocFetching.value = false;
            // }
        }
    }

    return {
        signedDoc,
        fetchSignedDoc,
        isSignedDocFetching,
        signedDocFetchError
    };
}