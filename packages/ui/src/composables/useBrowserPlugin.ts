import {ref} from 'vue';
import type {TSignature, TSignContext} from '@/types';
import {ESignSoftware, ESignStrategy} from '@/types';
import type {IBrowserPluginStoreAsync, TCertificatesMap} from '@/provider/BrowserPlugin/StoreService';
import type {ISignServiceAsync} from '@/provider/BrowserPlugin/SignService';
import {Environment, IEnvironment} from '@/provider/BrowserPlugin';
import CadesBrowserPluginAsync from '@/provider/BrowserPlugin/CadesBrowserPluginAsync';

export function useBrowserPluginConnector() {
    const env = ref<IEnvironment | null>(null);
    const browserPluginFactory = ref<CadesBrowserPluginAsync | null>(null);
    const connectBrowserPluginFactoryError = ref<Error | null>(null);
    const isConnecting = ref(false);

    const getBrowserPluginFactory = async (): Promise<void> => {
        env.value = Environment.i();
        isConnecting.value = true;
        try {
            browserPluginFactory.value = await CadesBrowserPluginAsync.initByEnvironment(env.value);
        } catch (e) {
            console.error(e);
            connectBrowserPluginFactoryError.value = new Error('Plugin connection error');
        } finally {
            isConnecting.value = false;
        }
    }
    return {env, getBrowserPluginFactory, browserPluginFactory, connectBrowserPluginFactoryError};
}

export function useCreateSignature(signService: ISignServiceAsync) {
    const isSigning = ref(false);
    const signature = ref<TSignature | null>(null);
    const createSignatureError = ref<Error | null>(null);

    const createSignature = async (ctx: TSignContext): Promise<void> => {
        isSigning.value = true;
        const {content, strategy} = ctx;
        if (content.type !== 'text') {
            createSignatureError.value = new Error('Неподходящий тип контекста');
            isSigning.value = false;
            return;
        }
        const {type, software} = strategy
        if (content.type === 'text' && type === ESignStrategy.Qualified && software === ESignSoftware.CPro) {
            try {
                signature.value = await signService.signText(content.unsigned_message, strategy.certificate_identifier);
            } catch (e) {
                console.error(e);
                createSignatureError.value = new Error('Не удалось сформировать подпись');
            }
        } else {
            createSignatureError.value = new Error('Неподходящий контекст');
        }
        isSigning.value = false;
    }

    return {
        isSigning,
        signature,
        createSignature,
        createSignatureError,
    }
}

export function useCertificateListFetch(store: IBrowserPluginStoreAsync) {
    const isCertificatesFetching = ref<boolean>(false);
    const certificates = ref<TCertificatesMap | null>(null);
    const certificatesFetchError = ref<Error | null>(null);
    const fetchCertificateList = async (): Promise<void> => {
        certificates.value = null;
        isCertificatesFetching.value = true;
        certificatesFetchError.value = null;
        try {
            certificates.value = await store.getCertificates();
        } catch (e) {
            certificatesFetchError.value = new Error('Не удалось извлечь сертификаты из хранилища');
        } finally {
            isCertificatesFetching.value = false;
        }
    }
    return {
        isCertificatesFetching,
        certificates,
        fetchCertificateList,
        certificatesFetchError
    }
}