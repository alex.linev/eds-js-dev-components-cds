import {defineStore} from 'pinia';
import type {TCertificatesMap, IBrowserPluginStoreAsync} from '@/provider/BrowserPlugin/StoreService';
import {Ref, ref} from 'vue';

export type TCertificatesStore = {
    isCertificatesFetching: Ref<boolean>,
    certificates: Ref<TCertificatesMap | null>,
    certificatesFetchError: Ref<Error | null>,
    fetchCertificateList(pluginStore: IBrowserPluginStoreAsync): Promise<void>
}

export const useCertificatesStore = defineStore('certificates', (): TCertificatesStore => {
        const isCertificatesFetching = ref<boolean>(false);
        const certificates = ref<TCertificatesMap | null>(null);
        const certificatesFetchError = ref<Error | null>(null);
        const fetchCertificateList = async (pluginStore: IBrowserPluginStoreAsync): Promise<void> => {
            certificates.value = null;
            isCertificatesFetching.value = true;
            certificatesFetchError.value = null;
            try {
                certificates.value = await pluginStore.getCertificates();
            } catch (e) {
                certificatesFetchError.value = new Error('Не удалось извлечь сертификаты из хранилища');
            } finally {
                isCertificatesFetching.value = false;
            }
        }
        return {
            isCertificatesFetching, certificates, certificatesFetchError, fetchCertificateList
        };
    }
);
