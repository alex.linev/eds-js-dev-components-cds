import {defineStore} from 'pinia';
import {ref} from 'vue';

export const useProcessingStore = defineStore('processing', () => {
    const isProcessing = ref(false);
    const processingMessage = ref<string>('');
    const setProcessing = (message?: string) => {
        isProcessing.value = true;
        processingMessage.value = message ?? '';
    }
    const clearProcessing = () => {
        isProcessing.value = false;
        processingMessage.value = '';
    }
    return {isProcessing, processingMessage, setProcessing, clearProcessing}
});