import {defineStore} from 'pinia';
import {ref} from 'vue';
import type {IEnvironment} from '@/provider/BrowserPlugin';
import CadesBrowserPluginAsync from '@/provider/BrowserPlugin/CadesBrowserPluginAsync';
import {Environment} from '@/provider/BrowserPlugin';

export const usePluginConnectorStore = defineStore('pluginConnector', () => {
    const env = ref<IEnvironment | null>(null);
    const browserPluginFactory = ref<CadesBrowserPluginAsync | null>(null);
    const connectBrowserPluginFactoryError = ref<Error | null>(null);
    const isConnecting = ref(false);

    const getBrowserPluginFactory = async (): Promise<void> => {
        connectBrowserPluginFactoryError.value = null;
        env.value = Environment.i();
        isConnecting.value = true;
        try {
            browserPluginFactory.value = await CadesBrowserPluginAsync.initByEnvironment(env.value);
        } catch (e) {
            console.error(e);
            connectBrowserPluginFactoryError.value = new Error('Plugin connection error');
        } finally {
            isConnecting.value = false;
        }
    }
    return {env, getBrowserPluginFactory, browserPluginFactory, connectBrowserPluginFactoryError};
});