import {defineStore} from 'pinia';
import {ref} from 'vue';
import type {ISignServiceAsync} from '@/provider/BrowserPlugin/SignService';
import type {TCertificateIdentifier} from '@/types';

export type TSignContext = {
    type: 'qualified',
    software: 'CPro',
    content: {
        type: 'text';
        text: string;
    },
    certificateIdentifier: TCertificateIdentifier;
}

export const useSigningStore = defineStore('signingStore', () => {
    const isSigning = ref(false);
    const isCreateSignatureError = ref(false);
    const createSignatureError = ref<Error | null>(null);
    const createSignatureResult = ref<string | null>(null);

    const createSignature = async (signService: ISignServiceAsync, context: TSignContext): Promise<void> => {
        isSigning.value = true;
        isCreateSignatureError.value = false;
        createSignatureResult.value = null;
        try {
            createSignatureResult.value = await signService.signText(context.content.text, context.certificateIdentifier);
        } catch (e) {
            isCreateSignatureError.value = true;
            createSignatureError.value = e as Error || new Error('При создании ЭП произошла ошибка');
        } finally {
            isSigning.value = false;
        }
    }
    return {
        isSigning,
        isCreateSignatureError,
        createSignatureError,
        createSignatureResult,
        createSignature
    }
});