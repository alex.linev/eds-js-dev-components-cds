import {defineStore} from 'pinia';
import {ref} from 'vue';

export const useErrorStore = defineStore('error', () => {
    const error = ref<string>('');
    const isError = !!error.value;
    const setError = (e: Error | string): void => {
        error.value = e instanceof Error ? e.message : e;
    }
    const clearError = () => {
        error.value = ''
    };
    return {
        error,
        isError,
        setError,
        clearError
    }
});