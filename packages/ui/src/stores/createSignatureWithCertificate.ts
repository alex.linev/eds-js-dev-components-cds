import {defineStore} from 'pinia';
import {reactive, Ref} from 'vue';
import {TCertificatesMap} from '@/provider/BrowserPlugin/StoreService';
import {
    TCertificateIdentifier,
    TSignature, TSignContent, TSignContext,
    TSignCProStrategy, TSignTextContent
} from '@/types';
import {IEnvironment} from '@/provider/BrowserPlugin';
import CadesBrowserPluginAsync from '@/provider/BrowserPlugin/CadesBrowserPluginAsync';
import {useBrowserPluginConnector, useCertificateListFetch, useCreateSignature} from '@/composables/useBrowserPlugin';

export type TStatus = 'idle' | 'processing' | 'certificate_select' | 'error' | 'done';

type TProcessing = {
    message: string
};
type TBaseState = {
    status: TStatus;
    error?: Error;
    processing?: TProcessing;
    env?: IEnvironment;
    objectFactory?: CadesBrowserPluginAsync;
    certificates?: TCertificatesMap;
    selectedCertificate?: TCertificateIdentifier;
    signature?: TSignature;
};
type TErrorState = TBaseState & {
    status: 'error';
    error: Error;
};
type TProcessingState = TBaseState & {
    status: 'processing';
    processing: TProcessing;
}
type TCertificateSelectState = TBaseState & {
    status: 'certificate_select';
    certificates: TCertificatesMap;
    selectedCertificate: null;
}
type TSignedState = TBaseState & {
    status: 'done';
    selectedCertificate: TCertificateIdentifier;
    signature: TSignature;
}
export type TState = TBaseState | TErrorState | TProcessingState | TCertificateSelectState | TSignedState;

export const getInitialState = (srcState?: TState): TState => ({
    ...srcState || {},
    ...{status: 'idle', selectedCertificate: undefined, error: undefined, signature: undefined}
});

export const createSignatureWithCertificate = defineStore('createSignatureWithCertificate', (useState?: TState) => {
    const state = reactive<TState>(getInitialState(useState));
    const resetState = () => {
        state.status = 'idle';
        state.selectedCertificate = undefined;
        state.error = undefined;
        state.signature = undefined;
    }

    const setProcessingState = (message: string) => {
        state.status = 'processing';
        state.processing = {message};
    }

    const setErrorState = (message: string) => {
        state.status = 'error';
        state.error = new Error(message);
    }
    const setSigned = (signature: TSignature) => {
        state.status = 'done';
        state.signature = signature;
    }

    ///////////

    const connectToPlugin = async (): Promise<void> => {
        setProcessingState('Подключение к плагину');
        const {
            env,
            getBrowserPluginFactory,
            browserPluginFactory,
            connectBrowserPluginFactoryError
        } = useBrowserPluginConnector();
        await getBrowserPluginFactory();
        if (connectBrowserPluginFactoryError.value) {
            console.log('Raised Error in connectToPlugin', connectBrowserPluginFactoryError.value);
            setErrorState('Ошибка при подключении к плагину');
            return;
        }
        if (!env?.value) {
            console.log('по какой то причине нет ошибки но и нет env');
            setErrorState('Ошибка при подключении к плагину');
            return;
        }
        if (!browserPluginFactory?.value) {
            console.log('По какой то причине нет ошибки и нет фабрики объектов ');
            setErrorState('Ошибка при подключении к плагину');
            return;
        }
        state.env = env.value;
        state.objectFactory = browserPluginFactory.value;
    }

    /////
    const loadCertificates = async (): Promise<void> => {
        setProcessingState('Загрузка сертификатов');
        if (!state.objectFactory) {
            console.error('Какого то хрена нет фабрики');
            return;
        }
        const {
            certificates,
            fetchCertificateList,
            certificatesFetchError
        } = useCertificateListFetch(state.objectFactory.storeService);
        await fetchCertificateList();
        if (certificatesFetchError?.value) {
            setErrorState('Ошибка загрузки сертификатов');
            return;
        }
        if (!certificates.value) {
            setErrorState('Какого то хрена нет сертификатов и ошибки тоже нет');
            return;
        }
        state.status = 'idle';
        state.certificates = certificates.value;
    }


    /////
    const createSignatureWithCertificate = async (certificate: TCertificateIdentifier, signContent: TSignContent): Promise<void> => {
        setProcessingState('Формирование подписи');
        if (!state.objectFactory) {
            console.error('Какого то хрена нет фабрики');
            return;
        }
        const {
            signature,
            createSignature,
            createSignatureError
        } = useCreateSignature(state.objectFactory.signService);
        const ctx = { // fixme контекст вообще не нужен тут
            content:   signContent as TSignTextContent,
            strategy: {type: 'qualified', software: 'cpro', certificate_identifier: certificate} as TSignCProStrategy
        }
        await createSignature(ctx);
        if (createSignatureError?.value) {
            console.log('sign error', createSignatureError.value);
            setErrorState('Ошибка при создании подписи')
            return;
        }
        if (!signature?.value) {
            console.log('sign error', signature.value);
            setErrorState('Ошибка при создании подписи')
            return;
        }
        setSigned(signature.value as string);
    }

    /////
    const loadCertificatesList = async (): Promise<void> => {
        resetState();
        console.log('loadCertificatesList in sign store process invoked ', state);
        if (!state.objectFactory) {
            await connectToPlugin();
            if (state.status === 'error') {
                return;
            }
        }
        if (!state.certificates) {
            await loadCertificates();
            if (state.status === 'error') {
                return;
            }
        }
        state.status = 'certificate_select';
    }

    /////
    const sign = async (ctx: TSignContext, certificate: TCertificateIdentifier): Promise<void> => {
        resetState();
        state.selectedCertificate = certificate;
        console.log('SSign in sign store process invoked ', ctx, state);
        if (!state.objectFactory) {
            await connectToPlugin();
            if (state.status === 'error') {
                return;
            }
        }

        // что-то тут не так, по идее, нужно порезать на две части, одна про загрузку и показ
        // списка сертификатов для выбора, а вторая для подписания уже конкретным сертификатом,
        // но есть нюанс - если нужно подписывать заранее предустановленным сертификатом -
        // хорошо бы его тоже сперва показать хоть и в единственным в списке

        // Если в стейте уже есть сертификаты и не надо обязательно перегрузить - переключаемся в стейт выбора сертификата
        if (!state.certificates) {
            await loadCertificates();
            if (state.status === 'error') {
                return;
            }
        }
        await createSignatureWithCertificate(state.selectedCertificate, ctx.content);
        // сертификатов нет - грузим и переключаемся в стейт выбора сертификата
    }
    return {
        state,
        sign,
        loadCertificatesList
    }
});
