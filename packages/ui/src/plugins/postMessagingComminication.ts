import {App} from 'vue';
import {receiveCommandToIframeWidget, sendFromIframeWidget} from 'eds-test-pkg-iframe-client';

export default {
    install(app: App, options: any) {
        console.log('register $edsWidgetPostMessagingCommunication', options);
        console.log('register $edsWidgetPostMessagingCommunication');
        app.provide('subscribeEdsPostMessagingCommands', receiveCommandToIframeWidget);
        app.provide('sendEdsPostMessagingEvent', sendFromIframeWidget);
    }
}
