/**
 * @author API Team <api-dev-team@b2b-center.ru>
 */
import type { ICertificate, ICertificates, IEncodedData, IOID, IPublicKey, IStore } from './CapicomSync';
import type {Async} from '@/provider/BrowserPlugin';

// export interface ICertificateAsync extends Async<ICertificate> {}
//
// export interface ICertificatesAsync<T = ICertificate> extends Async<ICertificates<T>> {}
//
// export interface IStoreAsync<T = ICertificate> extends Async<IStore<T>> {}
//
// export interface IEncodedDataAsync extends Async<IEncodedData> {}
//
// export interface IOIDAsync extends Async<IOID> {}
//
// export interface IPublicKeyAsync extends Async<IPublicKey> {}

export type ICertificateAsync = Async<ICertificate>;

export type ICertificatesAsync<T = ICertificate> = Async<ICertificates<T>>;

export type IStoreAsync<T = ICertificate> = Async<IStore<T>>;

export type IEncodedDataAsync = Async<IEncodedData>;

export type IOIDAsync = Async<IOID>;

export type IPublicKeyAsync = Async<IPublicKey>;
