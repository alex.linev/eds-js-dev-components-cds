import Bowser from 'bowser';
import {ISignServiceAsync} from '@/provider/BrowserPlugin/SignService';
import BrowserPluginStoreAsync from '@/provider/BrowserPlugin/StoreService'; // todo: Подумать, как не тащить это сюда

type UNKNOWN_BROWSER_PARAM = 'unknown';

// TODO: Собирать ли типы в один файл?
type TBrowserInfo = {
  name: string | UNKNOWN_BROWSER_PARAM;
  version: string | UNKNOWN_BROWSER_PARAM;
};

type TOsInfo = {
  name: string | UNKNOWN_BROWSER_PARAM;
  version?: string | UNKNOWN_BROWSER_PARAM;
  bits?: string;
};

export enum EPluginConnectionType {
  ExtensionWithStaticUrl = 'extension_with_static_url',
  ExtensionWithDynamicUrl = 'extension_with_dynamic_url',
  NPAPI = 'npapi'
}

// todo: Переделать, т.к. тут херня какая то
export enum EPluginConnectionStaticUrl {
  Chrome = 'chrome-extension://iifchhfnnmpdbibifmljnfjhpififfog/nmcades_plugin_api.js',
  Opera = 'chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js'
}

// export type TConnectionTypeInfo = { type: EPluginConnectionType; connectionUrl?: string };
export type TPluginConnectionInfo =
  | {
      type: EPluginConnectionType.ExtensionWithStaticUrl;
      connectionUrl: EPluginConnectionStaticUrl;
    }
  | { type: EPluginConnectionType.ExtensionWithDynamicUrl }
  | { type: EPluginConnectionType.NPAPI };

export type TEnvironment = {
  pluginConnectionType: TPluginConnectionInfo;
  canPromise: boolean;
  systemInfo: {
    browser: TBrowserInfo;
    os: TOsInfo;
  };
  // todo: plugin information
};

export interface IEnvironment {
  // detectByBrowser(): Promise<TEnvironment>;
  get pluginConnectionInfo(): TPluginConnectionInfo;
  get canPromise(): boolean;
  toJson(): string;
}

export class Environment implements IEnvironment {
  private static _instance: Environment;

  private constructor(private readonly env: TEnvironment) {}

  static i(): IEnvironment {
    if (!Environment._instance) {
      Environment._instance = Environment.createByBrowser();
    }
    return Environment._instance;
  }

  static createByBrowser(): Environment {
    const browserParser = Bowser.getParser(window.navigator.userAgent);
    let connectionType: TPluginConnectionInfo;
    const browserName = browserParser.getBrowser().name;
    switch (browserName) {
      case Bowser.BROWSER_MAP.firefox:
      case Bowser.BROWSER_MAP.edge:
      case Bowser.BROWSER_MAP.safari:
        // todo: fallback to npapi when old browser is detected.
        connectionType = { type: EPluginConnectionType.ExtensionWithDynamicUrl };
        break;
      case Bowser.BROWSER_MAP.ie:
        connectionType = { type: EPluginConnectionType.NPAPI };
        break;
      default:
        connectionType = {
          type: EPluginConnectionType.ExtensionWithStaticUrl,
          connectionUrl:
            browserName && [Bowser.BROWSER_MAP.opera, Bowser.BROWSER_MAP.yandex].includes(browserName)
              ? EPluginConnectionStaticUrl.Opera
              : EPluginConnectionStaticUrl.Chrome
        };
    }
    const env: TEnvironment = {
      pluginConnectionType: connectionType,
      canPromise: 'Promise' in window,
      systemInfo: {
        browser: {
          name: browserParser.getBrowserName(),
          version: browserParser.getBrowserVersion() ?? 'unknown'
        },
        os: {
          name: browserParser.getOSName(),
          version: browserParser.getOSVersion() ?? 'unknown'
        }
      }
    };
    return new Environment(env);
  }

  get pluginConnectionInfo(): TPluginConnectionInfo {
    return this.env.pluginConnectionType;
  }

  get canPromise(): boolean {
    return this.env.canPromise;
  }

  toJson(): string {
    return JSON.stringify(this.env);
  }
}
