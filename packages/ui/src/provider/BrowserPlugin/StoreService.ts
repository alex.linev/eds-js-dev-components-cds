import type CadesBrowserPluginAsync from './CadesBrowserPluginAsync';
import type { Async, ICPCertificate } from './cadescom';
import { CAPICOM_CERTIFICATE_FIND_TYPE } from './capicom';

export interface IBrowserPluginStoreAsync {
  getCertificates(): Promise<TCertificatesMap>;
  getCertificate(identifier: TThumbprint): Promise<Async<ICPCertificate>>;
}

export type TThumbprint = string;

export type TCertificateItem = {
  serialNumber: string;
  thumbprint: TThumbprint;
  owner: string;
  getName: () => string
};

export type TCertificatesMap = Map<TThumbprint, TCertificateItem>;

export default class BrowserPluginStoreAsync implements IBrowserPluginStoreAsync {
  constructor(readonly _plugin: CadesBrowserPluginAsync) {}

  public async getCertificate(identifier: string): Promise<Async<ICPCertificate>> {
    const store = await this._plugin._providerInstance.CreateObjectAsync('CAdESCOM.Store');
    await store.Open();
    const certificatesObj = await store.Certificates;
    const findedCertObj = await certificatesObj.Find(
      CAPICOM_CERTIFICATE_FIND_TYPE.CAPICOM_CERTIFICATE_FIND_SHA1_HASH,
      identifier
    );
    const foundedNum = await findedCertObj.Count;
    if (!foundedNum) {
      throw new Error(`Certificate ${identifier} was not found in store`);
    }
    const certificateObj = await findedCertObj.Item(1);
    console.log('FOUNDED CERT ', identifier, await certificateObj.Thumbprint);
    if (!certificateObj) {
      throw new Error('Error while extract item at index 1');
    }
    return certificateObj;
  }

  public async getCertificates(): Promise<TCertificatesMap> {
    const store = await this._plugin._providerInstance.CreateObjectAsync('CAdESCOM.Store');
    await store.Open();
    const certificatesStoreObj = await store.Certificates;
    const totalNum = await certificatesStoreObj.Count;
    const certificateMap = new Map();
    if (!totalNum) {
      return certificateMap;
    }
    for (let i = 0; i < totalNum; i++) {
      const c = await certificatesStoreObj.Item(i + 1);
      const owner = await c.SubjectName;
      const parsed_subject = {
            cn: parseCertificateSubjectParam(owner, Constant.DN_ATTRIBUTE_COMMON_NAME),
            o: parseCertificateSubjectParam(owner, Constant.DN_ATTRIBUTE_ORGANIZATION_NAME),
            n: parseCertificateSubjectParam(owner, Constant.DN_ATTRIBUTE_USER_SURNAME),
            g: parseCertificateSubjectParam(owner, Constant.DN_ATTRIBUTE_GIVENNAME)
          };
      const issuer = await c.IssuerName;
      const parsed_issuer = {
        o: parseCertificateSubjectParam(issuer, Constant.DN_ATTRIBUTE_ORGANIZATION_NAME)
      };
      const validToDate = new Date(await c.ValidToDate);
      const o: TCertificateItem = {
        serialNumber: await c.SerialNumber,
        thumbprint: await c.Thumbprint,
        owner,
        // issuer,
        getName() {
          const {subject = {cn: '', o: ''}, issuer = {o: ''}} = {
            subject: parsed_subject,
            issuer: parsed_issuer
          };
          const subject_organization = subject.o && subject.o !== subject.cn ? `${subject.o}, ` : '';
          const valid_to = `действителен до ${getFormattedDate(validToDate)}г.`;
          return `${subject.cn}, ${subject_organization} ${valid_to}, ${issuer.o}`;
        }
      };
      certificateMap.set(o.thumbprint, o);
    }

    await store.Close();
    return certificateMap;
  }
}

const Constant = {
  DN_ATTRIBUTE_COMMON_NAME: 'CN', //Общее имя
  DN_ATTRIBUTE_LOCALITY_NAME: 'L', //Город
  DN_ATTRIBUTE_STATE_OR_PROVINCE_NAME: 'S', //Область /* should be "ST" (rfc1327) but MS uses 'S' */
  DN_ATTRIBUTE_ORGANIZATION_NAME: 'O', // Организация
  DN_ATTRIBUTE_ORGANIZATION_UNIT_NAME: 'OU', //Подразделение
  DN_ATTRIBUTE_COUNTRY_NAME: 'C', //Страна/регион
  DN_ATTRIBUTE_COUNTRY_EMAIL: 'E', //email
  DN_ATTRIBUTE_STREET_ADDRESS: 'STREET', //Адрес
  DN_ATTRIBUTE_USER_SURNAME: 'SN', //Фамилия
  DN_ATTRIBUTE_GIVENNAME: 'G', //Имя
  DN_ATTRIBUTE_TITLE: 'T', //Должность/звание
}

const parseCertificateSubjectParam = (subject: string, param: string, default_value = '') : string => {
  let arr = subject.split(', ');
  for (let i = 0; i < arr.length; i++) {
    let a = arr[i].split('=', 2);
    if (a[0] && a[1]) {
      let k = a[0].toUpperCase();
      if (k === param) {
        return a[1].replace(/["']/g, '');
      }
    }
  }
  return default_value;
}

const getFormattedDate = (val: string | Date, with_time = false): string => {
  if (typeof(val) !== 'object') {
    const reg_exp = new RegExp('^[0-9]+.[0-9]+.[0-9]{4}' + (with_time ? 's[0-9]:[0-9]:[0-9]' : ''));
    const date_arr = reg_exp.exec(val);
    if (date_arr && date_arr[0]) {
      return date_arr[0];
    }
    val = new Date(val);
  }
  let m = val.getMonth() + 1;
  let month = String(m);
  if (m < 10) {
    month = '0' + month;
  }
  let result = (val.getDate() + '.' + month + '.' + val.getFullYear());
  if (with_time) {
    let minutes = ('0' + val.getMinutes()).slice(-2),
        hours   = ('0' + val.getHours()).slice(-2),
        seconds = ('0' + val.getSeconds()).slice(-2);
    result += ' ' + hours + ':' + minutes + ':' + seconds;
  }
  return result;
}