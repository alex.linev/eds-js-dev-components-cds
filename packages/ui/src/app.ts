import {createApp} from 'vue'
import App from '@/app/App.vue'
import { createPinia } from 'pinia'
import {createCds, components, directives} from '@central-design-system/components';
import {icons} from '@central-design-system/icons';
import '@central-design-system/components/dist/cds.css';
import postMessagingCommunication from '@/plugins/postMessagingComminication';

const cds = createCds({
    components,
    directives,
    icons
});

export function runApp(targetId: string, opts?: Record<string, unknown>): void {
    console.log('RUN APP ', opts)
    const app = createApp(App, opts ?? {});
    app.use(createPinia());
    app.use(cds);
    app.use(postMessagingCommunication, {})
    app.mount(targetId);
}
