var __defProp = Object.defineProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};
class e {
  getName() {
    return "SomeClass";
  }
}
/**
* @vue/shared v3.4.26
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/
/*! #__NO_SIDE_EFFECTS__ */
function t(e2, t2) {
  const n2 = new Set(e2.split(","));
  return t2 ? (e3) => n2.has(e3.toLowerCase()) : (e3) => n2.has(e3);
}
const n = "production" !== process.env.NODE_ENV ? Object.freeze({}) : {}, o = "production" !== process.env.NODE_ENV ? Object.freeze([]) : [], r = () => {
}, s = () => false, a = (e2) => 111 === e2.charCodeAt(0) && 110 === e2.charCodeAt(1) && (e2.charCodeAt(2) > 122 || e2.charCodeAt(2) < 97), l = (e2) => e2.startsWith("onUpdate:"), i = Object.assign, c = (e2, t2) => {
  const n2 = e2.indexOf(t2);
  n2 > -1 && e2.splice(n2, 1);
}, d = Object.prototype.hasOwnProperty, u = (e2, t2) => d.call(e2, t2), p = Array.isArray, f = (e2) => "[object Map]" === _(e2), h = (e2) => "[object Set]" === _(e2), v = (e2) => "function" == typeof e2, m = (e2) => "string" == typeof e2, g = (e2) => "symbol" == typeof e2, y = (e2) => null !== e2 && "object" == typeof e2, b = (e2) => (y(e2) || v(e2)) && v(e2.then) && v(e2.catch), w = Object.prototype.toString, _ = (e2) => w.call(e2), C = (e2) => _(e2).slice(8, -1), x = (e2) => "[object Object]" === _(e2), k = (e2) => m(e2) && "NaN" !== e2 && "-" !== e2[0] && "" + parseInt(e2, 10) === e2, S = t(",key,ref,ref_for,ref_key,onVnodeBeforeMount,onVnodeMounted,onVnodeBeforeUpdate,onVnodeUpdated,onVnodeBeforeUnmount,onVnodeUnmounted"), E = t("bind,cloak,else-if,else,for,html,if,model,on,once,pre,show,slot,text,memo"), I = (e2) => {
  const t2 = /* @__PURE__ */ Object.create(null);
  return (n2) => t2[n2] || (t2[n2] = e2(n2));
}, O = /-(\w)/g, N = I((e2) => e2.replace(O, (e3, t2) => t2 ? t2.toUpperCase() : "")), M = /\B([A-Z])/g, A = I((e2) => e2.replace(M, "-$1").toLowerCase()), D = I((e2) => e2.charAt(0).toUpperCase() + e2.slice(1)), V = I((e2) => e2 ? `on${D(e2)}` : ""), T = (e2, t2) => !Object.is(e2, t2), L = (e2, t2) => {
  for (let n2 = 0; n2 < e2.length; n2++)
    e2[n2](t2);
}, P = (e2, t2, n2, o2 = false) => {
  Object.defineProperty(e2, t2, { configurable: true, enumerable: false, writable: o2, value: n2 });
}, F = (e2) => {
  const t2 = parseFloat(e2);
  return isNaN(t2) ? e2 : t2;
}, R = (e2) => {
  const t2 = m(e2) ? Number(e2) : NaN;
  return isNaN(t2) ? e2 : t2;
};
let $;
const B = () => $ || ($ = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof self ? self : "undefined" != typeof window ? window : "undefined" != typeof global ? global : {});
function H(e2) {
  if (p(e2)) {
    const t2 = {};
    for (let n2 = 0; n2 < e2.length; n2++) {
      const o2 = e2[n2], r2 = m(o2) ? Z(o2) : H(o2);
      if (r2)
        for (const e3 in r2)
          t2[e3] = r2[e3];
    }
    return t2;
  }
  if (m(e2) || y(e2))
    return e2;
}
const j = /;(?![^(]*\))/g, K = /:([^]+)/, z = /\/\*[^]*?\*\//g;
function Z(e2) {
  const t2 = {};
  return e2.replace(z, "").split(j).forEach((e3) => {
    if (e3) {
      const n2 = e3.split(K);
      n2.length > 1 && (t2[n2[0].trim()] = n2[1].trim());
    }
  }), t2;
}
function U(e2) {
  let t2 = "";
  if (m(e2))
    t2 = e2;
  else if (p(e2))
    for (let n2 = 0; n2 < e2.length; n2++) {
      const o2 = U(e2[n2]);
      o2 && (t2 += o2 + " ");
    }
  else if (y(e2))
    for (const n2 in e2)
      e2[n2] && (t2 += n2 + " ");
  return t2.trim();
}
function W(e2) {
  if (!e2)
    return null;
  let { class: t2, style: n2 } = e2;
  return t2 && !m(t2) && (e2.class = U(t2)), n2 && (e2.style = H(n2)), e2;
}
const Y = t("html,body,base,head,link,meta,style,title,address,article,aside,footer,header,hgroup,h1,h2,h3,h4,h5,h6,nav,section,div,dd,dl,dt,figcaption,figure,picture,hr,img,li,main,ol,p,pre,ul,a,b,abbr,bdi,bdo,br,cite,code,data,dfn,em,i,kbd,mark,q,rp,rt,ruby,s,samp,small,span,strong,sub,sup,time,u,var,wbr,area,audio,map,track,video,embed,object,param,source,canvas,script,noscript,del,ins,caption,col,colgroup,table,thead,tbody,td,th,tr,button,datalist,fieldset,form,input,label,legend,meter,optgroup,option,output,progress,select,textarea,details,dialog,menu,summary,template,blockquote,iframe,tfoot"), G = t("svg,animate,animateMotion,animateTransform,circle,clipPath,color-profile,defs,desc,discard,ellipse,feBlend,feColorMatrix,feComponentTransfer,feComposite,feConvolveMatrix,feDiffuseLighting,feDisplacementMap,feDistantLight,feDropShadow,feFlood,feFuncA,feFuncB,feFuncG,feFuncR,feGaussianBlur,feImage,feMerge,feMergeNode,feMorphology,feOffset,fePointLight,feSpecularLighting,feSpotLight,feTile,feTurbulence,filter,foreignObject,g,hatch,hatchpath,image,line,linearGradient,marker,mask,mesh,meshgradient,meshpatch,meshrow,metadata,mpath,path,pattern,polygon,polyline,radialGradient,rect,set,solidcolor,stop,switch,symbol,text,textPath,title,tspan,unknown,use,view"), q = t("annotation,annotation-xml,maction,maligngroup,malignmark,math,menclose,merror,mfenced,mfrac,mfraction,mglyph,mi,mlabeledtr,mlongdiv,mmultiscripts,mn,mo,mover,mpadded,mphantom,mprescripts,mroot,mrow,ms,mscarries,mscarry,msgroup,msline,mspace,msqrt,msrow,mstack,mstyle,msub,msubsup,msup,mtable,mtd,mtext,mtr,munder,munderover,none,semantics"), X = t("itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly");
function J(e2) {
  return !!e2 || "" === e2;
}
const Q = (e2) => m(e2) ? e2 : null == e2 ? "" : p(e2) || y(e2) && (e2.toString === w || !v(e2.toString)) ? JSON.stringify(e2, ee, 2) : String(e2), ee = (e2, t2) => t2 && t2.__v_isRef ? ee(e2, t2.value) : f(t2) ? { [`Map(${t2.size})`]: [...t2.entries()].reduce((e3, [t3, n2], o2) => (e3[te(t3, o2) + " =>"] = n2, e3), {}) } : h(t2) ? { [`Set(${t2.size})`]: [...t2.values()].map((e3) => te(e3)) } : g(t2) ? te(t2) : !y(t2) || p(t2) || x(t2) ? t2 : String(t2), te = (e2, t2 = "") => {
  var n2;
  return g(e2) ? `Symbol(${null != (n2 = e2.description) ? n2 : t2})` : e2;
};
/**
* @vue/reactivity v3.4.26
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/
function ne(e2, ...t2) {
  console.warn(`[Vue warn] ${e2}`, ...t2);
}
let oe, re;
class se {
  constructor(e2 = false) {
    this.detached = e2, this._active = true, this.effects = [], this.cleanups = [], this.parent = oe, !e2 && oe && (this.index = (oe.scopes || (oe.scopes = [])).push(this) - 1);
  }
  get active() {
    return this._active;
  }
  run(e2) {
    if (this._active) {
      const t2 = oe;
      try {
        return oe = this, e2();
      } finally {
        oe = t2;
      }
    } else
      "production" !== process.env.NODE_ENV && ne("cannot run an inactive effect scope.");
  }
  on() {
    oe = this;
  }
  off() {
    oe = this.parent;
  }
  stop(e2) {
    if (this._active) {
      let t2, n2;
      for (t2 = 0, n2 = this.effects.length; t2 < n2; t2++)
        this.effects[t2].stop();
      for (t2 = 0, n2 = this.cleanups.length; t2 < n2; t2++)
        this.cleanups[t2]();
      if (this.scopes)
        for (t2 = 0, n2 = this.scopes.length; t2 < n2; t2++)
          this.scopes[t2].stop(true);
      if (!this.detached && this.parent && !e2) {
        const e3 = this.parent.scopes.pop();
        e3 && e3 !== this && (this.parent.scopes[this.index] = e3, e3.index = this.index);
      }
      this.parent = void 0, this._active = false;
    }
  }
}
function ae(e2) {
  return new se(e2);
}
function le() {
  return oe;
}
function ie(e2) {
  oe ? oe.cleanups.push(e2) : "production" !== process.env.NODE_ENV && ne("onScopeDispose() is called when there is no active effect scope to be associated with.");
}
class ce {
  constructor(e2, t2, n2, o2) {
    this.fn = e2, this.trigger = t2, this.scheduler = n2, this.active = true, this.deps = [], this._dirtyLevel = 4, this._trackId = 0, this._runnings = 0, this._shouldSchedule = false, this._depsLength = 0, function(e3, t3 = oe) {
      t3 && t3.active && t3.effects.push(e3);
    }(this, o2);
  }
  get dirty() {
    if (2 === this._dirtyLevel || 3 === this._dirtyLevel) {
      this._dirtyLevel = 1, ge();
      for (let e2 = 0; e2 < this._depsLength; e2++) {
        const t2 = this.deps[e2];
        if (t2.computed && (de(t2.computed), this._dirtyLevel >= 4))
          break;
      }
      1 === this._dirtyLevel && (this._dirtyLevel = 0), ye();
    }
    return this._dirtyLevel >= 4;
  }
  set dirty(e2) {
    this._dirtyLevel = e2 ? 4 : 0;
  }
  run() {
    if (this._dirtyLevel = 0, !this.active)
      return this.fn();
    let e2 = he, t2 = re;
    try {
      return he = true, re = this, this._runnings++, ue(this), this.fn();
    } finally {
      pe(this), this._runnings--, re = t2, he = e2;
    }
  }
  stop() {
    this.active && (ue(this), pe(this), this.onStop && this.onStop(), this.active = false);
  }
}
function de(e2) {
  return e2.value;
}
function ue(e2) {
  e2._trackId++, e2._depsLength = 0;
}
function pe(e2) {
  if (e2.deps.length > e2._depsLength) {
    for (let t2 = e2._depsLength; t2 < e2.deps.length; t2++)
      fe(e2.deps[t2], e2);
    e2.deps.length = e2._depsLength;
  }
}
function fe(e2, t2) {
  const n2 = e2.get(t2);
  void 0 !== n2 && t2._trackId !== n2 && (e2.delete(t2), 0 === e2.size && e2.cleanup());
}
let he = true, ve = 0;
const me = [];
function ge() {
  me.push(he), he = false;
}
function ye() {
  const e2 = me.pop();
  he = void 0 === e2 || e2;
}
function be() {
  ve++;
}
function we() {
  for (ve--; !ve && Ce.length; )
    Ce.shift()();
}
function _e(e2, t2, n2) {
  var o2;
  if (t2.get(e2) !== e2._trackId) {
    t2.set(e2, e2._trackId);
    const r2 = e2.deps[e2._depsLength];
    r2 !== t2 ? (r2 && fe(r2, e2), e2.deps[e2._depsLength++] = t2) : e2._depsLength++, "production" !== process.env.NODE_ENV && (null == (o2 = e2.onTrack) || o2.call(e2, i({ effect: e2 }, n2)));
  }
}
const Ce = [];
function xe(e2, t2, n2) {
  var o2;
  be();
  for (const r2 of e2.keys()) {
    let s2;
    r2._dirtyLevel < t2 && (null != s2 ? s2 : s2 = e2.get(r2) === r2._trackId) && (r2._shouldSchedule || (r2._shouldSchedule = 0 === r2._dirtyLevel), r2._dirtyLevel = t2), r2._shouldSchedule && (null != s2 ? s2 : s2 = e2.get(r2) === r2._trackId) && ("production" !== process.env.NODE_ENV && (null == (o2 = r2.onTrigger) || o2.call(r2, i({ effect: r2 }, n2))), r2.trigger(), r2._runnings && !r2.allowRecurse || 2 === r2._dirtyLevel || (r2._shouldSchedule = false, r2.scheduler && Ce.push(r2.scheduler)));
  }
  we();
}
const ke = (e2, t2) => {
  const n2 = /* @__PURE__ */ new Map();
  return n2.cleanup = e2, n2.computed = t2, n2;
}, Se = /* @__PURE__ */ new WeakMap(), Ee = Symbol("production" !== process.env.NODE_ENV ? "iterate" : ""), Ie = Symbol("production" !== process.env.NODE_ENV ? "Map key iterate" : "");
function Oe(e2, t2, n2) {
  if (he && re) {
    let o2 = Se.get(e2);
    o2 || Se.set(e2, o2 = /* @__PURE__ */ new Map());
    let r2 = o2.get(n2);
    r2 || o2.set(n2, r2 = ke(() => o2.delete(n2))), _e(re, r2, "production" !== process.env.NODE_ENV ? { target: e2, type: t2, key: n2 } : void 0);
  }
}
function Ne(e2, t2, n2, o2, r2, s2) {
  const a2 = Se.get(e2);
  if (!a2)
    return;
  let l2 = [];
  if ("clear" === t2)
    l2 = [...a2.values()];
  else if ("length" === n2 && p(e2)) {
    const e3 = Number(o2);
    a2.forEach((t3, n3) => {
      ("length" === n3 || !g(n3) && n3 >= e3) && l2.push(t3);
    });
  } else
    switch (void 0 !== n2 && l2.push(a2.get(n2)), t2) {
      case "add":
        p(e2) ? k(n2) && l2.push(a2.get("length")) : (l2.push(a2.get(Ee)), f(e2) && l2.push(a2.get(Ie)));
        break;
      case "delete":
        p(e2) || (l2.push(a2.get(Ee)), f(e2) && l2.push(a2.get(Ie)));
        break;
      case "set":
        f(e2) && l2.push(a2.get(Ee));
    }
  be();
  for (const a3 of l2)
    a3 && xe(a3, 4, "production" !== process.env.NODE_ENV ? { target: e2, type: t2, key: n2, newValue: o2, oldValue: r2, oldTarget: s2 } : void 0);
  we();
}
const Me = t("__proto__,__v_isRef,__isVue"), Ae = new Set(Object.getOwnPropertyNames(Symbol).filter((e2) => "arguments" !== e2 && "caller" !== e2).map((e2) => Symbol[e2]).filter(g)), De = Ve();
function Ve() {
  const e2 = {};
  return ["includes", "indexOf", "lastIndexOf"].forEach((t2) => {
    e2[t2] = function(...e3) {
      const n2 = xt(this);
      for (let e4 = 0, t3 = this.length; e4 < t3; e4++)
        Oe(n2, "get", e4 + "");
      const o2 = n2[t2](...e3);
      return -1 === o2 || false === o2 ? n2[t2](...e3.map(xt)) : o2;
    };
  }), ["push", "pop", "shift", "unshift", "splice"].forEach((t2) => {
    e2[t2] = function(...e3) {
      ge(), be();
      const n2 = xt(this)[t2].apply(this, e3);
      return we(), ye(), n2;
    };
  }), e2;
}
function Te(e2) {
  g(e2) || (e2 = String(e2));
  const t2 = xt(this);
  return Oe(t2, "has", e2), t2.hasOwnProperty(e2);
}
class Le {
  constructor(e2 = false, t2 = false) {
    this._isReadonly = e2, this._isShallow = t2;
  }
  get(e2, t2, n2) {
    const o2 = this._isReadonly, r2 = this._isShallow;
    if ("__v_isReactive" === t2)
      return !o2;
    if ("__v_isReadonly" === t2)
      return o2;
    if ("__v_isShallow" === t2)
      return r2;
    if ("__v_raw" === t2)
      return n2 === (o2 ? r2 ? ht : ft : r2 ? pt : ut).get(e2) || Object.getPrototypeOf(e2) === Object.getPrototypeOf(n2) ? e2 : void 0;
    const s2 = p(e2);
    if (!o2) {
      if (s2 && u(De, t2))
        return Reflect.get(De, t2, n2);
      if ("hasOwnProperty" === t2)
        return Te;
    }
    const a2 = Reflect.get(e2, t2, n2);
    return (g(t2) ? Ae.has(t2) : Me(t2)) ? a2 : (o2 || Oe(e2, "get", t2), r2 ? a2 : Mt(a2) ? s2 && k(t2) ? a2 : a2.value : y(a2) ? o2 ? mt(a2) : vt(a2) : a2);
  }
}
class Pe extends Le {
  constructor(e2 = false) {
    super(false, e2);
  }
  set(e2, t2, n2, o2) {
    let r2 = e2[t2];
    if (!this._isShallow) {
      const t3 = wt(r2);
      if (_t(n2) || wt(n2) || (r2 = xt(r2), n2 = xt(n2)), !p(e2) && Mt(r2) && !Mt(n2))
        return !t3 && (r2.value = n2, true);
    }
    const s2 = p(e2) && k(t2) ? Number(t2) < e2.length : u(e2, t2), a2 = Reflect.set(e2, t2, n2, o2);
    return e2 === xt(o2) && (s2 ? T(n2, r2) && Ne(e2, "set", t2, n2, r2) : Ne(e2, "add", t2, n2)), a2;
  }
  deleteProperty(e2, t2) {
    const n2 = u(e2, t2), o2 = e2[t2], r2 = Reflect.deleteProperty(e2, t2);
    return r2 && n2 && Ne(e2, "delete", t2, void 0, o2), r2;
  }
  has(e2, t2) {
    const n2 = Reflect.has(e2, t2);
    return g(t2) && Ae.has(t2) || Oe(e2, "has", t2), n2;
  }
  ownKeys(e2) {
    return Oe(e2, "iterate", p(e2) ? "length" : Ee), Reflect.ownKeys(e2);
  }
}
class Fe extends Le {
  constructor(e2 = false) {
    super(true, e2);
  }
  set(e2, t2) {
    return "production" !== process.env.NODE_ENV && ne(`Set operation on key "${String(t2)}" failed: target is readonly.`, e2), true;
  }
  deleteProperty(e2, t2) {
    return "production" !== process.env.NODE_ENV && ne(`Delete operation on key "${String(t2)}" failed: target is readonly.`, e2), true;
  }
}
const Re = new Pe(), $e = new Fe(), Be = new Pe(true), He = new Fe(true), je = (e2) => e2, Ke = (e2) => Reflect.getPrototypeOf(e2);
function ze(e2, t2, n2 = false, o2 = false) {
  const r2 = xt(e2 = e2.__v_raw), s2 = xt(t2);
  n2 || (T(t2, s2) && Oe(r2, "get", t2), Oe(r2, "get", s2));
  const { has: a2 } = Ke(r2), l2 = o2 ? je : n2 ? Et : St;
  return a2.call(r2, t2) ? l2(e2.get(t2)) : a2.call(r2, s2) ? l2(e2.get(s2)) : void (e2 !== r2 && e2.get(t2));
}
function Ze(e2, t2 = false) {
  const n2 = this.__v_raw, o2 = xt(n2), r2 = xt(e2);
  return t2 || (T(e2, r2) && Oe(o2, "has", e2), Oe(o2, "has", r2)), e2 === r2 ? n2.has(e2) : n2.has(e2) || n2.has(r2);
}
function Ue(e2, t2 = false) {
  return e2 = e2.__v_raw, !t2 && Oe(xt(e2), "iterate", Ee), Reflect.get(e2, "size", e2);
}
function We(e2) {
  e2 = xt(e2);
  const t2 = xt(this);
  return Ke(t2).has.call(t2, e2) || (t2.add(e2), Ne(t2, "add", e2, e2)), this;
}
function Ye(e2, t2) {
  t2 = xt(t2);
  const n2 = xt(this), { has: o2, get: r2 } = Ke(n2);
  let s2 = o2.call(n2, e2);
  s2 ? "production" !== process.env.NODE_ENV && dt(n2, o2, e2) : (e2 = xt(e2), s2 = o2.call(n2, e2));
  const a2 = r2.call(n2, e2);
  return n2.set(e2, t2), s2 ? T(t2, a2) && Ne(n2, "set", e2, t2, a2) : Ne(n2, "add", e2, t2), this;
}
function Ge(e2) {
  const t2 = xt(this), { has: n2, get: o2 } = Ke(t2);
  let r2 = n2.call(t2, e2);
  r2 ? "production" !== process.env.NODE_ENV && dt(t2, n2, e2) : (e2 = xt(e2), r2 = n2.call(t2, e2));
  const s2 = o2 ? o2.call(t2, e2) : void 0, a2 = t2.delete(e2);
  return r2 && Ne(t2, "delete", e2, void 0, s2), a2;
}
function qe() {
  const e2 = xt(this), t2 = 0 !== e2.size, n2 = "production" !== process.env.NODE_ENV ? f(e2) ? new Map(e2) : new Set(e2) : void 0, o2 = e2.clear();
  return t2 && Ne(e2, "clear", void 0, void 0, n2), o2;
}
function Xe(e2, t2) {
  return function(n2, o2) {
    const r2 = this, s2 = r2.__v_raw, a2 = xt(s2), l2 = t2 ? je : e2 ? Et : St;
    return !e2 && Oe(a2, "iterate", Ee), s2.forEach((e3, t3) => n2.call(o2, l2(e3), l2(t3), r2));
  };
}
function Je(e2, t2, n2) {
  return function(...o2) {
    const r2 = this.__v_raw, s2 = xt(r2), a2 = f(s2), l2 = "entries" === e2 || e2 === Symbol.iterator && a2, i2 = "keys" === e2 && a2, c2 = r2[e2](...o2), d2 = n2 ? je : t2 ? Et : St;
    return !t2 && Oe(s2, "iterate", i2 ? Ie : Ee), { next() {
      const { value: e3, done: t3 } = c2.next();
      return t3 ? { value: e3, done: t3 } : { value: l2 ? [d2(e3[0]), d2(e3[1])] : d2(e3), done: t3 };
    }, [Symbol.iterator]() {
      return this;
    } };
  };
}
function Qe(e2) {
  return function(...t2) {
    if ("production" !== process.env.NODE_ENV) {
      const n2 = t2[0] ? `on key "${t2[0]}" ` : "";
      ne(`${D(e2)} operation ${n2}failed: target is readonly.`, xt(this));
    }
    return "delete" !== e2 && ("clear" === e2 ? void 0 : this);
  };
}
function et() {
  const e2 = { get(e3) {
    return ze(this, e3);
  }, get size() {
    return Ue(this);
  }, has: Ze, add: We, set: Ye, delete: Ge, clear: qe, forEach: Xe(false, false) }, t2 = { get(e3) {
    return ze(this, e3, false, true);
  }, get size() {
    return Ue(this);
  }, has: Ze, add: We, set: Ye, delete: Ge, clear: qe, forEach: Xe(false, true) }, n2 = { get(e3) {
    return ze(this, e3, true);
  }, get size() {
    return Ue(this, true);
  }, has(e3) {
    return Ze.call(this, e3, true);
  }, add: Qe("add"), set: Qe("set"), delete: Qe("delete"), clear: Qe("clear"), forEach: Xe(true, false) }, o2 = { get(e3) {
    return ze(this, e3, true, true);
  }, get size() {
    return Ue(this, true);
  }, has(e3) {
    return Ze.call(this, e3, true);
  }, add: Qe("add"), set: Qe("set"), delete: Qe("delete"), clear: Qe("clear"), forEach: Xe(true, true) };
  return ["keys", "values", "entries", Symbol.iterator].forEach((r2) => {
    e2[r2] = Je(r2, false, false), n2[r2] = Je(r2, true, false), t2[r2] = Je(r2, false, true), o2[r2] = Je(r2, true, true);
  }), [e2, n2, t2, o2];
}
const [tt, nt, ot, rt] = et();
function st(e2, t2) {
  const n2 = t2 ? e2 ? rt : ot : e2 ? nt : tt;
  return (t3, o2, r2) => "__v_isReactive" === o2 ? !e2 : "__v_isReadonly" === o2 ? e2 : "__v_raw" === o2 ? t3 : Reflect.get(u(n2, o2) && o2 in t3 ? n2 : t3, o2, r2);
}
const at = { get: st(false, false) }, lt = { get: st(false, true) }, it = { get: st(true, false) }, ct = { get: st(true, true) };
function dt(e2, t2, n2) {
  const o2 = xt(n2);
  if (o2 !== n2 && t2.call(e2, o2)) {
    const t3 = C(e2);
    ne(`Reactive ${t3} contains both the raw and reactive versions of the same object${"Map" === t3 ? " as keys" : ""}, which can lead to inconsistencies. Avoid differentiating between the raw and reactive versions of an object and only use the reactive version if possible.`);
  }
}
const ut = /* @__PURE__ */ new WeakMap(), pt = /* @__PURE__ */ new WeakMap(), ft = /* @__PURE__ */ new WeakMap(), ht = /* @__PURE__ */ new WeakMap();
function vt(e2) {
  return wt(e2) ? e2 : yt(e2, false, Re, at, ut);
}
function mt(e2) {
  return yt(e2, true, $e, it, ft);
}
function gt(e2) {
  return yt(e2, true, He, ct, ht);
}
function yt(e2, t2, n2, o2, r2) {
  if (!y(e2))
    return "production" !== process.env.NODE_ENV && ne(`value cannot be made reactive: ${String(e2)}`), e2;
  if (e2.__v_raw && (!t2 || !e2.__v_isReactive))
    return e2;
  const s2 = r2.get(e2);
  if (s2)
    return s2;
  const a2 = (l2 = e2).__v_skip || !Object.isExtensible(l2) ? 0 : function(e3) {
    switch (e3) {
      case "Object":
      case "Array":
        return 1;
      case "Map":
      case "Set":
      case "WeakMap":
      case "WeakSet":
        return 2;
      default:
        return 0;
    }
  }(C(l2));
  var l2;
  if (0 === a2)
    return e2;
  const i2 = new Proxy(e2, 2 === a2 ? o2 : n2);
  return r2.set(e2, i2), i2;
}
function bt(e2) {
  return wt(e2) ? bt(e2.__v_raw) : !(!e2 || !e2.__v_isReactive);
}
function wt(e2) {
  return !(!e2 || !e2.__v_isReadonly);
}
function _t(e2) {
  return !(!e2 || !e2.__v_isShallow);
}
function Ct(e2) {
  return !!e2 && !!e2.__v_raw;
}
function xt(e2) {
  const t2 = e2 && e2.__v_raw;
  return t2 ? xt(t2) : e2;
}
function kt(e2) {
  return Object.isExtensible(e2) && P(e2, "__v_skip", true), e2;
}
const St = (e2) => y(e2) ? vt(e2) : e2, Et = (e2) => y(e2) ? mt(e2) : e2;
class It {
  constructor(e2, t2, n2, o2) {
    this.getter = e2, this._setter = t2, this.dep = void 0, this.__v_isRef = true, this.__v_isReadonly = false, this.effect = new ce(() => e2(this._value), () => Nt(this, 2 === this.effect._dirtyLevel ? 2 : 3)), this.effect.computed = this, this.effect.active = this._cacheable = !o2, this.__v_isReadonly = n2;
  }
  get value() {
    const e2 = xt(this);
    return e2._cacheable && !e2.effect.dirty || !T(e2._value, e2._value = e2.effect.run()) || Nt(e2, 4), Ot(e2), e2.effect._dirtyLevel >= 2 && ("production" !== process.env.NODE_ENV && this._warnRecursive && ne("Computed is still dirty after getter evaluation, likely because a computed is mutating its own dependency in its getter. State mutations in computed getters should be avoided.  Check the docs for more details: https://vuejs.org/guide/essentials/computed.html#getters-should-be-side-effect-free", "\n\ngetter: ", this.getter), Nt(e2, 2)), e2._value;
  }
  set value(e2) {
    this._setter(e2);
  }
  get _dirty() {
    return this.effect.dirty;
  }
  set _dirty(e2) {
    this.effect.dirty = e2;
  }
}
function Ot(e2) {
  var t2;
  he && re && (e2 = xt(e2), _e(re, null != (t2 = e2.dep) ? t2 : e2.dep = ke(() => e2.dep = void 0, e2 instanceof It ? e2 : void 0), "production" !== process.env.NODE_ENV ? { target: e2, type: "get", key: "value" } : void 0));
}
function Nt(e2, t2 = 4, n2) {
  const o2 = (e2 = xt(e2)).dep;
  o2 && xe(o2, t2, "production" !== process.env.NODE_ENV ? { target: e2, type: "set", key: "value", newValue: n2 } : void 0);
}
function Mt(e2) {
  return !(!e2 || true !== e2.__v_isRef);
}
function At(e2) {
  return Vt(e2, false);
}
function Dt(e2) {
  return Vt(e2, true);
}
function Vt(e2, t2) {
  return Mt(e2) ? e2 : new Tt(e2, t2);
}
class Tt {
  constructor(e2, t2) {
    this.__v_isShallow = t2, this.dep = void 0, this.__v_isRef = true, this._rawValue = t2 ? e2 : xt(e2), this._value = t2 ? e2 : St(e2);
  }
  get value() {
    return Ot(this), this._value;
  }
  set value(e2) {
    const t2 = this.__v_isShallow || _t(e2) || wt(e2);
    e2 = t2 ? e2 : xt(e2), T(e2, this._rawValue) && (this._rawValue = e2, this._value = t2 ? e2 : St(e2), Nt(this, 4, e2));
  }
}
function Lt(e2) {
  return Mt(e2) ? e2.value : e2;
}
const Pt = { get: (e2, t2, n2) => Lt(Reflect.get(e2, t2, n2)), set: (e2, t2, n2, o2) => {
  const r2 = e2[t2];
  return Mt(r2) && !Mt(n2) ? (r2.value = n2, true) : Reflect.set(e2, t2, n2, o2);
} };
function Ft(e2) {
  return bt(e2) ? e2 : new Proxy(e2, Pt);
}
class Rt {
  constructor(e2) {
    this.dep = void 0, this.__v_isRef = true;
    const { get: t2, set: n2 } = e2(() => Ot(this), () => Nt(this));
    this._get = t2, this._set = n2;
  }
  get value() {
    return this._get();
  }
  set value(e2) {
    this._set(e2);
  }
}
function $t(e2) {
  "production" === process.env.NODE_ENV || Ct(e2) || ne("toRefs() expects a reactive object but received a plain one.");
  const t2 = p(e2) ? new Array(e2.length) : {};
  for (const n2 in e2)
    t2[n2] = Kt(e2, n2);
  return t2;
}
class Bt {
  constructor(e2, t2, n2) {
    this._object = e2, this._key = t2, this._defaultValue = n2, this.__v_isRef = true;
  }
  get value() {
    const e2 = this._object[this._key];
    return void 0 === e2 ? this._defaultValue : e2;
  }
  set value(e2) {
    this._object[this._key] = e2;
  }
  get dep() {
    return function(e2, t2) {
      const n2 = Se.get(e2);
      return n2 && n2.get(t2);
    }(xt(this._object), this._key);
  }
}
class Ht {
  constructor(e2) {
    this._getter = e2, this.__v_isRef = true, this.__v_isReadonly = true;
  }
  get value() {
    return this._getter();
  }
}
function jt(e2, t2, n2) {
  return Mt(e2) ? e2 : v(e2) ? new Ht(e2) : y(e2) && arguments.length > 1 ? Kt(e2, t2, n2) : At(e2);
}
function Kt(e2, t2, n2) {
  const o2 = e2[t2];
  return Mt(o2) ? o2 : new Bt(e2, t2, n2);
}
/**
* @vue/runtime-core v3.4.26
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/
const zt = [];
function Zt(e2) {
  zt.push(e2);
}
function Ut() {
  zt.pop();
}
function Wt(e2, ...t2) {
  ge();
  const n2 = zt.length ? zt[zt.length - 1].component : null, o2 = n2 && n2.appContext.config.warnHandler, r2 = function() {
    let e3 = zt[zt.length - 1];
    if (!e3)
      return [];
    const t3 = [];
    for (; e3; ) {
      const n3 = t3[0];
      n3 && n3.vnode === e3 ? n3.recurseCount++ : t3.push({ vnode: e3, recurseCount: 0 });
      const o3 = e3.component && e3.component.parent;
      e3 = o3 && o3.vnode;
    }
    return t3;
  }();
  if (o2)
    Xt(o2, n2, 11, [e2 + t2.map((e3) => {
      var t3, n3;
      return null != (n3 = null == (t3 = e3.toString) ? void 0 : t3.call(e3)) ? n3 : JSON.stringify(e3);
    }).join(""), n2 && n2.proxy, r2.map(({ vnode: e3 }) => `at <${va(n2, e3.type)}>`).join("\n"), r2]);
  else {
    const n3 = [`[Vue warn]: ${e2}`, ...t2];
    r2.length && n3.push("\n", ...function(e3) {
      const t3 = [];
      return e3.forEach((e4, n4) => {
        t3.push(...0 === n4 ? [] : ["\n"], ...function({ vnode: e5, recurseCount: t4 }) {
          const n5 = t4 > 0 ? `... (${t4} recursive calls)` : "", o3 = !!e5.component && null == e5.component.parent, r3 = ` at <${va(e5.component, e5.type, o3)}`, s2 = ">" + n5;
          return e5.props ? [r3, ...Yt(e5.props), s2] : [r3 + s2];
        }(e4));
      }), t3;
    }(r2)), console.warn(...n3);
  }
  ye();
}
function Yt(e2) {
  const t2 = [], n2 = Object.keys(e2);
  return n2.slice(0, 3).forEach((n3) => {
    t2.push(...Gt(n3, e2[n3]));
  }), n2.length > 3 && t2.push(" ..."), t2;
}
function Gt(e2, t2, n2) {
  return m(t2) ? (t2 = JSON.stringify(t2), n2 ? t2 : [`${e2}=${t2}`]) : "number" == typeof t2 || "boolean" == typeof t2 || null == t2 ? n2 ? t2 : [`${e2}=${t2}`] : Mt(t2) ? (t2 = Gt(e2, xt(t2.value), true), n2 ? t2 : [`${e2}=Ref<`, t2, ">"]) : v(t2) ? [`${e2}=fn${t2.name ? `<${t2.name}>` : ""}`] : (t2 = xt(t2), n2 ? t2 : [`${e2}=`, t2]);
}
const qt = { sp: "serverPrefetch hook", bc: "beforeCreate hook", c: "created hook", bm: "beforeMount hook", m: "mounted hook", bu: "beforeUpdate hook", u: "updated", bum: "beforeUnmount hook", um: "unmounted hook", a: "activated hook", da: "deactivated hook", ec: "errorCaptured hook", rtc: "renderTracked hook", rtg: "renderTriggered hook", 0: "setup function", 1: "render function", 2: "watcher getter", 3: "watcher callback", 4: "watcher cleanup function", 5: "native event handler", 6: "component event handler", 7: "vnode hook", 8: "directive hook", 9: "transition hook", 10: "app errorHandler", 11: "app warnHandler", 12: "ref function", 13: "async component loader", 14: "scheduler flush. This is likely a Vue internals bug. Please open an issue at https://github.com/vuejs/core ." };
function Xt(e2, t2, n2, o2) {
  try {
    return o2 ? e2(...o2) : e2();
  } catch (e3) {
    Qt(e3, t2, n2);
  }
}
function Jt(e2, t2, n2, o2) {
  if (v(e2)) {
    const r2 = Xt(e2, t2, n2, o2);
    return r2 && b(r2) && r2.catch((e3) => {
      Qt(e3, t2, n2);
    }), r2;
  }
  if (p(e2)) {
    const r2 = [];
    for (let s2 = 0; s2 < e2.length; s2++)
      r2.push(Jt(e2[s2], t2, n2, o2));
    return r2;
  }
  "production" !== process.env.NODE_ENV && Wt("Invalid value type passed to callWithAsyncErrorHandling(): " + typeof e2);
}
function Qt(e2, t2, n2, o2 = true) {
  const r2 = t2 ? t2.vnode : null;
  if (t2) {
    let o3 = t2.parent;
    const r3 = t2.proxy, s2 = "production" !== process.env.NODE_ENV ? qt[n2] : `https://vuejs.org/error-reference/#runtime-${n2}`;
    for (; o3; ) {
      const t3 = o3.ec;
      if (t3) {
        for (let n3 = 0; n3 < t3.length; n3++)
          if (false === t3[n3](e2, r3, s2))
            return;
      }
      o3 = o3.parent;
    }
    const a2 = t2.appContext.config.errorHandler;
    if (a2)
      return ge(), Xt(a2, null, 10, [e2, r3, s2]), void ye();
  }
  !function(e3, t3, n3, o3 = true) {
    if ("production" !== process.env.NODE_ENV) {
      const r3 = qt[t3];
      if (n3 && Zt(n3), Wt("Unhandled error" + (r3 ? ` during execution of ${r3}` : "")), n3 && Ut(), o3)
        throw e3;
      console.error(e3);
    } else
      console.error(e3);
  }(e2, n2, r2, o2);
}
let en = false, tn = false;
const nn = [];
let on = 0;
const rn = [];
let sn = null, an = 0;
const ln = Promise.resolve();
let cn = null;
const dn = 100;
function un(e2) {
  const t2 = cn || ln;
  return e2 ? t2.then(this ? e2.bind(this) : e2) : t2;
}
function pn(e2) {
  nn.length && nn.includes(e2, en && e2.allowRecurse ? on + 1 : on) || (null == e2.id ? nn.push(e2) : nn.splice(function(e3) {
    let t2 = on + 1, n2 = nn.length;
    for (; t2 < n2; ) {
      const o2 = t2 + n2 >>> 1, r2 = nn[o2], s2 = gn(r2);
      s2 < e3 || s2 === e3 && r2.pre ? t2 = o2 + 1 : n2 = o2;
    }
    return t2;
  }(e2.id), 0, e2), fn());
}
function fn() {
  en || tn || (tn = true, cn = ln.then(bn));
}
function hn(e2) {
  p(e2) ? rn.push(...e2) : sn && sn.includes(e2, e2.allowRecurse ? an + 1 : an) || rn.push(e2), fn();
}
function vn(e2, t2, n2 = en ? on + 1 : 0) {
  for ("production" !== process.env.NODE_ENV && (t2 = t2 || /* @__PURE__ */ new Map()); n2 < nn.length; n2++) {
    const o2 = nn[n2];
    if (o2 && o2.pre) {
      if (e2 && o2.id !== e2.uid)
        continue;
      if ("production" !== process.env.NODE_ENV && wn(t2, o2))
        continue;
      nn.splice(n2, 1), n2--, o2();
    }
  }
}
function mn(e2) {
  if (rn.length) {
    const t2 = [...new Set(rn)].sort((e3, t3) => gn(e3) - gn(t3));
    if (rn.length = 0, sn)
      return void sn.push(...t2);
    for (sn = t2, "production" !== process.env.NODE_ENV && (e2 = e2 || /* @__PURE__ */ new Map()), an = 0; an < sn.length; an++)
      "production" !== process.env.NODE_ENV && wn(e2, sn[an]) || sn[an]();
    sn = null, an = 0;
  }
}
const gn = (e2) => null == e2.id ? 1 / 0 : e2.id, yn = (e2, t2) => {
  const n2 = gn(e2) - gn(t2);
  if (0 === n2) {
    if (e2.pre && !t2.pre)
      return -1;
    if (t2.pre && !e2.pre)
      return 1;
  }
  return n2;
};
function bn(e2) {
  tn = false, en = true, "production" !== process.env.NODE_ENV && (e2 = e2 || /* @__PURE__ */ new Map()), nn.sort(yn);
  const t2 = "production" !== process.env.NODE_ENV ? (t3) => wn(e2, t3) : r;
  try {
    for (on = 0; on < nn.length; on++) {
      const e3 = nn[on];
      if (e3 && false !== e3.active) {
        if ("production" !== process.env.NODE_ENV && t2(e3))
          continue;
        Xt(e3, null, 14);
      }
    }
  } finally {
    on = 0, nn.length = 0, mn(e2), en = false, cn = null, (nn.length || rn.length) && bn(e2);
  }
}
function wn(e2, t2) {
  if (e2.has(t2)) {
    const n2 = e2.get(t2);
    if (n2 > dn) {
      const e3 = t2.ownerInstance, n3 = e3 && ha(e3.type);
      return Qt(`Maximum recursive updates exceeded${n3 ? ` in component <${n3}>` : ""}. This means you have a reactive effect that is mutating its own dependencies and thus recursively triggering itself. Possible sources include component template, render function, updated hook or watcher source function.`, null, 10), true;
    }
    e2.set(t2, n2 + 1);
  } else
    e2.set(t2, 1);
}
let _n = false;
const Cn = /* @__PURE__ */ new Set();
"production" !== process.env.NODE_ENV && (B().__VUE_HMR_RUNTIME__ = { createRecord: In(kn), rerender: In(function(e2, t2) {
  const n2 = xn.get(e2);
  if (!n2)
    return;
  n2.initialDef.render = t2, [...n2.instances].forEach((e3) => {
    t2 && (e3.render = t2, Sn(e3.type).render = t2), e3.renderCache = [], _n = true, e3.effect.dirty = true, e3.update(), _n = false;
  });
}), reload: In(function(e2, t2) {
  const n2 = xn.get(e2);
  if (!n2)
    return;
  t2 = Sn(t2), En(n2.initialDef, t2);
  const o2 = [...n2.instances];
  for (const e3 of o2) {
    const o3 = Sn(e3.type);
    Cn.has(o3) || (o3 !== n2.initialDef && En(o3, t2), Cn.add(o3)), e3.appContext.propsCache.delete(e3.type), e3.appContext.emitsCache.delete(e3.type), e3.appContext.optionsCache.delete(e3.type), e3.ceReload ? (Cn.add(o3), e3.ceReload(t2.styles), Cn.delete(o3)) : e3.parent ? (e3.parent.effect.dirty = true, pn(e3.parent.update)) : e3.appContext.reload ? e3.appContext.reload() : "undefined" != typeof window ? window.location.reload() : console.warn("[HMR] Root or manually mounted instance modified. Full reload required.");
  }
  hn(() => {
    for (const e3 of o2)
      Cn.delete(Sn(e3.type));
  });
}) });
const xn = /* @__PURE__ */ new Map();
function kn(e2, t2) {
  return !xn.has(e2) && (xn.set(e2, { initialDef: Sn(t2), instances: /* @__PURE__ */ new Set() }), true);
}
function Sn(e2) {
  return ma(e2) ? e2.__vccOpts : e2;
}
function En(e2, t2) {
  i(e2, t2);
  for (const n2 in e2)
    "__file" === n2 || n2 in t2 || delete e2[n2];
}
function In(e2) {
  return (t2, n2) => {
    try {
      return e2(t2, n2);
    } catch (e3) {
      console.error(e3), console.warn("[HMR] Something went wrong during Vue component hot-reload. Full reload required.");
    }
  };
}
let On, Nn = [], Mn = false;
function An(e2, ...t2) {
  On ? On.emit(e2, ...t2) : Mn || Nn.push({ event: e2, args: t2 });
}
function Dn(e2, t2) {
  var n2, o2;
  if (On = e2, On)
    On.enabled = true, Nn.forEach(({ event: e3, args: t3 }) => On.emit(e3, ...t3)), Nn = [];
  else if ("undefined" != typeof window && window.HTMLElement && !(null == (o2 = null == (n2 = window.navigator) ? void 0 : n2.userAgent) ? void 0 : o2.includes("jsdom"))) {
    (t2.__VUE_DEVTOOLS_HOOK_REPLAY__ = t2.__VUE_DEVTOOLS_HOOK_REPLAY__ || []).push((e3) => {
      Dn(e3, t2);
    }), setTimeout(() => {
      On || (t2.__VUE_DEVTOOLS_HOOK_REPLAY__ = null, Mn = true, Nn = []);
    }, 3e3);
  } else
    Mn = true, Nn = [];
}
const Vn = Fn("component:added"), Tn = Fn("component:updated"), Ln = Fn("component:removed"), Pn = (e2) => {
  On && "function" == typeof On.cleanupBuffer && !On.cleanupBuffer(e2) && Ln(e2);
};
/*! #__NO_SIDE_EFFECTS__ */
function Fn(e2) {
  return (t2) => {
    An(e2, t2.appContext.app, t2.uid, t2.parent ? t2.parent.uid : void 0, t2);
  };
}
const Rn = Bn("perf:start"), $n = Bn("perf:end");
function Bn(e2) {
  return (t2, n2, o2) => {
    An(e2, t2.appContext.app, t2.uid, t2, n2, o2);
  };
}
function Hn(e2, t2, ...o2) {
  if (e2.isUnmounted)
    return;
  const r2 = e2.vnode.props || n;
  if ("production" !== process.env.NODE_ENV) {
    const { emitsOptions: n2, propsOptions: [r3] } = e2;
    if (n2)
      if (t2 in n2) {
        const e3 = n2[t2];
        if (v(e3)) {
          e3(...o2) || Wt(`Invalid event arguments: event validation failed for event "${t2}".`);
        }
      } else
        r3 && V(t2) in r3 || Wt(`Component emitted event "${t2}" but it is neither declared in the emits option nor as an "${V(t2)}" prop.`);
  }
  let s2 = o2;
  const a2 = t2.startsWith("update:"), l2 = a2 && t2.slice(7);
  if (l2 && l2 in r2) {
    const e3 = `${"modelValue" === l2 ? "model" : l2}Modifiers`, { number: t3, trim: a3 } = r2[e3] || n;
    a3 && (s2 = o2.map((e4) => m(e4) ? e4.trim() : e4)), t3 && (s2 = o2.map(F));
  }
  if ("production" !== process.env.NODE_ENV && function(e3, t3, n2) {
    An("component:emit", e3.appContext.app, e3, t3, n2);
  }(e2, t2, s2), "production" !== process.env.NODE_ENV) {
    const n2 = t2.toLowerCase();
    n2 !== t2 && r2[V(n2)] && Wt(`Event "${n2}" is emitted in component ${va(e2, e2.type)} but the handler is registered for "${t2}". Note that HTML attributes are case-insensitive and you cannot use v-on to listen to camelCase events when using in-DOM templates. You should probably use "${A(t2)}" instead of "${t2}".`);
  }
  let i2, c2 = r2[i2 = V(t2)] || r2[i2 = V(N(t2))];
  !c2 && a2 && (c2 = r2[i2 = V(A(t2))]), c2 && Jt(c2, e2, 6, s2);
  const d2 = r2[i2 + "Once"];
  if (d2) {
    if (e2.emitted) {
      if (e2.emitted[i2])
        return;
    } else
      e2.emitted = {};
    e2.emitted[i2] = true, Jt(d2, e2, 6, s2);
  }
}
function jn(e2, t2, n2 = false) {
  const o2 = t2.emitsCache, r2 = o2.get(e2);
  if (void 0 !== r2)
    return r2;
  const s2 = e2.emits;
  let a2 = {}, l2 = false;
  if (!v(e2)) {
    const o3 = (e3) => {
      const n3 = jn(e3, t2, true);
      n3 && (l2 = true, i(a2, n3));
    };
    !n2 && t2.mixins.length && t2.mixins.forEach(o3), e2.extends && o3(e2.extends), e2.mixins && e2.mixins.forEach(o3);
  }
  return s2 || l2 ? (p(s2) ? s2.forEach((e3) => a2[e3] = null) : i(a2, s2), y(e2) && o2.set(e2, a2), a2) : (y(e2) && o2.set(e2, null), null);
}
function Kn(e2, t2) {
  return !(!e2 || !a(t2)) && (t2 = t2.slice(2).replace(/Once$/, ""), u(e2, t2[0].toLowerCase() + t2.slice(1)) || u(e2, A(t2)) || u(e2, t2));
}
let zn = null, Zn = null;
function Un(e2) {
  const t2 = zn;
  return zn = e2, Zn = e2 && e2.type.__scopeId || null, t2;
}
function Wn(e2, t2 = zn, n2) {
  if (!t2)
    return e2;
  if (e2._n)
    return e2;
  const o2 = (...n3) => {
    o2._d && Is(-1);
    const r2 = Un(t2);
    let s2;
    try {
      s2 = e2(...n3);
    } finally {
      Un(r2), o2._d && Is(1);
    }
    return "production" !== process.env.NODE_ENV && Tn(t2), s2;
  };
  return o2._n = true, o2._c = true, o2._d = true, o2;
}
let Yn = false;
function Gn() {
  Yn = true;
}
function qn(e2) {
  const { type: t2, vnode: n2, proxy: o2, withProxy: r2, propsOptions: [s2], slots: i2, attrs: c2, emit: d2, render: u2, renderCache: p2, props: f2, data: h2, setupState: v2, ctx: m2, inheritAttrs: g2 } = e2, y2 = Un(e2);
  let b2, w2;
  "production" !== process.env.NODE_ENV && (Yn = false);
  try {
    if (4 & n2.shapeFlag) {
      const e3 = r2 || o2, t3 = "production" !== process.env.NODE_ENV && v2.__isScriptSetup ? new Proxy(e3, { get: (e4, t4, n3) => (Wt(`Property '${String(t4)}' was accessed via 'this'. Avoid using 'this' in templates.`), Reflect.get(e4, t4, n3)) }) : e3;
      b2 = Ks(u2.call(t3, e3, p2, "production" !== process.env.NODE_ENV ? gt(f2) : f2, v2, h2, m2)), w2 = c2;
    } else {
      const e3 = t2;
      "production" !== process.env.NODE_ENV && c2 === f2 && Gn(), b2 = Ks(e3.length > 1 ? e3("production" !== process.env.NODE_ENV ? gt(f2) : f2, "production" !== process.env.NODE_ENV ? { get attrs() {
        return Gn(), gt(c2);
      }, slots: i2, emit: d2 } : { attrs: c2, slots: i2, emit: d2 }) : e3("production" !== process.env.NODE_ENV ? gt(f2) : f2, null)), w2 = t2.props ? c2 : Qn(c2);
    }
  } catch (t3) {
    xs.length = 0, Qt(t3, e2, 1), b2 = Ps(_s);
  }
  let _2, C2 = b2;
  if ("production" !== process.env.NODE_ENV && b2.patchFlag > 0 && 2048 & b2.patchFlag && ([C2, _2] = Xn(b2)), w2 && false !== g2) {
    const e3 = Object.keys(w2), { shapeFlag: t3 } = C2;
    if (e3.length) {
      if (7 & t3)
        s2 && e3.some(l) && (w2 = eo(w2, s2)), C2 = $s(C2, w2, false, true);
      else if ("production" !== process.env.NODE_ENV && !Yn && C2.type !== _s) {
        const e4 = Object.keys(c2), t4 = [], n3 = [];
        for (let o3 = 0, r3 = e4.length; o3 < r3; o3++) {
          const r4 = e4[o3];
          a(r4) ? l(r4) || t4.push(r4[2].toLowerCase() + r4.slice(3)) : n3.push(r4);
        }
        n3.length && Wt(`Extraneous non-props attributes (${n3.join(", ")}) were passed to component but could not be automatically inherited because component renders fragment or text root nodes.`), t4.length && Wt(`Extraneous non-emits event listeners (${t4.join(", ")}) were passed to component but could not be automatically inherited because component renders fragment or text root nodes. If the listener is intended to be a component custom event listener only, declare it using the "emits" option.`);
      }
    }
  }
  return n2.dirs && ("production" === process.env.NODE_ENV || to(C2) || Wt("Runtime directive used on component with non-element root node. The directives will not function as intended."), C2 = $s(C2, null, false, true), C2.dirs = C2.dirs ? C2.dirs.concat(n2.dirs) : n2.dirs), n2.transition && ("production" === process.env.NODE_ENV || to(C2) || Wt("Component inside <Transition> renders non-element root node that cannot be animated."), C2.transition = n2.transition), "production" !== process.env.NODE_ENV && _2 ? _2(C2) : b2 = C2, Un(y2), b2;
}
const Xn = (e2) => {
  const t2 = e2.children, n2 = e2.dynamicChildren, o2 = Jn(t2, false);
  if (!o2)
    return [e2, void 0];
  if ("production" !== process.env.NODE_ENV && o2.patchFlag > 0 && 2048 & o2.patchFlag)
    return Xn(o2);
  const r2 = t2.indexOf(o2), s2 = n2 ? n2.indexOf(o2) : -1;
  return [Ks(o2), (o3) => {
    t2[r2] = o3, n2 && (s2 > -1 ? n2[s2] = o3 : o3.patchFlag > 0 && (e2.dynamicChildren = [...n2, o3]));
  }];
};
function Jn(e2, t2 = true) {
  let n2;
  for (let o2 = 0; o2 < e2.length; o2++) {
    const r2 = e2[o2];
    if (!As(r2))
      return;
    if (r2.type !== _s || "v-if" === r2.children) {
      if (n2)
        return;
      if (n2 = r2, "production" !== process.env.NODE_ENV && t2 && n2.patchFlag > 0 && 2048 & n2.patchFlag)
        return Jn(n2.children);
    }
  }
  return n2;
}
const Qn = (e2) => {
  let t2;
  for (const n2 in e2)
    ("class" === n2 || "style" === n2 || a(n2)) && ((t2 || (t2 = {}))[n2] = e2[n2]);
  return t2;
}, eo = (e2, t2) => {
  const n2 = {};
  for (const o2 in e2)
    l(o2) && o2.slice(9) in t2 || (n2[o2] = e2[o2]);
  return n2;
}, to = (e2) => 7 & e2.shapeFlag || e2.type === _s;
function no(e2, t2, n2) {
  const o2 = Object.keys(t2);
  if (o2.length !== Object.keys(e2).length)
    return true;
  for (let r2 = 0; r2 < o2.length; r2++) {
    const s2 = o2[r2];
    if (t2[s2] !== e2[s2] && !Kn(n2, s2))
      return true;
  }
  return false;
}
const oo = "components";
function ro(e2, t2) {
  return lo(oo, e2, true, t2) || e2;
}
const so = Symbol.for("v-ndc");
function ao(e2) {
  return m(e2) ? lo(oo, e2, false) || e2 : e2 || so;
}
function lo(e2, t2, n2 = true, o2 = false) {
  const r2 = zn || qs;
  if (r2) {
    const s2 = r2.type;
    if (e2 === oo) {
      const e3 = ha(s2, false);
      if (e3 && (e3 === t2 || e3 === N(t2) || e3 === D(N(t2))))
        return s2;
    }
    const a2 = io(r2[e2] || s2[e2], t2) || io(r2.appContext[e2], t2);
    if (!a2 && o2)
      return s2;
    if ("production" !== process.env.NODE_ENV && n2 && !a2) {
      const n3 = e2 === oo ? "\nIf this is a native custom element, make sure to exclude it from component resolution via compilerOptions.isCustomElement." : "";
      Wt(`Failed to resolve ${e2.slice(0, -1)}: ${t2}${n3}`);
    }
    return a2;
  }
  "production" !== process.env.NODE_ENV && Wt(`resolve${D(e2.slice(0, -1))} can only be used in render() or setup().`);
}
function io(e2, t2) {
  return e2 && (e2[t2] || e2[N(t2)] || e2[D(N(t2))]);
}
const co = Symbol.for("v-scx"), uo = () => {
  {
    const e2 = Or(co);
    return e2 || "production" !== process.env.NODE_ENV && Wt("Server rendering context not provided. Make sure to only call useSSRContext() conditionally in the server build."), e2;
  }
};
function po(e2, t2) {
  return vo(e2, null, t2);
}
const fo = {};
function ho(e2, t2, n2) {
  return "production" === process.env.NODE_ENV || v(t2) || Wt("`watch(fn, options?)` signature has been moved to a separate API. Use `watchEffect(fn, options?)` instead. `watch` now only supports `watch(source, cb, options?) signature."), vo(e2, t2, n2);
}
function vo(e2, t2, { immediate: o2, deep: s2, flush: a2, once: l2, onTrack: i2, onTrigger: d2 } = n) {
  if (t2 && l2) {
    const e3 = t2;
    t2 = (...t3) => {
      e3(...t3), I2();
    };
  }
  "production" !== process.env.NODE_ENV && void 0 !== s2 && "number" == typeof s2 && Wt('watch() "deep" option with number value will be used as watch depth in future versions. Please use a boolean instead to avoid potential breakage.'), "production" === process.env.NODE_ENV || t2 || (void 0 !== o2 && Wt('watch() "immediate" option is only respected when using the watch(source, callback, options?) signature.'), void 0 !== s2 && Wt('watch() "deep" option is only respected when using the watch(source, callback, options?) signature.'), void 0 !== l2 && Wt('watch() "once" option is only respected when using the watch(source, callback, options?) signature.'));
  const u2 = (e3) => {
    Wt("Invalid watch source: ", e3, "A watch source can only be a getter/effect function, a ref, a reactive object, or an array of these types.");
  }, f2 = qs, h2 = (e3) => true === s2 ? e3 : yo(e3, false === s2 ? 1 : void 0);
  let m2, g2, y2 = false, b2 = false;
  if (Mt(e2) ? (m2 = () => e2.value, y2 = _t(e2)) : bt(e2) ? (m2 = () => h2(e2), y2 = true) : p(e2) ? (b2 = true, y2 = e2.some((e3) => bt(e3) || _t(e3)), m2 = () => e2.map((e3) => Mt(e3) ? e3.value : bt(e3) ? h2(e3) : v(e3) ? Xt(e3, f2, 2) : void ("production" !== process.env.NODE_ENV && u2(e3)))) : v(e2) ? m2 = t2 ? () => Xt(e2, f2, 2) : () => (g2 && g2(), Jt(e2, f2, 3, [_2])) : (m2 = r, "production" !== process.env.NODE_ENV && u2(e2)), t2 && s2) {
    const e3 = m2;
    m2 = () => yo(e3());
  }
  let w2, _2 = (e3) => {
    g2 = S2.onStop = () => {
      Xt(e3, f2, 4), g2 = S2.onStop = void 0;
    };
  };
  if (aa) {
    if (_2 = r, t2 ? o2 && Jt(t2, f2, 3, [m2(), b2 ? [] : void 0, _2]) : m2(), "sync" !== a2)
      return r;
    {
      const e3 = uo();
      w2 = e3.__watcherHandles || (e3.__watcherHandles = []);
    }
  }
  let C2 = b2 ? new Array(e2.length).fill(fo) : fo;
  const x2 = () => {
    if (S2.active && S2.dirty)
      if (t2) {
        const e3 = S2.run();
        (s2 || y2 || (b2 ? e3.some((e4, t3) => T(e4, C2[t3])) : T(e3, C2))) && (g2 && g2(), Jt(t2, f2, 3, [e3, C2 === fo ? void 0 : b2 && C2[0] === fo ? [] : C2, _2]), C2 = e3);
      } else
        S2.run();
  };
  let k2;
  x2.allowRecurse = !!t2, "sync" === a2 ? k2 = x2 : "post" === a2 ? k2 = () => ss(x2, f2 && f2.suspense) : (x2.pre = true, f2 && (x2.id = f2.uid), k2 = () => pn(x2));
  const S2 = new ce(m2, r, k2), E2 = le(), I2 = () => {
    S2.stop(), E2 && c(E2.effects, S2);
  };
  return "production" !== process.env.NODE_ENV && (S2.onTrack = i2, S2.onTrigger = d2), t2 ? o2 ? x2() : C2 = S2.run() : "post" === a2 ? ss(S2.run.bind(S2), f2 && f2.suspense) : S2.run(), w2 && w2.push(I2), I2;
}
function mo(e2, t2, n2) {
  const o2 = this.proxy, r2 = m(e2) ? e2.includes(".") ? go(o2, e2) : () => o2[e2] : e2.bind(o2, o2);
  let s2;
  v(t2) ? s2 = t2 : (s2 = t2.handler, n2 = t2);
  const a2 = ea(this), l2 = vo(r2, s2.bind(o2), n2);
  return a2(), l2;
}
function go(e2, t2) {
  const n2 = t2.split(".");
  return () => {
    let t3 = e2;
    for (let e3 = 0; e3 < n2.length && t3; e3++)
      t3 = t3[n2[e3]];
    return t3;
  };
}
function yo(e2, t2 = 1 / 0, n2) {
  if (t2 <= 0 || !y(e2) || e2.__v_skip)
    return e2;
  if ((n2 = n2 || /* @__PURE__ */ new Set()).has(e2))
    return e2;
  if (n2.add(e2), t2--, Mt(e2))
    yo(e2.value, t2, n2);
  else if (p(e2))
    for (let o2 = 0; o2 < e2.length; o2++)
      yo(e2[o2], t2, n2);
  else if (h(e2) || f(e2))
    e2.forEach((e3) => {
      yo(e3, t2, n2);
    });
  else if (x(e2))
    for (const o2 in e2)
      yo(e2[o2], t2, n2);
  return e2;
}
function bo(e2) {
  E(e2) && Wt("Do not use built-in directive ids as custom directive id: " + e2);
}
function wo(e2, t2) {
  if (null === zn)
    return "production" !== process.env.NODE_ENV && Wt("withDirectives can only be used inside render functions."), e2;
  const o2 = ua(zn) || zn.proxy, r2 = e2.dirs || (e2.dirs = []);
  for (let e3 = 0; e3 < t2.length; e3++) {
    let [s2, a2, l2, i2 = n] = t2[e3];
    s2 && (v(s2) && (s2 = { mounted: s2, updated: s2 }), s2.deep && yo(a2), r2.push({ dir: s2, instance: o2, value: a2, oldValue: void 0, arg: l2, modifiers: i2 }));
  }
  return e2;
}
function _o(e2, t2, n2, o2) {
  const r2 = e2.dirs, s2 = t2 && t2.dirs;
  for (let a2 = 0; a2 < r2.length; a2++) {
    const l2 = r2[a2];
    s2 && (l2.oldValue = s2[a2].value);
    let i2 = l2.dir[o2];
    i2 && (ge(), Jt(i2, n2, 8, [e2.el, l2, e2, t2]), ye());
  }
}
const Co = Symbol("_leaveCb"), xo = Symbol("_enterCb");
function ko() {
  const e2 = { isMounted: false, isLeaving: false, isUnmounting: false, leavingVNodes: /* @__PURE__ */ new Map() };
  return Zo(() => {
    e2.isMounted = true;
  }), Yo(() => {
    e2.isUnmounting = true;
  }), e2;
}
const So = [Function, Array], Eo = { mode: String, appear: Boolean, persisted: Boolean, onBeforeEnter: So, onEnter: So, onAfterEnter: So, onEnterCancelled: So, onBeforeLeave: So, onLeave: So, onAfterLeave: So, onLeaveCancelled: So, onBeforeAppear: So, onAppear: So, onAfterAppear: So, onAppearCancelled: So }, Io = { name: "BaseTransition", props: Eo, setup(e2, { slots: t2 }) {
  const n2 = Xs(), o2 = ko();
  return () => {
    const r2 = t2.default && To(t2.default(), true);
    if (!r2 || !r2.length)
      return;
    let s2 = r2[0];
    if (r2.length > 1) {
      let e3 = false;
      for (const t3 of r2)
        if (t3.type !== _s) {
          if ("production" !== process.env.NODE_ENV && e3) {
            Wt("<transition> can only be used on a single element or component. Use <transition-group> for lists.");
            break;
          }
          if (s2 = t3, e3 = true, "production" === process.env.NODE_ENV)
            break;
        }
    }
    const a2 = xt(e2), { mode: l2 } = a2;
    if ("production" !== process.env.NODE_ENV && l2 && "in-out" !== l2 && "out-in" !== l2 && "default" !== l2 && Wt(`invalid <transition> mode: ${l2}`), o2.isLeaving)
      return Ao(s2);
    const i2 = Do(s2);
    if (!i2)
      return Ao(s2);
    const c2 = Mo(i2, a2, o2, n2);
    Vo(i2, c2);
    const d2 = n2.subTree, u2 = d2 && Do(d2);
    if (u2 && u2.type !== _s && !Ds(i2, u2)) {
      const e3 = Mo(u2, a2, o2, n2);
      if (Vo(u2, e3), "out-in" === l2 && i2.type !== _s)
        return o2.isLeaving = true, e3.afterLeave = () => {
          o2.isLeaving = false, false !== n2.update.active && (n2.effect.dirty = true, n2.update());
        }, Ao(s2);
      "in-out" === l2 && i2.type !== _s && (e3.delayLeave = (e4, t3, n3) => {
        No(o2, u2)[String(u2.key)] = u2, e4[Co] = () => {
          t3(), e4[Co] = void 0, delete c2.delayedLeave;
        }, c2.delayedLeave = n3;
      });
    }
    return s2;
  };
} }, Oo = Io;
function No(e2, t2) {
  const { leavingVNodes: n2 } = e2;
  let o2 = n2.get(t2.type);
  return o2 || (o2 = /* @__PURE__ */ Object.create(null), n2.set(t2.type, o2)), o2;
}
function Mo(e2, t2, n2, o2) {
  const { appear: r2, mode: s2, persisted: a2 = false, onBeforeEnter: l2, onEnter: i2, onAfterEnter: c2, onEnterCancelled: d2, onBeforeLeave: u2, onLeave: f2, onAfterLeave: h2, onLeaveCancelled: v2, onBeforeAppear: m2, onAppear: g2, onAfterAppear: y2, onAppearCancelled: b2 } = t2, w2 = String(e2.key), _2 = No(n2, e2), C2 = (e3, t3) => {
    e3 && Jt(e3, o2, 9, t3);
  }, x2 = (e3, t3) => {
    const n3 = t3[1];
    C2(e3, t3), p(e3) ? e3.every((e4) => e4.length <= 1) && n3() : e3.length <= 1 && n3();
  }, k2 = { mode: s2, persisted: a2, beforeEnter(t3) {
    let o3 = l2;
    if (!n2.isMounted) {
      if (!r2)
        return;
      o3 = m2 || l2;
    }
    t3[Co] && t3[Co](true);
    const s3 = _2[w2];
    s3 && Ds(e2, s3) && s3.el[Co] && s3.el[Co](), C2(o3, [t3]);
  }, enter(e3) {
    let t3 = i2, o3 = c2, s3 = d2;
    if (!n2.isMounted) {
      if (!r2)
        return;
      t3 = g2 || i2, o3 = y2 || c2, s3 = b2 || d2;
    }
    let a3 = false;
    const l3 = e3[xo] = (t4) => {
      a3 || (a3 = true, C2(t4 ? s3 : o3, [e3]), k2.delayedLeave && k2.delayedLeave(), e3[xo] = void 0);
    };
    t3 ? x2(t3, [e3, l3]) : l3();
  }, leave(t3, o3) {
    const r3 = String(e2.key);
    if (t3[xo] && t3[xo](true), n2.isUnmounting)
      return o3();
    C2(u2, [t3]);
    let s3 = false;
    const a3 = t3[Co] = (n3) => {
      s3 || (s3 = true, o3(), C2(n3 ? v2 : h2, [t3]), t3[Co] = void 0, _2[r3] === e2 && delete _2[r3]);
    };
    _2[r3] = e2, f2 ? x2(f2, [t3, a3]) : a3();
  }, clone: (e3) => Mo(e3, t2, n2, o2) };
  return k2;
}
function Ao(e2) {
  if (Fo(e2))
    return (e2 = $s(e2)).children = null, e2;
}
function Do(e2) {
  if (!Fo(e2))
    return e2;
  if ("production" !== process.env.NODE_ENV && e2.component)
    return e2.component.subTree;
  const { shapeFlag: t2, children: n2 } = e2;
  if (n2) {
    if (16 & t2)
      return n2[0];
    if (32 & t2 && v(n2.default))
      return n2.default();
  }
}
function Vo(e2, t2) {
  6 & e2.shapeFlag && e2.component ? Vo(e2.component.subTree, t2) : 128 & e2.shapeFlag ? (e2.ssContent.transition = t2.clone(e2.ssContent), e2.ssFallback.transition = t2.clone(e2.ssFallback)) : e2.transition = t2;
}
function To(e2, t2 = false, n2) {
  let o2 = [], r2 = 0;
  for (let s2 = 0; s2 < e2.length; s2++) {
    let a2 = e2[s2];
    const l2 = null == n2 ? a2.key : String(n2) + String(null != a2.key ? a2.key : s2);
    a2.type === bs ? (128 & a2.patchFlag && r2++, o2 = o2.concat(To(a2.children, t2, l2))) : (t2 || a2.type !== _s) && o2.push(null != l2 ? $s(a2, { key: l2 }) : a2);
  }
  if (r2 > 1)
    for (let e3 = 0; e3 < o2.length; e3++)
      o2[e3].patchFlag = -2;
  return o2;
}
/*! #__NO_SIDE_EFFECTS__ */
function Lo(e2, t2) {
  return v(e2) ? (() => i({ name: e2.name }, t2, { setup: e2 }))() : e2;
}
const Po = (e2) => !!e2.type.__asyncLoader, Fo = (e2) => e2.type.__isKeepAlive;
function Ro(e2, t2) {
  Bo(e2, "a", t2);
}
function $o(e2, t2) {
  Bo(e2, "da", t2);
}
function Bo(e2, t2, n2 = qs) {
  const o2 = e2.__wdc || (e2.__wdc = () => {
    let t3 = n2;
    for (; t3; ) {
      if (t3.isDeactivated)
        return;
      t3 = t3.parent;
    }
    return e2();
  });
  if (jo(t2, o2, n2), n2) {
    let e3 = n2.parent;
    for (; e3 && e3.parent; )
      Fo(e3.parent.vnode) && Ho(o2, t2, n2, e3), e3 = e3.parent;
  }
}
function Ho(e2, t2, n2, o2) {
  const r2 = jo(t2, e2, o2, true);
  Go(() => {
    c(o2[t2], r2);
  }, n2);
}
function jo(e2, t2, n2 = qs, o2 = false) {
  if (n2) {
    const r2 = n2[e2] || (n2[e2] = []), s2 = t2.__weh || (t2.__weh = (...o3) => {
      if (n2.isUnmounted)
        return;
      ge();
      const r3 = ea(n2), s3 = Jt(t2, n2, e2, o3);
      return r3(), ye(), s3;
    });
    return o2 ? r2.unshift(s2) : r2.push(s2), s2;
  }
  if ("production" !== process.env.NODE_ENV) {
    Wt(`${V(qt[e2].replace(/ hook$/, ""))} is called when there is no active component instance to be associated with. Lifecycle injection APIs can only be used during execution of setup(). If you are using async setup(), make sure to register lifecycle hooks before the first await statement.`);
  }
}
const Ko = (e2) => (t2, n2 = qs) => (!aa || "sp" === e2) && jo(e2, (...e3) => t2(...e3), n2), zo = Ko("bm"), Zo = Ko("m"), Uo = Ko("bu"), Wo = Ko("u"), Yo = Ko("bum"), Go = Ko("um"), qo = Ko("sp"), Xo = Ko("rtg"), Jo = Ko("rtc");
function Qo(e2, t2 = qs) {
  jo("ec", e2, t2);
}
function er(e2, t2, n2, o2) {
  let r2;
  const s2 = n2 && n2[o2];
  if (p(e2) || m(e2)) {
    r2 = new Array(e2.length);
    for (let n3 = 0, o3 = e2.length; n3 < o3; n3++)
      r2[n3] = t2(e2[n3], n3, void 0, s2 && s2[n3]);
  } else if ("number" == typeof e2) {
    "production" === process.env.NODE_ENV || Number.isInteger(e2) || Wt(`The v-for range expect an integer value but got ${e2}.`), r2 = new Array(e2);
    for (let n3 = 0; n3 < e2; n3++)
      r2[n3] = t2(n3 + 1, n3, void 0, s2 && s2[n3]);
  } else if (y(e2))
    if (e2[Symbol.iterator])
      r2 = Array.from(e2, (e3, n3) => t2(e3, n3, void 0, s2 && s2[n3]));
    else {
      const n3 = Object.keys(e2);
      r2 = new Array(n3.length);
      for (let o3 = 0, a2 = n3.length; o3 < a2; o3++) {
        const a3 = n3[o3];
        r2[o3] = t2(e2[a3], a3, o3, s2 && s2[o3]);
      }
    }
  else
    r2 = [];
  return n2 && (n2[o2] = r2), r2;
}
function tr(e2, t2) {
  for (let n2 = 0; n2 < t2.length; n2++) {
    const o2 = t2[n2];
    if (p(o2))
      for (let t3 = 0; t3 < o2.length; t3++)
        e2[o2[t3].name] = o2[t3].fn;
    else
      o2 && (e2[o2.name] = o2.key ? (...e3) => {
        const t3 = o2.fn(...e3);
        return t3 && (t3.key = o2.key), t3;
      } : o2.fn);
  }
  return e2;
}
function nr(e2, t2, n2 = {}, o2, r2) {
  if (zn.isCE || zn.parent && Po(zn.parent) && zn.parent.isCE)
    return "default" !== t2 && (n2.name = t2), Ps("slot", n2, o2 && o2());
  let s2 = e2[t2];
  "production" !== process.env.NODE_ENV && s2 && s2.length > 1 && (Wt("SSR-optimized slot function detected in a non-SSR-optimized render function. You need to mark this component with $dynamic-slots in the parent template."), s2 = () => []), s2 && s2._c && (s2._d = false), Ss();
  const a2 = s2 && or(s2(n2)), l2 = Ms(bs, { key: n2.key || a2 && a2.key || `_${t2}` }, a2 || (o2 ? o2() : []), a2 && 1 === e2._ ? 64 : -2);
  return !r2 && l2.scopeId && (l2.slotScopeIds = [l2.scopeId + "-s"]), s2 && s2._c && (s2._d = true), l2;
}
function or(e2) {
  return e2.some((e3) => !As(e3) || e3.type !== _s && !(e3.type === bs && !or(e3.children))) ? e2 : null;
}
const rr = (e2) => e2 ? ra(e2) ? ua(e2) || e2.proxy : rr(e2.parent) : null, sr = i(/* @__PURE__ */ Object.create(null), { $: (e2) => e2, $el: (e2) => e2.vnode.el, $data: (e2) => e2.data, $props: (e2) => "production" !== process.env.NODE_ENV ? gt(e2.props) : e2.props, $attrs: (e2) => "production" !== process.env.NODE_ENV ? gt(e2.attrs) : e2.attrs, $slots: (e2) => "production" !== process.env.NODE_ENV ? gt(e2.slots) : e2.slots, $refs: (e2) => "production" !== process.env.NODE_ENV ? gt(e2.refs) : e2.refs, $parent: (e2) => rr(e2.parent), $root: (e2) => rr(e2.root), $emit: (e2) => e2.emit, $options: (e2) => vr(e2), $forceUpdate: (e2) => e2.f || (e2.f = () => {
  e2.effect.dirty = true, pn(e2.update);
}), $nextTick: (e2) => e2.n || (e2.n = un.bind(e2.proxy)), $watch: (e2) => mo.bind(e2) }), ar = (e2) => "_" === e2 || "$" === e2, lr = (e2, t2) => e2 !== n && !e2.__isScriptSetup && u(e2, t2), ir = { get({ _: e2 }, t2) {
  if ("__v_skip" === t2)
    return true;
  const { ctx: o2, setupState: r2, data: s2, props: a2, accessCache: l2, type: i2, appContext: c2 } = e2;
  if ("production" !== process.env.NODE_ENV && "__isVue" === t2)
    return true;
  let d2;
  if ("$" !== t2[0]) {
    const i3 = l2[t2];
    if (void 0 !== i3)
      switch (i3) {
        case 1:
          return r2[t2];
        case 2:
          return s2[t2];
        case 4:
          return o2[t2];
        case 3:
          return a2[t2];
      }
    else {
      if (lr(r2, t2))
        return l2[t2] = 1, r2[t2];
      if (s2 !== n && u(s2, t2))
        return l2[t2] = 2, s2[t2];
      if ((d2 = e2.propsOptions[0]) && u(d2, t2))
        return l2[t2] = 3, a2[t2];
      if (o2 !== n && u(o2, t2))
        return l2[t2] = 4, o2[t2];
      ur && (l2[t2] = 0);
    }
  }
  const p2 = sr[t2];
  let f2, h2;
  return p2 ? ("$attrs" === t2 ? (Oe(e2.attrs, "get", ""), "production" !== process.env.NODE_ENV && Gn()) : "production" !== process.env.NODE_ENV && "$slots" === t2 && Oe(e2, "get", t2), p2(e2)) : (f2 = i2.__cssModules) && (f2 = f2[t2]) ? f2 : o2 !== n && u(o2, t2) ? (l2[t2] = 4, o2[t2]) : (h2 = c2.config.globalProperties, u(h2, t2) ? h2[t2] : void ("production" === process.env.NODE_ENV || !zn || m(t2) && 0 === t2.indexOf("__v") || (s2 !== n && ar(t2[0]) && u(s2, t2) ? Wt(`Property ${JSON.stringify(t2)} must be accessed via $data because it starts with a reserved character ("$" or "_") and is not proxied on the render context.`) : e2 === zn && Wt(`Property ${JSON.stringify(t2)} was accessed during render but is not defined on instance.`))));
}, set({ _: e2 }, t2, o2) {
  const { data: r2, setupState: s2, ctx: a2 } = e2;
  return lr(s2, t2) ? (s2[t2] = o2, true) : "production" !== process.env.NODE_ENV && s2.__isScriptSetup && u(s2, t2) ? (Wt(`Cannot mutate <script setup> binding "${t2}" from Options API.`), false) : r2 !== n && u(r2, t2) ? (r2[t2] = o2, true) : u(e2.props, t2) ? ("production" !== process.env.NODE_ENV && Wt(`Attempting to mutate prop "${t2}". Props are readonly.`), false) : "$" === t2[0] && t2.slice(1) in e2 ? ("production" !== process.env.NODE_ENV && Wt(`Attempting to mutate public property "${t2}". Properties starting with $ are reserved and readonly.`), false) : ("production" !== process.env.NODE_ENV && t2 in e2.appContext.config.globalProperties ? Object.defineProperty(a2, t2, { enumerable: true, configurable: true, value: o2 }) : a2[t2] = o2, true);
}, has({ _: { data: e2, setupState: t2, accessCache: o2, ctx: r2, appContext: s2, propsOptions: a2 } }, l2) {
  let i2;
  return !!o2[l2] || e2 !== n && u(e2, l2) || lr(t2, l2) || (i2 = a2[0]) && u(i2, l2) || u(r2, l2) || u(sr, l2) || u(s2.config.globalProperties, l2);
}, defineProperty(e2, t2, n2) {
  return null != n2.get ? e2._.accessCache[t2] = 0 : u(n2, "value") && this.set(e2, t2, n2.value, null), Reflect.defineProperty(e2, t2, n2);
} };
function cr(e2) {
  return p(e2) ? e2.reduce((e3, t2) => (e3[t2] = null, e3), {}) : e2;
}
function dr(e2, t2) {
  return e2 && t2 ? p(e2) && p(t2) ? e2.concat(t2) : i({}, cr(e2), cr(t2)) : e2 || t2;
}
"production" !== process.env.NODE_ENV && (ir.ownKeys = (e2) => (Wt("Avoid app logic that relies on enumerating keys on a component instance. The keys will be empty in production mode to avoid performance overhead."), Reflect.ownKeys(e2)));
let ur = true;
function pr(e2) {
  const t2 = vr(e2), n2 = e2.proxy, o2 = e2.ctx;
  ur = false, t2.beforeCreate && fr(t2.beforeCreate, e2, "bc");
  const { data: s2, computed: a2, methods: l2, watch: i2, provide: c2, inject: d2, created: u2, beforeMount: f2, mounted: h2, beforeUpdate: m2, updated: g2, activated: w2, deactivated: _2, beforeDestroy: C2, beforeUnmount: x2, destroyed: k2, unmounted: S2, render: E2, renderTracked: I2, renderTriggered: O2, errorCaptured: N2, serverPrefetch: M2, expose: A2, inheritAttrs: D2, components: V2, directives: T2, filters: L2 } = t2, P2 = "production" !== process.env.NODE_ENV ? function() {
    const e3 = /* @__PURE__ */ Object.create(null);
    return (t3, n3) => {
      e3[n3] ? Wt(`${t3} property "${n3}" is already defined in ${e3[n3]}.`) : e3[n3] = t3;
    };
  }() : null;
  if ("production" !== process.env.NODE_ENV) {
    const [t3] = e2.propsOptions;
    if (t3)
      for (const e3 in t3)
        P2("Props", e3);
  }
  if (d2 && function(e3, t3, n3 = r) {
    p(e3) && (e3 = br(e3));
    for (const o3 in e3) {
      const r2 = e3[o3];
      let s3;
      s3 = y(r2) ? "default" in r2 ? Or(r2.from || o3, r2.default, true) : Or(r2.from || o3) : Or(r2), Mt(s3) ? Object.defineProperty(t3, o3, { enumerable: true, configurable: true, get: () => s3.value, set: (e4) => s3.value = e4 }) : t3[o3] = s3, "production" !== process.env.NODE_ENV && n3("Inject", o3);
    }
  }(d2, o2, P2), l2)
    for (const e3 in l2) {
      const t3 = l2[e3];
      v(t3) ? ("production" !== process.env.NODE_ENV ? Object.defineProperty(o2, e3, { value: t3.bind(n2), configurable: true, enumerable: true, writable: true }) : o2[e3] = t3.bind(n2), "production" !== process.env.NODE_ENV && P2("Methods", e3)) : "production" !== process.env.NODE_ENV && Wt(`Method "${e3}" has type "${typeof t3}" in the component definition. Did you reference the function correctly?`);
    }
  if (s2) {
    "production" === process.env.NODE_ENV || v(s2) || Wt("The data option must be a function. Plain object usage is no longer supported.");
    const t3 = s2.call(n2, n2);
    if ("production" !== process.env.NODE_ENV && b(t3) && Wt("data() returned a Promise - note data() cannot be async; If you intend to perform data fetching before component renders, use async setup() + <Suspense>."), y(t3)) {
      if (e2.data = vt(t3), "production" !== process.env.NODE_ENV)
        for (const e3 in t3)
          P2("Data", e3), ar(e3[0]) || Object.defineProperty(o2, e3, { configurable: true, enumerable: true, get: () => t3[e3], set: r });
    } else
      "production" !== process.env.NODE_ENV && Wt("data() should return an object.");
  }
  if (ur = true, a2)
    for (const e3 in a2) {
      const t3 = a2[e3], s3 = v(t3) ? t3.bind(n2, n2) : v(t3.get) ? t3.get.bind(n2, n2) : r;
      "production" !== process.env.NODE_ENV && s3 === r && Wt(`Computed property "${e3}" has no getter.`);
      const l3 = !v(t3) && v(t3.set) ? t3.set.bind(n2) : "production" !== process.env.NODE_ENV ? () => {
        Wt(`Write operation failed: computed property "${e3}" is readonly.`);
      } : r, i3 = ga({ get: s3, set: l3 });
      Object.defineProperty(o2, e3, { enumerable: true, configurable: true, get: () => i3.value, set: (e4) => i3.value = e4 }), "production" !== process.env.NODE_ENV && P2("Computed", e3);
    }
  if (i2)
    for (const e3 in i2)
      hr(i2[e3], o2, n2, e3);
  if (c2) {
    const e3 = v(c2) ? c2.call(n2) : c2;
    Reflect.ownKeys(e3).forEach((t3) => {
      Ir(t3, e3[t3]);
    });
  }
  function F2(e3, t3) {
    p(t3) ? t3.forEach((t4) => e3(t4.bind(n2))) : t3 && e3(t3.bind(n2));
  }
  if (u2 && fr(u2, e2, "c"), F2(zo, f2), F2(Zo, h2), F2(Uo, m2), F2(Wo, g2), F2(Ro, w2), F2($o, _2), F2(Qo, N2), F2(Jo, I2), F2(Xo, O2), F2(Yo, x2), F2(Go, S2), F2(qo, M2), p(A2))
    if (A2.length) {
      const t3 = e2.exposed || (e2.exposed = {});
      A2.forEach((e3) => {
        Object.defineProperty(t3, e3, { get: () => n2[e3], set: (t4) => n2[e3] = t4 });
      });
    } else
      e2.exposed || (e2.exposed = {});
  E2 && e2.render === r && (e2.render = E2), null != D2 && (e2.inheritAttrs = D2), V2 && (e2.components = V2), T2 && (e2.directives = T2);
}
function fr(e2, t2, n2) {
  Jt(p(e2) ? e2.map((e3) => e3.bind(t2.proxy)) : e2.bind(t2.proxy), t2, n2);
}
function hr(e2, t2, n2, o2) {
  const r2 = o2.includes(".") ? go(n2, o2) : () => n2[o2];
  if (m(e2)) {
    const n3 = t2[e2];
    v(n3) ? ho(r2, n3) : "production" !== process.env.NODE_ENV && Wt(`Invalid watch handler specified by key "${e2}"`, n3);
  } else if (v(e2))
    ho(r2, e2.bind(n2));
  else if (y(e2))
    if (p(e2))
      e2.forEach((e3) => hr(e3, t2, n2, o2));
    else {
      const o3 = v(e2.handler) ? e2.handler.bind(n2) : t2[e2.handler];
      v(o3) ? ho(r2, o3, e2) : "production" !== process.env.NODE_ENV && Wt(`Invalid watch handler specified by key "${e2.handler}"`, o3);
    }
  else
    "production" !== process.env.NODE_ENV && Wt(`Invalid watch option: "${o2}"`, e2);
}
function vr(e2) {
  const t2 = e2.type, { mixins: n2, extends: o2 } = t2, { mixins: r2, optionsCache: s2, config: { optionMergeStrategies: a2 } } = e2.appContext, l2 = s2.get(t2);
  let i2;
  return l2 ? i2 = l2 : r2.length || n2 || o2 ? (i2 = {}, r2.length && r2.forEach((e3) => mr(i2, e3, a2, true)), mr(i2, t2, a2)) : i2 = t2, y(t2) && s2.set(t2, i2), i2;
}
function mr(e2, t2, n2, o2 = false) {
  const { mixins: r2, extends: s2 } = t2;
  s2 && mr(e2, s2, n2, true), r2 && r2.forEach((t3) => mr(e2, t3, n2, true));
  for (const r3 in t2)
    if (o2 && "expose" === r3)
      "production" !== process.env.NODE_ENV && Wt('"expose" option is ignored when declared in mixins or extends. It should only be declared in the base component itself.');
    else {
      const o3 = gr[r3] || n2 && n2[r3];
      e2[r3] = o3 ? o3(e2[r3], t2[r3]) : t2[r3];
    }
  return e2;
}
const gr = { data: yr, props: Cr, emits: Cr, methods: _r, computed: _r, beforeCreate: wr, created: wr, beforeMount: wr, mounted: wr, beforeUpdate: wr, updated: wr, beforeDestroy: wr, beforeUnmount: wr, destroyed: wr, unmounted: wr, activated: wr, deactivated: wr, errorCaptured: wr, serverPrefetch: wr, components: _r, directives: _r, watch: function(e2, t2) {
  if (!e2)
    return t2;
  if (!t2)
    return e2;
  const n2 = i(/* @__PURE__ */ Object.create(null), e2);
  for (const o2 in t2)
    n2[o2] = wr(e2[o2], t2[o2]);
  return n2;
}, provide: yr, inject: function(e2, t2) {
  return _r(br(e2), br(t2));
} };
function yr(e2, t2) {
  return t2 ? e2 ? function() {
    return i(v(e2) ? e2.call(this, this) : e2, v(t2) ? t2.call(this, this) : t2);
  } : t2 : e2;
}
function br(e2) {
  if (p(e2)) {
    const t2 = {};
    for (let n2 = 0; n2 < e2.length; n2++)
      t2[e2[n2]] = e2[n2];
    return t2;
  }
  return e2;
}
function wr(e2, t2) {
  return e2 ? [...new Set([].concat(e2, t2))] : t2;
}
function _r(e2, t2) {
  return e2 ? i(/* @__PURE__ */ Object.create(null), e2, t2) : t2;
}
function Cr(e2, t2) {
  return e2 ? p(e2) && p(t2) ? [.../* @__PURE__ */ new Set([...e2, ...t2])] : i(/* @__PURE__ */ Object.create(null), cr(e2), cr(null != t2 ? t2 : {})) : t2;
}
function xr() {
  return { app: null, config: { isNativeTag: s, performance: false, globalProperties: {}, optionMergeStrategies: {}, errorHandler: void 0, warnHandler: void 0, compilerOptions: {} }, mixins: [], components: {}, directives: {}, provides: /* @__PURE__ */ Object.create(null), optionsCache: /* @__PURE__ */ new WeakMap(), propsCache: /* @__PURE__ */ new WeakMap(), emitsCache: /* @__PURE__ */ new WeakMap() };
}
let kr = 0;
function Sr(e2, t2) {
  return function(n2, o2 = null) {
    v(n2) || (n2 = i({}, n2)), null == o2 || y(o2) || ("production" !== process.env.NODE_ENV && Wt("root props passed to app.mount() must be an object."), o2 = null);
    const r2 = xr(), s2 = /* @__PURE__ */ new WeakSet();
    let a2 = false;
    const l2 = r2.app = { _uid: kr++, _component: n2, _props: o2, _container: null, _context: r2, _instance: null, version: wa, get config() {
      return r2.config;
    }, set config(e3) {
      "production" !== process.env.NODE_ENV && Wt("app.config cannot be replaced. Modify individual options instead.");
    }, use: (e3, ...t3) => (s2.has(e3) ? "production" !== process.env.NODE_ENV && Wt("Plugin has already been applied to target app.") : e3 && v(e3.install) ? (s2.add(e3), e3.install(l2, ...t3)) : v(e3) ? (s2.add(e3), e3(l2, ...t3)) : "production" !== process.env.NODE_ENV && Wt('A plugin must either be a function or an object with an "install" function.'), l2), mixin: (e3) => (r2.mixins.includes(e3) ? "production" !== process.env.NODE_ENV && Wt("Mixin has already been applied to target app" + (e3.name ? `: ${e3.name}` : "")) : r2.mixins.push(e3), l2), component: (e3, t3) => ("production" !== process.env.NODE_ENV && oa(e3, r2.config), t3 ? ("production" !== process.env.NODE_ENV && r2.components[e3] && Wt(`Component "${e3}" has already been registered in target app.`), r2.components[e3] = t3, l2) : r2.components[e3]), directive: (e3, t3) => ("production" !== process.env.NODE_ENV && bo(e3), t3 ? ("production" !== process.env.NODE_ENV && r2.directives[e3] && Wt(`Directive "${e3}" has already been registered in target app.`), r2.directives[e3] = t3, l2) : r2.directives[e3]), mount(s3, i2, c2) {
      if (!a2) {
        "production" !== process.env.NODE_ENV && s3.__vue_app__ && Wt("There is already an app instance mounted on the host container.\n If you want to mount another app on the same host container, you need to unmount the previous app by calling `app.unmount()` first.");
        const d2 = Ps(n2, o2);
        return d2.appContext = r2, true === c2 ? c2 = "svg" : false === c2 && (c2 = void 0), "production" !== process.env.NODE_ENV && (r2.reload = () => {
          e2($s(d2), s3, c2);
        }), i2 && t2 ? t2(d2, s3) : e2(d2, s3, c2), a2 = true, l2._container = s3, s3.__vue_app__ = l2, "production" !== process.env.NODE_ENV && (l2._instance = d2.component, function(e3, t3) {
          An("app:init", e3, t3, { Fragment: bs, Text: ws, Comment: _s, Static: Cs });
        }(l2, wa)), ua(d2.component) || d2.component.proxy;
      }
      "production" !== process.env.NODE_ENV && Wt("App has already been mounted.\nIf you want to remount the same app, move your app creation logic into a factory function and create fresh app instances for each mount - e.g. `const createMyApp = () => createApp(App)`");
    }, unmount() {
      a2 ? (e2(null, l2._container), "production" !== process.env.NODE_ENV && (l2._instance = null, function(e3) {
        An("app:unmount", e3);
      }(l2)), delete l2._container.__vue_app__) : "production" !== process.env.NODE_ENV && Wt("Cannot unmount an app that is not mounted.");
    }, provide: (e3, t3) => ("production" !== process.env.NODE_ENV && e3 in r2.provides && Wt(`App already provides property with key "${String(e3)}". It will be overwritten with the new value.`), r2.provides[e3] = t3, l2), runWithContext(e3) {
      const t3 = Er;
      Er = l2;
      try {
        return e3();
      } finally {
        Er = t3;
      }
    } };
    return l2;
  };
}
let Er = null;
function Ir(e2, t2) {
  if (qs) {
    let n2 = qs.provides;
    const o2 = qs.parent && qs.parent.provides;
    o2 === n2 && (n2 = qs.provides = Object.create(o2)), n2[e2] = t2;
  } else
    "production" !== process.env.NODE_ENV && Wt("provide() can only be used inside setup().");
}
function Or(e2, t2, n2 = false) {
  const o2 = qs || zn;
  if (o2 || Er) {
    const r2 = o2 ? null == o2.parent ? o2.vnode.appContext && o2.vnode.appContext.provides : o2.parent.provides : Er._context.provides;
    if (r2 && e2 in r2)
      return r2[e2];
    if (arguments.length > 1)
      return n2 && v(t2) ? t2.call(o2 && o2.proxy) : t2;
    "production" !== process.env.NODE_ENV && Wt(`injection "${String(e2)}" not found.`);
  } else
    "production" !== process.env.NODE_ENV && Wt("inject() can only be used inside setup() or functional components.");
}
const Nr = {}, Mr = () => Object.create(Nr), Ar = (e2) => Object.getPrototypeOf(e2) === Nr;
function Dr(e2, t2, n2, o2 = false) {
  const r2 = {}, s2 = Mr();
  e2.propsDefaults = /* @__PURE__ */ Object.create(null), Vr(e2, t2, r2, s2);
  for (const t3 in e2.propsOptions[0])
    t3 in r2 || (r2[t3] = void 0);
  "production" !== process.env.NODE_ENV && Br(t2 || {}, r2, e2), n2 ? e2.props = o2 ? r2 : yt(r2, false, Be, lt, pt) : e2.type.props ? e2.props = r2 : e2.props = s2, e2.attrs = s2;
}
function Vr(e2, t2, o2, r2) {
  const [s2, a2] = e2.propsOptions;
  let l2, i2 = false;
  if (t2)
    for (let n2 in t2) {
      if (S(n2))
        continue;
      const c2 = t2[n2];
      let d2;
      s2 && u(s2, d2 = N(n2)) ? a2 && a2.includes(d2) ? (l2 || (l2 = {}))[d2] = c2 : o2[d2] = c2 : Kn(e2.emitsOptions, n2) || n2 in r2 && c2 === r2[n2] || (r2[n2] = c2, i2 = true);
    }
  if (a2) {
    const t3 = xt(o2), r3 = l2 || n;
    for (let n2 = 0; n2 < a2.length; n2++) {
      const l3 = a2[n2];
      o2[l3] = Tr(s2, t3, l3, r3[l3], e2, !u(r3, l3));
    }
  }
  return i2;
}
function Tr(e2, t2, n2, o2, r2, s2) {
  const a2 = e2[n2];
  if (null != a2) {
    const e3 = u(a2, "default");
    if (e3 && void 0 === o2) {
      const e4 = a2.default;
      if (a2.type !== Function && !a2.skipFactory && v(e4)) {
        const { propsDefaults: s3 } = r2;
        if (n2 in s3)
          o2 = s3[n2];
        else {
          const a3 = ea(r2);
          o2 = s3[n2] = e4.call(null, t2), a3();
        }
      } else
        o2 = e4;
    }
    a2[0] && (s2 && !e3 ? o2 = false : !a2[1] || "" !== o2 && o2 !== A(n2) || (o2 = true));
  }
  return o2;
}
function Lr(e2, t2, r2 = false) {
  const s2 = t2.propsCache, a2 = s2.get(e2);
  if (a2)
    return a2;
  const l2 = e2.props, c2 = {}, d2 = [];
  let f2 = false;
  if (!v(e2)) {
    const n2 = (e3) => {
      f2 = true;
      const [n3, o2] = Lr(e3, t2, true);
      i(c2, n3), o2 && d2.push(...o2);
    };
    !r2 && t2.mixins.length && t2.mixins.forEach(n2), e2.extends && n2(e2.extends), e2.mixins && e2.mixins.forEach(n2);
  }
  if (!l2 && !f2)
    return y(e2) && s2.set(e2, o), o;
  if (p(l2))
    for (let e3 = 0; e3 < l2.length; e3++) {
      "production" === process.env.NODE_ENV || m(l2[e3]) || Wt("props must be strings when using array syntax.", l2[e3]);
      const t3 = N(l2[e3]);
      Pr(t3) && (c2[t3] = n);
    }
  else if (l2) {
    "production" === process.env.NODE_ENV || y(l2) || Wt("invalid props options", l2);
    for (const e3 in l2) {
      const t3 = N(e3);
      if (Pr(t3)) {
        const n2 = l2[e3], o2 = c2[t3] = p(n2) || v(n2) ? { type: n2 } : i({}, n2);
        if (o2) {
          const e4 = $r(Boolean, o2.type), n3 = $r(String, o2.type);
          o2[0] = e4 > -1, o2[1] = n3 < 0 || e4 < n3, (e4 > -1 || u(o2, "default")) && d2.push(t3);
        }
      }
    }
  }
  const h2 = [c2, d2];
  return y(e2) && s2.set(e2, h2), h2;
}
function Pr(e2) {
  return "$" !== e2[0] && !S(e2) || ("production" !== process.env.NODE_ENV && Wt(`Invalid prop name: "${e2}" is a reserved property.`), false);
}
function Fr(e2) {
  if (null === e2)
    return "null";
  if ("function" == typeof e2)
    return e2.name || "";
  if ("object" == typeof e2) {
    return e2.constructor && e2.constructor.name || "";
  }
  return "";
}
function Rr(e2, t2) {
  return Fr(e2) === Fr(t2);
}
function $r(e2, t2) {
  return p(t2) ? t2.findIndex((t3) => Rr(t3, e2)) : v(t2) && Rr(t2, e2) ? 0 : -1;
}
function Br(e2, t2, n2) {
  const o2 = xt(t2), r2 = n2.propsOptions[0];
  for (const t3 in r2) {
    let n3 = r2[t3];
    null != n3 && Hr(t3, o2[t3], n3, "production" !== process.env.NODE_ENV ? gt(o2) : o2, !u(e2, t3) && !u(e2, A(t3)));
  }
}
function Hr(e2, t2, n2, o2, r2) {
  const { type: s2, required: a2, validator: l2, skipCheck: i2 } = n2;
  if (a2 && r2)
    Wt('Missing required prop: "' + e2 + '"');
  else if (null != t2 || a2) {
    if (null != s2 && true !== s2 && !i2) {
      let n3 = false;
      const o3 = p(s2) ? s2 : [s2], r3 = [];
      for (let e3 = 0; e3 < o3.length && !n3; e3++) {
        const { valid: s3, expectedType: a3 } = Kr(t2, o3[e3]);
        r3.push(a3 || ""), n3 = s3;
      }
      if (!n3)
        return void Wt(function(e3, t3, n4) {
          if (0 === n4.length)
            return `Prop type [] for prop "${e3}" won't match anything. Did you mean to use type Array instead?`;
          let o4 = `Invalid prop: type check failed for prop "${e3}". Expected ${n4.map(D).join(" | ")}`;
          const r4 = n4[0], s3 = C(t3), a3 = zr(t3, r4), l3 = zr(t3, s3);
          1 === n4.length && Zr(r4) && !function(...e4) {
            return e4.some((e5) => "boolean" === e5.toLowerCase());
          }(r4, s3) && (o4 += ` with value ${a3}`);
          o4 += `, got ${s3} `, Zr(s3) && (o4 += `with value ${l3}.`);
          return o4;
        }(e2, t2, r3));
    }
    l2 && !l2(t2, o2) && Wt('Invalid prop: custom validator check failed for prop "' + e2 + '".');
  }
}
const jr = t("String,Number,Boolean,Function,Symbol,BigInt");
function Kr(e2, t2) {
  let n2;
  const o2 = Fr(t2);
  if (jr(o2)) {
    const r2 = typeof e2;
    n2 = r2 === o2.toLowerCase(), n2 || "object" !== r2 || (n2 = e2 instanceof t2);
  } else
    n2 = "Object" === o2 ? y(e2) : "Array" === o2 ? p(e2) : "null" === o2 ? null === e2 : e2 instanceof t2;
  return { valid: n2, expectedType: o2 };
}
function zr(e2, t2) {
  return "String" === t2 ? `"${e2}"` : "Number" === t2 ? `${Number(e2)}` : `${e2}`;
}
function Zr(e2) {
  return ["string", "number", "boolean"].some((t2) => e2.toLowerCase() === t2);
}
const Ur = (e2) => "_" === e2[0] || "$stable" === e2, Wr = (e2) => p(e2) ? e2.map(Ks) : [Ks(e2)], Yr = (e2, t2, n2) => {
  if (t2._n)
    return t2;
  const o2 = Wn((...o3) => ("production" === process.env.NODE_ENV || !qs || n2 && n2.root !== qs.root || Wt(`Slot "${e2}" invoked outside of the render function: this will not track dependencies used in the slot. Invoke the slot function inside the render function instead.`), Wr(t2(...o3))), n2);
  return o2._c = false, o2;
}, Gr = (e2, t2, n2) => {
  const o2 = e2._ctx;
  for (const n3 in e2) {
    if (Ur(n3))
      continue;
    const r2 = e2[n3];
    if (v(r2))
      t2[n3] = Yr(n3, r2, o2);
    else if (null != r2) {
      "production" !== process.env.NODE_ENV && Wt(`Non-function value encountered for slot "${n3}". Prefer function slots for better performance.`);
      const e3 = Wr(r2);
      t2[n3] = () => e3;
    }
  }
}, qr = (e2, t2) => {
  "production" === process.env.NODE_ENV || Fo(e2.vnode) || Wt("Non-function value encountered for default slot. Prefer function slots for better performance.");
  const n2 = Wr(t2);
  e2.slots.default = () => n2;
}, Xr = (e2, t2) => {
  const n2 = e2.slots = Mr();
  if (32 & e2.vnode.shapeFlag) {
    const e3 = t2._;
    e3 ? (i(n2, t2), P(n2, "_", e3, true)) : Gr(t2, n2);
  } else
    t2 && qr(e2, t2);
}, Jr = (e2, t2, o2) => {
  const { vnode: r2, slots: s2 } = e2;
  let a2 = true, l2 = n;
  if (32 & r2.shapeFlag) {
    const n2 = t2._;
    n2 ? "production" !== process.env.NODE_ENV && _n ? (i(s2, t2), Ne(e2, "set", "$slots")) : o2 && 1 === n2 ? a2 = false : (i(s2, t2), o2 || 1 !== n2 || delete s2._) : (a2 = !t2.$stable, Gr(t2, s2)), l2 = t2;
  } else
    t2 && (qr(e2, t2), l2 = { default: 1 });
  if (a2)
    for (const e3 in s2)
      Ur(e3) || null != l2[e3] || delete s2[e3];
};
function Qr(e2, t2, o2, r2, s2 = false) {
  if (p(e2))
    return void e2.forEach((e3, n2) => Qr(e3, t2 && (p(t2) ? t2[n2] : t2), o2, r2, s2));
  if (Po(r2) && !s2)
    return;
  const a2 = 4 & r2.shapeFlag ? ua(r2.component) || r2.component.proxy : r2.el, l2 = s2 ? null : a2, { i: i2, r: d2 } = e2;
  if ("production" !== process.env.NODE_ENV && !i2)
    return void Wt("Missing ref owner context. ref cannot be used on hoisted vnodes. A vnode with ref must be created inside the render function.");
  const f2 = t2 && t2.r, h2 = i2.refs === n ? i2.refs = {} : i2.refs, g2 = i2.setupState;
  if (null != f2 && f2 !== d2 && (m(f2) ? (h2[f2] = null, u(g2, f2) && (g2[f2] = null)) : Mt(f2) && (f2.value = null)), v(d2))
    Xt(d2, i2, 12, [l2, h2]);
  else {
    const t3 = m(d2), n2 = Mt(d2);
    if (t3 || n2) {
      const r3 = () => {
        if (e2.f) {
          const n3 = t3 ? u(g2, d2) ? g2[d2] : h2[d2] : d2.value;
          s2 ? p(n3) && c(n3, a2) : p(n3) ? n3.includes(a2) || n3.push(a2) : t3 ? (h2[d2] = [a2], u(g2, d2) && (g2[d2] = h2[d2])) : (d2.value = [a2], e2.k && (h2[e2.k] = d2.value));
        } else
          t3 ? (h2[d2] = l2, u(g2, d2) && (g2[d2] = l2)) : n2 ? (d2.value = l2, e2.k && (h2[e2.k] = l2)) : "production" !== process.env.NODE_ENV && Wt("Invalid template ref type:", d2, `(${typeof d2})`);
      };
      l2 ? (r3.id = -1, ss(r3, o2)) : r3();
    } else
      "production" !== process.env.NODE_ENV && Wt("Invalid template ref type:", d2, `(${typeof d2})`);
  }
}
let es, ts;
function ns(e2, t2) {
  e2.appContext.config.performance && rs() && ts.mark(`vue-${t2}-${e2.uid}`), "production" !== process.env.NODE_ENV && Rn(e2, t2, rs() ? ts.now() : Date.now());
}
function os(e2, t2) {
  if (e2.appContext.config.performance && rs()) {
    const n2 = `vue-${t2}-${e2.uid}`, o2 = n2 + ":end";
    ts.mark(o2), ts.measure(`<${va(e2, e2.type)}> ${t2}`, n2, o2), ts.clearMarks(n2), ts.clearMarks(o2);
  }
  "production" !== process.env.NODE_ENV && $n(e2, t2, rs() ? ts.now() : Date.now());
}
function rs() {
  return void 0 !== es || ("undefined" != typeof window && window.performance ? (es = true, ts = window.performance) : es = false), es;
}
const ss = function(e2, t2) {
  t2 && t2.pendingBranch ? p(e2) ? t2.effects.push(...e2) : t2.effects.push(e2) : hn(e2);
};
function as(e2) {
  return function(e3, t2) {
    !function() {
      const e4 = [];
      if ("boolean" != typeof __VUE_PROD_HYDRATION_MISMATCH_DETAILS__ && ("production" !== process.env.NODE_ENV && e4.push("__VUE_PROD_HYDRATION_MISMATCH_DETAILS__"), B().__VUE_PROD_HYDRATION_MISMATCH_DETAILS__ = false), "production" !== process.env.NODE_ENV && e4.length) {
        const t3 = e4.length > 1;
        console.warn(`Feature flag${t3 ? "s" : ""} ${e4.join(", ")} ${t3 ? "are" : "is"} not explicitly defined. You are running the esm-bundler build of Vue, which expects these compile-time feature flags to be globally injected via the bundler config in order to get better tree-shaking in the production bundle.

For more details, see https://link.vuejs.org/feature-flags.`);
      }
    }();
    const s2 = B();
    s2.__VUE__ = true, "production" === process.env.NODE_ENV || Dn(s2.__VUE_DEVTOOLS_GLOBAL_HOOK__, s2);
    const { insert: a2, remove: l2, patchProp: i2, createElement: c2, createText: d2, createComment: f2, setText: h2, setElementText: v2, parentNode: m2, nextSibling: g2, setScopeId: y2 = r, insertStaticContent: w2 } = e3, _2 = (e4, t3, n2, o2 = null, r2 = null, s3 = null, a3 = void 0, l3 = null, i3 = ("production" === process.env.NODE_ENV || !_n) && !!t3.dynamicChildren) => {
      if (e4 === t3)
        return;
      e4 && !Ds(e4, t3) && (o2 = te2(e4), q2(e4, r2, s3, true), e4 = null), -2 === t3.patchFlag && (i3 = false, t3.dynamicChildren = null);
      const { type: c3, ref: d3, shapeFlag: u2 } = t3;
      switch (c3) {
        case ws:
          C2(e4, t3, n2, o2);
          break;
        case _s:
          x2(e4, t3, n2, o2);
          break;
        case Cs:
          null == e4 ? k2(t3, n2, o2, a3) : "production" !== process.env.NODE_ENV && E2(e4, t3, n2, a3);
          break;
        case bs:
          $2(e4, t3, n2, o2, r2, s3, a3, l3, i3);
          break;
        default:
          1 & u2 ? M2(e4, t3, n2, o2, r2, s3, a3, l3, i3) : 6 & u2 ? H2(e4, t3, n2, o2, r2, s3, a3, l3, i3) : 64 & u2 || 128 & u2 ? c3.process(e4, t3, n2, o2, r2, s3, a3, l3, i3, re2) : "production" !== process.env.NODE_ENV && Wt("Invalid VNode type:", c3, `(${typeof c3})`);
      }
      null != d3 && r2 && Qr(d3, e4 && e4.ref, s3, t3 || e4, !t3);
    }, C2 = (e4, t3, n2, o2) => {
      if (null == e4)
        a2(t3.el = d2(t3.children), n2, o2);
      else {
        const n3 = t3.el = e4.el;
        t3.children !== e4.children && h2(n3, t3.children);
      }
    }, x2 = (e4, t3, n2, o2) => {
      null == e4 ? a2(t3.el = f2(t3.children || ""), n2, o2) : t3.el = e4.el;
    }, k2 = (e4, t3, n2, o2) => {
      [e4.el, e4.anchor] = w2(e4.children, t3, n2, o2, e4.el, e4.anchor);
    }, E2 = (e4, t3, n2, o2) => {
      if (t3.children !== e4.children) {
        const r2 = g2(e4.anchor);
        O2(e4), [t3.el, t3.anchor] = w2(t3.children, n2, r2, o2);
      } else
        t3.el = e4.el, t3.anchor = e4.anchor;
    }, I2 = ({ el: e4, anchor: t3 }, n2, o2) => {
      let r2;
      for (; e4 && e4 !== t3; )
        r2 = g2(e4), a2(e4, n2, o2), e4 = r2;
      a2(t3, n2, o2);
    }, O2 = ({ el: e4, anchor: t3 }) => {
      let n2;
      for (; e4 && e4 !== t3; )
        n2 = g2(e4), l2(e4), e4 = n2;
      l2(t3);
    }, M2 = (e4, t3, n2, o2, r2, s3, a3, l3, i3) => {
      "svg" === t3.type ? a3 = "svg" : "math" === t3.type && (a3 = "mathml"), null == e4 ? D2(t3, n2, o2, r2, s3, a3, l3, i3) : P2(e4, t3, r2, s3, a3, l3, i3);
    }, D2 = (e4, t3, n2, o2, r2, s3, l3, d3) => {
      let u2, p2;
      const { props: f3, shapeFlag: h3, transition: m3, dirs: g3 } = e4;
      if (u2 = e4.el = c2(e4.type, s3, f3 && f3.is, f3), 8 & h3 ? v2(u2, e4.children) : 16 & h3 && T2(e4.children, u2, null, o2, r2, ls(e4, s3), l3, d3), g3 && _o(e4, null, o2, "created"), V2(u2, e4, e4.scopeId, l3, o2), f3) {
        for (const t4 in f3)
          "value" === t4 || S(t4) || i2(u2, t4, null, f3[t4], s3, e4.children, o2, r2, ee2);
        "value" in f3 && i2(u2, "value", null, f3.value, s3), (p2 = f3.onVnodeBeforeMount) && Ws(p2, o2, e4);
      }
      "production" !== process.env.NODE_ENV && (Object.defineProperty(u2, "__vnode", { value: e4, enumerable: false }), Object.defineProperty(u2, "__vueParentComponent", { value: o2, enumerable: false })), g3 && _o(e4, null, o2, "beforeMount");
      const y3 = function(e5, t4) {
        return (!e5 || e5 && !e5.pendingBranch) && t4 && !t4.persisted;
      }(r2, m3);
      y3 && m3.beforeEnter(u2), a2(u2, t3, n2), ((p2 = f3 && f3.onVnodeMounted) || y3 || g3) && ss(() => {
        p2 && Ws(p2, o2, e4), y3 && m3.enter(u2), g3 && _o(e4, null, o2, "mounted");
      }, r2);
    }, V2 = (e4, t3, n2, o2, r2) => {
      if (n2 && y2(e4, n2), o2)
        for (let t4 = 0; t4 < o2.length; t4++)
          y2(e4, o2[t4]);
      if (r2) {
        let n3 = r2.subTree;
        if ("production" !== process.env.NODE_ENV && n3.patchFlag > 0 && 2048 & n3.patchFlag && (n3 = Jn(n3.children) || n3), t3 === n3) {
          const t4 = r2.vnode;
          V2(e4, t4, t4.scopeId, t4.slotScopeIds, r2.parent);
        }
      }
    }, T2 = (e4, t3, n2, o2, r2, s3, a3, l3, i3 = 0) => {
      for (let c3 = i3; c3 < e4.length; c3++) {
        const i4 = e4[c3] = l3 ? zs(e4[c3]) : Ks(e4[c3]);
        _2(null, i4, t3, n2, o2, r2, s3, a3, l3);
      }
    }, P2 = (e4, t3, o2, r2, s3, a3, l3) => {
      const c3 = t3.el = e4.el;
      let { patchFlag: d3, dynamicChildren: u2, dirs: p2 } = t3;
      d3 |= 16 & e4.patchFlag;
      const f3 = e4.props || n, h3 = t3.props || n;
      let m3;
      if (o2 && is(o2, false), (m3 = h3.onVnodeBeforeUpdate) && Ws(m3, o2, t3, e4), p2 && _o(t3, e4, o2, "beforeUpdate"), o2 && is(o2, true), "production" !== process.env.NODE_ENV && _n && (d3 = 0, l3 = false, u2 = null), u2 ? (F2(e4.dynamicChildren, u2, c3, o2, r2, ls(t3, s3), a3), "production" !== process.env.NODE_ENV && cs(e4, t3)) : l3 || U2(e4, t3, c3, null, o2, r2, ls(t3, s3), a3, false), d3 > 0) {
        if (16 & d3)
          R2(c3, t3, f3, h3, o2, r2, s3);
        else if (2 & d3 && f3.class !== h3.class && i2(c3, "class", null, h3.class, s3), 4 & d3 && i2(c3, "style", f3.style, h3.style, s3), 8 & d3) {
          const n2 = t3.dynamicProps;
          for (let t4 = 0; t4 < n2.length; t4++) {
            const a4 = n2[t4], l4 = f3[a4], d4 = h3[a4];
            d4 === l4 && "value" !== a4 || i2(c3, a4, l4, d4, s3, e4.children, o2, r2, ee2);
          }
        }
        1 & d3 && e4.children !== t3.children && v2(c3, t3.children);
      } else
        l3 || null != u2 || R2(c3, t3, f3, h3, o2, r2, s3);
      ((m3 = h3.onVnodeUpdated) || p2) && ss(() => {
        m3 && Ws(m3, o2, t3, e4), p2 && _o(t3, e4, o2, "updated");
      }, r2);
    }, F2 = (e4, t3, n2, o2, r2, s3, a3) => {
      for (let l3 = 0; l3 < t3.length; l3++) {
        const i3 = e4[l3], c3 = t3[l3], d3 = i3.el && (i3.type === bs || !Ds(i3, c3) || 70 & i3.shapeFlag) ? m2(i3.el) : n2;
        _2(i3, c3, d3, null, o2, r2, s3, a3, true);
      }
    }, R2 = (e4, t3, o2, r2, s3, a3, l3) => {
      if (o2 !== r2) {
        if (o2 !== n)
          for (const n2 in o2)
            S(n2) || n2 in r2 || i2(e4, n2, o2[n2], null, l3, t3.children, s3, a3, ee2);
        for (const n2 in r2) {
          if (S(n2))
            continue;
          const c3 = r2[n2], d3 = o2[n2];
          c3 !== d3 && "value" !== n2 && i2(e4, n2, d3, c3, l3, t3.children, s3, a3, ee2);
        }
        "value" in r2 && i2(e4, "value", o2.value, r2.value, l3);
      }
    }, $2 = (e4, t3, n2, o2, r2, s3, l3, i3, c3) => {
      const u2 = t3.el = e4 ? e4.el : d2(""), p2 = t3.anchor = e4 ? e4.anchor : d2("");
      let { patchFlag: f3, dynamicChildren: h3, slotScopeIds: v3 } = t3;
      "production" !== process.env.NODE_ENV && (_n || 2048 & f3) && (f3 = 0, c3 = false, h3 = null), v3 && (i3 = i3 ? i3.concat(v3) : v3), null == e4 ? (a2(u2, n2, o2), a2(p2, n2, o2), T2(t3.children || [], n2, p2, r2, s3, l3, i3, c3)) : f3 > 0 && 64 & f3 && h3 && e4.dynamicChildren ? (F2(e4.dynamicChildren, h3, n2, r2, s3, l3, i3), "production" !== process.env.NODE_ENV ? cs(e4, t3) : (null != t3.key || r2 && t3 === r2.subTree) && cs(e4, t3, true)) : U2(e4, t3, n2, p2, r2, s3, l3, i3, c3);
    }, H2 = (e4, t3, n2, o2, r2, s3, a3, l3, i3) => {
      t3.slotScopeIds = l3, null == e4 ? 512 & t3.shapeFlag ? r2.ctx.activate(t3, n2, o2, a3, i3) : j2(t3, n2, o2, r2, s3, a3, i3) : K2(e4, t3, i3);
    }, j2 = (e4, t3, o2, s3, a3, l3, i3) => {
      const c3 = e4.component = function(e5, t4, o3) {
        const s4 = e5.type, a4 = (t4 ? t4.appContext : e5.appContext) || Ys, l4 = { uid: Gs++, vnode: e5, type: s4, parent: t4, appContext: a4, root: null, next: null, subTree: null, effect: null, update: null, scope: new se(true), render: null, proxy: null, exposed: null, exposeProxy: null, withProxy: null, provides: t4 ? t4.provides : Object.create(a4.provides), accessCache: null, renderCache: [], components: null, directives: null, propsOptions: Lr(s4, a4), emitsOptions: jn(s4, a4), emit: null, emitted: null, propsDefaults: n, inheritAttrs: s4.inheritAttrs, ctx: n, data: n, props: n, attrs: n, slots: n, refs: n, setupState: n, setupContext: null, attrsProxy: null, slotsProxy: null, suspense: o3, suspenseId: o3 ? o3.pendingId : 0, asyncDep: null, asyncResolved: false, isMounted: false, isUnmounted: false, isDeactivated: false, bc: null, c: null, bm: null, m: null, bu: null, u: null, um: null, bum: null, da: null, a: null, rtg: null, rtc: null, ec: null, sp: null };
        "production" !== process.env.NODE_ENV ? l4.ctx = function(e6) {
          const t5 = {};
          return Object.defineProperty(t5, "_", { configurable: true, enumerable: false, get: () => e6 }), Object.keys(sr).forEach((n2) => {
            Object.defineProperty(t5, n2, { configurable: true, enumerable: false, get: () => sr[n2](e6), set: r });
          }), t5;
        }(l4) : l4.ctx = { _: l4 };
        l4.root = t4 ? t4.root : l4, l4.emit = Hn.bind(null, l4), e5.ce && e5.ce(l4);
        return l4;
      }(e4, s3, a3);
      if ("production" !== process.env.NODE_ENV && c3.type.__hmrId && function(e5) {
        const t4 = e5.type.__hmrId;
        let n2 = xn.get(t4);
        n2 || (kn(t4, e5.type), n2 = xn.get(t4)), n2.instances.add(e5);
      }(c3), "production" !== process.env.NODE_ENV && (Zt(e4), ns(c3, "mount")), Fo(e4) && (c3.ctx.renderer = re2), "production" !== process.env.NODE_ENV && ns(c3, "init"), function(e5, t4 = false) {
        t4 && Qs(t4);
        const { props: n2, children: o3 } = e5.vnode, s4 = ra(e5);
        Dr(e5, n2, s4, t4), Xr(e5, o3);
        const a4 = s4 ? function(e6, t5) {
          var n3;
          const o4 = e6.type;
          if ("production" !== process.env.NODE_ENV) {
            if (o4.name && oa(o4.name, e6.appContext.config), o4.components) {
              const t6 = Object.keys(o4.components);
              for (let n4 = 0; n4 < t6.length; n4++)
                oa(t6[n4], e6.appContext.config);
            }
            if (o4.directives) {
              const e7 = Object.keys(o4.directives);
              for (let t6 = 0; t6 < e7.length; t6++)
                bo(e7[t6]);
            }
            o4.compilerOptions && ia() && Wt('"compilerOptions" is only supported when using a build of Vue that includes the runtime compiler. Since you are using a runtime-only build, the options should be passed via your build tool config instead.');
          }
          e6.accessCache = /* @__PURE__ */ Object.create(null), e6.proxy = new Proxy(e6.ctx, ir), "production" === process.env.NODE_ENV || function(e7) {
            const { ctx: t6, propsOptions: [n4] } = e7;
            n4 && Object.keys(n4).forEach((n5) => {
              Object.defineProperty(t6, n5, { enumerable: true, configurable: true, get: () => e7.props[n5], set: r });
            });
          }(e6);
          const { setup: s5 } = o4;
          if (s5) {
            const r2 = e6.setupContext = s5.length > 1 ? function(e7) {
              const t6 = (t7) => {
                if ("production" !== process.env.NODE_ENV && (e7.exposed && Wt("expose() should be called only once per setup()."), null != t7)) {
                  let e8 = typeof t7;
                  "object" === e8 && (p(t7) ? e8 = "array" : Mt(t7) && (e8 = "ref")), "object" !== e8 && Wt(`expose() should be passed a plain object, received ${e8}.`);
                }
                e7.exposed = t7 || {};
              };
              if ("production" !== process.env.NODE_ENV) {
                let n4;
                return Object.freeze({ get attrs() {
                  return n4 || (n4 = new Proxy(e7.attrs, da));
                }, get slots() {
                  return function(e8) {
                    return e8.slotsProxy || (e8.slotsProxy = new Proxy(e8.slots, { get: (t7, n5) => (Oe(e8, "get", "$slots"), t7[n5]) }));
                  }(e7);
                }, get emit() {
                  return (t7, ...n5) => e7.emit(t7, ...n5);
                }, expose: t6 });
              }
              return { attrs: new Proxy(e7.attrs, da), slots: e7.slots, emit: e7.emit, expose: t6 };
            }(e6) : null, a5 = ea(e6);
            ge();
            const l4 = Xt(s5, e6, 0, ["production" !== process.env.NODE_ENV ? gt(e6.props) : e6.props, r2]);
            if (ye(), a5(), b(l4)) {
              if (l4.then(ta, ta), t5)
                return l4.then((n4) => {
                  la(e6, n4, t5);
                }).catch((t6) => {
                  Qt(t6, e6, 0);
                });
              if (e6.asyncDep = l4, "production" !== process.env.NODE_ENV && !e6.suspense) {
                Wt(`Component <${null != (n3 = o4.name) ? n3 : "Anonymous"}>: setup function returned a promise, but no <Suspense> boundary was found in the parent component tree. A component with async setup() must be nested in a <Suspense> in order to be rendered.`);
              }
            } else
              la(e6, l4, t5);
          } else
            ca(e6, t5);
        }(e5, t4) : void 0;
        t4 && Qs(false);
      }(c3), "production" !== process.env.NODE_ENV && os(c3, "init"), c3.asyncDep) {
        if (a3 && a3.registerDep(c3, z2), !e4.el) {
          const e5 = c3.subTree = Ps(_s);
          x2(null, e5, t3, o2);
        }
      } else
        z2(c3, e4, t3, o2, a3, l3, i3);
      "production" !== process.env.NODE_ENV && (Ut(), os(c3, "mount"));
    }, K2 = (e4, t3, n2) => {
      const o2 = t3.component = e4.component;
      if (function(e5, t4, n3) {
        const { props: o3, children: r2, component: s3 } = e5, { props: a3, children: l3, patchFlag: i3 } = t4, c3 = s3.emitsOptions;
        if ("production" !== process.env.NODE_ENV && (r2 || l3) && _n)
          return true;
        if (t4.dirs || t4.transition)
          return true;
        if (!(n3 && i3 >= 0))
          return !(!r2 && !l3 || l3 && l3.$stable) || o3 !== a3 && (o3 ? !a3 || no(o3, a3, c3) : !!a3);
        if (1024 & i3)
          return true;
        if (16 & i3)
          return o3 ? no(o3, a3, c3) : !!a3;
        if (8 & i3) {
          const e6 = t4.dynamicProps;
          for (let t5 = 0; t5 < e6.length; t5++) {
            const n4 = e6[t5];
            if (a3[n4] !== o3[n4] && !Kn(c3, n4))
              return true;
          }
        }
        return false;
      }(e4, t3, n2)) {
        if (o2.asyncDep && !o2.asyncResolved)
          return "production" !== process.env.NODE_ENV && Zt(t3), Z2(o2, t3, n2), void ("production" !== process.env.NODE_ENV && Ut());
        o2.next = t3, function(e5) {
          const t4 = nn.indexOf(e5);
          t4 > on && nn.splice(t4, 1);
        }(o2.update), o2.effect.dirty = true, o2.update();
      } else
        t3.el = e4.el, o2.vnode = t3;
    }, z2 = (e4, t3, n2, o2, s3, a3, l3) => {
      const i3 = () => {
        if (e4.isMounted) {
          let { next: t4, bu: n3, u: o3, parent: r2, vnode: c4 } = e4;
          {
            const n4 = ds(e4);
            if (n4)
              return t4 && (t4.el = c4.el, Z2(e4, t4, l3)), void n4.asyncDep.then(() => {
                e4.isUnmounted || i3();
              });
          }
          let d4, u2 = t4;
          "production" !== process.env.NODE_ENV && Zt(t4 || e4.vnode), is(e4, false), t4 ? (t4.el = c4.el, Z2(e4, t4, l3)) : t4 = c4, n3 && L(n3), (d4 = t4.props && t4.props.onVnodeBeforeUpdate) && Ws(d4, r2, t4, c4), is(e4, true), "production" !== process.env.NODE_ENV && ns(e4, "render");
          const p2 = qn(e4);
          "production" !== process.env.NODE_ENV && os(e4, "render");
          const f3 = e4.subTree;
          e4.subTree = p2, "production" !== process.env.NODE_ENV && ns(e4, "patch"), _2(f3, p2, m2(f3.el), te2(f3), e4, s3, a3), "production" !== process.env.NODE_ENV && os(e4, "patch"), t4.el = p2.el, null === u2 && function({ vnode: e5, parent: t5 }, n4) {
            for (; t5; ) {
              const o4 = t5.subTree;
              if (o4.suspense && o4.suspense.activeBranch === e5 && (o4.el = e5.el), o4 !== e5)
                break;
              (e5 = t5.vnode).el = n4, t5 = t5.parent;
            }
          }(e4, p2.el), o3 && ss(o3, s3), (d4 = t4.props && t4.props.onVnodeUpdated) && ss(() => Ws(d4, r2, t4, c4), s3), "production" !== process.env.NODE_ENV && Tn(e4), "production" !== process.env.NODE_ENV && Ut();
        } else {
          let r2;
          const { el: l4, props: i4 } = t3, { bm: c4, m: d4, parent: u2 } = e4, p2 = Po(t3);
          if (is(e4, false), c4 && L(c4), !p2 && (r2 = i4 && i4.onVnodeBeforeMount) && Ws(r2, u2, t3), is(e4, true), l4 && le2) {
            const n3 = () => {
              "production" !== process.env.NODE_ENV && ns(e4, "render"), e4.subTree = qn(e4), "production" !== process.env.NODE_ENV && os(e4, "render"), "production" !== process.env.NODE_ENV && ns(e4, "hydrate"), le2(l4, e4.subTree, e4, s3, null), "production" !== process.env.NODE_ENV && os(e4, "hydrate");
            };
            p2 ? t3.type.__asyncLoader().then(() => !e4.isUnmounted && n3()) : n3();
          } else {
            "production" !== process.env.NODE_ENV && ns(e4, "render");
            const r3 = e4.subTree = qn(e4);
            "production" !== process.env.NODE_ENV && os(e4, "render"), "production" !== process.env.NODE_ENV && ns(e4, "patch"), _2(null, r3, n2, o2, e4, s3, a3), "production" !== process.env.NODE_ENV && os(e4, "patch"), t3.el = r3.el;
          }
          if (d4 && ss(d4, s3), !p2 && (r2 = i4 && i4.onVnodeMounted)) {
            const e5 = t3;
            ss(() => Ws(r2, u2, e5), s3);
          }
          (256 & t3.shapeFlag || u2 && Po(u2.vnode) && 256 & u2.vnode.shapeFlag) && e4.a && ss(e4.a, s3), e4.isMounted = true, "production" !== process.env.NODE_ENV && Vn(e4), t3 = n2 = o2 = null;
        }
      }, c3 = e4.effect = new ce(i3, r, () => pn(d3), e4.scope), d3 = e4.update = () => {
        c3.dirty && c3.run();
      };
      d3.id = e4.uid, is(e4, true), "production" !== process.env.NODE_ENV && (c3.onTrack = e4.rtc ? (t4) => L(e4.rtc, t4) : void 0, c3.onTrigger = e4.rtg ? (t4) => L(e4.rtg, t4) : void 0, d3.ownerInstance = e4), d3();
    }, Z2 = (e4, t3, n2) => {
      t3.component = e4;
      const o2 = e4.vnode.props;
      e4.vnode = t3, e4.next = null, function(e5, t4, n3, o3) {
        const { props: r2, attrs: s3, vnode: { patchFlag: a3 } } = e5, l3 = xt(r2), [i3] = e5.propsOptions;
        let c3 = false;
        if ("production" !== process.env.NODE_ENV && function(e6) {
          for (; e6; ) {
            if (e6.type.__hmrId)
              return true;
            e6 = e6.parent;
          }
        }(e5) || !(o3 || a3 > 0) || 16 & a3) {
          let o4;
          Vr(e5, t4, r2, s3) && (c3 = true);
          for (const s4 in l3)
            t4 && (u(t4, s4) || (o4 = A(s4)) !== s4 && u(t4, o4)) || (i3 ? !n3 || void 0 === n3[s4] && void 0 === n3[o4] || (r2[s4] = Tr(i3, l3, s4, void 0, e5, true)) : delete r2[s4]);
          if (s3 !== l3)
            for (const e6 in s3)
              t4 && u(t4, e6) || (delete s3[e6], c3 = true);
        } else if (8 & a3) {
          const n4 = e5.vnode.dynamicProps;
          for (let o4 = 0; o4 < n4.length; o4++) {
            let a4 = n4[o4];
            if (Kn(e5.emitsOptions, a4))
              continue;
            const d3 = t4[a4];
            if (i3)
              if (u(s3, a4))
                d3 !== s3[a4] && (s3[a4] = d3, c3 = true);
              else {
                const t5 = N(a4);
                r2[t5] = Tr(i3, l3, t5, d3, e5, false);
              }
            else
              d3 !== s3[a4] && (s3[a4] = d3, c3 = true);
          }
        }
        c3 && Ne(e5.attrs, "set", ""), "production" !== process.env.NODE_ENV && Br(t4 || {}, r2, e5);
      }(e4, t3.props, o2, n2), Jr(e4, t3.children, n2), ge(), vn(e4), ye();
    }, U2 = (e4, t3, n2, o2, r2, s3, a3, l3, i3 = false) => {
      const c3 = e4 && e4.children, d3 = e4 ? e4.shapeFlag : 0, u2 = t3.children, { patchFlag: p2, shapeFlag: f3 } = t3;
      if (p2 > 0) {
        if (128 & p2)
          return void Y2(c3, u2, n2, o2, r2, s3, a3, l3, i3);
        if (256 & p2)
          return void W2(c3, u2, n2, o2, r2, s3, a3, l3, i3);
      }
      8 & f3 ? (16 & d3 && ee2(c3, r2, s3), u2 !== c3 && v2(n2, u2)) : 16 & d3 ? 16 & f3 ? Y2(c3, u2, n2, o2, r2, s3, a3, l3, i3) : ee2(c3, r2, s3, true) : (8 & d3 && v2(n2, ""), 16 & f3 && T2(u2, n2, o2, r2, s3, a3, l3, i3));
    }, W2 = (e4, t3, n2, r2, s3, a3, l3, i3, c3) => {
      t3 = t3 || o;
      const d3 = (e4 = e4 || o).length, u2 = t3.length, p2 = Math.min(d3, u2);
      let f3;
      for (f3 = 0; f3 < p2; f3++) {
        const o2 = t3[f3] = c3 ? zs(t3[f3]) : Ks(t3[f3]);
        _2(e4[f3], o2, n2, null, s3, a3, l3, i3, c3);
      }
      d3 > u2 ? ee2(e4, s3, a3, true, false, p2) : T2(t3, n2, r2, s3, a3, l3, i3, c3, p2);
    }, Y2 = (e4, t3, n2, r2, s3, a3, l3, i3, c3) => {
      let d3 = 0;
      const u2 = t3.length;
      let p2 = e4.length - 1, f3 = u2 - 1;
      for (; d3 <= p2 && d3 <= f3; ) {
        const o2 = e4[d3], r3 = t3[d3] = c3 ? zs(t3[d3]) : Ks(t3[d3]);
        if (!Ds(o2, r3))
          break;
        _2(o2, r3, n2, null, s3, a3, l3, i3, c3), d3++;
      }
      for (; d3 <= p2 && d3 <= f3; ) {
        const o2 = e4[p2], r3 = t3[f3] = c3 ? zs(t3[f3]) : Ks(t3[f3]);
        if (!Ds(o2, r3))
          break;
        _2(o2, r3, n2, null, s3, a3, l3, i3, c3), p2--, f3--;
      }
      if (d3 > p2) {
        if (d3 <= f3) {
          const e5 = f3 + 1, o2 = e5 < u2 ? t3[e5].el : r2;
          for (; d3 <= f3; )
            _2(null, t3[d3] = c3 ? zs(t3[d3]) : Ks(t3[d3]), n2, o2, s3, a3, l3, i3, c3), d3++;
        }
      } else if (d3 > f3)
        for (; d3 <= p2; )
          q2(e4[d3], s3, a3, true), d3++;
      else {
        const h3 = d3, v3 = d3, m3 = /* @__PURE__ */ new Map();
        for (d3 = v3; d3 <= f3; d3++) {
          const e5 = t3[d3] = c3 ? zs(t3[d3]) : Ks(t3[d3]);
          null != e5.key && ("production" !== process.env.NODE_ENV && m3.has(e5.key) && Wt("Duplicate keys found during update:", JSON.stringify(e5.key), "Make sure keys are unique."), m3.set(e5.key, d3));
        }
        let g3, y3 = 0;
        const b2 = f3 - v3 + 1;
        let w3 = false, C3 = 0;
        const x3 = new Array(b2);
        for (d3 = 0; d3 < b2; d3++)
          x3[d3] = 0;
        for (d3 = h3; d3 <= p2; d3++) {
          const o2 = e4[d3];
          if (y3 >= b2) {
            q2(o2, s3, a3, true);
            continue;
          }
          let r3;
          if (null != o2.key)
            r3 = m3.get(o2.key);
          else
            for (g3 = v3; g3 <= f3; g3++)
              if (0 === x3[g3 - v3] && Ds(o2, t3[g3])) {
                r3 = g3;
                break;
              }
          void 0 === r3 ? q2(o2, s3, a3, true) : (x3[r3 - v3] = d3 + 1, r3 >= C3 ? C3 = r3 : w3 = true, _2(o2, t3[r3], n2, null, s3, a3, l3, i3, c3), y3++);
        }
        const k3 = w3 ? function(e5) {
          const t4 = e5.slice(), n3 = [0];
          let o2, r3, s4, a4, l4;
          const i4 = e5.length;
          for (o2 = 0; o2 < i4; o2++) {
            const i5 = e5[o2];
            if (0 !== i5) {
              if (r3 = n3[n3.length - 1], e5[r3] < i5) {
                t4[o2] = r3, n3.push(o2);
                continue;
              }
              for (s4 = 0, a4 = n3.length - 1; s4 < a4; )
                l4 = s4 + a4 >> 1, e5[n3[l4]] < i5 ? s4 = l4 + 1 : a4 = l4;
              i5 < e5[n3[s4]] && (s4 > 0 && (t4[o2] = n3[s4 - 1]), n3[s4] = o2);
            }
          }
          s4 = n3.length, a4 = n3[s4 - 1];
          for (; s4-- > 0; )
            n3[s4] = a4, a4 = t4[a4];
          return n3;
        }(x3) : o;
        for (g3 = k3.length - 1, d3 = b2 - 1; d3 >= 0; d3--) {
          const e5 = v3 + d3, o2 = t3[e5], p3 = e5 + 1 < u2 ? t3[e5 + 1].el : r2;
          0 === x3[d3] ? _2(null, o2, n2, p3, s3, a3, l3, i3, c3) : w3 && (g3 < 0 || d3 !== k3[g3] ? G2(o2, n2, p3, 2) : g3--);
        }
      }
    }, G2 = (e4, t3, n2, o2, r2 = null) => {
      const { el: s3, type: l3, transition: i3, children: c3, shapeFlag: d3 } = e4;
      if (6 & d3)
        return void G2(e4.component.subTree, t3, n2, o2);
      if (128 & d3)
        return void e4.suspense.move(t3, n2, o2);
      if (64 & d3)
        return void l3.move(e4, t3, n2, re2);
      if (l3 === bs) {
        a2(s3, t3, n2);
        for (let e5 = 0; e5 < c3.length; e5++)
          G2(c3[e5], t3, n2, o2);
        return void a2(e4.anchor, t3, n2);
      }
      if (l3 === Cs)
        return void I2(e4, t3, n2);
      if (2 !== o2 && 1 & d3 && i3)
        if (0 === o2)
          i3.beforeEnter(s3), a2(s3, t3, n2), ss(() => i3.enter(s3), r2);
        else {
          const { leave: e5, delayLeave: o3, afterLeave: r3 } = i3, l4 = () => a2(s3, t3, n2), c4 = () => {
            e5(s3, () => {
              l4(), r3 && r3();
            });
          };
          o3 ? o3(s3, l4, c4) : c4();
        }
      else
        a2(s3, t3, n2);
    }, q2 = (e4, t3, n2, o2 = false, r2 = false) => {
      const { type: s3, props: a3, ref: l3, children: i3, dynamicChildren: c3, shapeFlag: d3, patchFlag: u2, dirs: p2 } = e4;
      if (null != l3 && Qr(l3, null, n2, e4, true), 256 & d3)
        return void t3.ctx.deactivate(e4);
      const f3 = 1 & d3 && p2, h3 = !Po(e4);
      let v3;
      if (h3 && (v3 = a3 && a3.onVnodeBeforeUnmount) && Ws(v3, t3, e4), 6 & d3)
        Q2(e4.component, n2, o2);
      else {
        if (128 & d3)
          return void e4.suspense.unmount(n2, o2);
        f3 && _o(e4, null, t3, "beforeUnmount"), 64 & d3 ? e4.type.remove(e4, t3, n2, r2, re2, o2) : c3 && (s3 !== bs || u2 > 0 && 64 & u2) ? ee2(c3, t3, n2, false, true) : (s3 === bs && 384 & u2 || !r2 && 16 & d3) && ee2(i3, t3, n2), o2 && X2(e4);
      }
      (h3 && (v3 = a3 && a3.onVnodeUnmounted) || f3) && ss(() => {
        v3 && Ws(v3, t3, e4), f3 && _o(e4, null, t3, "unmounted");
      }, n2);
    }, X2 = (e4) => {
      const { type: t3, el: n2, anchor: o2, transition: r2 } = e4;
      if (t3 === bs)
        return void ("production" !== process.env.NODE_ENV && e4.patchFlag > 0 && 2048 & e4.patchFlag && r2 && !r2.persisted ? e4.children.forEach((e5) => {
          e5.type === _s ? l2(e5.el) : X2(e5);
        }) : J2(n2, o2));
      if (t3 === Cs)
        return void O2(e4);
      const s3 = () => {
        l2(n2), r2 && !r2.persisted && r2.afterLeave && r2.afterLeave();
      };
      if (1 & e4.shapeFlag && r2 && !r2.persisted) {
        const { leave: t4, delayLeave: o3 } = r2, a3 = () => t4(n2, s3);
        o3 ? o3(e4.el, s3, a3) : a3();
      } else
        s3();
    }, J2 = (e4, t3) => {
      let n2;
      for (; e4 !== t3; )
        n2 = g2(e4), l2(e4), e4 = n2;
      l2(t3);
    }, Q2 = (e4, t3, n2) => {
      "production" !== process.env.NODE_ENV && e4.type.__hmrId && function(e5) {
        xn.get(e5.type.__hmrId).instances.delete(e5);
      }(e4);
      const { bum: o2, scope: r2, update: s3, subTree: a3, um: l3 } = e4;
      o2 && L(o2), r2.stop(), s3 && (s3.active = false, q2(a3, e4, t3, n2)), l3 && ss(l3, t3), ss(() => {
        e4.isUnmounted = true;
      }, t3), t3 && t3.pendingBranch && !t3.isUnmounted && e4.asyncDep && !e4.asyncResolved && e4.suspenseId === t3.pendingId && (t3.deps--, 0 === t3.deps && t3.resolve()), "production" !== process.env.NODE_ENV && Pn(e4);
    }, ee2 = (e4, t3, n2, o2 = false, r2 = false, s3 = 0) => {
      for (let a3 = s3; a3 < e4.length; a3++)
        q2(e4[a3], t3, n2, o2, r2);
    }, te2 = (e4) => 6 & e4.shapeFlag ? te2(e4.component.subTree) : 128 & e4.shapeFlag ? e4.suspense.next() : g2(e4.anchor || e4.el);
    let ne2 = false;
    const oe2 = (e4, t3, n2) => {
      null == e4 ? t3._vnode && q2(t3._vnode, null, null, true) : _2(t3._vnode || null, e4, t3, null, null, null, n2), ne2 || (ne2 = true, vn(), mn(), ne2 = false), t3._vnode = e4;
    }, re2 = { p: _2, um: q2, m: G2, r: X2, mt: j2, mc: T2, pc: U2, pbc: F2, n: te2, o: e3 };
    let ae2, le2;
    t2 && ([ae2, le2] = t2(re2));
    return { render: oe2, hydrate: ae2, createApp: Sr(oe2, ae2) };
  }(e2);
}
function ls({ type: e2, props: t2 }, n2) {
  return "svg" === n2 && "foreignObject" === e2 || "mathml" === n2 && "annotation-xml" === e2 && t2 && t2.encoding && t2.encoding.includes("html") ? void 0 : n2;
}
function is({ effect: e2, update: t2 }, n2) {
  e2.allowRecurse = t2.allowRecurse = n2;
}
function cs(e2, t2, n2 = false) {
  const o2 = e2.children, r2 = t2.children;
  if (p(o2) && p(r2))
    for (let e3 = 0; e3 < o2.length; e3++) {
      const t3 = o2[e3];
      let s2 = r2[e3];
      1 & s2.shapeFlag && !s2.dynamicChildren && ((s2.patchFlag <= 0 || 32 === s2.patchFlag) && (s2 = r2[e3] = zs(r2[e3]), s2.el = t3.el), n2 || cs(t3, s2)), s2.type === ws && (s2.el = t3.el), "production" === process.env.NODE_ENV || s2.type !== _s || s2.el || (s2.el = t3.el);
    }
}
function ds(e2) {
  const t2 = e2.subTree.component;
  if (t2)
    return t2.asyncDep && !t2.asyncResolved ? t2 : ds(t2);
}
const us = (e2) => e2 && (e2.disabled || "" === e2.disabled), ps = (e2) => "undefined" != typeof SVGElement && e2 instanceof SVGElement, fs = (e2) => "function" == typeof MathMLElement && e2 instanceof MathMLElement, hs = (e2, t2) => {
  const n2 = e2 && e2.to;
  if (m(n2)) {
    if (t2) {
      const e3 = t2(n2);
      return e3 || "production" !== process.env.NODE_ENV && Wt(`Failed to locate Teleport target with selector "${n2}". Note the target element must exist before the component is mounted - i.e. the target cannot be rendered by the component itself, and ideally should be outside of the entire Vue component tree.`), e3;
    }
    return "production" !== process.env.NODE_ENV && Wt("Current renderer does not support string target for Teleports. (missing querySelector renderer option)"), null;
  }
  return "production" === process.env.NODE_ENV || n2 || us(e2) || Wt(`Invalid Teleport target: ${n2}`), n2;
}, vs = { name: "Teleport", __isTeleport: true, process(e2, t2, n2, o2, r2, s2, a2, l2, i2, c2) {
  const { mc: d2, pc: u2, pbc: p2, o: { insert: f2, querySelector: h2, createText: v2, createComment: m2 } } = c2, g2 = us(t2.props);
  let { shapeFlag: y2, children: b2, dynamicChildren: w2 } = t2;
  if ("production" !== process.env.NODE_ENV && _n && (i2 = false, w2 = null), null == e2) {
    const e3 = t2.el = "production" !== process.env.NODE_ENV ? m2("teleport start") : v2(""), c3 = t2.anchor = "production" !== process.env.NODE_ENV ? m2("teleport end") : v2("");
    f2(e3, n2, o2), f2(c3, n2, o2);
    const u3 = t2.target = hs(t2.props, h2), p3 = t2.targetAnchor = v2("");
    u3 ? (f2(p3, u3), "svg" === a2 || ps(u3) ? a2 = "svg" : ("mathml" === a2 || fs(u3)) && (a2 = "mathml")) : "production" === process.env.NODE_ENV || g2 || Wt("Invalid Teleport target on mount:", u3, `(${typeof u3})`);
    const w3 = (e4, t3) => {
      16 & y2 && d2(b2, e4, t3, r2, s2, a2, l2, i2);
    };
    g2 ? w3(n2, c3) : u3 && w3(u3, p3);
  } else {
    t2.el = e2.el;
    const o3 = t2.anchor = e2.anchor, d3 = t2.target = e2.target, f3 = t2.targetAnchor = e2.targetAnchor, v3 = us(e2.props), m3 = v3 ? n2 : d3, y3 = v3 ? o3 : f3;
    if ("svg" === a2 || ps(d3) ? a2 = "svg" : ("mathml" === a2 || fs(d3)) && (a2 = "mathml"), w2 ? (p2(e2.dynamicChildren, w2, m3, r2, s2, a2, l2), cs(e2, t2, true)) : i2 || u2(e2, t2, m3, y3, r2, s2, a2, l2, false), g2)
      v3 ? t2.props && e2.props && t2.props.to !== e2.props.to && (t2.props.to = e2.props.to) : ms(t2, n2, o3, c2, 1);
    else if ((t2.props && t2.props.to) !== (e2.props && e2.props.to)) {
      const e3 = t2.target = hs(t2.props, h2);
      e3 ? ms(t2, e3, null, c2, 0) : "production" !== process.env.NODE_ENV && Wt("Invalid Teleport target on update:", d3, `(${typeof d3})`);
    } else
      v3 && ms(t2, d3, f3, c2, 1);
  }
  ys(t2);
}, remove(e2, t2, n2, o2, { um: r2, o: { remove: s2 } }, a2) {
  const { shapeFlag: l2, children: i2, anchor: c2, targetAnchor: d2, target: u2, props: p2 } = e2;
  if (u2 && s2(d2), a2 && s2(c2), 16 & l2) {
    const e3 = a2 || !us(p2);
    for (let o3 = 0; o3 < i2.length; o3++) {
      const s3 = i2[o3];
      r2(s3, t2, n2, e3, !!s3.dynamicChildren);
    }
  }
}, move: ms, hydrate: function(e2, t2, n2, o2, r2, s2, { o: { nextSibling: a2, parentNode: l2, querySelector: i2 } }, c2) {
  const d2 = t2.target = hs(t2.props, i2);
  if (d2) {
    const i3 = d2._lpa || d2.firstChild;
    if (16 & t2.shapeFlag)
      if (us(t2.props))
        t2.anchor = c2(a2(e2), t2, l2(e2), n2, o2, r2, s2), t2.targetAnchor = i3;
      else {
        t2.anchor = a2(e2);
        let l3 = i3;
        for (; l3; )
          if (l3 = a2(l3), l3 && 8 === l3.nodeType && "teleport anchor" === l3.data) {
            t2.targetAnchor = l3, d2._lpa = t2.targetAnchor && a2(t2.targetAnchor);
            break;
          }
        c2(i3, t2, d2, n2, o2, r2, s2);
      }
    ys(t2);
  }
  return t2.anchor && a2(t2.anchor);
} };
function ms(e2, t2, n2, { o: { insert: o2 }, m: r2 }, s2 = 2) {
  0 === s2 && o2(e2.targetAnchor, t2, n2);
  const { el: a2, anchor: l2, shapeFlag: i2, children: c2, props: d2 } = e2, u2 = 2 === s2;
  if (u2 && o2(a2, t2, n2), (!u2 || us(d2)) && 16 & i2)
    for (let e3 = 0; e3 < c2.length; e3++)
      r2(c2[e3], t2, n2, 2);
  u2 && o2(l2, t2, n2);
}
const gs = vs;
function ys(e2) {
  const t2 = e2.ctx;
  if (t2 && t2.ut) {
    let n2 = e2.children[0].el;
    for (; n2 && n2 !== e2.targetAnchor; )
      1 === n2.nodeType && n2.setAttribute("data-v-owner", t2.uid), n2 = n2.nextSibling;
    t2.ut();
  }
}
const bs = Symbol.for("v-fgt"), ws = Symbol.for("v-txt"), _s = Symbol.for("v-cmt"), Cs = Symbol.for("v-stc"), xs = [];
let ks = null;
function Ss(e2 = false) {
  xs.push(ks = e2 ? null : []);
}
let Es = 1;
function Is(e2) {
  Es += e2;
}
function Os(e2) {
  return e2.dynamicChildren = Es > 0 ? ks || o : null, xs.pop(), ks = xs[xs.length - 1] || null, Es > 0 && ks && ks.push(e2), e2;
}
function Ns(e2, t2, n2, o2, r2, s2) {
  return Os(Ls(e2, t2, n2, o2, r2, s2, true));
}
function Ms(e2, t2, n2, o2, r2) {
  return Os(Ps(e2, t2, n2, o2, r2, true));
}
function As(e2) {
  return !!e2 && true === e2.__v_isVNode;
}
function Ds(e2, t2) {
  return "production" !== process.env.NODE_ENV && 6 & t2.shapeFlag && Cn.has(t2.type) ? (e2.shapeFlag &= -257, t2.shapeFlag &= -513, false) : e2.type === t2.type && e2.key === t2.key;
}
const Vs = ({ key: e2 }) => null != e2 ? e2 : null, Ts = ({ ref: e2, ref_key: t2, ref_for: n2 }) => ("number" == typeof e2 && (e2 = "" + e2), null != e2 ? m(e2) || Mt(e2) || v(e2) ? { i: zn, r: e2, k: t2, f: !!n2 } : e2 : null);
function Ls(e2, t2 = null, n2 = null, o2 = 0, r2 = null, s2 = e2 === bs ? 0 : 1, a2 = false, l2 = false) {
  const i2 = { __v_isVNode: true, __v_skip: true, type: e2, props: t2, key: t2 && Vs(t2), ref: t2 && Ts(t2), scopeId: Zn, slotScopeIds: null, children: n2, component: null, suspense: null, ssContent: null, ssFallback: null, dirs: null, transition: null, el: null, anchor: null, target: null, targetAnchor: null, staticCount: 0, shapeFlag: s2, patchFlag: o2, dynamicProps: r2, dynamicChildren: null, appContext: null, ctx: zn };
  return l2 ? (Zs(i2, n2), 128 & s2 && e2.normalize(i2)) : n2 && (i2.shapeFlag |= m(n2) ? 8 : 16), "production" !== process.env.NODE_ENV && i2.key != i2.key && Wt("VNode created with invalid key (NaN). VNode type:", i2.type), Es > 0 && !a2 && ks && (i2.patchFlag > 0 || 6 & s2) && 32 !== i2.patchFlag && ks.push(i2), i2;
}
const Ps = "production" !== process.env.NODE_ENV ? (...e2) => Fs(...e2) : Fs;
function Fs(e2, t2 = null, n2 = null, o2 = 0, r2 = null, s2 = false) {
  if (e2 && e2 !== so || ("production" === process.env.NODE_ENV || e2 || Wt(`Invalid vnode type when creating vnode: ${e2}.`), e2 = _s), As(e2)) {
    const o3 = $s(e2, t2, true);
    return n2 && Zs(o3, n2), Es > 0 && !s2 && ks && (6 & o3.shapeFlag ? ks[ks.indexOf(e2)] = o3 : ks.push(o3)), o3.patchFlag |= -2, o3;
  }
  if (ma(e2) && (e2 = e2.__vccOpts), t2) {
    t2 = Rs(t2);
    let { class: e3, style: n3 } = t2;
    e3 && !m(e3) && (t2.class = U(e3)), y(n3) && (Ct(n3) && !p(n3) && (n3 = i({}, n3)), t2.style = H(n3));
  }
  const a2 = m(e2) ? 1 : ((e3) => e3.__isSuspense)(e2) ? 128 : ((e3) => e3.__isTeleport)(e2) ? 64 : y(e2) ? 4 : v(e2) ? 2 : 0;
  return "production" !== process.env.NODE_ENV && 4 & a2 && Ct(e2) && Wt("Vue received a Component that was made a reactive object. This can lead to unnecessary performance overhead and should be avoided by marking the component with `markRaw` or using `shallowRef` instead of `ref`.", "\nComponent that was made reactive: ", e2 = xt(e2)), Ls(e2, t2, n2, o2, r2, a2, s2, true);
}
function Rs(e2) {
  return e2 ? Ct(e2) || Ar(e2) ? i({}, e2) : e2 : null;
}
function $s(e2, t2, n2 = false, o2 = false) {
  const { props: r2, ref: s2, patchFlag: a2, children: l2, transition: i2 } = e2, c2 = t2 ? Us(r2 || {}, t2) : r2, d2 = { __v_isVNode: true, __v_skip: true, type: e2.type, props: c2, key: c2 && Vs(c2), ref: t2 && t2.ref ? n2 && s2 ? p(s2) ? s2.concat(Ts(t2)) : [s2, Ts(t2)] : Ts(t2) : s2, scopeId: e2.scopeId, slotScopeIds: e2.slotScopeIds, children: "production" !== process.env.NODE_ENV && -1 === a2 && p(l2) ? l2.map(Bs) : l2, target: e2.target, targetAnchor: e2.targetAnchor, staticCount: e2.staticCount, shapeFlag: e2.shapeFlag, patchFlag: t2 && e2.type !== bs ? -1 === a2 ? 16 : 16 | a2 : a2, dynamicProps: e2.dynamicProps, dynamicChildren: e2.dynamicChildren, appContext: e2.appContext, dirs: e2.dirs, transition: i2, component: e2.component, suspense: e2.suspense, ssContent: e2.ssContent && $s(e2.ssContent), ssFallback: e2.ssFallback && $s(e2.ssFallback), el: e2.el, anchor: e2.anchor, ctx: e2.ctx, ce: e2.ce };
  return i2 && o2 && (d2.transition = i2.clone(d2)), d2;
}
function Bs(e2) {
  const t2 = $s(e2);
  return p(e2.children) && (t2.children = e2.children.map(Bs)), t2;
}
function Hs(e2 = " ", t2 = 0) {
  return Ps(ws, null, e2, t2);
}
function js(e2 = "", t2 = false) {
  return t2 ? (Ss(), Ms(_s, null, e2)) : Ps(_s, null, e2);
}
function Ks(e2) {
  return null == e2 || "boolean" == typeof e2 ? Ps(_s) : p(e2) ? Ps(bs, null, e2.slice()) : "object" == typeof e2 ? zs(e2) : Ps(ws, null, String(e2));
}
function zs(e2) {
  return null === e2.el && -1 !== e2.patchFlag || e2.memo ? e2 : $s(e2);
}
function Zs(e2, t2) {
  let n2 = 0;
  const { shapeFlag: o2 } = e2;
  if (null == t2)
    t2 = null;
  else if (p(t2))
    n2 = 16;
  else if ("object" == typeof t2) {
    if (65 & o2) {
      const n3 = t2.default;
      return void (n3 && (n3._c && (n3._d = false), Zs(e2, n3()), n3._c && (n3._d = true)));
    }
    {
      n2 = 32;
      const o3 = t2._;
      o3 || Ar(t2) ? 3 === o3 && zn && (1 === zn.slots._ ? t2._ = 1 : (t2._ = 2, e2.patchFlag |= 1024)) : t2._ctx = zn;
    }
  } else
    v(t2) ? (t2 = { default: t2, _ctx: zn }, n2 = 32) : (t2 = String(t2), 64 & o2 ? (n2 = 16, t2 = [Hs(t2)]) : n2 = 8);
  e2.children = t2, e2.shapeFlag |= n2;
}
function Us(...e2) {
  const t2 = {};
  for (let n2 = 0; n2 < e2.length; n2++) {
    const o2 = e2[n2];
    for (const e3 in o2)
      if ("class" === e3)
        t2.class !== o2.class && (t2.class = U([t2.class, o2.class]));
      else if ("style" === e3)
        t2.style = H([t2.style, o2.style]);
      else if (a(e3)) {
        const n3 = t2[e3], r2 = o2[e3];
        !r2 || n3 === r2 || p(n3) && n3.includes(r2) || (t2[e3] = n3 ? [].concat(n3, r2) : r2);
      } else
        "" !== e3 && (t2[e3] = o2[e3]);
  }
  return t2;
}
function Ws(e2, t2, n2, o2 = null) {
  Jt(e2, t2, 7, [n2, o2]);
}
const Ys = xr();
let Gs = 0;
let qs = null;
const Xs = () => qs || zn;
let Js, Qs;
{
  const e2 = B(), t2 = (t3, n2) => {
    let o2;
    return (o2 = e2[t3]) || (o2 = e2[t3] = []), o2.push(n2), (e3) => {
      o2.length > 1 ? o2.forEach((t4) => t4(e3)) : o2[0](e3);
    };
  };
  Js = t2("__VUE_INSTANCE_SETTERS__", (e3) => qs = e3), Qs = t2("__VUE_SSR_SETTERS__", (e3) => aa = e3);
}
const ea = (e2) => {
  const t2 = qs;
  return Js(e2), e2.scope.on(), () => {
    e2.scope.off(), Js(t2);
  };
}, ta = () => {
  qs && qs.scope.off(), Js(null);
}, na = t("slot,component");
function oa(e2, { isNativeTag: t2 }) {
  (na(e2) || t2(e2)) && Wt("Do not use built-in or reserved HTML elements as component id: " + e2);
}
function ra(e2) {
  return 4 & e2.vnode.shapeFlag;
}
let sa, aa = false;
function la(e2, t2, n2) {
  v(t2) ? e2.type.__ssrInlineRender ? e2.ssrRender = t2 : e2.render = t2 : y(t2) ? ("production" !== process.env.NODE_ENV && As(t2) && Wt("setup() should not return VNodes directly - return a render function instead."), "production" !== process.env.NODE_ENV && (e2.devtoolsRawSetupState = t2), e2.setupState = Ft(t2), "production" !== process.env.NODE_ENV && function(e3) {
    const { ctx: t3, setupState: n3 } = e3;
    Object.keys(xt(n3)).forEach((e4) => {
      if (!n3.__isScriptSetup) {
        if (ar(e4[0]))
          return void Wt(`setup() return property ${JSON.stringify(e4)} should not start with "$" or "_" which are reserved prefixes for Vue internals.`);
        Object.defineProperty(t3, e4, { enumerable: true, configurable: true, get: () => n3[e4], set: r });
      }
    });
  }(e2)) : "production" !== process.env.NODE_ENV && void 0 !== t2 && Wt("setup() should return an object. Received: " + (null === t2 ? "null" : typeof t2)), ca(e2, n2);
}
const ia = () => !sa;
function ca(e2, t2, n2) {
  const o2 = e2.type;
  if (!e2.render) {
    if (!t2 && sa && !o2.render) {
      const t3 = o2.template || vr(e2).template;
      if (t3) {
        "production" !== process.env.NODE_ENV && ns(e2, "compile");
        const { isCustomElement: n3, compilerOptions: r2 } = e2.appContext.config, { delimiters: s2, compilerOptions: a2 } = o2, l2 = i(i({ isCustomElement: n3, delimiters: s2 }, r2), a2);
        o2.render = sa(t3, l2), "production" !== process.env.NODE_ENV && os(e2, "compile");
      }
    }
    e2.render = o2.render || r;
  }
  {
    const t3 = ea(e2);
    ge();
    try {
      pr(e2);
    } finally {
      ye(), t3();
    }
  }
  "production" === process.env.NODE_ENV || o2.render || e2.render !== r || t2 || (o2.template ? Wt('Component provided template option but runtime compilation is not supported in this build of Vue. Configure your bundler to alias "vue" to "vue/dist/vue.esm-bundler.js".') : Wt("Component is missing template or render function."));
}
const da = "production" !== process.env.NODE_ENV ? { get: (e2, t2) => (Gn(), Oe(e2, "get", ""), e2[t2]), set: () => (Wt("setupContext.attrs is readonly."), false), deleteProperty: () => (Wt("setupContext.attrs is readonly."), false) } : { get: (e2, t2) => (Oe(e2, "get", ""), e2[t2]) };
function ua(e2) {
  if (e2.exposed)
    return e2.exposeProxy || (e2.exposeProxy = new Proxy(Ft(kt(e2.exposed)), { get: (t2, n2) => n2 in t2 ? t2[n2] : n2 in sr ? sr[n2](e2) : void 0, has: (e3, t2) => t2 in e3 || t2 in sr }));
}
const pa = /(?:^|[-_])(\w)/g, fa = (e2) => e2.replace(pa, (e3) => e3.toUpperCase()).replace(/[-_]/g, "");
function ha(e2, t2 = true) {
  return v(e2) ? e2.displayName || e2.name : e2.name || t2 && e2.__name;
}
function va(e2, t2, n2 = false) {
  let o2 = ha(t2);
  if (!o2 && t2.__file) {
    const e3 = t2.__file.match(/([^/\\]+)\.\w+$/);
    e3 && (o2 = e3[1]);
  }
  if (!o2 && e2 && e2.parent) {
    const n3 = (e3) => {
      for (const n4 in e3)
        if (e3[n4] === t2)
          return n4;
    };
    o2 = n3(e2.components || e2.parent.type.components) || n3(e2.appContext.components);
  }
  return o2 ? fa(o2) : n2 ? "App" : "Anonymous";
}
function ma(e2) {
  return v(e2) && "__vccOpts" in e2;
}
const ga = (e2, t2) => {
  const n2 = function(e3, t3, n3 = false) {
    let o2, s2;
    const a2 = v(e3);
    a2 ? (o2 = e3, s2 = "production" !== process.env.NODE_ENV ? () => {
      ne("Write operation failed: computed value is readonly");
    } : r) : (o2 = e3.get, s2 = e3.set);
    const l2 = new It(o2, s2, a2 || !s2, n3);
    return "production" !== process.env.NODE_ENV && t3 && !n3 && (l2.effect.onTrack = t3.onTrack, l2.effect.onTrigger = t3.onTrigger), l2;
  }(e2, t2, aa);
  if ("production" !== process.env.NODE_ENV) {
    const e3 = Xs();
    e3 && e3.appContext.config.warnRecursiveComputed && (n2._warnRecursive = true);
  }
  return n2;
};
function ya(e2, t2, o2 = n) {
  const r2 = Xs();
  if ("production" !== process.env.NODE_ENV && !r2)
    return Wt("useModel() called without active instance."), At();
  if ("production" !== process.env.NODE_ENV && !r2.propsOptions[0][t2])
    return Wt(`useModel() called with prop "${t2}" which is not declared.`), At();
  const s2 = N(t2), a2 = A(t2), l2 = (c2 = (n2, l3) => {
    let c3;
    return function(e3, t3) {
      vo(e3, null, "production" !== process.env.NODE_ENV ? i({}, t3, { flush: "sync" }) : { flush: "sync" });
    }(() => {
      const n3 = e2[t2];
      T(c3, n3) && (c3 = n3, l3());
    }), { get: () => (n2(), o2.get ? o2.get(c3) : c3), set(e3) {
      const n3 = r2.vnode.props;
      n3 && (t2 in n3 || s2 in n3 || a2 in n3) && (`onUpdate:${t2}` in n3 || `onUpdate:${s2}` in n3 || `onUpdate:${a2}` in n3) || !T(e3, c3) || (c3 = e3, l3()), r2.emit(`update:${t2}`, o2.set ? o2.set(e3) : e3);
    } };
  }, new Rt(c2));
  var c2;
  const d2 = "modelValue" === t2 ? "modelModifiers" : `${t2}Modifiers`;
  return l2[Symbol.iterator] = () => {
    let t3 = 0;
    return { next: () => t3 < 2 ? { value: t3++ ? e2[d2] || {} : l2, done: false } : { done: true } };
  }, l2;
}
function ba(e2, t2, n2) {
  const o2 = arguments.length;
  return 2 === o2 ? y(t2) && !p(t2) ? As(t2) ? Ps(e2, null, [t2]) : Ps(e2, t2) : Ps(e2, null, t2) : (o2 > 3 ? n2 = Array.prototype.slice.call(arguments, 2) : 3 === o2 && As(n2) && (n2 = [n2]), Ps(e2, t2, n2));
}
const wa = "3.4.26", _a = "production" !== process.env.NODE_ENV ? Wt : r;
process.env.NODE_ENV, process.env.NODE_ENV;
/**
* @vue/runtime-dom v3.4.26
* (c) 2018-present Yuxi (Evan) You and Vue contributors
* @license MIT
**/
const Ca = "undefined" != typeof document ? document : null, xa = Ca && Ca.createElement("template"), ka = { insert: (e2, t2, n2) => {
  t2.insertBefore(e2, n2 || null);
}, remove: (e2) => {
  const t2 = e2.parentNode;
  t2 && t2.removeChild(e2);
}, createElement: (e2, t2, n2, o2) => {
  const r2 = "svg" === t2 ? Ca.createElementNS("http://www.w3.org/2000/svg", e2) : "mathml" === t2 ? Ca.createElementNS("http://www.w3.org/1998/Math/MathML", e2) : Ca.createElement(e2, n2 ? { is: n2 } : void 0);
  return "select" === e2 && o2 && null != o2.multiple && r2.setAttribute("multiple", o2.multiple), r2;
}, createText: (e2) => Ca.createTextNode(e2), createComment: (e2) => Ca.createComment(e2), setText: (e2, t2) => {
  e2.nodeValue = t2;
}, setElementText: (e2, t2) => {
  e2.textContent = t2;
}, parentNode: (e2) => e2.parentNode, nextSibling: (e2) => e2.nextSibling, querySelector: (e2) => Ca.querySelector(e2), setScopeId(e2, t2) {
  e2.setAttribute(t2, "");
}, insertStaticContent(e2, t2, n2, o2, r2, s2) {
  const a2 = n2 ? n2.previousSibling : t2.lastChild;
  if (r2 && (r2 === s2 || r2.nextSibling))
    for (; t2.insertBefore(r2.cloneNode(true), n2), r2 !== s2 && (r2 = r2.nextSibling); )
      ;
  else {
    xa.innerHTML = "svg" === o2 ? `<svg>${e2}</svg>` : "mathml" === o2 ? `<math>${e2}</math>` : e2;
    const r3 = xa.content;
    if ("svg" === o2 || "mathml" === o2) {
      const e3 = r3.firstChild;
      for (; e3.firstChild; )
        r3.appendChild(e3.firstChild);
      r3.removeChild(e3);
    }
    t2.insertBefore(r3, n2);
  }
  return [a2 ? a2.nextSibling : t2.firstChild, n2 ? n2.previousSibling : t2.lastChild];
} }, Sa = "transition", Ea = "animation", Ia = Symbol("_vtc"), Oa = (e2, { slots: t2 }) => ba(Oo, Va(e2), t2);
Oa.displayName = "Transition";
const Na = { name: String, type: String, css: { type: Boolean, default: true }, duration: [String, Number, Object], enterFromClass: String, enterActiveClass: String, enterToClass: String, appearFromClass: String, appearActiveClass: String, appearToClass: String, leaveFromClass: String, leaveActiveClass: String, leaveToClass: String }, Ma = Oa.props = i({}, Eo, Na), Aa = (e2, t2 = []) => {
  p(e2) ? e2.forEach((e3) => e3(...t2)) : e2 && e2(...t2);
}, Da = (e2) => !!e2 && (p(e2) ? e2.some((e3) => e3.length > 1) : e2.length > 1);
function Va(e2) {
  const t2 = {};
  for (const n3 in e2)
    n3 in Na || (t2[n3] = e2[n3]);
  if (false === e2.css)
    return t2;
  const { name: n2 = "v", type: o2, duration: r2, enterFromClass: s2 = `${n2}-enter-from`, enterActiveClass: a2 = `${n2}-enter-active`, enterToClass: l2 = `${n2}-enter-to`, appearFromClass: c2 = s2, appearActiveClass: d2 = a2, appearToClass: u2 = l2, leaveFromClass: p2 = `${n2}-leave-from`, leaveActiveClass: f2 = `${n2}-leave-active`, leaveToClass: h2 = `${n2}-leave-to` } = e2, v2 = function(e3) {
    if (null == e3)
      return null;
    if (y(e3))
      return [Ta(e3.enter), Ta(e3.leave)];
    {
      const t3 = Ta(e3);
      return [t3, t3];
    }
  }(r2), m2 = v2 && v2[0], g2 = v2 && v2[1], { onBeforeEnter: b2, onEnter: w2, onEnterCancelled: _2, onLeave: C2, onLeaveCancelled: x2, onBeforeAppear: k2 = b2, onAppear: S2 = w2, onAppearCancelled: E2 = _2 } = t2, I2 = (e3, t3, n3) => {
    Pa(e3, t3 ? u2 : l2), Pa(e3, t3 ? d2 : a2), n3 && n3();
  }, O2 = (e3, t3) => {
    e3._isLeaving = false, Pa(e3, p2), Pa(e3, h2), Pa(e3, f2), t3 && t3();
  }, N2 = (e3) => (t3, n3) => {
    const r3 = e3 ? S2 : w2, a3 = () => I2(t3, e3, n3);
    Aa(r3, [t3, a3]), Fa(() => {
      Pa(t3, e3 ? c2 : s2), La(t3, e3 ? u2 : l2), Da(r3) || $a(t3, o2, m2, a3);
    });
  };
  return i(t2, { onBeforeEnter(e3) {
    Aa(b2, [e3]), La(e3, s2), La(e3, a2);
  }, onBeforeAppear(e3) {
    Aa(k2, [e3]), La(e3, c2), La(e3, d2);
  }, onEnter: N2(false), onAppear: N2(true), onLeave(e3, t3) {
    e3._isLeaving = true;
    const n3 = () => O2(e3, t3);
    La(e3, p2), La(e3, f2), Ka(), Fa(() => {
      e3._isLeaving && (Pa(e3, p2), La(e3, h2), Da(C2) || $a(e3, o2, g2, n3));
    }), Aa(C2, [e3, n3]);
  }, onEnterCancelled(e3) {
    I2(e3, false), Aa(_2, [e3]);
  }, onAppearCancelled(e3) {
    I2(e3, true), Aa(E2, [e3]);
  }, onLeaveCancelled(e3) {
    O2(e3), Aa(x2, [e3]);
  } });
}
function Ta(e2) {
  const t2 = R(e2);
  return "production" !== process.env.NODE_ENV && function(e3, t3) {
    "production" !== process.env.NODE_ENV && void 0 !== e3 && ("number" != typeof e3 ? Wt(`${t3} is not a valid number - got ${JSON.stringify(e3)}.`) : isNaN(e3) && Wt(`${t3} is NaN - the duration expression might be incorrect.`));
  }(t2, "<transition> explicit duration"), t2;
}
function La(e2, t2) {
  t2.split(/\s+/).forEach((t3) => t3 && e2.classList.add(t3)), (e2[Ia] || (e2[Ia] = /* @__PURE__ */ new Set())).add(t2);
}
function Pa(e2, t2) {
  t2.split(/\s+/).forEach((t3) => t3 && e2.classList.remove(t3));
  const n2 = e2[Ia];
  n2 && (n2.delete(t2), n2.size || (e2[Ia] = void 0));
}
function Fa(e2) {
  requestAnimationFrame(() => {
    requestAnimationFrame(e2);
  });
}
let Ra = 0;
function $a(e2, t2, n2, o2) {
  const r2 = e2._endId = ++Ra, s2 = () => {
    r2 === e2._endId && o2();
  };
  if (n2)
    return setTimeout(s2, n2);
  const { type: a2, timeout: l2, propCount: i2 } = Ba(e2, t2);
  if (!a2)
    return o2();
  const c2 = a2 + "end";
  let d2 = 0;
  const u2 = () => {
    e2.removeEventListener(c2, p2), s2();
  }, p2 = (t3) => {
    t3.target === e2 && ++d2 >= i2 && u2();
  };
  setTimeout(() => {
    d2 < i2 && u2();
  }, l2 + 1), e2.addEventListener(c2, p2);
}
function Ba(e2, t2) {
  const n2 = window.getComputedStyle(e2), o2 = (e3) => (n2[e3] || "").split(", "), r2 = o2(`${Sa}Delay`), s2 = o2(`${Sa}Duration`), a2 = Ha(r2, s2), l2 = o2(`${Ea}Delay`), i2 = o2(`${Ea}Duration`), c2 = Ha(l2, i2);
  let d2 = null, u2 = 0, p2 = 0;
  t2 === Sa ? a2 > 0 && (d2 = Sa, u2 = a2, p2 = s2.length) : t2 === Ea ? c2 > 0 && (d2 = Ea, u2 = c2, p2 = i2.length) : (u2 = Math.max(a2, c2), d2 = u2 > 0 ? a2 > c2 ? Sa : Ea : null, p2 = d2 ? d2 === Sa ? s2.length : i2.length : 0);
  return { type: d2, timeout: u2, propCount: p2, hasTransform: d2 === Sa && /\b(transform|all)(,|$)/.test(o2(`${Sa}Property`).toString()) };
}
function Ha(e2, t2) {
  for (; e2.length < t2.length; )
    e2 = e2.concat(e2);
  return Math.max(...t2.map((t3, n2) => ja(t3) + ja(e2[n2])));
}
function ja(e2) {
  return "auto" === e2 ? 0 : 1e3 * Number(e2.slice(0, -1).replace(",", "."));
}
function Ka() {
  return document.body.offsetHeight;
}
const za = Symbol("_vod"), Za = Symbol("_vsh"), Ua = { beforeMount(e2, { value: t2 }, { transition: n2 }) {
  e2[za] = "none" === e2.style.display ? "" : e2.style.display, n2 && t2 ? n2.beforeEnter(e2) : Wa(e2, t2);
}, mounted(e2, { value: t2 }, { transition: n2 }) {
  n2 && t2 && n2.enter(e2);
}, updated(e2, { value: t2, oldValue: n2 }, { transition: o2 }) {
  !t2 != !n2 && (o2 ? t2 ? (o2.beforeEnter(e2), Wa(e2, true), o2.enter(e2)) : o2.leave(e2, () => {
    Wa(e2, false);
  }) : Wa(e2, t2));
}, beforeUnmount(e2, { value: t2 }) {
  Wa(e2, t2);
} };
function Wa(e2, t2) {
  e2.style.display = t2 ? e2[za] : "none", e2[Za] = !t2;
}
"production" !== process.env.NODE_ENV && (Ua.name = "show");
const Ya = Symbol("production" !== process.env.NODE_ENV ? "CSS_VAR_TEXT" : ""), Ga = /(^|;)\s*display\s*:/;
const qa = /[^\\];\s*$/, Xa = /\s*!important$/;
function Ja(e2, t2, n2) {
  if (p(n2))
    n2.forEach((n3) => Ja(e2, t2, n3));
  else if (null == n2 && (n2 = ""), "production" !== process.env.NODE_ENV && qa.test(n2) && _a(`Unexpected semicolon at the end of '${t2}' style value: '${n2}'`), t2.startsWith("--"))
    e2.setProperty(t2, n2);
  else {
    const o2 = function(e3, t3) {
      const n3 = el[t3];
      if (n3)
        return n3;
      let o3 = N(t3);
      if ("filter" !== o3 && o3 in e3)
        return el[t3] = o3;
      o3 = D(o3);
      for (let n4 = 0; n4 < Qa.length; n4++) {
        const r2 = Qa[n4] + o3;
        if (r2 in e3)
          return el[t3] = r2;
      }
      return t3;
    }(e2, t2);
    Xa.test(n2) ? e2.setProperty(A(o2), n2.replace(Xa, ""), "important") : e2[o2] = n2;
  }
}
const Qa = ["Webkit", "Moz", "ms"], el = {};
const tl = "http://www.w3.org/1999/xlink";
function nl(e2, t2, n2, o2) {
  e2.addEventListener(t2, n2, o2);
}
const ol = Symbol("_vei");
function rl(e2, t2, n2, o2, r2 = null) {
  const s2 = e2[ol] || (e2[ol] = {}), a2 = s2[t2];
  if (o2 && a2)
    a2.value = "production" !== process.env.NODE_ENV ? cl(o2, t2) : o2;
  else {
    const [n3, l2] = function(e3) {
      let t3;
      if (sl.test(e3)) {
        let n5;
        for (t3 = {}; n5 = e3.match(sl); )
          e3 = e3.slice(0, e3.length - n5[0].length), t3[n5[0].toLowerCase()] = true;
      }
      const n4 = ":" === e3[2] ? e3.slice(3) : A(e3.slice(2));
      return [n4, t3];
    }(t2);
    if (o2) {
      const a3 = s2[t2] = function(e3, t3) {
        const n4 = (e4) => {
          if (e4._vts) {
            if (e4._vts <= n4.attached)
              return;
          } else
            e4._vts = Date.now();
          Jt(function(e5, t4) {
            if (p(t4)) {
              const n5 = e5.stopImmediatePropagation;
              return e5.stopImmediatePropagation = () => {
                n5.call(e5), e5._stopped = true;
              }, t4.map((e6) => (t5) => !t5._stopped && e6 && e6(t5));
            }
            return t4;
          }(e4, n4.value), t3, 5, [e4]);
        };
        return n4.value = e3, n4.attached = il(), n4;
      }("production" !== process.env.NODE_ENV ? cl(o2, t2) : o2, r2);
      nl(e2, n3, a3, l2);
    } else
      a2 && (!function(e3, t3, n4, o3) {
        e3.removeEventListener(t3, n4, o3);
      }(e2, n3, a2, l2), s2[t2] = void 0);
  }
}
const sl = /(?:Once|Passive|Capture)$/;
let al = 0;
const ll = Promise.resolve(), il = () => al || (ll.then(() => al = 0), al = Date.now());
function cl(e2, t2) {
  return v(e2) || p(e2) ? e2 : (_a(`Wrong type passed as event handler to ${t2} - did you forget @ or : in front of your prop?
Expected function or array of functions, received type ${typeof e2}.`), r);
}
const dl = (e2) => 111 === e2.charCodeAt(0) && 110 === e2.charCodeAt(1) && e2.charCodeAt(2) > 96 && e2.charCodeAt(2) < 123;
const ul = /* @__PURE__ */ new WeakMap(), pl = /* @__PURE__ */ new WeakMap(), fl = Symbol("_moveCb"), hl = Symbol("_enterCb"), vl = { name: "TransitionGroup", props: i({}, Ma, { tag: String, moveClass: String }), setup(e2, { slots: t2 }) {
  const n2 = Xs(), o2 = ko();
  let r2, s2;
  return Wo(() => {
    if (!r2.length)
      return;
    const t3 = e2.moveClass || `${e2.name || "v"}-move`;
    if (!function(e3, t4, n3) {
      const o4 = e3.cloneNode(), r3 = e3[Ia];
      r3 && r3.forEach((e4) => {
        e4.split(/\s+/).forEach((e5) => e5 && o4.classList.remove(e5));
      });
      n3.split(/\s+/).forEach((e4) => e4 && o4.classList.add(e4)), o4.style.display = "none";
      const s3 = 1 === t4.nodeType ? t4 : t4.parentNode;
      s3.appendChild(o4);
      const { hasTransform: a2 } = Ba(o4);
      return s3.removeChild(o4), a2;
    }(r2[0].el, n2.vnode.el, t3))
      return;
    r2.forEach(gl), r2.forEach(yl);
    const o3 = r2.filter(bl);
    Ka(), o3.forEach((e3) => {
      const n3 = e3.el, o4 = n3.style;
      La(n3, t3), o4.transform = o4.webkitTransform = o4.transitionDuration = "";
      const r3 = n3[fl] = (e4) => {
        e4 && e4.target !== n3 || e4 && !/transform$/.test(e4.propertyName) || (n3.removeEventListener("transitionend", r3), n3[fl] = null, Pa(n3, t3));
      };
      n3.addEventListener("transitionend", r3);
    });
  }), () => {
    const a2 = xt(e2), l2 = Va(a2);
    let i2 = a2.tag || bs;
    if (r2 = [], s2)
      for (let e3 = 0; e3 < s2.length; e3++) {
        const t3 = s2[e3];
        t3.el && t3.el instanceof Element && (r2.push(t3), Vo(t3, Mo(t3, l2, o2, n2)), ul.set(t3, t3.el.getBoundingClientRect()));
      }
    s2 = t2.default ? To(t2.default()) : [];
    for (let e3 = 0; e3 < s2.length; e3++) {
      const t3 = s2[e3];
      null != t3.key ? Vo(t3, Mo(t3, l2, o2, n2)) : "production" !== process.env.NODE_ENV && _a("<TransitionGroup> children must be keyed.");
    }
    return Ps(i2, null, s2);
  };
} }, ml = vl;
function gl(e2) {
  const t2 = e2.el;
  t2[fl] && t2[fl](), t2[hl] && t2[hl]();
}
function yl(e2) {
  pl.set(e2, e2.el.getBoundingClientRect());
}
function bl(e2) {
  const t2 = ul.get(e2), n2 = pl.get(e2), o2 = t2.left - n2.left, r2 = t2.top - n2.top;
  if (o2 || r2) {
    const t3 = e2.el.style;
    return t3.transform = t3.webkitTransform = `translate(${o2}px,${r2}px)`, t3.transitionDuration = "0s", e2;
  }
}
const wl = (e2) => {
  const t2 = e2.props["onUpdate:modelValue"] || false;
  return p(t2) ? (e3) => L(t2, e3) : t2;
};
function _l(e2) {
  e2.target.composing = true;
}
function Cl(e2) {
  const t2 = e2.target;
  t2.composing && (t2.composing = false, t2.dispatchEvent(new Event("input")));
}
const xl = Symbol("_assign"), kl = { created(e2, { modifiers: { lazy: t2, trim: n2, number: o2 } }, r2) {
  e2[xl] = wl(r2);
  const s2 = o2 || r2.props && "number" === r2.props.type;
  nl(e2, t2 ? "change" : "input", (t3) => {
    if (t3.target.composing)
      return;
    let o3 = e2.value;
    n2 && (o3 = o3.trim()), s2 && (o3 = F(o3)), e2[xl](o3);
  }), n2 && nl(e2, "change", () => {
    e2.value = e2.value.trim();
  }), t2 || (nl(e2, "compositionstart", _l), nl(e2, "compositionend", Cl), nl(e2, "change", Cl));
}, mounted(e2, { value: t2 }) {
  e2.value = null == t2 ? "" : t2;
}, beforeUpdate(e2, { value: t2, modifiers: { lazy: n2, trim: o2, number: r2 } }, s2) {
  if (e2[xl] = wl(s2), e2.composing)
    return;
  const a2 = null == t2 ? "" : t2;
  if ((!r2 && "number" !== e2.type || /^0\d/.test(e2.value) ? e2.value : F(e2.value)) !== a2) {
    if (document.activeElement === e2 && "range" !== e2.type) {
      if (n2)
        return;
      if (o2 && e2.value.trim() === a2)
        return;
    }
    e2.value = a2;
  }
} }, Sl = ["ctrl", "shift", "alt", "meta"], El = { stop: (e2) => e2.stopPropagation(), prevent: (e2) => e2.preventDefault(), self: (e2) => e2.target !== e2.currentTarget, ctrl: (e2) => !e2.ctrlKey, shift: (e2) => !e2.shiftKey, alt: (e2) => !e2.altKey, meta: (e2) => !e2.metaKey, left: (e2) => "button" in e2 && 0 !== e2.button, middle: (e2) => "button" in e2 && 1 !== e2.button, right: (e2) => "button" in e2 && 2 !== e2.button, exact: (e2, t2) => Sl.some((n2) => e2[`${n2}Key`] && !t2.includes(n2)) }, Il = (e2, t2) => {
  const n2 = e2._withMods || (e2._withMods = {}), o2 = t2.join(".");
  return n2[o2] || (n2[o2] = (n3, ...o3) => {
    for (let e3 = 0; e3 < t2.length; e3++) {
      const o4 = El[t2[e3]];
      if (o4 && o4(n3, t2))
        return;
    }
    return e2(n3, ...o3);
  });
}, Ol = { esc: "escape", space: " ", up: "arrow-up", left: "arrow-left", right: "arrow-right", down: "arrow-down", delete: "backspace" }, Nl = (e2, t2) => {
  const n2 = e2._withKeys || (e2._withKeys = {}), o2 = t2.join(".");
  return n2[o2] || (n2[o2] = (n3) => {
    if (!("key" in n3))
      return;
    const o3 = A(n3.key);
    return t2.some((e3) => e3 === o3 || Ol[e3] === o3) ? e2(n3) : void 0;
  });
}, Ml = i({ patchProp: (e2, t2, n2, o2, r2, s2, i2, c2, d2) => {
  const u2 = "svg" === r2;
  "class" === t2 ? function(e3, t3, n3) {
    const o3 = e3[Ia];
    o3 && (t3 = (t3 ? [t3, ...o3] : [...o3]).join(" ")), null == t3 ? e3.removeAttribute("class") : n3 ? e3.setAttribute("class", t3) : e3.className = t3;
  }(e2, o2, u2) : "style" === t2 ? function(e3, t3, n3) {
    const o3 = e3.style, r3 = m(n3);
    let s3 = false;
    if (n3 && !r3) {
      if (t3)
        if (m(t3))
          for (const e4 of t3.split(";")) {
            const t4 = e4.slice(0, e4.indexOf(":")).trim();
            null == n3[t4] && Ja(o3, t4, "");
          }
        else
          for (const e4 in t3)
            null == n3[e4] && Ja(o3, e4, "");
      for (const e4 in n3)
        "display" === e4 && (s3 = true), Ja(o3, e4, n3[e4]);
    } else if (r3) {
      if (t3 !== n3) {
        const e4 = o3[Ya];
        e4 && (n3 += ";" + e4), o3.cssText = n3, s3 = Ga.test(n3);
      }
    } else
      t3 && e3.removeAttribute("style");
    za in e3 && (e3[za] = s3 ? o3.display : "", e3[Za] && (o3.display = "none"));
  }(e2, n2, o2) : a(t2) ? l(t2) || rl(e2, t2, 0, o2, i2) : ("." === t2[0] ? (t2 = t2.slice(1), 1) : "^" === t2[0] ? (t2 = t2.slice(1), 0) : function(e3, t3, n3, o3) {
    if (o3)
      return "innerHTML" === t3 || "textContent" === t3 || !!(t3 in e3 && dl(t3) && v(n3));
    if ("spellcheck" === t3 || "draggable" === t3 || "translate" === t3)
      return false;
    if ("form" === t3)
      return false;
    if ("list" === t3 && "INPUT" === e3.tagName)
      return false;
    if ("type" === t3 && "TEXTAREA" === e3.tagName)
      return false;
    if ("width" === t3 || "height" === t3) {
      const t4 = e3.tagName;
      if ("IMG" === t4 || "VIDEO" === t4 || "CANVAS" === t4 || "SOURCE" === t4)
        return false;
    }
    if (dl(t3) && m(n3))
      return false;
    return t3 in e3;
  }(e2, t2, o2, u2)) ? function(e3, t3, n3, o3, r3, s3, a2) {
    if ("innerHTML" === t3 || "textContent" === t3)
      return o3 && a2(o3, r3, s3), void (e3[t3] = null == n3 ? "" : n3);
    const l2 = e3.tagName;
    if ("value" === t3 && "PROGRESS" !== l2 && !l2.includes("-")) {
      const o4 = null == n3 ? "" : n3;
      return ("OPTION" === l2 ? e3.getAttribute("value") || "" : e3.value) === o4 && "_value" in e3 || (e3.value = o4), null == n3 && e3.removeAttribute(t3), void (e3._value = n3);
    }
    let i3 = false;
    if ("" === n3 || null == n3) {
      const o4 = typeof e3[t3];
      "boolean" === o4 ? n3 = J(n3) : null == n3 && "string" === o4 ? (n3 = "", i3 = true) : "number" === o4 && (n3 = 0, i3 = true);
    }
    try {
      e3[t3] = n3;
    } catch (e4) {
      "production" === process.env.NODE_ENV || i3 || _a(`Failed setting prop "${t3}" on <${l2.toLowerCase()}>: value ${n3} is invalid.`, e4);
    }
    i3 && e3.removeAttribute(t3);
  }(e2, t2, o2, s2, i2, c2, d2) : ("true-value" === t2 ? e2._trueValue = o2 : "false-value" === t2 && (e2._falseValue = o2), function(e3, t3, n3, o3, r3) {
    if (o3 && t3.startsWith("xlink:"))
      null == n3 ? e3.removeAttributeNS(tl, t3.slice(6, t3.length)) : e3.setAttributeNS(tl, t3, n3);
    else {
      const o4 = X(t3);
      null == n3 || o4 && !J(n3) ? e3.removeAttribute(t3) : e3.setAttribute(t3, o4 ? "" : n3);
    }
  }(e2, t2, o2, u2));
} }, ka);
let Al;
const Dl = (...e2) => {
  const t2 = (Al || (Al = as(Ml))).createApp(...e2);
  "production" !== process.env.NODE_ENV && (function(e3) {
    Object.defineProperty(e3.config, "isNativeTag", { value: (e4) => Y(e4) || G(e4) || q(e4), writable: false });
  }(t2), function(e3) {
    {
      const t3 = e3.config.isCustomElement;
      Object.defineProperty(e3.config, "isCustomElement", { get: () => t3, set() {
        _a("The `isCustomElement` config option is deprecated. Use `compilerOptions.isCustomElement` instead.");
      } });
      const n3 = e3.config.compilerOptions, o2 = 'The `compilerOptions` config option is only respected when using a build of Vue.js that includes the runtime compiler (aka "full build"). Since you are using the runtime-only build, `compilerOptions` must be passed to `@vue/compiler-dom` in the build setup instead.\n- For vue-loader: pass it via vue-loader\'s `compilerOptions` loader option.\n- For vue-cli: see https://cli.vuejs.org/guide/webpack.html#modifying-options-of-a-loader\n- For vite: pass it via @vitejs/plugin-vue options. See https://github.com/vitejs/vite-plugin-vue/tree/main/packages/plugin-vue#example-for-passing-options-to-vuecompiler-sfc';
      Object.defineProperty(e3.config, "compilerOptions", { get: () => (_a(o2), n3), set() {
        _a(o2);
      } });
    }
  }(t2));
  const { mount: n2 } = t2;
  return t2.mount = (e3) => {
    const o2 = function(e4) {
      if (m(e4)) {
        const t3 = document.querySelector(e4);
        return "production" === process.env.NODE_ENV || t3 || _a(`Failed to mount app: mount target selector "${e4}" returned null.`), t3;
      }
      "production" !== process.env.NODE_ENV && window.ShadowRoot && e4 instanceof window.ShadowRoot && "closed" === e4.mode && _a('mounting on a ShadowRoot with `{mode: "closed"}` may lead to unpredictable bugs');
      return e4;
    }(e3);
    if (!o2)
      return;
    const r2 = t2._component;
    v(r2) || r2.render || r2.template || (r2.template = o2.innerHTML), o2.innerHTML = "";
    const s2 = n2(o2, false, function(e4) {
      if (e4 instanceof SVGElement)
        return "svg";
      if ("function" == typeof MathMLElement && e4 instanceof MathMLElement)
        return "mathml";
    }(o2));
    return o2 instanceof Element && (o2.removeAttribute("v-cloak"), o2.setAttribute("data-v-app", "")), s2;
  }, t2;
};
"production" !== process.env.NODE_ENV && function() {
  if ("production" === process.env.NODE_ENV || "undefined" == typeof window)
    return;
  const e2 = { style: "color:#3ba776" }, t2 = { style: "color:#1677ff" }, o2 = { style: "color:#f5222d" }, r2 = { style: "color:#eb2f96" }, s2 = { header: (t3) => y(t3) ? t3.__isVue ? ["div", e2, "VueInstance"] : Mt(t3) ? ["div", {}, ["span", e2, f2(t3)], "<", c2(t3.value), ">"] : bt(t3) ? ["div", {}, ["span", e2, _t(t3) ? "ShallowReactive" : "Reactive"], "<", c2(t3), ">" + (wt(t3) ? " (readonly)" : "")] : wt(t3) ? ["div", {}, ["span", e2, _t(t3) ? "ShallowReadonly" : "Readonly"], "<", c2(t3), ">"] : null : null, hasBody: (e3) => e3 && e3.__isVue, body(e3) {
    if (e3 && e3.__isVue)
      return ["div", {}, ...a2(e3.$)];
  } };
  function a2(e3) {
    const t3 = [];
    e3.type.props && e3.props && t3.push(l2("props", xt(e3.props))), e3.setupState !== n && t3.push(l2("setup", e3.setupState)), e3.data !== n && t3.push(l2("data", xt(e3.data)));
    const o3 = d2(e3, "computed");
    o3 && t3.push(l2("computed", o3));
    const s3 = d2(e3, "inject");
    return s3 && t3.push(l2("injected", s3)), t3.push(["div", {}, ["span", { style: r2.style + ";opacity:0.66" }, "$ (internal): "], ["object", { object: e3 }]]), t3;
  }
  function l2(e3, t3) {
    return t3 = i({}, t3), Object.keys(t3).length ? ["div", { style: "line-height:1.25em;margin-bottom:0.6em" }, ["div", { style: "color:#476582" }, e3], ["div", { style: "padding-left:1.25em" }, ...Object.keys(t3).map((e4) => ["div", {}, ["span", r2, e4 + ": "], c2(t3[e4], false)])]] : ["span", {}];
  }
  function c2(e3, n2 = true) {
    return "number" == typeof e3 ? ["span", t2, e3] : "string" == typeof e3 ? ["span", o2, JSON.stringify(e3)] : "boolean" == typeof e3 ? ["span", r2, e3] : y(e3) ? ["object", { object: n2 ? xt(e3) : e3 }] : ["span", o2, String(e3)];
  }
  function d2(e3, t3) {
    const n2 = e3.type;
    if (v(n2))
      return;
    const o3 = {};
    for (const r3 in e3.ctx)
      u2(n2, r3, t3) && (o3[r3] = e3.ctx[r3]);
    return o3;
  }
  function u2(e3, t3, n2) {
    const o3 = e3[n2];
    return !!(p(o3) && o3.includes(t3) || y(o3) && t3 in o3) || !(!e3.extends || !u2(e3.extends, t3, n2)) || !(!e3.mixins || !e3.mixins.some((e4) => u2(e4, t3, n2))) || void 0;
  }
  function f2(e3) {
    return _t(e3) ? "ShallowRef" : e3.effect ? "ComputedRef" : "Ref";
  }
  window.devtoolsFormatters ? window.devtoolsFormatters.push(s2) : window.devtoolsFormatters = [s2];
}();
var Vl = false;
function Tl(e2, t2, n2) {
  return Array.isArray(e2) ? (e2.length = Math.max(e2.length, t2), e2.splice(t2, 1, n2), n2) : (e2[t2] = n2, n2);
}
function Ll(e2, t2) {
  Array.isArray(e2) ? e2.splice(t2, 1) : delete e2[t2];
}
function Pl() {
  return "undefined" != typeof navigator && "undefined" != typeof window ? window : "undefined" != typeof globalThis ? globalThis : {};
}
const Fl = "function" == typeof Proxy, Rl = "devtools-plugin:setup";
let $l, Bl, Hl;
function jl() {
  return function() {
    var e2;
    return void 0 !== $l || ("undefined" != typeof window && window.performance ? ($l = true, Bl = window.performance) : "undefined" != typeof globalThis && (null === (e2 = globalThis.perf_hooks) || void 0 === e2 ? void 0 : e2.performance) ? ($l = true, Bl = globalThis.perf_hooks.performance) : $l = false), $l;
  }() ? Bl.now() : Date.now();
}
class Kl {
  constructor(e2, t2) {
    this.target = null, this.targetQueue = [], this.onQueue = [], this.plugin = e2, this.hook = t2;
    const n2 = {};
    if (e2.settings)
      for (const t3 in e2.settings) {
        const o3 = e2.settings[t3];
        n2[t3] = o3.defaultValue;
      }
    const o2 = `__vue-devtools-plugin-settings__${e2.id}`;
    let r2 = Object.assign({}, n2);
    try {
      const e3 = localStorage.getItem(o2), t3 = JSON.parse(e3);
      Object.assign(r2, t3);
    } catch (e3) {
    }
    this.fallbacks = { getSettings: () => r2, setSettings(e3) {
      try {
        localStorage.setItem(o2, JSON.stringify(e3));
      } catch (e4) {
      }
      r2 = e3;
    }, now: () => jl() }, t2 && t2.on("plugin:settings:set", (e3, t3) => {
      e3 === this.plugin.id && this.fallbacks.setSettings(t3);
    }), this.proxiedOn = new Proxy({}, { get: (e3, t3) => this.target ? this.target.on[t3] : (...e4) => {
      this.onQueue.push({ method: t3, args: e4 });
    } }), this.proxiedTarget = new Proxy({}, { get: (e3, t3) => this.target ? this.target[t3] : "on" === t3 ? this.proxiedOn : Object.keys(this.fallbacks).includes(t3) ? (...e4) => (this.targetQueue.push({ method: t3, args: e4, resolve: () => {
    } }), this.fallbacks[t3](...e4)) : (...e4) => new Promise((n3) => {
      this.targetQueue.push({ method: t3, args: e4, resolve: n3 });
    }) });
  }
  async setRealTarget(e2) {
    this.target = e2;
    for (const e3 of this.onQueue)
      this.target.on[e3.method](...e3.args);
    for (const e3 of this.targetQueue)
      e3.resolve(await this.target[e3.method](...e3.args));
  }
}
function zl(e2, t2) {
  const n2 = e2, o2 = Pl(), r2 = Pl().__VUE_DEVTOOLS_GLOBAL_HOOK__, s2 = Fl && n2.enableEarlyProxy;
  if (!r2 || !o2.__VUE_DEVTOOLS_PLUGIN_API_AVAILABLE__ && s2) {
    const e3 = s2 ? new Kl(n2, r2) : null;
    (o2.__VUE_DEVTOOLS_PLUGINS__ = o2.__VUE_DEVTOOLS_PLUGINS__ || []).push({ pluginDescriptor: n2, setupFn: t2, proxy: e3 }), e3 && t2(e3.proxiedTarget);
  } else
    r2.emit(Rl, e2, t2);
}
/*!
 * pinia v2.1.7
 * (c) 2023 Eduardo San Martin Morote
 * @license MIT
 */
const Zl = (e2) => Hl = e2, Ul = "production" !== process.env.NODE_ENV ? Symbol("pinia") : Symbol();
function Wl(e2) {
  return e2 && "object" == typeof e2 && "[object Object]" === Object.prototype.toString.call(e2) && "function" != typeof e2.toJSON;
}
var Yl;
!function(e2) {
  e2.direct = "direct", e2.patchObject = "patch object", e2.patchFunction = "patch function";
}(Yl || (Yl = {}));
const Gl = "undefined" != typeof window, ql = "production" !== process.env.NODE_ENV && !("test" === process.env.NODE_ENV) && Gl, Xl = (() => "object" == typeof window && window.window === window ? window : "object" == typeof self && self.self === self ? self : "object" == typeof global && global.global === global ? global : "object" == typeof globalThis ? globalThis : { HTMLElement: null })();
function Jl(e2, t2, n2) {
  const o2 = new XMLHttpRequest();
  o2.open("GET", e2), o2.responseType = "blob", o2.onload = function() {
    oi(o2.response, t2, n2);
  }, o2.onerror = function() {
    console.error("could not download file");
  }, o2.send();
}
function Ql(e2) {
  const t2 = new XMLHttpRequest();
  t2.open("HEAD", e2, false);
  try {
    t2.send();
  } catch (e3) {
  }
  return t2.status >= 200 && t2.status <= 299;
}
function ei(e2) {
  try {
    e2.dispatchEvent(new MouseEvent("click"));
  } catch (t2) {
    const n2 = document.createEvent("MouseEvents");
    n2.initMouseEvent("click", true, true, window, 0, 0, 0, 80, 20, false, false, false, false, 0, null), e2.dispatchEvent(n2);
  }
}
const ti = "object" == typeof navigator ? navigator : { userAgent: "" }, ni = (() => /Macintosh/.test(ti.userAgent) && /AppleWebKit/.test(ti.userAgent) && !/Safari/.test(ti.userAgent))(), oi = Gl ? "undefined" != typeof HTMLAnchorElement && "download" in HTMLAnchorElement.prototype && !ni ? function(e2, t2 = "download", n2) {
  const o2 = document.createElement("a");
  o2.download = t2, o2.rel = "noopener", "string" == typeof e2 ? (o2.href = e2, o2.origin !== location.origin ? Ql(o2.href) ? Jl(e2, t2, n2) : (o2.target = "_blank", ei(o2)) : ei(o2)) : (o2.href = URL.createObjectURL(e2), setTimeout(function() {
    URL.revokeObjectURL(o2.href);
  }, 4e4), setTimeout(function() {
    ei(o2);
  }, 0));
} : "msSaveOrOpenBlob" in ti ? function(e2, t2 = "download", n2) {
  if ("string" == typeof e2)
    if (Ql(e2))
      Jl(e2, t2, n2);
    else {
      const t3 = document.createElement("a");
      t3.href = e2, t3.target = "_blank", setTimeout(function() {
        ei(t3);
      });
    }
  else
    navigator.msSaveOrOpenBlob(function(e3, { autoBom: t3 = false } = {}) {
      return t3 && /^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e3.type) ? new Blob([String.fromCharCode(65279), e3], { type: e3.type }) : e3;
    }(e2, n2), t2);
} : function(e2, t2, n2, o2) {
  (o2 = o2 || open("", "_blank")) && (o2.document.title = o2.document.body.innerText = "downloading...");
  if ("string" == typeof e2)
    return Jl(e2, t2, n2);
  const r2 = "application/octet-stream" === e2.type, s2 = /constructor/i.test(String(Xl.HTMLElement)) || "safari" in Xl, a2 = /CriOS\/[\d]+/.test(navigator.userAgent);
  if ((a2 || r2 && s2 || ni) && "undefined" != typeof FileReader) {
    const t3 = new FileReader();
    t3.onloadend = function() {
      let e3 = t3.result;
      if ("string" != typeof e3)
        throw o2 = null, new Error("Wrong reader.result type");
      e3 = a2 ? e3 : e3.replace(/^data:[^;]*;/, "data:attachment/file;"), o2 ? o2.location.href = e3 : location.assign(e3), o2 = null;
    }, t3.readAsDataURL(e2);
  } else {
    const t3 = URL.createObjectURL(e2);
    o2 ? o2.location.assign(t3) : location.href = t3, o2 = null, setTimeout(function() {
      URL.revokeObjectURL(t3);
    }, 4e4);
  }
} : () => {
};
function ri(e2, t2) {
  const n2 = "\u{1F34D} " + e2;
  "function" == typeof __VUE_DEVTOOLS_TOAST__ ? __VUE_DEVTOOLS_TOAST__(n2, t2) : "error" === t2 ? console.error(n2) : "warn" === t2 ? console.warn(n2) : console.log(n2);
}
function si(e2) {
  return "_a" in e2 && "install" in e2;
}
function ai() {
  if (!("clipboard" in navigator))
    return ri("Your browser doesn't support the Clipboard API", "error"), true;
}
function li(e2) {
  return !!(e2 instanceof Error && e2.message.toLowerCase().includes("document is not focused")) && (ri('You need to activate the "Emulate a focused page" setting in the "Rendering" panel of devtools.', "warn"), true);
}
let ii;
async function ci(e2) {
  try {
    const t2 = (ii || (ii = document.createElement("input"), ii.type = "file", ii.accept = ".json"), function() {
      return new Promise((e3, t3) => {
        ii.onchange = async () => {
          const t4 = ii.files;
          if (!t4)
            return e3(null);
          const n3 = t4.item(0);
          return e3(n3 ? { text: await n3.text(), file: n3 } : null);
        }, ii.oncancel = () => e3(null), ii.onerror = t3, ii.click();
      });
    }), n2 = await t2();
    if (!n2)
      return;
    const { text: o2, file: r2 } = n2;
    di(e2, JSON.parse(o2)), ri(`Global state imported from "${r2.name}".`);
  } catch (e3) {
    ri("Failed to import the state from JSON. Check the console for more details.", "error"), console.error(e3);
  }
}
function di(e2, t2) {
  for (const n2 in t2) {
    const o2 = e2.state.value[n2];
    o2 ? Object.assign(o2, t2[n2]) : e2.state.value[n2] = t2[n2];
  }
}
function ui(e2) {
  return { _custom: { display: e2 } };
}
const pi = "\u{1F34D} Pinia (root)", fi = "_root";
function hi(e2) {
  return si(e2) ? { id: fi, label: pi } : { id: e2.$id, label: e2.$id };
}
function vi(e2) {
  return e2 ? Array.isArray(e2) ? e2.reduce((e3, t2) => (e3.keys.push(t2.key), e3.operations.push(t2.type), e3.oldValue[t2.key] = t2.oldValue, e3.newValue[t2.key] = t2.newValue, e3), { oldValue: {}, keys: [], operations: [], newValue: {} }) : { operation: ui(e2.type), key: ui(e2.key), oldValue: e2.oldValue, newValue: e2.newValue } : {};
}
function mi(e2) {
  switch (e2) {
    case Yl.direct:
      return "mutation";
    case Yl.patchFunction:
    case Yl.patchObject:
      return "$patch";
    default:
      return "unknown";
  }
}
let gi = true;
const yi = [], bi = "pinia:mutations", wi = "pinia", { assign: _i } = Object, Ci = (e2) => "\u{1F34D} " + e2;
function xi(e2, t2) {
  zl({ id: "dev.esm.pinia", label: "Pinia \u{1F34D}", logo: "https://pinia.vuejs.org/logo.svg", packageName: "pinia", homepage: "https://pinia.vuejs.org", componentStateTypes: yi, app: e2 }, (n2) => {
    "function" != typeof n2.now && ri("You seem to be using an outdated version of Vue Devtools. Are you still using the Beta release instead of the stable one? You can find the links at https://devtools.vuejs.org/guide/installation.html."), n2.addTimelineLayer({ id: bi, label: "Pinia \u{1F34D}", color: 15064968 }), n2.addInspector({ id: wi, label: "Pinia \u{1F34D}", icon: "storage", treeFilterPlaceholder: "Search stores", actions: [{ icon: "content_copy", action: () => {
      !async function(e3) {
        if (!ai())
          try {
            await navigator.clipboard.writeText(JSON.stringify(e3.state.value)), ri("Global state copied to clipboard.");
          } catch (e4) {
            if (li(e4))
              return;
            ri("Failed to serialize the state. Check the console for more details.", "error"), console.error(e4);
          }
      }(t2);
    }, tooltip: "Serialize and copy the state" }, { icon: "content_paste", action: async () => {
      await async function(e3) {
        if (!ai())
          try {
            di(e3, JSON.parse(await navigator.clipboard.readText())), ri("Global state pasted from clipboard.");
          } catch (e4) {
            if (li(e4))
              return;
            ri("Failed to deserialize the state from clipboard. Check the console for more details.", "error"), console.error(e4);
          }
      }(t2), n2.sendInspectorTree(wi), n2.sendInspectorState(wi);
    }, tooltip: "Replace the state with the content of your clipboard" }, { icon: "save", action: () => {
      !async function(e3) {
        try {
          oi(new Blob([JSON.stringify(e3.state.value)], { type: "text/plain;charset=utf-8" }), "pinia-state.json");
        } catch (e4) {
          ri("Failed to export the state as JSON. Check the console for more details.", "error"), console.error(e4);
        }
      }(t2);
    }, tooltip: "Save the state as a JSON file" }, { icon: "folder_open", action: async () => {
      await ci(t2), n2.sendInspectorTree(wi), n2.sendInspectorState(wi);
    }, tooltip: "Import the state from a JSON file" }], nodeActions: [{ icon: "restore", tooltip: 'Reset the state (with "$reset")', action: (e3) => {
      const n3 = t2._s.get(e3);
      n3 ? "function" != typeof n3.$reset ? ri(`Cannot reset "${e3}" store because it doesn't have a "$reset" method implemented.`, "warn") : (n3.$reset(), ri(`Store "${e3}" reset.`)) : ri(`Cannot reset "${e3}" store because it wasn't found.`, "warn");
    } }] }), n2.on.inspectComponent((e3, t3) => {
      const n3 = e3.componentInstance && e3.componentInstance.proxy;
      if (n3 && n3._pStores) {
        const t4 = e3.componentInstance.proxy._pStores;
        Object.values(t4).forEach((t5) => {
          e3.instanceData.state.push({ type: Ci(t5.$id), key: "state", editable: true, value: t5._isOptionsAPI ? { _custom: { value: xt(t5.$state), actions: [{ icon: "restore", tooltip: "Reset the state of this store", action: () => t5.$reset() }] } } : Object.keys(t5.$state).reduce((e4, n4) => (e4[n4] = t5.$state[n4], e4), {}) }), t5._getters && t5._getters.length && e3.instanceData.state.push({ type: Ci(t5.$id), key: "getters", editable: false, value: t5._getters.reduce((e4, n4) => {
            try {
              e4[n4] = t5[n4];
            } catch (t6) {
              e4[n4] = t6;
            }
            return e4;
          }, {}) });
        });
      }
    }), n2.on.getInspectorTree((n3) => {
      if (n3.app === e2 && n3.inspectorId === wi) {
        let e3 = [t2];
        e3 = e3.concat(Array.from(t2._s.values())), n3.rootNodes = (n3.filter ? e3.filter((e4) => "$id" in e4 ? e4.$id.toLowerCase().includes(n3.filter.toLowerCase()) : pi.toLowerCase().includes(n3.filter.toLowerCase())) : e3).map(hi);
      }
    }), n2.on.getInspectorState((n3) => {
      if (n3.app === e2 && n3.inspectorId === wi) {
        const e3 = n3.nodeId === fi ? t2 : t2._s.get(n3.nodeId);
        if (!e3)
          return;
        e3 && (n3.state = function(e4) {
          if (si(e4)) {
            const t4 = Array.from(e4._s.keys()), n4 = e4._s, o2 = { state: t4.map((t5) => ({ editable: true, key: t5, value: e4.state.value[t5] })), getters: t4.filter((e5) => n4.get(e5)._getters).map((e5) => {
              const t5 = n4.get(e5);
              return { editable: false, key: e5, value: t5._getters.reduce((e6, n5) => (e6[n5] = t5[n5], e6), {}) };
            }) };
            return o2;
          }
          const t3 = { state: Object.keys(e4.$state).map((t4) => ({ editable: true, key: t4, value: e4.$state[t4] })) };
          return e4._getters && e4._getters.length && (t3.getters = e4._getters.map((t4) => ({ editable: false, key: t4, value: e4[t4] }))), e4._customProperties.size && (t3.customProperties = Array.from(e4._customProperties).map((t4) => ({ editable: true, key: t4, value: e4[t4] }))), t3;
        }(e3));
      }
    }), n2.on.editInspectorState((n3, o2) => {
      if (n3.app === e2 && n3.inspectorId === wi) {
        const e3 = n3.nodeId === fi ? t2 : t2._s.get(n3.nodeId);
        if (!e3)
          return ri(`store "${n3.nodeId}" not found`, "error");
        const { path: o3 } = n3;
        si(e3) ? o3.unshift("state") : 1 === o3.length && e3._customProperties.has(o3[0]) && !(o3[0] in e3.$state) || o3.unshift("$state"), gi = false, n3.set(e3, o3, n3.state.value), gi = true;
      }
    }), n2.on.editComponentState((e3) => {
      if (e3.type.startsWith("\u{1F34D}")) {
        const n3 = e3.type.replace(/^🍍\s*/, ""), o2 = t2._s.get(n3);
        if (!o2)
          return ri(`store "${n3}" not found`, "error");
        const { path: r2 } = e3;
        if ("state" !== r2[0])
          return ri(`Invalid path for store "${n3}":
${r2}
Only state can be modified.`);
        r2[0] = "$state", gi = false, e3.set(o2, r2, e3.state.value), gi = true;
      }
    });
  });
}
let ki, Si = 0;
function Ei(e2, t2, n2) {
  const o2 = t2.reduce((t3, n3) => (t3[n3] = xt(e2)[n3], t3), {});
  for (const t3 in o2)
    e2[t3] = function() {
      const r2 = Si, s2 = n2 ? new Proxy(e2, { get: (...e3) => (ki = r2, Reflect.get(...e3)), set: (...e3) => (ki = r2, Reflect.set(...e3)) }) : e2;
      ki = r2;
      const a2 = o2[t3].apply(s2, arguments);
      return ki = void 0, a2;
    };
}
function Ii({ app: e2, store: t2, options: n2 }) {
  if (t2.$id.startsWith("__hot:"))
    return;
  t2._isOptionsAPI = !!n2.state, Ei(t2, Object.keys(n2.actions), t2._isOptionsAPI);
  const o2 = t2._hotUpdate;
  xt(t2)._hotUpdate = function(e3) {
    o2.apply(this, arguments), Ei(t2, Object.keys(e3._hmrPayload.actions), !!t2._isOptionsAPI);
  }, function(e3, t3) {
    yi.includes(Ci(t3.$id)) || yi.push(Ci(t3.$id)), zl({ id: "dev.esm.pinia", label: "Pinia \u{1F34D}", logo: "https://pinia.vuejs.org/logo.svg", packageName: "pinia", homepage: "https://pinia.vuejs.org", componentStateTypes: yi, app: e3, settings: { logStoreChanges: { label: "Notify about new/deleted stores", type: "boolean", defaultValue: true } } }, (e4) => {
      const n3 = "function" == typeof e4.now ? e4.now.bind(e4) : Date.now;
      t3.$onAction(({ after: o4, onError: r3, name: s2, args: a2 }) => {
        const l2 = Si++;
        e4.addTimelineEvent({ layerId: bi, event: { time: n3(), title: "\u{1F6EB} " + s2, subtitle: "start", data: { store: ui(t3.$id), action: ui(s2), args: a2 }, groupId: l2 } }), o4((o5) => {
          ki = void 0, e4.addTimelineEvent({ layerId: bi, event: { time: n3(), title: "\u{1F6EC} " + s2, subtitle: "end", data: { store: ui(t3.$id), action: ui(s2), args: a2, result: o5 }, groupId: l2 } });
        }), r3((o5) => {
          ki = void 0, e4.addTimelineEvent({ layerId: bi, event: { time: n3(), logType: "error", title: "\u{1F4A5} " + s2, subtitle: "end", data: { store: ui(t3.$id), action: ui(s2), args: a2, error: o5 }, groupId: l2 } });
        });
      }, true), t3._customProperties.forEach((o4) => {
        ho(() => Lt(t3[o4]), (t4, r3) => {
          e4.notifyComponentUpdate(), e4.sendInspectorState(wi), gi && e4.addTimelineEvent({ layerId: bi, event: { time: n3(), title: "Change", subtitle: o4, data: { newValue: t4, oldValue: r3 }, groupId: ki } });
        }, { deep: true });
      }), t3.$subscribe(({ events: o4, type: r3 }, s2) => {
        if (e4.notifyComponentUpdate(), e4.sendInspectorState(wi), !gi)
          return;
        const a2 = { time: n3(), title: mi(r3), data: _i({ store: ui(t3.$id) }, vi(o4)), groupId: ki };
        r3 === Yl.patchFunction ? a2.subtitle = "\u2935\uFE0F" : r3 === Yl.patchObject ? a2.subtitle = "\u{1F9E9}" : o4 && !Array.isArray(o4) && (a2.subtitle = o4.type), o4 && (a2.data["rawEvent(s)"] = { _custom: { display: "DebuggerEvent", type: "object", tooltip: "raw DebuggerEvent[]", value: o4 } }), e4.addTimelineEvent({ layerId: bi, event: a2 });
      }, { detached: true, flush: "sync" });
      const o3 = t3._hotUpdate;
      t3._hotUpdate = kt((r3) => {
        o3(r3), e4.addTimelineEvent({ layerId: bi, event: { time: n3(), title: "\u{1F525} " + t3.$id, subtitle: "HMR update", data: { store: ui(t3.$id), info: ui("HMR update") } } }), e4.notifyComponentUpdate(), e4.sendInspectorTree(wi), e4.sendInspectorState(wi);
      });
      const { $dispose: r2 } = t3;
      t3.$dispose = () => {
        r2(), e4.notifyComponentUpdate(), e4.sendInspectorTree(wi), e4.sendInspectorState(wi), e4.getSettings().logStoreChanges && ri(`Disposed "${t3.$id}" store \u{1F5D1}`);
      }, e4.notifyComponentUpdate(), e4.sendInspectorTree(wi), e4.sendInspectorState(wi), e4.getSettings().logStoreChanges && ri(`"${t3.$id}" store installed \u{1F195}`);
    });
  }(e2, t2);
}
function Oi(e2, t2) {
  for (const n2 in t2) {
    const o2 = t2[n2];
    if (!(n2 in e2))
      continue;
    const r2 = e2[n2];
    Wl(r2) && Wl(o2) && !Mt(o2) && !bt(o2) ? e2[n2] = Oi(r2, o2) : e2[n2] = o2;
  }
  return e2;
}
const Ni = () => {
};
function Mi(e2, t2, n2, o2 = Ni) {
  e2.push(t2);
  const r2 = () => {
    const n3 = e2.indexOf(t2);
    n3 > -1 && (e2.splice(n3, 1), o2());
  };
  return !n2 && le() && ie(r2), r2;
}
function Ai(e2, ...t2) {
  e2.slice().forEach((e3) => {
    e3(...t2);
  });
}
const Di = (e2) => e2();
function Vi(e2, t2) {
  e2 instanceof Map && t2 instanceof Map && t2.forEach((t3, n2) => e2.set(n2, t3)), e2 instanceof Set && t2 instanceof Set && t2.forEach(e2.add, e2);
  for (const n2 in t2) {
    if (!t2.hasOwnProperty(n2))
      continue;
    const o2 = t2[n2], r2 = e2[n2];
    Wl(r2) && Wl(o2) && e2.hasOwnProperty(n2) && !Mt(o2) && !bt(o2) ? e2[n2] = Vi(r2, o2) : e2[n2] = o2;
  }
  return e2;
}
const Ti = "production" !== process.env.NODE_ENV ? Symbol("pinia:skipHydration") : Symbol();
const { assign: Li } = Object;
function Pi(e2) {
  return !(!Mt(e2) || !e2.effect);
}
function Fi(e2, t2, n2, o2) {
  const { state: r2, actions: s2, getters: a2 } = t2, l2 = n2.state.value[e2];
  let i2;
  return i2 = Ri(e2, function() {
    l2 || "production" !== process.env.NODE_ENV && o2 || (n2.state.value[e2] = r2 ? r2() : {});
    const t3 = "production" !== process.env.NODE_ENV && o2 ? $t(At(r2 ? r2() : {}).value) : $t(n2.state.value[e2]);
    return Li(t3, s2, Object.keys(a2 || {}).reduce((o3, r3) => ("production" !== process.env.NODE_ENV && r3 in t3 && console.warn(`[\u{1F34D}]: A getter cannot have the same name as another state property. Rename one of them. Found with "${r3}" in store "${e2}".`), o3[r3] = kt(ga(() => {
      Zl(n2);
      const t4 = n2._s.get(e2);
      return a2[r3].call(t4, t4);
    })), o3), {}));
  }, t2, n2, o2, true), i2;
}
function Ri(e2, t2, n2 = {}, o2, r2, s2) {
  let a2;
  const l2 = Li({ actions: {} }, n2);
  if ("production" !== process.env.NODE_ENV && !o2._e.active)
    throw new Error("Pinia destroyed");
  const i2 = { deep: true };
  let c2, d2;
  "production" === process.env.NODE_ENV || Vl || (i2.onTrigger = (e3) => {
    c2 ? u2 = e3 : 0 != c2 || C2._hotUpdating || (Array.isArray(u2) ? u2.push(e3) : console.error("\u{1F34D} debuggerEvents should be an array. This is most likely an internal Pinia bug."));
  });
  let u2, p2 = [], f2 = [];
  const h2 = o2.state.value[e2];
  s2 || h2 || "production" !== process.env.NODE_ENV && r2 || (o2.state.value[e2] = {});
  const v2 = At({});
  let m2;
  function g2(t3) {
    let n3;
    c2 = d2 = false, "production" !== process.env.NODE_ENV && (u2 = []), "function" == typeof t3 ? (t3(o2.state.value[e2]), n3 = { type: Yl.patchFunction, storeId: e2, events: u2 }) : (Vi(o2.state.value[e2], t3), n3 = { type: Yl.patchObject, payload: t3, storeId: e2, events: u2 });
    const r3 = m2 = Symbol();
    un().then(() => {
      m2 === r3 && (c2 = true);
    }), d2 = true, Ai(p2, n3, o2.state.value[e2]);
  }
  const y2 = s2 ? function() {
    const { state: e3 } = n2, t3 = e3 ? e3() : {};
    this.$patch((e4) => {
      Li(e4, t3);
    });
  } : "production" !== process.env.NODE_ENV ? () => {
    throw new Error(`\u{1F34D}: Store "${e2}" is built using the setup syntax and does not implement $reset().`);
  } : Ni;
  function b2(t3, n3) {
    return function() {
      Zl(o2);
      const r3 = Array.from(arguments), s3 = [], a3 = [];
      let l3;
      Ai(f2, { args: r3, name: t3, store: C2, after: function(e3) {
        s3.push(e3);
      }, onError: function(e3) {
        a3.push(e3);
      } });
      try {
        l3 = n3.apply(this && this.$id === e2 ? this : C2, r3);
      } catch (e3) {
        throw Ai(a3, e3), e3;
      }
      return l3 instanceof Promise ? l3.then((e3) => (Ai(s3, e3), e3)).catch((e3) => (Ai(a3, e3), Promise.reject(e3))) : (Ai(s3, l3), l3);
    };
  }
  const w2 = kt({ actions: {}, getters: {}, state: [], hotState: v2 }), _2 = { _p: o2, $id: e2, $onAction: Mi.bind(null, f2), $patch: g2, $reset: y2, $subscribe(t3, n3 = {}) {
    const r3 = Mi(p2, t3, n3.detached, () => s3()), s3 = a2.run(() => ho(() => o2.state.value[e2], (o3) => {
      ("sync" === n3.flush ? d2 : c2) && t3({ storeId: e2, type: Yl.direct, events: u2 }, o3);
    }, Li({}, i2, n3)));
    return r3;
  }, $dispose: function() {
    a2.stop(), p2 = [], f2 = [], o2._s.delete(e2);
  } }, C2 = vt("production" !== process.env.NODE_ENV || ql ? Li({ _hmrPayload: w2, _customProperties: kt(/* @__PURE__ */ new Set()) }, _2) : _2);
  o2._s.set(e2, C2);
  const x2 = (o2._a && o2._a.runWithContext || Di)(() => o2._e.run(() => (a2 = ae()).run(t2)));
  for (const t3 in x2) {
    const a3 = x2[t3];
    if (Mt(a3) && !Pi(a3) || bt(a3))
      "production" !== process.env.NODE_ENV && r2 ? Tl(v2.value, t3, jt(x2, t3)) : s2 || (!h2 || Wl(k2 = a3) && k2.hasOwnProperty(Ti) || (Mt(a3) ? a3.value = h2[t3] : Vi(a3, h2[t3])), o2.state.value[e2][t3] = a3), "production" !== process.env.NODE_ENV && w2.state.push(t3);
    else if ("function" == typeof a3) {
      const e3 = "production" !== process.env.NODE_ENV && r2 ? a3 : b2(t3, a3);
      x2[t3] = e3, "production" !== process.env.NODE_ENV && (w2.actions[t3] = a3), l2.actions[t3] = a3;
    } else if ("production" !== process.env.NODE_ENV && Pi(a3) && (w2.getters[t3] = s2 ? n2.getters[t3] : a3, Gl)) {
      (x2._getters || (x2._getters = kt([]))).push(t3);
    }
  }
  var k2;
  if (Li(C2, x2), Li(xt(C2), x2), Object.defineProperty(C2, "$state", { get: () => "production" !== process.env.NODE_ENV && r2 ? v2.value : o2.state.value[e2], set: (e3) => {
    if ("production" !== process.env.NODE_ENV && r2)
      throw new Error("cannot set hotState");
    g2((t3) => {
      Li(t3, e3);
    });
  } }), "production" !== process.env.NODE_ENV && (C2._hotUpdate = kt((t3) => {
    C2._hotUpdating = true, t3._hmrPayload.state.forEach((e3) => {
      if (e3 in C2.$state) {
        const n3 = t3.$state[e3], o3 = C2.$state[e3];
        "object" == typeof n3 && Wl(n3) && Wl(o3) ? Oi(n3, o3) : t3.$state[e3] = o3;
      }
      Tl(C2, e3, jt(t3.$state, e3));
    }), Object.keys(C2.$state).forEach((e3) => {
      e3 in t3.$state || Ll(C2, e3);
    }), c2 = false, d2 = false, o2.state.value[e2] = jt(t3._hmrPayload, "hotState"), d2 = true, un().then(() => {
      c2 = true;
    });
    for (const e3 in t3._hmrPayload.actions) {
      const n3 = t3[e3];
      Tl(C2, e3, b2(e3, n3));
    }
    for (const e3 in t3._hmrPayload.getters) {
      const n3 = t3._hmrPayload.getters[e3], r3 = s2 ? ga(() => (Zl(o2), n3.call(C2, C2))) : n3;
      Tl(C2, e3, r3);
    }
    Object.keys(C2._hmrPayload.getters).forEach((e3) => {
      e3 in t3._hmrPayload.getters || Ll(C2, e3);
    }), Object.keys(C2._hmrPayload.actions).forEach((e3) => {
      e3 in t3._hmrPayload.actions || Ll(C2, e3);
    }), C2._hmrPayload = t3._hmrPayload, C2._getters = t3._getters, C2._hotUpdating = false;
  })), ql) {
    const e3 = { writable: true, configurable: true, enumerable: false };
    ["_p", "_hmrPayload", "_getters", "_customProperties"].forEach((t3) => {
      Object.defineProperty(C2, t3, Li({ value: C2[t3] }, e3));
    });
  }
  return o2._p.forEach((e3) => {
    if (ql) {
      const t3 = a2.run(() => e3({ store: C2, app: o2._a, pinia: o2, options: l2 }));
      Object.keys(t3 || {}).forEach((e4) => C2._customProperties.add(e4)), Li(C2, t3);
    } else
      Li(C2, a2.run(() => e3({ store: C2, app: o2._a, pinia: o2, options: l2 })));
  }), "production" !== process.env.NODE_ENV && C2.$state && "object" == typeof C2.$state && "function" == typeof C2.$state.constructor && !C2.$state.constructor.toString().includes("[native code]") && console.warn(`[\u{1F34D}]: The "state" must be a plain object. It cannot be
	state: () => new MyClass()
Found in store "${C2.$id}".`), h2 && s2 && n2.hydrate && n2.hydrate(C2.$state, h2), c2 = true, d2 = true, C2;
}
var $i = ((e2) => (e2.CPro = "cpro", e2.NCA = "nca", e2.Fake = "fake", e2))($i || {}), Bi = ((e2) => (e2.Qualified = "qualified", e2.Simple = "simple", e2.Mock = "mock", e2))(Bi || {}), Hi = ((e2) => (e2[e2.CAPICOM_CERTIFICATE_FIND_SHA1_HASH = 0] = "CAPICOM_CERTIFICATE_FIND_SHA1_HASH", e2[e2.CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME = 1] = "CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME", e2[e2.CAPICOM_CERTIFICATE_FIND_ISSUER_NAME = 2] = "CAPICOM_CERTIFICATE_FIND_ISSUER_NAME", e2[e2.CAPICOM_CERTIFICATE_FIND_ROOT_NAME = 3] = "CAPICOM_CERTIFICATE_FIND_ROOT_NAME", e2[e2.CAPICOM_CERTIFICATE_FIND_TEMPLATE_NAME = 4] = "CAPICOM_CERTIFICATE_FIND_TEMPLATE_NAME", e2[e2.CAPICOM_CERTIFICATE_FIND_EXTENSION = 5] = "CAPICOM_CERTIFICATE_FIND_EXTENSION", e2[e2.CAPICOM_CERTIFICATE_FIND_EXTENDED_PROPERTY = 6] = "CAPICOM_CERTIFICATE_FIND_EXTENDED_PROPERTY", e2[e2.CAPICOM_CERTIFICATE_FIND_APPLICATION_POLICY = 7] = "CAPICOM_CERTIFICATE_FIND_APPLICATION_POLICY", e2[e2.CAPICOM_CERTIFICATE_FIND_CERTIFICATE_POLICY = 8] = "CAPICOM_CERTIFICATE_FIND_CERTIFICATE_POLICY", e2[e2.CAPICOM_CERTIFICATE_FIND_TIME_VALID = 9] = "CAPICOM_CERTIFICATE_FIND_TIME_VALID", e2[e2.CAPICOM_CERTIFICATE_FIND_TIME_NOT_YET_VALID = 10] = "CAPICOM_CERTIFICATE_FIND_TIME_NOT_YET_VALID", e2[e2.CAPICOM_CERTIFICATE_FIND_TIME_EXPIRED = 11] = "CAPICOM_CERTIFICATE_FIND_TIME_EXPIRED", e2[e2.CAPICOM_CERTIFICATE_FIND_KEY_USAGE = 12] = "CAPICOM_CERTIFICATE_FIND_KEY_USAGE", e2))(Hi || {}), ji = ((e2) => (e2[e2.CAPICOM_CERTIFICATE_INCLUDE_CHAIN_EXCEPT_ROOT = 0] = "CAPICOM_CERTIFICATE_INCLUDE_CHAIN_EXCEPT_ROOT", e2[e2.CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY = 2] = "CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY", e2[e2.CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN = 1] = "CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN", e2))(ji || {}), Ki = ((e2) => (e2[e2.CADESCOM_CADES_BES = 1] = "CADESCOM_CADES_BES", e2[e2.CADESCOM_CADES_DEFAULT = 0] = "CADESCOM_CADES_DEFAULT", e2[e2.CADESCOM_CADES_T = 5] = "CADESCOM_CADES_T", e2[e2.CADESCOM_CADES_X_LONG_TYPE_1 = 93] = "CADESCOM_CADES_X_LONG_TYPE_1", e2))(Ki || {}), zi = ((e2) => (e2[e2.CADESCOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME = 0] = "CADESCOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME", e2[e2.CADESCOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME = 1] = "CADESCOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME", e2[e2.CADESCOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_DESCRIPTION = 2] = "CADESCOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_DESCRIPTION", e2[e2.CADESCOM_ATTRIBUTE_OTHER = -1] = "CADESCOM_ATTRIBUTE_OTHER", e2))(zi || {}), Zi = ((e2) => (e2[e2.LOG_LEVEL_DEBUG = 4] = "LOG_LEVEL_DEBUG", e2[e2.LOG_LEVEL_INFO = 2] = "LOG_LEVEL_INFO", e2[e2.LOG_LEVEL_ERROR = 1] = "LOG_LEVEL_ERROR", e2))(Zi || {});
const Ui = { "Amazon Silk": "amazon_silk", "Android Browser": "android", Bada: "bada", BlackBerry: "blackberry", Chrome: "chrome", Chromium: "chromium", Electron: "electron", Epiphany: "epiphany", Firefox: "firefox", Focus: "focus", Generic: "generic", "Google Search": "google_search", Googlebot: "googlebot", "Internet Explorer": "ie", "K-Meleon": "k_meleon", Maxthon: "maxthon", "Microsoft Edge": "edge", "MZ Browser": "mz", "NAVER Whale Browser": "naver", Opera: "opera", "Opera Coast": "opera_coast", PhantomJS: "phantomjs", Puffin: "puffin", QupZilla: "qupzilla", QQ: "qq", QQLite: "qqlite", Safari: "safari", Sailfish: "sailfish", "Samsung Internet for Android": "samsung_internet", SeaMonkey: "seamonkey", Sleipnir: "sleipnir", Swing: "swing", Tizen: "tizen", "UC Browser": "uc", Vivaldi: "vivaldi", "WebOS Browser": "webos", WeChat: "wechat", "Yandex Browser": "yandex", Roku: "roku" }, Wi = { amazon_silk: "Amazon Silk", android: "Android Browser", bada: "Bada", blackberry: "BlackBerry", chrome: "Chrome", chromium: "Chromium", electron: "Electron", epiphany: "Epiphany", firefox: "Firefox", focus: "Focus", generic: "Generic", googlebot: "Googlebot", google_search: "Google Search", ie: "Internet Explorer", k_meleon: "K-Meleon", maxthon: "Maxthon", edge: "Microsoft Edge", mz: "MZ Browser", naver: "NAVER Whale Browser", opera: "Opera", opera_coast: "Opera Coast", phantomjs: "PhantomJS", puffin: "Puffin", qupzilla: "QupZilla", qq: "QQ Browser", qqlite: "QQ Browser Lite", safari: "Safari", sailfish: "Sailfish", samsung_internet: "Samsung Internet for Android", seamonkey: "SeaMonkey", sleipnir: "Sleipnir", swing: "Swing", tizen: "Tizen", uc: "UC Browser", vivaldi: "Vivaldi", webos: "WebOS Browser", wechat: "WeChat", yandex: "Yandex Browser" }, Yi = { tablet: "tablet", mobile: "mobile", desktop: "desktop", tv: "tv" }, Gi = { WindowsPhone: "Windows Phone", Windows: "Windows", MacOS: "macOS", iOS: "iOS", Android: "Android", WebOS: "WebOS", BlackBerry: "BlackBerry", Bada: "Bada", Tizen: "Tizen", Linux: "Linux", ChromeOS: "Chrome OS", PlayStation4: "PlayStation 4", Roku: "Roku" }, qi = { EdgeHTML: "EdgeHTML", Blink: "Blink", Trident: "Trident", Presto: "Presto", Gecko: "Gecko", WebKit: "WebKit" };
class Xi {
  static getFirstMatch(e2, t2) {
    const n2 = t2.match(e2);
    return n2 && n2.length > 0 && n2[1] || "";
  }
  static getSecondMatch(e2, t2) {
    const n2 = t2.match(e2);
    return n2 && n2.length > 1 && n2[2] || "";
  }
  static matchAndReturnConst(e2, t2, n2) {
    if (e2.test(t2))
      return n2;
  }
  static getWindowsVersionName(e2) {
    switch (e2) {
      case "NT":
        return "NT";
      case "XP":
      case "NT 5.1":
        return "XP";
      case "NT 5.0":
        return "2000";
      case "NT 5.2":
        return "2003";
      case "NT 6.0":
        return "Vista";
      case "NT 6.1":
        return "7";
      case "NT 6.2":
        return "8";
      case "NT 6.3":
        return "8.1";
      case "NT 10.0":
        return "10";
      default:
        return;
    }
  }
  static getMacOSVersionName(e2) {
    const t2 = e2.split(".").splice(0, 2).map((e3) => parseInt(e3, 10) || 0);
    if (t2.push(0), 10 === t2[0])
      switch (t2[1]) {
        case 5:
          return "Leopard";
        case 6:
          return "Snow Leopard";
        case 7:
          return "Lion";
        case 8:
          return "Mountain Lion";
        case 9:
          return "Mavericks";
        case 10:
          return "Yosemite";
        case 11:
          return "El Capitan";
        case 12:
          return "Sierra";
        case 13:
          return "High Sierra";
        case 14:
          return "Mojave";
        case 15:
          return "Catalina";
        default:
          return;
      }
  }
  static getAndroidVersionName(e2) {
    const t2 = e2.split(".").splice(0, 2).map((e3) => parseInt(e3, 10) || 0);
    if (t2.push(0), !(1 === t2[0] && t2[1] < 5))
      return 1 === t2[0] && t2[1] < 6 ? "Cupcake" : 1 === t2[0] && t2[1] >= 6 ? "Donut" : 2 === t2[0] && t2[1] < 2 ? "Eclair" : 2 === t2[0] && 2 === t2[1] ? "Froyo" : 2 === t2[0] && t2[1] > 2 ? "Gingerbread" : 3 === t2[0] ? "Honeycomb" : 4 === t2[0] && t2[1] < 1 ? "Ice Cream Sandwich" : 4 === t2[0] && t2[1] < 4 ? "Jelly Bean" : 4 === t2[0] && t2[1] >= 4 ? "KitKat" : 5 === t2[0] ? "Lollipop" : 6 === t2[0] ? "Marshmallow" : 7 === t2[0] ? "Nougat" : 8 === t2[0] ? "Oreo" : 9 === t2[0] ? "Pie" : void 0;
  }
  static getVersionPrecision(e2) {
    return e2.split(".").length;
  }
  static compareVersions(e2, t2, n2 = false) {
    const o2 = Xi.getVersionPrecision(e2), r2 = Xi.getVersionPrecision(t2);
    let s2 = Math.max(o2, r2), a2 = 0;
    const l2 = Xi.map([e2, t2], (e3) => {
      const t3 = s2 - Xi.getVersionPrecision(e3), n3 = e3 + new Array(t3 + 1).join(".0");
      return Xi.map(n3.split("."), (e4) => new Array(20 - e4.length).join("0") + e4).reverse();
    });
    for (n2 && (a2 = s2 - Math.min(o2, r2)), s2 -= 1; s2 >= a2; ) {
      if (l2[0][s2] > l2[1][s2])
        return 1;
      if (l2[0][s2] === l2[1][s2]) {
        if (s2 === a2)
          return 0;
        s2 -= 1;
      } else if (l2[0][s2] < l2[1][s2])
        return -1;
    }
  }
  static map(e2, t2) {
    const n2 = [];
    let o2;
    if (Array.prototype.map)
      return Array.prototype.map.call(e2, t2);
    for (o2 = 0; o2 < e2.length; o2 += 1)
      n2.push(t2(e2[o2]));
    return n2;
  }
  static find(e2, t2) {
    let n2, o2;
    if (Array.prototype.find)
      return Array.prototype.find.call(e2, t2);
    for (n2 = 0, o2 = e2.length; n2 < o2; n2 += 1) {
      const o3 = e2[n2];
      if (t2(o3, n2))
        return o3;
    }
  }
  static assign(e2, ...t2) {
    const n2 = e2;
    let o2, r2;
    if (Object.assign)
      return Object.assign(e2, ...t2);
    for (o2 = 0, r2 = t2.length; o2 < r2; o2 += 1) {
      const e3 = t2[o2];
      if ("object" == typeof e3 && null !== e3) {
        Object.keys(e3).forEach((t3) => {
          n2[t3] = e3[t3];
        });
      }
    }
    return e2;
  }
  static getBrowserAlias(e2) {
    return Ui[e2];
  }
  static getBrowserTypeByAlias(e2) {
    return Wi[e2] || "";
  }
}
const Ji = /version\/(\d+(\.?_?\d+)+)/i, Qi = [{ test: [/googlebot/i], describe(e2) {
  const t2 = { name: "Googlebot" }, n2 = Xi.getFirstMatch(/googlebot\/(\d+(\.\d+))/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/opera/i], describe(e2) {
  const t2 = { name: "Opera" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:opera)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/opr\/|opios/i], describe(e2) {
  const t2 = { name: "Opera" }, n2 = Xi.getFirstMatch(/(?:opr|opios)[\s/](\S+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/SamsungBrowser/i], describe(e2) {
  const t2 = { name: "Samsung Internet for Android" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:SamsungBrowser)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/Whale/i], describe(e2) {
  const t2 = { name: "NAVER Whale Browser" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:whale)[\s/](\d+(?:\.\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/MZBrowser/i], describe(e2) {
  const t2 = { name: "MZ Browser" }, n2 = Xi.getFirstMatch(/(?:MZBrowser)[\s/](\d+(?:\.\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/focus/i], describe(e2) {
  const t2 = { name: "Focus" }, n2 = Xi.getFirstMatch(/(?:focus)[\s/](\d+(?:\.\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/swing/i], describe(e2) {
  const t2 = { name: "Swing" }, n2 = Xi.getFirstMatch(/(?:swing)[\s/](\d+(?:\.\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/coast/i], describe(e2) {
  const t2 = { name: "Opera Coast" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:coast)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/opt\/\d+(?:.?_?\d+)+/i], describe(e2) {
  const t2 = { name: "Opera Touch" }, n2 = Xi.getFirstMatch(/(?:opt)[\s/](\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/yabrowser/i], describe(e2) {
  const t2 = { name: "Yandex Browser" }, n2 = Xi.getFirstMatch(/(?:yabrowser)[\s/](\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/ucbrowser/i], describe(e2) {
  const t2 = { name: "UC Browser" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:ucbrowser)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/Maxthon|mxios/i], describe(e2) {
  const t2 = { name: "Maxthon" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:Maxthon|mxios)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/epiphany/i], describe(e2) {
  const t2 = { name: "Epiphany" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:epiphany)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/puffin/i], describe(e2) {
  const t2 = { name: "Puffin" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:puffin)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/sleipnir/i], describe(e2) {
  const t2 = { name: "Sleipnir" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:sleipnir)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/k-meleon/i], describe(e2) {
  const t2 = { name: "K-Meleon" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/(?:k-meleon)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/micromessenger/i], describe(e2) {
  const t2 = { name: "WeChat" }, n2 = Xi.getFirstMatch(/(?:micromessenger)[\s/](\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/qqbrowser/i], describe(e2) {
  const t2 = { name: /qqbrowserlite/i.test(e2) ? "QQ Browser Lite" : "QQ Browser" }, n2 = Xi.getFirstMatch(/(?:qqbrowserlite|qqbrowser)[/](\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/msie|trident/i], describe(e2) {
  const t2 = { name: "Internet Explorer" }, n2 = Xi.getFirstMatch(/(?:msie |rv:)(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/\sedg\//i], describe(e2) {
  const t2 = { name: "Microsoft Edge" }, n2 = Xi.getFirstMatch(/\sedg\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/edg([ea]|ios)/i], describe(e2) {
  const t2 = { name: "Microsoft Edge" }, n2 = Xi.getSecondMatch(/edg([ea]|ios)\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/vivaldi/i], describe(e2) {
  const t2 = { name: "Vivaldi" }, n2 = Xi.getFirstMatch(/vivaldi\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/seamonkey/i], describe(e2) {
  const t2 = { name: "SeaMonkey" }, n2 = Xi.getFirstMatch(/seamonkey\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/sailfish/i], describe(e2) {
  const t2 = { name: "Sailfish" }, n2 = Xi.getFirstMatch(/sailfish\s?browser\/(\d+(\.\d+)?)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/silk/i], describe(e2) {
  const t2 = { name: "Amazon Silk" }, n2 = Xi.getFirstMatch(/silk\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/phantom/i], describe(e2) {
  const t2 = { name: "PhantomJS" }, n2 = Xi.getFirstMatch(/phantomjs\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/slimerjs/i], describe(e2) {
  const t2 = { name: "SlimerJS" }, n2 = Xi.getFirstMatch(/slimerjs\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/blackberry|\bbb\d+/i, /rim\stablet/i], describe(e2) {
  const t2 = { name: "BlackBerry" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/blackberry[\d]+\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/(web|hpw)[o0]s/i], describe(e2) {
  const t2 = { name: "WebOS Browser" }, n2 = Xi.getFirstMatch(Ji, e2) || Xi.getFirstMatch(/w(?:eb)?[o0]sbrowser\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/bada/i], describe(e2) {
  const t2 = { name: "Bada" }, n2 = Xi.getFirstMatch(/dolfin\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/tizen/i], describe(e2) {
  const t2 = { name: "Tizen" }, n2 = Xi.getFirstMatch(/(?:tizen\s?)?browser\/(\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/qupzilla/i], describe(e2) {
  const t2 = { name: "QupZilla" }, n2 = Xi.getFirstMatch(/(?:qupzilla)[\s/](\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/firefox|iceweasel|fxios/i], describe(e2) {
  const t2 = { name: "Firefox" }, n2 = Xi.getFirstMatch(/(?:firefox|iceweasel|fxios)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/electron/i], describe(e2) {
  const t2 = { name: "Electron" }, n2 = Xi.getFirstMatch(/(?:electron)\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/MiuiBrowser/i], describe(e2) {
  const t2 = { name: "Miui" }, n2 = Xi.getFirstMatch(/(?:MiuiBrowser)[\s/](\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/chromium/i], describe(e2) {
  const t2 = { name: "Chromium" }, n2 = Xi.getFirstMatch(/(?:chromium)[\s/](\d+(\.?_?\d+)+)/i, e2) || Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/chrome|crios|crmo/i], describe(e2) {
  const t2 = { name: "Chrome" }, n2 = Xi.getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/GSA/i], describe(e2) {
  const t2 = { name: "Google Search" }, n2 = Xi.getFirstMatch(/(?:GSA)\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test(e2) {
  const t2 = !e2.test(/like android/i), n2 = e2.test(/android/i);
  return t2 && n2;
}, describe(e2) {
  const t2 = { name: "Android Browser" }, n2 = Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/playstation 4/i], describe(e2) {
  const t2 = { name: "PlayStation 4" }, n2 = Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/safari|applewebkit/i], describe(e2) {
  const t2 = { name: "Safari" }, n2 = Xi.getFirstMatch(Ji, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/.*/i], describe(e2) {
  const t2 = -1 !== e2.search("\\(") ? /^(.*)\/(.*)[ \t]\((.*)/ : /^(.*)\/(.*) /;
  return { name: Xi.getFirstMatch(t2, e2), version: Xi.getSecondMatch(t2, e2) };
} }], ec = [{ test: [/Roku\/DVP/], describe(e2) {
  const t2 = Xi.getFirstMatch(/Roku\/DVP-(\d+\.\d+)/i, e2);
  return { name: Gi.Roku, version: t2 };
} }, { test: [/windows phone/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i, e2);
  return { name: Gi.WindowsPhone, version: t2 };
} }, { test: [/windows /i], describe(e2) {
  const t2 = Xi.getFirstMatch(/Windows ((NT|XP)( \d\d?.\d)?)/i, e2), n2 = Xi.getWindowsVersionName(t2);
  return { name: Gi.Windows, version: t2, versionName: n2 };
} }, { test: [/Macintosh(.*?) FxiOS(.*?)\//], describe(e2) {
  const t2 = { name: Gi.iOS }, n2 = Xi.getSecondMatch(/(Version\/)(\d[\d.]+)/, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/macintosh/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/mac os x (\d+(\.?_?\d+)+)/i, e2).replace(/[_\s]/g, "."), n2 = Xi.getMacOSVersionName(t2), o2 = { name: Gi.MacOS, version: t2 };
  return n2 && (o2.versionName = n2), o2;
} }, { test: [/(ipod|iphone|ipad)/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/os (\d+([_\s]\d+)*) like mac os x/i, e2).replace(/[_\s]/g, ".");
  return { name: Gi.iOS, version: t2 };
} }, { test(e2) {
  const t2 = !e2.test(/like android/i), n2 = e2.test(/android/i);
  return t2 && n2;
}, describe(e2) {
  const t2 = Xi.getFirstMatch(/android[\s/-](\d+(\.\d+)*)/i, e2), n2 = Xi.getAndroidVersionName(t2), o2 = { name: Gi.Android, version: t2 };
  return n2 && (o2.versionName = n2), o2;
} }, { test: [/(web|hpw)[o0]s/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/(?:web|hpw)[o0]s\/(\d+(\.\d+)*)/i, e2), n2 = { name: Gi.WebOS };
  return t2 && t2.length && (n2.version = t2), n2;
} }, { test: [/blackberry|\bbb\d+/i, /rim\stablet/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/rim\stablet\sos\s(\d+(\.\d+)*)/i, e2) || Xi.getFirstMatch(/blackberry\d+\/(\d+([_\s]\d+)*)/i, e2) || Xi.getFirstMatch(/\bbb(\d+)/i, e2);
  return { name: Gi.BlackBerry, version: t2 };
} }, { test: [/bada/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/bada\/(\d+(\.\d+)*)/i, e2);
  return { name: Gi.Bada, version: t2 };
} }, { test: [/tizen/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/tizen[/\s](\d+(\.\d+)*)/i, e2);
  return { name: Gi.Tizen, version: t2 };
} }, { test: [/linux/i], describe: () => ({ name: Gi.Linux }) }, { test: [/CrOS/], describe: () => ({ name: Gi.ChromeOS }) }, { test: [/PlayStation 4/], describe(e2) {
  const t2 = Xi.getFirstMatch(/PlayStation 4[/\s](\d+(\.\d+)*)/i, e2);
  return { name: Gi.PlayStation4, version: t2 };
} }], tc = [{ test: [/googlebot/i], describe: () => ({ type: "bot", vendor: "Google" }) }, { test: [/huawei/i], describe(e2) {
  const t2 = Xi.getFirstMatch(/(can-l01)/i, e2) && "Nova", n2 = { type: Yi.mobile, vendor: "Huawei" };
  return t2 && (n2.model = t2), n2;
} }, { test: [/nexus\s*(?:7|8|9|10).*/i], describe: () => ({ type: Yi.tablet, vendor: "Nexus" }) }, { test: [/ipad/i], describe: () => ({ type: Yi.tablet, vendor: "Apple", model: "iPad" }) }, { test: [/Macintosh(.*?) FxiOS(.*?)\//], describe: () => ({ type: Yi.tablet, vendor: "Apple", model: "iPad" }) }, { test: [/kftt build/i], describe: () => ({ type: Yi.tablet, vendor: "Amazon", model: "Kindle Fire HD 7" }) }, { test: [/silk/i], describe: () => ({ type: Yi.tablet, vendor: "Amazon" }) }, { test: [/tablet(?! pc)/i], describe: () => ({ type: Yi.tablet }) }, { test(e2) {
  const t2 = e2.test(/ipod|iphone/i), n2 = e2.test(/like (ipod|iphone)/i);
  return t2 && !n2;
}, describe(e2) {
  const t2 = Xi.getFirstMatch(/(ipod|iphone)/i, e2);
  return { type: Yi.mobile, vendor: "Apple", model: t2 };
} }, { test: [/nexus\s*[0-6].*/i, /galaxy nexus/i], describe: () => ({ type: Yi.mobile, vendor: "Nexus" }) }, { test: [/[^-]mobi/i], describe: () => ({ type: Yi.mobile }) }, { test: (e2) => "blackberry" === e2.getBrowserName(true), describe: () => ({ type: Yi.mobile, vendor: "BlackBerry" }) }, { test: (e2) => "bada" === e2.getBrowserName(true), describe: () => ({ type: Yi.mobile }) }, { test: (e2) => "windows phone" === e2.getBrowserName(), describe: () => ({ type: Yi.mobile, vendor: "Microsoft" }) }, { test(e2) {
  const t2 = Number(String(e2.getOSVersion()).split(".")[0]);
  return "android" === e2.getOSName(true) && t2 >= 3;
}, describe: () => ({ type: Yi.tablet }) }, { test: (e2) => "android" === e2.getOSName(true), describe: () => ({ type: Yi.mobile }) }, { test: (e2) => "macos" === e2.getOSName(true), describe: () => ({ type: Yi.desktop, vendor: "Apple" }) }, { test: (e2) => "windows" === e2.getOSName(true), describe: () => ({ type: Yi.desktop }) }, { test: (e2) => "linux" === e2.getOSName(true), describe: () => ({ type: Yi.desktop }) }, { test: (e2) => "playstation 4" === e2.getOSName(true), describe: () => ({ type: Yi.tv }) }, { test: (e2) => "roku" === e2.getOSName(true), describe: () => ({ type: Yi.tv }) }], nc = [{ test: (e2) => "microsoft edge" === e2.getBrowserName(true), describe(e2) {
  if (/\sedg\//i.test(e2))
    return { name: qi.Blink };
  const t2 = Xi.getFirstMatch(/edge\/(\d+(\.?_?\d+)+)/i, e2);
  return { name: qi.EdgeHTML, version: t2 };
} }, { test: [/trident/i], describe(e2) {
  const t2 = { name: qi.Trident }, n2 = Xi.getFirstMatch(/trident\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: (e2) => e2.test(/presto/i), describe(e2) {
  const t2 = { name: qi.Presto }, n2 = Xi.getFirstMatch(/presto\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test(e2) {
  const t2 = e2.test(/gecko/i), n2 = e2.test(/like gecko/i);
  return t2 && !n2;
}, describe(e2) {
  const t2 = { name: qi.Gecko }, n2 = Xi.getFirstMatch(/gecko\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }, { test: [/(apple)?webkit\/537\.36/i], describe: () => ({ name: qi.Blink }) }, { test: [/(apple)?webkit/i], describe(e2) {
  const t2 = { name: qi.WebKit }, n2 = Xi.getFirstMatch(/webkit\/(\d+(\.?_?\d+)+)/i, e2);
  return n2 && (t2.version = n2), t2;
} }];
class oc {
  constructor(e2, t2 = false) {
    if (null == e2 || "" === e2)
      throw new Error("UserAgent parameter can't be empty");
    this._ua = e2, this.parsedResult = {}, true !== t2 && this.parse();
  }
  getUA() {
    return this._ua;
  }
  test(e2) {
    return e2.test(this._ua);
  }
  parseBrowser() {
    this.parsedResult.browser = {};
    const e2 = Xi.find(Qi, (e3) => {
      if ("function" == typeof e3.test)
        return e3.test(this);
      if (e3.test instanceof Array)
        return e3.test.some((e4) => this.test(e4));
      throw new Error("Browser's test function is not valid");
    });
    return e2 && (this.parsedResult.browser = e2.describe(this.getUA())), this.parsedResult.browser;
  }
  getBrowser() {
    return this.parsedResult.browser ? this.parsedResult.browser : this.parseBrowser();
  }
  getBrowserName(e2) {
    return e2 ? String(this.getBrowser().name).toLowerCase() || "" : this.getBrowser().name || "";
  }
  getBrowserVersion() {
    return this.getBrowser().version;
  }
  getOS() {
    return this.parsedResult.os ? this.parsedResult.os : this.parseOS();
  }
  parseOS() {
    this.parsedResult.os = {};
    const e2 = Xi.find(ec, (e3) => {
      if ("function" == typeof e3.test)
        return e3.test(this);
      if (e3.test instanceof Array)
        return e3.test.some((e4) => this.test(e4));
      throw new Error("Browser's test function is not valid");
    });
    return e2 && (this.parsedResult.os = e2.describe(this.getUA())), this.parsedResult.os;
  }
  getOSName(e2) {
    const { name: t2 } = this.getOS();
    return e2 ? String(t2).toLowerCase() || "" : t2 || "";
  }
  getOSVersion() {
    return this.getOS().version;
  }
  getPlatform() {
    return this.parsedResult.platform ? this.parsedResult.platform : this.parsePlatform();
  }
  getPlatformType(e2 = false) {
    const { type: t2 } = this.getPlatform();
    return e2 ? String(t2).toLowerCase() || "" : t2 || "";
  }
  parsePlatform() {
    this.parsedResult.platform = {};
    const e2 = Xi.find(tc, (e3) => {
      if ("function" == typeof e3.test)
        return e3.test(this);
      if (e3.test instanceof Array)
        return e3.test.some((e4) => this.test(e4));
      throw new Error("Browser's test function is not valid");
    });
    return e2 && (this.parsedResult.platform = e2.describe(this.getUA())), this.parsedResult.platform;
  }
  getEngine() {
    return this.parsedResult.engine ? this.parsedResult.engine : this.parseEngine();
  }
  getEngineName(e2) {
    return e2 ? String(this.getEngine().name).toLowerCase() || "" : this.getEngine().name || "";
  }
  parseEngine() {
    this.parsedResult.engine = {};
    const e2 = Xi.find(nc, (e3) => {
      if ("function" == typeof e3.test)
        return e3.test(this);
      if (e3.test instanceof Array)
        return e3.test.some((e4) => this.test(e4));
      throw new Error("Browser's test function is not valid");
    });
    return e2 && (this.parsedResult.engine = e2.describe(this.getUA())), this.parsedResult.engine;
  }
  parse() {
    return this.parseBrowser(), this.parseOS(), this.parsePlatform(), this.parseEngine(), this;
  }
  getResult() {
    return Xi.assign({}, this.parsedResult);
  }
  satisfies(e2) {
    const t2 = {};
    let n2 = 0;
    const o2 = {};
    let r2 = 0;
    if (Object.keys(e2).forEach((s2) => {
      const a2 = e2[s2];
      "string" == typeof a2 ? (o2[s2] = a2, r2 += 1) : "object" == typeof a2 && (t2[s2] = a2, n2 += 1);
    }), n2 > 0) {
      const e3 = Object.keys(t2), n3 = Xi.find(e3, (e4) => this.isOS(e4));
      if (n3) {
        const e4 = this.satisfies(t2[n3]);
        if (void 0 !== e4)
          return e4;
      }
      const o3 = Xi.find(e3, (e4) => this.isPlatform(e4));
      if (o3) {
        const e4 = this.satisfies(t2[o3]);
        if (void 0 !== e4)
          return e4;
      }
    }
    if (r2 > 0) {
      const e3 = Object.keys(o2), t3 = Xi.find(e3, (e4) => this.isBrowser(e4, true));
      if (void 0 !== t3)
        return this.compareVersion(o2[t3]);
    }
  }
  isBrowser(e2, t2 = false) {
    const n2 = this.getBrowserName().toLowerCase();
    let o2 = e2.toLowerCase();
    const r2 = Xi.getBrowserTypeByAlias(o2);
    return t2 && r2 && (o2 = r2.toLowerCase()), o2 === n2;
  }
  compareVersion(e2) {
    let t2 = [0], n2 = e2, o2 = false;
    const r2 = this.getBrowserVersion();
    if ("string" == typeof r2)
      return ">" === e2[0] || "<" === e2[0] ? (n2 = e2.substr(1), "=" === e2[1] ? (o2 = true, n2 = e2.substr(2)) : t2 = [], ">" === e2[0] ? t2.push(1) : t2.push(-1)) : "=" === e2[0] ? n2 = e2.substr(1) : "~" === e2[0] && (o2 = true, n2 = e2.substr(1)), t2.indexOf(Xi.compareVersions(r2, n2, o2)) > -1;
  }
  isOS(e2) {
    return this.getOSName(true) === String(e2).toLowerCase();
  }
  isPlatform(e2) {
    return this.getPlatformType(true) === String(e2).toLowerCase();
  }
  isEngine(e2) {
    return this.getEngineName(true) === String(e2).toLowerCase();
  }
  is(e2, t2 = false) {
    return this.isBrowser(e2, t2) || this.isOS(e2) || this.isPlatform(e2);
  }
  some(e2 = []) {
    return e2.some((e3) => this.is(e3));
  }
}
/*!
 * Bowser - a browser detector
 * https://github.com/lancedikson/bowser
 * MIT License | (c) Dustin Diaz 2012-2015
 * MIT License | (c) Denis Demchenko 2015-2019
 */
class rc {
  static getParser(e2, t2 = false) {
    if ("string" != typeof e2)
      throw new Error("UserAgent should be a string");
    return new oc(e2, t2);
  }
  static parse(e2) {
    return new oc(e2).getResult();
  }
  static get BROWSER_MAP() {
    return Wi;
  }
  static get ENGINE_MAP() {
    return qi;
  }
  static get OS_MAP() {
    return Gi;
  }
  static get PLATFORMS_MAP() {
    return Yi;
  }
}
var sc = ((e2) => (e2.ExtensionWithStaticUrl = "extension_with_static_url", e2.ExtensionWithDynamicUrl = "extension_with_dynamic_url", e2.NPAPI = "npapi", e2))(sc || {});
const _ac = class {
  constructor(e2) {
    this.env = e2;
  }
  static i() {
    return _ac._instance || (_ac._instance = _ac.createByBrowser()), _ac._instance;
  }
  static createByBrowser() {
    var _a2, _b2;
    const e2 = rc.getParser(window.navigator.userAgent);
    let t2;
    const n2 = e2.getBrowser().name;
    switch (n2) {
      case rc.BROWSER_MAP.firefox:
      case rc.BROWSER_MAP.edge:
      case rc.BROWSER_MAP.safari:
        t2 = { type: "extension_with_dynamic_url" };
        break;
      case rc.BROWSER_MAP.ie:
        t2 = { type: "npapi" };
        break;
      default:
        t2 = { type: "extension_with_static_url", connectionUrl: n2 && [rc.BROWSER_MAP.opera, rc.BROWSER_MAP.yandex].includes(n2) ? "chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js" : "chrome-extension://iifchhfnnmpdbibifmljnfjhpififfog/nmcades_plugin_api.js" };
    }
    const o2 = { pluginConnectionType: t2, canPromise: "Promise" in window, systemInfo: { browser: { name: e2.getBrowserName(), version: (_a2 = e2.getBrowserVersion()) != null ? _a2 : "unknown" }, os: { name: e2.getOSName(), version: (_b2 = e2.getOSVersion()) != null ? _b2 : "unknown" } } };
    return new _ac(o2);
  }
  get pluginConnectionInfo() {
    return this.env.pluginConnectionType;
  }
  get canPromise() {
    return this.env.canPromise;
  }
  toJson() {
    return JSON.stringify(this.env);
  }
};
let ac = _ac;
__publicField(ac, "_instance");
class lc {
  constructor(e2) {
    this._plugin = e2;
  }
  async getCertificate(e2) {
    const t2 = await this._plugin._providerInstance.CreateObjectAsync("CAdESCOM.Store");
    await t2.Open();
    const n2 = await t2.Certificates, o2 = await n2.Find(Hi.CAPICOM_CERTIFICATE_FIND_SHA1_HASH, e2);
    if (!await o2.Count)
      throw new Error(`Certificate ${e2} was not found in store`);
    const r2 = await o2.Item(1);
    if (console.log("FOUNDED CERT ", e2, await r2.Thumbprint), !r2)
      throw new Error("Error while extract item at index 1");
    return r2;
  }
  async getCertificates() {
    const e2 = await this._plugin._providerInstance.CreateObjectAsync("CAdESCOM.Store");
    await e2.Open();
    const t2 = await e2.Certificates, n2 = await t2.Count, o2 = /* @__PURE__ */ new Map();
    if (!n2)
      return o2;
    for (let e3 = 0; e3 < n2; e3++) {
      const n3 = await t2.Item(e3 + 1), r2 = await n3.SubjectName, s2 = { cn: cc(r2, ic.DN_ATTRIBUTE_COMMON_NAME), o: cc(r2, ic.DN_ATTRIBUTE_ORGANIZATION_NAME), n: cc(r2, ic.DN_ATTRIBUTE_USER_SURNAME), g: cc(r2, ic.DN_ATTRIBUTE_GIVENNAME) }, a2 = await n3.IssuerName, l2 = { o: cc(a2, ic.DN_ATTRIBUTE_ORGANIZATION_NAME) }, i2 = new Date(await n3.ValidToDate), c2 = { serialNumber: await n3.SerialNumber, thumbprint: await n3.Thumbprint, owner: r2, getName() {
        const { subject: e4 = { cn: "", o: "" }, issuer: t3 = { o: "" } } = { subject: s2, issuer: l2 }, n4 = e4.o && e4.o !== e4.cn ? `${e4.o}, ` : "", o3 = `\u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u0435\u043D \u0434\u043E ${dc(i2)}\u0433.`;
        return `${e4.cn}, ${n4} ${o3}, ${t3.o}`;
      } };
      o2.set(c2.thumbprint, c2);
    }
    return await e2.Close(), o2;
  }
}
const ic = { DN_ATTRIBUTE_COMMON_NAME: "CN", DN_ATTRIBUTE_LOCALITY_NAME: "L", DN_ATTRIBUTE_STATE_OR_PROVINCE_NAME: "S", DN_ATTRIBUTE_ORGANIZATION_NAME: "O", DN_ATTRIBUTE_ORGANIZATION_UNIT_NAME: "OU", DN_ATTRIBUTE_COUNTRY_NAME: "C", DN_ATTRIBUTE_COUNTRY_EMAIL: "E", DN_ATTRIBUTE_STREET_ADDRESS: "STREET", DN_ATTRIBUTE_USER_SURNAME: "SN", DN_ATTRIBUTE_GIVENNAME: "G", DN_ATTRIBUTE_TITLE: "T" }, cc = (e2, t2, n2 = "") => {
  let o2 = e2.split(", ");
  for (let e3 = 0; e3 < o2.length; e3++) {
    let n3 = o2[e3].split("=", 2);
    if (n3[0] && n3[1]) {
      if (n3[0].toUpperCase() === t2)
        return n3[1].replace(/["']/g, "");
    }
  }
  return n2;
}, dc = (e2, t2 = false) => {
  if ("object" != typeof e2) {
    const n3 = new RegExp("^[0-9]+.[0-9]+.[0-9]{4}" + (t2 ? "s[0-9]:[0-9]:[0-9]" : "")).exec(e2);
    if (n3 && n3[0])
      return n3[0];
    e2 = new Date(e2);
  }
  let n2 = e2.getMonth() + 1, o2 = String(n2);
  n2 < 10 && (o2 = "0" + o2);
  let r2 = e2.getDate() + "." + o2 + "." + e2.getFullYear();
  if (t2) {
    let t3 = ("0" + e2.getMinutes()).slice(-2);
    r2 += " " + ("0" + e2.getHours()).slice(-2) + ":" + t3 + ":" + ("0" + e2.getSeconds()).slice(-2);
  }
  return r2;
};
async function uc(e2, t2, n2 = { async: true, defer: true }) {
  return new Promise((o2, r2) => {
    const s2 = document.createElement("script");
    s2.type = "text/javascript", Object.assign(s2, n2), s2.addEventListener("load", () => {
      console.log("load listener"), o2({ remove() {
        document.removeChild(s2);
      } });
    }), s2.addEventListener("error", r2), s2.src = `${e2}${t2 ? `?${new URLSearchParams(t2).toString()}` : ""}`, document.body.appendChild(s2);
  });
}
class pc {
  constructor(e2) {
    __publicField(this, "current_log_level", Zi.LOG_LEVEL_ERROR);
    __publicField(this, "_pluginPromise");
    __publicField(this, "_initFn");
    __publicField(this, "LOG_LEVEL_DEBUG", Zi.LOG_LEVEL_DEBUG);
    __publicField(this, "LOG_LEVEL_INFO", Zi.LOG_LEVEL_INFO);
    __publicField(this, "LOG_LEVEL_ERROR", Zi.LOG_LEVEL_ERROR);
    this._initFn = e2, window.cadesplugin = this;
  }
  get _plugin() {
    return this._pluginPromise || (this._pluginPromise = this._initFn), this._pluginPromise;
  }
  get [Symbol.toStringTag]() {
    return "Promise";
  }
  then(e2, t2) {
    return this._plugin.then(e2, t2);
  }
  catch(e2) {
    return this._plugin.catch(e2);
  }
  finally(e2) {
    return this._plugin.finally(e2);
  }
}
class fc {
  constructor(e2, t2) {
    this._plugin = e2, this._store = t2;
  }
  async signText(e2, t2) {
    const n2 = await this._store.getCertificate(t2), o2 = await this._plugin.i.CreateObjectAsync("CAdESCOM.CPSigner");
    await o2.propset_Certificate(n2), await o2.propset_Options(ji.CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN);
    const r2 = await this._plugin.i.CreateObjectAsync("CAdESCOM.CadesSignedData"), s2 = await this._plugin.i.CreateObjectAsync("CAdESCOM.CPAttribute");
    await s2.propset_Name(zi.CADESCOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME), await s2.propset_Value(new Date()), await r2.propset_Content(e2);
    const a2 = (await r2.SignCades(o2, Ki.CADESCOM_CADES_BES, false)).replace(/[^\x00-\x7F]/g, "").replace(/(\r\n|\n|\r)/gm, "");
    return console.info("created signature is ", a2), a2;
  }
}
class hc {
  constructor(e2) {
    __publicField(this, "storeService");
    __publicField(this, "signService");
    this._providerInstance = e2, this.storeService = new lc(this), this.signService = new fc(this, this.storeService);
  }
  static async initByEnvironment(e2) {
    if (console.log("try create bp by environment", e2), !e2.canPromise)
      throw new Error("\u041F\u043E \u043A\u0430\u043A\u043E\u0439 \u0442\u043E \u043F\u0440\u0438\u0447\u0438\u043D\u0435 \u043D\u0435\u0442 Promise, \u0434\u043B\u044F \u0440\u0430\u0441\u0448\u0438\u0440\u0435\u043D\u0438\u044F \u043D\u0443\u0436\u0435\u043D \u043F\u043E\u043B\u0438\u0444\u0438\u043B \u0438\u043B\u0438 \u044D\u043A\u0441\u043F\u043E\u0442\u043D\u0443\u0442\u044C \u0435\u0433\u043E \u0432 window");
    const { pluginConnectionInfo: t2 } = e2;
    switch (console.log("Plugin connection info is", t2), t2.type) {
      case sc.ExtensionWithStaticUrl:
        return await hc.createWithStaticUrl(t2.connectionUrl);
      case sc.ExtensionWithDynamicUrl:
        return await hc.createWithUrlResolve();
      default:
        throw new Error("Wrong environment for async plugin wrapper passed");
    }
  }
  static async createWithStaticUrl(e2) {
    const t2 = await new pc(function(e3) {
      return new Promise(async (t3, n2) => {
        let o2 = false;
        const r2 = (e4) => {
          o2 || (o2 = true, n2(e4));
        }, s2 = async (n3) => {
          if ("string" == typeof n3.data && n3.data.match("cadesplugin_loaded"))
            try {
              await uc(e3);
              const n4 = await cpcsp_chrome_nmcades.CreatePluginObject(), o3 = await n4.CreateObjectAsync("CAdESCOM.About");
              console.log("Plugin CSPName ", await o3.CSPName()), t3(n4);
            } catch (e4) {
              r2(e4);
            } finally {
              window.removeEventListener("message", s2, false);
            }
        };
        try {
          setTimeout(() => r2(new Error("load timeout")), 5e4), window.addEventListener("message", s2, false), window.postMessage("cadesplugin_echo_request", "*");
        } catch (e4) {
          r2(e4 || "\u041F\u043B\u0430\u0433\u0438\u043D \u043D\u0435\u0434\u043E\u0441\u0442\u0443\u043F\u0435\u043D!");
        }
      });
    }(e2));
    return new hc(t2);
  }
  static async createWithUrlResolve() {
    const e2 = await new pc(new Promise(async (e3, t2) => {
      let n2 = false;
      const o2 = (e4) => {
        n2 || (n2 = true, t2(e4));
      }, r2 = async (t3) => {
        if ("string" == typeof t3.data && t3.data.match("cadesplugin_loaded"))
          try {
            const n3 = t3.data.split("url:").slice(1).join("");
            console.log("dynamic extension url ", n3), await uc(n3);
            const o3 = await cpcsp_chrome_nmcades.CreatePluginObject(), r3 = await o3.CreateObjectAsync("CAdESCOM.About");
            console.log("Plugin CSPName ", await r3.CSPName()), e3(o3);
          } catch (e4) {
            o2(e4);
          } finally {
            window.removeEventListener("message", r2, false);
          }
      };
      try {
        setTimeout(() => o2(new Error("load timeout")), 5e4), window.addEventListener("message", r2, false), window.postMessage("cadesplugin_echo_request", "*");
      } catch (e4) {
        o2(e4);
      }
    }));
    return new hc(e2);
  }
  get i() {
    return this._providerInstance;
  }
}
const vc = function(e2, t2, n2) {
  let o2, r2;
  const s2 = "function" == typeof t2;
  if ("string" == typeof e2)
    o2 = e2, r2 = s2 ? n2 : t2;
  else if (r2 = e2, o2 = e2.id, "production" !== process.env.NODE_ENV && "string" != typeof o2)
    throw new Error('[\u{1F34D}]: "defineStore()" must be passed a store id as its first argument.');
  function a2(e3, n3) {
    const l2 = !!(qs || zn || Er);
    if ((e3 = ("test" === process.env.NODE_ENV && Hl && Hl._testing ? null : e3) || (l2 ? Or(Ul, null) : null)) && Zl(e3), "production" !== process.env.NODE_ENV && !Hl)
      throw new Error('[\u{1F34D}]: "getActivePinia()" was called but there was no active Pinia. Are you trying to use a store before calling "app.use(pinia)"?\nSee https://pinia.vuejs.org/core-concepts/outside-component-usage.html for help.\nThis will fail in production.');
    (e3 = Hl)._s.has(o2) || (s2 ? Ri(o2, t2, r2, e3) : Fi(o2, r2, e3), "production" !== process.env.NODE_ENV && (a2._pinia = e3));
    const i2 = e3._s.get(o2);
    if ("production" !== process.env.NODE_ENV && n3) {
      const a3 = "__hot:" + o2, l3 = s2 ? Ri(a3, t2, r2, e3, true) : Fi(a3, Li({}, r2), e3, true);
      n3._hotUpdate(l3), delete e3.state.value[a3], e3._s.delete(a3);
    }
    if ("production" !== process.env.NODE_ENV && Gl) {
      const e4 = Xs();
      if (e4 && e4.proxy && !n3) {
        const t3 = e4.proxy;
        ("_pStores" in t3 ? t3._pStores : t3._pStores = {})[o2] = i2;
      }
    }
    return i2;
  }
  return a2.$id = o2, a2;
}("createSignatureWithCertificate", (e2) => {
  const t2 = vt({ ...e2 || {}, status: "idle", selectedCertificate: void 0, error: void 0, signature: void 0 });
  const n2 = () => {
    t2.status = "idle", t2.selectedCertificate = void 0, t2.error = void 0, t2.signature = void 0;
  }, o2 = (e3) => {
    t2.status = "processing", t2.processing = { message: e3 };
  }, r2 = (e3) => {
    t2.status = "error", t2.error = new Error(e3);
  }, s2 = async () => {
    o2("\u041F\u043E\u0434\u043A\u043B\u044E\u0447\u0435\u043D\u0438\u0435 \u043A \u043F\u043B\u0430\u0433\u0438\u043D\u0443");
    const { env: e3, getBrowserPluginFactory: n3, browserPluginFactory: s3, connectBrowserPluginFactoryError: a3 } = function() {
      const e4 = At(null), t3 = At(null), n4 = At(null), o3 = At(false);
      return { env: e4, getBrowserPluginFactory: async () => {
        e4.value = ac.i(), o3.value = true;
        try {
          t3.value = await hc.initByEnvironment(e4.value);
        } catch (e5) {
          console.error(e5), n4.value = new Error("Plugin connection error");
        } finally {
          o3.value = false;
        }
      }, browserPluginFactory: t3, connectBrowserPluginFactoryError: n4 };
    }();
    return await n3(), a3.value ? (console.log("Raised Error in connectToPlugin", a3.value), void r2("\u041E\u0448\u0438\u0431\u043A\u0430 \u043F\u0440\u0438 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0435\u043D\u0438\u0438 \u043A \u043F\u043B\u0430\u0433\u0438\u043D\u0443")) : (e3 == null ? void 0 : e3.value) ? (s3 == null ? void 0 : s3.value) ? (t2.env = e3.value, void (t2.objectFactory = s3.value)) : (console.log("\u041F\u043E \u043A\u0430\u043A\u043E\u0439 \u0442\u043E \u043F\u0440\u0438\u0447\u0438\u043D\u0435 \u043D\u0435\u0442 \u043E\u0448\u0438\u0431\u043A\u0438 \u0438 \u043D\u0435\u0442 \u0444\u0430\u0431\u0440\u0438\u043A\u0438 \u043E\u0431\u044A\u0435\u043A\u0442\u043E\u0432 "), void r2("\u041E\u0448\u0438\u0431\u043A\u0430 \u043F\u0440\u0438 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0435\u043D\u0438\u0438 \u043A \u043F\u043B\u0430\u0433\u0438\u043D\u0443")) : (console.log("\u043F\u043E \u043A\u0430\u043A\u043E\u0439 \u0442\u043E \u043F\u0440\u0438\u0447\u0438\u043D\u0435 \u043D\u0435\u0442 \u043E\u0448\u0438\u0431\u043A\u0438 \u043D\u043E \u0438 \u043D\u0435\u0442 env"), void r2("\u041E\u0448\u0438\u0431\u043A\u0430 \u043F\u0440\u0438 \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0435\u043D\u0438\u0438 \u043A \u043F\u043B\u0430\u0433\u0438\u043D\u0443"));
  }, a2 = async () => {
    if (o2("\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442\u043E\u0432"), !t2.objectFactory)
      return void console.error("\u041A\u0430\u043A\u043E\u0433\u043E \u0442\u043E \u0445\u0440\u0435\u043D\u0430 \u043D\u0435\u0442 \u0444\u0430\u0431\u0440\u0438\u043A\u0438");
    const { certificates: e3, fetchCertificateList: n3, certificatesFetchError: s3 } = function(e4) {
      const t3 = At(false), n4 = At(null), o3 = At(null);
      return { isCertificatesFetching: t3, certificates: n4, fetchCertificateList: async () => {
        n4.value = null, t3.value = true, o3.value = null;
        try {
          n4.value = await e4.getCertificates();
        } catch (e5) {
          o3.value = new Error("\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u0438\u0437\u0432\u043B\u0435\u0447\u044C \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442\u044B \u0438\u0437 \u0445\u0440\u0430\u043D\u0438\u043B\u0438\u0449\u0430");
        } finally {
          t3.value = false;
        }
      }, certificatesFetchError: o3 };
    }(t2.objectFactory.storeService);
    await n3(), (s3 == null ? void 0 : s3.value) ? r2("\u041E\u0448\u0438\u0431\u043A\u0430 \u0437\u0430\u0433\u0440\u0443\u0437\u043A\u0438 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442\u043E\u0432") : e3.value ? (t2.status = "idle", t2.certificates = e3.value) : r2("\u041A\u0430\u043A\u043E\u0433\u043E \u0442\u043E \u0445\u0440\u0435\u043D\u0430 \u043D\u0435\u0442 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442\u043E\u0432 \u0438 \u043E\u0448\u0438\u0431\u043A\u0438 \u0442\u043E\u0436\u0435 \u043D\u0435\u0442");
  }, l2 = async (e3, n3) => {
    if (o2("\u0424\u043E\u0440\u043C\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u0435 \u043F\u043E\u0434\u043F\u0438\u0441\u0438"), !t2.objectFactory)
      return void console.error("\u041A\u0430\u043A\u043E\u0433\u043E \u0442\u043E \u0445\u0440\u0435\u043D\u0430 \u043D\u0435\u0442 \u0444\u0430\u0431\u0440\u0438\u043A\u0438");
    const { signature: s3, createSignature: a3, createSignatureError: l3 } = function(e4) {
      const t3 = At(false), n4 = At(null), o3 = At(null);
      return { isSigning: t3, signature: n4, createSignature: async (r3) => {
        t3.value = true;
        const { content: s4, strategy: a4 } = r3;
        if ("text" !== s4.type)
          return o3.value = new Error("\u041D\u0435\u043F\u043E\u0434\u0445\u043E\u0434\u044F\u0449\u0438\u0439 \u0442\u0438\u043F \u043A\u043E\u043D\u0442\u0435\u043A\u0441\u0442\u0430"), void (t3.value = false);
        const { type: l4, software: i3 } = a4;
        if ("text" === s4.type && l4 === Bi.Qualified && i3 === $i.CPro)
          try {
            n4.value = await e4.signText(s4.unsigned_message, a4.certificate_identifier);
          } catch (e5) {
            console.error(e5), o3.value = new Error("\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u0441\u0444\u043E\u0440\u043C\u0438\u0440\u043E\u0432\u0430\u0442\u044C \u043F\u043E\u0434\u043F\u0438\u0441\u044C");
          }
        else
          o3.value = new Error("\u041D\u0435\u043F\u043E\u0434\u0445\u043E\u0434\u044F\u0449\u0438\u0439 \u043A\u043E\u043D\u0442\u0435\u043A\u0441\u0442");
        t3.value = false;
      }, createSignatureError: o3 };
    }(t2.objectFactory.signService), i2 = { content: n3, strategy: { type: "qualified", software: "cpro", certificate_identifier: e3 } };
    return await a3(i2), (l3 == null ? void 0 : l3.value) ? (console.log("sign error", l3.value), void r2("\u041E\u0448\u0438\u0431\u043A\u0430 \u043F\u0440\u0438 \u0441\u043E\u0437\u0434\u0430\u043D\u0438\u0438 \u043F\u043E\u0434\u043F\u0438\u0441\u0438")) : (s3 == null ? void 0 : s3.value) ? void ((e4) => {
      t2.status = "done", t2.signature = e4;
    })(s3.value) : (console.log("sign error", s3.value), void r2("\u041E\u0448\u0438\u0431\u043A\u0430 \u043F\u0440\u0438 \u0441\u043E\u0437\u0434\u0430\u043D\u0438\u0438 \u043F\u043E\u0434\u043F\u0438\u0441\u0438"));
  };
  return { state: t2, sign: async (e3, o3) => {
    n2(), t2.selectedCertificate = o3, console.log("SSign in sign store process invoked ", e3, t2), (t2.objectFactory || (await s2(), "error" !== t2.status)) && (t2.certificates || (await a2(), "error" !== t2.status)) && await l2(t2.selectedCertificate, e3.content);
  }, loadCertificatesList: async () => {
    n2(), console.log("loadCertificatesList in sign store process invoked ", t2), (t2.objectFactory || (await s2(), "error" !== t2.status)) && (t2.certificates || (await a2(), "error" !== t2.status)) && (t2.status = "certificate_select");
  } };
}), mc = Ls("h4", { class: "cds-mb-m" }, "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442", -1), gc = { class: "cds-mt-none cds-mb-xxs" }, yc = { class: "cds-font-body-size-xs" }, bc = Lo({ __name: "CertificatesListView", props: { modelValue: { default: /* @__PURE__ */ new Map() }, modelModifiers: {} }, emits: dr(["changed", "confirmed"], ["update:modelValue"]), setup(e2, { emit: t2 }) {
  const n2 = ya(e2, "modelValue"), o2 = At(null);
  ho(o2, (e3) => {
    console.log("selected: ", e3), r2("changed", o2.value);
  });
  const r2 = t2;
  return (e3, t3) => {
    const s2 = ro("CdsContainerItem"), a2 = ro("CdsContainer"), l2 = ro("CdsButton");
    return Ss(), Ns(bs, null, [Ps(a2, null, { default: Wn(() => [Ps(s2, null, { default: Wn(() => [mc, Ps(a2, { modelValue: o2.value, "onUpdate:modelValue": t3[0] || (t3[0] = (e4) => o2.value = e4), required: "", direction: "vertical", appearance: "secondary", "prepend-icon": "administrator" }, { default: Wn(() => [(Ss(true), Ns(bs, null, er(n2.value.values(), (e4) => (Ss(), Ms(s2, { key: e4.thumbprint, value: e4.thumbprint, group: "radio", type: "selectable", label: "\u0413\u0440\u0443\u043F\u043F\u0430 \u043A\u043E\u043D\u0442\u0435\u0439\u043D\u0435\u0440\u043E\u0432" }, { default: Wn(() => [Ls("h6", gc, Q(e4.thumbprint), 1), Ls("span", yc, Q(e4.getName()), 1)]), _: 2 }, 1032, ["value"]))), 128))]), _: 1 }, 8, ["modelValue"])]), _: 1 })]), _: 1 }), Ps(a2, { class: "cds-flex cds-justify-center cds-mt-no cds-mb-m" }, { default: Wn(() => [Ps(s2, null, { default: Wn(() => [Ps(l2, { appearance: "primary", onClick: t3[1] || (t3[1] = (e4) => {
      var _a2;
      return r2("confirmed", (_a2 = o2.value) != null ? _a2 : "");
    }), disabled: null === o2.value }, { default: Wn(() => [Hs(" \u041F\u043E\u0434\u043F\u0438\u0441\u0430\u0442\u044C ")]), _: 1 }, 8, ["disabled"])]), _: 1 })]), _: 1 })], 64);
  };
} }), wc = { key: 0 }, _c = Lo({ __name: "ProcessignView", props: { modelValue: { default: {} }, modelModifiers: {} }, emits: ["update:modelValue"], setup(e2) {
  const t2 = ya(e2, "modelValue");
  return (e3, n2) => {
    const o2 = ro("CdsLoader"), r2 = ro("CdsContainerItem"), s2 = ro("CdsContainer");
    return Ss(), Ms(s2, null, { default: Wn(() => [Ps(r2, null, { default: Wn(() => [Ps(o2), t2.value.message ? (Ss(), Ns("p", wc, Q(t2.value.message), 1)) : js("", true)]), _: 1 })]), _: 1 });
  };
} }), Cc = { key: 0 }, xc = Lo({ __name: "ErrorView", props: { modelValue: { default: {} }, modelModifiers: {} }, emits: ["update:modelValue"], setup(e2) {
  const t2 = ya(e2, "modelValue");
  return (e3, n2) => {
    const o2 = ro("CdsContainerItem"), r2 = ro("CdsContainer");
    return Ss(), Ms(r2, null, { default: Wn(() => [Ps(o2, null, { default: Wn(() => [Ls("h4", null, Q(t2.value.title || "\u041F\u0440\u043E\u0438\u0437\u043E\u0448\u043B\u0430 \u043E\u0448\u0438\u0431\u043A\u0430"), 1), t2.value.message ? (Ss(), Ns("p", Cc, Q(t2.value.message), 1)) : js("", true), nr(e3.$slots, "default")]), _: 3 })]), _: 3 });
  };
} });
var kc, Sc;
!function(e2) {
  e2.CPro = "cpro", e2.NCA = "nca", e2.Fake = "fake";
}(kc || (kc = {})), function(e2) {
  e2.Qualified = "qualified", e2.Simple = "simple", e2.Mock = "mock";
}(Sc || (Sc = {}));
var Ec = function(e2, t2) {
  return Ec = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(e3, t3) {
    e3.__proto__ = t3;
  } || function(e3, t3) {
    for (var n2 in t3)
      Object.prototype.hasOwnProperty.call(t3, n2) && (e3[n2] = t3[n2]);
  }, Ec(e2, t2);
};
function Ic(e2, t2) {
  if ("function" != typeof t2 && null !== t2)
    throw new TypeError("Class extends value " + String(t2) + " is not a constructor or null");
  function n2() {
    this.constructor = e2;
  }
  Ec(e2, t2), e2.prototype = null === t2 ? Object.create(t2) : (n2.prototype = t2.prototype, new n2());
}
function Oc(e2) {
  var t2 = "function" == typeof Symbol && Symbol.iterator, n2 = t2 && e2[t2], o2 = 0;
  if (n2)
    return n2.call(e2);
  if (e2 && "number" == typeof e2.length)
    return { next: function() {
      return e2 && o2 >= e2.length && (e2 = void 0), { value: e2 && e2[o2++], done: !e2 };
    } };
  throw new TypeError(t2 ? "Object is not iterable." : "Symbol.iterator is not defined.");
}
function Nc(e2, t2) {
  var n2 = "function" == typeof Symbol && e2[Symbol.iterator];
  if (!n2)
    return e2;
  var o2, r2, s2 = n2.call(e2), a2 = [];
  try {
    for (; (void 0 === t2 || t2-- > 0) && !(o2 = s2.next()).done; )
      a2.push(o2.value);
  } catch (e3) {
    r2 = { error: e3 };
  } finally {
    try {
      o2 && !o2.done && (n2 = s2.return) && n2.call(s2);
    } finally {
      if (r2)
        throw r2.error;
    }
  }
  return a2;
}
function Mc(e2, t2, n2) {
  if (n2 || 2 === arguments.length)
    for (var o2, r2 = 0, s2 = t2.length; r2 < s2; r2++)
      !o2 && r2 in t2 || (o2 || (o2 = Array.prototype.slice.call(t2, 0, r2)), o2[r2] = t2[r2]);
  return e2.concat(o2 || Array.prototype.slice.call(t2));
}
function Ac(e2) {
  return "function" == typeof e2;
}
function Dc(e2) {
  var t2 = e2(function(e3) {
    Error.call(e3), e3.stack = new Error().stack;
  });
  return t2.prototype = Object.create(Error.prototype), t2.prototype.constructor = t2, t2;
}
"function" == typeof SuppressedError && SuppressedError;
var Vc = Dc(function(e2) {
  return function(t2) {
    e2(this), this.message = t2 ? t2.length + " errors occurred during unsubscription:\n" + t2.map(function(e3, t3) {
      return t3 + 1 + ") " + e3.toString();
    }).join("\n  ") : "", this.name = "UnsubscriptionError", this.errors = t2;
  };
});
function Tc(e2, t2) {
  if (e2) {
    var n2 = e2.indexOf(t2);
    0 <= n2 && e2.splice(n2, 1);
  }
}
var Lc = function() {
  function e2(e3) {
    this.initialTeardown = e3, this.closed = false, this._parentage = null, this._finalizers = null;
  }
  var t2;
  return e2.prototype.unsubscribe = function() {
    var e3, t3, n2, o2, r2;
    if (!this.closed) {
      this.closed = true;
      var s2 = this._parentage;
      if (s2)
        if (this._parentage = null, Array.isArray(s2))
          try {
            for (var a2 = Oc(s2), l2 = a2.next(); !l2.done; l2 = a2.next()) {
              l2.value.remove(this);
            }
          } catch (t4) {
            e3 = { error: t4 };
          } finally {
            try {
              l2 && !l2.done && (t3 = a2.return) && t3.call(a2);
            } finally {
              if (e3)
                throw e3.error;
            }
          }
        else
          s2.remove(this);
      var i2 = this.initialTeardown;
      if (Ac(i2))
        try {
          i2();
        } catch (e4) {
          r2 = e4 instanceof Vc ? e4.errors : [e4];
        }
      var c2 = this._finalizers;
      if (c2) {
        this._finalizers = null;
        try {
          for (var d2 = Oc(c2), u2 = d2.next(); !u2.done; u2 = d2.next()) {
            var p2 = u2.value;
            try {
              Rc(p2);
            } catch (e4) {
              r2 = null != r2 ? r2 : [], e4 instanceof Vc ? r2 = Mc(Mc([], Nc(r2)), Nc(e4.errors)) : r2.push(e4);
            }
          }
        } catch (e4) {
          n2 = { error: e4 };
        } finally {
          try {
            u2 && !u2.done && (o2 = d2.return) && o2.call(d2);
          } finally {
            if (n2)
              throw n2.error;
          }
        }
      }
      if (r2)
        throw new Vc(r2);
    }
  }, e2.prototype.add = function(t3) {
    var n2;
    if (t3 && t3 !== this)
      if (this.closed)
        Rc(t3);
      else {
        if (t3 instanceof e2) {
          if (t3.closed || t3._hasParent(this))
            return;
          t3._addParent(this);
        }
        (this._finalizers = null !== (n2 = this._finalizers) && void 0 !== n2 ? n2 : []).push(t3);
      }
  }, e2.prototype._hasParent = function(e3) {
    var t3 = this._parentage;
    return t3 === e3 || Array.isArray(t3) && t3.includes(e3);
  }, e2.prototype._addParent = function(e3) {
    var t3 = this._parentage;
    this._parentage = Array.isArray(t3) ? (t3.push(e3), t3) : t3 ? [t3, e3] : e3;
  }, e2.prototype._removeParent = function(e3) {
    var t3 = this._parentage;
    t3 === e3 ? this._parentage = null : Array.isArray(t3) && Tc(t3, e3);
  }, e2.prototype.remove = function(t3) {
    var n2 = this._finalizers;
    n2 && Tc(n2, t3), t3 instanceof e2 && t3._removeParent(this);
  }, e2.EMPTY = ((t2 = new e2()).closed = true, t2), e2;
}(), Pc = Lc.EMPTY;
function Fc(e2) {
  return e2 instanceof Lc || e2 && "closed" in e2 && Ac(e2.remove) && Ac(e2.add) && Ac(e2.unsubscribe);
}
function Rc(e2) {
  Ac(e2) ? e2() : e2.unsubscribe();
}
var $c = { onUnhandledError: null, onStoppedNotification: null, Promise: void 0, useDeprecatedSynchronousErrorHandling: false, useDeprecatedNextContext: false }, Bc = { setTimeout: function(e2, t2) {
  for (var n2 = [], o2 = 2; o2 < arguments.length; o2++)
    n2[o2 - 2] = arguments[o2];
  return setTimeout.apply(void 0, Mc([e2, t2], Nc(n2)));
}, clearTimeout: function(e2) {
  return clearTimeout(e2);
}, delegate: void 0 };
function Hc(e2) {
  Bc.setTimeout(function() {
    throw e2;
  });
}
function jc() {
}
function Kc(e2) {
  e2();
}
var zc = function(e2) {
  function t2(t3) {
    var n2 = e2.call(this) || this;
    return n2.isStopped = false, t3 ? (n2.destination = t3, Fc(t3) && t3.add(n2)) : n2.destination = qc, n2;
  }
  return Ic(t2, e2), t2.create = function(e3, t3, n2) {
    return new Yc(e3, t3, n2);
  }, t2.prototype.next = function(e3) {
    this.isStopped || this._next(e3);
  }, t2.prototype.error = function(e3) {
    this.isStopped || (this.isStopped = true, this._error(e3));
  }, t2.prototype.complete = function() {
    this.isStopped || (this.isStopped = true, this._complete());
  }, t2.prototype.unsubscribe = function() {
    this.closed || (this.isStopped = true, e2.prototype.unsubscribe.call(this), this.destination = null);
  }, t2.prototype._next = function(e3) {
    this.destination.next(e3);
  }, t2.prototype._error = function(e3) {
    try {
      this.destination.error(e3);
    } finally {
      this.unsubscribe();
    }
  }, t2.prototype._complete = function() {
    try {
      this.destination.complete();
    } finally {
      this.unsubscribe();
    }
  }, t2;
}(Lc), Zc = Function.prototype.bind;
function Uc(e2, t2) {
  return Zc.call(e2, t2);
}
var Wc = function() {
  function e2(e3) {
    this.partialObserver = e3;
  }
  return e2.prototype.next = function(e3) {
    var t2 = this.partialObserver;
    if (t2.next)
      try {
        t2.next(e3);
      } catch (e4) {
        Gc(e4);
      }
  }, e2.prototype.error = function(e3) {
    var t2 = this.partialObserver;
    if (t2.error)
      try {
        t2.error(e3);
      } catch (e4) {
        Gc(e4);
      }
    else
      Gc(e3);
  }, e2.prototype.complete = function() {
    var e3 = this.partialObserver;
    if (e3.complete)
      try {
        e3.complete();
      } catch (e4) {
        Gc(e4);
      }
  }, e2;
}(), Yc = function(e2) {
  function t2(t3, n2, o2) {
    var r2, s2, a2 = e2.call(this) || this;
    Ac(t3) || !t3 ? r2 = { next: null != t3 ? t3 : void 0, error: null != n2 ? n2 : void 0, complete: null != o2 ? o2 : void 0 } : a2 && $c.useDeprecatedNextContext ? ((s2 = Object.create(t3)).unsubscribe = function() {
      return a2.unsubscribe();
    }, r2 = { next: t3.next && Uc(t3.next, s2), error: t3.error && Uc(t3.error, s2), complete: t3.complete && Uc(t3.complete, s2) }) : r2 = t3;
    return a2.destination = new Wc(r2), a2;
  }
  return Ic(t2, e2), t2;
}(zc);
function Gc(e2) {
  Hc(e2);
}
var qc = { closed: true, next: jc, error: function(e2) {
  throw e2;
}, complete: jc }, Xc = "function" == typeof Symbol && Symbol.observable || "@@observable";
function Jc(e2) {
  return e2;
}
var Qc = function() {
  function e2(e3) {
    e3 && (this._subscribe = e3);
  }
  return e2.prototype.lift = function(t2) {
    var n2 = new e2();
    return n2.source = this, n2.operator = t2, n2;
  }, e2.prototype.subscribe = function(e3, t2, n2) {
    var o2, r2 = this, s2 = (o2 = e3) && o2 instanceof zc || function(e4) {
      return e4 && Ac(e4.next) && Ac(e4.error) && Ac(e4.complete);
    }(o2) && Fc(o2) ? e3 : new Yc(e3, t2, n2);
    return Kc(function() {
      var e4 = r2, t3 = e4.operator, n3 = e4.source;
      s2.add(t3 ? t3.call(s2, n3) : n3 ? r2._subscribe(s2) : r2._trySubscribe(s2));
    }), s2;
  }, e2.prototype._trySubscribe = function(e3) {
    try {
      return this._subscribe(e3);
    } catch (t2) {
      e3.error(t2);
    }
  }, e2.prototype.forEach = function(e3, t2) {
    var n2 = this;
    return new (t2 = ed(t2))(function(t3, o2) {
      var r2 = new Yc({ next: function(t4) {
        try {
          e3(t4);
        } catch (e4) {
          o2(e4), r2.unsubscribe();
        }
      }, error: o2, complete: t3 });
      n2.subscribe(r2);
    });
  }, e2.prototype._subscribe = function(e3) {
    var t2;
    return null === (t2 = this.source) || void 0 === t2 ? void 0 : t2.subscribe(e3);
  }, e2.prototype[Xc] = function() {
    return this;
  }, e2.prototype.pipe = function() {
    for (var e3, t2 = [], n2 = 0; n2 < arguments.length; n2++)
      t2[n2] = arguments[n2];
    return (0 === (e3 = t2).length ? Jc : 1 === e3.length ? e3[0] : function(t3) {
      return e3.reduce(function(e4, t4) {
        return t4(e4);
      }, t3);
    })(this);
  }, e2.prototype.toPromise = function(e3) {
    var t2 = this;
    return new (e3 = ed(e3))(function(e4, n2) {
      var o2;
      t2.subscribe(function(e5) {
        return o2 = e5;
      }, function(e5) {
        return n2(e5);
      }, function() {
        return e4(o2);
      });
    });
  }, e2.create = function(t2) {
    return new e2(t2);
  }, e2;
}();
function ed(e2) {
  var t2;
  return null !== (t2 = null != e2 ? e2 : $c.Promise) && void 0 !== t2 ? t2 : Promise;
}
function td(e2) {
  return function(t2) {
    if (function(e3) {
      return Ac(null == e3 ? void 0 : e3.lift);
    }(t2))
      return t2.lift(function(t3) {
        try {
          return e2(t3, this);
        } catch (e3) {
          this.error(e3);
        }
      });
    throw new TypeError("Unable to lift unknown Observable type");
  };
}
var nd, od, rd, sd, ad, ld, id = function(e2) {
  function t2(t3, n2, o2, r2, s2, a2) {
    var l2 = e2.call(this, t3) || this;
    return l2.onFinalize = s2, l2.shouldUnsubscribe = a2, l2._next = n2 ? function(e3) {
      try {
        n2(e3);
      } catch (e4) {
        t3.error(e4);
      }
    } : e2.prototype._next, l2._error = r2 ? function(e3) {
      try {
        r2(e3);
      } catch (e4) {
        t3.error(e4);
      } finally {
        this.unsubscribe();
      }
    } : e2.prototype._error, l2._complete = o2 ? function() {
      try {
        o2();
      } catch (e3) {
        t3.error(e3);
      } finally {
        this.unsubscribe();
      }
    } : e2.prototype._complete, l2;
  }
  return Ic(t2, e2), t2.prototype.unsubscribe = function() {
    var t3;
    if (!this.shouldUnsubscribe || this.shouldUnsubscribe()) {
      var n2 = this.closed;
      e2.prototype.unsubscribe.call(this), !n2 && (null === (t3 = this.onFinalize) || void 0 === t3 || t3.call(this));
    }
  }, t2;
}(zc), cd = Dc(function(e2) {
  return function() {
    e2(this), this.name = "ObjectUnsubscribedError", this.message = "object unsubscribed";
  };
}), dd = function(e2) {
  function t2() {
    var t3 = e2.call(this) || this;
    return t3.closed = false, t3.currentObservers = null, t3.observers = [], t3.isStopped = false, t3.hasError = false, t3.thrownError = null, t3;
  }
  return Ic(t2, e2), t2.prototype.lift = function(e3) {
    var t3 = new ud(this, this);
    return t3.operator = e3, t3;
  }, t2.prototype._throwIfClosed = function() {
    if (this.closed)
      throw new cd();
  }, t2.prototype.next = function(e3) {
    var t3 = this;
    Kc(function() {
      var n2, o2;
      if (t3._throwIfClosed(), !t3.isStopped) {
        t3.currentObservers || (t3.currentObservers = Array.from(t3.observers));
        try {
          for (var r2 = Oc(t3.currentObservers), s2 = r2.next(); !s2.done; s2 = r2.next()) {
            s2.value.next(e3);
          }
        } catch (e4) {
          n2 = { error: e4 };
        } finally {
          try {
            s2 && !s2.done && (o2 = r2.return) && o2.call(r2);
          } finally {
            if (n2)
              throw n2.error;
          }
        }
      }
    });
  }, t2.prototype.error = function(e3) {
    var t3 = this;
    Kc(function() {
      if (t3._throwIfClosed(), !t3.isStopped) {
        t3.hasError = t3.isStopped = true, t3.thrownError = e3;
        for (var n2 = t3.observers; n2.length; )
          n2.shift().error(e3);
      }
    });
  }, t2.prototype.complete = function() {
    var e3 = this;
    Kc(function() {
      if (e3._throwIfClosed(), !e3.isStopped) {
        e3.isStopped = true;
        for (var t3 = e3.observers; t3.length; )
          t3.shift().complete();
      }
    });
  }, t2.prototype.unsubscribe = function() {
    this.isStopped = this.closed = true, this.observers = this.currentObservers = null;
  }, Object.defineProperty(t2.prototype, "observed", { get: function() {
    var e3;
    return (null === (e3 = this.observers) || void 0 === e3 ? void 0 : e3.length) > 0;
  }, enumerable: false, configurable: true }), t2.prototype._trySubscribe = function(t3) {
    return this._throwIfClosed(), e2.prototype._trySubscribe.call(this, t3);
  }, t2.prototype._subscribe = function(e3) {
    return this._throwIfClosed(), this._checkFinalizedStatuses(e3), this._innerSubscribe(e3);
  }, t2.prototype._innerSubscribe = function(e3) {
    var t3 = this, n2 = this, o2 = n2.hasError, r2 = n2.isStopped, s2 = n2.observers;
    return o2 || r2 ? Pc : (this.currentObservers = null, s2.push(e3), new Lc(function() {
      t3.currentObservers = null, Tc(s2, e3);
    }));
  }, t2.prototype._checkFinalizedStatuses = function(e3) {
    var t3 = this, n2 = t3.hasError, o2 = t3.thrownError, r2 = t3.isStopped;
    n2 ? e3.error(o2) : r2 && e3.complete();
  }, t2.prototype.asObservable = function() {
    var e3 = new Qc();
    return e3.source = this, e3;
  }, t2.create = function(e3, t3) {
    return new ud(e3, t3);
  }, t2;
}(Qc), ud = function(e2) {
  function t2(t3, n2) {
    var o2 = e2.call(this) || this;
    return o2.destination = t3, o2.source = n2, o2;
  }
  return Ic(t2, e2), t2.prototype.next = function(e3) {
    var t3, n2;
    null === (n2 = null === (t3 = this.destination) || void 0 === t3 ? void 0 : t3.next) || void 0 === n2 || n2.call(t3, e3);
  }, t2.prototype.error = function(e3) {
    var t3, n2;
    null === (n2 = null === (t3 = this.destination) || void 0 === t3 ? void 0 : t3.error) || void 0 === n2 || n2.call(t3, e3);
  }, t2.prototype.complete = function() {
    var e3, t3;
    null === (t3 = null === (e3 = this.destination) || void 0 === e3 ? void 0 : e3.complete) || void 0 === t3 || t3.call(e3);
  }, t2.prototype._subscribe = function(e3) {
    var t3, n2;
    return null !== (n2 = null === (t3 = this.source) || void 0 === t3 ? void 0 : t3.subscribe(e3)) && void 0 !== n2 ? n2 : Pc;
  }, t2;
}(dd), pd = function(e2) {
  function t2(t3) {
    var n2 = e2.call(this) || this;
    return n2._value = t3, n2;
  }
  return Ic(t2, e2), Object.defineProperty(t2.prototype, "value", { get: function() {
    return this.getValue();
  }, enumerable: false, configurable: true }), t2.prototype._subscribe = function(t3) {
    var n2 = e2.prototype._subscribe.call(this, t3);
    return !n2.closed && t3.next(this._value), n2;
  }, t2.prototype.getValue = function() {
    var e3 = this, t3 = e3.hasError, n2 = e3.thrownError, o2 = e3._value;
    if (t3)
      throw n2;
    return this._throwIfClosed(), o2;
  }, t2.prototype.next = function(t3) {
    e2.prototype.next.call(this, this._value = t3);
  }, t2;
}(dd);
function fd(e2, t2, n2) {
  var o2 = Ac(e2) || t2 || n2 ? { next: e2, error: t2, complete: n2 } : e2;
  return o2 ? td(function(e3, t3) {
    var n3;
    null === (n3 = o2.subscribe) || void 0 === n3 || n3.call(o2);
    var r2, s2, a2, l2, i2 = true;
    e3.subscribe((r2 = function(e4) {
      var n4;
      null === (n4 = o2.next) || void 0 === n4 || n4.call(o2, e4), t3.next(e4);
    }, s2 = function() {
      var e4;
      i2 = false, null === (e4 = o2.complete) || void 0 === e4 || e4.call(o2), t3.complete();
    }, a2 = function(e4) {
      var n4;
      i2 = false, null === (n4 = o2.error) || void 0 === n4 || n4.call(o2, e4), t3.error(e4);
    }, l2 = function() {
      var e4, t4;
      i2 && (null === (e4 = o2.unsubscribe) || void 0 === e4 || e4.call(o2)), null === (t4 = o2.finalize) || void 0 === t4 || t4.call(o2);
    }, new id(t3, r2, s2, a2, l2)));
  }) : Jc;
}
!function(e2) {
  e2.IDLE = "IDLE", e2.PENDING = "PENDING", e2.ERROR = "ERROR", e2.DONE = "DONE";
}(nd || (nd = {}));
class hd {
  constructor(e2) {
    __publicField(this, "state");
    __publicField(this, "apiProvider");
    this.apiProvider = e2, this.state = new pd({ status: nd.IDLE });
  }
  get state$() {
    return this.state.asObservable();
  }
  createSignedDoc(e2, t2) {
    const n2 = { strategy: { type: Sc.Qualified, software: kc.CPro, certificate_identifier: "blah blah" }, content: { type: "text", unsigned_message: t2 }, signature: e2 };
    this.state.next({ status: nd.PENDING, pending_message: "\u041E\u0442\u043F\u0440\u0430\u0432\u043A\u0430 \u0434\u0430\u043D\u043D\u044B\u0445 \u043D\u0430 \u0431\u0435\u043A" });
    const o2 = (r2 = this.apiProvider.createSignedDocument(n2), new Qc(function(e3) {
      r2.then(function(t3) {
        e3.closed || (e3.next(t3), e3.complete());
      }, function(t3) {
        return e3.error(t3);
      }).then(null, Hc);
    })).pipe(fd(console.log)).subscribe({ next: (e3) => this.state.next({ status: nd.DONE, result: { id: e3.doc_id } }), error: (e3) => this.state.next({ status: nd.ERROR, error: e3 }), complete: () => o2.unsubscribe() });
    var r2;
  }
}
class vd {
  constructor(e2, t2 = ((_b2) => (_b2 = ((_a2) => (_a2 = document == null ? void 0 : document.location) == null ? void 0 : _a2.href)()) != null ? _b2 : "https://b2b-center.ru.test/personal/iframe-sign-external/")()) {
    __publicField(this, "access_token");
    __publicField(this, "url");
    this.access_token = e2, this.url = t2;
  }
  async createSignedDocument(e2) {
    var _a2;
    try {
      const t2 = await fetch(this.url, { method: "POST", headers: { "Content-Type": "application/json", Accept: "application/json", Authorization: "Bearer " + this.access_token }, body: JSON.stringify(e2) }), n2 = await t2.json();
      return console.log("B2BCenterByUuidCreateDocProvider create success.", n2), { doc_id: (_a2 = n2.doc_id) != null ? _a2 : 123 };
    } catch (e3) {
      throw console.error(e3), e3;
    }
  }
}
class md {
  constructor() {
    __publicField(this, "store");
    __publicField(this, "_view");
    const e2 = new vd("eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzUxMiIsImtpZCI6IjY5MmJlM2ZjLWY0ODgtNDA4MC1hNjk1LTVmM2QwODI1YjZjOCJ9.eyJ0dHAiOiJhY2MiLCJpc3MiOiJodHRwczovL2IyYi1jZW50ZXIucnUudGVzdC9hdXRoL29wZW5pZC8iLCJzY29wZSI6WyJvcGVuaWQiLCJ1c2VyIiwiZmlybSJdLCJqdGkiOiIzMjEzZDdkNi05ODMxLTRmZjEtODExMC01NzIyZDZkYjg0OTQiLCJhdWQiOiJjYTcyMzFiZS0yNGY1LTQ0OWItYjkwYS1lNDk1NDJiYTQxN2IiLCJtc3ViIjpudWxsLCJzaWQiOiJlMjQyZTk1Zi05NDViLTQyMGMtODc0My1kMGQxMzRmZjc1YTIiLCJzdWIiOiI2MDEzNiIsImlhdCI6MTcxOTMzMzUzNCwiZXhwIjoxNzIwMTk3NTM0fQ.AHkYSy8byFqAPVS4GU_FClE2FFYsw-47TwaaAF7qsT8pNPY8JPcznnr6nPk1l6hXGhEIlZp4uxNhpX39Gi92vq9EAR4QgrCG585zvzGp8Pma1n8f4GYeWNr5wB3AoHlPmEbfR4_hkkOw9UXxrVocmPYlIeTtdoKrrwCcs02QHQpf5zoO");
    this.store = new hd(e2);
  }
  set view(e2) {
    this._view = e2;
  }
  get state() {
    return this.store.state$;
  }
  create(e2, t2) {
    if (this._view) {
      const e3 = this.state.subscribe({ next: (e4) => {
        var _a2, _b2, _c2;
        const { status: t3 } = e4;
        switch (t3) {
          case nd.PENDING:
            (_a2 = this._view) == null ? void 0 : _a2.onCreateSignedDocPending(e4.pending_message);
            break;
          case nd.DONE:
            (_b2 = this._view) == null ? void 0 : _b2.onCreateSignedDocDone({ doc_id: e4.result.id });
            break;
          case nd.ERROR:
            (_c2 = this._view) == null ? void 0 : _c2.onCreateSignedDocError(e4.error);
        }
      }, error: (t3) => {
        var _a2;
        (_a2 = this._view) == null ? void 0 : _a2.onCreateSignedDocError(t3), e3.unsubscribe();
      }, complete: () => {
        e3.unsubscribe();
      } });
    }
    console.log("create DOCUMENT"), this.store.createSignedDoc(e2, t2);
  }
}
!function(e2) {
  e2.SignContextText = "SignContextText", e2.SignContextFiles = "SignContextFiles";
}(od || (od = {})), function(e2) {
  e2.None = "none", e2.Base64 = "base64";
}(rd || (rd = {})), function(e2) {
  e2.BES = "BES", e2.XLT1 = "XLT1";
}(sd || (sd = {})), function(e2) {
  e2.Base = "base", e2.Iframe = "iframe";
}(ad || (ad = {})), function(e2) {
  e2.Json = "application/json", e2.FormData = "multipart/form-data", e2.UrlEncoded = "application/x-www-form-urlencoded", e2.Text = "text/plain";
}(ld || (ld = {}));
const gd = Lo({ __name: "App", props: { autostart: { type: Boolean, default: false }, unsigned_message: { type: String, default: "" }, onSignedMessage: { type: String, required: false } }, setup(e2) {
  const t2 = e2;
  console.log("direct unsigned message in props is", t2.unsigned_message);
  const n2 = Or("sendEdsPostMessagingEvent"), o2 = Or("subscribeEdsPostMessagingCommands");
  function r2(e3) {
    return "$el" in e3 ? { width: e3.$el.offsetWidth, height: e3.$el.offsetHeight } : { width: e3.offsetWidth, height: e3.offsetHeight };
  }
  function s2() {
    console.log("UPDATED HTML", a2.value), a2.value && n2({ name: "html_updated", data: r2(a2.value) });
  }
  Zo(() => {
    s2();
    const { autostart: e3 = false, unsigned_message: n3 } = t2;
    console.log("ON APP MOUNTED", t2, e3), e3 && (f2 = n3, g2());
  }), Wo(() => {
    s2();
  });
  const a2 = At(null);
  At(false);
  const l2 = At(false), i2 = At(/* @__PURE__ */ new Map()), c2 = At(false), d2 = At({}), u2 = At(false), p2 = At({});
  let f2 = (t2 == null ? void 0 : t2.unsigned_message) || "", h2 = (t2 == null ? void 0 : t2.onSignedMessage) || "";
  o2((e3) => {
    switch (console.info("in", e3), e3.name) {
      case "setup": {
        const { content: t3 } = e3.data || {};
        t3 && (f2 = t3);
        break;
      }
      case "load_certificates":
        g2();
        break;
      case "to_processing": {
        const { message: t3 } = e3.data || {};
        d2.value.message = t3, c2.value = true, l2.value = false;
      }
    }
  });
  const { state: v2, sign: m2, loadCertificatesList: g2 } = vc();
  ho(v2, (e3, t3) => {
    var _a2, _b2;
    switch (console.log("state", e3, t3), e3.status) {
      case "processing":
        c2.value = true, d2.value.message = (_b2 = (_a2 = e3.processing) == null ? void 0 : _a2.message) != null ? _b2 : "", u2.value = false, l2.value = false;
        break;
      case "error":
        u2.value = true, e3.error && (p2.value = e3.error), c2.value = false, l2.value = false;
        const t4 = { name: "failed", data: { error: e3.error } };
        n2(t4);
        break;
      case "certificate_select":
        c2.value = false, u2.value = false, l2.value = true, e3.certificates && (i2.value = e3.certificates);
        break;
      case "done":
        if (h2) {
          const t5 = new md();
          t5.view = { onCreateSignedDocDone(e4) {
            c2.value = false, u2.value = false, l2.value = false;
            const t6 = { name: "doc_created", data: { doc_id: e4.doc_id } };
            n2(t6);
          }, onCreateSignedDocError(e4) {
            c2.value = false, u2.value = true, p2.value.message = e4.message, console.log(p2);
          }, onCreateSignedDocPending(e4) {
            c2.value = true, d2.value.message = e4, u2.value = false, l2.value = false;
          } }, t5.create(e3.signature, f2);
        } else {
          c2.value = false, u2.value = false, l2.value = false;
          const t5 = { name: "signed", data: { signature: e3.signature } };
          n2(t5);
        }
        break;
      default:
        c2.value = false, l2.value = false;
    }
  });
  const y2 = (e3) => {
    const t3 = { content: { type: "text", unsigned_message: f2.length ? f2 : "Some test string" }, strategy: { type: "qualified", software: "cpro", certificate_identifier: "ABCDE" } };
    m2(t3, e3);
  };
  return At(false), At(false), (e3, t3) => {
    const n3 = ro("CdsButton");
    return Ss(), Ns("div", { ref_key: "edsSignBlockRef", ref: a2 }, ["idle" === Lt(v2).status ? (Ss(), Ms(n3, { key: 0, onClick: Lt(g2) }, { default: Wn(() => [Hs("Manual load")]), _: 1 }, 8, ["onClick"])) : js("", true), l2.value ? (Ss(), Ms(bc, { key: 1, modelValue: i2.value, "onUpdate:modelValue": t3[0] || (t3[0] = (e4) => i2.value = e4), onConfirmed: y2 }, null, 8, ["modelValue"])) : js("", true), u2.value ? (Ss(), Ms(xc, { key: 2, modelValue: p2.value, "onUpdate:modelValue": t3[1] || (t3[1] = (e4) => p2.value = e4) }, null, 8, ["modelValue"])) : js("", true), c2.value ? (Ss(), Ms(_c, { key: 3, modelValue: d2.value, "onUpdate:modelValue": t3[2] || (t3[2] = (e4) => d2.value = e4) }, null, 8, ["modelValue"])) : js("", true)], 512);
  };
} });
var yd = Object.defineProperty, bd = (e2, t2, n2) => (((e3, t3, n3) => {
  t3 in e3 ? yd(e3, t3, { enumerable: true, configurable: true, writable: true, value: n3 }) : e3[t3] = n3;
})(e2, "symbol" != typeof t2 ? t2 + "" : t2, n2), n2);
const wd = typeof window < "u", _d = wd && "IntersectionObserver" in window, Cd = wd && ("ontouchstart" in window || window.navigator.maxTouchPoints > 0);
wd && typeof CSS < "u" && typeof CSS.supports < "u" && CSS.supports("selector(:focus-visible)");
const xd = "cds-scroll-blocked", kd = Object.freeze({ enter: "Enter", tab: "Tab", delete: "Delete", esc: "Escape", space: " ", up: "ArrowUp", down: "ArrowDown", left: "ArrowLeft", right: "ArrowRight", end: "End", home: "Home", del: "Del", backspace: "Backspace", insert: "Insert", pageup: "PageUp", pagedown: "PageDown", shift: "Shift" }), Sd = Object.freeze({ hover: "cds-state-hover", focus: "cds-state-focus", active: "cds-state-active", visited: "cds-state-visited", disabled: "cds-state-disabled", checked: "cds-state-checked", indeterminate: "cds-state-indeterminate", invalid: "cds-state-invalid", empty: "cds-state-empty", loading: "cds-state-loading", readonly: "cds-state-readonly", dragging: "cds-state-dragging" });
function Ed(e2) {
  return Object.keys(e2);
}
function Id(e2, t2) {
  return t2.every((t3) => e2.hasOwnProperty(t3));
}
function Od(e2) {
  return "string" == typeof e2;
}
function Nd(e2) {
  return Array.isArray(e2);
}
function Md(e2) {
  return null !== e2 && "object" == typeof e2 && !Array.isArray(e2);
}
function Ad(e2) {
  return null != e2 && e2 === e2.window;
}
function Dd(e2 = {}, t2 = {}, n2) {
  const o2 = {};
  for (const t3 in e2)
    o2[t3] = e2[t3];
  for (const r2 in t2) {
    const s2 = e2[r2], a2 = t2[r2];
    Md(s2) && Md(a2) ? o2[r2] = Dd(s2, a2, n2) : Array.isArray(s2) && Array.isArray(a2) && n2 ? o2[r2] = n2(s2, a2) : o2[r2] = a2;
  }
  return o2;
}
function Vd(e2, t2) {
  if (e2 === t2)
    return true;
  if (e2 instanceof Date && t2 instanceof Date && e2.getTime() !== t2.getTime() || e2 !== Object(e2) || t2 !== Object(t2))
    return false;
  const n2 = Object.keys(e2);
  return n2.length === Object.keys(t2).length && n2.every((n3) => Vd(e2[n3], t2[n3]));
}
function Td(e2) {
  return null == e2 ? [] : Array.isArray(e2) ? e2 : [e2];
}
function Ld(e2, t2, n2) {
  const o2 = t2.length - 1;
  if (o2 < 0)
    return void 0 === e2 ? n2 : e2;
  for (let r2 = 0; r2 < o2; r2++) {
    if (null == e2)
      return n2;
    e2 = e2[t2[r2]];
  }
  return null == e2 || void 0 === e2[t2[o2]] ? n2 : e2[t2[o2]];
}
function Pd(e2, t2, n2) {
  if (true === t2)
    return void 0 === e2 ? n2 : e2;
  if (null == t2 || "boolean" == typeof t2)
    return n2;
  if (e2 !== Object(e2)) {
    if ("function" != typeof t2)
      return n2;
    const o3 = t2(e2, n2);
    return typeof o3 > "u" ? n2 : o3;
  }
  if ("string" == typeof t2)
    return function(e3, t3, n3) {
      return null != e3 && t3 && "string" == typeof t3 ? void 0 !== e3[t3] ? e3[t3] : Ld(e3, (t3 = (t3 = t3.replace(/\[(\w+)\]/g, ".$1")).replace(/^\./, "")).split("."), n3) : n3;
    }(e2, t2, n2);
  if (Array.isArray(t2))
    return Ld(e2, t2, n2);
  if ("function" != typeof t2)
    return n2;
  const o2 = t2(e2, n2);
  return typeof o2 > "u" ? n2 : o2;
}
function Fd(e2, t2) {
  const n2 = /* @__PURE__ */ Object.create(null), o2 = /* @__PURE__ */ Object.create(null);
  for (const r2 in e2)
    t2.some((e3) => e3 instanceof RegExp ? e3.test(r2) : e3 === r2) ? n2[r2] = e2[r2] : o2[r2] = e2[r2];
  return [n2, o2];
}
function Rd(e2, t2) {
  return e2.includes(t2);
}
function $d(e2) {
  return e2[2].toLowerCase() + e2.slice(3);
}
function Bd(e2, t2 = "px") {
  if (null != e2 && "" !== e2)
    return isNaN(+e2) ? String(e2) : isFinite(+e2) ? `${Number(e2)}${t2}` : void 0;
}
function Hd(e2) {
  return wd ? parseFloat(e2.replace("rem", "")) * parseFloat(getComputedStyle(document.documentElement).fontSize) : 0;
}
function jd(e2, t2 = 0, n2 = 1) {
  return Math.max(t2, Math.min(n2, e2));
}
function Kd(e2, t2 = 0) {
  return Array.from({ length: e2 }, (e3, n2) => t2 + n2);
}
const zd = /^on[^a-z]/, Zd = (e2) => zd.test(e2);
function Ud(e2, t2, n2 = "0") {
  return e2 + n2.repeat(Math.max(0, t2 - e2.length));
}
function Wd(e2, t2 = 2) {
  return function(e3, t3, n2) {
    return t3 |= 0, e3 = String(e3), n2 = String(n2), e3.length > t3 ? String(e3) : ((t3 -= e3.length) > n2.length && (n2 += n2.repeat(t3 / n2.length)), n2.slice(0, t3) + String(e3));
  }(e2, t2, "0");
}
function Yd(e2) {
  if (e2 && "$el" in e2) {
    const t2 = e2.$el;
    return (null == t2 ? void 0 : t2.nodeType) === Node.TEXT_NODE ? t2.nextElementSibling : t2;
  }
  return e2;
}
function Gd(e2, t2) {
  const n2 = Object.assign({}, e2);
  for (let e3 = 0; e3 < t2.length; e3 += 1) {
    delete n2[t2[e3]];
  }
  return n2;
}
function qd(e2, t2) {
  if (!t2 || "object" != typeof t2)
    return [];
  if (Array.isArray(t2))
    return t2.map((t3) => qd(e2, t3)).flat(1);
  if (Array.isArray(t2.children))
    return t2.children.map((t3) => qd(e2, t3)).flat(1);
  if (t2.component) {
    if (Object.getOwnPropertySymbols(t2.component.provides).includes(e2))
      return [t2.component];
    if (t2.component.subTree)
      return qd(e2, t2.component.subTree).flat(1);
  }
  return [];
}
function Xd(e2) {
  const t2 = ["button", "[href]", 'input:not([type="hidden"])', "select", "textarea", "[tabindex]"].map((e3) => `${e3}:not([tabindex="-1"]):not([disabled])`).join(", ");
  return [...e2.querySelectorAll(t2)];
}
function Jd(e2, t2) {
  if (!(wd && typeof CSS < "u" && void 0 !== typeof CSS.supports && CSS.supports(`selector(${t2})`)))
    return null;
  try {
    return !!e2 && e2.matches(t2);
  } catch {
    return null;
  }
}
function Qd(e2) {
  return Array.isArray(e2) && 1 === e2.length && (e2 = e2[0]), e2 && e2.__v_isVNode && "symbol" != typeof e2.type;
}
function eu() {
}
function tu(e2, t2) {
  return (n2) => Object.keys(e2).reduce((o2, r2) => {
    const s2 = "object" != typeof e2[r2] || null == e2[r2] || Array.isArray(e2[r2]) ? { type: e2[r2] } : e2[r2];
    return o2[r2] = n2 && r2 in n2 ? { ...s2, default: n2[r2] } : s2, t2 && !o2[r2].source && (o2[r2].source = t2), o2;
  }, {});
}
function nu(e2, t2) {
  const n2 = Xs();
  if (!n2)
    throw new Error(`[CDS] ${e2} ${t2 || "must be called from inside a setup function"}`);
  return n2;
}
let ou = 0, ru = /* @__PURE__ */ new WeakMap();
function su() {
  const e2 = nu("getUid");
  if (ru.has(e2))
    return ru.get(e2);
  {
    const t2 = ou++;
    return ru.set(e2, t2), t2;
  }
}
function au(e2) {
  return function(e3) {
    return Mt(e3) ? vt(new Proxy({}, { get: (t2, n2, o2) => Lt(Reflect.get(e3.value, n2, o2)), set: (t2, n2, o2) => (Mt(e3.value[n2]) && !Mt(o2) ? e3.value[n2].value = o2 : e3.value[n2] = o2, true), deleteProperty: (t2, n2) => Reflect.deleteProperty(e3.value, n2), has: (t2, n2) => Reflect.has(e3.value, n2), ownKeys: () => Object.keys(e3.value), getOwnPropertyDescriptor: () => ({ enumerable: true, configurable: true }) })) : vt(e3);
  }(ga(e2));
}
function lu(e2, ...t2) {
  const n2 = t2.flat(), o2 = n2[0];
  return au(() => Object.fromEntries("function" == typeof o2 ? Object.entries($t(e2)).filter(([e3, t3]) => o2(function(e4) {
    return "function" == typeof e4 ? e4() : Lt(e4);
  }(t3), e3)) : n2.map((t3) => [t3, jt(e2, t3)])));
}
function iu(e2) {
  const t2 = Dt();
  return po(() => {
    t2.value = e2();
  }, { flush: "sync" }), t2;
}
su.reset = () => {
  ou = 0, ru = /* @__PURE__ */ new WeakMap();
};
const cu = "accept acceptcharset accesskey action allowfullscreen allowtransparency\nalt async autocomplete autofocus autoplay capture cellpadding cellspacing challenge\ncharset checked classid classname colspan cols content contenteditable contextmenu\ncontrols coords crossorigin data datetime default defer dir disabled download draggable\nenctype form formaction formenctype formmethod formnovalidate formtarget frameborder\nheaders height hidden high href hreflang htmlfor for httpequiv icon id inputmode integrity\nis keyparams keytype kind label lang list loop low manifest marginheight marginwidth max maxlength media\nmediagroup method min minlength multiple muted name novalidate nonce open\noptimum pattern placeholder poster preload radiogroup readonly rel required\nreversed role rowspan rows sandbox scope scoped scrolling seamless selected\nshape size sizes span spellcheck src srcdoc srclang srcset start step style\nsummary tabindex target title type usemap value width wmode wrap onCopy onCut onPaste onCompositionend onCompositionstart onCompositionupdate onKeydown\n    onKeypress onKeyup onFocus onBlur onChange onInput onSubmit onClick onContextmenu onDoubleclick onDblclick\n    onDrag onDragend onDragenter onDragexit onDragleave onDragover onDragstart onDrop onMousedown\n    onMouseenter onMouseleave onMousemove onMouseout onMouseover onMouseup onSelect onTouchcancel\n    onTouchend onTouchmove onTouchstart onTouchstartPassive onTouchmovePassive onScroll onWheel onAbort onCanplay onCanplaythrough\n    onDurationchange onEmptied onEncrypted onEnded onError onLoadeddata onLoadedmetadata\n    onLoadstart onPause onPlay onPlaying onProgress onRatechange onSeeked onSeeking onStalled onSuspend onTimeupdate onVolumechange onWaiting onLoad onError".split(/[\s\n]+/);
function du(e2, t2 = false) {
  let n2;
  n2 = false === t2 ? { aria: true, data: true, attr: true } : true === t2 ? { aria: true } : { ...t2 };
  const o2 = {};
  return Object.keys(e2).forEach((t3) => {
    (n2.aria && ("role" === t3 || uu(t3, "aria-")) || n2.data && uu(t3, "data-") || n2.attr && (cu.includes(t3) || cu.includes(t3.toLowerCase()))) && (o2[t3] = e2[t3]);
  }), o2;
}
function uu(e2, t2) {
  return 0 === e2.indexOf(t2);
}
function pu(e2 = [], t2 = true) {
  const n2 = Array.isArray(e2) ? e2 : [e2], o2 = [];
  return n2.forEach((e3) => {
    Array.isArray(e3) ? o2.push(...pu(e3, t2)) : e3 && e3.type === bs ? o2.push(...pu(e3.children, t2)) : e3 && As(e3) ? t2 && !hu(e3) ? o2.push(e3) : t2 || o2.push(e3) : function(e4) {
      return null != e4 && "" !== e4;
    }(e3) && o2.push(e3);
  }), o2;
}
function fu(e2 = []) {
  const t2 = [];
  return e2.forEach((e3) => {
    Array.isArray(e3) ? t2.push(...e3) : (null == e3 ? void 0 : e3.type) === bs ? t2.push(...fu(e3.children)) : t2.push(e3);
  }), t2.filter((e3) => !hu(e3));
}
function hu(e2) {
  if (wd)
    return e2 && (e2.type === Comment || e2.type === bs && 0 === e2.children.length || e2.type === Text && "" === e2.children.trim());
}
let vu = false;
try {
  const e2 = Object.defineProperty({}, "passive", { get() {
    vu = true;
  } });
  window.addEventListener("testPassive", null, e2), window.removeEventListener("testPassive", null, e2);
} catch {
}
function mu(e2, t2, n2, o2) {
  if (e2 && e2.addEventListener) {
    let r2 = o2;
    void 0 === r2 && vu && ("touchstart" === t2 || "touchmove" === t2 || "wheel" === t2) && (r2 = { passive: false }), e2.addEventListener(t2, n2, r2);
  }
  return { remove: () => {
    e2 && e2.removeEventListener && e2.removeEventListener(t2, n2);
  } };
}
function gu(e2) {
  return e2.toLowerCase();
}
const yu = [/([a-z0-9])([A-Z])/g, /([A-Z])([A-Z][a-z])/g], bu = /[^A-Z0-9]+/gi;
function wu(e2, t2 = {}) {
  const { splitRegexp: n2 = yu, stripRegexp: o2 = bu, transform: r2 = gu, delimiter: s2 = " " } = t2, a2 = _u(_u(e2, n2, "$1\0$2"), o2, "\0");
  let l2 = 0, i2 = a2.length;
  for (; "\0" === a2.charAt(l2); )
    l2++;
  for (; "\0" === a2.charAt(i2 - 1); )
    i2--;
  return a2.slice(l2, i2).split("\0").map(r2).join(s2);
}
function _u(e2, t2, n2) {
  return t2 instanceof RegExp ? e2.replace(t2, n2) : t2.reduce((e3, t3) => e3.replace(t3, n2), e2);
}
function Cu(e2, t2) {
  const n2 = e2.charAt(0), o2 = e2.substr(1).toLowerCase();
  return t2 > 0 && n2 >= "0" && n2 <= "9" ? `_${n2}${o2}` : `${n2.toUpperCase()}${o2}`;
}
function xu(e2, t2 = {}) {
  return wu(e2, { delimiter: "", transform: Cu, ...t2 });
}
function ku(e2, t2 = {}) {
  return wu(e2, { delimiter: "-", ...t2 });
}
const Su = { notification: { closeLabel: "\u0417\u0430\u043A\u0440\u044B\u0442\u044C" }, table: { filterTitle: "\u0424\u0438\u043B\u044C\u0442\u0440 \u043C\u0435\u043D\u044E", filterConfirm: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C", filterReset: "\u0421\u0431\u0440\u043E\u0441", filterEmptyText: "\u041D\u0435\u0442 \u0444\u0438\u043B\u044C\u0442\u0440\u043E\u0432", filterCheckAll: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435 \u044D\u043B\u0435\u043C\u0435\u043D\u0442\u044B", filterSearchPlaceholder: "\u041F\u043E\u0438\u0441\u043A \u0444\u0438\u043B\u044C\u0442\u0440\u043E\u0432", emptyText: "\u041D\u0435\u0442 \u0434\u0430\u043D\u043D\u044B\u0445", selectAll: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0442\u0435\u043A\u0443\u0449\u0443\u044E \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u0443", selectInvert: "\u0418\u043D\u0432\u0435\u0440\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C \u0442\u0435\u043A\u0443\u0449\u0443\u044E \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u0443", selectNone: "\u041E\u0447\u0438\u0441\u0442\u0438\u0442\u044C \u0432\u0441\u0435 \u0434\u0430\u043D\u043D\u044B\u0435", selectionAll: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435 \u0434\u0430\u043D\u043D\u044B\u0435", sortTitle: "\u0421\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u043A\u0430", expand: "\u0420\u0430\u0441\u043A\u0440\u044B\u0442\u044C \u0441\u0442\u0440\u043E\u043A\u0443", collapse: "\u0421\u0432\u0435\u0440\u043D\u0443\u0442\u044C \u0441\u0442\u0440\u043E\u043A\u0443", triggerDesc: "\u041E\u0442\u0441\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C \u043F\u043E \u0443\u0431\u044B\u0432\u0430\u043D\u0438\u044E", triggerAsc: "\u041E\u0442\u0441\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C \u043F\u043E \u0432\u043E\u0437\u0440\u0430\u0441\u0442\u0430\u043D\u0438\u044E", cancelSort: "\u041E\u0442\u043C\u0435\u043D\u0438\u0442\u044C \u0441\u043E\u0440\u0442\u0438\u0440\u043E\u0432\u043A\u0443" }, date: { days: "\u0412\u043E\u0441\u043A\u0440\u0435\u0441\u0435\u043D\u044C\u0435_\u041F\u043E\u043D\u0435\u0434\u0435\u043B\u044C\u043D\u0438\u043A_\u0412\u0442\u043E\u0440\u043D\u0438\u043A_\u0421\u0440\u0435\u0434\u0430_\u0427\u0435\u0442\u0432\u0435\u0440\u0433_\u041F\u044F\u0442\u043D\u0438\u0446\u0430_\u0421\u0443\u0431\u0431\u043E\u0442\u0430".split("_"), daysShort: "\u0412\u0441_\u041F\u043D_\u0412\u0442_\u0421\u0440_\u0427\u0442_\u041F\u0442_\u0421\u0431".split("_"), months: "\u042F\u043D\u0432\u0430\u0440\u044C_\u0424\u0435\u0432\u0440\u0430\u043B\u044C_\u041C\u0430\u0440\u0442_\u0410\u043F\u0440\u0435\u043B\u044C_\u041C\u0430\u0439_\u0418\u044E\u043D\u044C_\u0418\u044E\u043B\u044C_\u0410\u0432\u0433\u0443\u0441\u0442_\u0421\u0435\u043D\u0442\u044F\u0431\u0440\u044C_\u041E\u043A\u0442\u044F\u0431\u0440\u044C_\u041D\u043E\u044F\u0431\u0440\u044C_\u0414\u0435\u043A\u0430\u0431\u0440\u044C".split("_"), monthsLong: "\u042F\u043D\u0432\u0430\u0440\u044F_\u0424\u0435\u0432\u0440\u0430\u043B\u044F_\u041C\u0430\u0440\u0442\u0430_\u0410\u043F\u0440\u0435\u043B\u044F_\u041C\u0430\u044F_\u0418\u044E\u043D\u044F_\u0418\u044E\u043B\u044F_\u0410\u0432\u0433\u0443\u0441\u0442\u0430_\u0421\u0435\u043D\u0442\u044F\u0431\u0440\u044F_\u041E\u043A\u0442\u044F\u0431\u0440\u044F_\u041D\u043E\u044F\u0431\u0440\u044F_\u0414\u0435\u043A\u0430\u0431\u0440\u044F".split("_"), monthsShort: "\u042F\u043D\u0432_\u0424\u0435\u0432\u0440_\u041C\u0430\u0440\u0442_\u0410\u043F\u0440_\u041C\u0430\u0439_\u0418\u044E\u043D\u044C_\u0418\u044E\u043B\u044C_\u0410\u0432\u0433_\u0421\u0435\u043D\u0442_\u041E\u043A\u0442_\u041D\u043E\u044F\u0431_\u0414\u0435\u043A".split("_"), firstDayOfWeek: 1, format24h: true, pluralDay: (e2) => function(e3, t2) {
  return t2[e3 % 10 == 1 && e3 % 100 != 11 ? 0 : e3 % 10 >= 2 && e3 % 10 <= 4 && (e3 % 100 < 10 || e3 % 100 >= 20) ? 1 : 2];
}(e2, ["\u0434\u0435\u043D\u044C", "\u0434\u043D\u044F", "\u0434\u043D\u0435\u0439"]) }, container: { prev: "\u041F\u0440\u0435\u0434\u044B\u0434\u0443\u0449\u0438\u0439", next: "\u0421\u043B\u0435\u0434\u0443\u044E\u0449\u0438\u0439" }, uploader: { uploading: "\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430", uploadFile: "\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C \u0444\u0430\u0439\u043B", downloadFile: "\u0421\u043A\u0430\u0447\u0430\u0442\u044C \u0444\u0430\u0439\u043B", removeFile: "\u0423\u0434\u0430\u043B\u0438\u0442\u044C \u0444\u0430\u0439\u043B", uploadError: "\u041E\u0448\u0438\u0431\u043A\u0430 \u0437\u0430\u0433\u0440\u0443\u0437\u043A\u0438", previewFile: "\u041F\u0440\u0435\u0434\u043F\u0440\u043E\u0441\u043C\u043E\u0442\u0440 \u0444\u0430\u0439\u043B\u0430" }, dropdown: { noData: "\u041D\u0435\u0442 \u0434\u0430\u043D\u043D\u044B\u0445", selectAll: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435" } }, Eu = 864e5, Iu = 36e5, Ou = 6e4, Nu = "YYYY-MM-DDTHH:mm:ss.SSSZ", Mu = /\[((?:[^\]\\]|\\]|\\)*)\]|d{1,4}|M{1,4}|m{1,2}|w{1,2}|Qo|Do|D{1,4}|YY(?:YY)?|H{1,2}|h{1,2}|s{1,2}|S{1,3}|Z{1,2}|a{1,2}|[AQExX]/g, Au = /(\[[^\]]*\])|d{1,4}|M{1,4}|m{1,2}|w{1,2}|Qo|Do|D{1,4}|YY(?:YY)?|H{1,2}|h{1,2}|s{1,2}|S{1,3}|Z{1,2}|a{1,2}|[AQExX]|([.*+:?^,\s${}()|\\]+)/g, Du = {}, Vu = { YY(e2, t2, n2) {
  const o2 = this.YYYY(e2, t2, n2) % 100;
  return o2 >= 0 ? Wd(o2) : "-" + Wd(Math.abs(o2));
}, YYYY: (e2, t2, n2) => null != n2 ? n2 : e2.getFullYear(), M: (e2) => e2.getMonth() + 1, MM: (e2) => Wd(e2.getMonth() + 1), MMM: (e2, t2) => t2.monthsShort[e2.getMonth()], MMMM: (e2, t2) => t2.months[e2.getMonth()], Q: (e2) => Math.ceil((e2.getMonth() + 1) / 3), Qo(e2) {
  return Ku(this.Q(e2));
}, D: (e2) => e2.getDate(), Do: (e2) => Ku(e2.getDate()), DD: (e2) => Wd(e2.getDate()), DDD: (e2) => Fu(e2), DDDD: (e2) => Wd(Fu(e2), 3), d: (e2) => e2.getDay(), dd(e2, t2) {
  return this.dddd(e2, t2).slice(0, 2);
}, ddd: (e2, t2) => t2.daysShort[e2.getDay()], dddd: (e2, t2) => t2.days[e2.getDay()], E: (e2) => e2.getDay() || 7, w: (e2) => Pu(e2), ww: (e2) => Wd(Pu(e2)), H: (e2) => e2.getHours(), HH: (e2) => Wd(e2.getHours()), h(e2) {
  const t2 = e2.getHours();
  return 0 === t2 ? 12 : t2 > 12 ? t2 % 12 : t2;
}, hh(e2) {
  return Wd(this.h(e2));
}, m: (e2) => e2.getMinutes(), mm: (e2) => Wd(e2.getMinutes()), s: (e2) => e2.getSeconds(), ss: (e2) => Wd(e2.getSeconds()), S: (e2) => Math.floor(e2.getMilliseconds() / 100), SS: (e2) => Wd(Math.floor(e2.getMilliseconds() / 10)), SSS: (e2) => Wd(e2.getMilliseconds(), 3), A(e2) {
  return this.H(e2) < 12 ? "AM" : "PM";
}, a(e2) {
  return this.H(e2) < 12 ? "am" : "pm";
}, aa(e2) {
  return this.H(e2) < 12 ? "a.m." : "p.m.";
}, Z: (e2, t2, n2, o2) => Hu(null == o2 ? e2.getTimezoneOffset() : o2, ":"), ZZ: (e2, t2, n2, o2) => Hu(null == o2 ? e2.getTimezoneOffset() : o2), X: (e2) => Math.floor(e2.getTime() / 1e3), x: (e2) => e2.getTime() };
function Tu(e2, t2, n2, o2, r2) {
  if (0 !== e2 && !e2 || e2 === 1 / 0 || e2 === -1 / 0)
    return;
  const s2 = new Date(e2);
  if (isNaN(s2))
    return;
  void 0 === t2 && (t2 = Nu);
  const a2 = Bu(n2, Su);
  return t2.replace(Mu, (e3, t3) => e3 in Vu ? Vu[e3](s2, a2, o2, r2) : void 0 === t3 ? e3 : t3.split("\\]").join("]"));
}
function Lu(e2, t2, n2, o2) {
  const r2 = { year: null, month: null, day: null, hour: null, minute: null, second: null, millisecond: null, timezoneOffset: null, dateHash: null, timeHash: null };
  if (void 0 !== o2 && Object.assign(r2, o2), null == e2 || "" === e2 || "string" != typeof e2)
    return r2;
  t2 || (t2 = Nu);
  const s2 = Bu(n2, Su), a2 = s2.months, l2 = s2.monthsShort, { regex: i2, map: c2 } = function(e3, t3) {
    const n3 = "(" + t3.days.join("|") + ")", o3 = e3 + n3;
    if (Du[o3])
      return Du[o3];
    const r3 = "(" + t3.daysShort.join("|") + ")", s3 = "(" + t3.months.join("|") + ")", a3 = "(" + t3.monthsShort.join("|") + ")", l3 = {};
    let i3 = 0;
    const c3 = e3.replace(Au, (e4) => {
      switch (i3++, e4) {
        case "YY":
          return l3.YY = i3, "(-?\\d{1,2})";
        case "YYYY":
          return l3.YYYY = i3, "(-?\\d{1,4})";
        case "M":
          return l3.M = i3, "(\\d{1,2})";
        case "MM":
          return l3.M = i3, "(\\d{2})";
        case "MMM":
          return l3.MMM = i3, a3;
        case "MMMM":
          return l3.MMMM = i3, s3;
        case "D":
          return l3.D = i3, "(\\d{1,2})";
        case "Do":
          return l3.D = i3++, "(\\d{1,2}(st|nd|rd|th))";
        case "DD":
          return l3.D = i3, "(\\d{2})";
        case "H":
          return l3.H = i3, "(\\d{1,2})";
        case "HH":
          return l3.H = i3, "(\\d{2})";
        case "h":
          return l3.h = i3, "(\\d{1,2})";
        case "hh":
          return l3.h = i3, "(\\d{2})";
        case "m":
          return l3.m = i3, "(\\d{1,2})";
        case "mm":
          return l3.m = i3, "(\\d{2})";
        case "s":
          return l3.s = i3, "(\\d{1,2})";
        case "ss":
          return l3.s = i3, "(\\d{2})";
        case "S":
          return l3.S = i3, "(\\d{1})";
        case "SS":
          return l3.S = i3, "(\\d{2})";
        case "SSS":
          return l3.S = i3, "(\\d{3})";
        case "A":
          return l3.A = i3, "(AM|PM)";
        case "a":
          return l3.a = i3, "(am|pm)";
        case "aa":
          return l3.aa = i3, "(a\\.m\\.|p\\.m\\.)";
        case "ddd":
          return r3;
        case "dddd":
          return n3;
        case "Q":
        case "d":
        case "E":
          return "(\\d{1})";
        case "Qo":
          return "(1st|2nd|3rd|4th)";
        case "DDD":
        case "DDDD":
          return "(\\d{1,3})";
        case "w":
          return "(\\d{1,2})";
        case "ww":
          return "(\\d{2})";
        case "Z":
          return l3.Z = i3, "(Z|[+-]\\d{2}:\\d{2})";
        case "ZZ":
          return l3.ZZ = i3, "(Z|[+-]\\d{2}\\d{2})";
        case "X":
          return l3.X = i3, "(-?\\d+)";
        case "x":
          return l3.x = i3, "(-?\\d{4,})";
        default:
          return i3--, "[" === e4[0] && (e4 = e4.substring(1, e4.length - 1)), e4.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
      }
    }), d3 = { map: l3, regex: new RegExp("^" + c3) };
    return Du[o3] = d3, d3;
  }(t2, s2), d2 = e2.match(i2);
  if (null === d2)
    return r2;
  let u2 = "";
  if (c2.X || c2.x) {
    const e3 = parseInt(d2[c2.X ? c2.X : c2.x], 10);
    if (isNaN(e3) || e3 < 0)
      return r2;
    const t3 = new Date(e3 * (c2.X ? 1e3 : 0));
    r2.year = t3.getFullYear(), r2.month = t3.getMonth() + 1, r2.date = t3.getDate(), r2.hour = t3.getHours(), r2.minute = t3.getMinutes(), r2.second = t3.getSeconds(), r2.millisecond = t3.getMilliseconds();
  } else {
    if (c2.YYYY)
      r2.year = parseInt(d2[c2.YYYY], 10);
    else if (c2.YY) {
      const e3 = parseInt(d2[c2.YY], 10);
      r2.year = e3 < 0 ? e3 : 2e3 + e3;
    }
    if (c2.M) {
      if (r2.month = parseInt(d2[c2.M], 10), r2.month < 1 || r2.month > 12)
        return r2;
    } else
      c2.MMM ? r2.month = l2.indexOf(d2[c2.MMM]) + 1 : c2.MMMM && (r2.month = a2.indexOf(d2[c2.MMMM]) + 1);
    if (c2.D) {
      if (r2.day = parseInt(d2[c2.D], 10), null === r2.year || null === r2.month || r2.day < 1)
        return r2;
      const e3 = new Date(r2.year, r2.month, 0).getDate();
      if (r2.day > e3)
        return r2;
    }
    c2.H ? r2.hour = parseInt(d2[c2.H], 10) % 24 : c2.h && (r2.hour = parseInt(d2[c2.h], 10) % 12, (c2.A && "PM" === d2[c2.A] || c2.a && "pm" === d2[c2.a] || c2.aa && "p.m." === d2[c2.aa]) && (r2.hour += 12), r2.hour = r2.hour % 24), c2.m && (r2.minute = parseInt(d2[c2.m], 10) % 60), c2.s && (r2.second = parseInt(d2[c2.s], 10) % 60), c2.S && (r2.millisecond = parseInt(d2[c2.S], 10) * 10 ** (3 - d2[c2.S].length)), (c2.Z || c2.ZZ) && (u2 = c2.Z ? d2[c2.Z].replace(":", "") : d2[c2.ZZ], r2.timezoneOffset = ("+" === u2[0] ? -1 : 1) * (60 * +u2.slice(1, 3) + +u2.slice(3, 5)));
  }
  return r2.dateHash = Wd(r2.year, 6) + "/" + Wd(r2.month) + "/" + Wd(r2.day), r2.timeHash = Wd(r2.hour) + ":" + Wd(r2.minute) + ":" + Wd(r2.second) + u2, r2;
}
function Pu(e2) {
  const t2 = new Date(e2.getFullYear(), e2.getMonth(), e2.getDate());
  t2.setDate(t2.getDate() - (t2.getDay() + 6) % 7 + 3);
  const n2 = new Date(t2.getFullYear(), 0, 4);
  n2.setDate(n2.getDate() - (n2.getDay() + 6) % 7 + 3);
  const o2 = t2.getTimezoneOffset() - n2.getTimezoneOffset();
  t2.setHours(t2.getHours() - o2);
  const r2 = (+t2 - +n2) / (7 * Eu);
  return 1 + Math.floor(r2);
}
function Fu(e2) {
  const t2 = $u(e2, Ru(e2, "year"), "days");
  return t2 ? t2 + 1 : void 0;
}
function Ru(e2, t2, n2) {
  const o2 = new Date(e2), r2 = "set" + (n2 ? "UTC" : "");
  switch (t2) {
    case "year":
    case "years":
      o2[`${r2}Month`](0);
    case "month":
    case "months":
      o2[`${r2}Date`](1);
    case "day":
    case "days":
    case "date":
      o2[`${r2}Hours`](0);
    case "hour":
    case "hours":
      o2[`${r2}Minutes`](0);
    case "minute":
    case "minutes":
      o2[`${r2}Seconds`](0);
    case "second":
    case "seconds":
      o2[`${r2}Milliseconds`](0);
  }
  return o2;
}
function $u(e2, t2, n2 = "days") {
  const o2 = new Date(e2), r2 = new Date(t2);
  switch (n2) {
    case "years":
    case "year":
      return o2.getFullYear() - r2.getFullYear();
    case "months":
    case "month":
      return 12 * (o2.getFullYear() - r2.getFullYear()) + o2.getMonth() - r2.getMonth();
    case "days":
    case "day":
    case "date":
      return ju(Ru(o2, "day"), Ru(r2, "day"), Eu);
    case "hours":
    case "hour":
      return ju(Ru(o2, "hour"), Ru(r2, "hour"), Iu);
    case "minutes":
    case "minute":
      return ju(Ru(o2, "minute"), Ru(r2, "minute"), Ou);
    case "seconds":
    case "second":
      return ju(Ru(o2, "second"), Ru(r2, "second"), 1e3);
    default:
      return 0;
  }
}
function Bu(e2, t2) {
  return e2 || (t2 ? t2.date : Su.date);
}
function Hu(e2, t2 = "") {
  const n2 = e2 > 0 ? "-" : "+", o2 = Math.abs(e2), r2 = o2 % 60;
  return n2 + Wd(Math.floor(o2 / 60)) + t2 + Wd(r2);
}
function ju(e2, t2, n2) {
  return (e2.getTime() - e2.getTimezoneOffset() * Ou - (t2.getTime() - t2.getTimezoneOffset() * Ou)) / n2;
}
function Ku(e2) {
  if (e2 >= 11 && e2 <= 13)
    return `${e2}th`;
  switch (e2 % 10) {
    case 1:
      return `${e2}st`;
    case 2:
      return `${e2}nd`;
    case 3:
      return `${e2}rd`;
  }
  return `${e2}th`;
}
function zu(e2) {
  if ("function" != typeof e2.getRootNode) {
    for (; e2.parentNode; )
      e2 = e2.parentNode;
    return e2 !== document ? null : document;
  }
  const t2 = e2.getRootNode();
  return t2 !== document && t2.getRootNode({ composed: true }) !== document ? null : t2;
}
function Zu(e2, t2) {
  return Array.isArray(e2) || void 0 === t2 ? Wu(e2) : function(e3, t3) {
    if (!Wu(e3))
      return false;
    const n2 = document.createElement("div"), o2 = n2.style[e3];
    return n2.style[e3] = t3, n2.style[e3] !== o2;
  }(e2, t2);
}
function Uu(e2 = "", t2) {
  const n2 = {}, o2 = /:(.+)/;
  return "object" == typeof e2 ? e2 : (e2.split(/;(?![^(]*\))/g).forEach(function(e3) {
    if (e3) {
      const r2 = e3.split(o2);
      if (r2.length > 1) {
        const e4 = t2 ? N(r2[0].trim()) : r2[0].trim();
        n2[e4] = r2[1].trim();
      }
    }
  }), n2);
}
function Wu(e2) {
  if (wd && window.document.documentElement) {
    const t2 = Array.isArray(e2) ? e2 : [e2], { documentElement: n2 } = window.document;
    return t2.some((e3) => e3 in n2.style);
  }
  return false;
}
const Yu = wd ? [null, document, document.body, document.scrollingElement, document.documentElement] : [];
function Gu(e2, t2) {
  const n2 = [];
  if (t2 && e2 && !t2.contains(e2))
    return n2;
  for (; e2 && (Xu(e2) && n2.push(e2), e2 !== t2); )
    e2 = e2.parentElement;
  return n2;
}
function qu(e2, t2) {
  let n2 = function(e3) {
    if (null == e3)
      return;
    if ("string" == typeof e3)
      try {
        return document.querySelector(e3) || void 0;
      } catch {
        return;
      }
    const t3 = Lt(e3);
    return t3 ? t3.$el || t3 : void 0;
  }(t2);
  if (void 0 === n2) {
    if (null == e2)
      return window;
    n2 = e2.closest(".cds-scrollable");
  }
  return Yu.includes(n2) ? window : n2;
}
function Xu(e2) {
  if (!e2 || e2.nodeType !== Node.ELEMENT_NODE)
    return false;
  const t2 = window.getComputedStyle(e2);
  return "scroll" === t2.overflowY || "auto" === t2.overflowY && e2.scrollHeight > e2.clientHeight;
}
let Ju;
function Qu(e2) {
  if (!wd)
    return 0;
  if (e2 || void 0 === Ju) {
    const e3 = document.createElement("div");
    e3.style.width = "100%", e3.style.height = "200px";
    const t2 = document.createElement("div"), n2 = t2.style;
    n2.position = "absolute", n2.top = "0", n2.left = "0", n2.pointerEvents = "none", n2.visibility = "hidden", n2.width = "200px", n2.height = "150px", n2.overflow = "hidden", t2.appendChild(e3), document.body.appendChild(t2);
    const o2 = e3.offsetWidth;
    t2.style.overflow = "scroll";
    let r2 = e3.offsetWidth;
    o2 === r2 && (r2 = t2.clientWidth), document.body.removeChild(t2), Ju = o2 - r2;
  }
  return Ju;
}
function ep(e2) {
  const t2 = e2.getBoundingClientRect(), n2 = document.documentElement;
  return { left: t2.left + (window.pageXOffset || n2.scrollLeft) - (n2.clientLeft || document.body.clientLeft || 0), top: t2.top + (window.pageYOffset || n2.scrollTop) - (n2.clientTop || document.body.clientTop || 0) };
}
function tp(e2) {
  const t2 = e2.match(/^(.*)px$/), n2 = Number(null == t2 ? void 0 : t2[1]);
  return Number.isNaN(n2) ? Qu() : n2;
}
const np = ["top", "bottom"], op = ["start", "end", "left", "right"];
function rp(e2) {
  let [t2, n2] = e2.split(" ");
  return n2 || (n2 = Rd(np, t2) ? "start" : Rd(op, t2) ? "top" : "center"), { side: sp(t2), align: sp(n2) };
}
function sp(e2) {
  return "start" === e2 ? "left" : "end" === e2 ? "right" : e2;
}
function ap(e2) {
  return { side: { center: "center", top: "bottom", bottom: "top", left: "right", right: "left" }[e2.side], align: e2.align };
}
function lp(e2) {
  return { side: e2.side, align: { center: "center", top: "bottom", bottom: "top", left: "right", right: "left" }[e2.align] };
}
function ip(e2) {
  return { side: e2.align, align: e2.side };
}
function cp(e2) {
  return Rd(np, e2.side) ? "y" : "x";
}
class dp {
  constructor({ x: e2, y: t2, width: n2, height: o2 }) {
    bd(this, "x"), bd(this, "y"), bd(this, "width"), bd(this, "height"), this.x = e2, this.y = t2, this.width = n2, this.height = o2;
  }
  get top() {
    return this.y;
  }
  get bottom() {
    return this.y + this.height;
  }
  get left() {
    return this.x;
  }
  get right() {
    return this.x + this.width;
  }
}
function up(e2, t2) {
  return { x: { before: Math.max(0, t2.left - e2.left), after: Math.max(0, e2.right - t2.right) }, y: { before: Math.max(0, t2.top - e2.top), after: Math.max(0, e2.bottom - t2.bottom) } };
}
function pp(e2) {
  const t2 = e2.getBoundingClientRect(), n2 = getComputedStyle(e2), o2 = n2.transform;
  if (o2) {
    let r2, s2, a2, l2, i2;
    if (o2.startsWith("matrix3d("))
      r2 = o2.slice(9, -1).split(/, /), s2 = +r2[0], a2 = +r2[5], l2 = +r2[12], i2 = +r2[13];
    else {
      if (!o2.startsWith("matrix("))
        return new dp(t2);
      r2 = o2.slice(7, -1).split(/, /), s2 = +r2[0], a2 = +r2[3], l2 = +r2[4], i2 = +r2[5];
    }
    const c2 = n2.transformOrigin, d2 = t2.x - l2 - (1 - s2) * parseFloat(c2), u2 = t2.y - i2 - (1 - a2) * parseFloat(c2.slice(c2.indexOf(" ") + 1)), p2 = s2 ? t2.width / s2 : e2.offsetWidth + 1, f2 = a2 ? t2.height / a2 : e2.offsetHeight + 1;
    return new dp({ x: d2, y: u2, width: p2, height: f2 });
  }
  return new dp(t2);
}
function fp(e2, t2, n2) {
  if (typeof e2.animate > "u")
    return { finished: Promise.resolve() };
  let o2;
  try {
    o2 = e2.animate(t2, n2);
  } catch {
    return { finished: Promise.resolve() };
  }
  return typeof o2.finished > "u" && (o2.finished = new Promise((e3) => {
    o2.onfinish = () => {
      e3(o2);
    };
  })), o2;
}
let hp = (e2) => setTimeout(e2, 16), vp = (e2) => clearTimeout(e2);
wd && "requestAnimationFrame" in window && (hp = (e2) => window.requestAnimationFrame(e2), vp = (e2) => window.cancelAnimationFrame(e2));
let mp = 0;
const gp = /* @__PURE__ */ new Map();
function yp(e2, t2 = 1) {
  mp += 1;
  const n2 = mp;
  return function t3(o2) {
    if (0 === o2)
      bp(n2), e2();
    else {
      const e3 = hp(() => {
        t3(o2 - 1);
      });
      gp.set(n2, e3);
    }
  }(t2), n2;
}
function bp(e2) {
  gp.delete(e2);
}
yp.cancel = (e2) => {
  const t2 = gp.get(e2);
  return bp(t2), vp(t2);
};
const wp = 450;
function _p(e2, t2 = {}) {
  const { getContainer: n2 = () => window, callback: o2, duration: r2 = wp } = t2, s2 = n2(), a2 = function(e3, t3) {
    var n3;
    if (!wd)
      return 0;
    const o3 = t3 ? "scrollTop" : "scrollLeft";
    let r3 = 0;
    return Ad(e3) ? r3 = e3[t3 ? "pageYOffset" : "pageXOffset"] : e3 instanceof Document ? r3 = e3.documentElement[o3] : e3 && (r3 = e3[o3]), e3 && !Ad(e3) && "number" != typeof r3 && (r3 = null == (n3 = (e3.ownerDocument || e3).documentElement) ? void 0 : n3[o3]), r3;
  }(s2, true), l2 = Date.now(), i2 = () => {
    const t3 = Date.now() - l2, n3 = function(e3, t4, n4, o3) {
      const r3 = n4 - t4;
      return e3 /= o3 / 2, e3 < 1 ? r3 / 2 * e3 * e3 * e3 + t4 : r3 / 2 * ((e3 -= 2) * e3 * e3 + 2) + t4;
    }(t3 > r2 ? r2 : t3, a2, e2, r2);
    Ad(s2) ? s2.scrollTo(window.pageXOffset, n3) : s2 instanceof HTMLDocument || "HTMLDocument" === s2.constructor.name ? s2.documentElement.scrollTop = n3 : s2.scrollTop = n3, t3 < r2 ? yp(i2) : "function" == typeof o2 && o2();
  };
  yp(i2);
}
function Cp(...e2) {
  const t2 = [];
  for (let n2 = 0; n2 < e2.length; n2++) {
    const o2 = e2[n2];
    if (o2) {
      if (Od(o2))
        t2.push(o2);
      else if (Nd(o2))
        for (let e3 = 0; e3 < o2.length; e3++) {
          const n3 = Cp(o2[e3]);
          n3 && t2.push(n3);
        }
      else if (Md(o2))
        for (const e3 in o2)
          o2[e3] && t2.push(e3);
    }
  }
  return t2.join(" ");
}
function xp(e2, t2) {
  return e2.classList ? e2.classList.contains(t2) : ` ${e2.className} `.indexOf(` ${t2} `) > -1;
}
function kp(e2, t2) {
  e2.classList ? e2.classList.add(t2) : xp(e2, t2) || (e2.className = `${e2.className} ${t2}`);
}
function Sp(e2, t2) {
  if (e2.classList)
    e2.classList.remove(t2);
  else if (xp(e2, t2)) {
    const n2 = e2.className;
    e2.className = ` ${n2} `.replace(` ${t2} `, " ");
  }
}
function Ep(e2, t2) {
  return void 0 !== e2 && e2() || t2;
}
function Ip(e2, t2) {
  return void 0 !== e2 ? t2.concat(e2()) : t2;
}
const Op = /* @__PURE__ */ new WeakMap();
const Np = 2.4, Mp = 0.2126729, Ap = 0.7151522, Dp = 0.072175, Vp = 0.55, Tp = 0.58, Lp = 0.57, Pp = 0.62, Fp = 0.03, Rp = 1.45, $p = 5e-4, Bp = 1.25, Hp = 1.25, jp = 0.078, Kp = 12.82051282051282, zp = 0.06, Zp = 1e-3;
function Up(e2, t2) {
  const n2 = (e2.r / 255) ** Np, o2 = (e2.g / 255) ** Np, r2 = (e2.b / 255) ** Np, s2 = (t2.r / 255) ** Np, a2 = (t2.g / 255) ** Np, l2 = (t2.b / 255) ** Np;
  let i2, c2 = n2 * Mp + o2 * Ap + r2 * Dp, d2 = s2 * Mp + a2 * Ap + l2 * Dp;
  if (c2 <= Fp && (c2 += (Fp - c2) ** Rp), d2 <= Fp && (d2 += (Fp - d2) ** Rp), Math.abs(d2 - c2) < $p)
    return 0;
  if (d2 > c2) {
    const e3 = (d2 ** Vp - c2 ** Tp) * Bp;
    i2 = e3 < Zp ? 0 : e3 < jp ? e3 - e3 * Kp * zp : e3 - zp;
  } else {
    const e3 = (d2 ** Pp - c2 ** Lp) * Hp;
    i2 = e3 > -Zp ? 0 : e3 > -jp ? e3 - e3 * Kp * zp : e3 + zp;
  }
  return 100 * i2;
}
const Wp = 0.20689655172413793, Yp = (e2) => e2 > Wp ** 3 ? Math.cbrt(e2) : e2 / (3 * Wp ** 2) + 4 / 29, Gp = (e2) => e2 > Wp ? e2 ** 3 : 3 * Wp ** 2 * (e2 - 4 / 29);
function qp(e2) {
  const t2 = Yp, n2 = t2(e2[1]);
  return [116 * n2 - 16, 500 * (t2(e2[0] / 0.95047) - n2), 200 * (n2 - t2(e2[2] / 1.08883))];
}
function Xp(e2) {
  const t2 = Gp, n2 = (e2[0] + 16) / 116;
  return [0.95047 * t2(n2 + e2[1] / 500), t2(n2), 1.08883 * t2(n2 - e2[2] / 200)];
}
const Jp = [[3.2406, -1.5372, -0.4986], [-0.9689, 1.8758, 0.0415], [0.0557, -0.204, 1.057]], Qp = (e2) => e2 <= 31308e-7 ? 12.92 * e2 : 1.055 * e2 ** (1 / 2.4) - 0.055, ef = [[0.4124, 0.3576, 0.1805], [0.2126, 0.7152, 0.0722], [0.0193, 0.1192, 0.9505]], tf = (e2) => e2 <= 0.04045 ? e2 / 12.92 : ((e2 + 0.055) / 1.055) ** 2.4;
function nf(e2) {
  const t2 = Array(3), n2 = Qp, o2 = Jp;
  for (let r2 = 0; r2 < 3; ++r2)
    t2[r2] = Math.round(255 * jd(n2(o2[r2][0] * e2[0] + o2[r2][1] * e2[1] + o2[r2][2] * e2[2])));
  return { r: t2[0], g: t2[1], b: t2[2] };
}
function of({ r: e2, g: t2, b: n2 }) {
  const o2 = [0, 0, 0], r2 = tf, s2 = ef;
  e2 = r2(e2 / 255), t2 = r2(t2 / 255), n2 = r2(n2 / 255);
  for (let r3 = 0; r3 < 3; ++r3)
    o2[r3] = s2[r3][0] * e2 + s2[r3][1] * t2 + s2[r3][2] * n2;
  return o2;
}
const rf = /^(?<fn>(?:rgb|hsl)a?)\((?<values>.+)\)/, sf = { rgb: (e2, t2, n2, o2) => ({ r: e2, g: t2, b: n2, a: o2 }), rgba: (e2, t2, n2, o2) => ({ r: e2, g: t2, b: n2, a: o2 }), hsl: (e2, t2, n2, o2) => ff({ h: e2, s: t2, l: n2, a: o2 }), hsla: (e2, t2, n2, o2) => ff({ h: e2, s: t2, l: n2, a: o2 }), hsv: (e2, t2, n2, o2) => pf({ h: e2, s: t2, v: n2, a: o2 }), hsva: (e2, t2, n2, o2) => pf({ h: e2, s: t2, v: n2, a: o2 }) };
function af(e2) {
  if ("number" == typeof e2)
    return { r: (16711680 & e2) >> 16, g: (65280 & e2) >> 8, b: 255 & e2 };
  if ("string" == typeof e2 && rf.test(e2)) {
    const { groups: t2 } = e2.match(rf), { fn: n2, values: o2 } = t2, r2 = o2.split(/, \s*/).map((e3) => e3.endsWith("%") && ["hsl", "hsla", "hsv", "hsva"].includes(n2) ? parseFloat(e3) / 100 : parseFloat(e3));
    return sf[n2](...r2);
  }
  if ("string" == typeof e2) {
    let t2 = e2.startsWith("#") ? e2.slice(1) : e2;
    return [3, 4].includes(t2.length) ? t2 = t2.split("").map((e3) => e3 + e3).join("") : [6, 8].includes(t2.length), function(e3) {
      e3 = function(e4) {
        return e4.startsWith("#") && (e4 = e4.slice(1)), e4 = e4.replace(/([^0-9a-f])/gi, "F"), (3 === e4.length || 4 === e4.length) && (e4 = e4.split("").map((e5) => e5 + e5).join("")), 6 !== e4.length && (e4 = Ud(Ud(e4, 6), 8, "F")), e4;
      }(e3);
      let [t3, n2, o2, r2] = function(e4, t4 = 1) {
        const n3 = [];
        let o3 = 0;
        for (; o3 < e4.length; )
          n3.push(e4.substr(o3, t4)), o3 += t4;
        return n3;
      }(e3, 2).map((e4) => parseInt(e4, 16));
      return r2 = void 0 === r2 ? r2 : r2 / 255, { r: t3, g: n2, b: o2, a: r2 };
    }(t2);
  }
  if ("object" == typeof e2) {
    if (Id(e2, ["r", "g", "b"]))
      return e2;
    if (Id(e2, ["h", "s", "l"]))
      return pf(uf(e2));
    if (Id(e2, ["h", "s", "v"]))
      return pf(e2);
  }
  throw new TypeError(`Invalid color: ${null == e2 ? e2 : String(e2) || e2.constructor.name}
Expected #hex, #hexa, rgb(), rgba(), hsl(), hsla(), object or number`);
}
function lf(e2, t2) {
  const n2 = qp(of(e2));
  return n2[0] = n2[0] + 10 * t2, nf(Xp(n2));
}
function cf(e2, t2) {
  const n2 = qp(of(e2));
  return n2[0] = n2[0] - 10 * t2, nf(Xp(n2));
}
function df({ r: e2, g: t2, b: n2, a: o2 }) {
  return `#${[mf(e2), mf(t2), mf(n2), void 0 !== o2 ? mf(Math.round(255 * o2)) : ""].join("")}`;
}
function uf(e2) {
  const { h: t2, s: n2, l: o2, a: r2 } = e2, s2 = o2 + n2 * Math.min(o2, 1 - o2);
  return { h: t2, s: 0 === s2 ? 0 : 2 - 2 * o2 / s2, v: s2, a: r2 };
}
function pf(e2) {
  const { h: t2, s: n2, v: o2, a: r2 } = e2, s2 = (e3) => {
    const r3 = (e3 + t2 / 60) % 6;
    return o2 - o2 * n2 * Math.max(Math.min(r3, 4 - r3, 1), 0);
  }, a2 = [s2(5), s2(3), s2(1)].map((e3) => Math.round(255 * e3));
  return { r: a2[0], g: a2[1], b: a2[2], a: r2 };
}
function ff(e2) {
  return pf(uf(e2));
}
function hf(e2) {
  return of(af(e2))[1];
}
function vf(e2) {
  const t2 = Math.abs(Up(af(0), af(e2)));
  return Math.abs(Up(af(16777215), af(e2))) > Math.min(t2, 50) ? "#fff" : "#000";
}
function mf(e2) {
  const t2 = Math.round(e2).toString(16);
  return ("00".substr(0, 2 - t2.length) + t2).toUpperCase();
}
const gf = Object.freeze({ lighten10: "#faecec", lighten50: "#fdcfcf", lighten100: "#ffb2b2", lighten200: "#ff6666", lighten300: "#f23838", lighten400: "#eb0000", lighten500: "#ca0000", lighten600: "#aa0000", lighten700: "#890000", lighten800: "#680000", lighten900: "#470000", darken10: "#1f0000", darken50: "#290000", darken100: "#370000", darken200: "#4F0000", darken300: "#680000", darken400: "#800000", darken500: "#980C0C", darken600: "#b13434", darken700: "#c96767", darken800: "#e2a4a4", darken900: "#f5e7e7" }), yf = Object.freeze({ lighten10: "#faf0ec", lighten50: "#f9ded2", lighten100: "#f7cbb8", lighten200: "#ec9f7f", lighten300: "#e1774a", lighten400: "#d6511a", lighten500: "#b93b06", lighten600: "#9d3104", lighten700: "#802802", lighten800: "#641e01", lighten900: "#471500", darken10: "#1f0900", darken50: "#290c00", darken100: "#371000", darken200: "#4F1800", darken300: "#681f00", darken400: "#802600", darken500: "#98360c", darken600: "#b15a34", darken700: "#c98467", darken800: "#e2b7a4", darken900: "#f5ebe7" }), bf = Object.freeze({ lighten10: "#fff7f1", lighten50: "#ffe8d7", lighten100: "#ffdec1", lighten200: "#ffbc89", lighten300: "#ff9b52", lighten400: "#f67b1d", lighten500: "#d35f06", lighten600: "#b04f04", lighten700: "#8d3e02", lighten800: "#6a2f01", lighten900: "#471f00", darken10: "#1f0d00", darken50: "#291100", darken100: "#371800", darken200: "#4F2200", darken300: "#682d00", darken400: "#803700", darken500: "#98490c", darken600: "#b16a34", darken700: "#c99267", darken800: "#e2bfa4", darken900: "#f5ede7" }), wf = Object.freeze({ lighten10: "#fffbf1", lighten50: "#fff1c3", lighten100: "#ffeb98", lighten200: "#ffd54b", lighten300: "#ffc000", lighten400: "#e0a900", lighten500: "#c29200", lighten600: "#a37b00", lighten700: "#856400", lighten800: "#664d00", lighten900: "#473600", darken10: "#1f1700", darken50: "#291e00", darken100: "#372900", darken200: "#4f3c00", darken300: "#684e00", darken400: "#806000", darken500: "#98750c", darken600: "#b19234", darken700: "#c9b167", darken800: "#e2d2a4", darken900: "#f5f1e7" }), _f = Object.freeze({ lighten10: "#fffef1", lighten50: "#fefcc9", lighten100: "#ffffa5", lighten200: "#fcf75f", lighten300: "#f6ef1d", lighten400: "#d9d200", lighten500: "#bcb600", lighten600: "#9f9900", lighten700: "#827d00", lighten800: "#656100", lighten900: "#474500", darken10: "#1f1e00", darken50: "#292700", darken100: "#373500", darken200: "#4f4d00", darken300: "#686400", darken400: "#807c00", darken500: "#98940c", darken600: "#b1ad34", darken700: "#c9c667", darken800: "#e2dfa4", darken900: "#f5f4e7" }), Cf = Object.freeze({ lighten10: "#fcfff1", lighten50: "#f2fec9", lighten100: "#ecffa5", lighten200: "#d7fc5f", lighten300: "#c3f61d", lighten400: "#a6d900", lighten500: "#90bc00", lighten600: "#7a9f00", lighten700: "#638200", lighten800: "#4d6500", lighten900: "#374700", darken10: "#1f1f00", darken50: "#1e2900", darken100: "#2a3700", darken200: "#3d4f00", darken300: "#506800", darken400: "#628000", darken500: "#78980c", darken600: "#94b134", darken700: "#b2c967", darken800: "#d3e2a4", darken900: "#f2f5e7" }), xf = Object.freeze({ lighten10: "#eefaec", lighten50: "#cbefc4", lighten100: "#aae59f", lighten200: "#72d161", lighten300: "#42bc2c", lighten400: "#1aa800", lighten500: "#179500", lighten600: "#148100", lighten700: "#116e00", lighten800: "#0e5b00", lighten900: "#0b4700", darken10: "#051f00", darken50: "#072900", darken100: "#083700", darken200: "#0c4f00", darken300: "#106800", darken400: "#138000", darken500: "#21980c", darken600: "#47b134", darken700: "#76c967", darken800: "#ade2a4", darken900: "#e9f5e7" }), kf = Object.freeze({ lighten10: "#ecfaf8", lighten50: "#c9fef5", lighten100: "#a5fff2", lighten200: "#5ffce2", lighten300: "#1df6d2", lighten400: "#00d9b5", lighten500: "#00bc9d", lighten600: "#009f84", lighten700: "#00826c", lighten800: "#006554", lighten900: "#00473c", darken10: "#001f1a", darken50: "#002922", darken100: "#00372c", darken200: "#004f42", darken300: "#006856", darken400: "#00806b", darken500: "#0c9881", darken600: "#34b19c", darken700: "#67c9b9", darken800: "#a4e2d7", darken900: "#e7f5f3" }), Sf = Object.freeze({ lighten10: "#ecf9fa", lighten50: "#b1f4fc", lighten100: "#76f0fe", lighten200: "#1de0f6", lighten300: "#00c7dd", lighten400: "#00b0c4", lighten500: "#009aab", lighten600: "#008492", lighten700: "#006d79", lighten800: "#005760", lighten900: "#004047", darken10: "#00161f", darken50: "#001d29", darken100: "#002737", darken200: "#00394f", darken300: "#004a68", darken400: "#005c80", darken500: "#0c7198", darken600: "#348eb1", darken700: "#67adc9", darken800: "#a4d0e2", darken900: "#e7f0f5" }), Ef = Object.freeze({ lighten10: "#ecf4fa", lighten50: "#d2edff", lighten100: "#b7e5ff", lighten200: "#7acaff", lighten300: "#3da2e5", lighten400: "#0b7ecb", lighten500: "#006ab0", lighten600: "#005a96", lighten700: "#004a7c", lighten800: "#003b62", lighten900: "#002b47", darken10: "#00121f", darken50: "#001829", darken100: "#002137", darken200: "#00304f", darken300: "#003e68", darken400: "#004d80", darken500: "#0c6098", darken600: "#347fb1", darken700: "#67a2c9", darken800: "#a4c9e2", darken900: "#e7eff5" }), If = Object.freeze({ lighten10: "#ecf1fa", lighten50: "#d7e6ff", lighten100: "#c1daff", lighten200: "#89b5ff", lighten300: "#5291fb", lighten400: "#1d6ef6", lighten500: "#0653d3", lighten600: "#0444b0", lighten700: "#02368d", lighten800: "#01286a", lighten900: "#001a47", darken10: "#000b1f", darken50: "#000e29", darken100: "#001437", darken200: "#001d4f", darken300: "#002668", darken400: "#002f80", darken500: "#0c4098", darken600: "#3462b1", darken700: "#678bc9", darken800: "#a4bbe2", darken900: "#e7ecf5" }), Of = Object.freeze({ lighten10: "#eeecfa", lighten50: "#ded7ff", lighten100: "#cdc1ff", lighten200: "#9c89ff", lighten300: "#6e52fb", lighten400: "#411df6", lighten500: "#2806d3", lighten600: "#2104b0", lighten700: "#19028d", lighten800: "#12016a", lighten900: "#0c0047", darken10: "#05001f", darken50: "#070029", darken100: "#090037", darken200: "#0d004f", darken300: "#100068", darken400: "#150080", darken500: "#240c98", darken600: "#4934b1", darken700: "#7767c9", darken800: "#aea4e2", darken900: "#e9e7f5" }), Nf = Object.freeze({ lighten10: "#f2ecfa", lighten50: "#e8d7ff", lighten100: "#d3c1ff", lighten200: "#bc89ff", lighten300: "#9b52fb", lighten400: "#7b1df6", lighten500: "#5f06d3", lighten600: "#4f04b0", lighten700: "#3e028d", lighten800: "#2f016a", lighten900: "#1f0047", darken10: "#0d001f", darken50: "#110029", darken100: "#180037", darken200: "#22004f", darken300: "#2d0068", darken400: "#370080", darken500: "#490c98", darken600: "#6a34b1", darken700: "#9267c9", darken800: "#bfa4e2", darken900: "#ede7f5" }), Mf = Object.freeze({ lighten10: "#f6ecfa", lighten50: "#f3d7ff", lighten100: "#f0c1ff", lighten200: "#dc89ff", lighten300: "#c852fb", lighten400: "#b51df6", lighten500: "#9606d3", lighten600: "#7d04b0", lighten700: "#64028d", lighten800: "#4b016a", lighten900: "#320047", darken10: "#15001f", darken50: "#1c0029", darken100: "#260037", darken200: "#38004f", darken300: "#490068", darken400: "#5a0080", darken500: "#6c0c98", darken600: "#8b34b1", darken700: "#ac67c9", darken800: "#cfa4e2", darken900: "#f1e7f5" }), Af = Object.freeze({ lighten10: "#f5f5f5", lighten50: "#ebebeb", lighten100: "#e5e5e5", lighten200: "#dadada", lighten300: "#bebebe", lighten400: "#999999", lighten500: "#858585", lighten600: "#555555", lighten700: "#333333", lighten800: "#222222", lighten900: "#111111", darken10: "#0d0d0d", darken50: "#171717", darken100: "#1f1f1f", darken200: "#2d2d2d", darken300: "#3b3b3b", darken400: "#494949", darken500: "#575757", darken600: "#656565", darken700: "#737373", darken800: "#818181", darken900: "#8f8f8f" }), Df = Object.freeze({ black: "#000000", white: "#ffffff", transparent: "#ffffff00" }), Vf = Object.freeze({ red: gf, chocolate: yf, orange: bf, yellow: wf, lemon: _f, lime: Cf, green: xf, cyan: kf, lightBlue: Ef, teal: Sf, blue: If, indigo: Of, purple: Nf, lightPurple: Mf, gray: Af, shades: Df }), Tf = Symbol("cds:teleport");
const Lf = tu({ align: { type: String, default: "start" } }, "align"), Pf = tu({ text: { type: [String, Number], default: void 0 } }, "text");
const Ff = tu({ title: { type: String, default: "" } }, "title");
function Rf(e2, t2) {
  return { hasTitle: ga(() => !!e2.title || !!t2.title) };
}
const $f = tu({ href: { type: String, default: void 0 }, to: { type: [String, Object], default: void 0 } }, "link");
function Bf(e2, t2) {
  e2.to && e2.href;
  const n2 = ao("RouterLink"), o2 = ga(() => !!e2.to), r2 = ga(() => !(!e2.href && !e2.to)), s2 = o2.value ? n2.useLink(e2) : void 0, a2 = ga(() => o2.value ? e2.to : void 0), l2 = ga(() => o2.value ? null == s2 ? void 0 : s2.route.value.href : e2.href), i2 = s2 && ga(() => {
    var e3;
    return null == (e3 = s2.isActive) ? void 0 : e3.value;
  }), c2 = ga(() => o2.value ? "router-link" : "a");
  return { isLink: r2, isActive: i2, route: null == s2 ? void 0 : s2.route, navigate: null == s2 ? void 0 : s2.navigate, to: a2, href: l2, tag: c2, handleClick: (n3) => {
    t2("click", n3), e2.disabled && (n3.preventDefault(), n3.stopPropagation(), n3.stopImmediatePropagation());
  } };
}
Lo({ name: "IconAdd" }), Lo({ name: "IconAdministrator" }), Lo({ name: "IconAlertFill" }), Lo({ name: "IconAlert" }), Lo({ name: "IconAnalog" }), Lo({ name: "IconArrowDown" }), Lo({ name: "IconArrowLeftBottom" }), Lo({ name: "IconArrowLeftTop" }), Lo({ name: "IconArrowLeft" }), Lo({ name: "IconArrowRightBottom" }), Lo({ name: "IconArrowRightTop" }), Lo({ name: "IconArrowRight" }), Lo({ name: "IconArrowUp" }), Lo({ name: "IconArrowsVertical" }), Lo({ name: "IconCalendar" }), Lo({ name: "IconCardBank" }), Lo({ name: "IconCategory" }), Lo({ name: "IconCheckFill" }), Lo({ name: "IconCheck" }), Lo({ name: "IconCheckboxFill" }), Lo({ name: "IconCheckbox" }), Lo({ name: "IconChevronDown" }), Lo({ name: "IconChevronLeft" }), Lo({ name: "IconChevronRight" }), Lo({ name: "IconChevronUp" }), Lo({ name: "IconChocholateMenu" }), Lo({ name: "IconChocolateMenu" }), Lo({ name: "IconClock" }), Lo({ name: "IconCollapse" }), Lo({ name: "IconComment" }), Lo({ name: "IconCompany" }), Lo({ name: "IconCompare" }), Lo({ name: "IconCopy" }), Lo({ name: "IconDataCards" }), Lo({ name: "IconDelete" }), Lo({ name: "IconDirectoryOpen" }), Lo({ name: "IconDirectory" }), Lo({ name: "IconDocDownload" }), Lo({ name: "IconDoc" }), Lo({ name: "IconDownload" }), Lo({ name: "IconDraggable" }), Lo({ name: "IconEdit" }), Lo({ name: "IconEnter" }), Lo({ name: "IconErrorFill" }), Lo({ name: "IconError" }), Lo({ name: "IconExpand" }), Lo({ name: "IconExternal" }), Lo({ name: "IconFacebookFill" }), Lo({ name: "IconFacebook" }), Lo({ name: "IconFilters" }), Lo({ name: "IconFlag" }), Lo({ name: "IconFunnel" }), Lo({ name: "IconGlobe" }), Lo({ name: "IconInfinity" }), Lo({ name: "IconInfoFill" }), Lo({ name: "IconInfo" }), Lo({ name: "IconInsert" }), Lo({ name: "IconLink" }), Lo({ name: "IconListSearch" }), Lo({ name: "IconList" }), Lo({ name: "IconLoader" }), Lo({ name: "IconLocation" }), Lo({ name: "IconLocked" }), Lo({ name: "IconLogin" }), Lo({ name: "IconLogout" }), Lo({ name: "IconMenu" }), Lo({ name: "IconMinus" }), Lo({ name: "IconNotification" }), Lo({ name: "IconOverflowMenuHorizontal" }), Lo({ name: "IconOverflowMenuVertical" }), Lo({ name: "IconPassword" }), Lo({ name: "IconPin" }), Lo({ name: "IconPlus" }), Lo({ name: "IconPrinter" }), Lo({ name: "IconQuestionFill" }), Lo({ name: "IconQuestion" }), Lo({ name: "IconRefresh" }), Lo({ name: "IconRemove" }), Lo({ name: "IconSandglass" }), Lo({ name: "IconSave" }), Lo({ name: "IconSearch" }), Lo({ name: "IconSettings" }), Lo({ name: "IconShoppingCart" }), Lo({ name: "IconSortDate" }), Lo({ name: "IconSource" }), Lo({ name: "IconStarFill" }), Lo({ name: "IconStar" }), Lo({ name: "IconTelegram" }), Lo({ name: "IconTimer" }), Lo({ name: "IconTool" }), Lo({ name: "IconTruck" }), Lo({ name: "IconUnlocked" }), Lo({ name: "IconUpload" }), Lo({ name: "IconUserAvatar" }), Lo({ name: "IconUserGroup" }), Lo({ name: "IconVisibilityOff" }), Lo({ name: "IconVisibilityOn" }), Lo({ name: "IconVk" }), Lo({ name: "IconWinner" }), Lo({ name: "IconWorkspace" }), Lo({ name: "IconYoutube" });
const Hf = ["add", "administrator", "alert-fill", "alert", "analog", "arrow-down", "arrow-left-bottom", "arrow-left-top", "arrow-left", "arrow-right-bottom", "arrow-right-top", "arrow-right", "arrow-up", "arrows-vertical", "calendar", "card-bank", "category", "check-fill", "check", "checkbox-fill", "checkbox", "chevron-down", "chevron-left", "chevron-right", "chevron-up", "chocholate-menu", "chocolate-menu", "clock", "collapse", "comment", "company", "compare", "copy", "data-cards", "delete", "directory-open", "directory", "doc-download", "doc", "download", "draggable", "edit", "enter", "error-fill", "error", "expand", "external", "facebook-fill", "facebook", "filters", "flag", "funnel", "globe", "infinity", "info-fill", "info", "insert", "link", "list-search", "list", "loader", "location", "locked", "login", "logout", "menu", "minus", "notification", "overflow-menu-horizontal", "overflow-menu-vertical", "password", "pin", "plus", "printer", "question-fill", "question", "refresh", "remove", "sandglass", "save", "search", "settings", "shopping-cart", "sort-date", "source", "star-fill", "star", "telegram", "timer", "tool", "truck", "unlocked", "upload", "user-avatar", "user-group", "visibility-off", "visibility-on", "vk", "winner", "workspace", "youtube"], jf = tu({ prependIcon: { type: String, default: void 0, validator: (e2) => Hf.includes(e2) }, appendIcon: { type: String, default: void 0, validator: (e2) => Hf.includes(e2) } }, "icon"), Kf = tu({ "onClick:prepend": { type: Function, default: void 0 }, "onClick:append": { type: Function, default: void 0 } }, "icon-events");
function zf(e2, t2, n2) {
  const o2 = ga(() => !!t2.prepend || !!e2.prependIcon), r2 = ga(() => !!t2.append || !!e2.appendIcon), s2 = ga(() => ({ [`${ku(n2)}--has-prepend`]: o2.value, [`${ku(n2)}--has-append`]: r2.value, [`${ku(n2)}--has-prepend-clickable`]: !!e2["onClick:prepend"], [`${ku(n2)}--has-append-clickable`]: !!e2["onClick:append"] })), a2 = e2["onClick:prepend"], l2 = e2["onClick:append"];
  return { hasPrepend: o2, hasAppend: r2, iconClasses: s2, handleClickPrepend: a2, handleClickAppend: l2 };
}
const Zf = tu({ loading: { type: Boolean, default: false } }, "loading");
function Uf(e2) {
  return { hasLoading: ga(() => e2.loading), loadingClasses: ga(() => {
    const { loading: t2 } = Lt(e2);
    return t2 ? Sd.loading : void 0;
  }) };
}
const Wf = ["primary", "secondary", "transparent"], Yf = tu({ appearance: { type: String, default: "primary", available: Wf, validator: (e2) => Wf.includes(e2) } }, "appearance");
function Gf(e2, t2) {
  return { appearanceClasses: ga(() => {
    const { appearance: n2 } = Lt(e2);
    return `${ku(t2)}__appearance--${n2}`;
  }) };
}
const qf = ["xs", "sm", "md", "lg", "xl"], Xf = tu({ size: { type: String, default: "md", available: qf, validator: (e2) => qf.includes(e2) } }, "size");
function Jf(e2, t2) {
  return { sizeClasses: ga(() => {
    const { size: n2 } = Lt(e2);
    return `${ku(t2)}__size--${n2}`;
  }) };
}
const Qf = tu({ disabled: { type: Boolean, default: false } }, "disabled");
function eh(e2) {
  return { disabledClasses: ga(() => {
    const { disabled: t2 } = Lt(e2);
    return t2 ? Sd.disabled : void 0;
  }) };
}
const th = tu({ readonly: { type: Boolean, default: false } }, "readonly");
const nh = tu({ invertColor: { type: Boolean, default: false } }, "invert-color");
function oh(e2, t2) {
  return { invertColorClasses: ga(() => {
    const { invertColor: n2 } = Lt(e2);
    return n2 ? `${ku(t2)}--invert-color` : void 0;
  }) };
}
const rh = ["yellow", "light-blue", "blue", "deep-purple", "purple", "pink"], sh = tu({ color: { type: String, available: rh, default: void 0, validator: (e2) => rh.includes(e2) } }, "color");
const ah = ["info", "success", "warning", "error", "progress"], lh = tu({ status: { type: String, default: "info", available: ah, description: "\u0421\u0442\u0430\u0442\u0443\u0441", validator: (e2) => ah.includes(e2) } }, "status");
function ih(e2, t2) {
  return { statusClasses: ga(() => {
    const { status: n2 } = Lt(e2);
    return `${ku(t2)}__status--${n2}`;
  }) };
}
const ch = tu({ tag: { type: String, default: "div", description: "\u041F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044C\u0441\u043A\u0438\u0439 \u044D\u043B\u0435\u043C\u0435\u043D\u0442 \u0434\u043B\u044F \u043E\u0442\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u0438\u044F" } }, "tag"), dh = tu({ items: { type: Array, default: () => [] }, itemTitle: { type: [String, Array, Function], default: "title" }, itemValue: { type: [String, Array, Function], default: "value" }, itemChildren: { type: [Boolean, String, Array, Function], default: "children" }, itemProps: { type: [Boolean, String, Array, Function], default: "props" }, returnObject: { type: Boolean, default: false } }, "items");
function uh(e2) {
  const t2 = Object.keys(dh()), n2 = ga(() => fh(e2, e2.items)), o2 = ga(() => Lt(n2).some((e3) => null == e3.value));
  return { items: n2, itemsProps: t2, transformIn: function(t3) {
    return Lt(o2) || (t3 = t3.filter((e3) => null !== e3)), t3.map((t4) => e2.returnObject && "string" == typeof t4 ? ph(e2, t4) : Lt(n2).find((e3) => Vd(t4, e3.value)) || ph(e2, t4));
  }, transformOut: function(t3) {
    return e2.returnObject ? t3.map(({ raw: e3 }) => e3) : t3.map(({ value: e3 }) => e3);
  } };
}
function ph(e2, t2) {
  var n2;
  const o2 = Pd(t2, e2.itemTitle, t2), r2 = Pd(t2, e2.itemValue, o2), s2 = Pd(t2, e2.itemChildren), a2 = { title: o2, value: r2, ...true === e2.itemProps ? "object" != typeof t2 || null == t2 || Array.isArray(t2) ? void 0 : "children" in t2 ? Gd(t2, ["children"]) : t2 : Pd(t2, e2.itemProps) };
  return { title: String(null != (n2 = a2.title) ? n2 : ""), value: a2.value, props: a2, children: Array.isArray(s2) ? fh(e2, s2) : void 0, raw: t2 };
}
function fh(e2, t2) {
  const n2 = [];
  for (const o2 of t2)
    n2.push(ph(e2, o2));
  return n2;
}
function hh(e2, t2) {
  let n2;
  function o2() {
    n2 = ae(), n2.run(() => t2.length ? t2(() => {
      null == n2 || n2.stop(), o2();
    }) : t2());
  }
  ho(e2, (e3) => {
    e3 && !n2 ? o2() : e3 || (null == n2 || n2.stop(), n2 = void 0);
  }, { immediate: true }), ie(() => {
    null == n2 || n2.stop();
  });
}
function vh(e2, t2, n2, o2 = (e3) => e3, r2 = (e3) => e3) {
  const s2 = nu("useProxyModel"), a2 = At("undefined" !== e2[t2] ? e2[t2] : n2), l2 = ku(t2), i2 = ga(l2 !== t2 ? () => {
    var n3, o3, r3, a3;
    return e2[t2], !(!(null == (n3 = s2.vnode.props) ? void 0 : n3.hasOwnProperty(t2)) && !(null == (o3 = s2.vnode.props) ? void 0 : o3.hasOwnProperty(l2)) || !(null == (r3 = s2.vnode.props) ? void 0 : r3.hasOwnProperty(`onUpdate:${t2}`)) && !(null == (a3 = s2.vnode.props) ? void 0 : a3.hasOwnProperty(`onUpdate:${l2}`)));
  } : () => {
    var n3, o3;
    return e2[t2], !(!(null == (n3 = s2.vnode.props) ? void 0 : n3.hasOwnProperty(t2)) || !(null == (o3 = s2.vnode.props) ? void 0 : o3.hasOwnProperty(`onUpdate:${t2}`)));
  });
  hh(() => !i2.value, () => {
    ho(() => e2[t2], (e3) => {
      a2.value = e3;
    });
  });
  const c2 = ga({ get() {
    const n3 = e2[t2];
    return o2(i2.value ? n3 : a2.value);
  }, set(n3) {
    const l3 = r2(n3), c3 = xt(i2.value ? e2[t2] : a2.value);
    c3 === l3 || o2(c3) === n3 || (a2.value = l3, null == s2 || s2.emit(`update:${t2}`, l3));
  } });
  return Object.defineProperty(c2, "externalValue", { get: () => i2.value ? e2[t2] : a2.value }), c2;
}
const mh = tu({ focused: { type: Boolean, default: false } }, "focus");
function gh(e2, t2) {
  const n2 = vh(e2, "focused");
  return { focusClasses: ga(() => ({ [`${ku(t2)}--focused`]: n2.value })), isFocused: n2, focus: function() {
    n2.value = true;
  }, blur: function() {
    n2.value = false;
  } };
}
const yh = { open: ({ id: e2, value: t2, opened: n2, parents: o2 }) => {
  if (t2) {
    const t3 = /* @__PURE__ */ new Set();
    t3.add(e2);
    let n3 = o2.get(e2);
    for (; null != n3; )
      t3.add(n3), n3 = o2.get(n3);
    return t3;
  }
  return n2.delete(e2), n2;
}, select: () => null }, bh = { open: ({ id: e2, value: t2, opened: n2, parents: o2 }) => {
  if (t2) {
    let t3 = o2.get(e2);
    for (n2.add(e2); null != t3 && t3 !== e2; )
      n2.add(t3), t3 = o2.get(t3);
    return n2;
  }
  return n2.delete(e2), n2;
}, select: () => null }, wh = { open: bh.open, select: ({ id: e2, value: t2, opened: n2, parents: o2 }) => {
  if (!t2)
    return n2;
  const r2 = [];
  let s2 = o2.get(e2);
  for (; null != s2; )
    r2.push(s2), s2 = o2.get(s2);
  return new Set(r2);
} }, _h = (e2) => {
  const t2 = { select: ({ id: t3, value: n2, selected: o2 }) => {
    if (t3 = xt(t3), e2 && !n2) {
      const e3 = Array.from(o2.entries()).reduce((e4, [t4, n3]) => "on" === n3 ? [...e4, t4] : e4, []);
      if (1 === e3.length && e3[0] === t3)
        return o2;
    }
    return o2.set(t3, n2 ? "on" : "off"), o2;
  }, in: (e3, n2, o2) => {
    let r2 = /* @__PURE__ */ new Map();
    for (const s2 of e3 || [])
      r2 = t2.select({ id: s2, value: true, selected: new Map(r2), children: n2, parents: o2 });
    return r2;
  }, out: (e3) => {
    const t3 = [];
    for (const [n2, o2] of e3.entries())
      "on" === o2 && t3.push(n2);
    return t3;
  } };
  return t2;
}, Ch = (e2) => {
  const t2 = _h(e2);
  return { select: ({ selected: e3, id: n2, ...o2 }) => {
    n2 = xt(n2);
    const r2 = e3.has(n2) ? /* @__PURE__ */ new Map([[n2, e3.get(n2)]]) : /* @__PURE__ */ new Map();
    return t2.select({ ...o2, id: n2, selected: r2 });
  }, in: (e3, n2, o2) => {
    let r2 = /* @__PURE__ */ new Map();
    return null != e3 && e3.length && (r2 = t2.in(e3.slice(0, 1), n2, o2)), r2;
  }, out: (e3, n2, o2) => t2.out(e3, n2, o2) };
}, xh = Symbol.for("cds:nested"), kh = { id: At(), root: { children: At(/* @__PURE__ */ new Map()), parents: At(/* @__PURE__ */ new Map()), opened: At(/* @__PURE__ */ new Set()), selected: At(/* @__PURE__ */ new Map()), selectedValues: At([]), register: () => null, unregister: () => null, open: () => null, openOnSelect: () => null, select: () => null } }, Sh = tu({ selectStrategy: [String, Function], openStrategy: [String, Function], opened: Array, selected: Array, required: Boolean }, "nested"), Eh = (e2, t2) => {
  let n2 = false;
  const o2 = At(/* @__PURE__ */ new Map()), r2 = At(/* @__PURE__ */ new Map()), s2 = vh(e2, "opened", e2.opened, (e3) => new Set(e3), (e3) => [...e3.values()]), a2 = ga(() => {
    if ("object" == typeof e2.selectStrategy)
      return e2.selectStrategy;
    switch (e2.selectStrategy) {
      case "single-leaf":
        return ((e3) => {
          const t3 = Ch(e3);
          return { select: ({ id: e4, selected: n3, children: o3, ...r3 }) => (e4 = xt(e4), o3.has(e4) ? n3 : t3.select({ id: e4, selected: n3, children: o3, ...r3 })), in: t3.in, out: t3.out };
        })(e2.required);
      case "leaf":
        return ((e3) => {
          const t3 = _h(e3);
          return { select: ({ id: e4, selected: n3, children: o3, ...r3 }) => (e4 = xt(e4), o3.has(e4) ? n3 : t3.select({ id: e4, selected: n3, children: o3, ...r3 })), in: t3.in, out: t3.out };
        })(e2.required);
      case "independent":
        return _h(e2.required);
      case "single-independent":
        return Ch(e2.required);
      default:
        return ((e3) => {
          const t3 = { select: ({ id: t4, value: n3, selected: o3, children: r3, parents: s3 }) => {
            t4 = xt(t4);
            const a3 = new Map(o3), l3 = [t4];
            for (; l3.length; ) {
              const e4 = l3.shift();
              o3.set(e4, n3 ? "on" : "off"), r3.has(e4) && l3.push(...r3.get(e4));
            }
            let i3 = s3.get(t4);
            for (; i3; ) {
              const e4 = r3.get(i3), t5 = e4.every((e5) => "on" === o3.get(e5)), n4 = e4.every((e5) => !o3.has(e5) || "off" === o3.get(e5));
              o3.set(i3, t5 ? "on" : n4 ? "off" : "indeterminate"), i3 = s3.get(i3);
            }
            return e3 && !n3 && 0 === Array.from(o3.entries()).reduce((e4, [t5, n4]) => "on" === n4 ? [...e4, t5] : e4, []).length ? a3 : o3;
          }, in: (e4, n3, o3) => {
            let r3 = /* @__PURE__ */ new Map();
            for (const s3 of e4 || [])
              r3 = t3.select({ id: s3, value: true, selected: new Map(r3), children: n3, parents: o3 });
            return r3;
          }, out: (e4, t4) => {
            const n3 = [];
            for (const [o3, r3] of e4.entries())
              "on" === r3 && !t4.has(o3) && n3.push(o3);
            return n3;
          } };
          return t3;
        })(e2.required);
    }
  }), l2 = ga(() => {
    if ("object" == typeof e2.openStrategy)
      return e2.openStrategy;
    switch (e2.openStrategy) {
      case "list":
        return wh;
      case "single":
        return yh;
      default:
        return bh;
    }
  }), i2 = vh(e2, "selected", e2.selected, (e3) => a2.value.in(e3, o2.value, r2.value), (e3) => a2.value.out(e3, o2.value, r2.value));
  function c2(e3) {
    const t3 = [];
    let n3 = e3;
    for (; null != n3; )
      t3.unshift(n3), n3 = r2.value.get(n3);
    return t3;
  }
  Yo(() => {
    n2 = true;
  });
  const d2 = { id: At(), root: { children: o2, parents: r2, opened: s2, selected: i2, selectedValues: ga(() => {
    const e3 = [];
    for (const [t3, n3] of i2.value.entries())
      "on" === n3 && e3.push(t3);
    return e3;
  }), register: (e3, t3, n3) => {
    t3 && e3 !== t3 && r2.value.set(e3, t3), n3 && o2.value.set(e3, []), null != t3 && o2.value.set(t3, [...o2.value.get(t3) || [], e3]);
  }, unregister: (e3) => {
    var t3;
    if (n2)
      return;
    o2.value.delete(e3);
    const a3 = r2.value.get(e3);
    if (a3) {
      const n3 = null != (t3 = o2.value.get(a3)) ? t3 : [];
      o2.value.set(a3, n3.filter((t4) => t4 !== e3)), r2.value.delete(e3), s2.value.delete(e3);
    }
  }, open: (e3, n3, a3) => {
    t2.emit("click:open", { id: e3, value: n3, path: c2(e3), event: a3 });
    const i3 = l2.value.open({ id: e3, value: n3, opened: new Set(s2.value), children: o2.value, parents: r2.value, event: a3 });
    i3 && (s2.value = i3);
  }, openOnSelect: (e3, t3, n3) => {
    const a3 = l2.value.select({ id: e3, value: t3, selected: new Map(i2.value), opened: new Set(s2.value), children: o2.value, parents: r2.value, event: n3 });
    a3 && (s2.value = a3);
  }, select: (e3, n3, s3) => {
    t2.emit("click:select", { id: e3, value: n3, path: c2(e3), event: s3 });
    const l3 = a2.value.select({ id: e3, value: n3, selected: new Map(i2.value), children: o2.value, parents: r2.value, event: s3 });
    l3 && (i2.value = l3), d2.root.openOnSelect(e3, n3, s3);
  } } };
  return Ir(xh, d2), d2.root;
}, Ih = (e2, t2) => {
  const n2 = Or(xh, kh), o2 = Symbol(su()), r2 = ga(() => {
    var t3;
    return null != (t3 = e2.value) ? t3 : o2;
  }), s2 = { ...n2, id: r2, open: (e3, t3) => n2.root.open(r2.value, e3, t3), openOnSelect: (e3, t3) => n2.root.openOnSelect(r2.value, e3, t3), isOpen: ga(() => n2.root.opened.value.has(r2.value)), parent: ga(() => n2.root.parents.value.get(r2.value)), select: (e3, t3) => n2.root.select(r2.value, e3, t3), isSelected: ga(() => "on" === n2.root.selected.value.get(xt(r2.value))), isIndeterminate: ga(() => "indeterminate" === n2.root.selected.value.get(r2.value)), isLeaf: ga(() => !n2.root.children.value.get(r2.value)), isGroupActivator: n2.isGroupActivator };
  return !n2.isGroupActivator && n2.root.register(r2.value, n2.id.value, t2), Yo(() => {
    !n2.isGroupActivator && n2.root.unregister(r2.value);
  }), t2 && Ir(xh, s2), s2;
};
function Oh(e2) {
  nu("useRender").render = e2;
}
const Nh = tu({ id: { type: String, default: void 0 }, label: { type: String, default: void 0 }, description: { type: String, default: void 0 }, value: { type: [String, Number, Boolean, Array], default: null }, messages: { type: [Array, String], default: () => [] }, persistentMessages: { type: Boolean, default: false } }, "input");
function Mh(e2, t2, n2 = "input") {
  const o2 = At(su());
  return { id: ga(() => e2.id || `${n2}-${Lt(o2)}`), hasLabel: ga(() => !!e2.label || !!t2.label), hasDescription: ga(() => !!e2.description || !!t2.description) };
}
function Ah(e2, t2) {
  const n2 = At({}), o2 = At({});
  return ho(() => e2, (e3) => {
    const [r2, s2] = function(e4, t3) {
      return Fd(e4, t3).reverse();
    }(e3, t2);
    n2.value = r2, o2.value = s2;
  }, { deep: true, immediate: true }), { inputProps: n2, excludedProps: o2 };
}
function Dh(e2) {
  const t2 = At({}), n2 = At({});
  return ho(() => e2, (e3) => {
    const [o2, r2] = function(e4) {
      return Fd(e4, ["class", "style", "id", /^data-/]);
    }(e3);
    t2.value = o2, n2.value = r2;
  }, { deep: true, immediate: true }), { rootAttrs: t2, inputAttrs: n2 };
}
const Vh = tu({ label: { type: String, default: "" }, description: { type: String, default: "" }, disabled: { type: Boolean, default: false }, readonly: { type: Boolean, default: false }, firstFail: { type: Boolean, default: false }, modelValue: { type: Boolean, default: null }, validateOn: { type: String, default: "input" } }, "form"), Th = Symbol.for("cds:form");
function Lh() {
  return Or(Th, null);
}
const Ph = ["blur", "input", "submit"], Fh = tu({ error: { type: Boolean, default: false }, errorMessages: { type: [Array, String], default: () => [] }, maxErrors: { type: [Number, String], default: 1 }, name: { type: String, default: void 0 }, rules: { type: Array, default: () => [] }, modelValue: { type: [String, Number, Boolean, Array, Object], default: null }, validateOn: { type: String, default: "input", available: Ph, validator: (e2) => Ph.includes(e2) }, validationValue: { type: [String, Number, Array, Object, Boolean], default: void 0 }, hideMessages: { type: Boolean, default: false } }, "validation");
const Rh = ["horizontal", "vertical"], $h = tu({ direction: { type: String, default: "horizontal", available: Rh, validator: (e2) => Rh.includes(e2) } }, "direction");
function Bh(e2, t2) {
  return { directionClasses: ga(() => {
    const { direction: n2 } = Lt(e2);
    return `${ku(t2)}__direction--${n2}`;
  }) };
}
const Hh = ["text", "password", "email", "number", "tel", "url"], jh = tu({ active: { type: Boolean, default: false }, autofocus: { type: Boolean, default: false }, prefix: { type: String, default: void 0 }, suffix: { type: String, default: void 0 }, prependInnerIcon: { type: String, default: void 0, validator: (e2) => Hf.includes(e2) }, appendInnerIcon: { type: String, default: void 0, validator: (e2) => Hf.includes(e2) }, type: { type: String, default: "text", validator: (e2) => Hh.includes(e2) }, role: { type: String, default: void 0 }, placeholder: { type: String, default: void 0 }, counter: { type: Boolean, default: false }, limit: { type: [Number, String], default: void 0 }, clearable: { type: Boolean, default: false }, persistentClearable: { type: Boolean, default: false }, "onClick:prependInner": { type: Function, default: void 0 }, "onClick:appendInner": { type: Function, default: void 0 } }, "field");
function Kh(e2, t2, n2) {
  const o2 = Object.keys(jh()), r2 = ga(() => !!e2.prependInnerIcon || !!t2["prepend-inner"]), s2 = ga(() => !!e2.appendInnerIcon || !!t2["append-inner"]), a2 = ga(() => ({ [`${ku(n2)}--has-prepend-inner`]: r2.value, [`${ku(n2)}--has-append-inner`]: s2.value, [`${ku(n2)}--has-prepend-inner-clickable`]: !!e2["onClick:prependInner"], [`${ku(n2)}--has-append-inner-clickable`]: !!e2["onClick:appendInner"] })), l2 = e2["onClick:prependInner"], i2 = e2["onClick:appendInner"];
  return { hasPrependInnerIcon: r2, hasAppendInnerIcon: s2, fieldClasses: a2, fieldProps: o2, handleClickPrependInner: l2, handleClickAppendInner: i2 };
}
const zh = tu({ transition: { type: [Boolean, String, Object], default: "fade-transition", validator: (e2) => true !== e2 } }, "transition"), Zh = (e2, { slots: t2 }) => {
  const { transition: n2, disabled: o2, group: r2, ...s2 } = e2, { component: a2 = r2 ? ml : Oa, ...l2 } = "object" == typeof n2 ? n2 : {};
  return ba(a2, Us("string" == typeof n2 ? { name: o2 ? "" : n2 } : l2, "string" == typeof n2 ? {} : Object.fromEntries(Object.entries({ disabled: o2, group: r2 }).filter(([e3, t3]) => void 0 !== t3)), s2), t2);
}, Uh = tu({ attach: { type: [Boolean, String, Object], default: false }, contained: { type: Boolean, default: false }, disabled: { type: Boolean, default: false }, persistent: { type: Boolean, default: false }, scrim: { type: [Boolean, String], default: true }, modelValue: { type: Boolean, default: false }, closeOnBack: { type: Boolean, default: true }, zIndex: { type: [Number, String], default: 2e3 }, contentClasses: { type: [String, Array, Object], default: null }, contentProps: { type: Object, default: () => ({}) }, noClickAnimation: { type: Boolean, default: false }, opacity: { type: [Number, String], default: void 0 } }, "overlay"), Wh = tu({ eager: { type: Boolean, default: false } }, "lazy");
function Yh(e2, t2) {
  const n2 = At(false), o2 = ga(() => n2.value || e2.eager || t2.value);
  return ho(t2, () => n2.value = true), { isBooted: n2, hasContent: o2, onAfterLeave: function() {
    e2.eager || (n2.value = false);
  } };
}
const Gh = tu({ closeDelay: { type: [Number, String], default: null }, openDelay: { type: [Number, String], default: null } }, "delay");
function qh(e2, t2) {
  let n2 = () => {
  };
  function o2(o3) {
    null == n2 || n2();
    const r2 = Number(o3 ? e2.openDelay : e2.closeDelay);
    return new Promise((e3) => {
      n2 = function(e4, t3) {
        if (!wd || 0 === e4)
          return t3(), () => {
          };
        const n3 = window.setTimeout(t3, e4);
        return () => window.clearTimeout(n3);
      }(r2, () => {
        null == t2 || t2(o3), e3(o3);
      });
    });
  }
  return { clearDelay: n2, runOpenDelay: function() {
    return o2(true);
  }, runCloseDelay: function() {
    return o2(false);
  } };
}
const Xh = tu({ height: { type: [Number, String], default: "" }, width: { type: [Number, String], default: "" }, maxHeight: { type: [Number, String], default: "" }, maxWidth: { type: [Number, String], default: "" }, minHeight: { type: [Number, String], default: "" }, minWidth: { type: [Number, String], default: "" } }, "dimensions");
function Jh(e2) {
  return { dimensionsStyles: ga(() => ({ height: Bd(e2.height), maxHeight: Bd(e2.maxHeight), maxWidth: Bd(e2.maxWidth), minHeight: Bd(e2.minHeight), minWidth: Bd(e2.minWidth), width: Bd(e2.width) })) };
}
const Qh = { mobileBreakpoint: "sm", thresholds: { xs: 320, sm: 672, md: 1056, lg: 1312, xl: 1584 } }, ev = Symbol.for("cds:display");
function tv(e2, t2) {
  const { thresholds: n2, mobileBreakpoint: o2 } = function(e3 = Qh) {
    return Dd(Qh, e3);
  }(e2), r2 = Dt(ov(t2)), s2 = Dt(rv(t2)), a2 = Dt(sv(t2)), l2 = vt({});
  function i2() {
    s2.value = rv(), r2.value = ov();
  }
  return po(() => {
    const e3 = r2.value < n2.xs, t3 = r2.value < n2.md && !e3, i3 = r2.value < n2.lg && !(t3 || e3), c2 = r2.value < n2.xl && !(i3 || t3 || e3), d2 = r2.value >= n2.xl, u2 = e3 ? "xs" : t3 ? "sm" : i3 ? "md" : c2 ? "lg" : "xl", p2 = "number" == typeof o2 ? o2 : n2[o2], f2 = r2.value < p2;
    l2.xs = e3, l2.sm = t3, l2.md = i3, l2.lg = c2, l2.xl = d2, l2.smUp = !e3, l2.mdUp = !(e3 || t3), l2.lgUp = !(e3 || t3 || i3), l2.xlUp = !(e3 || t3 || i3 || c2), l2.smDown = !(i3 || c2 || d2), l2.mdDown = !(c2 || d2), l2.lgDown = !d2, l2.name = u2, l2.height = s2.value, l2.width = r2.value, l2.mobile = f2, l2.mobileBreakpoint = o2, l2.platform = a2.value, l2.thresholds = n2;
  }), wd && window.addEventListener("resize", i2, { passive: true }), { ...$t(l2), update: function() {
    i2(), a2.value = sv();
  }, ssr: !!t2 };
}
function nv() {
  const e2 = Or(ev);
  if (!e2)
    throw new Error("Could not find `cds:display` injection key. Are you missing the createDisplay function?");
  return e2;
}
function ov(e2) {
  return wd && !e2 ? window.innerWidth : "object" == typeof e2 && e2.clientWidth || 0;
}
function rv(e2) {
  return wd && !e2 ? window.innerHeight : "object" == typeof e2 && e2.clientHeight || 0;
}
function sv(e2) {
  const t2 = wd && !e2 ? window.navigator.userAgent : "ssr";
  function n2(e3) {
    return Boolean(t2.match(e3));
  }
  return { android: n2(/android/i), ios: n2(/iphone|ipad|ipod/i), cordova: n2(/cordova/i), electron: n2(/electron/i), chrome: n2(/chrome/i), edge: n2(/edge/i), firefox: n2(/firefox/i), opera: n2(/opera/i), win: n2(/win/i), mac: n2(/mac/i), linux: n2(/linux/i), touch: Cd, ssr: "ssr" === t2 };
}
function av() {
  const e2 = nu("useScopeId").vnode.scopeId;
  return { scopeId: e2 ? { [e2]: "" } : {} };
}
let lv = false;
const iv = tu({ multiple: { type: Boolean, default: false }, openOnClear: { type: Boolean, default: false }, hideSelected: { type: Boolean, default: false }, hideNoData: { type: Boolean, default: false }, isSelectAll: { type: Boolean, default: false }, ellipsis: { type: Boolean, default: false }, listProps: { type: Object, default: void 0 }, menuIcon: { type: String, default: "chevron-down", validator: (e2) => Hf.includes(e2) } }, "select");
function cv(e2, t2, n2, o2) {
  const r2 = vh(e2, "modelValue", [], (e3) => n2(Td(e3)), (t3) => {
    var n3;
    const r3 = o2(t3);
    return e2.multiple ? r3 : null != (n3 = r3[0]) ? n3 : null;
  }), s2 = ga(() => r2.value.map((e3) => t2.value.find((t3) => Vd(t3.value, e3.value)) || e3)), a2 = ga(() => s2.value.map((e3) => e3.props.value)), l2 = ga(() => t2.value.length === s2.value.length), i2 = ga(() => s2.value.length > 0 && !l2.value), c2 = ga(() => e2.multiple ? "independent" : "single-independent"), d2 = ga(() => e2.hideSelected ? t2.value.filter((e3) => !s2.value.some((t3) => t3 === e3)) : t2.value);
  return { model: r2, selections: s2, selected: a2, selectedAll: l2, selectedIndeterminate: i2, selectStrategy: c2, displayItems: d2 };
}
function dv(e2, t2, n2) {
  const o2 = At({});
  return ho(() => e2, () => {
    const [r2] = Fd(e2, Object.keys(t2)), [s2] = Fd(r2, n2).reverse();
    o2.value = s2;
  }, { deep: true, immediate: true }), { inputProps: o2 };
}
const uv = tu({ customFilter: { type: Function, default: null }, customKeyFilter: { type: Object, default: null }, filterKeys: { type: [Array, String], default: "" }, filterMode: { type: String, default: "intersection" }, noFilter: { type: Boolean, default: false } }, "filter");
function pv(e2, t2, n2, o2) {
  const r2 = At([]), s2 = At(/* @__PURE__ */ new Map()), a2 = ga(() => null != o2 && o2.transform ? Lt(t2).map((e3) => [e3, o2.transform(e3)]) : Lt(t2));
  return po(() => {
    const o3 = "function" == typeof n2 ? n2() : Lt(n2), l2 = "string" != typeof o3 && "number" != typeof o3 ? "" : String(o3), i2 = function(e3, t3, n3) {
      var o4, r3, s3;
      const a3 = [], l3 = null != (o4 = null == n3 ? void 0 : n3.default) ? o4 : fv, i3 = !(null == n3 || !n3.filterKeys) && Td(n3.filterKeys), c3 = Object.keys(null != (r3 = null == n3 ? void 0 : n3.customKeyFilter) ? r3 : {}).length;
      if (null == e3 || !e3.length)
        return a3;
      e:
        for (let o5 = 0; o5 < e3.length; o5++) {
          const r4 = e3[o5], d3 = {}, u3 = {};
          let p2 = -1;
          if (t3 && (null == n3 || !n3.noFilter)) {
            if ("object" == typeof r4) {
              const e5 = i3 || Object.keys(r4);
              for (const o7 of e5) {
                const e6 = Pd(r4, o7, r4), a4 = null == (s3 = null == n3 ? void 0 : n3.customKeyFilter) ? void 0 : s3[o7];
                if (p2 = a4 ? a4(e6, t3, r4) : l3(e6, t3, r4), -1 !== p2 && false !== p2)
                  a4 ? d3[o7] = p2 : u3[o7] = p2;
                else if ("every" === (null == n3 ? void 0 : n3.filterMode))
                  continue e;
              }
            } else
              p2 = l3(r4, t3, r4), -1 !== p2 && false !== p2 && (u3.title = p2);
            const e4 = Object.keys(u3).length, o6 = Object.keys(d3).length;
            if (!e4 && !o6 || "union" === (null == n3 ? void 0 : n3.filterMode) && o6 !== c3 && !e4 || "intersection" === (null == n3 ? void 0 : n3.filterMode) && (o6 !== c3 || !e4))
              continue;
          }
          a3.push({ index: o5, matches: { ...u3, ...d3 } });
        }
      return a3;
    }(Lt(a2), l2, { customKeyFilter: e2.customKeyFilter, default: e2.customFilter, filterKeys: e2.filterKeys, filterMode: e2.filterMode, noFilter: e2.noFilter }), c2 = Lt(t2), d2 = [], u2 = /* @__PURE__ */ new Map();
    i2.forEach(({ index: e3, matches: t3 }) => {
      const n3 = c2[e3];
      d2.push(n3), u2.set(n3.value, t3);
    }), r2.value = d2, s2.value = u2;
  }), { filteredItems: r2, filteredMatches: s2, getMatches: function(e3) {
    return s2.value.get(e3.value);
  } };
}
function fv(e2, t2) {
  return null == e2 || null == t2 ? -1 : e2.toString().toLocaleLowerCase().indexOf(t2.toString().toLocaleLowerCase());
}
const hv = tu({ menu: { type: Boolean, default: void 0 }, menuProps: { type: Object, default: null }, menuHeight: { type: [String, Number], default: 160 } }, "menu");
function vv(e2) {
  const t2 = At(), n2 = vh(e2, "menu");
  return { internalMenu: ga({ get: () => n2.value, set: (o2) => {
    var r2;
    n2.value && !o2 && (null == (r2 = t2.value) ? void 0 : r2.openChildren) || e2.disabled || (n2.value = o2);
  } }), menuRef: t2 };
}
const mv = tu({ modelValue: { type: [String, Array, Object], default: null }, mask: { type: String, default: "DD.MM.YYYY" } }, "datetime");
function gv(e2) {
  const t2 = ga(() => !e2.disabled && !e2.readonly), n2 = ga(() => t2.value ? 0 : -1), o2 = Iv(e2);
  function r2() {
    return o2.date;
  }
  return { editable: t2, tabindex: n2, getCurrentDate: function(e3) {
    const t3 = new Date(), n3 = e3 ? null : 0;
    return { year: t3.getFullYear(), month: t3.getMonth() + 1, day: t3.getDate(), hour: n3, minute: n3, second: n3, millisecond: n3 };
  }, getShortDate: function(e3) {
    return { year: e3.year, month: e3.month, day: e3.day };
  }, getLocale: r2, getMask: function() {
    return e2.mask;
  }, decodeString: function(t3) {
    return Lu(t3, e2.mask, r2(), { hour: 0, minute: 0, second: 0, millisecond: 0 });
  } };
}
function yv(e2) {
  return e2.year + "/" + Wd(e2.month) + "/" + Wd(e2.day);
}
function bv(e2, t2 = "content") {
  const n2 = At(), o2 = At();
  if (wd) {
    const r2 = new ResizeObserver((n3) => {
      null == e2 || e2(n3, r2), n3.length && (o2.value = "content" === t2 ? n3[0].contentRect : n3[0].target.getBoundingClientRect());
    });
    Yo(() => {
      r2.disconnect();
    }), ho(n2, (e3, t3) => {
      t3 && (r2.unobserve(Yd(t3)), o2.value = void 0), e3 && r2.observe(Yd(e3));
    }, { flush: "post" });
  }
  return { resizeRef: n2, contentRect: mt(o2) };
}
function wv(e2) {
  const t2 = At("function" == typeof e2 ? e2() : e2);
  return [t2, function(e3) {
    t2.value = e3;
  }];
}
function _v(e2, t2) {
  const { defaultValue: n2, value: o2 = At() } = t2 || {};
  let r2 = "function" == typeof e2 ? e2() : e2;
  void 0 !== o2.value && (r2 = Lt(o2)), void 0 !== n2 && (r2 = "function" == typeof n2 ? n2() : n2);
  const s2 = At(r2), a2 = At(r2);
  return po(() => {
    let e3 = void 0 !== o2.value ? o2.value : s2.value;
    null != t2 && t2.postState && (e3 = t2.postState(e3)), a2.value = e3;
  }), ho(o2, () => {
    s2.value = o2.value;
  }), [a2, function(e3) {
    const n3 = a2.value;
    s2.value = e3, xt(a2.value) !== e3 && (null == t2 ? void 0 : t2.onChange) && t2.onChange(e3, n3);
  }];
}
const Cv = { notification: { closeLabel: "Close" }, table: { filterTitle: "Filter menu", filterConfirm: "OK", filterReset: "Reset", filterEmptyText: "No filters", filterCheckAll: "Select all items", filterSearchPlaceholder: "Search in filters", emptyText: "No data", selectAll: "Select current page", selectInvert: "Invert current page", selectNone: "Clear all data", selectionAll: "Select all data", sortTitle: "Sort", expand: "Expand row", collapse: "Collapse row", triggerDesc: "Click to sort descending", triggerAsc: "Click to sort ascending", cancelSort: "Click to cancel sorting" }, date: { days: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), daysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"), months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"), monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"), monthsLong: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"), firstDayOfWeek: 0, format24h: false, pluralDay: () => "days" }, container: { prev: "Previous", next: "Next" }, uploader: { uploading: "Uploading", uploadFile: "Upload file", downloadFile: "Download file", removeFile: "Remove file", uploadError: "Upload error", previewFile: "Preview file" }, dropdown: { noData: "No data", selectAll: "Select all" } }, xv = { ru: Su, en: Cv }, kv = "ru", Sv = Symbol.for("cds:locale"), Ev = tu({ locale: { type: [Object, String], default: void 0 } }, "locale");
function Iv(e2) {
  const t2 = e2.locale || Or(Sv);
  return Md(t2) ? { ...xv[kv], ...t2 } : xv[t2] ? { ...xv[t2] } : xv[kv];
}
const Ov = tu({ modelValue: { type: null, default: void 0 }, multiple: { type: Boolean, default: false }, required: { type: [Boolean, String], default: void 0 }, max: { type: Number, default: void 0 }, selectedClass: { type: String, default: void 0 } }, "group"), Nv = tu({ value: { type: null, default: void 0 }, selectedClass: { type: String, default: void 0 } }, "group-item"), Mv = tu({ centerActive: { type: Boolean, default: false }, showArrows: { type: [Boolean, String], validator: (e2) => "boolean" == typeof e2 || ["always", "desktop", "mobile"].includes(e2) } }, "slide-group");
function Av(e2, t2, n2 = true) {
  const o2 = nu("useGroupItem");
  if (!o2)
    throw new Error("[CDS] useGroupItem composable must be used inside a component setup function.");
  const r2 = su();
  Ir(Symbol.for(`${t2.description}:id`), r2);
  const s2 = Or(t2, null);
  if (!s2) {
    if (!n2)
      return s2;
    throw new Error(`[CDS] Could not find useGroup injection with symbol ${t2.description}`);
  }
  const a2 = jt(e2, "value"), l2 = ga(() => s2.disabled.value || e2.disabled);
  s2.register({ id: r2, value: a2, disabled: l2 }, o2), Yo(() => {
    s2.unregister(r2);
  });
  const i2 = ga(() => s2.isSelected(r2)), c2 = ga(() => i2.value && [s2.selectedClass.value, e2.selectedClass]);
  return ho(i2, (e3) => {
    o2.emit("group:selected", { value: e3 });
  }), { id: r2, isSelected: i2, toggle: () => s2.select(r2, !i2.value), select: (e3) => s2.select(r2, e3), selectedClass: c2, value: a2, disabled: l2, group: s2 };
}
function Dv(e2, t2) {
  let n2 = false;
  const o2 = vt([]), r2 = vh(e2, "modelValue", [], (e3) => null == e3 ? [] : Vv(o2, Td(e3)), (t3) => {
    const n3 = function(e3, t4) {
      const n4 = [];
      return t4.forEach((t5) => {
        const o3 = e3.findIndex((e4) => e4.id === t5);
        if (~o3) {
          const t6 = e3[o3];
          n4.push(null != t6.value ? t6.value : o3);
        }
      }), n4;
    }(o2, t3);
    return e2.multiple ? n3 : n3[0];
  }), s2 = nu("useGroup");
  function a2() {
    const t3 = o2.find((e3) => !e3.disabled);
    t3 && "force" === e2.required && !r2.value.length && (r2.value = [t3.id]);
  }
  function l2(t3) {
    if (e2.multiple, r2.value.length) {
      const e3 = r2.value[0], n3 = o2.findIndex((t4) => t4.id === e3);
      let s3 = (n3 + t3) % o2.length, a3 = o2[s3];
      for (; a3.disabled && s3 !== n3; )
        s3 = (s3 + t3) % o2.length, a3 = o2[s3];
      if (a3.disabled)
        return;
      r2.value = [o2[s3].id];
    } else {
      const e3 = o2.find((e4) => !e4.disabled);
      e3 && (r2.value = [e3.id]);
    }
  }
  Zo(() => {
    a2();
  }), Yo(() => {
    n2 = true;
  });
  const i2 = { register: function(e3, n3) {
    const r3 = e3, a3 = qd(Symbol.for(`${t2.description}:id`), null == s2 ? void 0 : s2.vnode).indexOf(n3);
    a3 > -1 ? o2.splice(a3, 0, r3) : o2.push(r3);
  }, unregister: function(e3) {
    if (n2)
      return;
    a2();
    const t3 = o2.findIndex((t4) => t4.id === e3);
    o2.splice(t3, 1);
  }, select: function(t3, n3) {
    const s3 = o2.find((e3) => e3.id === t3);
    if (!n3 || !(null == s3 ? void 0 : s3.disabled))
      if (e2.multiple) {
        const o3 = r2.value.slice(), s4 = o3.findIndex((e3) => e3 === t3), a3 = ~s4;
        if (n3 = null != n3 ? n3 : !a3, a3 && e2.required && o3.length <= 1 || !a3 && null != e2.max && o3.length + 1 > e2.max)
          return;
        s4 < 0 && n3 ? o3.push(t3) : s4 >= 0 && !n3 && o3.splice(s4, 1), r2.value = o3;
      } else {
        const o3 = r2.value.includes(t3);
        if (e2.required && o3)
          return;
        r2.value = (null != n3 ? n3 : !o3) ? [t3] : [];
      }
  }, selected: r2, disabled: jt(e2, "disabled"), prev: () => l2(o2.length - 1), next: () => l2(1), isSelected: (e3) => r2.value.includes(e3), selectedClass: ga(() => e2.selectedClass), items: ga(() => o2), getItemIndex: (e3) => function(e4, t3) {
    const n3 = Vv(e4, [t3]);
    return n3.length ? e4.findIndex((e5) => e5.id === n3[0]) : -1;
  }(o2, e3) };
  return Ir(t2, i2), i2;
}
function Vv(e2, t2) {
  const n2 = [];
  return t2.forEach((t3) => {
    const o2 = e2.find((e3) => Vd(t3, e3.value)), r2 = e2[t3];
    null != (null == o2 ? void 0 : o2.value) ? n2.push(o2.id) : null != r2 && n2.push(r2.id);
  }), n2;
}
const Tv = ["default", "important"], Lv = tu({ variant: { type: String, default: "default", validator: (e2) => Tv.includes(e2) } }, "variant");
const Pv = Symbol("cds:layout"), Fv = () => Or(Pv, {}), Rv = tu({ modelValue: { type: Boolean, default: true }, reveal: { type: Boolean, default: false }, revealOffset: { type: Number, default: 250 }, bordered: { type: Boolean, default: false }, elevated: { type: Boolean, default: false }, height: { type: [String, Number], default: 50 } }, "layout");
function $v(e2, t2, n2 = "H") {
  const o2 = "H" === n2, r2 = "F" === n2, s2 = Fv(), a2 = nv(), l2 = At(parseInt(e2.height, 10)), i2 = At(true), c2 = ga(() => e2.reveal || Lt(s2.view).indexOf(n2) > -1 || Lt(a2.platform).ios && Lt(s2.isContainer)), d2 = At(!wd || Lt(s2.isContainer) ? 0 : window.innerHeight), u2 = ga(() => Lt(s2.isContainer) ? Lt(s2.containerHeight) : Lt(d2)), p2 = ga(() => {
    if (!e2.modelValue)
      return 0;
    if (Lt(c2))
      return Lt(i2) ? Lt(l2) : 0;
    let t3 = 0;
    return o2 && (t3 = Lt(l2) - Lt(s2.scroll).position.top), r2 && (t3 = Lt(s2.scroll).position.top + Lt(u2) + Lt(l2) - Lt(s2.height)), t3 > 0 ? t3 : 0;
  }), f2 = ga(() => !e2.modelValue || Lt(c2) && !Lt(i2)), h2 = ga(() => e2.modelValue && Lt(f2) && e2.reveal), v2 = ga(() => ({ [`${ku(t2)}`]: true, [`${ku(t2)}--fixed`]: Lt(c2), [`${ku(t2)}--absolute`]: !Lt(c2), [`${ku(t2)}--hidden`]: Lt(f2), [`${ku(t2)}--bordered`]: e2.bordered, [`${ku(t2)}--elevated`]: e2.elevated, [`${ku(t2)}--prevent-focus`]: !e2.modelValue })), m2 = ga(() => {
    const e3 = o2 ? Lt(s2.rows).top : Lt(s2.rows).bottom, t3 = {};
    return "l" === e3[0] && s2.left.space && (t3.left = `${s2.left.size}px`), "r" === e3[2] && s2.right.space && (t3.right = `${s2.right.size}px`), t3;
  });
  function g2(e3, t3) {
    s2.update(o2 ? "header" : "footer", e3, t3);
  }
  function y2(e3, t3) {
    Lt(e3) !== t3 && (e3.value = t3);
  }
  ho(() => e2.modelValue, (e3) => {
    g2("space", e3), y2(i2, true), s2.animate();
  }), ho(p2, (e3) => {
    g2("offset", e3);
  }), ho(() => e2.reveal, (t3) => {
    !t3 && y2(i2, e2.modelValue);
  });
  const b2 = {};
  return s2.instances[o2 ? "header" : "footer"] = b2, e2.modelValue && g2("size", Lt(l2)), g2("space", e2.modelValue), g2("offset", Lt(p2)), Yo(() => {
    s2.instances[o2 ? "header" : "footer"] === b2 && (s2.instances[o2 ? "header" : "footer"] = void 0, g2("size", 0), g2("offset", 0), g2("space", false));
  }), { layout: s2, size: l2, revealed: i2, fixed: c2, windowHeight: d2, containerHeight: u2, offset: p2, hidden: f2, revealOnFocus: h2, classes: v2, style: m2, updateLocal: y2, updateRevealed: function() {
    if (!e2.reveal)
      return;
    const { direction: t3, position: n3, inflectionPoint: o3 } = Lt(s2.scroll);
    y2(i2, "up" === t3 || n3.top - o3.top < 100 || Lt(s2.height) - Lt(u2) - n3.top - Lt(l2) < 300);
  }, onResize: function({ height: e3 }) {
    y2(l2, e3), g2("size", e3);
  } };
}
const Bv = { dark: false, colors: { primary: Vf.blue.lighten400, "primary-lighten-1": Vf.blue.lighten50, "primary-lighten-2": Vf.blue.lighten10, secondary: Vf.blue.lighten400, "secondary-lighten-1": Vf.blue.lighten50, "secondary-lighten-2": Vf.blue.lighten10, "surface-primary": Vf.shades.white, "surface-secondary": Vf.gray.lighten10, "surface-inverse": Vf.shades.black, "surface-text": Vf.blue.lighten400, "surface-disabled": Vf.gray.lighten50, "surface-above": Vf.shades.white, "field-primary": Vf.shades.white, "field-secondary": Vf.gray.lighten10, "text-primary": Vf.shades.black, "text-secondary": Vf.gray.lighten500, "text-tertiary": Vf.gray.lighten600, "text-inverse": Vf.shades.white, "text-disabled": Vf.gray.lighten500, "text-above": Vf.shades.white, "neutral-primary": Vf.gray.lighten300, "neutral-primary-darken-1": Vf.gray.lighten400, "neutral-primary-darken-2": Vf.gray.lighten500, "neutral-secondary": Vf.gray.lighten100, "neutral-secondary-darken-1": Vf.gray.lighten200, "neutral-interactive": Vf.blue.lighten400, link: Vf.blue.lighten400, "link-visited": Vf.purple.lighten400, danger: Vf.red.lighten400, "danger-lighten-1": Vf.red.lighten50, "danger-lighten-2": Vf.red.lighten10, warning: Vf.yellow.lighten300, "warning-lighten-1": Vf.yellow.lighten50, "warning-lighten-2": Vf.yellow.lighten10, success: Vf.green.lighten400, "success-lighten-1": Vf.green.lighten50, "success-lighten-2": Vf.green.lighten10, info: Vf.gray.lighten500, "info-lighten-1": Vf.gray.lighten50, "info-lighten-2": Vf.gray.lighten10, progress: Vf.lightBlue.lighten400, "progress-lighten-1": Vf.lightBlue.lighten50, "progress-lighten-2": Vf.lightBlue.lighten10 }, variables: { "overlay-primary": "rgba(85, 85, 85, 0.65)", "overlay-secondary": "rgba(245, 245, 245, 0.65)" } }, Hv = { dark: false, colors: { primary: Vf.lightBlue.lighten200, "primary-lighten-1": Vf.gray.lighten100, "primary-lighten-2": Vf.lightBlue.lighten10, secondary: Vf.lightBlue.lighten500, "secondary-lighten-1": Vf.gray.lighten100, "secondary-lighten-2": Vf.lightBlue.lighten10, "surface-primary": Vf.gray.lighten10, "surface-secondary": Vf.shades.white, "surface-inverse": Vf.shades.black, "surface-text": Vf.shades.black, "surface-disabled": Vf.gray.lighten50, "surface-above": Vf.shades.black, "field-primary": Vf.shades.white, "field-secondary": Vf.gray.lighten10, "text-primary": Vf.shades.black, "text-secondary": Vf.gray.lighten300, "text-tertiary": Vf.gray.lighten600, "text-inverse": Vf.shades.white, "text-disabled": Vf.gray.lighten500, "text-above": Vf.shades.white, "neutral-primary": Vf.gray.lighten300, "neutral-primary-darken-1": Vf.gray.lighten400, "neutral-primary-darken-2": Vf.gray.lighten500, "neutral-secondary": Vf.gray.lighten50, "neutral-secondary-darken-1": Vf.gray.lighten200, "neutral-interactive": Vf.lightBlue.lighten200, link: Vf.lightBlue.lighten400, "link-visited": Vf.purple.lighten700, danger: Vf.red.lighten200, "danger-lighten-1": Vf.red.lighten50, "danger-lighten-2": Vf.red.lighten10, warning: Vf.yellow.lighten300, "warning-lighten-1": Vf.yellow.lighten50, "warning-lighten-2": Vf.yellow.lighten10, success: Vf.green.lighten200, "success-lighten-1": Vf.green.lighten50, "success-lighten-2": Vf.green.lighten10, info: Vf.gray.lighten500, "info-lighten-1": Vf.gray.lighten50, "info-lighten-2": Vf.gray.lighten10, progress: Vf.lightBlue.lighten400, "progress-lighten-1": Vf.lightBlue.lighten50, "progress-lighten-2": Vf.lightBlue.lighten10 }, variables: { "overlay-primary": "rgba(85, 85, 85, 0.65)", "overlay-secondary": "rgba(245, 245, 245, 0.65)" } }, jv = { dark: true, colors: { primary: Vf.blue.lighten400, "primary-lighten-1": Vf.gray.lighten700, "primary-lighten-2": Vf.gray.lighten500, secondary: Vf.blue.lighten400, "secondary-lighten-1": Vf.blue.lighten50, "secondary-lighten-2": Vf.blue.lighten10, "surface-primary": Vf.shades.black, "surface-secondary": Vf.gray.darken200, "surface-inverse": Vf.shades.white, "surface-text": Vf.shades.white, "surface-disabled": Vf.gray.darken700, "surface-above": Vf.shades.white, "field-primary": Vf.gray.darken200, "field-secondary": Vf.shades.black, "text-primary": Vf.shades.white, "text-secondary": Vf.shades.white, "text-tertiary": Vf.gray.darken900, "text-inverse": Vf.shades.black, "text-disabled": Vf.gray.darken400, "text-above": Vf.shades.white, "neutral-primary": Vf.gray.darken400, "neutral-primary-darken-1": Vf.gray.darken500, "neutral-primary-darken-2": Vf.gray.darken600, "neutral-secondary": Vf.gray.darken100, "neutral-secondary-darken-1": Vf.gray.darken200, "neutral-interactive": Vf.blue.darken400, link: Vf.blue.lighten400, "link-visited": Vf.purple.lighten400, danger: Vf.red.lighten400, "danger-lighten-1": Vf.red.darken100, "danger-lighten-2": Vf.red.darken50, warning: Vf.yellow.lighten300, "warning-lighten-1": Vf.yellow.darken100, "warning-lighten-2": Vf.yellow.darken50, success: Vf.green.lighten400, "success-lighten-1": Vf.green.darken100, "success-lighten-2": Vf.green.darken50, info: Vf.gray.lighten500, "info-lighten-1": Vf.gray.darken100, "info-lighten-2": Vf.gray.darken50, progress: Vf.lightBlue.lighten400, "progress-lighten-1": Vf.lightBlue.darken100, "progress-lighten-2": Vf.lightBlue.darken50 }, variables: { "overlay-primary": "rgba(85, 85, 85, 0.65)", "overlay-secondary": "rgba(245, 245, 245, 0.65)" } }, Kv = { dark: false, colors: { primary: Vf.red.lighten500, "primary-lighten-1": Vf.red.lighten50, "primary-lighten-2": Vf.red.lighten10, secondary: Vf.red.lighten400, "secondary-lighten-1": Vf.red.lighten50, "secondary-lighten-2": Vf.red.lighten10, "surface-primary": Vf.shades.white, "surface-secondary": Vf.gray.lighten10, "surface-inverse": Vf.shades.black, "surface-text": Vf.red.lighten400, "surface-disabled": Vf.gray.lighten50, "surface-above": Vf.shades.white, "field-primary": Vf.shades.white, "field-secondary": Vf.gray.lighten10, "text-primary": Vf.shades.black, "text-secondary": Vf.gray.lighten500, "text-tertiary": Vf.gray.lighten600, "text-inverse": Vf.shades.white, "text-disabled": Vf.gray.lighten300, "text-above": Vf.shades.white, "neutral-primary": Vf.gray.lighten300, "neutral-primary-darken-1": Vf.gray.lighten400, "neutral-primary-darken-2": Vf.gray.lighten500, "neutral-secondary": Vf.gray.lighten100, "neutral-secondary-darken-1": Vf.gray.lighten200, "neutral-interactive": Vf.blue.lighten400, link: Vf.blue.lighten400, "link-visited": Vf.purple.lighten400, danger: Vf.red.lighten400, "danger-lighten-1": Vf.red.lighten50, "danger-lighten-2": Vf.red.lighten10, warning: Vf.yellow.lighten300, "warning-lighten-1": Vf.yellow.lighten50, "warning-lighten-2": Vf.yellow.lighten10, success: Vf.green.lighten400, "success-lighten-1": Vf.green.lighten50, "success-lighten-2": Vf.green.lighten10, info: Vf.gray.lighten500, "info-lighten-1": Vf.gray.lighten50, "info-lighten-2": Vf.gray.lighten10, progress: Vf.lightBlue.lighten400, "progress-lighten-1": Vf.lightBlue.lighten50, "progress-lighten-2": Vf.lightBlue.lighten10 }, variables: { "overlay-primary": "rgba(85, 85, 85, 0.65)", "overlay-secondary": "rgba(245, 245, 245, 0.65)" } }, zv = Symbol.for("cds:theme"), Zv = tu({ theme: { type: String, available: ["cds", "b2b", "promo", "dark"], default: void 0 } }, "theme"), Uv = { defaultTheme: "cds", variations: { colors: ["primary"], lighten: 0, darken: 2 }, themes: { cds: Bv, b2b: Hv, promo: Kv, dark: jv } };
function Wv(e2) {
  const t2 = function(e3 = Uv) {
    var t3, n3, o3;
    if (!e3)
      return { ...Uv, isDisabled: true };
    const r3 = {};
    for (const [s3, a3] of Object.entries(null != (t3 = e3.themes) ? t3 : {})) {
      const e4 = a3.dark || "dark" === s3 ? null == (n3 = Uv.themes) ? void 0 : n3.dark : null == (o3 = Uv.themes) ? void 0 : o3.cds;
      r3[s3] = Dd(e4, a3);
    }
    return Dd(Uv, { ...e3, themes: r3 });
  }(e2), n2 = At(t2.defaultTheme), o2 = At(t2.themes), r2 = ga(() => {
    const e3 = {};
    for (const [n3, r3] of Object.entries(Lt(o2))) {
      const o3 = e3[n3] = { ...r3, colors: { ...r3.colors } };
      if (t2.variations)
        for (const e4 of t2.variations.colors) {
          const n4 = o3.colors[e4];
          if (n4)
            for (const r4 of ["lighten", "darken"]) {
              const s3 = "lighten" === r4 ? lf : cf;
              for (const a3 of Kd(t2.variations[r4], 1))
                o3.colors[`${e4}-${r4}-${a3}`] = df(s3(af(n4), a3));
            }
        }
      for (const e4 of Object.keys(o3.colors)) {
        if (/^on-[a-z]/.test(e4) || o3.colors[`on-${e4}`])
          continue;
        const t3 = `on-${e4}`, n4 = af(o3.colors[e4]);
        o3.colors[t3] = vf(n4);
      }
    }
    return e3;
  }), s2 = ga(() => Lt(r2)[Lt(n2)]), a2 = ga(() => {
    const e3 = [];
    Lt(s2).dark && Gv(e3, ":root", ["color-scheme: dark"]), Gv(e3, ":root", qv(Lt(s2)));
    for (const [t4, n4] of Object.entries(Lt(r2)))
      Gv(e3, `.cds-theme--${t4}`, ["color-scheme: " + (n4.dark ? "dark" : "normal"), ...qv(n4)]);
    const t3 = [], n3 = [], o3 = new Set(Object.values(Lt(r2)).flatMap((e4) => Object.keys(e4.colors)));
    for (const e4 of o3)
      /^on-[a-z]/.test(e4) ? Gv(n3, `.${e4}`, [`color: rgb(var(--cds-theme-${e4})) !important`]) : (Gv(t3, `.cds-bg-${e4}`, [`--cds-theme-overlay-multiplier: var(--cds-theme-${e4}-overlay-multiplier)`, `background-color: rgb(var(--cds-theme-${e4})) !important`, `color: rgb(var(--cds-theme-on-${e4})) !important`]), Gv(n3, `.cds-text-${e4}`, [`color: rgb(var(--cds-theme-${e4})) !important`]), Gv(n3, `.cds-border-${e4}`, [`--cds-border-color: var(--v-theme-${e4})`]));
    return e3.push(...t3, ...n3), e3.map((e4, t4) => 0 === t4 ? e4 : `    ${e4}`).join("");
  });
  function l2() {
    return { style: [{ children: Lt(a2), id: "cds-theme-stylesheet", nonce: t2.cspNonce || false }] };
  }
  const i2 = ga(() => t2.isDisabled ? void 0 : `cds-theme--${Lt(n2)}`);
  return { install: function(e3) {
    if (t2.isDisabled)
      return;
    const n3 = e3._context.provides.usehead;
    if (n3)
      if (n3.push) {
        const e4 = n3.push(l2);
        wd && ho(a2, () => e4.patch(l2));
      } else
        wd ? (n3.addHeadObjs(ga(l2)), po(() => n3.updateDOM())) : n3.addHeadObjs(l2());
    else {
      let e4 = wd ? document.getElementById("cds-theme-stylesheet") : null;
      const n4 = () => {
        if (typeof document < "u" && !e4) {
          const n5 = document.createElement("style");
          n5.type = "text/css", n5.id = "cds-theme-stylesheet", t2.cspNonce && n5.setAttribute("nonce", t2.cspNonce), e4 = n5, document.head.appendChild(e4);
        }
        e4 && (e4.innerHTML = Lt(a2));
      };
      wd ? ho(a2, n4, { immediate: true }) : n4();
    }
  }, isDisabled: t2.isDisabled, name: n2, themes: o2, current: s2, computedThemes: r2, themeClasses: i2, styles: a2, global: { name: n2, current: s2 } };
}
function Yv(e2) {
  nu("provideTheme");
  const t2 = Or(zv, null);
  if (!t2)
    throw new Error("Could not find CDS theme injection");
  const n2 = ga(() => {
    var n3;
    return null != (n3 = e2.theme) ? n3 : Lt(t2.name);
  }), o2 = ga(() => Lt(t2.themes)[Lt(n2)]), r2 = ga(() => t2.isDisabled ? void 0 : `cds-theme--${Lt(n2)}`), s2 = { ...t2, name: n2, current: o2, themeClasses: r2 };
  return Ir(zv, s2), s2;
}
function Gv(e2, t2, n2) {
  e2.push(`${t2} {
`, ...n2.map((e3) => `  ${e3};
`), "}\n");
}
function qv(e2) {
  const t2 = e2.dark ? 2 : 1, n2 = e2.dark ? 1 : 2, o2 = [];
  for (const [r2, s2] of Object.entries(e2.colors)) {
    const e3 = af(s2);
    o2.push(`--cds-theme-${r2}: ${e3.r},${e3.g},${e3.b}`), r2.startsWith("on-") || o2.push(`--cds-theme-${r2}-overlay-multiplier: ${hf(s2) > 0.18 ? t2 : n2}`);
  }
  for (const [t3, n3] of Object.entries(e2.variables)) {
    const e3 = "string" == typeof n3 && n3.startsWith("#") ? af(n3) : void 0, r2 = e3 ? `${e3.r}, ${e3.g}, ${e3.b}` : void 0;
    o2.push(`--cds-${t3}: ${null != r2 ? r2 : n3}`);
  }
  return o2;
}
const Xv = tu({ transparent: { type: Boolean, default: false } }, "invert-color");
function Jv(e2, t2) {
  return { transparentClasses: ga(() => {
    const { transparent: n2 } = Lt(e2);
    return n2 ? `${ku(t2)}--transparent` : void 0;
  }) };
}
function Qv(e2, t2) {
  const n2 = Dt(false);
  let o2;
  return { onListScroll: function(e3) {
    cancelAnimationFrame(o2), n2.value = true, o2 = requestAnimationFrame(() => {
      o2 = requestAnimationFrame(() => {
        n2.value = false;
      });
    });
  }, onListKeydown: async function(o3) {
    var r2, s2;
    if (o3.key === kd.tab && (null == (r2 = Lt(t2)) || r2.focus()), ![kd.pagedown, kd.pageup, kd.home, kd.end].includes(o3.key))
      return;
    const a2 = null == (s2 = Lt(e2)) ? void 0 : s2.$el;
    if (!a2)
      return;
    (o3.key === kd.home || o3.key === kd.end) && a2.scrollTo({ top: o3.key === kd.home ? 0 : a2.scrollHeight, behavior: "smooth" }), await async function() {
      await new Promise((e3) => requestAnimationFrame(e3)), await new Promise((e3) => requestAnimationFrame(e3)), await new Promise((e3) => requestAnimationFrame(e3)), await new Promise((e3) => {
        if (Lt(n2)) {
          const t3 = ho(n2, () => {
            t3(), e3();
          });
        } else
          e3();
      });
    }();
    const l2 = a2.querySelectorAll(":scope > :not(.cds-virtual-scroll__spacer)");
    if (o3.key === kd.pagedown || o3.key === kd.home) {
      const e3 = a2.getBoundingClientRect().top;
      for (const t3 of l2)
        if (t3.getBoundingClientRect().top >= e3) {
          t3.focus();
          break;
        }
    } else {
      const e3 = a2.getBoundingClientRect().bottom;
      for (const t3 of [...l2].reverse())
        if (t3.getBoundingClientRect().bottom <= e3) {
          t3.focus();
          break;
        }
    }
  } };
}
const em = Symbol.for("cds:stack"), tm = vt([]);
const nm = Symbol.for("cds:menu"), om = tu({ activator: { type: [String, Object], default: null }, activatorProps: { type: Object, default: () => ({}) }, target: { type: [String, Object], default: void 0 }, openOnClick: { type: Boolean, default: void 0 }, openOnHover: { type: Boolean, default: false }, openOnFocus: { type: Boolean, default: void 0 }, closeOnContentClick: { type: Boolean, default: false } }, "overlay-activator");
function rm(e2, { isActive: t2, isTop: n2 }) {
  const o2 = nu("useActivator"), r2 = At();
  let s2 = false, a2 = false, l2 = true;
  const i2 = ga(() => e2.openOnFocus || null == e2.openOnFocus && e2.openOnHover), c2 = ga(() => e2.openOnClick || null == e2.openOnClick && !e2.openOnHover && !Lt(i2)), { runOpenDelay: d2, runCloseDelay: u2 } = qh(e2, (o3) => {
    o3 === (e2.openOnHover && s2 || i2.value && a2) && (!e2.openOnHover || !t2.value || n2.value) && (t2.value !== o3 && (l2 = true), t2.value = o3);
  }), p2 = At(), f2 = (e3) => {
    e3.stopPropagation(), r2.value = e3.currentTarget || e3.target, t2.value || (p2.value = [e3.clientX, e3.clientY]), t2.value = !t2.value;
  }, h2 = (e3) => {
    var t3;
    null != (t3 = e3.sourceCapabilities) && t3.firesTouchEvents || (s2 = true, r2.value = e3.currentTarget || e3.target, d2());
  }, v2 = () => {
    s2 = false, u2();
  }, m2 = (e3) => {
    false !== Jd(e3.target, ":focus-visible") && (a2 = true, e3.stopPropagation(), r2.value = e3.currentTarget || e3.target, d2());
  }, g2 = (e3) => {
    a2 = false, e3.stopPropagation(), u2();
  }, y2 = ga(() => {
    const t3 = {};
    return c2.value && (t3.onClick = f2), e2.openOnHover && (t3.onMouseenter = h2, t3.onMouseleave = v2), i2.value && (t3.onFocus = m2, t3.onBlur = g2), t3;
  }), b2 = ga(() => {
    const n3 = {};
    if (e2.openOnHover && (n3.onMouseenter = () => {
      s2 = true, d2();
    }, n3.onMouseleave = () => {
      s2 = false, u2();
    }), e2.openOnFocus && (n3.onFocusin = () => {
      a2 = true, d2();
    }, n3.onFocusout = () => {
      a2 = false, u2();
    }), e2.closeOnContentClick) {
      const e3 = Or(nm, null);
      n3.onClick = () => {
        t2.value = false, null == e3 || e3.closeParents();
      };
    }
    return n3;
  }), w2 = ga(() => {
    const t3 = {};
    return e2.openOnHover && (t3.onMouseenter = () => {
      l2 && (s2 = true, l2 = false, d2());
    }, t3.onMouseleave = () => {
      s2 = false, u2();
    }), t3;
  });
  ho(n2, (n3) => {
    n3 && (e2.openOnHover && !s2 && (!i2.value || !a2) || i2.value && !a2 && (!e2.openOnHover || !s2)) && (t2.value = false);
  }), ho(t2, (e3) => {
    e3 || setTimeout(() => {
      p2.value = void 0;
    });
  }, { flush: "post" });
  const _2 = At();
  po(() => {
    !_2.value || un(() => {
      r2.value = Yd(Lt(_2));
    });
  });
  const C2 = At(), x2 = ga(() => "cursor" === e2.target && p2.value ? p2.value : Lt(C2) ? Yd(Lt(C2)) : sm(e2.target, o2) || Lt(r2)), k2 = ga(() => Array.isArray(Lt(x2)) ? void 0 : Lt(x2));
  let S2;
  return ho(() => !!e2.activator, (t3) => {
    t3 && wd ? (S2 = ae(), S2.run(() => {
      !function(e3, t4, { activatorEl: n3, activatorEvents: o3 }) {
        function r3(t5 = a3(), n4 = e3.activatorProps) {
          !t5 || function(e4, t6) {
            Object.keys(t6).forEach((n5) => {
              if (Zd(n5)) {
                const o4 = $d(n5), r4 = Op.get(e4);
                if (null == t6[n5])
                  null == r4 || r4.forEach((t7) => {
                    const [n6, s4] = t7;
                    n6 === o4 && (e4.removeEventListener(o4, s4), r4.delete(t7));
                  });
                else if (!r4 || ![...r4].some((e5) => e5[0] === o4 && e5[1] === t6[n5])) {
                  e4.addEventListener(o4, t6[n5]);
                  const s4 = r4 || /* @__PURE__ */ new Set();
                  s4.add([o4, t6[n5]]), Op.has(e4) || Op.set(e4, s4);
                }
              } else
                null == t6[n5] ? e4.removeAttribute(n5) : e4.setAttribute(n5, t6[n5]);
            });
          }(t5, Us(Lt(o3), n4));
        }
        function s3(t5 = a3(), n4 = e3.activatorProps) {
          !t5 || function(e4, t6) {
            Object.keys(t6).forEach((t7) => {
              if (Zd(t7)) {
                const n5 = $d(t7), o4 = Op.get(e4);
                null == o4 || o4.forEach((t8) => {
                  const [r4, s4] = t8;
                  r4 === n5 && (e4.removeEventListener(n5, s4), o4.delete(t8));
                });
              } else
                e4.removeAttribute(t7);
            });
          }(t5, Us(Lt(o3), n4));
        }
        function a3(o4 = e3.activator) {
          const r4 = sm(o4, t4);
          return n3.value = (null == r4 ? void 0 : r4.nodeType) === Node.ELEMENT_NODE ? r4 : void 0, n3.value;
        }
        ho(() => e3.activator, (e4, t5) => {
          if (t5 && e4 !== t5) {
            const e5 = a3(t5);
            e5 && s3(e5);
          }
          e4 && un(() => r3());
        }, { immediate: true }), ho(() => e3.activatorProps, () => {
          r3();
        }), ie(() => {
          s3();
        });
      }(e2, o2, { activatorEl: r2, activatorEvents: y2 });
    })) : S2 && S2.stop();
  }, { flush: "post", immediate: true }), ie(() => {
    null == S2 || S2.stop();
  }), { activatorEl: r2, activatorRef: _2, target: x2, targetEl: k2, targetRef: C2, activatorEvents: y2, contentEvents: b2, scrimEvents: w2 };
}
function sm(e2, t2) {
  var n2, o2;
  if (!e2)
    return;
  let r2;
  if ("parent" === e2) {
    let e3 = null == (o2 = null == (n2 = null == t2 ? void 0 : t2.proxy) ? void 0 : n2.$el) ? void 0 : o2.parentNode;
    for (; null != e3 && e3.hasAttribute("data-no-activator"); )
      e3 = e3.parentNode;
    r2 = e3;
  } else
    r2 = "string" == typeof e2 ? document.querySelector(e2) : "$el" in e2 ? e2.$el : e2;
  return r2;
}
const am = 24;
function lm(e2, t2) {
  return { x: e2.x + t2.x, y: e2.y + t2.y };
}
function im(e2, t2) {
  if ("top" === e2.side || "bottom" === e2.side) {
    const { side: n2, align: o2 } = e2;
    return lm({ x: "left" === o2 ? 0 : "center" === o2 ? t2.width / 2 : "right" === o2 ? t2.width : o2, y: "top" === n2 ? 0 : "bottom" === n2 ? t2.height : n2 }, t2);
  }
  if ("left" === e2.side || "right" === e2.side) {
    const { side: n2, align: o2 } = e2;
    return lm({ x: "left" === n2 ? 0 : "right" === n2 ? t2.width : n2, y: "top" === o2 ? 0 : "center" === o2 ? t2.height / 2 : "bottom" === o2 ? t2.height : o2 }, t2);
  }
  return lm({ x: t2.width / 2, y: t2.height / 2 }, t2);
}
let cm = true;
const dm = [];
let um = -1;
function pm() {
  cancelAnimationFrame(um), um = requestAnimationFrame(() => {
    const e2 = dm.shift();
    e2 && e2(), dm.length ? pm() : cm = true;
  });
}
const fm = { static: ym, connected: function(e2, t2, n2) {
  (Array.isArray(Lt(e2.target)) || function(e3) {
    for (; e3; ) {
      if ("fixed" === window.getComputedStyle(e3).position)
        return true;
      e3 = e3.offsetParent;
    }
    return false;
  }(Lt(e2.activatorEl))) && Object.assign(n2.value, { position: "fixed", top: 0, left: 0 });
  const { preferredAnchor: o2, preferredOrigin: r2 } = function(e3) {
    const t3 = vt({}), n3 = ga(e3);
    return po(() => {
      for (const e4 in n3.value)
        t3[e4] = n3.value[e4];
    }, { flush: "sync" }), $t(t3);
  }(() => {
    const e3 = rp(t2.location), n3 = "overlap" === t2.origin ? e3 : "auto" === t2.origin ? ap(e3) : rp(t2.origin);
    return e3.side === n3.side && e3.align === lp(n3).align ? { preferredAnchor: ip(e3), preferredOrigin: ip(n3) } : { preferredAnchor: e3, preferredOrigin: n3 };
  }), [s2, a2, l2, i2] = ["minWidth", "minHeight", "maxWidth", "maxHeight"].map((e3) => ga(() => {
    const n3 = parseFloat(t2[e3]);
    return isNaN(n3) ? 1 / 0 : n3;
  })), c2 = ga(() => {
    if (Array.isArray(t2.offset))
      return t2.offset;
    if ("string" == typeof t2.offset) {
      const e3 = t2.offset.split(" ").map(parseFloat);
      return e3.length < 2 && e3.push(0), e3;
    }
    return "number" == typeof t2.offset ? [t2.offset, 0] : [0, 0];
  });
  let d2 = false;
  const u2 = new ResizeObserver(() => {
    d2 && p2();
  });
  function p2() {
    if (d2 = false, requestAnimationFrame(() => d2 = true), !e2.target.value || !e2.contentEl.value)
      return;
    const t3 = function(e3) {
      return Array.isArray(e3) ? new dp({ x: e3[0], y: e3[1], width: 0, height: 0 }) : e3.getBoundingClientRect();
    }(e2.target.value), u3 = function(e3) {
      const t4 = pp(e3);
      return e3.style.removeProperty("right"), t4.x -= parseFloat(e3.style.left || 0), t4.y -= parseFloat(e3.style.top || 0), t4;
    }(e2.contentEl.value), p3 = Gu(e2.contentEl.value);
    p3.length || (p3.push(document.documentElement), e2.contentEl.value.style.top && e2.contentEl.value.style.left || (u3.x += parseFloat(document.documentElement.style.getPropertyValue("--cds-body-scroll-x") || "0"), u3.y += parseFloat(document.documentElement.style.getPropertyValue("--cds-body-scroll-y") || "0")));
    const f2 = p3.reduce((e3, t4) => {
      const n3 = t4.getBoundingClientRect(), o3 = new dp({ x: t4 === document.documentElement ? 0 : n3.x, y: t4 === document.documentElement ? 0 : n3.y, width: t4.clientWidth, height: t4.clientHeight });
      return e3 ? new dp({ x: Math.max(e3.left, o3.left), y: Math.max(e3.top, o3.top), width: Math.min(e3.right, o3.right) - Math.max(e3.left, o3.left), height: Math.min(e3.bottom, o3.bottom) - Math.max(e3.top, o3.top) }) : o3;
    }, void 0);
    f2.x += 12, f2.y += 12, f2.width -= 24, f2.height -= 24;
    let h2 = { anchor: Lt(o2), origin: Lt(r2) };
    function v2(e3) {
      const n3 = new dp(u3), o3 = im(e3.anchor, t3), r3 = im(e3.origin, n3);
      let { x: s3, y: a3 } = function(e4, t4) {
        return { x: e4.x - t4.x, y: e4.y - t4.y };
      }(o3, r3);
      switch (e3.anchor.side) {
        case "top":
          a3 -= c2.value[0];
          break;
        case "bottom":
          a3 += c2.value[0];
          break;
        case "left":
          s3 -= c2.value[0];
          break;
        case "right":
          s3 += c2.value[0];
      }
      switch (e3.anchor.align) {
        case "top":
          a3 -= c2.value[1];
          break;
        case "bottom":
          a3 += c2.value[1];
          break;
        case "left":
          s3 -= c2.value[1];
          break;
        case "right":
          s3 += c2.value[1];
      }
      return n3.x += s3, n3.y += a3, n3.width = Math.min(n3.width, Lt(l2)), n3.height = Math.min(n3.height, Lt(i2)), { overflows: up(n3, f2), x: s3, y: a3 };
    }
    let m2 = 0, g2 = 0;
    const y2 = { x: 0, y: 0 }, b2 = { x: false, y: false };
    let w2 = -1;
    for (; ; ) {
      if (w2++ > 10) {
        console.error("Infinite loop detected in connectedLocationStrategy");
        break;
      }
      const { x: e3, y: t4, overflows: n3 } = v2(h2);
      m2 += e3, g2 += t4, u3.x += e3, u3.y += t4;
      {
        const e4 = cp(h2.anchor), t5 = n3.x.before || n3.x.after, o3 = n3.y.before || n3.y.after;
        let r3 = false;
        if (["x", "y"].forEach((s3) => {
          if ("x" === s3 && t5 && !b2.x || "y" === s3 && o3 && !b2.y) {
            const t6 = { anchor: { ...h2.anchor }, origin: { ...h2.origin } }, o4 = "x" === s3 ? "y" === e4 ? lp : ap : "y" === e4 ? ap : lp;
            t6.anchor = o4(t6.anchor), t6.origin = o4(t6.origin);
            const { overflows: a3 } = v2(t6);
            (a3[s3].before <= n3[s3].before && a3[s3].after <= n3[s3].after || a3[s3].before + a3[s3].after < (n3[s3].before + n3[s3].after) / 2) && (h2 = t6, r3 = b2[s3] = true);
          }
        }), r3)
          continue;
      }
      n3.x.before && (m2 += n3.x.before, u3.x += n3.x.before), n3.x.after && (m2 -= n3.x.after, u3.x -= n3.x.after), n3.y.before && (g2 += n3.y.before, u3.y += n3.y.before), n3.y.after && (g2 -= n3.y.after, u3.y -= n3.y.after);
      {
        const e4 = up(u3, f2);
        y2.x = f2.width - e4.x.before - e4.x.after, y2.y = f2.height - e4.y.before - e4.y.after, m2 += e4.x.before, u3.x += e4.x.before, g2 += e4.y.before, u3.y += e4.y.before;
      }
      break;
    }
    const _2 = cp(h2.anchor);
    return Object.assign(Lt(n2), { "--cds-overlay-anchor-origin": `${h2.anchor.side} ${h2.anchor.align}`, transformOrigin: `${h2.origin.side} ${h2.origin.align}`, top: Bd(bm(g2)), left: Bd(bm(m2)), right: void 0, minWidth: Bd("y" === _2 ? Math.min(Lt(s2), t3.width) : Lt(s2)), maxWidth: Bd(wm(jd(y2.x, Lt(s2) === 1 / 0 ? 0 : Lt(s2), Lt(l2)))), maxHeight: Bd(wm(jd(y2.y, Lt(a2) === 1 / 0 ? 0 : Lt(a2), Lt(i2)))) }), { available: y2, contentBox: u3 };
  }
  return ho([e2.target, e2.contentEl], ([e3, t3], [n3, o3]) => {
    n3 && !Array.isArray(n3) && u2.unobserve(n3), e3 && !Array.isArray(e3) && u2.observe(e3), o3 && u2.unobserve(o3), t3 && u2.observe(t3);
  }, { immediate: true }), ie(() => {
    u2.disconnect();
  }), ho(() => [Lt(o2), Lt(r2), t2.offset, t2.minWidth, t2.minHeight, t2.maxWidth, t2.maxHeight], () => p2()), un(() => {
    const e3 = p2();
    if (!e3)
      return;
    const { available: t3, contentBox: n3 } = e3;
    n3.height > t3.y && requestAnimationFrame(() => {
      p2(), requestAnimationFrame(() => {
        p2();
      });
    });
  }), { updateLocation: p2 };
} }, hm = tu({ locationStrategy: { type: [String, Function], default: "static", validator: (e2) => "function" == typeof e2 || e2 in fm }, location: { type: String, default: "bottom" }, origin: { type: String, default: "auto" }, offset: { type: [Number, String, Array], default: null } }, "location-strategies"), vm = tu({ location: { type: String, default: "bottom" } }, "location");
const mm = { center: "center", top: "bottom", bottom: "top", left: "right", right: "left" };
function gm(e2, t2 = false, n2, o2) {
  if (o2) {
    const { elements: e3 } = function(e4) {
      const t4 = At([]);
      if (!wd)
        return { elements: t4 };
      const n3 = () => {
        t4.value = Array.from(document.querySelectorAll(`[data-alert=${e4}]`));
      };
      let o3;
      return Zo(() => {
        n3(), o3 = new MutationObserver(n3), o3.observe(document.body, { childList: true, subtree: true });
      }), Go(() => {
        o3 && o3.disconnect();
      }), { elements: t4 };
    }(o2), { updateLocation: t3 } = ym();
    ho(e3, (e4) => {
      t3(e4);
    });
  }
  return { locationStyles: ga(() => {
    if (!e2.location)
      return {};
    const { side: o3, align: r2 } = rp(e2.location.split(" ").length > 1 ? e2.location : `${e2.location} center`);
    function s2(e3) {
      return n2 ? n2(e3) : 0;
    }
    const a2 = {};
    return "center" !== o3 && (t2 ? a2[mm[o3]] = `calc(100% - ${s2(o3)}px` : a2[o3] = 0), "center" !== r2 ? t2 ? a2[mm[r2]] = `calc(100% - ${s2(r2)}px` : a2[r2] = 0 : ("center" === o3 ? a2.top = a2.left = "50%" : a2[{ top: "left", bottom: "left", left: "top", right: "top" }[o3]] = "50%", a2.transform = { top: "translateX(-50%)", bottom: "translateX(-50%)", left: "translateY(-50%)", right: "translateY(-50%)", center: "translate(-50%, -50%)" }[o3]), a2;
  }) };
}
function ym() {
  return { updateLocation: function(e2) {
    e2 instanceof Event || requestAnimationFrame(() => {
      const t2 = { top: { left: [], center: [], right: [] }, bottom: { left: [], center: [], right: [] } };
      e2.forEach((e3) => {
        const n2 = "0px" === e3.style.top ? "top" : "bottom", o2 = "0px" === e3.style.left ? "left" : "0px" === e3.style.right ? "right" : "center";
        t2[n2][o2].push(e3);
      }), Object.entries(t2).forEach(([e3, t3]) => {
        Object.entries(t3).forEach(([t4, n2]) => {
          n2.forEach((o2, r2) => {
            const s2 = ((null == o2 ? void 0 : o2.offsetHeight) || 0) + am, a2 = (n2.length - 1 - r2) * s2, l2 = "top" === e3 ? `translateY(${a2}px)` : `translateY(-${a2}px)`;
            "center" === t4 ? (o2.style.left = "50%", o2.style.transform = `translateX(-50%) ${l2}`) : o2.style.transform = l2;
          });
        });
      });
    });
  } };
}
function bm(e2) {
  return Math.round(e2 * devicePixelRatio) / devicePixelRatio;
}
function wm(e2) {
  return Math.ceil(e2 * devicePixelRatio) / devicePixelRatio;
}
const _m = { none: null, close: function(e2) {
  var t2;
  xm(null != (t2 = e2.targetEl.value) ? t2 : e2.contentEl.value, function() {
    e2.isActive.value = false;
  });
}, block: function(e2, t2) {
  var n2;
  const o2 = null == (n2 = e2.root.value) ? void 0 : n2.offsetParent, r2 = [.../* @__PURE__ */ new Set([...Gu(e2.targetEl.value, t2.contained ? o2 : void 0), ...Gu(e2.contentEl.value, t2.contained ? o2 : void 0)])].filter((e3) => !e3.classList.contains(xd)), s2 = window.innerWidth - document.documentElement.offsetWidth, a2 = (l2 = o2 || document.documentElement, Xu(l2) && l2);
  var l2;
  a2 && e2.root.value.classList.add(xd), r2.forEach((e3) => {
    e3.style.setProperty("--cds-body-scroll-x", Bd(-e3.scrollLeft)), e3.style.setProperty("--cds-body-scroll-y", Bd(-e3.scrollTop)), e3 !== document.documentElement && e3.style.setProperty("--cds-scrollbar-offset", Bd(s2)), e3.classList.add(xd);
  }), ie(() => {
    r2.forEach((e3) => {
      const t3 = parseFloat(e3.style.getPropertyValue("--cds-body-scroll-x")), n3 = parseFloat(e3.style.getPropertyValue("--cds-body-scroll-y")), o3 = e3.style.scrollBehavior;
      e3.style.removeProperty("--cds-body-scroll-x"), e3.style.removeProperty("--cds-body-scroll-y"), e3.style.removeProperty("--cds-scrollbar-offset"), e3.classList.remove(xd), e3.scrollLeft = -t3, e3.scrollTop = -n3, e3.style.scrollBehavior = o3;
    }), a2 && e2.root.value.classList.remove(xd);
  });
}, reposition: function(e2, t2, n2) {
  let o2 = false, r2 = -1, s2 = -1;
  function a2(t3) {
    !function(e3) {
      !cm || dm.length ? (dm.push(e3), pm()) : (cm = false, e3(), pm());
    }(() => {
      var n3, r3;
      const s3 = performance.now();
      null == (r3 = (n3 = e2.updateLocation).value) || r3.call(n3, t3), o2 = (performance.now() - s3) / (1e3 / 60) > 2;
    });
  }
  s2 = (typeof requestIdleCallback > "u" ? (e3) => e3() : requestIdleCallback)(() => {
    n2.run(() => {
      var t3;
      xm(null != (t3 = e2.targetEl.value) ? t3 : e2.contentEl.value, (e3) => {
        o2 ? (cancelAnimationFrame(r2), r2 = requestAnimationFrame(() => {
          r2 = requestAnimationFrame(() => {
            a2(e3);
          });
        })) : a2(e3);
      });
    });
  }), ie(() => {
    typeof cancelIdleCallback < "u" && cancelIdleCallback(s2), cancelAnimationFrame(r2);
  });
} }, Cm = tu({ scrollStrategy: { type: [String, Function], default: "block", validator: (e2) => "function" == typeof e2 || e2 in _m } }, "scroll-strategy");
function xm(e2, t2) {
  const n2 = [document, ...Gu(e2)];
  n2.forEach((e3) => {
    e3.addEventListener("scroll", t2, { passive: true });
  }), ie(() => {
    n2.forEach((e3) => {
      e3.removeEventListener("scroll", t2);
    });
  });
}
function km(e2) {
  const { modelValue: t2, ...n2 } = e2;
  return ba(Oa, { name: "fade-transition", appear: true }, { default: () => t2 && ba("div", { class: ["cds-overlay__scrim"], ...n2 }) });
}
const Sm = Symbol.for("cssGrid"), Em = tu({ condensed: { type: Boolean, default: false }, narrow: { type: Boolean, default: false } }, "grid"), Im = tu({ subgrid: { type: Boolean, default: false } }, "subgrid"), Om = tu({ wide: { type: Boolean, default: false } }, "wide"), Nm = ["xs", "sm", "md", "lg", "xl"], Mm = tu({ xs: { type: [Object, Number, String, Boolean], default: void 0 }, sm: { type: [Object, Number, String, Boolean], default: void 0 }, md: { type: [Object, Number, String, Boolean], default: void 0 }, lg: { type: [Object, Number, String, Boolean], default: void 0 }, xl: { type: [Object, Number, String, Boolean], default: void 0 }, span: { type: [Object, Number, String], default: void 0 } }, "columns"), Am = { cssGrid: false, ssr: false, locale: "ru" };
function Dm(e2) {
  var t2, n2, o2;
  const r2 = this.$, s2 = null != (o2 = null == (t2 = r2.parent) ? void 0 : t2.provides) ? o2 : null == (n2 = r2.vnode.appContext) ? void 0 : n2.provides;
  if (s2 && e2 in s2)
    return s2[e2];
}
const Vm = "CdsIcon", Tm = (e2, t2) => {
  const n2 = e2.__vccOpts || e2;
  for (const [e3, o2] of t2)
    n2[e3] = o2;
  return n2;
};
const Lm = Tm(Lo({ name: Vm, props: { name: { type: String, default: void 0, validator: (e2) => Hf.includes(e2) }, ...Xf(), ...nh(), ...Zv() }, emits: [], setup(e2) {
  const { sizeClasses: t2 } = Jf(e2, Vm), { invertColorClasses: n2 } = oh(e2, Vm), { themeClasses: o2 } = Yv(e2);
  return e2.name ? { iconComponent: ga(() => "Icon" + xu(e2.name)), sizeClasses: t2, themeClasses: o2, invertColorClasses: n2 } : { sizeClasses: t2, themeClasses: o2, invertColorClasses: n2 };
} }), [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("i", { class: U(["cds-icon", [e2.sizeClasses, e2.themeClasses, e2.invertColorClasses]]) }, [e2.name ? (Ss(), Ms(ao(e2.iconComponent), { key: 0 })) : js("", true), nr(e2.$slots, "default")], 2);
}]]);
function Pm(e2, t2 = "top center 0", n2) {
  return Lo({ name: e2, props: { group: Boolean, hideOnLeave: Boolean, leaveAbsolute: Boolean, mode: { type: String, default: n2 }, origin: { type: String, default: t2 } }, setup: (t3, { slots: n3 }) => () => ba(t3.group ? ml : Oa, { name: e2, mode: t3.mode, onBeforeEnter(e3) {
    e3.style.transformOrigin = t3.origin;
  }, onLeave(e3) {
    if (t3.leaveAbsolute) {
      const { offsetTop: t4, offsetLeft: n4, offsetWidth: o2, offsetHeight: r2 } = e3;
      e3._transitionInitialStyles = { position: e3.style.position, top: e3.style.top, left: e3.style.left, width: e3.style.width, height: e3.style.height }, e3.style.position = "absolute", e3.style.top = `${t4}px`, e3.style.left = `${n4}px`, e3.style.width = `${o2}px`, e3.style.height = `${r2}px`;
    }
    t3.hideOnLeave && e3.style.setProperty("display", "none", "important");
  }, onAfterLeave(e3) {
    if (t3.leaveAbsolute && (null == e3 ? void 0 : e3._transitionInitialStyles)) {
      const { position: t4, top: n4, left: o2, width: r2, height: s2 } = e3._transitionInitialStyles;
      delete e3._transitionInitialStyles, e3.style.position = t4 || "", e3.style.top = n4 || "", e3.style.left = o2 || "", e3.style.width = r2 || "", e3.style.height = s2 || "";
    }
  } }, n3.default) });
}
function Fm(e2, t2, n2 = "in-out") {
  return Lo({ name: e2, props: { mode: { type: String, default: n2 } }, setup: (n3, { slots: o2 }) => () => ba(Oa, { name: e2, ...t2 }, o2.default) });
}
function Rm(e2 = "", t2 = false) {
  const n2 = t2 ? "width" : "height", o2 = N(`offset-${n2}`);
  return { onBeforeEnter(e3) {
    e3._parent = e3.parentNode, e3._initialStyle = { transition: e3.style.transition, overflow: e3.style.overflow, [n2]: e3.style[n2] };
  }, onEnter(t3) {
    const r3 = t3._initialStyle;
    t3.style.setProperty("transition", "none", "important"), t3.style.overflow = "hidden";
    const s3 = `${t3[o2]}px`;
    t3.style[n2] = "0", t3.offsetHeight, t3.style.transition = null == r3 ? void 0 : r3.transition, e2 && t3._parent && t3._parent.classList.add(e2), requestAnimationFrame(() => {
      t3.style[n2] = s3;
    });
  }, onAfterEnter: s2, onEnterCancelled: s2, onLeave(e3) {
    e3._initialStyle = { transition: "", overflow: e3.style.overflow, [n2]: e3.style[n2] }, e3.style.overflow = "hidden", e3.style[n2] = `${e3[o2]}px`, e3.offsetHeight, requestAnimationFrame(() => e3.style[n2] = "0");
  }, onAfterLeave: r2, onLeaveCancelled: r2 };
  function r2(t3) {
    e2 && t3._parent && t3._parent.classList.remove(e2), s2(t3);
  }
  function s2(e3) {
    const t3 = e3._initialStyle[n2];
    e3.style.overflow = e3._initialStyle.overflow, null != t3 && (e3.style[n2] = t3), delete e3._initialStyle;
  }
}
const $m = Lo({ name: "CdsTransitionModal", props: { target: Object }, setup(e2, { slots: t2 }) {
  const n2 = { onBeforeEnter(e3) {
    e3.style.pointerEvents = "none", e3.style.visibility = "hidden";
  }, async onEnter(t3, n3) {
    var o2;
    await new Promise((e3) => requestAnimationFrame(e3)), t3.style.visibility = "";
    const { x: r2, y: s2, sx: a2, sy: l2, speed: i2 } = Bm(e2.target, t3), c2 = fp(t3, [{ transform: `translate(${r2}px, ${s2}px) scale(${a2}, ${l2})`, opacity: 0 }, {}], { duration: 225 * i2, easing: "cubic-bezier(0.0, 0, 0.2, 1)" });
    null == (o2 = Hm(t3)) || o2.forEach((e3) => {
      fp(e3, [{ opacity: 0 }, { opacity: 0, offset: 0.33 }, {}], { duration: 225 * i2, easing: "cubic-bezier(0.4, 0, 0.2, 1)" });
    }), c2.finished.then(() => n3());
  }, onAfterEnter(e3) {
    e3.style.removeProperty("pointer-events");
  }, onBeforeLeave(e3) {
    e3.style.pointerEvents = "none";
  }, async onLeave(t3, n3) {
    var o2;
    await new Promise((e3) => requestAnimationFrame(e3));
    const { x: r2, y: s2, sx: a2, sy: l2, speed: i2 } = Bm(e2.target, t3);
    fp(t3, [{}, { transform: `translate(${r2}px, ${s2}px) scale(${a2}, ${l2})`, opacity: 0 }], { duration: 125 * i2, easing: "cubic-bezier(0.4, 0, 1, 1)" }).finished.then(() => n3()), null == (o2 = Hm(t3)) || o2.forEach((e3) => {
      fp(e3, [{}, { opacity: 0, offset: 0.2 }, { opacity: 0 }], { duration: 250 * i2, easing: "cubic-bezier(0.4, 0, 0.2, 1)" });
    });
  }, onAfterLeave(e3) {
    e3.style.removeProperty("pointer-events");
  } };
  return () => e2.target ? ba(Oa, { name: "modal-transition", ...n2, css: false }, t2) : ba(Oa, { name: "modal-transition" }, t2);
} });
function Bm(e2, t2) {
  const n2 = e2.getBoundingClientRect(), o2 = pp(t2), [r2, s2] = getComputedStyle(t2).transformOrigin.split(" ").map((e3) => parseFloat(e3)), [a2, l2] = getComputedStyle(t2).getPropertyValue("--cds-overlay-anchor-origin").split(" ");
  let i2 = n2.left + n2.width / 2;
  "left" === a2 || "left" === l2 ? i2 -= n2.width / 2 : ("right" === a2 || "right" === l2) && (i2 += n2.width / 2);
  let c2 = n2.top + n2.height / 2;
  "top" === a2 || "top" === l2 ? c2 -= n2.height / 2 : ("bottom" === a2 || "bottom" === l2) && (c2 += n2.height / 2);
  const d2 = n2.width / o2.width, u2 = n2.height / o2.height, p2 = Math.max(1, d2, u2), f2 = d2 / p2 || 0, h2 = u2 / p2 || 0, v2 = o2.width * o2.height / (window.innerWidth * window.innerHeight), m2 = v2 > 0.12 ? Math.min(1.5, 10 * (v2 - 0.12) + 1) : 1;
  return { x: i2 - (r2 + o2.left), y: c2 - (s2 + o2.top), sx: f2, sy: h2, speed: m2 };
}
function Hm(e2) {
  var t2;
  const n2 = null == (t2 = e2.querySelector(":scope > .cds-card, :scope > .cds-container, :scope > .cds-list")) ? void 0 : t2.children;
  return n2 && [...n2];
}
const jm = Fm("expand-transition", Rm()), Km = Fm("expand-transition-x", Rm("", true)), zm = Pm("fade-transition"), Zm = Pm("slide-transition-y"), Um = Pm("slide-transition-x");
Pm("slide-transition-right"), Pm("slide-transition-left"), Pm("jump-transition-right"), Pm("jump-transition-left"), Pm("scale-transition"), Pm("modal-transition-bottom"), Pm("modal-transition-top");
const Wm = Tm(Lo({ name: "CdsInputMessages", components: { CdsTransition: Zh }, props: { active: Boolean, error: Boolean, messages: { type: [Array, String], default: () => [] }, ...zh({ transition: { component: Zm, leaveAbsolute: true, group: true } }) }, setup: (e2) => ({ internalMessages: ga(() => Td(e2.messages)), messagesClasses: ga(() => ({ "cds-input__messages--error": e2.error })) }) }), [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-transition");
  return Ss(), Ms(a2, { transition: e2.transition, tag: "div", class: U(["cds-input__messages", [e2.messagesClasses]]), role: "alert", "aria-live": "polite" }, { default: Wn(() => [e2.active || e2.error ? (Ss(), Ns(bs, { key: 0 }, [(Ss(true), Ns(bs, null, er(e2.internalMessages, (e3, t3) => (Ss(), Ns("span", { key: `${t3}-${e3}` }, Q(e3), 1))), 128)), Ls("span", null, [nr(e2.$slots, "default")])], 64)) : js("", true)]), _: 3 }, 8, ["transition", "class"]);
}]]), Ym = "CdsInput", Gm = Lo({ name: Ym, props: { ...Nh(), ...jf(), ...Fh(), ...mh(), ...Qf(), ...th(), ...Xf(), ...Zv(), "onClick:prepend": Function, "onClick:append": Function }, emits: { "update:modelValue": (e2) => true, "click:control": (e2) => true, "mousedown:control": (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  var r2;
  const { id: s2 } = Mh(e2, t2), { sizeClasses: a2 } = Jf(e2, Ym), { themeClasses: l2 } = Yv(e2), i2 = ga(() => `${Lt(s2)}-messages`), { errorMessages: c2, isPure: d2, isDirty: u2, isDisabled: p2, isReadonly: f2, isValid: h2, isValidating: v2, validate: m2, resetValidation: g2, reset: y2, validationClasses: b2 } = function(e3, t3, n3 = su()) {
    const o3 = vh(e3, "modelValue"), r3 = ga(() => void 0 === e3.validationValue ? o3.value : e3.validationValue), s3 = Lh(), a3 = At([]), l3 = At(true), i3 = ga(() => !(!Td("" === o3.value ? null : o3.value).length && !Td("" === r3.value ? null : r3.value).length)), c3 = ga(() => !(!e3.disabled && !(null == s3 ? void 0 : s3.isDisabled.value))), d3 = ga(() => !(!e3.readonly && !(null == s3 ? void 0 : s3.isReadonly.value))), u3 = ga(() => e3.errorMessages.length ? Td(e3.errorMessages).slice(0, Math.max(0, +e3.maxErrors)) : a3.value), p3 = ga(() => !e3.error && !u3.value.length && (!e3.rules.length || !l3.value || null)), f3 = At(false), h3 = ga(() => ({ [`${ku(t3)}--error`]: false === p3.value, [`${ku(t3)}--dirty`]: i3.value, [`${ku(t3)}--disabled`]: c3.value, [`${ku(t3)}--readonly`]: d3.value })), v3 = ga(() => {
      var t4;
      return null != (t4 = e3.name) ? t4 : Lt(n3);
    });
    zo(() => {
      null == s3 || s3.register({ id: v3.value, validate: b3, reset: g3, resetValidation: y3 });
    }), Yo(() => {
      null == s3 || s3.unregister(v3.value);
    });
    const m3 = ga(() => e3.validateOn || (null == s3 ? void 0 : s3.validateOn.value) || "input");
    function g3() {
      y3(), o3.value = null;
    }
    function y3() {
      a3.value = [], l3.value = true;
    }
    async function b3() {
      var t4;
      const n4 = [];
      f3.value = true;
      for (const o4 of e3.rules) {
        if (n4.length >= (null != (t4 = e3.maxErrors) ? t4 : 1))
          break;
        const s4 = await ("function" == typeof o4 ? o4 : () => o4)(r3.value);
        true !== s4 && "string" == typeof s4 && n4.push(s4);
      }
      return a3.value = n4, f3.value = false, l3.value = false, a3.value;
    }
    return Zo(() => null == s3 ? void 0 : s3.update(v3.value, p3.value, u3.value)), hh(() => "input" === m3.value, () => {
      ho(r3, () => {
        if (null != r3.value)
          b3();
        else if (e3.focused) {
          const t4 = ho(() => e3.focused, (e4) => {
            e4 || b3(), t4();
          });
        }
      });
    }), hh(() => "blur" === m3.value, () => {
      ho(() => e3.focused, (e4) => {
        e4 || b3();
      });
    }), ho(p3, () => {
      null == s3 || s3.update(v3.value, p3.value, u3.value);
    }), { errorMessages: u3, isPure: l3, isDirty: i3, isDisabled: c3, isReadonly: d3, isValid: p3, isValidating: f3, reset: g3, resetValidation: y3, validate: b3, validationClasses: h3 };
  }(e2, Ym, s2), w2 = ga(() => e2.errorMessages.length ? e2.errorMessages : Lt(c2).length ? Lt(c2) : e2.messages), { hasAppend: _2, hasPrepend: C2, handleClickPrepend: x2, handleClickAppend: k2 } = zf(e2, t2, Ym), { isFocused: S2, focusClasses: E2 } = gh(e2, Ym);
  null == (r2 = t2.messages) || r2.call(t2).every((e3) => e3.type === Symbol.for("Fragment") || e3.type === Symbol.for("Comment"));
  const I2 = ga(() => {
    var n3;
    return !(!Lt(w2).length && !t2.messages && (null == (n3 = e2.messages) || !n3.length) || e2.hideMessages);
  }), O2 = ga(() => !!t2.default), N2 = ga(() => ({ id: s2, isDirty: u2, isDisabled: p2, isReadonly: f2, isPure: d2, isValid: h2, isValidating: v2, reset: y2, resetValidation: g2, validate: m2 }));
  return Oh(() => {
    var o3, r3, s3;
    return ba("div", { class: Cp("cds-input", Lt(b2), Lt(E2), Lt(a2), Lt(l2)) }, [Lt(C2) && ba("div", { class: "cds-input__prepend", onClick: x2 }, [null == (o3 = t2.prepend) ? void 0 : o3.call(t2), !!e2.prependIcon && ba(Lm, { name: e2.prependIcon })]), Lt(O2) && ba("div", { class: "cds-input__control", onClick: (e3) => n2("click:control", e3), onMousedown: (e3) => n2("mousedown:control", e3) }, null == (r3 = t2.default) ? void 0 : r3.call(t2, Lt(N2))), Lt(_2) && ba("div", { class: "cds-input__append", onClick: k2 }, [null == (s3 = t2.append) ? void 0 : s3.call(t2), !!e2.appendIcon && ba(Lm, { name: e2.appendIcon })]), Lt(I2) && ba(Wm, { id: Lt(i2), active: Lt(S2) || e2.persistentMessages, error: !Lt(h2), messages: Lt(w2) }, { default: () => {
      var e3;
      return null == (e3 = t2.messages) ? void 0 : e3.call(t2);
    } })]);
  }), o2({ id: s2, isFocused: S2, isDirty: u2, isDisabled: p2, isReadonly: f2, isPure: d2, isValid: h2, isValidating: v2, messages: w2, reset: y2, resetValidation: g2, validate: m2 }), {};
} }), qm = Lo({ name: "CdsInputLabel", props: { ...Pf(), ...ch({ tag: "label" }) } }), Xm = { class: "cds-input__label-text" };
const Jm = Tm(qm, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ms(ao(e2.tag), { class: "cds-input__label" }, { default: Wn(() => [Ls("span", Xm, [Hs(Q(e2.text) + " ", 1), nr(e2.$slots, "default")])]), _: 3 });
}]]);
const Qm = Tm(Lo({ name: "CdsInputDescription", props: { ...Pf(), ...ch() } }), [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ms(ao(e2.tag), { class: "cds-input__description" }, { default: Wn(() => [Hs(Q(e2.text) + " ", 1), nr(e2.$slots, "default")]), _: 3 });
}]]), eg = Lo({ name: "CdsInputCounter", functional: true, props: { max: { type: [Number, String], default: 0 }, current: { type: [Number, String], default: 0 } }, setup(e2) {
  const t2 = e2.max > 0 ? [ba("span", {}, "/"), ba("span", {}, e2.max)] : [];
  return () => ba("div", { class: "cds-input__counter" }, [ba("span", {}, e2.current), ...t2]);
} }), tg = { model: At(null), isGroup: At(false), disabledGroup: At(false), readonlyGroup: At(false), errorGroup: At(false), handleInputCheckbox: () => {
} }, ng = Symbol.for("cds:checkbox"), og = "CdsCheckboxGroup", rg = Lo({ name: og, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm }, props: { ...Nh(), ...Fh(), ...$h({ direction: "vertical" }), ...Qf(), ...th(), ...Zv() }, emits: ["update:modelValue"], setup(e2, { slots: t2 }) {
  const n2 = vh(e2, "modelValue", () => []), { themeClasses: o2 } = Yv(e2), { hasLabel: r2, hasDescription: s2 } = Mh(e2, t2, "checkbox-group"), { directionClasses: a2 } = Bh(e2, og), { inputProps: l2 } = Ah(e2, ["direction"]);
  return Ir(ng, { model: n2, isGroup: At(true), disabledGroup: At(e2.disabled), readonlyGroup: At(e2.readonly), errorGroup: At(e2.error), handleInputCheckbox: ({ id: e3, checked: t3, value: o3 }) => {
    const r3 = o3 || e3;
    !Array.isArray(Lt(n2)) || (n2.value = t3 ? [...Lt(n2), r3] : Lt(n2).filter((e4) => r3 !== e4));
  } }), { inputProps: l2, hasLabel: r2, hasDescription: s2, directionClasses: a2, themeClasses: o2 };
} }), sg = { class: "cds-checkbox-group__controls" };
const ag = Tm(rg, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-input");
  return Ss(), Ms(i2, Us({ id: e2.id, class: ["cds-checkbox-group", [e2.directionClasses, e2.themeClasses]] }, { ...e2.inputProps }), { messages: Wn(() => [nr(e2.$slots, "messages")]), default: Wn(() => [e2.hasLabel ? (Ss(), Ms(a2, { key: 0, text: e2.label, tag: "div" }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["text"])) : js("", true), e2.hasDescription ? (Ss(), Ms(l2, { key: 1, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true), Ls("div", sg, [nr(e2.$slots, "default")])]), _: 3 }, 16, ["id", "class"]);
}]]), lg = "CdsCheckbox", ig = Lo({ name: lg, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm }, inheritAttrs: false, props: { indeterminate: { type: Boolean, default: false }, ...Nh(), ...Fh(), ...mh(), ...Qf(), ...th(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "update:indeterminate": (e2) => true }, setup(e2, { attrs: t2, slots: n2, expose: o2 }) {
  const r2 = vh(e2, "modelValue"), s2 = vh(e2, "indeterminate"), { id: a2, hasDescription: l2 } = Mh(e2, n2, "checkbox"), { isFocused: i2, focus: c2, blur: d2 } = gh(e2, lg), { inputProps: u2 } = Ah(e2, ["indeterminate", "focused"]), { rootAttrs: p2, inputAttrs: f2 } = Dh(t2), h2 = ga(() => Lt(s2) ? Lt(r2) || "mixed" : Lt(r2) ? "true" : "false"), { model: v2, isGroup: m2, disabledGroup: g2, readonlyGroup: y2, errorGroup: b2, handleInputCheckbox: w2 } = Or(ng, tg), _2 = At(), C2 = ga(() => Lt(g2) || e2.disabled), x2 = ga(() => Lt(y2) || e2.readonly), k2 = ga(() => Lt(b2) || e2.error), S2 = ga(() => ({ "cds-checkbox--disabled": Lt(C2), "cds-checkbox--checked": Lt(r2) || !!t2.checked, "cds-checkbox--indeterminate": Lt(s2) }));
  return ho(v2, (t3) => {
    Array.isArray(t3) && e2.value && (r2.value = t3.includes(e2.value));
  }, { immediate: true }), o2({ id: a2, isGroup: m2, validationRef: _2 }), { id: a2, model: r2, hasDescription: l2, inputProps: u2, rootAttrs: p2, inputAttrs: f2, isFocused: i2, ariaChecked: h2, internalDisabled: C2, internalReadonly: x2, internalError: k2, isGroup: m2, checkboxClasses: S2, onBlur: d2, onFocus: c2, onInput: function(t3) {
    r2.value = t3.target.checked, m2 && w2({ id: Lt(a2), checked: Lt(r2), value: e2.value });
  }, onChange: function() {
    Lt(s2) && (s2.value = false);
  } };
} }), cg = ["id", "name", "checked", "disabled", "value", "aria-disabled", "aria-checked", "aria-invalid"];
const dg = Tm(ig, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-input");
  return Ss(), Ms(i2, Us({ id: e2.id, ref: "validationRef", focused: e2.isFocused, class: ["cds-checkbox", e2.checkboxClasses] }, { ...e2.inputProps, ...e2.rootAttrs }, { "hide-messages": e2.isGroup || e2.inputProps.hideMessages }), { default: Wn(({ isDisabled: n3, isReadonly: o3, isValid: r3 }) => [Ls("input", Us({ id: e2.id, type: "checkbox", name: e2.name, checked: e2.model, disabled: e2.internalDisabled || e2.internalReadonly || n3.value || o3.value, value: e2.value, "aria-disabled": e2.internalDisabled || e2.internalReadonly || n3.value || o3.value, "aria-checked": e2.ariaChecked, "aria-invalid": e2.internalError || false === r3.value }, { ...e2.inputAttrs }, { onBlur: t2[0] || (t2[0] = (...t3) => e2.onBlur && e2.onBlur(...t3)), onFocus: t2[1] || (t2[1] = (...t3) => e2.onFocus && e2.onFocus(...t3)), onInput: t2[2] || (t2[2] = (...t3) => e2.onInput && e2.onInput(...t3)), onChange: t2[3] || (t2[3] = (...t3) => e2.onChange && e2.onChange(...t3)) }), null, 16, cg), Ps(a2, { for: e2.id, text: e2.label }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["for", "text"]), e2.hasDescription ? (Ss(), Ms(l2, { key: 0, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true)]), messages: Wn(() => [nr(e2.$slots, "messages")]), _: 3 }, 16, ["id", "focused", "class", "hide-messages"]);
}]]);
function ug(e2) {
  const t2 = ba("span", {}, e2.item.title);
  if (e2.isPure || null == e2.matches)
    return t2;
  if (Array.isArray(e2.matches))
    throw new Error("Multiple matches are not supported");
  return "number" == typeof e2.matches && ~e2.matches ? ba(bs, {}, [ba("span", { class: `cds-${e2.name}__unmask` }, e2.item.title.substr(0, e2.matches)), ba("span", { class: `cds-${e2.name}__mask` }, e2.item.title.substr(e2.matches, e2.length)), ba("span", { class: `cds-${e2.name}__unmask` }, e2.item.title.substr(e2.matches + e2.length))]) : t2;
}
const pg = Symbol.for("cds:list");
function fg() {
  const e2 = Or(pg, { hasPrepend: At(false), updateHasPrepend: () => null }), t2 = { hasPrepend: At(false), updateHasPrepend: (e3) => {
    e3 && (t2.hasPrepend.value = e3);
  } };
  return Ir(pg, t2), e2;
}
function hg(e2, t2) {
  return () => {
    var n2, o2;
    return null != (o2 = (n2 = t2.slots).default) && o2.call(n2) ? { default: t2.slots.default } : e2.items.map(({ children: n3, props: o3, raw: r2 }) => {
      const s2 = { default: t2.slots.default ? (e3) => {
        var n4, o4;
        return null == (o4 = (n4 = t2.slots).default) ? void 0 : o4.call(n4, { ...e3, item: r2 });
      } : void 0, prepend: t2.slots.prepend ? (e3) => {
        var n4, o4;
        return null == (o4 = (n4 = t2.slots).prepend) ? void 0 : o4.call(n4, { ...e3, item: r2 });
      } : void 0, append: t2.slots.append ? (e3) => {
        var n4, o4;
        return null == (o4 = (n4 = t2.slots).append) ? void 0 : o4.call(n4, { ...e3, item: r2 });
      } : void 0, title: t2.slots.title ? (e3) => {
        var n4, o4;
        return null == (o4 = (n4 = t2.slots).title) ? void 0 : o4.call(n4, { ...e3, item: r2 });
      } : void 0, description: t2.slots.description ? (e3) => {
        var n4, o4;
        return null == (o4 = (n4 = t2.slots).description) ? void 0 : o4.call(n4, { ...e3, item: r2 });
      } : void 0 };
      return null != n3 && n3.length ? ba(Ag, { value: null == o3 ? void 0 : o3.value }, { activator: ({ props: e3 }) => ba(Sg, { ...o3, ...e3 }, s2), default: () => hg({ ...e2, items: n3 }, t2)() }) : ba(Sg, o3, s2);
    });
  };
}
function vg() {
  return Or(pg, null);
}
const mg = "CdsList", gg = Lo({ name: mg, props: { lines: { type: [Boolean, String], default: "one" }, ...dh(), ...ch(), ...Xf({ size: "xs" }), ...Qf(), ...nh(), ...Xv(), ...Sh({ selectStrategy: "single-leaf", openStrategy: "list" }), ...Zv() }, emits: { "update:selected": (e2) => true, "update:opened": (e2) => true, "click:open": (e2) => true, "click:select": (e2) => true }, setup(e2, t2) {
  const { items: n2 } = uh(e2), { themeClasses: o2 } = Yv(e2), { disabledClasses: r2 } = eh(e2), { sizeClasses: s2 } = Jf(e2, mg), { invertColorClasses: a2 } = oh(e2, mg), { transparentClasses: l2 } = Jv(e2, mg), { open: i2, select: c2 } = Eh(e2, t2), { contentRef: d2, onFocus: u2, onFocusin: p2, onFocusout: f2, onKeydown: h2, focus: v2 } = function() {
    const e3 = At(null), t3 = At(false);
    function n3(t4) {
      var o3, r3, s3;
      if (!e3.value)
        return;
      const a3 = [...e3.value.querySelectorAll('button, input:not([tabindex="-1"]), select, textarea, [href], [tabindex]:not([tabindex="-1"])')].filter((e4) => !e4.hasAttribute("disabled")), l3 = a3.indexOf(document.activeElement);
      if (t4)
        if ("first" === t4)
          null == (r3 = a3[0]) || r3.focus();
        else if ("last" === t4)
          null == (s3 = a3.at(-1)) || s3.focus();
        else {
          let e4, o4 = l3;
          const r4 = "next" === t4 ? 1 : -1;
          do {
            o4 += r4, e4 = a3[o4];
          } while ((!e4 || null == e4.offsetParent) && o4 < a3.length && o4 >= 0);
          e4 ? e4.focus() : n3("next" === t4 ? "first" : "last");
        }
      else
        e3.value.contains(document.activeElement) || null == (o3 = a3[0]) || o3.focus();
    }
    return { contentRef: e3, isFocused: t3, onFocus: function(o3) {
      var r3;
      !t3.value && !o3.relatedTarget && (null == (r3 = e3.value) ? void 0 : r3.contains(o3.relatedTarget)) && n3();
    }, onFocusin: function() {
      t3.value = true;
    }, onFocusout: function() {
      t3.value = false;
    }, onKeydown: function(t4) {
      if (e3.value) {
        switch (t4.key) {
          case "ArrowDown":
            n3("next");
            break;
          case "ArrowUp":
            n3("previous");
            break;
          case "Home":
            n3("first");
            break;
          case "End":
            n3("last");
            break;
          default:
            return;
        }
        t4.preventDefault();
      }
    }, focus: n3 };
  }(), m2 = ga(() => e2.lines ? `cds-list--lines-${e2.lines}` : void 0);
  return fg(), Oh(() => {
    const i3 = hg({ ...e2, items: Lt(n2) }, t2);
    return ba(e2.tag, { ref: d2, role: "listbox", class: ["cds-list", Lt(m2), Lt(s2), Lt(a2), Lt(l2), Lt(r2), Lt(o2)], onFocus: u2, onFocusin: p2, onFocusout: f2, onKeydown: h2 }, i3());
  }), t2.expose({ open: i2, select: c2, focus: v2 }), {};
} }), yg = "CdsListItem", bg = Lo({ name: yg, components: { CdsIcon: Lm }, inheritAttrs: false, props: { title: { type: String, default: void 0 }, description: { type: String, default: void 0 }, value: { type: [String, Number, Object, Array, Boolean], default: null }, active: { type: Boolean, default: false }, isClickable: { type: Boolean, default: false }, ...jf(), ...$f(), ...ch(), ...Qf() }, emits: { click: (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const r2 = vg(), { href: s2, to: a2, ...l2 } = Bf(e2, n2), i2 = ga(() => {
    var t3;
    return null != (t3 = e2.value) ? t3 : s2.value;
  }), { select: c2, isSelected: d2, isIndeterminate: u2, isGroupActivator: p2, root: f2, parent: h2, openOnSelect: v2 } = Ih(i2, false), { disabledClasses: m2 } = eh(e2), { hasPrepend: g2, hasAppend: y2 } = zf(e2, t2, yg), b2 = ga(() => {
    var t3;
    return false !== e2.active && (e2.active || (null == (t3 = l2.isActive) ? void 0 : t3.value) || d2.value);
  }), w2 = ga(() => l2.isLink.value), _2 = ga(() => e2.isClickable || !e2.disabled && (l2.isLink.value || null != e2.value && !!r2)), C2 = ga(() => w2.value ? l2.tag.value : "div"), x2 = ga(() => !!t2.title || !!e2.title), k2 = ga(() => !!t2.description || !!e2.description), S2 = ga(() => ({ isActive: b2.value, select: c2, isSelected: d2.value, isIndeterminate: u2.value })), E2 = ga(() => ({ "cds-list-item--active": b2.value, "cds-list-item--clickable": _2.value, "cds-list-item--selected": d2.value }));
  function I2(t3) {
    var o3;
    n2("click", t3), !p2 && _2.value && (null == (o3 = l2.navigate) || o3.call(l2, t3), null != e2.value && c2(!d2.value, t3));
  }
  return ho(() => {
    var e3;
    return null == (e3 = l2.isActive) ? void 0 : e3.value;
  }, (e3) => {
    e3 && null != h2.value && f2.open(h2.value, true), e3 && v2(e3);
  }, { immediate: true }), null == r2 || r2.updateHasPrepend(g2.value), o2({ isActive: b2, isSelected: d2, isIndeterminate: u2, select: c2 }), { internalTag: C2, href: s2, to: a2, slotProps: S2, hasTitle: x2, hasDescription: k2, hasPrepend: g2, hasAppend: y2, classes: E2, disabledClasses: m2, tabindex: _2.value ? 0 : void 0, onClick: I2, onKeyDown: _2.value && !w2.value && function(e3) {
    ("Enter" === e3.key || " " === e3.key) && (e3.preventDefault(), I2(e3));
  } };
} }), wg = { key: 0, class: "cds-list-item__prepend" }, _g = { class: "cds-list-item__content" }, Cg = { key: 0, class: "cds-list-item-title" }, xg = { key: 1, class: "cds-list-item-description" }, kg = { key: 1, class: "cds-list-item__append" };
const Sg = Tm(bg, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-icon");
  return Ss(), Ms(ao(e2.tag), { class: "cds-list-wrapper" }, { default: Wn(() => [(Ss(), Ms(ao(e2.internalTag), Us({ to: e2.to, href: e2.href, class: [[e2.classes, e2.disabledClasses], "cds-list-item"], tabindex: e2.tabindex }, e2.$attrs, { onClick: e2.onClick, onKeydown: e2.onKeyDown }), { default: Wn(() => [e2.hasPrepend ? (Ss(), Ns("div", wg, [nr(e2.$slots, "prepend", W(Rs(e2.slotProps))), e2.prependIcon ? (Ss(), Ms(a2, { key: 0, name: e2.prependIcon }, null, 8, ["name"])) : js("", true)])) : js("", true), Ls("div", _g, [e2.hasTitle ? (Ss(), Ns("div", Cg, [nr(e2.$slots, "title", {}, () => [Hs(Q(e2.title), 1)])])) : js("", true), e2.hasDescription ? (Ss(), Ns("div", xg, [nr(e2.$slots, "description", {}, () => [Hs(Q(e2.description), 1)])])) : js("", true), nr(e2.$slots, "default")]), e2.hasAppend ? (Ss(), Ns("div", kg, [nr(e2.$slots, "append", W(Rs(e2.slotProps))), e2.appendIcon ? (Ss(), Ms(a2, { key: 0, name: e2.appendIcon }, null, 8, ["name"])) : js("", true)])) : js("", true)]), _: 3 }, 16, ["to", "href", "class", "tabindex", "onClick", "onKeydown"]))]), _: 3 });
}]]), Eg = Lo({ name: "CdsListGroupActivator", setup: (e2, { slots: t2 }) => ((() => {
  const e3 = Or(xh, kh);
  Ir(xh, { ...e3, isGroupActivator: true });
})(), () => {
  var e3;
  return null == (e3 = t2.default) ? void 0 : e3.call(t2);
}) }), Ig = Lo({ name: "CdsListGroup", components: { CdsListGroupActivator: Eg, CdsTransitionExpand: jm }, props: { title: { type: String, default: void 0 }, collapseIcon: { type: String, default: "chevron-up" }, expandIcon: { type: String, default: "chevron-down" }, value: { type: [String, Number], default: void 0 }, fluid: { type: Boolean, default: false }, subgroup: { type: Boolean, default: false }, ...ch() }, setup(e2, { expose: t2 }) {
  const { isOpen: n2, open: o2, id: r2 } = Ih(jt(e2, "value"), true), s2 = vg(), a2 = ga(() => `cds-list-group--id-${String(r2.value)}`), l2 = ga(() => n2.value ? e2.collapseIcon : e2.expandIcon), i2 = ga(() => !e2.subgroup), c2 = ga(() => ({ onClick: u2, id: a2.value, appendIcon: l2.value, class: "cds-list-group__header", isClickable: i2.value })), d2 = ga(() => ({ "cds-list-group--open": n2.value, "cds-list-group--prepend": null == s2 ? void 0 : s2.hasPrepend.value, "cds-list-group--fluid": e2.fluid, "cds-list-group--subgroup": e2.subgroup }));
  function u2(e3) {
    o2(!n2.value, e3);
  }
  return fg(), t2({ id: a2, isOpen: n2, isExpandable: i2 }), { id: a2, isOpen: n2, activatorProps: c2, isExpandable: i2, classes: d2 };
} }), Og = { key: 0, class: "cds-list-group__title" }, Ng = ["aria-labelledby"], Mg = ["aria-labelledby"];
const Ag = Tm(Ig, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-list-group-activator"), l2 = ro("cds-transition-expand");
  return Ss(), Ms(ao(e2.tag), { class: U(["cds-list-group", e2.classes]) }, { default: Wn(() => [e2.title ? (Ss(), Ns("div", Og, [nr(e2.$slots, "title"), Hs(" " + Q(e2.title), 1)])) : js("", true), Ps(a2, null, { default: Wn(() => [nr(e2.$slots, "activator", { props: e2.activatorProps, isOpen: e2.isOpen })]), _: 3 }), e2.isExpandable ? (Ss(), Ms(l2, { key: 1 }, { default: Wn(() => [wo(Ls("div", { class: "cds-list-group__items", role: "group", "aria-labelledby": e2.id }, [nr(e2.$slots, "default")], 8, Ng), [[Ua, e2.isOpen]])]), _: 3 })) : (Ss(), Ns("div", { key: 2, class: "cds-list-group__items", role: "group", "aria-labelledby": e2.id }, [nr(e2.$slots, "default")], 8, Mg))]), _: 3 }, 8, ["class"]);
}]]);
function Dg() {
  return true;
}
function Vg(e2, t2, n2) {
  if (!e2 || false === Tg(e2, n2))
    return false;
  const o2 = zu(t2);
  if (typeof ShadowRoot < "u" && o2 instanceof ShadowRoot && o2.host === e2.target)
    return false;
  const r2 = ("object" == typeof n2.value && n2.value.include || (() => []))();
  return r2.push(t2), !r2.some((t3) => null == t3 ? void 0 : t3.contains(e2.target));
}
function Tg(e2, t2) {
  return ("object" == typeof t2.value && t2.value.closeConditional || Dg)(e2);
}
function Lg(e2, t2) {
  const n2 = zu(e2);
  t2(document), typeof ShadowRoot < "u" && n2 instanceof ShadowRoot && t2(n2);
}
const Pg = { mounted(e2, t2) {
  const n2 = (n3) => function(e3, t3, n4) {
    const o3 = "function" == typeof n4.value ? n4.value : n4.value.handler;
    t3._clickOutside.lastMousedownWasOutside && Vg(e3, t3, n4) && setTimeout(() => {
      Tg(e3, n4) && o3 && o3(e3);
    }, 0);
  }(n3, e2, t2), o2 = (n3) => {
    e2._clickOutside.lastMousedownWasOutside = Vg(n3, e2, t2);
  };
  Lg(e2, (e3) => {
    e3.addEventListener("click", n2, true), e3.addEventListener("mousedown", o2, true);
  }), e2._clickOutside || (e2._clickOutside = { lastMousedownWasOutside: true }), e2._clickOutside[t2.instance.$.uid] = { onClick: n2, onMousedown: o2 };
}, unmounted(e2, t2) {
  !e2._clickOutside || (Lg(e2, (n2) => {
    var o2;
    if (!n2 || null == (o2 = e2._clickOutside) || !o2[t2.instance.$.uid])
      return;
    const { onClick: r2, onMousedown: s2 } = e2._clickOutside[t2.instance.$.uid];
    n2.removeEventListener("click", r2, true), n2.removeEventListener("mousedown", s2, true);
  }), delete e2._clickOutside[t2.instance.$.uid]);
} }, Fg = (e2) => {
  const { touchstartX: t2, touchendX: n2, touchstartY: o2, touchendY: r2 } = e2;
  e2.offsetX = n2 - t2, e2.offsetY = r2 - o2, Math.abs(e2.offsetY) < 0.5 * Math.abs(e2.offsetX) && (e2.left && n2 < t2 - 16 && e2.left(e2), e2.right && n2 > t2 + 16 && e2.right(e2)), Math.abs(e2.offsetX) < 0.5 * Math.abs(e2.offsetY) && (e2.up && r2 < o2 - 16 && e2.up(e2), e2.down && r2 > o2 + 16 && e2.down(e2));
};
function Rg(e2 = {}) {
  const t2 = { touchstartX: 0, touchstartY: 0, touchendX: 0, touchendY: 0, touchmoveX: 0, touchmoveY: 0, offsetX: 0, offsetY: 0, left: e2.left, right: e2.right, up: e2.up, down: e2.down, start: e2.start, move: e2.move, end: e2.end };
  return { touchstart: (e3) => function(e4, t3) {
    var n2;
    const o2 = e4.changedTouches[0];
    t3.touchstartX = o2.clientX, t3.touchstartY = o2.clientY, null == (n2 = t3.start) || n2.call(t3, { originalEvent: e4, ...t3 });
  }(e3, t2), touchend: (e3) => function(e4, t3) {
    var n2;
    const o2 = e4.changedTouches[0];
    t3.touchendX = o2.clientX, t3.touchendY = o2.clientY, null == (n2 = t3.end) || n2.call(t3, { originalEvent: e4, ...t3 }), Fg(t3);
  }(e3, t2), touchmove: (e3) => function(e4, t3) {
    var n2;
    const o2 = e4.changedTouches[0];
    t3.touchmoveX = o2.clientX, t3.touchmoveY = o2.clientY, null == (n2 = t3.move) || n2.call(t3, { originalEvent: e4, ...t3 });
  }(e3, t2) };
}
const $g = { mounted: function(e2, t2) {
  var n2, o2, r2;
  const s2 = t2.value, a2 = null != s2 && s2.parent ? e2.parentElement : e2, l2 = null != (n2 = null == s2 ? void 0 : s2.options) ? n2 : { passive: true }, i2 = null == (o2 = t2.instance) ? void 0 : o2.$.uid;
  if (!a2 || !i2)
    return;
  const c2 = Rg(t2.value);
  a2._touchHandlers = null != (r2 = a2._touchHandlers) ? r2 : /* @__PURE__ */ Object.create(null), a2._touchHandlers[i2] = c2, Ed(c2).forEach((e3) => {
    a2.addEventListener(e3, c2[e3], l2);
  });
}, unmounted: function(e2, t2) {
  var n2, o2;
  const r2 = null != (n2 = t2.value) && n2.parent ? e2.parentElement : e2, s2 = null == (o2 = t2.instance) ? void 0 : o2.$.uid;
  if (null == r2 || !r2._touchHandlers || !s2)
    return;
  const a2 = r2._touchHandlers[s2];
  Ed(a2).forEach((e3) => {
    r2.removeEventListener(e3, a2[e3]);
  }), delete r2._touchHandlers[s2];
} };
function Bg(e2, t2) {
  var n2, o2;
  const r2 = null == (n2 = e2._observe) ? void 0 : n2[t2.instance.$.uid];
  !r2 || (r2.observer.unobserve(e2), null == (o2 = e2._observe) || delete o2[t2.instance.$.uid]);
}
const Hg = { mounted: function(e2, t2) {
  if (!_d)
    return;
  const n2 = t2.modifiers || {}, o2 = t2.value, { handler: r2, options: s2 } = "object" == typeof o2 ? o2 : { handler: o2, options: {} }, a2 = new IntersectionObserver((o3 = [], s3) => {
    var a3;
    const l2 = null == (a3 = e2._observe) ? void 0 : a3[t2.instance.$.uid];
    if (!l2)
      return;
    const i2 = o3.some((e3) => e3.isIntersecting);
    r2 && (!n2.quiet || l2.init) && (!n2.once || i2 || l2.init) && r2(i2, o3, s3), i2 && n2.once ? Bg(e2, t2) : l2.init = true;
  }, s2);
  e2._observe = Object(e2._observe), e2._observe[t2.instance.$.uid] = { init: false, observer: a2 }, a2.observe(e2);
}, unmounted: Bg }, jg = Object.freeze(Object.defineProperty({ __proto__: null, ClickOutside: Pg, Touch: $g, Intersect: Hg }, Symbol.toStringTag, { value: "Module" })), Kg = Lo({ name: "CdsOverlay", directives: { ClickOutside: Pg }, inheritAttrs: false, props: { absolute: { type: Boolean, default: false }, ...Uh(), ...om(), ...Gh(), ...Xh(), ...hm(), ...Cm(), ...Wh(), ...zh(), _disableGlobalStack: { type: Boolean, default: void 0 } }, emits: { "update:modelValue": (e2) => true, "click:outside": (e2) => true, afterEnter: () => true, afterLeave: () => true }, setup(e2, { attrs: t2, emit: n2, slots: o2, expose: r2 }) {
  const s2 = vh(e2, "modelValue"), a2 = ga({ get: () => s2.value, set: (t3) => {
    t3 && e2.disabled || (s2.value = t3);
  } }), { teleportTarget: l2 } = function(e3, t3) {
    return { teleportTarget: ga(() => {
      if (!wd)
        return;
      const n3 = { target: document.body, contained: false, containerClassName: t3 }, o3 = { ...n3, ...Or(Tf, { ...n3 }) }, r3 = o3.contained || Lt(e3);
      if (true === r3)
        return;
      const s3 = false === r3 ? o3.target : "string" == typeof r3 ? document.querySelector(r3) : r3;
      if (null == s3)
        return;
      let a3 = s3.querySelector(`:scope > .${o3.containerClassName}`);
      return a3 || (a3 = document.createElement("div"), a3.className = o3.containerClassName, s3.appendChild(a3)), a3;
    }) };
  }(ga(() => e2.attach || e2.contained), "cds-overlay-container"), { hasContent: i2, onAfterLeave: c2 } = Yh(e2, a2), { globalTop: d2, localTop: u2, stackStyles: p2 } = function(e3, t3, n3 = false) {
    const o3 = nu("useStack"), r3 = !n3, s3 = Or(em, void 0), a3 = vt({ activeChildren: /* @__PURE__ */ new Set() }), l3 = Dt(true), i3 = Dt(+t3.value);
    Ir(em, a3), hh(e3, () => {
      var e4;
      const n4 = null == (e4 = tm.at(-1)) ? void 0 : e4[1];
      i3.value = n4 ? n4 + 10 : +t3.value, r3 && tm.push([o3.uid, i3.value]), null == s3 || s3.activeChildren.add(o3.uid), ie(() => {
        if (r3) {
          const e5 = xt(tm).findIndex((e6) => e6[0] === o3.uid);
          tm.splice(e5, 1);
        }
        null == s3 || s3.activeChildren.delete(o3.uid);
      });
    }), r3 && po(() => {
      var e4;
      const t4 = (null == (e4 = tm.at(-1)) ? void 0 : e4[0]) === o3.uid;
      setTimeout(() => l3.value = t4);
    });
    const c3 = ga(() => !a3.activeChildren.size);
    return { globalTop: mt(l3), localTop: c3, stackStyles: ga(() => ({ zIndex: i3.value })) };
  }(a2, jt(e2, "zIndex"), e2._disableGlobalStack), { activatorEl: f2, activatorRef: h2, target: v2, targetEl: m2, targetRef: g2, activatorEvents: y2, contentEvents: b2, scrimEvents: w2 } = rm(e2, { isActive: a2, isTop: u2 }), { dimensionsStyles: _2 } = Jh(e2), C2 = function() {
    if (!wd)
      return Dt(false);
    const { ssr: e3 } = nv();
    if (e3) {
      const e4 = Dt(false);
      return Zo(() => {
        e4.value = true;
      }), e4;
    }
    return Dt(true);
  }(), { scopeId: x2 } = av();
  ho(() => e2.disabled, (e3) => {
    e3 && (a2.value = false);
  });
  const k2 = At(), S2 = At(), E2 = At(), { contentStyles: I2, updateLocation: O2 } = function(e3, t3) {
    const n3 = At({}), o3 = At();
    function r3(e4) {
      var t4;
      null == (t4 = o3.value) || t4.call(o3, e4);
    }
    return wd && hh(() => !(!t3.isActive.value || !e3.locationStrategy), (s3) => {
      var a3, l3;
      ho(() => e3.locationStrategy, s3), ie(() => {
        window.removeEventListener("resize", r3), o3.value = void 0;
      }), window.addEventListener("resize", r3, { passive: true }), "function" == typeof e3.locationStrategy ? o3.value = null == (a3 = e3.locationStrategy(t3, e3, n3)) ? void 0 : a3.updateLocation : o3.value = null == (l3 = fm[e3.locationStrategy](t3, e3, n3)) ? void 0 : l3.updateLocation;
    }), { contentStyles: n3, updateLocation: o3 };
  }(e2, { contentEl: E2, activatorEl: f2, target: v2, isActive: a2 });
  !function(e3, t3) {
    if (!wd)
      return;
    let n3;
    po(async () => {
      null == n3 || n3.stop(), t3.isActive.value && e3.scrollStrategy && (n3 = ae(), await new Promise((e4) => setTimeout(e4)), n3.active && n3.run(() => {
        var o3;
        "function" == typeof e3.scrollStrategy ? e3.scrollStrategy(t3, e3, n3) : null == (o3 = _m[e3.scrollStrategy]) || o3.call(_m, t3, e3, n3);
      }));
    }), ie(() => {
      null == n3 || n3.stop();
    });
  }(e2, { root: k2, contentEl: E2, targetEl: m2, isActive: a2, updateLocation: O2 });
  const N2 = ga(() => ({ "cds-overlay--active": Lt(a2), "cds-overlay--absolute": e2.absolute || e2.contained, "cds-overlay--contained": e2.contained })), M2 = ga(() => [Lt(p2), { "--cds-overlay-opacity": e2.opacity, top: Bd(Lt(A2)) }]);
  wd && ho(a2, (e3) => {
    e3 ? window.addEventListener("keydown", L2) : window.removeEventListener("keydown", L2);
  }, { immediate: true }), Yo(() => {
    !wd || window.removeEventListener("keydown", L2);
  });
  const A2 = At();
  ho(() => a2.value && (e2.absolute || e2.contained) && null == l2.value, (e3) => {
    if (e3) {
      const e4 = function(e5) {
        for (; e5; ) {
          if (Xu(e5))
            return e5;
          e5 = e5.parentElement;
        }
        return document.scrollingElement;
      }(k2.value);
      e4 && e4 !== document.scrollingElement && (A2.value = e4.scrollTop);
    }
  });
  const D2 = function() {
    var e3, t3;
    return null == (t3 = null == (e3 = nu("useRouter")) ? void 0 : e3.proxy) ? void 0 : t3.$router;
  }();
  function V2(t3) {
    n2("click:outside", t3), e2.persistent ? R2() : a2.value = false;
  }
  function T2(t3) {
    return a2.value && d2.value && (!e2.scrim || t3.target === Lt(S2));
  }
  function L2(t3) {
    var n3, o3;
    "Escape" === t3.key && d2.value && (e2.persistent ? R2() : (a2.value = false, null != (n3 = E2.value) && n3.contains(document.activeElement) && (null == (o3 = f2.value) || o3.focus())));
  }
  function P2() {
    n2("afterEnter");
  }
  function F2() {
    c2(), n2("afterLeave");
  }
  function R2() {
    e2.noClickAnimation || E2.value && fp(E2.value, [{ transformOrigin: "center" }, { transform: "scale(1.03)" }, { transformOrigin: "center" }], { duration: 150, easing: "cubic-bezier(0.4, 0, 0.2, 1)" });
  }
  return hh(() => e2.closeOnBack, () => {
    !function(e3, t3) {
      let n3, o3, r3 = false;
      function s3(e4) {
        var t4;
        null != (t4 = e4.state) && t4.replaced || (r3 = true, setTimeout(() => r3 = false));
      }
      wd && (un(() => {
        window.addEventListener("popstate", s3), n3 = null == e3 ? void 0 : e3.beforeEach((e4, n4, o4) => {
          lv ? r3 ? t3(o4) : o4() : setTimeout(() => r3 ? t3(o4) : o4()), lv = true;
        }), o3 = null == e3 ? void 0 : e3.afterEach(() => {
          lv = false;
        });
      }), ie(() => {
        window.removeEventListener("popstate", s3), null == n3 || n3(), null == o3 || o3();
      }));
    }(D2, (t3) => {
      d2.value && a2.value ? (t3(false), e2.persistent ? R2() : a2.value = false) : t3();
    });
  }), Oh(() => {
    var n3;
    return ba(bs, {}, [null == (n3 = o2.activator) ? void 0 : n3.call(o2, { isActive: Lt(a2), props: Us({ ref: h2, targetRef: g2 }, Lt(y2), e2.activatorProps) }), C2.value && ba(gs, { disabled: !Lt(l2), to: Lt(l2) }, [i2.value && ba("div", { ref: k2, ...t2, class: Cp("cds-overlay", Lt(N2), t2.class), style: Lt(M2), ...x2 }, [ba(km, { ref: S2, modelValue: Lt(a2) && !!e2.scrim, ...Lt(w2) }), ba(Zh, { appear: true, persisted: true, transition: e2.transition, onAfterEnter: P2, onAfterLeave: F2 }, { default: () => {
      var t3;
      return wo(ba("div", { ref: E2, class: ["cds-overlay__content", e2.contentClasses], style: [Lt(_2), Lt(I2)], ...Lt(b2), ...e2.contentProps }, null == (t3 = o2.default) ? void 0 : t3.call(o2, { isActive: a2 })), [[Ua, Lt(a2)], [Pg, { handler: V2, closeConditional: T2, include: () => [Lt(f2)] }]]);
    } })])])]);
  }), r2({ activatorEl: f2, contentEl: E2, globalTop: d2, localTop: u2, updateLocation: O2 }), {};
} }), zg = Lo({ name: "CdsMenu", props: { id: { type: String, default: void 0 }, ...Uh({ scrim: false }), ...om({ closeOnContentClick: true }), ...Gh({ openDelay: 300, closeDelay: 250 }), ...hm({ locationStrategy: "connected" }), ...Cm({ scrollStrategy: "reposition" }), ...Wh(), ...Xh(), ...zh(), ...Zv() }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const { themeClasses: r2 } = Yv(e2), s2 = vh(e2, "modelValue"), { scopeId: a2 } = av(), l2 = su(), i2 = ga(() => e2.id || `cds-menu-${l2}`), c2 = At(), d2 = Or(nm, null), u2 = At(0);
  function p2() {
    null == d2 || d2.closeParents();
  }
  Ir(nm, { register() {
    ++u2.value;
  }, unregister() {
    --u2.value;
  }, closeParents() {
    setTimeout(() => {
      u2.value || (s2.value = false, null == d2 || d2.closeParents());
    });
  } }), ho(s2, (e3) => {
    e3 ? null == d2 || d2.register() : null == d2 || d2.unregister();
  });
  const f2 = ga(() => Us({ "aria-haspopup": "menu", "aria-expanded": String(Lt(s2)), "aria-owns": Lt(i2) }, e2.activatorProps));
  return Oh(() => {
    const [n3] = Fd(e2, Object.keys(Kg.props));
    return ba(Kg, { ref: c2, class: Cp("cds-menu", Lt(r2)), ...n3, modelValue: Lt(s2), absolute: true, activatorProps: Lt(f2), "onClick:outside": p2, "onUpdate:modelValue": (e3) => {
      s2.value = e3;
    }, ...a2 }, { activator: t2.activator, default: t2.default });
  }), o2({ overlay: c2, openChildren: u2 }), {};
} }), Zg = "CdsLoader", Ug = Lo({ name: Zg, props: { fixed: { type: Boolean, default: false, description: "\u0417\u0430\u0444\u0438\u043A\u0441\u0438\u0440\u043E\u0432\u0430\u0442\u044C \u0437\u0430\u0433\u0440\u0443\u0437\u0447\u0438\u043A \u043E\u0442\u043D\u043E\u0441\u0438\u0442\u0435\u043B\u044C\u043D\u043E \u0442\u0435\u043A\u0443\u0449\u0435\u0433\u043E \u043A\u043E\u043D\u0442\u0435\u0439\u043D\u0435\u0440\u0430" }, overlay: { type: Boolean, default: false, description: "\u041E\u0442\u043E\u0431\u0440\u0430\u0436\u0430\u0442\u044C \u0444\u0438\u043A\u0441\u0438\u0440\u043E\u0432\u0430\u043D\u043D\u044B\u0439 \u043F\u043E\u043B\u043D\u043E\u044D\u043A\u0440\u0430\u043D\u043D\u044B\u0439 \u0437\u0430\u0433\u0440\u0443\u0437\u0447\u0438\u043A" }, description: { type: String, default: "\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430", description: "\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u043E\u043F\u0438\u0441\u0430\u043D\u0438\u0435, \u043A\u043E\u0442\u043E\u0440\u043E\u0435 \u043B\u0443\u0447\u0448\u0435 \u0432\u0441\u0435\u0433\u043E \u043E\u043F\u0438\u0441\u044B\u0432\u0430\u0435\u0442 \u0441\u043E\u0441\u0442\u043E\u044F\u043D\u0438\u0435 \u0437\u0430\u0433\u0440\u0443\u0437\u043A\u0438" }, ...Xf(), ...Zv() }, setup(e2, { attrs: t2 }) {
  const { themeClasses: n2 } = Yv(e2), { sizeClasses: o2 } = Jf(e2, Zg);
  return { attrs: t2, loaderClasses: ga(() => ({ "cds-loader__overlay": e2.overlay, "cds-loader__fixed": e2.fixed })), sizeClasses: o2, themeClasses: n2 };
} }), Wg = ["aria-valuetext", "aria-label"], Yg = [Ls("svg", { viewBox: "0 0 24 24" }, [Ls("circle", { cx: "12", cy: "12", r: "10.5" })], -1)];
const Gg = Tm(Ug, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("div", { class: U(["", [e2.loaderClasses, e2.themeClasses]]) }, [Ls("div", Us({ role: "progressbar", "aria-valuetext": e2.description, "aria-label": e2.description, class: ["cds-loader", e2.sizeClasses] }, e2.attrs), Yg, 16, Wg)], 2);
}]]), qg = "CdsTextInput", Xg = Lo({ name: qg, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm, CdsInputCounter: eg, CdsIcon: Lm, CdsLoader: Gg, CdsTransition: Zh }, inheritAttrs: false, props: { ...jh(), ...Nh(), ...Fh(), ...jf(), ...Kf(), ...mh(), ...Qf(), ...th(), ...Zf(), ...Xf(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "click:clear": (e2) => true, "click:prepend": (e2) => true, "click:append": (e2) => true, "click:prependInner": (e2) => true, "click:appendInner": (e2) => true, "click:control": (e2) => true, "mousedown:control": (e2) => true }, setup(e2, { attrs: t2, emit: n2, slots: o2, expose: r2 }) {
  const s2 = vh(e2, "modelValue"), { id: a2, hasLabel: l2, hasDescription: i2 } = Mh(e2, o2, qg), { hasPrepend: c2, hasAppend: d2, iconClasses: u2 } = zf(e2, o2, qg), { hasLoading: p2, loadingClasses: f2 } = Uf(e2), { isFocused: h2, focus: v2, blur: m2 } = gh(e2, qg), { hasPrependInnerIcon: g2, hasAppendInnerIcon: y2, fieldClasses: b2, fieldProps: w2, handleClickAppendInner: _2, handleClickPrependInner: C2 } = Kh(e2, o2, qg), { inputProps: x2 } = Ah(e2, [...w2, "modelValue", "focused", "loading", "theme"]), { rootAttrs: k2, inputAttrs: S2 } = Dh(t2), E2 = ga(() => e2.clearable), I2 = ga(() => {
    var e3;
    return (null != (e3 = Lt(s2)) ? e3 : "").toString().length;
  }), { themeClasses: O2 } = Yv(e2), N2 = At(), M2 = At(), A2 = At(), D2 = ga(() => ({ "cds-text-input--has-prefix": !!e2.prefix, "cds-text-input--has-suffix": !!e2.suffix }));
  function V2() {
    var e3;
    Lt(M2) !== document.activeElement && (null == (e3 = Lt(M2)) || e3.focus()), Lt(h2) || v2();
  }
  return ho(() => e2.value, (e3) => {
    e3 && (s2.value = e3);
  }, { immediate: true }), r2({ fieldRef: N2, inputRef: M2, validationRef: A2, focus: V2, blur: m2 }), { id: a2, model: s2, fieldRef: N2, inputRef: M2, validationRef: A2, isFocused: h2, rootAttrs: k2, inputAttrs: S2, inputProps: x2, hasLabel: l2, hasDescription: i2, hasPrepend: c2, hasAppend: d2, hasPrependInnerIcon: g2, hasAppendInnerIcon: y2, hasLoading: p2, hasClear: E2, counterValue: I2, iconClasses: u2, inputClasses: D2, themeClasses: O2, fieldClasses: b2, loadingClasses: f2, classesStates: Sd, onBlur: m2, onFocus: V2, onInput: function(e3) {
    s2.value = e3.target.value;
  }, onClear: function(e3) {
    e3.stopPropagation(), V2(), un(() => {
      s2.value = null, n2("click:clear", e3);
    });
  }, onControlClick: function(e3) {
    V2(), n2("click:control", e3);
  }, onControlMousedown: function(e3) {
    n2("mousedown:control", e3), e3.target !== Lt(M2) && (V2(), e3.preventDefault());
  }, handleClickPrependInner: C2, handleClickAppendInner: _2 };
} }), Jg = { class: "cds-text-input__input", "data-no-activator": "" }, Qg = ["id", "type", "value", "placeholder", "disabled", "readonly", "aria-disabled", "aria-invalid", "autofocus", "maxlength"], ey = { key: 2, class: "cds-text-input__clear" };
const ty = Tm(Xg, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-icon"), c2 = ro("cds-loader"), d2 = ro("cds-transition"), u2 = ro("cds-input"), p2 = ro("cds-input-counter");
  return Ss(), Ns("div", Us({ ref: "fieldRef", class: ["cds-text-field", e2.themeClasses] }, e2.rootAttrs), [e2.hasLabel ? (Ss(), Ms(a2, { key: 0, for: e2.id, text: e2.label }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["for", "text"])) : js("", true), e2.hasDescription ? (Ss(), Ms(l2, { key: 1, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true), Ps(u2, Us({ id: e2.id, ref: "validationRef", modelValue: e2.model, "onUpdate:modelValue": t2[8] || (t2[8] = (t3) => e2.model = t3), focused: e2.isFocused, class: ["cds-text-input", [e2.inputClasses, e2.fieldClasses, e2.iconClasses, e2.loadingClasses]] }, { ...e2.inputProps }, { "onClick:control": e2.onControlClick, "onMousedown:control": e2.onControlMousedown }), tr({ default: Wn(({ isDisabled: n3, isReadonly: o3, isValid: r3 }) => [e2.hasPrependInnerIcon ? (Ss(), Ns("div", { key: 0, class: "cds-text-input__prepend-inner", onClick: t2[0] || (t2[0] = (...t3) => e2.handleClickPrependInner && e2.handleClickPrependInner(...t3)) }, [nr(e2.$slots, "prepend-inner"), e2.prependInnerIcon ? (Ss(), Ms(i2, { key: 0, name: e2.prependInnerIcon, size: e2.size }, null, 8, ["name", "size"])) : js("", true)])) : js("", true), e2.prefix ? (Ss(), Ns("span", { key: 1, class: "cds-text-input__prefix", onClick: t2[1] || (t2[1] = (...t3) => e2.onFocus && e2.onFocus(...t3)) }, Q(e2.prefix), 1)) : js("", true), Ls("div", Jg, [nr(e2.$slots, "default"), Ls("input", Us({ id: e2.id, ref: "inputRef", type: e2.type, value: e2.model, placeholder: e2.placeholder, disabled: n3.value, readonly: o3.value || e2.loading, "aria-disabled": o3.value || n3.value, "aria-invalid": e2.error || false === r3.value, autofocus: e2.autofocus, maxlength: e2.limit ? +e2.limit : void 0 }, { ...e2.inputAttrs }, { onInput: t2[2] || (t2[2] = (...t3) => e2.onInput && e2.onInput(...t3)), onFocus: t2[3] || (t2[3] = (...t3) => e2.onFocus && e2.onFocus(...t3)), onBlur: t2[4] || (t2[4] = (...t3) => e2.onBlur && e2.onBlur(...t3)) }), null, 16, Qg)]), e2.hasClear && !e2.hasLoading ? (Ss(), Ns("div", ey, [Ls("button", { type: "button", title: "\u041E\u0447\u0438\u0441\u0442\u0438\u0442\u044C \u043F\u043E\u043B\u0435 \u0432\u0432\u043E\u0434\u0430", class: U(["cds-text-input__clear--button", { [e2.classesStates.active]: !!e2.model || e2.persistentClearable }]), onClick: t2[5] || (t2[5] = (...t3) => e2.onClear && e2.onClear(...t3)) }, [Ps(i2, { name: "remove", size: e2.size }, null, 8, ["size"])], 2)])) : js("", true), e2.suffix ? (Ss(), Ns("span", { key: 3, class: "cds-text-input__suffix", onClick: t2[6] || (t2[6] = (...t3) => e2.onFocus && e2.onFocus(...t3)) }, Q(e2.suffix), 1)) : js("", true), e2.hasAppendInnerIcon || e2.hasLoading ? (Ss(), Ns("div", { key: 4, class: "cds-text-input__append-inner", onClick: t2[7] || (t2[7] = (...t3) => e2.handleClickAppendInner && e2.handleClickAppendInner(...t3)) }, [e2.hasLoading ? (Ss(), Ms(d2, { key: 0, transition: "fade-transition", role: "alert", "aria-busy": "true" }, { default: Wn(() => [Ps(c2, { size: "xs", class: "cds-text-input__loader" })]), _: 1 })) : js("", true), nr(e2.$slots, "append-inner"), e2.appendInnerIcon ? (Ss(), Ms(i2, { key: 1, name: e2.appendInnerIcon, size: e2.size }, null, 8, ["name", "size"])) : js("", true)])) : js("", true)]), messages: Wn(() => [nr(e2.$slots, "messages")]), _: 2 }, [e2.hasPrepend ? { name: "prepend", fn: Wn(() => [nr(e2.$slots, "prepend")]), key: "0" } : void 0, e2.hasAppend ? { name: "append", fn: Wn(() => [nr(e2.$slots, "append")]), key: "1" } : void 0]), 1040, ["id", "modelValue", "focused", "class", "onClick:control", "onMousedown:control"]), e2.counter || e2.limit ? (Ss(), Ms(p2, { key: 2, current: e2.counterValue, max: e2.limit }, null, 8, ["current", "max"])) : js("", true)], 16);
}]]), ny = "CdsAutocomplete", oy = Lo({ name: ny, props: { search: { type: String, default: void 0 }, autoSelectFirst: { type: [Boolean, String], default: void 0 }, ...iv(), ...uv({ filterKeys: ["title"] }), ...jh({ role: "combobox" }), ...Nh(), ...Fh({ modelValue: null }), ...Ev(), ...jf(), ...Kf(), ...Wh(), ...Zf(), ...mh(), ...Qf(), ...th(), ...Xf(), ...hv(), ...dh({ itemChildren: false }), ...zh(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "update:menu": (e2) => true, "update:search": (e2) => true, "update:selectedAll": (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const { dropdown: r2 } = Iv(e2), { isFocused: s2 } = gh(e2, ny), { internalMenu: a2, menuRef: l2 } = vv(e2), { items: i2, transformIn: c2, transformOut: d2 } = uh(e2), { model: u2, selected: p2, selectStrategy: f2, selectedAll: h2, selectedIndeterminate: v2 } = cv(e2, i2, c2, d2), m2 = vh(e2, "search", ""), g2 = Lh(), y2 = Dt(true), b2 = Dt(false), w2 = Dt(-1), _2 = At(), C2 = At(), { onListKeydown: x2, onListScroll: k2 } = Qv(C2, _2), { filteredItems: S2, getMatches: E2 } = pv(e2, i2, () => Lt(y2) ? "" : Lt(m2)), I2 = ga(() => e2.hideSelected ? Lt(S2).filter((e3) => !Lt(u2).some((t3) => t3.value === e3.value)) : Lt(S2)), O2 = ga(() => {
    var t3;
    return (true === e2.autoSelectFirst || "exact" === e2.autoSelectFirst && Lt(m2) === (null == (t3 = Lt(I2)[0]) ? void 0 : t3.title)) && Lt(I2).length > 0 && !Lt(y2) && !Lt(b2);
  }), N2 = ga(() => e2.hideNoData && !Lt(i2).length || e2.readonly || Lt(null == g2 ? void 0 : g2.isReadonly));
  function M2(t3) {
    e2.openOnClear && (a2.value = true), m2.value = "";
  }
  function A2() {
    Lt(N2) || (a2.value = true);
  }
  function D2(e3) {
    Lt(N2) || (Lt(s2) && (e3.preventDefault(), e3.stopPropagation()), a2.value = !Lt(a2));
  }
  function V2(t3) {
    var n3, o3;
    if (e2.readonly || Lt(null == g2 ? void 0 : g2.isReadonly))
      return;
    const r3 = null == (n3 = Lt(_2)) ? void 0 : n3.selectionStart, s3 = Lt(u2).length;
    if ((Lt(w2) > -1 || [kd.enter, kd.down, kd.up].includes(t3.key)) && t3.preventDefault(), [kd.enter, kd.down].includes(t3.key) && (a2.value = true), [kd.esc].includes(t3.key) && (a2.value = false), Lt(O2) && [kd.enter, kd.tab].includes(t3.key) && H2(Lt(I2)[0]), t3.key === kd.down && (null == (o3 = Lt(C2)) || o3.focus("next")), e2.multiple) {
      if ([kd.backspace, kd.delete].includes(t3.key)) {
        if (Lt(w2) < 0)
          return void (t3.key === kd.backspace && !Lt(m2) && (w2.value = s3 - 1));
        const e3 = Lt(w2), n4 = Lt(u2)[Lt(w2)];
        n4 && H2(n4), w2.value = e3 >= s3 - 1 ? s3 - 2 : e3;
      }
      if (t3.key === kd.left) {
        if (Lt(w2) < 0 && r3 > 0)
          return;
        const e3 = Lt(w2) > -1 ? Lt(w2) - 1 : s3 - 1;
        Lt(u2)[e3] ? w2.value = e3 : w2.value = -1;
      }
      if (t3.key === kd.right) {
        if (Lt(w2) < 0)
          return;
        const e3 = Lt(w2) + 1;
        Lt(u2)[e3] ? w2.value = e3 : w2.value = -1;
      }
    }
  }
  function T2(e3) {
    m2.value = e3.target.value;
  }
  function L2(e3) {
    if (Jd(Lt(_2), ":autofill") || Jd(Lt(_2), ":-webkit-autofill")) {
      const t3 = Lt(i2).find((t4) => t4.title === e3.target.value);
      t3 && H2(t3);
    }
  }
  function P2() {
    var e3;
    Lt(s2) && (y2.value = true, null == (e3 = Lt(_2)) || e3.focus());
  }
  function F2(e3) {
    s2.value = true, setTimeout(() => {
      b2.value = true;
    });
  }
  function R2(e3) {
    b2.value = false;
  }
  function $2(t3) {
    (null == t3 || "" === t3 && !e2.multiple) && (u2.value = []);
  }
  const B2 = Dt(false);
  function H2(t3) {
    if (e2.multiple) {
      const e3 = Lt(u2).findIndex((e4) => Vd(e4.value, t3.value));
      if (-1 === e3)
        u2.value = [...Lt(u2), t3];
      else {
        const t4 = [...Lt(u2)];
        t4.splice(e3, 1), u2.value = t4;
      }
    } else
      u2.value = [t3], B2.value = true, m2.value = t3.title, a2.value = false, y2.value = true, un(() => B2.value = false);
  }
  function j2() {
    e2.readonly || e2.disabled || e2.multiple && (!h2.value || v2.value ? u2.value = [...i2.value] : u2.value = [], n2("update:selectedAll", !h2.value));
  }
  ho(s2, (t3, n3) => {
    var o3, r3;
    t3 !== n3 && (t3 ? (B2.value = true, m2.value = e2.multiple ? "" : String(null != (r3 = null == (o3 = Lt(u2).at(-1)) ? void 0 : o3.props.title) ? r3 : ""), y2.value = true, un(() => B2.value = false)) : (e2.multiple || Lt(m2) ? Lt(O2) && !Lt(b2) && !Lt(u2).some(({ value: e3 }) => e3 === Lt(I2)[0].value) && H2(Lt(I2)[0]) : u2.value = [], a2.value = false, m2.value = "", w2.value = -1));
  }), ho(m2, (e3) => {
    !Lt(s2) || Lt(B2) || (e3 && (a2.value = true), y2.value = !e3);
  }), ho(a2, () => {
    !e2.hideSelected && Lt(a2) && Lt(u2).length && (Lt(I2).findIndex((e3) => Lt(u2).some((t3) => e3.value === t3.value)), wd && window.requestAnimationFrame(() => {
    }));
  });
  const { inputProps: K2 } = dv(e2, ty.props, []), z2 = ga(() => !Lt(I2).length && !e2.hideNoData);
  return Oh(() => {
    const n3 = !(e2.hideNoData && !Lt(I2).length) || t2["prepend-item"] || t2["append-item"] || t2["no-data"], o3 = Lt(u2).length > 0;
    return ba(ty, { ref: _2, ...Lt(K2), modelValue: Lt(m2), "onUpdate:modelValue": $2, focused: Lt(s2), "onUpdate:focused": (e3) => s2.value = e3, validationValue: u2.externalValue, dirty: o3, persistentClearable: o3, class: Cp("cds-autocomplete", "cds-autocomplete--" + (e2.multiple ? "multiple" : "single"), { "cds-autocomplete--ellipsis": e2.ellipsis, "cds-autocomplete--selected": Lt(u2).length, "cds-autocomplete--active-menu": Lt(a2), "cds-autocomplete--selection-slot": !!t2.selection, "cds-autocomplete--selecting-index": Lt(w2) > -1 }), onInput: T2, onChange: L2, onKeydown: V2, "onClick:clear": M2, "onMousedown:control": A2 }, { ...t2, default: () => ba(bs, {}, [ba(zg, { ref: l2, modelValue: Lt(a2), "onUpdate:modelValue": (e3) => a2.value = e3, activator: "parent", maxHeight: e2.menuHeight, contentClasses: "cds-autocomplete__content", disabled: Lt(N2), eager: e2.eager, openOnClick: false, closeOnContentClick: false, transition: e2.transition, onAfterLeave: P2, ...e2.menuProps }, { default: () => n3 && ba(gg, { ref: C2, selected: Lt(p2), selectStrategy: Lt(f2), tabindex: "-1", onMousedown: (e3) => e3.preventDefault(), onKeydown: x2, onFocusin: F2, onFocusout: R2, onScrollPassive: k2, ...e2.listProps }, { default: () => {
      var n4, o4, s3, a3, l3;
      return ba(bs, {}, [null != (o4 = null == (n4 = t2["prepend-item"]) ? void 0 : n4.call(t2)) ? o4 : e2.multiple && e2.isSelectAll && ba(Sg, { isClickable: true, title: r2.selectAll, class: "cds-autocomplete__select-all", onClick: j2 }, { prepend: () => ba(dg, { modelValue: Lt(h2), indeterminate: Lt(v2), hideMessages: true, tabindex: "-1" }) }), Lt(z2) && (null != (a3 = null == (s3 = t2["no-data"]) ? void 0 : s3.call(t2)) ? a3 : ba(Sg, { title: r2.noData })), Lt(I2).map((n5, o5) => {
        var r3, s4;
        const a4 = Us(n5.props, { key: o5, active: !(!Lt(O2) || 0 !== o5) || void 0, onClick: () => H2(n5) });
        return null != (s4 = null == (r3 = t2.item) ? void 0 : r3.call(t2, { item: n5, index: o5, props: a4 })) ? s4 : ba(Sg, { ...a4 }, { prepend: ({ isSelected: t3 }) => ba(bs, {}, [e2.multiple && !e2.hideSelected ? ba(dg, { key: n5.value, modelValue: Lt(t3), hideMessages: true, tabindex: "-1" }) : void 0, n5.props.prependIcon && ba(Lm, { name: n5.props.prependIcon })]), title: () => {
          var e3, t3, o6;
          return ug({ name: "autocomplete", isPure: Lt(y2), item: n5, matches: null == (e3 = E2(n5)) ? void 0 : e3.title, length: null != (o6 = null == (t3 = Lt(m2)) ? void 0 : t3.length) ? o6 : 0 });
        } });
      }), null == (l3 = t2["append-item"]) ? void 0 : l3.call(t2)]);
    } }) }), Lt(u2).map((n4, o4) => {
      var r3, s3;
      return ba("div", { key: n4.value, class: Cp("cds-autocomplete__selection", { "cds-autocomplete__selection--selected": o4 === Lt(w2) }) }, null != (s3 = null == (r3 = t2.selection) ? void 0 : r3.call(t2, { item: n4, index: o4 })) ? s3 : ba("span", {}, [n4.title, e2.multiple && o4 < Lt(u2).length - 1 && ba("span", { class: "cds-autocomplete__selection--comma" }, ",")]));
    })]), "append-inner": () => {
      var o4;
      return [null == (o4 = t2["append-inner"]) ? void 0 : o4.call(t2), Lt(n3) && ba(Lm, { name: e2.menuIcon, onClick: eu, class: "cds-autocomplete__menu-icon", onMousedown: D2 })];
    } });
  }), o2({ isFocused: s2, isPure: y2, menu: a2, filteredItems: S2, select: H2, selectAll: j2, menuRef: l2, listRef: C2, textInputRef: _2 }), {};
} }), ry = "CdsBadge", sy = Lo({ name: ry, props: { count: { type: [Number, String], default: void 0 }, limited: { type: Boolean, default: true }, ...nh(), ...Zv() }, setup(e2) {
  const { themeClasses: t2 } = Yv(e2), { invertColorClasses: n2 } = oh(e2, ry), o2 = ga(() => {
    if (void 0 !== e2.count)
      return e2.limited ? e2.count < 99 && e2.count > -99 ? e2.count : e2.count < -99 ? "-99" : "99+" : e2.count;
  });
  return Oh(() => ba("span", { class: Cp("cds-badge", Lt(n2), Lt(t2)) }, Lt(o2))), {};
} }), ay = Lo({ name: "CdsLink", props: { ...Pf(), ...$f(), ...Qf(), ...Zv() }, emits: { click: (e2) => true }, setup(e2, { emit: t2 }) {
  const { themeClasses: n2 } = Yv(e2), { href: o2, to: r2, tag: s2, handleClick: a2 } = Bf(e2, t2), { disabledClasses: l2 } = eh(e2);
  return { tag: s2, to: r2, href: o2, disabledClasses: l2, themeClasses: n2, handleClick: a2 };
} });
const ly = Tm(ay, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ms(ao(e2.tag), { to: e2.to, href: e2.href, class: U([[e2.disabledClasses, e2.themeClasses], "cds-link"]), onClick: e2.handleClick }, { default: Wn(() => [Hs(Q(e2.text) + " ", 1), nr(e2.$slots, "default")]), _: 3 }, 8, ["to", "href", "class", "onClick"]);
}]]), iy = Lo({ name: "CdsBreadcrumbsItem", props: { currentPage: { type: Boolean, default: false }, ...Pf(), ...$f(), ...Zv() }, setup(e2, { slots: t2, emit: n2 }) {
  const { isLink: o2, href: r2, to: s2 } = Bf(e2, n2), a2 = ga(() => !e2.currentPage && Lt(o2)), l2 = ga(() => e2.text || "");
  return Oh(() => {
    var n3;
    return ba("li", { class: "cds-breadcrumbs__item" }, [Lt(a2) ? ba(ly, { text: Lt(l2), to: e2.to && Lt(s2), href: e2.href && Lt(r2), title: Lt(l2), theme: e2.theme }, { default: () => {
      var e3;
      return null == (e3 = t2.default) ? void 0 : e3.call(t2);
    } }) : ba("span", { title: Lt(l2) }, [Lt(l2), null == (n3 = t2.default) ? void 0 : n3.call(t2)])]);
  }), {};
} }), cy = Lo({ name: "CdsBreadcrumbs", props: { ...dh(), ...Zv() }, setup(e2, { slots: t2, expose: n2 }) {
  const { items: o2 } = uh(e2), r2 = ga(() => !!t2.default), s2 = (e3) => Lt(o2).length - 1 === e3;
  return Oh(() => {
    var n3;
    return ba("ui", { class: "cds-breadcrumbs" }, [Lt(r2) ? null == (n3 = t2.default) ? void 0 : n3.call(t2) : Lt(o2).map((t3, n4) => ba(iy, { key: n4, text: t3.title, to: t3.props.to, href: t3.props.href, theme: e2.theme, currentPage: s2(n4) }))]);
  }), n2({ items: o2, isLastItem: s2 }), {};
} }), dy = Symbol.for("cds:button-toggle"), uy = "CdsButton", py = Lo({ name: uy, components: { CdsLoader: Gg, CdsIcon: Lm }, props: { active: { type: Boolean, default: void 0 }, icon: { type: [String, Boolean], default: void 0, validator: (e2) => "boolean" == typeof e2 || Hf.includes(e2) }, ...Pf(), ...Yf(), ...Zv(), ...Xf(), ...jf(), ...Qf(), ...th(), ...$f(), ...Zf(), ...nh(), ...Nv(), symbol: { type: null, default: dy } }, emits: { "group:selected": (e2) => true }, setup(e2, { slots: t2, emit: n2, attrs: o2, expose: r2 }) {
  const { themeClasses: s2 } = Yv(e2), { appearanceClasses: a2 } = Gf(e2, uy), { sizeClasses: l2 } = Jf(e2, uy), { loadingClasses: i2 } = Uf(e2), { readonlyClasses: c2 } = function(e3) {
    return { readonlyClasses: ga(() => {
      const { readonly: t3 } = Lt(e3);
      return t3 ? Sd.readonly : void 0;
    }) };
  }(e2), { invertColorClasses: d2 } = oh(e2, uy), { isLink: u2, isActive: p2, tag: f2, href: h2, to: v2 } = Bf(e2, n2), { hasAppend: m2, hasPrepend: g2 } = zf(e2, t2, uy), y2 = Av(e2, e2.symbol, false), b2 = ga(() => !!t2.default), w2 = ga(() => Lt(null == y2 ? void 0 : y2.disabled) || e2.disabled), _2 = ga(() => {
    if (void 0 !== e2.value)
      return Object(e2.value) === e2.value ? JSON.stringify(e2.value, null, 0) : e2.value;
  }), C2 = ga(() => void 0 !== e2.active ? e2.active : Lt(u2) ? Lt(p2) : Lt(null == y2 ? void 0 : y2.isSelected)), x2 = ga(() => ({ "cds-button__icon": !!e2.icon })), k2 = ga(() => "boolean" != typeof e2.icon ? e2.icon : void 0), S2 = ga(() => Cp({ "cds-button--active": Lt(C2), [Sd.disabled]: Lt(w2) }, Lt(s2), Lt(a2), Lt(l2), Lt(i2), Lt(c2), Lt(d2), Lt(x2), Lt(null == y2 ? void 0 : y2.selectedClass)));
  return r2({ toggle: () => null == y2 ? void 0 : y2.toggle(), isDisabled: w2 }), { href: h2, to: v2, tag: Lt(u2) ? f2 : "button", type: Lt(u2) ? void 0 : "button", valueAttr: _2, internalIcon: k2, hasPrepend: g2, hasAppend: m2, hasDefaultSlot: b2, isDisabled: w2, classes: S2, onClick: function(e3) {
    Lt(w2) || Lt(u2) && (e3.metaKey || e3.ctrlKey || e3.shiftKey || 0 !== e3.button || "_blank" === o2.target) || null == y2 || y2.toggle();
  } };
} }), fy = { key: 0, class: "cds-button__prepend" }, hy = { key: 1, class: "cds-button__text", "data-no-activator": "" }, vy = { key: 2, class: "cds-button__append" };
const my = Tm(py, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-icon"), l2 = ro("cds-loader");
  return Ss(), Ms(ao(e2.tag), { type: e2.type, to: e2.to, href: e2.href, disabled: e2.isDisabled || e2.loading, value: e2.valueAttr, class: U([e2.classes, "cds-button"]), onClick: e2.onClick }, { default: Wn(() => [e2.hasPrepend || e2.icon ? (Ss(), Ns("span", fy, [e2.prependIcon || e2.internalIcon ? (Ss(), Ms(a2, { key: 0, name: e2.prependIcon || e2.internalIcon, size: e2.size }, null, 8, ["name", "size"])) : js("", true), nr(e2.$slots, "prepend")])) : js("", true), e2.hasDefaultSlot || e2.text ? (Ss(), Ns("span", hy, [e2.text ? (Ss(), Ns(bs, { key: 0 }, [Hs(Q(e2.text), 1)], 64)) : nr(e2.$slots, "default", { key: 1 })])) : js("", true), e2.hasAppend ? (Ss(), Ns("span", vy, [e2.appendIcon ? (Ss(), Ms(a2, { key: 0, name: e2.appendIcon, size: e2.size }, null, 8, ["name", "size"])) : js("", true), nr(e2.$slots, "append")])) : js("", true), e2.loading ? (Ss(), Ms(l2, { key: 3, size: "xs", fixed: "" })) : js("", true)]), _: 3 }, 8, ["type", "to", "href", "disabled", "value", "class", "onClick"]);
}]]), gy = "CdsTag", yy = Lo({ name: gy, components: { CdsIcon: Lm }, props: { removable: { type: Boolean, default: false, description: "\u0423\u0434\u0430\u043B\u044F\u0435\u043C\u044B\u0439 \u0442\u0435\u0433" }, ...Xf(), ...Qf(), ...$f(), ...sh(), ...Pf(), ...Zv() }, emits: { remove: (e2) => true }, setup(e2, { slots: t2, emit: n2 }) {
  const { themeClasses: o2 } = Yv(e2), { sizeClasses: r2 } = Jf(e2, gy), { disabledClasses: s2 } = eh(e2), { colorClasses: a2 } = function(e3, t3) {
    return { colorClasses: ga(() => {
      const { color: n3 } = e3;
      if (n3)
        return `${ku(t3)}__color--${n3}`;
    }) };
  }(e2, gy), { isLink: l2, tag: i2, href: c2, to: d2 } = Bf(e2, n2);
  !e2.text && !t2.default && _a(`[CDS] 'text' is a required prop for ${gy}`);
  const u2 = { sizeClasses: r2, disabledClasses: s2, colorClasses: a2, tagClasses: ga(() => ({ "cds-tag--interactive": Lt(l2), "cds-tag--removable": e2.removable })), themeClasses: o2 };
  return { tag: Lt(l2) ? i2 : "span", href: c2, to: d2, ...u2, onRemove: (e3) => {
    n2("remove", e3);
  } };
} }), by = { class: "cds-tag__text", tabindex: "-1" }, wy = ["disabled"];
const _y = Tm(yy, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-icon");
  return Ss(), Ms(ao(e2.tag), { to: e2.to, href: e2.href, class: U(["cds-tag", [e2.tagClasses, e2.sizeClasses, e2.disabledClasses, e2.colorClasses, e2.themeClasses]]) }, { default: Wn(() => [Ls("span", by, [e2.text ? (Ss(), Ns(bs, { key: 0 }, [Hs(Q(e2.text), 1)], 64)) : nr(e2.$slots, "text", { key: 1 })]), nr(e2.$slots, "default"), e2.removable ? (Ss(), Ns("button", { key: 0, "aria-label": "\u0423\u0434\u0430\u043B\u0438\u0442\u044C", type: "button", disabled: e2.disabled, class: "cds-tag__remove", onClick: t2[0] || (t2[0] = Il((...t3) => e2.onRemove && e2.onRemove(...t3), ["prevent"])), onKeydown: t2[1] || (t2[1] = Nl(Il((...t3) => e2.onRemove && e2.onRemove(...t3), ["prevent"]), ["enter"])) }, [Ps(a2, { class: "cds-tag__icon", size: e2.size, name: "remove" }, null, 8, ["size"])], 40, wy)) : js("", true)]), _: 3 }, 8, ["to", "href", "class"]);
}]]), Cy = "CdsCombobox", xy = Lo({ name: Cy, components: { CdsTextInput: ty, CdsMenu: zg, CdsList: gg, CdsListItem: Sg, CdsCheckbox: dg, CdsTag: _y, CdsIcon: Lm, CdsHighlight: ug }, props: { delimiters: { type: Array, default: () => [] }, autoSelectFirst: { type: [Boolean, String], default: void 0 }, ...iv({ hideNoData: true }), ...uv({ filterKeys: ["title"] }), ...jh({ role: "combobox" }), ...Nh(), ...Fh(), ...jf(), ...Kf(), ...Wh(), ...hv(), ...Ev(), ...mh(), ...Qf(), ...th(), ...Zf(), ...Xf(), ...dh({ returnObject: true }), ...zh(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "update:menu": (e2) => true, "update:search": (e2) => true, "update:selectedAll": (e2) => true }, setup(e2, { emit: t2, slots: n2, expose: o2 }) {
  var r2, s2;
  const { dropdown: a2 } = Iv(e2), { hasLabel: l2, hasDescription: i2 } = Mh(e2, n2, Cy), { hasPrependInnerIcon: c2 } = Kh(e2, n2, Cy), { hasAppend: d2, hasPrepend: u2 } = zf(e2, n2, Cy), { isFocused: p2, focus: f2 } = gh(e2, Cy), { items: h2, transformIn: v2, transformOut: m2 } = uh(e2), { internalMenu: g2, menuRef: y2 } = vv(e2), { model: b2, selections: w2, selected: _2, selectStrategy: C2, selectedAll: x2, selectedIndeterminate: k2 } = cv(e2, h2, v2, m2), S2 = Lh(), E2 = At(-1), I2 = Dt(true), O2 = Dt(false);
  let N2 = false;
  const M2 = At(e2.multiple ? "" : null != (s2 = null == (r2 = Lt(b2)[0]) ? void 0 : r2.title) ? s2 : ""), A2 = ga({ get: () => Lt(M2), set: (t3) => {
    var n3;
    if (M2.value = t3, e2.multiple || (b2.value = [ph(e2, t3)]), t3 && e2.multiple && (null == (n3 = e2.delimiters) ? void 0 : n3.length)) {
      const n4 = t3.split(new RegExp(`(?:${e2.delimiters.join("|")})+`));
      n4.length > 1 && (n4.forEach((t4) => {
        (t4 = t4.trim()) && G2(ph(e2, t4));
      }), M2.value = "");
    }
    t3 || (E2.value = -1), I2.value = !t3;
  } });
  ho(M2, (e3) => {
    N2 ? un(() => N2 = false) : Lt(p2) && !Lt(g2) && (g2.value = true), t2("update:search", e3);
  }), ho(b2, (t3) => {
    var n3, o3;
    e2.multiple || (M2.value = null != (o3 = null == (n3 = t3[0]) ? void 0 : n3.title) ? o3 : "");
  });
  const { filteredItems: D2, getMatches: V2 } = pv(e2, h2, () => Lt(I2) ? "" : Lt(A2)), T2 = ga(() => e2.hideSelected ? Lt(D2).filter((e3) => !Lt(w2).some((t3) => t3.value === e3.value)) : Lt(D2)), L2 = ga(() => {
    var t3;
    return (true === e2.autoSelectFirst || "exact" === e2.autoSelectFirst && Lt(A2) === (null == (t3 = Lt(T2)[0]) ? void 0 : t3.title)) && T2.value.length > 0 && !Lt(I2) && !O2.value;
  }), P2 = ga(() => !e2.hideNoData && !Lt(T2).length), F2 = ga(() => Lt(w2)[Lt(E2)]), R2 = ga(() => Lt(b2).length > 0), $2 = ga(() => b2.externalValue), B2 = ga(() => ({ "cds-combobox--active-menu": Lt(g2), "cds-combobox--selected": Lt(b2).length, "cds-combobox--selecting-index": Lt(E2) > -1, "cds-combobox--ellipsis": e2.ellipsis, ["cds-combobox--" + (e2.multiple ? "multiple" : "single")]: true })), { inputProps: H2 } = dv(e2, ty.props, ["modelValue", "validationValue", "focused", "appendInnerIcon", "clearable", "counter"]), j2 = At(), K2 = At(), { onListScroll: z2, onListKeydown: Z2 } = Qv(K2, j2), U2 = ga(() => !!(!e2.hideNoData || Lt(T2).length || n2.prepend || n2.append || n2["no-data"])), W2 = ga(() => !!n2.selection), Y2 = ga(() => e2.hideNoData && !Lt(h2).length || e2.readonly || Lt(null == S2 ? void 0 : S2.isReadonly));
  function G2(t3) {
    if (!e2.readonly && !e2.disabled)
      if (e2.multiple) {
        const e3 = Lt(_2).findIndex((e4) => Vd(e4, t3.value));
        if (-1 === e3)
          b2.value = [...Lt(b2), t3];
        else {
          const t4 = [...Lt(b2)];
          t4.splice(e3, 1), b2.value = t4;
        }
        A2.value = "";
      } else
        b2.value = [t3], M2.value = t3.title, un(() => {
          g2.value = false, I2.value = true;
        });
  }
  function q2() {
    e2.readonly || e2.disabled || e2.multiple && (!Lt(x2) || Lt(k2) ? b2.value = [...Lt(h2)] : b2.value = [], t2("update:selectedAll", !Lt(x2)));
  }
  return ho(D2, (t3) => {
    !t3.length && e2.hideNoData && (g2.value = false);
  }), ho(p2, (t3, n3) => {
    if (!t3 && t3 !== n3) {
      if (E2.value = -1, g2.value = false, Lt(L2) && !Lt(O2) && !Lt(b2).some(({ value: e3 }) => e3 === Lt(T2)[0].value))
        return void G2(Lt(T2)[0]);
      if (Lt(A2)) {
        if (e2.multiple)
          return void G2(ph(e2, Lt(A2)));
        if (!Lt(W2))
          return;
        Lt(b2).some(({ title: e3 }) => e3 === Lt(A2)) ? M2.value = "" : G2(ph(e2, Lt(A2)));
      }
    }
  }), ho(() => e2.items, (e3) => {
    !Lt(p2) || !e3.length || Lt(g2) || (g2.value = true);
  }), o2({ isFocused: p2, isPure: I2, menu: g2, filteredItems: D2, select: G2, selectAll: q2, menuRef: y2, listRef: K2, textInputRef: j2 }), { model: b2, search: A2, internalMenu: g2, menuDisabled: Y2, displayItems: T2, selected: _2, selectedAll: x2, selectStrategy: C2, selections: w2, selectionIndex: E2, selectedIndeterminate: k2, isFocused: p2, isDirty: R2, isPure: I2, inputRef: j2, menuRef: y2, listRef: K2, inputProps: H2, externalValidationValue: $2, hasList: U2, hasLabel: l2, hasDescription: i2, hasPrepend: u2, hasAppend: d2, hasPrependInnerIcon: c2, hasNoDataItem: P2, comboboxClasses: B2, comboboxLocale: a2, noop: eu, select: G2, selectAll: q2, mergeItemProps: function(e3) {
    return Us(e3.props, { onClick: () => G2(e3) });
  }, onClear: function() {
    N2 = true, e2.openOnClear && (g2.value = true);
  }, onKeydown: function(t3) {
    var n3, o3, r3, s3, a3;
    if (e2.readonly || Lt(null == S2 ? void 0 : S2.isReadonly))
      return;
    const l3 = null == (n3 = Lt(j2)) ? void 0 : n3.selectionStart, i3 = Lt(_2).length;
    if ((Lt(E2) > -1 || [kd.up, kd.down, kd.enter, kd.home, kd.end].includes(t3.key)) && t3.preventDefault(), [kd.enter, kd.down].includes(t3.key) && (g2.value = true), [kd.esc].includes(t3.key) && (g2.value = false), [kd.enter, kd.esc, kd.tab].includes(t3.key) && (I2.value = true), t3.key === kd.down ? null == (o3 = Lt(K2)) || o3.focus("next") : t3.key === kd.up ? null == (r3 = Lt(K2)) || r3.focus("prev") : t3.key === kd.home ? null == (s3 = Lt(K2)) || s3.focus("first") : t3.key === kd.end && (null == (a3 = Lt(K2)) || a3.focus("last")), e2.multiple) {
      if ([kd.backspace, kd.delete].includes(t3.key)) {
        if (Lt(E2) < 0)
          return void (t3.key === kd.backspace && !Lt(A2) && (E2.value = i3 - 1));
        const e3 = Lt(E2);
        Lt(F2) && G2(Lt(F2)), E2.value = e3 >= i3 - 1 ? i3 - 2 : e3;
      }
      if (t3.key === kd.left) {
        if (Lt(E2) < 0 && l3 > 0)
          return;
        const e3 = Lt(E2) > -1 ? Lt(E2) - 1 : i3 - 1;
        Lt(w2)[e3] ? E2.value = e3 : E2.value = -1;
      }
      if (t3.key === kd.right) {
        if (Lt(E2) < 0)
          return;
        const e3 = E2.value + 1;
        Lt(w2)[e3] ? E2.value = e3 : E2.value = -1;
      }
      t3.key === kd.enter && Lt(A2) && (G2(ph(e2, Lt(A2))), A2.value = "");
    }
  }, onFocusin: function() {
    f2(), setTimeout(() => {
      O2.value = true;
    });
  }, onFocusout: function(e3) {
    O2.value = false;
  }, onAfterLeave: function() {
    var e3;
    Lt(p2) && (I2.value = true, null == (e3 = Lt(j2)) || e3.focus());
  }, onMousedownControl: function() {
    Lt(Y2) || (g2.value = true);
  }, onMousedownMenuIcon: function(e3) {
    Lt(Y2) || (Lt(p2) && (e3.preventDefault(), e3.stopPropagation()), g2.value = !Lt(g2));
  }, onInputModelValue: function(t3) {
    (null == t3 || "" === t3 && !e2.multiple) && (b2.value = []);
  }, onListKeydown: Z2, onListScroll: z2, getMatches: V2 };
} }), ky = { class: "cds-combobox__selection--text" }, Sy = { key: 0, class: "cds-combobox__selection--comma" };
const Ey = Tm(xy, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-list-item"), l2 = ro("cds-checkbox"), i2 = ro("cds-highlight"), c2 = ro("cds-list"), d2 = ro("cds-menu"), u2 = ro("cds-tag"), p2 = ro("cds-icon"), f2 = ro("cds-text-input");
  return Ss(), Ms(f2, Us({ ref: "inputRef", modelValue: e2.search, "onUpdate:modelValue": t2[3] || (t2[3] = (t3) => e2.search = t3), focused: e2.isFocused, "onUpdate:focused": t2[4] || (t2[4] = (t3) => e2.isFocused = t3), "validation-value": e2.externalValidationValue, dirty: e2.isDirty, clearable: !e2.multiple && e2.clearable, counter: false, size: e2.size, class: [e2.comboboxClasses, "cds-combobox"] }, e2.inputProps, { onKeydown: e2.onKeydown, "onClick:clear": e2.onClear, "onUpdate:modelValue": e2.onInputModelValue, "onMousedown:control": e2.onMousedownControl }), tr({ "append-inner": Wn(() => [nr(e2.$slots, "append-inner"), e2.hasList ? (Ss(), Ms(p2, { key: 0, name: e2.menuIcon, class: "cds-combobox__menu-icon", onClick: e2.noop, onMousedown: e2.onMousedownMenuIcon }, null, 8, ["name", "onClick", "onMousedown"])) : js("", true)]), messages: Wn(() => [nr(e2.$slots, "messages")]), default: Wn(() => [nr(e2.$slots, "default", {}, () => [Ps(d2, Us({ ref: "menuRef", modelValue: e2.internalMenu, "onUpdate:modelValue": t2[2] || (t2[2] = (t3) => e2.internalMenu = t3), eager: e2.eager, "max-height": e2.menuHeight, disabled: e2.menuDisabled, "close-on-content-click": false, "open-on-click": false, transition: e2.transition, activator: "parent", "content-classes": "cds-combobox__content" }, e2.menuProps, { onAfterLeave: e2.onAfterLeave }), { default: Wn(() => [e2.hasList ? (Ss(), Ms(c2, Us({ key: 0, ref: "listRef", selected: e2.selected, "select-strategy": e2.selectStrategy, tabindex: "-1", "aria-live": "polite" }, e2.listProps, { onKeydown: e2.onListKeydown, onFocusin: e2.onFocusin, onFocusout: e2.onFocusout, onScrollPassive: e2.onListScroll, onMousedown: t2[1] || (t2[1] = (e3) => e3.preventDefault()) }), { default: Wn(() => [e2.hasNoDataItem ? nr(e2.$slots, "no-data", { key: 0 }, () => [Ps(a2, { title: e2.comboboxLocale.noData }, null, 8, ["title"])]) : js("", true), nr(e2.$slots, "prepend-item", {}, () => [e2.multiple && e2.isSelectAll ? (Ss(), Ms(a2, { key: 0, "is-clickable": "", title: e2.comboboxLocale.selectAll, class: "cds-combobox__select-all", onClick: e2.selectAll }, { prepend: Wn(() => [Ps(l2, { modelValue: e2.selectedAll, "onUpdate:modelValue": t2[0] || (t2[0] = (t3) => e2.selectedAll = t3), indeterminate: e2.selectedIndeterminate, tabindex: "-1", "hide-messages": "" }, null, 8, ["modelValue", "indeterminate"])]), _: 1 }, 8, ["title", "onClick"])) : js("", true)]), (Ss(true), Ns(bs, null, er(e2.displayItems, (t3, n3) => nr(e2.$slots, "item", W(Us({ key: n3 }, { item: t3, index: n3, props: e2.mergeItemProps(t3) })), () => [Ps(a2, Us({ disabled: e2.readonly }, t3.props, { onClick: (n4) => e2.select(t3) }), tr({ title: Wn(() => {
    var n4, o3;
    return [Ps(i2, { "is-pure": e2.isPure, item: t3, matches: null == (n4 = e2.getMatches(t3)) ? void 0 : n4.title, length: null != (o3 = e2.search.length) ? o3 : 0, name: "combobox" }, null, 8, ["is-pure", "item", "matches", "length"])];
  }), _: 2 }, [e2.multiple && !e2.hideSelected ? { name: "prepend", fn: Wn(({ isSelected: e3 }) => [Ps(l2, { "model-value": e3, tabindex: "-1", "hide-messages": "" }, null, 8, ["model-value"])]), key: "0" } : void 0]), 1040, ["disabled", "onClick"])])), 128)), nr(e2.$slots, "append-item")]), _: 3 }, 16, ["selected", "select-strategy", "onKeydown", "onFocusin", "onFocusout", "onScrollPassive"])) : js("", true)]), _: 3 }, 16, ["modelValue", "eager", "max-height", "disabled", "transition", "onAfterLeave"]), (Ss(true), Ns(bs, null, er(e2.selections, (t3, n3) => (Ss(), Ns("div", { key: t3.value, class: U(["cds-combobox__selection", { "cds-combobox__selection--selected": n3 === e2.selectionIndex }]) }, [nr(e2.$slots, "selection", W(Rs({ item: t3, index: n3 })), () => [Ls("span", ky, Q(t3.title), 1), e2.multiple && n3 < e2.selections.length - 1 ? (Ss(), Ns("span", Sy, ",")) : js("", true)])], 2))), 128))]), e2.hasAppend ? nr(e2.$slots, "append", { key: 0 }) : js("", true)]), _: 2 }, [e2.hasLabel ? { name: "label", fn: Wn(() => [nr(e2.$slots, "label")]), key: "0" } : void 0, e2.hasDescription ? { name: "description", fn: Wn(() => [nr(e2.$slots, "description")]), key: "1" } : void 0, e2.hasPrepend ? { name: "prepend", fn: Wn(() => [nr(e2.$slots, "prepend")]), key: "2" } : void 0, e2.hasPrependInnerIcon || e2.counter && e2.multiple ? { name: "prepend-inner", fn: Wn(() => [e2.selected.length > 0 ? (Ss(), Ms(u2, { key: 0, text: `${e2.selected.length}` }, null, 8, ["text"])) : js("", true), nr(e2.$slots, "prepend-inner")]), key: "3" } : void 0]), 1040, ["modelValue", "focused", "validation-value", "dirty", "clearable", "size", "class", "onKeydown", "onClick:clear", "onUpdate:modelValue", "onMousedown:control"]);
}]]), Iy = Symbol.for("cds:container"), Oy = Symbol.for("cds:container-group"), Ny = "CdsContainer", My = Lo({ name: Ny, directives: { Touch: $g }, props: { onlySingle: { type: Boolean, default: false }, showArrows: { type: [Boolean, String], validator: (e2) => "boolean" == typeof e2 || "hover" === e2 }, continuous: { type: Boolean, default: false }, touch: { type: [Boolean, Object], default: void 0 }, ...Ov(), ...Yf(), ...$h(), ...Ev(), ...ch(), ...Qf(), ...Zv(), ...jf({ prependIcon: "arrow-left", appendIcon: "arrow-right" }) }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, expose: n2 }) {
  const o2 = Dv(e2, Oy), { themeClasses: r2 } = Yv(e2), { appearanceClasses: s2 } = Gf(e2, Ny), { disabledClasses: a2 } = eh(e2), { directionClasses: l2 } = Bh(e2, Ny), { container: i2 } = Iv(e2), { onlySingle: c2, multiple: d2 } = $t(e2), u2 = At(), p2 = Dt(false), f2 = ga(() => `cds-container-${"vertical" === e2.direction ? "y" : "x"}${Lt(p2) ? "-reverse" : ""}-transition`), h2 = Dt(0), v2 = At(void 0), m2 = ga(() => Lt(o2.items).findIndex((e3) => Lt(o2.selected).includes(e3.id)));
  ho(m2, (e3, t3) => {
    const n3 = Lt(o2.items).length, r3 = n3 - 1;
    p2.value = n3 <= 2 ? e3 < t3 : e3 === r3 && 0 === t3 || (0 !== e3 || t3 !== r3) && e3 < t3;
  }), ((e3) => {
    Ir(Iy, e3);
  })({ transition: f2, rootRef: u2, onlySingle: c2, multiple: d2, isReversed: p2, transitionCount: h2, transitionHeight: v2 });
  const g2 = ga(() => e2.onlySingle && false !== e2.showArrows), y2 = ga(() => e2.continuous || 0 !== Lt(m2)), b2 = ga(() => e2.continuous || Lt(m2) !== Lt(o2.items).length - 1);
  function w2() {
    Lt(y2) && o2.prev();
  }
  function _2() {
    Lt(b2) && o2.next();
  }
  const C2 = ga(() => {
    const n3 = [], r3 = { icon: e2.prependIcon, class: "cds-container__arrow-left", onClick: o2.prev, ariaLabel: i2.prev };
    n3.push(Lt(y2) ? t2.prev ? t2.prev({ props: r3 }) : ba(my, { appearance: "transparent", ...r3 }) : ba("div"));
    const s3 = { icon: e2.appendIcon, class: "cds-container__arrow-right", onClick: o2.next, ariaLabel: i2.next };
    return n3.push(b2.value ? t2.next ? t2.next({ props: s3 }) : ba(my, { appearance: "transparent", ...s3 }) : ba("div")), n3;
  }), x2 = ga(() => false === e2.touch ? e2.touch : { left: _2, right: w2, start: ({ originalEvent: e3 }) => {
    e3.stopPropagation();
  }, ...true === e2.touch ? {} : e2.touch });
  Oh(() => {
    var n3;
    return ba(e2.tag, { ref: u2, class: Cp("cds-container", Lt(s2), Lt(l2), Lt(a2), Lt(r2), { "cds-container--show-arrows-on-hover": "hover" === e2.showArrows, "cds-container--only-single": e2.onlySingle }) }, wo(ba("div", { class: "cds-container__container" }, [null == (n3 = t2.default) ? void 0 : n3.call(t2, o2), Lt(g2) && ba("div", { class: "cds-container__arrows" }, Lt(C2))]), [[$g, Lt(x2)]]));
  }), n2({ group: o2, next: _2, prev: w2 });
} }), Ay = Lo({ name: "CdsContainerItem", props: { reverseTransition: { type: [Boolean, String], default: void 0 }, transition: { type: [Boolean, String], default: void 0 }, ...Xh(), ...Nv(), ...Wh(), ...Qf() }, emits: { "group:selected": (e2) => true }, setup(e2, { slots: t2 }) {
  const n2 = Or(Iy, {}), o2 = Av(e2, Oy), { disabledClasses: r2 } = eh(e2), { isBooted: s2 } = function() {
    const e3 = Dt(false);
    return Zo(() => {
      window.requestAnimationFrame(() => {
        e3.value = true;
      });
    }), { ssrBootStyles: ga(() => e3.value ? void 0 : { transition: "none !important" }), isBooted: mt(e3) };
  }(), { dimensionsStyles: a2 } = Jh(e2);
  if (!n2 || !o2)
    throw new Error("CdsContainerItem must be used inside a CdsContainer");
  const l2 = ga(() => !n2.onlySingle.value && !e2.disabled && !!o2.value.value), i2 = ga(() => n2.multiple.value ? o2.isSelected.value ? "checkbox-fill" : "checkbox" : o2.isSelected.value ? "check-fill" : "check"), c2 = Dt(false), d2 = ga(() => s2.value && (n2.isReversed.value ? false !== e2.reverseTransition : false !== e2.transition));
  function u2() {
    !c2.value || !n2 || (c2.value = false, n2.transitionCount.value > 0 && (n2.transitionCount.value -= 1, 0 === n2.transitionCount.value && (n2.transitionHeight.value = void 0)));
  }
  function p2() {
    var e3;
    c2.value || !n2 || (c2.value = true, 0 === n2.transitionCount.value && (n2.transitionHeight.value = Bd(null == (e3 = n2.rootRef.value) ? void 0 : e3.clientHeight)), n2.transitionCount.value += 1);
  }
  function f2() {
    u2();
  }
  function h2(e3) {
    !c2.value || un(() => {
      !d2.value || !c2.value || !n2 || (n2.transitionHeight.value = Bd(e3.clientHeight));
    });
  }
  const v2 = ga(() => {
    const t3 = n2.isReversed.value ? e2.reverseTransition : e2.transition;
    return !!d2.value && { name: "string" != typeof t3 ? n2.transition.value : t3, onBeforeEnter: p2, onAfterEnter: u2, onEnterCancelled: f2, onBeforeLeave: p2, onAfterLeave: u2, onLeaveCancelled: f2, onEnter: h2 };
  }), { hasContent: m2 } = Yh(e2, o2.isSelected);
  return Oh(() => ba(Zh, { transition: v2.value, disabled: !s2.value }, { default: () => {
    var e3, s3;
    return wo(ba("div", { tabindex: l2.value ? 0 : void 0, class: Cp("cds-container__item", r2.value, o2.selectedClass.value, { "cds-container__item--selectable": l2.value, "cds-container__item--selected": o2.isSelected.value }), style: Lt(a2), onClick: () => !n2.onlySingle.value && o2.select(!o2.isSelected.value) }, [l2.value && ba(Lm, { name: i2.value, class: "cds-container__item-icon" }), n2.onlySingle.value ? m2.value && (null == (e3 = t2.default) ? void 0 : e3.call(t2)) : null == (s3 = t2.default) ? void 0 : s3.call(t2)]), [[Ua, !n2.onlySingle.value || o2.isSelected.value]]);
  } })), {};
} });
function Dy(e2) {
  const t2 = Math.abs(e2);
  return Math.sign(e2) * (t2 / ((1 / 0.501 - 2) * (1 - t2) + 1));
}
function Vy({ selectedElement: e2, containerSize: t2, contentSize: n2, currentScrollOffset: o2, isHorizontal: r2 }) {
  const s2 = r2 ? e2.clientWidth : e2.clientHeight, a2 = r2 ? e2.offsetLeft : e2.offsetTop, l2 = t2 + o2, i2 = s2 + a2, c2 = 0.4 * s2;
  return a2 <= o2 ? o2 = Math.max(a2 - c2, 0) : l2 <= i2 && (o2 = Math.min(o2 - (l2 - i2 - c2), n2 - t2)), o2;
}
const Ty = Symbol.for("cds:slide-group"), Ly = Lo({ name: "CdsSlideGroup", props: { ...ch(), ...$h(), ...Qf(), ...Mv(), ...Ov({ selectedClass: "cds-slide-group-item--active" }), ...jf({ prependIcon: "arrow-left", appendIcon: "arrow-right" }), symbol: { type: null, default: Ty } }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2 }) {
  const { mobile: n2 } = nv(), o2 = Dv(e2, e2.symbol), r2 = Dt(false), s2 = Dt(0), a2 = Dt(0), l2 = Dt(0), i2 = ga(() => "horizontal" === e2.direction), { resizeRef: c2, contentRect: d2 } = bv(), { resizeRef: u2, contentRect: p2 } = bv(), f2 = ga(() => Lt(o2.selected).length ? Lt(o2.items).findIndex((e3) => e3.id === Lt(o2.selected)[0]) : -1), h2 = ga(() => Lt(o2.selected).length ? Lt(o2.items).findIndex((e3) => e3.id === Lt(o2.selected)[Lt(o2.selected).length - 1]) : -1);
  if (wd) {
    let t3 = -1;
    ho(() => [Lt(o2.selected), Lt(d2), Lt(p2), Lt(i2)], () => {
      cancelAnimationFrame(t3), t3 = requestAnimationFrame(() => {
        var t4;
        if (Lt(d2) && Lt(p2)) {
          const e3 = Lt(i2) ? "width" : "height";
          a2.value = Lt(d2)[e3], l2.value = Lt(p2)[e3], r2.value = Lt(a2) + 1 < Lt(l2);
        }
        if (Lt(f2) >= 0 && Lt(u2)) {
          const n3 = null == (t4 = Lt(u2)) ? void 0 : t4.children[Lt(h2)];
          0 !== Lt(f2) && Lt(r2) ? e2.centerActive ? s2.value = function({ selectedElement: e3, containerSize: t5, contentSize: n4, isHorizontal: o3 }) {
            const r3 = o3 ? e3.clientWidth : e3.clientHeight, s3 = (o3 ? e3.offsetLeft : e3.offsetTop) + r3 / 2 - t5 / 2;
            return Math.min(n4 - t5, Math.max(0, s3));
          }({ selectedElement: n3, containerSize: Lt(a2), contentSize: Lt(l2), isHorizontal: Lt(i2) }) : Lt(r2) && (s2.value = Vy({ selectedElement: n3, containerSize: Lt(a2), contentSize: Lt(l2), currentScrollOffset: Lt(s2), isHorizontal: Lt(i2) })) : s2.value = 0;
        }
      });
    });
  }
  const v2 = Dt(false);
  let m2 = 0, g2 = 0;
  function y2(e3) {
    const t3 = Lt(i2) ? "clientX" : "clientY";
    g2 = 1 * Lt(s2), m2 = e3.touches[0][t3], v2.value = true;
  }
  function b2(e3) {
    if (!Lt(r2))
      return;
    const t3 = Lt(i2) ? "clientX" : "clientY";
    s2.value = 1 * (g2 + m2 - e3.touches[0][t3]);
  }
  function w2(e3) {
    const t3 = Lt(l2) - Lt(a2);
    Lt(s2) < 0 || !Lt(r2) ? s2.value = 0 : Lt(s2) >= t3 && (s2.value = t3), v2.value = false;
  }
  function _2() {
    !c2.value || (c2.value[Lt(i2) ? "scrollLeft" : "scrollTop"] = 0);
  }
  const C2 = Dt(false);
  function x2(e3) {
    if (C2.value = true, Lt(r2) && Lt(u2)) {
      for (const t3 of e3.composedPath())
        for (const e4 of Lt(u2).children)
          if (e4 === t3)
            return void (s2.value = Vy({ selectedElement: e4, containerSize: Lt(a2), contentSize: Lt(l2), currentScrollOffset: Lt(s2), isHorizontal: Lt(i2) }));
    }
  }
  function k2(e3) {
    C2.value = false;
  }
  function S2(e3) {
    var t3;
    !Lt(C2) && (!e3.relatedTarget || !(null == (t3 = Lt(u2)) ? void 0 : t3.contains(e3.relatedTarget))) && I2();
  }
  function E2(e3) {
    Lt(c2) || (Lt(i2) ? e3.key === kd.right ? I2("next") : e3.key === kd.left && I2("prev") : e3.key === kd.down ? I2("next") : e3.key === kd.up && I2("prev"));
  }
  function I2(e3) {
    var t3, n3, o3, r3, s3;
    if (Lt(u2))
      if (e3)
        if ("next" === e3) {
          const e4 = null == (n3 = Lt(u2).querySelector(":focus")) ? void 0 : n3.nextElementSibling;
          e4 ? e4.focus() : I2("first");
        } else if ("prev" === e3) {
          const e4 = null == (o3 = Lt(u2).querySelector(":focus")) ? void 0 : o3.previousElementSibling;
          e4 ? e4.focus() : I2("last");
        } else
          "first" === e3 ? null == (r3 = Lt(c2).firstElementChild) || r3.focus() : "last" === e3 && (null == (s3 = Lt(c2).lastElementChild) || s3.focus());
      else
        null == (t3 = Xd(Lt(u2))[0]) || t3.focus();
  }
  function O2(e3) {
    const t3 = Lt(s2) + ("prev" === e3 ? -1 : 1) * Lt(a2);
    s2.value = jd(t3, 0, Lt(l2) - Lt(a2));
  }
  const N2 = ga(() => {
    let e3 = Lt(s2) > Lt(l2) - Lt(a2) ? -(Lt(l2) - Lt(a2)) + Dy(Lt(l2) - Lt(a2) - Lt(s2)) : -Lt(s2);
    Lt(s2) <= 0 && (e3 = Dy(-Lt(s2)));
    return { transform: `translate${Lt(i2) ? "X" : "Y"}(${1 * e3}px)`, transition: Lt(v2) ? "none" : "", willChange: Lt(v2) ? "transform" : "" };
  }), M2 = ga(() => ({ next: o2.next, prev: o2.prev, select: o2.select, isSelected: o2.isSelected })), A2 = ga(() => {
    switch (e2.showArrows) {
      case "always":
        return true;
      case "desktop":
        return !Lt(n2);
      case true:
        return Lt(r2) || Math.abs(Lt(s2)) > 0;
      case "mobile":
        return Lt(n2) || Lt(r2) || Math.abs(Lt(s2)) > 0;
      default:
        return !Lt(n2) && (Lt(r2) || Math.abs(Lt(s2)) > 0);
    }
  }), D2 = ga(() => Math.abs(Lt(s2)) > 0), V2 = ga(() => Lt(l2) > Math.abs(Lt(s2) + Lt(a2)));
  return Oh(() => {
    var n3, s3, a3, l3, d3;
    return ba(e2.tag, { class: Cp("cds-slide-group", { "cds-slide-group--vertical": !Lt(i2), "cds-slide-group--has-affixes": Lt(A2), "cds-slide-group--is-overflowing": Lt(r2) }), tabindex: Lt(C2) || Lt(o2.selected).length ? -1 : 0, onFocus: S2 }, [Lt(A2) && ba("div", { key: "prev", class: Cp("cds-slide-group__prev", { "cds-slide-group__prev--disabled": !Lt(D2) }), onClick: () => O2("prev") }, null != (s3 = null == (n3 = t2.prev) ? void 0 : n3.call(t2, Lt(M2))) ? s3 : ba(zm, null, { default: () => ba(Lm, { name: e2.prependIcon }) })), ba("div", { key: "container", ref: c2, class: "cds-slide-group__container", onScroll: _2 }, ba("div", { ref: u2, class: "cds-slide-group__content", style: Lt(N2), onTouchstartPassive: y2, onTouchmovePassive: b2, onTouchendPassive: w2, onFocusin: x2, onFocusout: k2, onKeydown: E2 }, null == (a3 = t2.default) ? void 0 : a3.call(t2, Lt(M2)))), Lt(A2) && ba("div", { key: "next", class: Cp("cds-slide-group__next", { "cds-slide-group__next--disabled": !Lt(V2) }), onClick: () => O2("next") }, null != (d3 = null == (l3 = t2.next) ? void 0 : l3.call(t2, Lt(M2))) ? d3 : ba(zm, null, { default: () => ba(Lm, { name: e2.appendIcon }) }))]);
  }), { selected: o2.selected, scrollTo: O2, scrollOffset: s2, focus: I2 };
} }), Py = Lo({ name: "CdsSlideGroupItem", props: { ...Nv(), ...Qf() }, emits: { "group:selected": (e2) => true }, setup(e2, { slots: t2 }) {
  const n2 = Av(e2, Ty);
  return () => {
    var e3;
    return null == (e3 = t2.default) ? void 0 : e3.call(t2, { isSelected: Lt(n2.isSelected), select: n2.select, toggle: n2.toggle, selectedClass: Lt(n2.selectedClass) });
  };
} }), Fy = Symbol("cds:tabs");
const Ry = "CdsTabs", $y = Lo({ name: Ry, props: { items: { type: Array, default: () => [] }, grow: { type: Boolean, default: void 0 }, height: { type: [Number, String], default: 40 }, stacked: { type: Boolean, default: void 0 }, ...ch(), ...Lf(), ...$h(), ...Qf(), ...Mv(), ...nh(), ...Zv(), ...Ov({ required: "force" }), ...jf({ prependIcon: "chevron-left", appendIcon: "chevron-right" }) }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2 }) {
  const o2 = vh(e2, "modelValue"), r2 = ga(() => function(e3) {
    return e3 ? e3.map((e4) => "string" == typeof e4 ? { title: e4, value: e4 } : e4) : [];
  }(e2.items)), { themeClasses: s2 } = Yv(e2), { invertColorClasses: a2 } = oh(e2, Ry);
  return Oh(() => {
    const n3 = Gd(e2, ["align", "grow", "height", "stacked", "items"]);
    return ba(Ly, { ...n3, modelValue: Lt(o2), role: "tablist", class: Cp("cds-tabs", `cds-tabs--${e2.direction}`, `cds-tabs--align-${e2.align}`, { "cds-tabs--grow": e2.grow, "cds-tabs--stacked": e2.stacked }, Lt(a2), Lt(s2)), style: { height: Bd(e2.height) }, symbol: Fy, "onUpdate:modelValue": (e3) => {
      o2.value = e3;
    } }, { default: () => {
      var n4;
      return t2.default ? null == (n4 = t2.default) ? void 0 : n4.call(t2) : Lt(r2).map((t3) => ba(By, { ...t3, key: t3.title, invertColor: e2.invertColor }));
    } });
  }), {};
} }), By = Lo({ name: "CdsTab", props: { hideSlider: { type: Boolean, default: false }, ...Nv({ selectedClass: "cds-tab--selected" }), ...Pf(), ...$h(), ...jf(), ...Xf(), ...Qf(), ...th(), ...Zf() }, emits: { "group:selected": (e2) => true }, setup(e2, { slots: t2, attrs: n2, expose: o2 }) {
  const r2 = ga(() => "horizontal" === e2.direction), s2 = Dt(false), a2 = At(), l2 = At();
  function i2({ value: e3 }) {
    var t3, n3;
    if (s2.value = e3, e3) {
      const e4 = null == (n3 = null == (t3 = Lt(a2)) ? void 0 : t3.$el.parentElement) ? void 0 : n3.querySelector(".cds-tab--selected .cds-tab__slider"), o3 = Lt(l2);
      if (!e4 || !o3)
        return;
      const s3 = getComputedStyle(e4).color, i3 = e4.getBoundingClientRect(), c2 = o3.getBoundingClientRect(), d2 = Lt(r2) ? "x" : "y", u2 = Lt(r2) ? "X" : "Y", p2 = Lt(r2) ? "right" : "bottom", f2 = Lt(r2) ? "width" : "height", h2 = i3[d2] > c2[d2] ? i3[p2] - c2[p2] : i3[d2] - c2[d2], v2 = Math.sign(h2) > 0 ? Lt(r2) ? "right" : "bottom" : Math.sign(h2) < 0 ? Lt(r2) ? "left" : "top" : "center", m2 = (Math.abs(h2) + (Math.sign(h2) < 0 ? i3[f2] : c2[f2])) / Math.max(i3[f2], c2[f2]), g2 = 1.5;
      fp(o3, { backgroundColor: [s3, ""], transform: [`translate${u2}(${h2}px) scale${u2}(${i3[f2] / c2[f2]})`, `translate${u2}(${h2 / g2}px) scale${u2}(${(m2 - 1) / g2 + 1})`, ""], transformOrigin: Array(3).fill(v2) }, { duration: 225, easing: "cubic-bezier(0.45, 0, 0.55, 1)" });
    }
  }
  return Oh(() => {
    const o3 = Gd(e2, ["direction", "hideSlider"]);
    return ba(my, { ref: a2, symbol: Fy, active: false, appearance: "transparent", role: "tab", class: "cds-tab", tabindex: Lt(s2) ? 0 : -1, ariaSelected: String(Lt(s2)), ...o3, ...n2, "onGroup:selected": i2 }, { default: () => {
      var n3, o4;
      return [null != (o4 = null == (n3 = t2.default) ? void 0 : n3.call(t2)) ? o4 : e2.text, !e2.hideSlider && ba("div", { ref: l2, class: "cds-tab__slider" })];
    } });
  }), o2({ updateSlider: i2 }), {};
} }), Hy = Lo({ name: "CdsContentSwitcher", props: { modelValue: { type: null, default: void 0 }, grow: { type: Boolean, default: true }, ...Qf(), ...Zv() }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2 }) {
  const { themeClasses: o2 } = Yv(e2), { modelValue: r2 } = $t(e2);
  return Oh(() => ba($y, { modelValue: Lt(r2), disabled: e2.disabled, height: 32, grow: e2.grow, class: Cp("cds-content-switcher", Lt(o2)), "onUpdate:modelValue": (e3) => {
    n2("update:modelValue", e3);
  } }, () => {
    var e3;
    return null == (e3 = t2.default) ? void 0 : e3.call(t2);
  })), {};
} }), jy = Lo({ name: "CdsContentSwitcherItem", props: { ...Nv(), ...Qf(), ...th(), ...Zf(), ...jf(), ...Pf() }, emits: {}, setup(e2, { slots: t2 }) {
  const n2 = At();
  return Oh(() => ba(By, { ref: n2, readonly: e2.readonly, disabled: e2.disabled, prependIcon: e2.prependIcon, appendIcon: e2.appendIcon, loading: e2.loading, value: e2.value, selectedClass: Cp("cds-content-switcher__item--selected", e2.selectedClass), class: "cds-tab cds-content-switcher__item" }, () => {
    var n3, o2;
    return null != (o2 = null == (n3 = t2.default) ? void 0 : n3.call(t2)) ? o2 : e2.text;
  })), { tabRef: n2 };
} }), Ky = 20, zy = " \u2014 ", Zy = ["Calendar", "Years", "Months"], Uy = (e2) => /^-?[\d]+\/[0-1]\d$/.test(e2);
function Wy(e2) {
  return e2.year + "/" + Wd(e2.month);
}
const Yy = Lo({ name: "CdsDatePicker", props: { ...tu({ title: { type: String, default: void 0 }, description: { type: String, default: void 0 }, header: { type: Boolean, default: false }, todayBtn: { type: Boolean, default: false }, defaultYearMonth: { type: [String], default: void 0, validator: Uy }, view: { type: String, default: "Calendar", validator: (e2) => Zy.includes(e2) }, multiple: { type: Boolean, default: false }, range: { type: Boolean, default: false }, firstDayOfWeek: { type: [String, Number], default: 1 }, allowedDates: { type: [Array, Function] }, minYearMonth: { type: String, default: void 0, validator: Uy }, maxYearMonth: { type: String, default: void 0, validator: Uy }, noUnset: { type: Boolean, default: false }, events: { type: [Array, Function] }, eventsColor: { type: String, default: "green" }, emitImmediately: { type: Boolean, default: false }, hideSiblingMonth: { type: Boolean, default: false } }, "calendar")(), ...mv(), ...Qf(), ...th(), ...Ev(), ...Zv() }, emits: { "update:modelValue": (...e2) => true, navigation: ({ year: e2, month: t2 }) => true, rangeStart: (e2) => true, rangeEnd: ({ from: e2, to: t2 }) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const { themeClasses: r2 } = Yv(e2), s2 = vh(e2, "modelValue", [], (e3) => Td(e3)), { tabindex: a2, getCurrentDate: l2, getShortDate: i2, getLocale: c2, getMask: d2, decodeString: u2 } = gv(e2), p2 = At(c2()), f2 = At(d2()), { today: h2, editRange: v2, getNativeDateFn: m2, defaultView: g2, monthDirection: y2, yearDirection: b2, viewModel: w2, daysModel: _2, minSelectedModel: C2, maxSelectedModel: x2, daysInModel: k2, startYear: S2, daysOfWeek: E2, viewMonthHash: I2, minNav: O2, maxNav: N2, navBoundaries: M2, days: A2, getViewModel: D2, updateViewModel: V2, setToday: T2, setCalendarTo: L2, goToMonth: P2, goToYear: F2, setMonth: R2, setYear: $2, setView: B2, setEditingRange: H2, toggleDate: j2, addToModel: K2, addToModelRange: z2, removeFromModel: Z2, removeFromModelRange: U2, offsetCalendar: W2, encodeEntry: Y2 } = function(e3, t3, n3, o3, r3, s3, a3, l3) {
    const i3 = ga(() => r3()), c3 = At(null), d3 = ga(() => (e4) => new Date(e4.year, e4.month - 1, e4.day)), u3 = At(e3.view), p3 = At("left"), f3 = At("left"), h3 = ga(() => e3.emitImmediately && !e3.multiple && !e3.range), v3 = At($3()), m3 = ga(() => t3.value.filter((e4) => "string" == typeof e4).map((e4) => s3(e4)).filter((e4) => e4.dateHash && e4.day && e4.month && e4.year)), g3 = ga(() => {
      const e4 = (e5) => s3(e5);
      return t3.value.filter((e5) => Md(e5) && e5.from && e5.to).map((t4) => ({ from: e4(t4.from), to: e4(t4.to) })).filter((e5) => e5.from.dateHash && e5.to.dateHash && e5.from.dateHash < e5.to.dateHash);
    }), y3 = ga(() => m3.value.concat(g3.value.map((e4) => e4.from)).sort((e4, t4) => e4.year - t4.year || e4.month - t4.month)[0]), b3 = ga(() => m3.value.concat(g3.value.map((e4) => e4.to)).sort((e4, t4) => t4.year - e4.year || t4.month - e4.month)[0]), w3 = ga(() => m3.value.length + g3.value.reduce((e4, t4) => e4 + 1 + $u(d3.value(t4.to), d3.value(t4.from)), 0)), _3 = ga(() => Wy(v3.value)), C3 = v3.value.year, x3 = At(C3 && C3 - C3 % Ky - (C3 < 0 ? Ky : 0)), k3 = ga(() => e3.firstDayOfWeek ? +e3.firstDayOfWeek : n3.value.firstDayOfWeek), S3 = ga(() => {
      const e4 = n3.value.daysShort, t4 = k3.value;
      return t4 > 0 ? e4.slice(t4, 7).concat(e4.slice(0, t4)) : e4;
    }), E3 = ga(() => {
      const e4 = v3.value;
      return new Date(e4.year, e4.month, 0).getDate();
    }), I3 = ga(() => {
      if (!e3.minYearMonth)
        return;
      const [t4, n4] = e3.minYearMonth.split("/");
      return { year: parseInt(t4, 10), month: parseInt(n4, 10) };
    }), O3 = ga(() => {
      if (!e3.maxYearMonth)
        return;
      const [t4, n4] = e3.maxYearMonth.split("/");
      return { year: parseInt(t4, 10), month: parseInt(n4, 10) };
    }), N3 = ga(() => {
      const e4 = { month: { prev: true, next: true }, year: { prev: true, next: true } };
      return I3.value && I3.value.year >= v3.value.year && (e4.year.prev = false, I3.value.year === v3.value.year && I3.value.month >= v3.value.month && (e4.month.prev = false)), O3.value && O3.value.year <= v3.value.year && (e4.year.next = false, O3.value.year === v3.value.year && O3.value.month <= v3.value.month && (e4.month.next = false)), e4;
    }), M3 = ga(() => {
      const e4 = {};
      return m3.value.forEach((t4) => {
        const n4 = Wy(t4);
        e4[n4] || (e4[n4] = []), e4[n4].push(t4.day);
      }), e4;
    }), A3 = ga(() => {
      const e4 = {};
      return g3.value.forEach((t4) => {
        const n4 = Wy(t4.from), o4 = Wy(t4.to);
        if (e4[n4] || (e4[n4] = []), e4[n4].push({ from: t4.from.day, to: n4 === o4 ? t4.to.day : void 0, range: t4 }), n4 < o4) {
          let n5;
          const { year: r4, month: s4 } = t4.from, a4 = s4 < 12 ? { year: r4, month: s4 + 1 } : { year: r4 + 1, month: 1 };
          for (; (n5 = Wy(a4)) <= o4; )
            e4[n5] || (e4[n5] = []), e4[n5].push({ from: void 0, to: n5 === o4 ? t4.to.day : void 0, range: t4 }), a4.month++, a4.month > 12 && (a4.year++, a4.month = 1);
        }
      }), e4;
    }), D3 = ga(() => {
      if (!c3.value)
        return;
      const { init: e4, initHash: t4, final: n4, finalHash: o4 } = c3.value, [r4, s4] = t4 <= o4 ? [e4, n4] : [n4, e4];
      if (!r4 || !s4)
        return;
      const a4 = Wy(r4), l4 = Wy(s4);
      if (a4 !== _3.value && l4 !== _3.value)
        return;
      const i4 = {};
      return a4 === _3.value ? (i4.from = r4.day, i4.includeFrom = true) : i4.from = 1, l4 === _3.value ? (i4.to = s4.day, i4.includeTo = true) : i4.to = E3.value, i4;
    }), V3 = ga(() => {
      const t4 = {};
      if (!e3.allowedDates) {
        for (let e4 = 1; e4 <= E3.value; e4++)
          t4[e4] = true;
        return t4;
      }
      const n4 = "function" == typeof e3.allowedDates ? e3.allowedDates : (t5) => {
        var n5;
        return null == (n5 = e3.allowedDates) ? void 0 : n5.includes(t5);
      };
      for (let e4 = 1; e4 <= E3.value; e4++) {
        const o4 = _3.value + "/" + Wd(e4);
        t4[e4] = n4(o4);
      }
      return t4;
    }), T3 = ga(() => "function" == typeof e3.eventsColor ? e3.eventsColor : () => e3.eventsColor), L3 = ga(() => {
      const t4 = {};
      if (e3.events) {
        const n4 = "function" == typeof e3.events ? e3.events : (t5) => {
          var n5;
          return null == (n5 = e3.events) ? void 0 : n5.includes(t5);
        };
        for (let e4 = 0; e4 <= E3.value; e4++) {
          const o4 = _3.value + "/" + Wd(e4);
          t4[e4] = n4(o4) && T3.value(o4);
        }
      } else
        for (let e4 = 1; e4 <= E3.value; e4++)
          t4[e4] = false;
      return t4;
    }), P3 = ga(() => {
      const { year: e4, month: t4 } = v3.value, n4 = new Date(e4, t4 - 1, 1), o4 = new Date(e4, t4 - 1, 0).getDate();
      return { days: n4.getDay() - k3.value - 1, endDay: o4 };
    }), F3 = ga(() => {
      var t4, n4, o4, r4, s4, a4;
      const l4 = [], { days: c4, endDay: d4 } = P3.value, u4 = c4 < 0 ? c4 + 7 : c4;
      if (u4 < 6)
        for (let e4 = d4 - u4; e4 <= d4; e4++)
          l4.push({ i: e4, fill: true });
      const p4 = l4.length;
      for (let e4 = 1; e4 <= E3.value; e4++) {
        const t5 = { i: e4, event: L3.value[e4], classes: [] };
        V3.value[e4] && (t5.in = true, t5.flat = true), l4.push(t5);
      }
      if (M3.value[_3.value] && M3.value[_3.value].forEach((e4) => {
        const t5 = p4 + e4 - 1;
        l4[t5] = { ...l4[t5], selected: true, unelevated: true, flat: false };
      }), A3.value[_3.value] && A3.value[_3.value].forEach((e4) => {
        if (e4.from) {
          const t5 = p4 + e4.from - 1, n5 = p4 + (e4.to || E3.value) - 1;
          for (let o5 = t5; o5 <= n5; o5++)
            l4[o5] = { ...l4[o5], range: e4.range, unelevated: true };
          l4[t5] = { ...l4[t5], rangeFrom: true, flat: false }, e4.to && (l4[n5] = { ...l4[n5], rangeTo: true, flat: false });
        } else if (e4.to) {
          const t5 = p4 + e4.to - 1;
          for (let n5 = p4; n5 <= t5; n5++)
            l4[n5] = { ...l4[n5], range: e4.range, unelevated: true };
          l4[t5] = { ...l4[t5], rangeTo: true, flat: false };
        } else {
          const t5 = p4 + E3.value - 1;
          for (let n5 = p4; n5 <= t5; n5++)
            l4[n5] = { ...l4[n5], range: e4.range, unelevated: true };
        }
      }), D3.value && (null == (t4 = D3.value) ? void 0 : t4.from) && (null == (n4 = D3.value) ? void 0 : n4.to)) {
        const e4 = p4 + (null == (o4 = D3.value) ? void 0 : o4.from) - 1, t5 = p4 + (null == (r4 = D3.value) ? void 0 : r4.to) - 1;
        for (let n5 = e4; n5 <= t5; n5++)
          l4[n5].editRange = true;
        null != (s4 = D3.value) && s4.includeFrom && (l4[e4].editRangeFrom = true), null != (a4 = D3.value) && a4.includeTo && (l4[t5].editRangeTo = true);
      }
      v3.value.year === i3.value.year && v3.value.month === i3.value.month && (l4[p4 + i3.value.day - 1].today = true);
      const f4 = l4.length % 7;
      if (f4 > 0) {
        const e4 = 7 - f4;
        for (let t5 = 1; t5 <= e4; t5++)
          l4.push({ i: t5, fill: true });
      }
      return l4.forEach((t5) => {
        const n5 = ["cds-date-picker__calendar-item"];
        t5.fill ? (n5.push("cds-date-picker__calendar-item--fill"), e3.hideSiblingMonth && n5.push("cds-date-picker__calendar-item--hide-sibling-month")) : (n5.push("cds-date-picker__calendar-item--" + (t5.in ? "in" : "out")), t5.range && n5.push("cds-date-picker__calendar-item__range" + (t5.rangeTo ? "--to" : t5.rangeFrom ? "--from" : "")), t5.editRange && n5.push(`cds-date-picker__calendar-item__edit-range${t5.editRangeFrom ? "--from" : ""}${t5.editRangeTo ? "--to" : ""}`), (t5.range || t5.editRange) && n5.push(`cds-date-picker__day--${t5.color}`)), t5.classes = n5;
      }), l4;
    }), R3 = ga(() => (e4, t4, r4) => Tu(new Date(e4.year, e4.month - 1, e4.day, e4.hour, e4.minute, e4.second, e4.millisecond), t4 || o3.value, r4 || n3.value, e4.year, e4.timezoneOffset));
    function $3() {
      if (!t3.value.length)
        return B3();
      const e4 = t3.value[t3.value.length - 1], n4 = s3(e4.from ? e4.from : e4);
      return null === n4.dateHash ? B3() : n4;
    }
    function B3() {
      let t4, n4;
      if (e3.defaultYearMonth)
        [t4, n4] = e3.defaultYearMonth.split("/"), t4 = parseInt(t4, 10), n4 = parseInt(n4, 10);
      else {
        const e4 = i3.value ? i3.value : r3();
        t4 = e4.year, n4 = e4.month;
      }
      return { year: t4, month: n4, day: 1, hour: 0, minute: 0, second: 0, millisecond: 0, dateHash: t4 + "/" + Wd(n4) + "/01" };
    }
    function H3(e4, t4, n4) {
      if (I3.value && e4 <= I3.value.year && (e4 = I3.value.year, t4 < I3.value.month && (t4 = I3.value.month)), O3.value && e4 >= O3.value.year && (e4 = O3.value.year, t4 > O3.value.month && (t4 = O3.value.month)), n4) {
        const { hour: e5, minute: t5, second: o5, millisecond: r4, timezoneOffset: s4, timeHash: a4 } = n4;
        v3.value = { ...v3.value, hour: e5, minute: t5, second: o5, millisecond: r4, timezoneOffset: s4, timeHash: a4 };
      }
      const o4 = e4 + "/" + Wd(t4) + "/01";
      o4 !== v3.value.dateHash && (p3.value = v3.value.dateHash && v3.value.dateHash < o4 ? "left" : "right", e4 !== v3.value.year && (f3.value = p3.value), un(() => {
        x3.value = e4 - e4 % Ky - (e4 < 0 ? Ky : 0), v3.value = { ...v3.value, year: e4, month: t4, day: 1, dateHash: o4 };
      }));
    }
    function j3(e4, t4) {
      const [n4] = Zy;
      u3.value = n4, H3(e4, t4);
    }
    function K3(n4) {
      let o4;
      if (e3.multiple) {
        const e4 = t3.value.slice();
        e4.push(W3(n4)), o4 = e4;
      } else
        o4 = W3(n4);
      a3(o4, "add", n4);
    }
    function z3(t4) {
      let n4;
      if (e3.multiple) {
        if (t4.from) {
          const e4 = yv(t4.from), o4 = yv(t4.to), r4 = m3.value.filter((t5) => !!t5.dateHash && (t5.dateHash < e4 || t5.dateHash > o4)), s4 = g3.value.filter(({ from: t5, to: n5 }) => !(!n5.dateHash || !t5.dateHash) && (n5.dateHash < e4 || t5.dateHash > o4));
          n4 = [...r4, ...s4, t4].map((e5) => W3(e5));
        }
      } else
        n4 = W3(t4);
      a3(n4, "add", t4);
    }
    function Z3(n4) {
      if (e3.noUnset)
        return;
      let o4 = null;
      if (e3.multiple) {
        const e4 = W3(n4);
        o4 = t3.value.filter((t4) => t4 !== e4), o4.length || (o4 = null);
      }
      a3(o4, "remove", n4);
    }
    function U3(n4) {
      if (e3.noUnset)
        return;
      let o4 = null;
      if (e3.multiple) {
        const e4 = W3(n4);
        n4.from && (o4 = t3.value.filter((t4) => {
          t4.from && t4.from !== e4.from && (t4.to, e4.to);
        }), o4.length || (o4 = null));
      }
      a3(o4, "remove", n4);
    }
    function W3(e4, t4, n4) {
      return e4.from ? { from: R3.value(e4.from, t4, n4), to: R3.value(e4.to, t4, n4) } : R3.value(e4, t4, n4);
    }
    function Y3(e4) {
      let t4 = v3.value.year, n4 = +v3.value.month + e4;
      13 === n4 ? (n4 = 1, t4++) : 0 === n4 && (n4 = 12, t4--), H3(t4, n4), h3.value && l3("month");
    }
    function G3(e4) {
      H3(+v3.value.year + e4, v3.value.month), h3.value && l3("year");
    }
    return { today: i3, editRange: c3, getNativeDateFn: d3, defaultView: u3, monthDirection: p3, yearDirection: f3, isImmediate: h3, viewModel: v3, daysModel: m3, rangeModel: g3, minSelectedModel: y3, maxSelectedModel: b3, daysInModel: w3, startYear: x3, daysOfWeek: S3, daysInMonth: E3, viewMonthHash: _3, minNav: I3, maxNav: O3, navBoundaries: N3, days: F3, daysMap: M3, rangeMap: A3, rangeView: D3, selectionDaysMap: V3, getViewModel: $3, updateViewModel: H3, setToday: function() {
      const e4 = i3.value, t4 = M3.value[Wy(e4)];
      (!t4 || !t4.includes(e4.day)) && K3(e4), j3(e4.year, e4.month);
    }, setCalendarTo: j3, goToMonth: Y3, goToYear: G3, setMonth: function(e4) {
      H3(v3.value.year, e4), u3.value = "Calendar", h3.value && l3("month");
    }, setYear: function(t4) {
      H3(t4, v3.value.month), u3.value = "Years" === e3.view ? "Months" : "Calendar", h3.value && l3("year");
    }, setView: function(e4) {
      !Zy.includes(e4) || (u3.value = e4);
    }, setEditingRange: function(t4, n4) {
      if (!e3.range || !t4)
        return void (c3.value = null);
      const o4 = { ...v3.value, from: t4 }, r4 = n4 ? { ...v3.value, to: n4 } : o4;
      c3.value = { init: o4, initHash: yv(o4), final: r4, finalHash: yv(r4) }, j3(o4.year, o4.month);
    }, toggleDate: function(e4, t4) {
      const n4 = M3.value[t4];
      e4.from ? (n4 && n4.includes(e4.day) ? U3 : z3)(e4) : (n4 && n4.includes(e4.day) ? Z3 : K3)(e4);
    }, addToModel: K3, addToModelRange: z3, removeFromModel: Z3, removeFromModelRange: U3, offsetCalendar: function(e4, t4) {
      ["month", "years"].includes(e4) && ("month" === e4 ? Y3 : G3)(t4 ? -1 : 1);
    }, encodeEntry: W3 };
  }(e2, s2, p2, f2, l2, u2, function(t3, o3, r3) {
    const s3 = t3 && 1 === t3.length && !e2.multiple ? t3[0] : t3;
    J2 = s3;
    const { reason: a3, details: l3 } = oe2(o3, r3);
    n2("update:modelValue", s3, a3, l3);
  }, function(e3) {
    const t3 = Lt(_2)[0] && Lt(_2)[0].dateHash ? { ...Lt(_2)[0] } : { ...Lt(w2) };
    un(() => {
      t3.year = Lt(w2).year, t3.month = Lt(w2).month;
      const o3 = new Date(t3.year, t3.month, 0).getDate();
      t3.day = Math.min(Math.max(1, t3.day), o3);
      const r3 = Y2(t3);
      J2 = r3;
      const { details: s3 } = oe2("", t3);
      n2("update:modelValue", r3, e3, s3);
    });
  }), { getCache: G2 } = function() {
    const e3 = /* @__PURE__ */ new Map();
    return { getCache: function(t3, n3) {
      return e3[t3] || (e3[t3] = n3);
    }, getCacheWithFn: function(t3, n3) {
      return e3[t3] || (e3[t3] = n3());
    } };
  }(), q2 = At(), X2 = At();
  let J2;
  const Q2 = ga(() => {
    if (e2.title)
      return e2.title;
    if (Lt(v2)) {
      const e3 = Lt(v2).init;
      if (e3) {
        const t4 = Lt(m2)(e3);
        return Lt(p2).daysShort[t4.getDay()] + ", " + e3.day + " " + Lt(p2).monthsLong[e3.month - 1] + zy + "?";
      }
    }
    if (!Lt(k2))
      return zy;
    if (Lt(k2) > 1)
      return `${Lt(k2)} ${Lt(p2).pluralDay(Lt(k2))}`;
    const t3 = Lt(_2)[0], n3 = Lt(m2)(t3);
    return isNaN(n3.valueOf()) ? zy : Lt(p2).daysShort[n3.getDay()] + ", " + t3.day + " " + Lt(p2).monthsLong[t3.month - 1];
  }), ee2 = ga(() => {
    if (e2.description)
      return e2.description;
    if (0 === Lt(k2))
      return zy;
    if (Lt(k2) > 1) {
      const e3 = Lt(C2), t3 = Lt(x2), n3 = Lt(p2).monthsShort;
      return n3[e3.month - 1] + (e3.year !== t3.year ? " " + e3.year + zy + n3[t3.month - 1] + " " : e3.month !== t3.month ? zy + n3[t3.month - 1] : "") + " " + t3.year;
    }
    return Lt(_2)[0].year;
  }), te2 = ga(() => e2.disabled ? { "aria-disabled": "true" } : e2.readonly ? { "aria-readonly": "true" } : {}), ne2 = ga(() => ({ "cds-date-picker": true, "cds-date-picker--is-header": e2.header || e2.title, "cds-date-picker--disabled": e2.disabled, "cds-date-picker--readonly": e2.readonly }));
  function oe2(e3, t3) {
    return t3.from ? { reason: `${e3}-range`, details: { ...i2(t3.target), from: i2(t3.from), to: i2(t3.to) } } : { reason: `${e3}-day`, details: i2(t3) };
  }
  function re2() {
    if (e2.header || e2.title)
      return ba("div", { class: "cds-date-picker__header" }, [ba("div", { class: "cds-date-picker__header-title" }, [ba(Zh, { transition: "fade-transition-minimal" }, { default: () => t2.title ? t2.title({ date: Lt(Q2) }) : ba("div", { key: "header-title-" + Lt(Q2), class: ["cds-date-picker__header-label", { "cds-date-picker__header-label--active": "Calendar" === Lt(g2) }], tabindex: Lt(a2), ...G2("viewCalendar", { onClick() {
        g2.value = "Calendar";
      }, onKeyup(e3) {
        e3.key === kd.enter && (g2.value = "Calendar");
      } }) }, [Lt(Q2)]) }), e2.todayBtn ? ba(my, { appearance: "transparent", class: "cds-date-picker__header-today", icon: "calendar", tabindex: Lt(a2), onClick: T2 }) : null]), ba("div", { class: "cds-date-picker__header-description" }, [ba(Zh, { transition: "fade-transition-minimal" }, { default: () => t2.description ? t2.description({ year: Lt(ee2) }) : ba("div", { key: "header-year-" + Lt(ee2), class: ["cds-date-picker__header-year", { "cds-date-picker__header-year--active": "Years" === Lt(g2) }], tabindex: Lt(a2), ...G2("viewYear", { onClick() {
        g2.value = "Years";
      }, onKeyup(e3) {
        e3.key === kd.enter && (g2.value = "Years");
      } }) }, [Lt(ee2)]) })])]);
  }
  function se2({ label: e3, type: t3, key: n3, direction: o3, goTo: r3, boundaries: s3, classname: l3 }) {
    return [ba("div", { class: "cds-date-picker__arrow" }, [ba(my, { appearance: "transparent", icon: "chevron-left", disabled: !s3.prev, tabindex: Lt(a2), ...G2("go-#" + t3, { onClick() {
      r3(-1);
    } }) })]), ba("div", { class: `${l3}` }, [ba(Zh, { transition: `jump-transition-${o3}` }, () => ba("div", { key: n3 }, [ba(my, { appearance: "transparent", text: String(e3), tabindex: Lt(a2), ...G2("view#" + t3, { onClick: () => {
      g2.value = t3;
    } }) })]))]), ba("div", { class: "cds-date-picker__arrow" }, [ba(my, { appearance: "transparent", icon: "chevron-right", disabled: !s3.next, tabindex: Lt(a2), ...G2("go+#" + t3, { onClick() {
      r3(1);
    } }) })])];
  }
  ho(() => s2, (e3) => {
    if (J2 === e3)
      J2 = 0;
    else {
      const e4 = D2();
      V2(e4.year, e4.month, e4);
    }
  }), ho(g2, () => {
    Lt(q2) && Lt(X2) && Lt(X2).contains(document.activeElement) && Lt(q2).focus();
  }), ho(() => Lt(w2).year + "|" + Lt(w2).month, () => {
    n2("navigation", { year: Lt(w2).year, month: Lt(w2).month });
  });
  const ae2 = { Calendar: () => [ba("div", { key: "calendar-view", class: "cds-date-picker__view cds-date-picker__calendar" }, [ba("div", { class: "cds-date-picker__navigation" }, [...se2({ label: Lt(p2).months[Lt(w2).month - 1], type: "Months", key: Lt(w2).month, direction: Lt(y2), goTo: P2, boundaries: Lt(M2).month, classname: "cds-date-picker__navigation--months" }), ...se2({ label: String(Lt(w2).year), type: "Years", key: Lt(w2).year, direction: Lt(b2), goTo: F2, boundaries: Lt(M2).year, classname: "cds-date-picker__navigation--years" })]), ba("div", { class: "cds-date-picker__calendar-weekdays" }, Lt(E2).map((e3) => ba("div", { class: "cds-date-picker__calendar-item" }, [ba("div", e3)]))), ba("div", { class: "cds-date-picker__calendar-days-container" }, ba(Zh, { transition: `slide-transition-${Lt(y2)}` }, () => ba("div", { key: Lt(I2), class: "cds-date-picker__calendar-days" }, Lt(A2).map((t3) => ba("div", { class: [t3.classes, { "cds-date-picker__calendar-item--today": t3.today, "cds-date-picker__calendar-item--active": !t3.flat, [`cds-date-picker__calendar-item--event--${t3.event}`]: t3.event }] }, [t3.in ? ba(my, { appearance: t3.flat ? "transparent" : "primary", class: ["cds-date-picker__calendar-day"], text: String(t3.i), tabindex: Lt(a2), ...G2("day#" + t3.i, { onClick: () => {
    !function(t4) {
      const o3 = { ...Lt(w2), day: t4 };
      if (e2.range)
        if (Lt(v2)) {
          const e3 = Lt(v2).initHash, t5 = yv(o3), r3 = e3 <= t5 ? { from: Lt(v2).init, to: o3 } : { from: o3, to: Lt(v2).init };
          v2.value = null, e3 === t5 ? K2(o3) : z2({ target: o3, ...r3 }), n2("rangeEnd", { from: i2(r3.from), to: i2(r3.to) });
        } else {
          const r3 = Lt(A2).find((e3) => !e3.fill && e3.i === t4);
          if (!e2.noUnset && (null == r3 ? void 0 : r3.range))
            return void U2({ target: o3, from: r3.range.from, to: r3.range.to });
          if (null != r3 && r3.selected)
            return void Z2(o3);
          const s3 = yv(o3);
          v2.value = { init: o3, initHash: s3, final: o3, finalHash: s3 }, n2("rangeStart", i2(o3));
        }
      else
        j2(o3, Lt(I2));
    }(t3.i);
  }, onMouseover: () => {
    !function(e3) {
      if (Lt(v2)) {
        const t4 = { ...Lt(w2), day: e3 };
        v2.value = { ...Lt(v2), final: t4, finalHash: yv(t4) };
      }
    }(t3.i);
  } }) }) : ba("div", "" + t3.i)])))))])], Months: () => {
    const e3 = Lt(w2).year === Lt(h2).year, t3 = (e4) => {
      var t4, n3, o3, r3;
      return O2 && Lt(w2).year === (null == (t4 = Lt(O2)) ? void 0 : t4.year) && (null == (n3 = Lt(O2)) ? void 0 : n3.month) > e4 || N2 && Lt(w2).year === (null == (o3 = Lt(N2)) ? void 0 : o3.year) && (null == (r3 = Lt(N2)) ? void 0 : r3.month) < e4;
    };
    return ba("div", { key: "months-view", class: "cds-date-picker__view cds-date-picker__months" }, Lt(p2).monthsShort.map((n3, o3) => {
      const r3 = Lt(w2).month === o3 + 1;
      return ba("div", { class: "cds-date-picker__months-item" }, [ba(my, { appearance: r3 ? "primary" : "transparent", class: ["cds-date-picker__month", { "cds-date-picker__month--today": e3 && Lt(h2).month === o3 + 1 }, { "cds-date-picker__month--active": r3 }], text: String(n3), disabled: t3(o3 + 1), tabindex: Lt(a2), ...G2("month#" + o3, { onClick: () => {
        R2(o3 + 1);
      } }) })]);
    }));
  }, Years: () => {
    const e3 = Lt(S2), t3 = e3 + Ky, n3 = [], o3 = (e4) => Lt(O2) && Lt(O2).year > e4 || Lt(N2) && Lt(N2).year < e4;
    for (let r3 = e3; r3 <= t3; r3++) {
      const e4 = Lt(w2).year === r3;
      n3.push(ba("div", { class: "cds-date-picker__years-item" }, [ba(my, { key: `year-${r3}`, appearance: e4 ? "primary" : "transparent", class: ["cds-date-picker__year", { "cds-date-picker__year--active": e4 }, { "cds-date-picker__year--today": Lt(h2).year === r3 }], text: String(r3), disabled: o3(r3), tabindex: Lt(a2), ...G2("year#" + r3, { onClick: () => {
        $2(r3);
      } }) })]));
    }
    return ba("div", { key: "years-view", class: "cds-date-picker__view cds-date-picker__years" }, [ba("div", { class: "cds-date-picker__years-container" }, [ba("div", { class: "cds-date-picker__years-prev" }, [ba(my, { appearance: "transparent", icon: "chevron-left", disabled: o3(e3), tabindex: Lt(a2), ...G2("year-prev", { onClick: () => {
      S2.value -= Ky;
    } }) })]), ba("div", { class: "cds-date-picker__years-body" }, n3), ba("div", { class: "cds-date-picker__years-next" }, [ba(my, { appearance: "transparent", icon: "chevron-right", disabled: o3(t3), tabindex: Lt(a2), ...G2("year-next", { onClick: () => {
      S2.value += Ky;
    } }) })])])]);
  } };
  return Oh(() => {
    var e3;
    return ba("div", { ref: X2, class: Cp(Lt(ne2), Lt(r2)), ...Lt(te2) }, [re2(), ba("div", { ref: q2, class: "cds-date-picker__container", tabindex: -1 }, ba("div", { class: "cds-date-picker__body" }, [ba(Zh, { appear: true, transition: "fade-transition-minimal" }, ae2[Lt(g2)]), t2.default ? ba("div", { class: "cds-date-picker__actions" }, [null == (e3 = t2.default) ? void 0 : e3.call(t2)]) : null]))]);
  }), o2({ setToday: T2, setCalendarTo: L2, setView: B2, offsetCalendar: W2, setEditingRange: H2 }), {};
} }), Gy = "3rem", qy = "800", Xy = Hd("15rem"), Jy = Hd(Gy), Qy = Number(qy) + 1, eb = "CdsDrawer", tb = Lo({ name: eb, props: { modelValue: { type: Boolean, default: null }, permanent: { type: Boolean, default: false }, temporary: { type: Boolean, default: false }, minified: { type: Boolean, default: false }, fixed: { type: Boolean, default: false }, right: { type: Boolean, default: false }, border: { type: Boolean, default: false }, fixedSlots: { type: Boolean, default: false }, ...nh(), ...Zv(), ...ch({ tag: "aside" }) }, emits: { opened: (e2) => true, closed: (e2) => true, "update:modelValue": (e2) => true }, setup(e2, { emit: t2, slots: n2, attrs: o2, expose: r2 }) {
  const s2 = At(), a2 = vh(e2, "modelValue", null, (e3) => !!e3), l2 = ga(() => !e2.permanent && e2.temporary), i2 = At(false), c2 = nv(), d2 = Fv(), u2 = At(!!Object.keys(d2).length), { themeClasses: p2 } = Yv(e2), f2 = ga(() => Lt(u2) ? e2.fixed || Lt(d2.view).indexOf(e2.right ? "R" : "L") > -1 || Lt(c2.platform).ios && Lt(d2.isContainer) : e2.fixed), { invertColorClasses: h2 } = oh(e2, eb), v2 = ga(() => ({ "cds-drawer--active": Lt(a2), "cds-drawer--hover": Lt(i2), "cds-drawer--temporary": Lt(l2), "cds-drawer--minified": e2.minified, "cds-drawer--right": e2.right, "cds-drawer--fixed": Lt(f2), "cds-drawer--border": e2.border, "cds-drawer--fixed-slots": e2.fixedSlots })), m2 = ga(() => !!Lt(u2) && (e2.right ? "r" === Lt(d2.rows).top[2] : "l" === Lt(d2.rows).top[0])), g2 = ga(() => !!Lt(u2) && (e2.right ? "r" === Lt(d2.rows).bottom[2] : "l" === Lt(d2.rows).bottom[0])), y2 = ga(() => {
    const t3 = {};
    return Lt(u2) && (d2.header.space && !Lt(m2) && (Lt(f2) ? (t3.top = d2.header.offset + "px", t3.height = `calc(100% - ${d2.header.offset}px)`) : d2.header.space && (t3.top = d2.header.size + "px", t3.height = `calc(100% - ${d2.header.size}px)`)), d2.footer.space && !Lt(g2) && (Lt(f2) ? (t3.bottom = d2.footer.offset + "px", t3.height = `calc(100% - ${d2.footer.offset}px - ${d2.header.offset}px)`) : d2.footer.space && (t3.bottom = d2.footer.size + "px", t3.height = `calc(100% - ${d2.footer.size}px - ${d2.header.size}px)`)), Lt(m2) && d2.footer.space && (t3.height = `calc(100% - ${d2.footer.size}px)`, e2.minified && (t3.zIndex = Qy)), Lt(g2) && d2.header.space && (t3.height = `calc(100% - ${d2.header.size}px)`, e2.minified && (t3.zIndex = Qy)), Lt(m2) && Lt(g2) && (t3.height = "100%", t3.zIndex = Qy)), t3;
  }), b2 = ga(() => e2.modelValue), w2 = ga(() => !!n2.prepend), _2 = ga(() => !!n2.append);
  let C2 = null;
  zo(() => {
    null !== e2.modelValue || Lt(l2) || (a2.value = e2.permanent);
  }), Yo(() => {
    Lt(u2) && d2.update(e2.right ? "right" : "left", "space", false);
  }), ho(() => Lt(a2), (e3) => {
    e3 ? (C2 = document.activeElement, t2("opened", e3)) : (C2 && C2.focus(), t2("closed", e3));
  }), Lt(u2) && (ho(c2.mdUp, (t3) => {
    d2.update(e2.right ? "right" : "left", "space", !e2.temporary && t3);
  }, { immediate: true }), ho(() => e2.minified, (t3) => {
    d2.update(e2.right ? "right" : "left", "size", e2.minified ? Jy : Xy);
  }, { immediate: true }), ho(() => e2.temporary, (t3) => {
    d2.update(e2.right ? "right" : "left", "space", !t3);
  }, { immediate: true }));
  return r2({ drawerRef: s2 }), { attrs: o2, drawerRef: s2, hasPrependSlot: w2, hasAppendSlot: _2, hasOverlay: b2, drawerClasses: v2, invertColorClasses: h2, themeClasses: p2, styles: y2, onClose: () => {
    a2.value = false;
  }, onMouseenter: () => {
    i2.value = true;
  }, onMouseleave: () => {
    i2.value = false;
  } };
} }), nb = { key: 0, class: "cds-drawer__prepend" }, ob = { class: "cds-drawer__content" }, rb = { key: 1, class: "cds-drawer__append" };
const sb = Tm(tb, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns(bs, null, [(Ss(), Ms(ao(e2.tag), Us({ ref: "drawerRef", class: ["cds-drawer", [e2.drawerClasses, e2.invertColorClasses, e2.themeClasses]], style: e2.styles, tabindex: "-1" }, e2.attrs, { onKeydown: Nl(e2.onClose, ["esc"]), onMouseenter: e2.onMouseenter, onMouseleave: e2.onMouseleave }), { default: Wn(() => [e2.hasPrependSlot ? (Ss(), Ns("div", nb, [nr(e2.$slots, "prepend", W(Rs({ invertColor: e2.invertColor })))])) : js("", true), Ls("nav", ob, [nr(e2.$slots, "default", W(Rs({ invertColor: e2.invertColor })))]), e2.hasAppendSlot ? (Ss(), Ns("div", rb, [nr(e2.$slots, "append", W(Rs({ invertColor: e2.invertColor })))])) : js("", true)]), _: 3 }, 16, ["class", "style", "onKeydown", "onMouseenter", "onMouseleave"])), e2.hasOverlay ? (Ss(), Ns("div", { key: 0, class: "cds-drawer__overlay", onClick: t2[0] || (t2[0] = (...t3) => e2.onClose && e2.onClose(...t3)) })) : js("", true)], 64);
}]]), ab = "CdsDropdown", lb = Lo({ name: ab, components: { CdsTextInput: ty, CdsMenu: zg, CdsList: gg, CdsListItem: Sg, CdsCheckbox: dg, CdsTag: _y, CdsIcon: Lm }, props: { ...iv(), ...jh(), ...Nh(), ...Fh(), ...jf(), ...Kf(), ...Wh(), ...Ev(), ...hv(), ...mh(), ...Qf(), ...th(), ...Zf(), ...Xf(), ...dh(), ...zh(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "update:menu": (e2) => true, "update:selectedAll": (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const { dropdown: r2 } = Iv(e2), { hasLabel: s2, hasDescription: a2 } = Mh(e2, t2, ab), { hasPrependInnerIcon: l2, hasAppendInnerIcon: i2 } = Kh(e2, t2, ab), { hasAppend: c2, hasPrepend: d2 } = zf(e2, t2, ab), { isFocused: u2, focus: p2 } = gh(e2, ab), { items: f2, transformIn: h2, transformOut: v2 } = uh(e2), { internalMenu: m2, menuRef: g2 } = vv(e2), { model: y2, selections: b2, selected: w2, selectedAll: _2, selectedIndeterminate: C2, selectStrategy: x2, displayItems: k2 } = cv(e2, f2, h2, v2), S2 = Lh(), E2 = ga(() => Lt(y2).map((e3) => e3.props.value).join(", ")), I2 = ga(() => !e2.hideNoData && !Lt(k2).length), O2 = ga(() => Lt(y2).length > 0), N2 = ga(() => y2.externalValue), M2 = ga(() => ({ "cds-dropdown--active-menu": Lt(m2), "cds-dropdown--selected": Lt(y2).length, "cds-dropdown--ellipsis": e2.ellipsis, ["cds-dropdown--" + (e2.multiple ? "multiple" : "single")]: true })), { inputProps: A2 } = dv(e2, ty.props, ["modelValue", "validationValue", "readonly", "focused", "appendInnerIcon", "counter"]), D2 = At(), V2 = At(), T2 = ga(() => !!(!e2.hideNoData || Lt(k2).length || t2.prepend || t2.append || t2["no-data"]));
  function L2(t3) {
    if (!e2.readonly && !e2.disabled)
      if (e2.multiple) {
        const e3 = Lt(w2).findIndex((e4) => Vd(e4, t3.value));
        if (-1 === e3)
          y2.value = [...Lt(y2), t3];
        else {
          const t4 = [...Lt(y2)];
          t4.splice(e3, 1), y2.value = t4;
        }
      } else
        y2.value = [t3], m2.value = false;
  }
  function P2() {
    e2.readonly || e2.disabled || e2.multiple && (!Lt(_2) || Lt(C2) ? y2.value = [...Lt(f2)] : y2.value = [], n2("update:selectedAll", !Lt(_2)));
  }
  let F2, R2 = "";
  return o2({ isFocused: u2, menu: m2, focus: p2, select: L2, selectAll: P2, menuRef: g2, listRef: V2, textInputRef: D2 }), { model: y2, internalMenu: m2, inputModelValue: E2, displayItems: k2, selections: b2, selected: w2, selectedAll: _2, selectedIndeterminate: C2, selectStrategy: x2, isDirty: O2, isFocused: u2, externalValidationValue: N2, inputRef: D2, menuRef: g2, listRef: V2, inputProps: A2, hasList: T2, hasLabel: s2, hasDescription: a2, hasPrepend: d2, hasAppend: c2, hasPrependInnerIcon: l2, hasAppendInnerIcon: i2, hasNoDataItem: I2, dropdownClasses: M2, dropdownLocale: r2, noop: eu, select: L2, selectAll: P2, mergeItemProps: function(e3) {
    return Us(e3.props, { onClick: () => L2(e3) });
  }, onKeydown: function(t3) {
    var n3, o3, r3, s3;
    if (e2.readonly || Lt(null == S2 ? void 0 : S2.isReadonly))
      return;
    if ([kd.enter, kd.space, kd.down, kd.up, kd.home, kd.end].includes(t3.key) && t3.preventDefault(), [kd.enter, kd.down, kd.space].includes(t3.key) && (m2.value = true), [kd.esc, kd.tab].includes(t3.key) && (m2.value = false), t3.key === kd.down ? null == (n3 = Lt(V2)) || n3.focus("next") : t3.key === kd.up ? null == (o3 = Lt(V2)) || o3.focus("prev") : t3.key === kd.home ? null == (r3 = Lt(V2)) || r3.focus("first") : t3.key === kd.end && (null == (s3 = Lt(V2)) || s3.focus("last")), e2.multiple || !function(e3) {
      const t4 = 1 === e3.key.length, n4 = !e3.ctrlKey && !e3.metaKey && !e3.altKey;
      return t4 && n4;
    }(t3))
      return;
    const a3 = performance.now();
    a3 - F2 > 1e3 && (R2 = ""), R2 += t3.key.toLowerCase(), F2 = a3;
    const l3 = Lt(f2).find((e3) => e3.title.toLowerCase().startsWith(R2));
    void 0 !== l3 && (y2.value = [l3]);
  }, onFocusin: function() {
    p2();
  }, onFocusout: function(e3) {
    var t3;
    null == e3.relatedTarget && (null == (t3 = Lt(D2)) || t3.focus());
  }, onBlur: function(e3) {
    var t3;
    null != (t3 = Lt(V2)) && t3.$el.contains(e3.relatedTarget) || (m2.value = false);
  }, onClear: function() {
    e2.openOnClear && (m2.value = true), y2.value = [];
  }, onInputModelValue: function(e3) {
    null == e3 && (y2.value = []);
  }, onMousedownControl: function() {
    e2.hideNoData && !Lt(f2).length || e2.readonly || Lt(null == S2 ? void 0 : S2.isReadonly) || (m2.value = !m2.value);
  }, onMousedownMenuIcon: function(t3) {
    e2.readonly || e2.disabled || (t3.preventDefault(), t3.stopPropagation(), m2.value = !Lt(m2));
  } };
} }), ib = { class: "cds-dropdown__selection--text" }, cb = { key: 0, class: "cds-dropdown__selection--comma" };
const db = Tm(lb, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-list-item"), l2 = ro("cds-checkbox"), i2 = ro("cds-list"), c2 = ro("cds-menu"), d2 = ro("cds-icon"), u2 = ro("cds-tag"), p2 = ro("cds-text-input");
  return Ss(), Ms(p2, Us({ ref: "inputRef", focused: e2.isFocused, "onUpdate:focused": t2[2] || (t2[2] = (t3) => e2.isFocused = t3), "model-value": e2.inputModelValue, placeholder: e2.placeholder, dirty: e2.isDirty, "validation-value": e2.externalValidationValue, readonly: "", counter: false, class: [e2.dropdownClasses, "cds-dropdown"] }, e2.inputProps, { onBlur: e2.onBlur, onKeydown: e2.onKeydown, "onClick:clear": e2.onClear, "onUpdate:modelValue": e2.onInputModelValue, "onMousedown:control": e2.onMousedownControl }), tr({ "append-inner": Wn(() => [nr(e2.$slots, "append-inner"), Ps(d2, { name: e2.menuIcon, class: "cds-dropdown__menu-icon", onClick: e2.noop, onMousedown: e2.onMousedownMenuIcon }, null, 8, ["name", "onClick", "onMousedown"])]), messages: Wn(() => [nr(e2.$slots, "messages")]), default: Wn(() => [nr(e2.$slots, "default", {}, () => [Ps(c2, Us({ ref: "menuRef", modelValue: e2.internalMenu, "onUpdate:modelValue": t2[1] || (t2[1] = (t3) => e2.internalMenu = t3), eager: e2.eager, "max-height": e2.menuHeight, "close-on-content-click": false, "open-on-click": false, transition: e2.transition, activator: "parent", "content-classes": "cds-dropdown__content" }, e2.menuProps), { default: Wn(() => [e2.hasList ? (Ss(), Ms(i2, Us({ key: 0, ref: "listRef", selected: e2.selected, "select-strategy": e2.selectStrategy }, e2.listProps, { onFocusin: e2.onFocusin, onFocusout: e2.onFocusout }), { default: Wn(() => [e2.hasNoDataItem ? nr(e2.$slots, "no-data", { key: 0 }, () => [Ps(a2, { title: e2.dropdownLocale.noData }, null, 8, ["title"])]) : js("", true), nr(e2.$slots, "prepend-item", {}, () => [e2.multiple && e2.isSelectAll ? (Ss(), Ms(a2, { key: 0, "is-clickable": "", title: e2.dropdownLocale.selectAll, class: "cds-dropdown__select-all", onClick: e2.selectAll }, { prepend: Wn(() => [Ps(l2, { modelValue: e2.selectedAll, "onUpdate:modelValue": t2[0] || (t2[0] = (t3) => e2.selectedAll = t3), indeterminate: e2.selectedIndeterminate, tabindex: "-1", "hide-messages": "" }, null, 8, ["modelValue", "indeterminate"])]), _: 1 }, 8, ["title", "onClick"])) : js("", true)]), (Ss(true), Ns(bs, null, er(e2.displayItems, (t3, n3) => nr(e2.$slots, "item", W(Us({ key: n3 }, { item: t3, index: n3, props: e2.mergeItemProps(t3) })), () => [Ps(a2, Us({ disabled: e2.readonly }, t3.props, { onClick: (n4) => e2.select(t3) }), tr({ _: 2 }, [e2.multiple && !e2.hideSelected ? { name: "prepend", fn: Wn(({ isSelected: e3 }) => [Ps(l2, { "model-value": e3, tabindex: "-1", "hide-messages": "" }, null, 8, ["model-value"])]), key: "0" } : void 0]), 1040, ["disabled", "onClick"])])), 128)), nr(e2.$slots, "append-item")]), _: 3 }, 16, ["selected", "select-strategy", "onFocusin", "onFocusout"])) : js("", true)]), _: 3 }, 16, ["modelValue", "eager", "max-height", "transition"]), (Ss(true), Ns(bs, null, er(e2.selections, (t3, n3) => (Ss(), Ns("div", { key: t3.value, class: "cds-dropdown__selection" }, [nr(e2.$slots, "selection", W(Rs({ item: t3, index: n3 })), () => [Ls("span", ib, Q(t3.title), 1), e2.multiple && n3 < e2.selections.length - 1 ? (Ss(), Ns("span", cb, ",")) : js("", true)])]))), 128))])]), _: 2 }, [e2.hasLabel ? { name: "label", fn: Wn(() => [nr(e2.$slots, "label")]), key: "0" } : void 0, e2.hasDescription ? { name: "description", fn: Wn(() => [nr(e2.$slots, "description")]), key: "1" } : void 0, e2.hasPrepend ? { name: "prepend", fn: Wn(() => [nr(e2.$slots, "prepend")]), key: "2" } : void 0, e2.hasPrependInnerIcon || e2.counter && e2.multiple ? { name: "prepend-inner", fn: Wn(() => [e2.selected.length > 0 ? (Ss(), Ms(u2, { key: 0, text: `${e2.selected.length}` }, null, 8, ["text"])) : js("", true), nr(e2.$slots, "prepend-inner")]), key: "3" } : void 0, e2.hasAppend ? { name: "append", fn: Wn(() => [nr(e2.$slots, "append")]), key: "4" } : void 0]), 1040, ["focused", "model-value", "placeholder", "dirty", "validation-value", "class", "onBlur", "onKeydown", "onClick:clear", "onUpdate:modelValue", "onMousedown:control"]);
}]]);
Lo({ name: "ActivesRiseRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "BankRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "BookBuilding", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "BookChat", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "BookFinger", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "BooksAppleHat", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "CalendarWatch", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "CarBoxes", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "Chip", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "ChocholateMenu", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "DisplayAnalytics", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "DisplayDemo", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "DisplayUserProfile", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "DocLaptop", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "FingerClickRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "HammerBuildings", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "HandCardRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "HandGear", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "HandGlass", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "HandMoney", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "HandshakeRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "Key", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "LikeDisplayClick", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "LikeProfileRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "MailOpened", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "ManLaptopWorld", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "ManSpyglass", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "PhoneInHand", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "Rocket", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "SandwatchRub", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "SearchInterface", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "Support", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "UserAnalytics", props: { mono: { type: Boolean, default: false } } }), Lo({ name: "WomanChat", props: { mono: { type: Boolean, default: false } } });
const ub = ["actives-rise-rub", "bank-rub", "book-building", "book-chat", "book-finger", "books-apple-hat", "calendar-watch", "car-boxes", "chip", "chocholate-menu", "display-analytics", "display-demo", "display-user-profile", "doc-laptop", "finger-click-rub", "hammer-buildings", "hand-card-rub", "hand-gear", "hand-glass", "hand-money", "handshake-rub", "key", "like-display-click", "like-profile-rub", "mail-opened", "man-laptop-world", "man-spyglass", "phone-in-hand", "rocket", "sandwatch-rub", "search-interface", "support", "user-analytics", "woman-chat"], pb = Lo({ name: "CdsEmptyState", props: { description: { type: String, default: "" }, illustration: { type: String, default: void 0, validator: (e2) => ub.includes(e2) }, ...Ff() }, setup(e2, { slots: t2, expose: n2 }) {
  const { hasTitle: o2 } = Rf(e2, t2);
  o2.value;
  const r2 = ga(() => !!t2.illustration), s2 = ga(() => !!t2.title), a2 = ga(() => !!t2.description), l2 = ga(() => xu(e2.illustration));
  return n2({ illustrationComponent: l2 }), { illustrationComponent: l2, hasIllustrationSlot: r2, hasTitleSlot: s2, hasDescriptionSlot: a2 };
} }), fb = { class: "cds-empty-state" }, hb = { key: 0, class: "cds-empty-state__image" }, vb = { key: 1, class: "cds-empty-state__title" }, mb = { key: 0 }, gb = { key: 2, class: "cds-empty-state__description" }, yb = { key: 0 };
const bb = Tm(pb, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("div", fb, [e2.hasIllustrationSlot || e2.illustration ? (Ss(), Ns("div", hb, [e2.illustration ? (Ss(), Ms(ao(e2.illustrationComponent), { key: 0 })) : nr(e2.$slots, "illustration", { key: 1 })])) : js("", true), e2.title || e2.hasTitleSlot ? (Ss(), Ns("div", vb, [e2.title ? (Ss(), Ns("h4", mb, Q(e2.title), 1)) : nr(e2.$slots, "title", { key: 1 })])) : js("", true), e2.description || e2.hasDescriptionSlot ? (Ss(), Ns("div", gb, [e2.description ? (Ss(), Ns("span", yb, Q(e2.description), 1)) : nr(e2.$slots, "description", { key: 1 })])) : js("", true), nr(e2.$slots, "actions")]);
}]]), wb = ["default", "accordion"], _b = tu({ variant: { type: String, default: "default", validator: (e2) => wb.includes(e2) } }, "CdsExpandablePanels"), Cb = Symbol.for("cds:expandable-panels-group"), xb = Symbol.for("cds:expandable-panel"), kb = "CdsExpandablePanels", Sb = Lo({ name: kb, props: { ..._b(), ...Yf(), ...Qf(), ...th(), ...Ov(), ...ch(), ...Zv() }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2 }) {
  const { themeClasses: n2 } = Yv(e2), o2 = ga(() => e2.variant && `cds-expandable-panels--variant-${e2.variant}`), { appearanceClasses: r2 } = Gf(e2, kb);
  return Dv(e2, Cb), Ir(xb, { readOnly: e2.readonly }), Oh(() => {
    var s2;
    return ba(e2.tag, { class: Cp("cds-expandable-panels", Lt(o2), Lt(r2), Lt(n2)) }, [null == (s2 = t2.default) ? void 0 : s2.call(t2)]);
  }), {};
} }), Eb = Lo({ name: "CdsExpandablePanelContent", props: { ...Wh() }, setup(e2, { slots: t2, expose: n2 }) {
  const o2 = Or(Cb);
  if (!o2)
    throw new Error("[CDS] cds-expandable-panel-content needs to be placed inside v-expandable-panel");
  const { hasContent: r2 } = Yh(e2, o2.isSelected);
  return Oh(() => ba(jm, {}, { default: () => {
    var e3;
    return [Lt(o2.isSelected) && ba("div", { class: "cds-expandable-panel__content" }, [t2.default && r2 && ba("div", { class: Cp("cds-expandable-panel__content-wrapper") }, [null == (e3 = t2.default) ? void 0 : e3.call(t2)])])];
  } })), n2({ ...o2 }), {};
} }), Ib = tu({ expandIcon: { type: String, default: "chevron-down", validator: (e2) => Hf.includes(e2) }, collapseIcon: { type: String, default: "chevron-up", validator: (e2) => Hf.includes(e2) }, hideActions: { type: Boolean, default: false } }, "CdsExpandablePanelTitle"), Ob = Lo({ name: "CdsExpandablePanelTitle", props: { ...Ib(), ...th(), ...Wh() }, setup(e2, { slots: t2, expose: n2 }) {
  const o2 = Or(Cb), r2 = Or(xb), s2 = ga(() => (null == r2 ? void 0 : r2.readOnly) || e2.readonly);
  if (!o2)
    throw new Error("[CDS] cds-expandable-panel-title needs to be placed inside v-expandable-panel");
  const a2 = ga(() => ({ collapseIcon: e2.collapseIcon, expanded: Lt(o2.isSelected), disabled: Lt(o2.disabled), readOnly: Lt(s2), expandIcon: e2.expandIcon }));
  return Oh(() => {
    var n3;
    return ba("button", { class: Cp("cds-expandable-panel__title", { "cds-expandable-panel__title--selected": Lt(o2.isSelected), "cds-expandable-panel__title--disabled": Lt(o2.disabled), "cds-expandable-panel__title--readonly": Lt(s2) }), onClick: Lt(s2) ? void 0 : o2.toggle, tabindex: Lt(o2.disabled) ? -1 : void 0, ariaExpanded: Lt(o2.isSelected), disabled: Lt(o2.disabled), type: "button" }, [ba("span", { class: "cds-expandable-panel__title-overlay" }), null == (n3 = t2.default) ? void 0 : n3.call(t2, Lt(a2)), !e2.hideActions && ba("span", { class: "cds-expandable-panel__title-icon" }, [t2.action ? t2.action(Lt(a2)) : ba(Lm, { class: "cds-expandable-panel__title-icon", name: Lt(o2.isSelected) ? e2.collapseIcon : e2.expandIcon })])]);
  }), n2({ ...o2 }), {};
} }), Nb = Lo({ name: "CdsExpandablePanel", props: { ...Ib(), ...Nv(), ...Qf(), ...th(), ...Ff(), ...Wh(), ...Pf(), ...ch() }, emits: { "group:selected": (e2) => true }, setup(e2, { slots: t2, expose: n2 }) {
  const o2 = Av(e2, Cb), r2 = ga(() => Lt(null == o2 ? void 0 : o2.disabled) || e2.disabled), s2 = ga(() => o2.group.items.value.reduce((e3, t3, n3) => (Lt(o2.group.selected).includes(t3.id) && e3.push(n3), e3), [])), a2 = ga(() => {
    const e3 = Lt(o2.group.items).findIndex((e4) => e4.id === o2.id);
    return !Lt(o2.isSelected) && Lt(s2).some((t3) => t3 - e3 == 1);
  }), l2 = ga(() => {
    const e3 = Lt(o2.group.items).findIndex((e4) => e4.id === o2.id);
    return !Lt(o2.isSelected) && Lt(s2).some((t3) => t3 - e3 == -1);
  });
  return Ir(Cb, o2), Oh(() => {
    var n3;
    const s3 = !(!t2.text && !e2.text), i2 = !(!t2.title && !e2.title);
    return ba(e2.tag, { class: Cp("cds-expandable-panel", { "cds-expandable-panel--selected": Lt(o2.isSelected), "cds-expandable-panel--disabled": Lt(r2), "cds-expandable-panel--before-selected": Lt(a2), "cds-expandable-panel--after-selected": Lt(l2) }) }, [i2 && ba(Ob, { key: "title", expandIcon: e2.expandIcon, collapseIcon: e2.collapseIcon, hideActions: e2.hideActions, readonly: e2.readonly }, { default: () => t2.title ? t2.title() : e2.title }), s3 && ba(Eb, { key: "text" }, { default: () => t2.text ? t2.text() : e2.text }), null == (n3 = t2.default) ? void 0 : n3.call(t2)]);
  }), n2({ ...o2, isDisabled: r2 }), {};
} }), Mb = typeof ResizeObserver < "u", Ab = Lo({ name: "CdsLayoutResizeObserver", props: { debounce: { type: [String, Number], default: 100 } }, emits: { resize: (e2) => true }, setup(e2, { emit: t2 }) {
  if (!wd)
    return eu;
  let n2, o2 = null, r2 = { width: -1, height: -1 };
  function s2(t3, n3, r3) {
    r3 || 0 === e2.debounce || "0" === e2.debounce ? a2() : null === o2 && (o2 = setTimeout(a2, Number(e2.debounce)));
  }
  function a2() {
    if (null !== o2 && (clearTimeout(o2), o2 = null), n2) {
      const { offsetWidth: e3, offsetHeight: o3 } = n2;
      (e3 !== r2.width || o3 !== r2.height) && (r2 = { width: e3, height: o3 }, t2("resize", r2));
    }
  }
  const { proxy: l2 } = nu("resize-observer");
  if (!Mb)
    return eu;
  let i2;
  const c2 = (e3) => {
    n2 = null == l2 ? void 0 : l2.$el.parentNode, n2 ? (i2 = new ResizeObserver(s2), i2.observe(n2), a2()) : e3 || un(() => {
      c2(true);
    });
  };
  return Zo(() => {
    c2();
  }), Yo(() => {
    null !== o2 && clearTimeout(o2), void 0 !== i2 && (void 0 !== i2.disconnect ? null == i2 || i2.disconnect() : n2 && i2.unobserve(n2));
  }), eu;
} }), Db = ["both", "horizontal", "vertical"], Vb = Lo({ name: "CdsLayoutScrollObserver", props: { axis: { type: String, validator: (e2) => Db.includes(e2) }, debounce: { type: [String, Number], default: void 0 }, scrollTarget: { type: [String, Object], default: void 0 } }, emits: { scroll: (e2) => true }, setup(e2, { emit: t2, expose: n2 }) {
  if (!wd)
    return eu;
  const o2 = { position: { top: 0, left: 0 }, direction: "down", directionChanged: false, delta: { top: 0, left: 0 }, inflectionPoint: { top: 0, left: 0 } };
  let r2, s2, a2 = null;
  const l2 = () => {
    null !== a2 && a2();
    const n3 = Math.max(0, function(e3) {
      return e3 === window ? window.pageYOffset || window.scrollY || document.body.scrollTop || 0 : null == e3 ? void 0 : e3.scrollTop;
    }(r2)), s3 = function(e3) {
      return e3 === window ? window.pageXOffset || window.scrollX || document.body.scrollLeft || 0 : null == e3 ? void 0 : e3.scrollLeft;
    }(r2), l3 = { top: n3 - o2.position.top, left: s3 - o2.position.left };
    if ("vertical" === e2.axis && 0 === l3.top || "horizontal" === e2.axis && 0 === l3.left)
      return;
    const i3 = Math.abs(l3.top) >= Math.abs(l3.left) ? l3.top < 0 ? "up" : "down" : l3.left < 0 ? "left" : "right";
    o2.position = { top: n3, left: s3 }, o2.directionChanged = o2.direction !== i3, o2.delta = l3, o2.directionChanged && (o2.direction = i3, o2.inflectionPoint = o2.position), t2("scroll", o2);
  }, i2 = () => {
    r2 = qu(s2, e2.scrollTarget), null == r2 || r2.addEventListener("scroll", () => d2(true), { passive: false }), d2(true);
  }, c2 = () => {
    void 0 !== r2 && (r2.removeEventListener("scroll", () => d2(true)), r2 = void 0);
  }, d2 = (t3) => {
    if (t3 || 0 === e2.debounce || "0" === e2.debounce)
      l2();
    else if (null === a2) {
      const [t4, n3] = e2.debounce ? [setTimeout(l2, Number(e2.debounce), clearTimeout)] : [requestAnimationFrame, cancelAnimationFrame];
      a2 = () => {
        null == n3 || n3(t4), a2 = null;
      };
    }
  }, { proxy: u2 } = nu("scroll-observer");
  return Zo(() => {
    var e3;
    s2 = null == (e3 = null == u2 ? void 0 : u2.$el) ? void 0 : e3.parentNode, i2();
  }), Yo(() => {
    null !== a2 && a2(), c2();
  }), ho(() => e2.scrollTarget, () => {
    c2(), i2();
  }), n2({ proxy: u2, trigger: d2, getPosition: () => o2 }), eu;
} }), Tb = "CdsFooter", Lb = Lo({ name: Tb, props: { ...Rv(), ...nh(), ...Xv(), ...Zv() }, emits: { reveal: (e2) => true, focusin: (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const r2 = Fv(), { themeClasses: s2 } = Yv(e2), { invertColorClasses: a2 } = oh(e2, Tb), { transparentClasses: l2 } = Jv(e2, Tb);
  if (!Object.keys(r2).length)
    return () => {
    };
  const { size: i2, revealed: c2, windowHeight: d2, revealOnFocus: u2, classes: p2, style: f2, updateRevealed: h2, updateLocal: v2, onResize: m2 } = $v(e2, Tb, "F"), g2 = nv();
  function y2(e3) {
    Lt(u2) && v2(c2, true), n2("focusin", e3);
  }
  return ho(c2, (e3) => {
    r2.animate(), n2("reveal", e3);
  }), ho([i2, r2.scroll, r2.height], h2), ho(() => g2.height, (e3) => {
    !Lt(r2.isContainer) && v2(d2, e3);
  }), Oh(() => {
    const e3 = Ip(t2.default, [ba(Ab, { debounce: 0, onResize: m2 })]);
    return ba("footer", { class: Cp(Lt(p2), Lt(a2), Lt(l2), Lt(s2)), style: Lt(f2), onFocusin: y2 }, e3);
  }), o2({ ...r2 }), {};
} }), Pb = Lo({ name: "CdsForm", components: { CdsInputLabel: Jm, CdsInputDescription: Qm }, props: { ...Vh(), ...Zv() }, emits: { "update:modelValue": (e2) => true, submit: (e2) => true }, setup(e2, { emit: t2, slots: n2, expose: o2 }) {
  const r2 = function(e3) {
    const t3 = vh(e3, "modelValue"), n3 = ga(() => e3.disabled), o3 = ga(() => e3.readonly), r3 = At(false), s3 = At([]), a3 = At([]);
    return ho(s3, () => {
      let e4 = 0, n4 = 0;
      const o4 = [];
      for (const t4 of s3.value)
        false === t4.isValid ? (n4++, o4.push({ id: t4.id, errorMessages: t4.errorMessages })) : true === t4.isValid && e4++;
      a3.value = o4, t3.value = !(n4 > 0) && (e4 === s3.value.length || null);
    }, { deep: true }), Ir(Th, { register: ({ id: e4, validate: t4, reset: n4, resetValidation: o4 }) => {
      s3.value.some((t5) => t5.id === e4), s3.value.push({ id: e4, validate: t4, reset: n4, resetValidation: o4, isValid: null, errorMessages: [] });
    }, unregister: (e4) => {
      s3.value = s3.value.filter((t4) => t4.id !== e4);
    }, update: (e4, t4, n4) => {
      const o4 = s3.value.find((t5) => t5.id === e4);
      !o4 || (o4.isValid = t4, o4.errorMessages = n4);
    }, isDisabled: n3, isReadonly: o3, isValidating: r3, items: s3, validateOn: jt(e3, "validateOn") }), { errors: a3, isDisabled: n3, isReadonly: o3, isValidating: r3, model: t3, validate: async function() {
      const t4 = [];
      let n4 = true;
      a3.value = [], r3.value = true;
      for (const o4 of s3.value) {
        const r4 = await o4.validate();
        if (r4.length > 0 && (n4 = false, t4.push({ id: o4.id, errorMessages: r4 })), !n4 && e3.firstFail)
          break;
      }
      return a3.value = t4, r3.value = false, { valid: n4, errors: a3.value };
    }, reset: function() {
      s3.value.forEach((e4) => e4.reset()), t3.value = null;
    }, resetValidation: function() {
      s3.value.forEach((e4) => e4.resetValidation()), a3.value = [], t3.value = null;
    } };
  }(e2), s2 = At(), { themeClasses: a2 } = Yv(e2), l2 = ga(() => !!e2.label || !!n2.label), i2 = ga(() => !!e2.description || !!n2.description);
  function c2(e3) {
    e3.preventDefault(), r2.reset();
  }
  function d2(e3) {
    const n3 = e3, o3 = r2.validate();
    n3.then = o3.then.bind(o3), n3.catch = o3.catch.bind(o3), n3.finally = o3.finally.bind(o3), t2("submit", n3), n3.defaultPrevented || o3.then(({ valid: e4 }) => {
      var t3;
      e4 && (null == (t3 = Lt(s2)) || t3.submit());
    }), n3.preventDefault();
  }
  return Oh(() => {
    var t3;
    return ba("form", { ref: s2, novalidate: true, class: Cp("cds-form", Lt(a2)), onReset: c2, onSubmit: d2 }, [Lt(l2) && ba(Jm, { tag: "legend", text: e2.label, class: "cds-form__label" }, { label: n2.label }), Lt(i2) && ba(Qm, { text: e2.description, class: "cds-form__description" }, { description: n2.description }), null == (t3 = n2.default) ? void 0 : t3.call(n2, r2)]);
  }), o2({ ...r2, form: r2, formRef: s2 }), {};
} }), Fb = Lo({ name: "CdsCol", props: { ...ch(), ...Mm() }, setup(e2, t2) {
  const { prefix: n2 } = { prefix: "cds" }, { breakpoints: o2 } = ((e3) => ({ breakpoints: Nm.map((t3) => e3[t3]) }))(e2), r2 = Or(Sm, false), s2 = r2 ? `${n2}--css-grid-column` : `${n2}-col`, a2 = r2 ? ((e3, t3) => {
    const n3 = [];
    if ("number" == typeof e3 || "string" == typeof e3)
      n3.push(`${t3}--col-span-${e3}`);
    else if ("object" == typeof e3) {
      const { span: o3, start: r3, end: s3 } = e3;
      null != o3 && n3.push(`${t3}--col-span-${o3}`), null != r3 && n3.push(`${t3}--col-start-${r3}`), null != s3 && n3.push(`${t3}--col-end-${s3}`);
    }
    return n3.join(" ");
  })(e2.span, n2) : "", l2 = r2 ? ((e3, t3) => {
    const n3 = [];
    for (let o3 = 0; o3 < e3.length; o3++) {
      const r3 = e3[o3];
      if (null == r3)
        continue;
      const s3 = Nm[o3];
      if (true !== r3)
        if ("number" != typeof r3)
          if ("string" == typeof r3 && r3.endsWith("%"))
            n3.push(`${t3}--${s3}:col-span-${r3.slice(0, -1)}`);
          else if ("string" != typeof r3) {
            if ("object" == typeof r3) {
              const { span: e4, offset: o4, start: a3, end: l3 } = r3;
              o4 && o4 > 0 && n3.push(`${t3}--${s3}:col-start-${+o4 + 1}`), a3 && n3.push(`${t3}--${s3}:col-start-${a3}`), l3 && n3.push(`${t3}--${s3}:col-end-${l3}`), "number" == typeof e4 ? n3.push(`${t3}--${s3}:col-span-${e4}`) : e4 && e4.endsWith("%") && n3.push(`${t3}--${s3}:col-span-${e4.slice(0, -1)}`);
            }
          } else
            n3.push(`${t3}--${s3}:col-span-${r3}`);
        else
          n3.push(`${t3}--${s3}:col-span-${r3}`);
      else
        n3.push(`${t3}--${s3}:col-span-auto`);
    }
    return n3.join(" ");
  })(o2, n2) : ((e3, t3) => {
    const n3 = [];
    for (let o3 = 0; o3 < e3.length; o3++) {
      const r3 = e3[o3];
      if (null == r3)
        continue;
      const s3 = Nm[o3];
      if (true === r3 || "string" == typeof r3 && "" === r3)
        n3.push(`${t3}-col-${s3}`);
      else if ("number" != typeof r3 && "string" != typeof r3) {
        if ("object" == typeof r3) {
          const { span: e4, offset: o4, order: a3 } = r3;
          ("number" == typeof e4 || "string" == typeof e4) && n3.push(`${t3}-col-${s3}-${e4}`), (true === e4 || !e4) && n3.push(`${t3}-col-${s3}`), o4 && n3.push(`${t3}-offset-${s3}-${o4}`), a3 && n3.push(`${t3}-order-${s3}-${a3}`);
        }
      } else
        n3.push(`${t3}-col-${s3}-${r3}`);
    }
    return n3.join(" ");
  })(o2, n2);
  return () => ba(e2.tag, { class: { [s2]: r2 || 0 === l2.length, [l2]: !!l2, [a2]: !!a2 } }, t2.slots.default && t2.slots.default());
} });
const Rb = Lo({ name: "CdsGrid", props: { ...ch(), ...Em(), ...Im(), ...Om() }, setup: (e2, t2) => Or(Sm, false) ? function(e3, t3) {
  const { prefix: n2 } = { prefix: "cds" }, { suffix: o2 } = ((e4) => ({ suffix: e4.subgrid ? "subgrid" : "css-grid" }))(e3);
  return () => ba(e3.tag, { class: { [`${n2}--${o2}`]: true, [`${n2}--${o2}--condensed`]: e3.condensed, [`${n2}--${o2}--narrow`]: e3.narrow, [`${n2}--${o2}--wide`]: e3.subgrid || e3.wide } }, t3.slots.default && t3.slots.default());
}(e2, t2) : function(e3, t3) {
  const { prefix: n2 } = { prefix: "cds" };
  return () => ba(e3.tag, { class: { [`${n2}-grid`]: true, [`${n2}-grid-condensed`]: e3.condensed, [`${n2}-grid-narrow`]: e3.narrow, [`${n2}-grid--wide`]: e3.wide } }, t3.slots.default && t3.slots.default());
}(e2, t2) }), $b = Lo({ name: "CdsRow", props: { ...ch(), ...Em() }, setup(e2, t2) {
  const { prefix: n2 } = { prefix: "cds" };
  return () => ba(e2.tag, { class: { [`${n2}-row`]: true, [`${n2}-row-condensed`]: e2.condensed, [`${n2}-row-narrow`]: e2.narrow } }, t2.slots.default && t2.slots.default());
} }), Bb = "CdsHeader", Hb = Lo({ name: Bb, props: { ...Rv(), ...nh(), ...Xv(), ...Zv() }, emits: { reveal: (e2) => true, focusin: (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const r2 = Fv(), { themeClasses: s2 } = Yv(e2), { invertColorClasses: a2 } = oh(e2, Bb), { transparentClasses: l2 } = Jv(e2, Bb);
  if (!Object.keys(r2).length)
    return () => {
    };
  const { revealed: i2, revealOnFocus: c2, classes: d2, style: u2, updateLocal: p2, onResize: f2 } = $v(e2, Bb, "H");
  function h2(e3) {
    Lt(c2) && p2(i2, true), n2("focusin", e3);
  }
  return ho(i2, (e3) => {
    r2.animate(), n2("reveal", e3);
  }), ho(r2.scroll, (t3) => {
    e2.reveal && p2(i2, "up" === t3.direction || t3.position.top <= e2.revealOffset || t3.position.top - t3.inflectionPoint.top < 100);
  }), Oh(() => {
    const e3 = function(e4, t3) {
      if (void 0 !== e4) {
        const t4 = e4();
        if (null != t4)
          return t4.slice();
      }
      return t3;
    }(t2.default, []);
    return e3.push(ba(Ab, { debounce: 0, onResize: f2 })), ba("header", { class: Cp(Lt(d2), Lt(a2), Lt(l2), Lt(s2)), style: Lt(u2), onFocusin: h2 }, e3);
  }), o2({ ...r2 }), {};
} }), jb = Lo({ name: "CdsLayout", props: { container: { type: Boolean, default: false }, view: { type: String, default: "hhh lpr fff", validator: (e2) => /^(h|l)h(h|r) lpr (f|l)f(f|r)$/.test(e2.toLowerCase()) }, ...Zv() }, emits: { resize: (e2) => true, scroll: (e2) => true, scrollHeight: (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const r2 = nv(), { themeClasses: s2 } = Yv(e2), a2 = At(), l2 = r2.height, i2 = e2.container ? At(0) : r2.width, c2 = At({ position: { top: 0, left: 0 }, direction: "down", directionChanged: false, delta: { top: 0, left: 0 }, inflectionPoint: { top: 0, left: 0 } }), d2 = At(0), u2 = At(Qu()), p2 = ga(() => "cds-layout cds-layout--" + (e2.container ? "containerized" : "standard")), f2 = ga(() => e2.container ? null : { minHeight: Lt(l2) + "px" }), h2 = ga(() => 0 !== Lt(u2) ? { right: `${Lt(u2)}px` } : null), v2 = ga(() => 0 !== u2.value ? { left: 0, right: `-${Lt(u2)}px`, width: `calc(100% + ${Lt(u2)}px)` } : null);
  function m2(t3) {
    (e2.container || !document.documentElement.classList.contains(xd)) && (c2.value = { position: t3.position, direction: t3.direction, directionChanged: t3.directionChanged, inflectionPoint: t3.inflectionPoint, delta: t3.delta }, n2("scroll", Lt(c2)));
  }
  function g2(t3) {
    const { width: o3, height: r3 } = t3;
    let s3 = false;
    Lt(l2) !== r3 && (s3 = true, l2.value = r3, void 0 !== e2.onScrollHeight && n2("scrollHeight", r3), b2()), Lt(i2) !== o3 && (s3 = true, i2.value = o3), s3 && n2("resize", t3);
  }
  function y2({ height: e3 }) {
    Lt(d2) !== e3 && (d2.value = e3, b2());
  }
  function b2() {
    if (e2.container) {
      const e3 = l2.value > d2.value ? Qu() : 0;
      u2.value !== e3 && (u2.value = e3);
    }
  }
  let w2 = null;
  const _2 = { instances: {}, view: ga(() => e2.view), isContainer: ga(() => e2.container), rootRef: a2, height: l2, containerHeight: d2, scrollbarWidth: u2, totalWidth: ga(() => Lt(i2) + Lt(u2)), rows: ga(() => {
    const t3 = e2.view.toLowerCase().split(" ");
    return { top: t3[0].split(""), middle: t3[1].split(""), bottom: t3[2].split("") };
  }), header: vt({ size: 0, offset: 0, space: false }), footer: vt({ size: 0, offset: 0, space: false }), right: vt({ size: Xy, offset: 0, space: false }), left: vt({ size: Xy, offset: 0, space: false }), scroll: c2, animate() {
    null !== w2 ? clearTimeout(w2) : document.body.classList.add("cds-body--layout-animate"), w2 = setTimeout(() => {
      w2 = null, document.body.classList.remove("cds-body--layout-animate");
    }, 155);
  }, update(e3, t3, n3) {
    _2[e3][t3] = n3;
  } };
  if (((e3) => {
    Ir(Pv, e3);
  })(_2), wd && Qu() > 0) {
    let t3 = null;
    const n3 = document.body, o3 = () => {
      t3 = null, n3.classList.remove("cds-hide-scrollbar");
    }, s3 = () => {
      if (null === t3) {
        if (n3.scrollHeight > Lt(r2.height))
          return;
        n3.classList.add("cds-hide-scrollbar");
      } else
        clearTimeout(t3);
      t3 = setTimeout(o3, 300);
    }, a3 = (e3) => {
      null !== t3 && "remove" === e3 && (clearTimeout(t3), o3()), window[`${e3}EventListener`]("resize", s3);
    };
    ho(() => e2.container ? "remove" : "add", a3), !e2.container && a3("add"), Go(() => {
      a3("remove");
    });
  }
  return Oh(() => {
    const n3 = Ip(t2.default, [ba(Vb, { onScroll: m2 }), ba(Ab, { onResize: g2 })]), o3 = ba("div", { ref: e2.container ? void 0 : a2, class: Cp(Lt(p2), Lt(s2)), style: Lt(f2), tabindex: -1 }, n3);
    return e2.container ? ba("div", { class: "cds-layout__container", ref: a2 }, [ba(Ab, { onResize: y2 }), ba("div", { class: "cds-layout-container__target", style: Lt(h2) }, [ba("div", { class: "cds-scrollable", style: Lt(v2) }, [o3])])]) : o3;
  }), o2({ ..._2 }), {};
} }), Kb = Symbol("cds:main"), zb = Lo({ name: "CdsMain", setup(e2, { slots: t2, expose: n2 }) {
  const o2 = Fv();
  if (!Object.keys(o2).length)
    return () => {
    };
  ((e3) => {
    Ir(Kb, e3);
  })(true);
  const r2 = ga(() => {
    const e3 = {};
    return o2.header.space && (e3.paddingTop = o2.header.size + "px"), o2.right.space && (e3.paddingRight = o2.right.size + "px"), o2.footer.space && (e3.paddingBottom = o2.footer.size + "px"), o2.left.space && (e3.paddingLeft = o2.left.size + "px"), e3;
  });
  return n2({ ...o2 }), Oh(() => ba("main", { class: "cds-main", style: Lt(r2) }, Ep(t2.default, []))), {};
} }), Zb = "CdsModal", Ub = Lo({ name: Zb, props: { fullscreen: { type: Boolean, default: false }, scrollable: { type: Boolean, default: false }, retainFocus: { type: Boolean, default: true }, dismissBtn: { type: Boolean, default: false }, ...om(), ...Xh(), ...Wh(), ...Gh(), ...Zf(), ...Xf(), ...Zv(), ...Uh({ zIndex: 2500 }), ...hm({ origin: "center center" }), ...Cm({ scrollStrategy: "block" }), ...zh({ transition: { component: $m } }) }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const r2 = vh(e2, "modelValue"), { themeClasses: s2 } = Yv(e2), { sizeClasses: a2 } = Jf(e2, Zb), { scopeId: l2 } = av(), i2 = At();
  function c2(e3) {
    var t3, n3;
    const o3 = e3.relatedTarget, r3 = e3.target;
    if (o3 !== r3 && (null == (t3 = Lt(i2)) ? void 0 : t3.contentEl) && (null == (n3 = Lt(i2)) ? void 0 : n3.globalTop) && ![document, Lt(i2).contentEl].includes(r3) && !Lt(i2).contentEl.contains(r3)) {
      const e4 = Xd(Lt(i2).contentEl);
      if (!e4.length)
        return;
      const t4 = e4[0], n4 = e4[e4.length - 1];
      o3 === t4 ? n4.focus() : t4.focus();
    }
  }
  wd && ho(() => Lt(r2) && e2.retainFocus, (e3) => {
    e3 ? document.addEventListener("focusin", c2) : document.removeEventListener("focusin", c2);
  }, { immediate: true }), ho(r2, async (e3) => {
    var t3, n3;
    await un(), e3 ? null == (t3 = Lt(i2).contentEl) || t3.focus({ preventScroll: true }) : null == (n3 = Lt(i2).activatorEl) || n3.focus({ preventScroll: true });
  });
  const d2 = ga(() => Us({ "aria-haspopup": "dialog", "aria-expanded": String(Lt(r2)) }, e2.activatorProps)), u2 = ga(() => ({ icon: "remove", appearance: "transparent", class: "cds-modal__dismiss-btn", onClick: () => r2.value = false }));
  return Oh(() => {
    const o3 = Gd(e2, ["fullscreen", "retainFocus", "scrollable", "dismissBtn"]);
    return ba(Kg, { ref: i2, class: Cp("cds-modal", Lt(a2), Lt(s2), { "cds-modal--fullscreen": e2.fullscreen, "cds-modal--scrollable": e2.scrollable, "cds-modal--loading": e2.loading, "cds-modal--has-dismiss-btn": e2.dismissBtn }), ...o3, activatorProps: Lt(d2), modalValue: Lt(r2), "onUpdate:modelValue": (e3) => {
      r2.value = e3, n2("update:modelValue", e3);
    }, "aria-modal": "true", role: "dialog", ...l2 }, { activator: t2.activator, default: (...n3) => ba(My, { class: "cds-modal__container" }, { default: () => ba(Ay, {}, { default: () => {
      var o4, r3;
      return [e2.loading && ba(Gg, { fixed: true, overlay: true }), e2.dismissBtn ? ba(my, Lt(u2)) : null == (o4 = t2.dismissBtn) ? void 0 : o4.call(t2, { props: Lt(u2) }), null == (r3 = t2.default) ? void 0 : r3.call(t2, ...n3)];
    } }) }) });
  }), o2({ overlay: i2 }), {};
} }), Wb = tu({ icon: { type: [Boolean, String, Function, Object], default: null }, customIcon: { type: String, default: void 0, validator: (e2) => !e2 || Hf.includes(e2) }, closable: { type: Boolean, default: false }, closeLabel: { type: String, default: "\u0417\u0430\u043A\u0440\u044B\u0442\u044C" }, closeIcon: { type: String, default: "remove", validator: (e2) => Hf.includes(e2) } }, "notification");
const Yb = "CdsNotification", Gb = Lo({ name: Yb, props: { modelValue: { type: Boolean, default: true }, ...Ff(), ...Pf(), ...Wb(), ...Lv(), ...lh(), ...ch(), ...Ev(), ...Zv() }, emits: { "click:close": (e2) => true, "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2 }) {
  const { themeClasses: o2 } = Yv(e2), r2 = vh(e2, "modelValue"), { icon: s2 } = function(e3) {
    return { icon: ga(() => {
      if (false !== e3.icon)
        return e3.customIcon ? e3.customIcon : "warning" === e3.status ? "alert-fill" : "success" === e3.status ? "check-fill" : "progress" === e3.status ? "refresh" : `${e3.status}-fill`;
    }) };
  }(e2), { notification: a2 } = Iv(e2), { variantClasses: l2 } = function(e3, t3) {
    return { variantClasses: ga(() => {
      const { variant: n3 } = Lt(e3);
      return `${ku(t3)}__variant--${n3}`;
    }) };
  }(e2, Yb), { statusClasses: i2 } = ih(e2, Yb), c2 = ga(() => ({ appearance: "transparent", icon: e2.closeIcon, "aria-label": a2.closeLabel, onClick(e3) {
    r2.value = false, n2("click:close", e3);
  } }));
  return () => {
    var n3, a3, d2, u2, p2, f2, h2, v2;
    const m2 = !(!t2.prepend && !Lt(s2)), g2 = !!t2.append, y2 = !(!t2.title && !e2.title), b2 = !!t2.actions, w2 = !(!t2.close && !e2.closable);
    return Lt(r2) && ba(e2.tag, { role: "alert", class: Cp("cds-notification", Lt(l2), Lt(i2), Lt(o2)) }, [m2 && ba("div", { key: "prepend", class: "cds-notification__prepend" }, t2.prepend || ba(Lm, { name: Lt(s2) })), ba("div", { class: "cds-notification__content" }, [y2 && ba("div", { key: "title", class: "cds-notification__title" }, null != (a3 = null == (n3 = t2.title) ? void 0 : n3.call(t2)) ? a3 : e2.title), null != (u2 = null == (d2 = t2.text) ? void 0 : d2.call(t2)) ? u2 : e2.text, null == (p2 = t2.default) ? void 0 : p2.call(t2), b2 && ba("div", { key: "actions", class: "cds-notification__actions" }, null == (f2 = t2.actions) ? void 0 : f2.call(t2))]), g2 && ba("div", { key: "append", class: "cds-notification__append" }, null == (h2 = t2.append) ? void 0 : h2.call(t2)), w2 && ba("div", { key: "close", class: "cds-notification__close" }, t2.close ? null == (v2 = t2.close) ? void 0 : v2.call(t2, { props: Lt(c2) }) : ba(my, { ...Lt(c2) }))]);
  };
} });
var qb = function() {
  return qb = Object.assign || function(e2) {
    for (var t2, n2 = 1, o2 = arguments.length; n2 < o2; n2++)
      for (var r2 in t2 = arguments[n2])
        Object.prototype.hasOwnProperty.call(t2, r2) && (e2[r2] = t2[r2]);
    return e2;
  }, qb.apply(this, arguments);
};
function Xb(e2) {
  return e2.toLowerCase();
}
var Jb = [/([a-z0-9])([A-Z])/g, /([A-Z])([A-Z][a-z])/g], Qb = /[^A-Z0-9]+/gi;
function ew(e2, t2, n2) {
  return t2 instanceof RegExp ? e2.replace(t2, n2) : t2.reduce(function(e3, t3) {
    return e3.replace(t3, n2);
  }, e2);
}
function tw(e2, t2) {
  return void 0 === t2 && (t2 = {}), function(e3, t3) {
    void 0 === t3 && (t3 = {});
    for (var n2 = t3.splitRegexp, o2 = void 0 === n2 ? Jb : n2, r2 = t3.stripRegexp, s2 = void 0 === r2 ? Qb : r2, a2 = t3.transform, l2 = void 0 === a2 ? Xb : a2, i2 = t3.delimiter, c2 = void 0 === i2 ? " " : i2, d2 = ew(ew(e3, o2, "$1\0$2"), s2, "\0"), u2 = 0, p2 = d2.length; "\0" === d2.charAt(u2); )
      u2++;
    for (; "\0" === d2.charAt(p2 - 1); )
      p2--;
    return d2.slice(u2, p2).split("\0").map(l2).join(c2);
  }(e2, qb({ delimiter: "." }, t2));
}
function nw(e2, t2) {
  return void 0 === t2 && (t2 = {}), tw(e2, qb({ delimiter: "-" }, t2));
}
const ow = "cds-notification-alert__wrapper", rw = "CdsNotificationAlert", sw = Lo({ name: rw, props: { timeout: { type: [Number, String], default: 5e3 }, ...Ff(), ...Pf(), ...Wb(), ...Lv(), ...lh(), ...ch(), ...Ev(), ...Uh(), ...om(), ...Wh(), ...Gh(), ...Xh(), ...Zv(), ...hm({ location: "top" }), ...zh({ transition: "notification-transition" }) }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const r2 = vh(e2, "modelValue"), { themeClasses: s2 } = Yv(e2), { locationStyles: a2 } = gm(e2, false, void 0, ow), { scopeId: l2 } = av(), i2 = At();
  ho(r2, d2), ho(() => e2.timeout, d2), Zo(() => {
    Lt(r2) && d2();
  });
  let c2 = -1;
  function d2() {
    const t3 = Number(e2.timeout);
    0 !== t3 && (window.clearTimeout(c2), Lt(r2) && -1 !== t3 && (c2 = window.setTimeout(() => {
      r2.value = false;
    }, t3)));
  }
  function u2() {
    window.clearTimeout(c2);
  }
  return Oh(() => {
    const o3 = Gd(e2, ["tag", "timeout", "variant", "status", "text", "title", "icon", "customIcon", "closable", "closeLabel", "closeIcon", "locale"]);
    return ba(Kg, { class: Cp(nw(rw), Lt(s2), { "cds-notification-alert--active": Lt(r2) }), ...o3, modelValue: Lt(r2), "onUpdate:modelValue": (e3) => {
      r2.value = e3, n2("update:modelValue", e3);
    }, contentProps: Us({ class: Cp(ow), style: Lt(a2), "data-alert": ow, onPointerenter: u2, onPointerleave: d2 }, o3.contentProps), persistent: true, scrim: false, noClickAnimation: true, scrollStrategy: "none", ...l2 }, { activator: t2.activator, default: () => ba(Gb, { title: e2.title, text: e2.text, variant: e2.variant, status: e2.status, tag: e2.tag, locale: e2.locale, icon: e2.icon, customIcon: e2.customIcon, closable: e2.closable, closeLabel: e2.closeLabel, closeIcon: e2.closeIcon, "onClick:close": () => {
      r2.value = false, n2("update:modelValue", false);
    } }, { title: t2.title, default: t2.default, text: t2.text, prepend: t2.prepend, append: t2.append, close: t2.close, actions: t2.actions }) });
  }), o2({ overlay: i2 }), {};
} }), aw = Lo({ name: "CdsPage", props: { padding: { type: Boolean, default: false }, styleFn: { type: Function, default: void 0 } }, emits: {}, setup(e2, { slots: t2, expose: n2 }) {
  const o2 = Fv(), r2 = Or(Kb, false), s2 = nv(), a2 = Lt(s2.height);
  if (!r2)
    return () => {
    };
  const l2 = ga(() => {
    const t3 = (o2.header.space ? o2.header.size : 0) + (o2.footer.space ? o2.footer.size : 0);
    if ("function" == typeof e2.styleFn) {
      const n3 = Lt(o2.isContainer) ? Lt(o2.containerHeight) : Lt(s2.height);
      return e2.styleFn(t3, n3);
    }
    return { minHeight: Lt(o2.isContainer) ? Lt(o2.containerHeight) - t3 + "px" : 0 === Lt(s2.height) ? 0 !== t3 ? `calc(100vh - ${t3}px)` : "100vh" : a2 - t3 + "px" };
  }), i2 = ga(() => ({ "cds-page": true, "cds-page--padding": e2.padding }));
  return Oh(() => ba("div", { class: Lt(i2), style: Lt(l2) }, Ep(t2.default, []))), n2({ layout: o2 }), {};
} }), lw = Lo({ name: "CdsPagination", props: { start: { type: [Number, String], default: 1 }, length: { type: [Number, String], default: 1, validator: (e2) => e2 % 1 == 0 }, totalVisible: { type: [Number, String], default: void 0 }, ellipsis: { type: String, default: "..." }, modelValue: { type: Number, default: (e2) => e2.start }, ...ch({ tag: "nav" }), ...Qf(), ...Zv() }, emits: { "update:modelValue": (e2) => true, first: (e2) => true, last: (e2) => true, next: (e2) => true, prev: (e2) => true }, setup(e2, { slots: t2, emit: n2, expose: o2 }) {
  const { themeClasses: r2 } = Yv(e2), s2 = vh(e2, "modelValue"), { width: a2 } = nv(), l2 = Dt(-1), { refs: i2, updateRef: c2 } = function() {
    const e3 = At([]);
    return Uo(() => e3.value = []), { refs: e3, updateRef: function(t3, n3) {
      e3.value[n3] = t3;
    } };
  }(), { resizeRef: d2 } = bv((e3) => {
    if (!e3.length)
      return;
    const { target: t3, contentRect: n3 } = e3[0], o3 = t3.querySelector(".cds-pagination__list > *");
    if (!o3)
      return;
    const r3 = n3.width, s3 = o3.offsetWidth + 2 * parseFloat(getComputedStyle(o3).marginRight);
    l2.value = g2(r3, s3);
  }), u2 = ga(() => parseInt(e2.length, 10)), p2 = ga(() => parseInt(e2.start, 10)), f2 = ga(() => e2.totalVisible ? parseInt(e2.totalVisible, 10) : Lt(l2) >= 0 ? Lt(l2) : g2(Lt(a2), 32)), h2 = ga(() => {
    if (Lt(u2) <= 0 || isNaN(Lt(u2)) || Lt(u2) > Number.MAX_SAFE_INTEGER)
      return [];
    if (Lt(f2) <= 1)
      return [Lt(s2)];
    if (Lt(u2) <= Lt(f2))
      return Kd(Lt(u2), Lt(p2));
    const t3 = Lt(f2) % 2 == 0, n3 = t3 ? Lt(f2) / 2 : Math.floor(Lt(f2) / 2), o3 = t3 ? n3 : n3 + 1, r3 = Lt(u2) - n3;
    if (o3 - Lt(s2) >= 0)
      return [...Kd(Math.max(1, Lt(f2) - 1), Lt(p2)), e2.ellipsis, Lt(u2)];
    if (Lt(s2) - r3 >= (t3 ? 1 : 0)) {
      const t4 = Lt(f2) - 1, n4 = Lt(u2) - t4 + Lt(p2);
      return [Lt(p2), e2.ellipsis, ...Kd(t4, n4)];
    }
    {
      const t4 = Math.max(1, Lt(f2) - 3), n4 = 1 === t4 ? Lt(s2) : Lt(s2) - Math.ceil(t4 / 2) + Lt(p2);
      return [Lt(p2), e2.ellipsis, ...Kd(t4, n4), e2.ellipsis, Lt(u2)];
    }
  }), v2 = ga(() => Lt(h2).map((t3, n3) => {
    const o3 = (e3) => c2(e3, n3);
    return "string" == typeof t3 ? { isActive: false, key: `ellipsis-${n3}`, page: t3, props: { ref: o3, appearance: "transparent", readonly: true, class: "cds-pagination__ellipsis" } } : { isActive: t3 === Lt(s2), key: t3, page: t3, props: { ref: o3, appearance: "transparent", disabled: !!e2.disabled || +e2.length < 2, onClick: () => w2(t3) } };
  })), m2 = ga(() => ({ prev: { appearance: "transparent", icon: "chevron-left", disabled: !!e2.disabled || Lt(s2) <= Lt(p2), onClick: C2 }, next: { appearance: "transparent", icon: "chevron-right", disabled: !!e2.disabled || Lt(s2) >= Lt(p2) + Lt(u2) - 1, onClick: _2 } }));
  function g2(e3, t3) {
    return Math.max(0, Math.floor(+((e3 - 3 * t3) / t3).toFixed(2)));
  }
  function y2() {
    var e3;
    const t3 = Lt(s2) - Lt(p2);
    null == (e3 = Lt(i2)[t3]) || e3.$el.focus();
  }
  function b2(t3) {
    t3.key === kd.left && !e2.disabled && Lt(s2) > +e2.start ? (s2.value = s2.value - 1, un(y2)) : t3.key === kd.right && !e2.disabled && Lt(s2) < Lt(p2) + Lt(u2) - 1 && (s2.value = s2.value + 1, un(y2));
  }
  function w2(e3, t3) {
    s2.value = e3, t3 && n2(t3, e3);
  }
  function _2() {
    w2(Lt(s2) + 1, "next");
  }
  function C2() {
    w2(Lt(s2) - 1, "prev");
  }
  return Oh(() => ba(e2.tag, { ref: d2, class: Cp("cds-pagination", Lt(r2)), role: "navigation", onKeydown: b2 }, ba("ul", { class: "cds-pagination__list" }, [ba("li", { key: "prev", class: "cds-pagination__prev" }, t2.prev ? t2.prev(Lt(m2).prev) : ba(my, Lt(m2).prev)), ...Lt(v2).map((e3) => ba("li", { key: e3.key, class: { "cds-pagination__item": true, "cds-pagination__item--active": e3.isActive } }, t2.item ? t2.item(e3) : ba(my, e3.props, () => e3.page))), ba("li", { key: "prev", class: "cds-pagination__next" }, t2.next ? t2.next(Lt(m2).next) : ba(my, Lt(m2).next))]))), o2({ setFirst: function() {
    w2(Lt(p2), "first");
  }, setLast: function() {
    w2(Lt(p2) + Lt(u2), "last");
  }, setNext: _2, setPrev: C2 }), {};
} }), iw = Lo({ name: "CdsProgress", props: { modelValue: { type: [Number, String], default: 0 }, absolute: { type: Boolean, default: false }, active: { type: Boolean, default: true }, bgColor: { type: String, default: void 0 }, bgOpacity: { type: [Number, String], default: void 0 }, bufferValue: { type: [Number, String], default: 0 }, height: { type: [Number, String], default: 4 }, indeterminate: { type: Boolean, default: false }, clickable: { type: Boolean, default: false }, max: { type: [Number, String], default: 100 }, stream: { type: Boolean, default: false }, reverse: { type: Boolean, default: false }, ...ch(), ...Zv(), ...vm({ location: "top" }) }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, emit: n2 }) {
  const o2 = vh(e2, "modelValue"), { themeClasses: r2 } = Yv(e2), { locationStyles: s2 } = gm(e2), { intersectionRef: a2, isIntersection: l2 } = function(e3, t3) {
    const n3 = At(), o3 = Dt(false);
    if (_d) {
      const r3 = new IntersectionObserver((t4) => {
        null == e3 || e3(t4, r3), o3.value = !!t4.find((e4) => e4.isIntersecting);
      }, t3);
      Yo(() => {
        r3.disconnect();
      }), ho(n3, (e4, t4) => {
        t4 && (r3.unobserve(t4), o3.value = false), e4 && r3.observe(e4);
      }, { flush: "post" });
    }
    return { intersectionRef: n3, isIntersection: o3 };
  }(), i2 = ga(() => parseInt(e2.max, 10)), c2 = ga(() => parseInt(e2.height, 10)), d2 = ga(() => parseFloat(e2.bufferValue) / Lt(i2) * 100), u2 = ga(() => parseFloat(Lt(o2)) / Lt(i2) * 100), p2 = ga(() => Math.round(Lt(u2))), f2 = ga(() => e2.indeterminate ? "fade-transition" : "slide-transition-x"), h2 = ga(() => null == e2.bgOpacity ? e2.bgOpacity : parseFloat(e2.bgOpacity));
  function v2(t3) {
    if (!Lt(a2))
      return;
    const { left: n3, right: r3, width: s3 } = Lt(a2).getBoundingClientRect(), l3 = e2.reverse ? s3 - t3.clientX + (r3 - s3) : t3.clientX - n3;
    o2.value = Math.round(l3 / s3 * Lt(i2));
  }
  return ho(o2, () => {
    n2("update:modelValue", Lt(p2));
  }), Oh(() => ba(e2.tag, { ref: a2, role: "progressbar", class: Cp("cds-progress", Lt(r2), { "cds-progress--absolute": e2.absolute, "cds-progress--active": e2.active && Lt(l2), "cds-progress-reverse": e2.reverse }), style: { bottom: "bottom" === e2.location ? 0 : void 0, top: "top" === e2.location ? 0 : void 0, height: e2.active ? Bd(Lt(c2)) : 0, "--cds-progress-height": Bd(Lt(c2)), ...Lt(s2) }, "aria-hidden": e2.active ? "false" : "true", "aria-valuemin": 0, "aira-valuemax": e2.max, "aria-valuenow": e2.indeterminate ? void 0 : Lt(u2), onClick: e2.clickable && v2 }, [e2.stream && ba("div", { key: "stream", class: Cp("cds-progress__stream"), style: { [e2.reverse ? "left" : "right"]: Bd(-Lt(c2)), borderTop: `${Bd(Lt(c2) / 2)} dotted`, opacity: Lt(h2), top: `calc(50%  - ${Bd(Lt(c2) / 4)})`, width: Bd(100 - Lt(d2), "%"), "--cds-progress-stream-to": Bd(Lt(c2) * (e2.reverse ? 1 : -1)) } }), ba("div", { class: Cp("cds-progress__background"), style: { backgroundColor: e2.bgOpacity, opacity: Lt(h2), width: Bd(e2.stream ? Lt(u2) : 100, "%") } }), ba(Oa, { name: Lt(f2) }, { default: () => e2.indeterminate ? ba("div", { class: Cp("cds-progress__indeterminate") }, ["long", "short"].map((t3) => ba("div", { key: "bar", class: Cp("cds-progress__indeterminate", t3), style: { backgroundColor: e2.bgColor } }))) : ba("div", { class: Cp("cds-progress__determinate"), style: { backgroundColor: e2.bgColor, width: Bd(Lt(u2), "%") } }) }), t2.default && ba("div", { class: "cds-progress__content" }, t2.default({ value: Lt(u2), buffer: Lt(d2) }))])), {};
} });
const cw = Tm(iw, [["render", function(e2, t2, n2, o2, r2, s2) {
  return "Component: CdsProgress";
}]]), dw = { model: At(null), isGroup: At(false), disabledGroup: At(false), readonlyGroup: At(false), errorGroup: At(false), handleChangeRadio: () => {
} }, uw = Symbol.for("cds:radio"), pw = "CdsRadioButton", fw = Lo({ name: pw, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm }, props: { ...Nh(), ...Fh(), ...$h({ direction: "vertical" }), ...Qf(), ...th(), ...Zv() }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2 }) {
  const n2 = vh(e2, "modelValue", () => []), { themeClasses: o2 } = Yv(e2), { hasLabel: r2, hasDescription: s2 } = Mh(e2, t2, pw), { directionClasses: a2 } = Bh(e2, pw), { inputProps: l2 } = Ah(e2, ["direction"]);
  return Ir(uw, { model: n2, isGroup: At(true), disabledGroup: At(e2.disabled), readonlyGroup: At(e2.readonly), errorGroup: At(e2.error), handleChangeRadio: (e3) => {
    n2.value = e3;
  } }), { inputProps: l2, hasLabel: r2, hasDescription: s2, directionClasses: a2, themeClasses: o2 };
} }), hw = { class: "cds-radio-button__controls" };
const vw = Tm(fw, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-input");
  return Ss(), Ms(i2, Us({ id: e2.id, class: ["cds-radio-button", [e2.directionClasses, e2.themeClasses]] }, { ...e2.inputProps }), { messages: Wn(() => [nr(e2.$slots, "messages")]), default: Wn(() => [e2.hasLabel ? (Ss(), Ms(a2, { key: 0, text: e2.label, tag: "div" }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["text"])) : js("", true), e2.hasDescription ? (Ss(), Ms(l2, { key: 1, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true), Ls("div", hw, [nr(e2.$slots, "default")])]), _: 3 }, 16, ["id", "class"]);
}]]), mw = "CdsRadio", gw = Lo({ name: mw, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm }, props: { ...Nh(), ...Fh({ hideMessages: true }), ...mh(), ...Qf(), ...th() }, emits: { "update:focused": (e2) => true }, setup(e2, { attrs: t2, slots: n2, expose: o2 }) {
  const { id: r2, hasLabel: s2, hasDescription: a2 } = Mh(e2, n2, "radio"), { isFocused: l2, focus: i2, blur: c2 } = gh(e2, mw), { rootAttrs: d2, inputAttrs: u2 } = Dh(t2), { inputProps: p2 } = Ah(e2, ["hideMessages"]), f2 = At(), { isGroup: h2, model: v2, disabledGroup: m2, readonlyGroup: g2, errorGroup: y2, handleChangeRadio: b2 } = Or(uw, dw), w2 = ga(() => Lt(m2) || e2.disabled), _2 = ga(() => Lt(g2) || e2.readonly), C2 = ga(() => Lt(y2) || e2.error), x2 = ga(() => Lt(h2) || e2.hideMessages), k2 = ga(() => Lt(v2) === e2.value), S2 = ga(() => ({ "cds-radio--disabled": Lt(w2), "cds-radio--checked": Lt(v2) }));
  return o2({ id: r2, validationRef: f2, isGroup: h2 }), { id: r2, model: v2, checked: k2, hasLabel: s2, hasDescription: a2, validationRef: f2, isGroup: h2, isFocused: l2, inputProps: p2, rootAttrs: d2, inputAttrs: u2, internalDisabled: w2, internalReadonly: _2, internalError: C2, internalHideMessages: x2, radioClasses: S2, onBlur: c2, onFocus: i2, onChange: (e3) => {
    const t3 = e3.target.value;
    b2(t3);
  } };
} }), yw = ["id", "name", "checked", "disabled", "aria-disabled", "aria-checked", "aria-invalid", "value"];
const bw = Tm(gw, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-input");
  return Ss(), Ms(i2, Us({ id: e2.id, ref: "validationRef", focused: e2.isFocused, class: ["cds-radio", e2.radioClasses] }, { ...e2.inputProps, ...e2.rootAttrs }, { "hide-messages": e2.internalHideMessages }), { default: Wn(({ isDisabled: n3, isReadonly: o3, isValid: r3 }) => [Ls("input", Us({ id: e2.id, type: "radio", name: e2.name, checked: e2.checked, disabled: e2.internalDisabled || e2.internalReadonly || n3.value || o3.value, "aria-disabled": e2.internalDisabled || e2.internalReadonly || n3.value || o3.value, "aria-checked": e2.checked, "aria-invalid": e2.internalError || false === r3.value, value: e2.value }, e2.inputAttrs, { onBlur: t2[0] || (t2[0] = (...t3) => e2.onBlur && e2.onBlur(...t3)), onFocus: t2[1] || (t2[1] = (...t3) => e2.onFocus && e2.onFocus(...t3)), onChange: t2[2] || (t2[2] = (...t3) => e2.onChange && e2.onChange(...t3)) }), null, 16, yw), e2.hasLabel ? (Ss(), Ms(a2, { key: 0, for: e2.id, text: e2.label }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["for", "text"])) : js("", true), e2.hasDescription ? (Ss(), Ms(l2, { key: 1, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true)]), messages: Wn(() => [nr(e2.$slots, "messages")]), _: 3 }, 16, ["id", "focused", "class", "hide-messages"]);
}]]), ww = "CdsSelect", _w = Lo({ name: ww, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm, CdsIcon: Lm, CdsLoader: Gg, CdsTransition: Zh }, props: { ...jh(), ...Nh(), ...Fh(), ...jf(), ...Kf(), ...mh(), ...Qf(), ...th(), ...Zf(), ...dh(), ...Xf(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "click:clear": (e2) => true, "click:prepend": (e2) => true, "click:append": (e2) => true, "click:prependInner": (e2) => true, "click:appendInner": (e2) => true }, setup(e2, { slots: t2, attrs: n2, emit: o2, expose: r2 }) {
  const s2 = vh(e2, "modelValue"), { themeClasses: a2 } = Yv(e2), { id: l2, hasLabel: i2, hasDescription: c2 } = Mh(e2, t2, ww), { hasPrepend: d2, hasAppend: u2, iconClasses: p2 } = zf(e2, t2, ww), { hasLoading: f2, loadingClasses: h2 } = Uf(e2), { isFocused: v2, focus: m2, blur: g2 } = gh(e2, ww), { hasPrependInnerIcon: y2, hasAppendInnerIcon: b2, fieldClasses: w2, fieldProps: _2, handleClickPrependInner: C2, handleClickAppendInner: x2 } = Kh(e2, t2, ww), { items: k2, itemsProps: S2 } = uh(e2), { inputProps: E2 } = Ah(e2, [..._2, ...S2, "modelValue", "focused", "loading"]), { rootAttrs: I2, inputAttrs: O2 } = Dh(n2), N2 = ga(() => !!Lt(s2) || e2.active), M2 = ga(() => e2.clearable), A2 = At(), D2 = At(), V2 = At(), T2 = ga(() => ({ "cds-select--active": Lt(N2), "cds-select--has-prefix": !!e2.prefix, "cds-select--has-suffix": !!e2.suffix })), L2 = ga(() => ({ selected: Lt(s2) })), P2 = () => {
    var e3;
    Lt(D2) !== document.activeElement && (null == (e3 = Lt(D2)) || e3.focus()), v2.value || m2();
  };
  return r2({ id: l2, fieldRef: A2, validationRef: V2, selectRef: D2 }), { id: l2, model: s2, items: k2, optionProps: L2, fieldRef: A2, selectRef: D2, validationRef: V2, isFocused: v2, rootAttrs: I2, inputAttrs: O2, inputProps: E2, hasLabel: i2, hasDescription: c2, hasPrepend: d2, hasAppend: u2, hasPrependInnerIcon: y2, hasAppendInnerIcon: b2, hasLoading: f2, hasClear: M2, fieldClasses: w2, selectClasses: T2, iconClasses: p2, loadingClasses: h2, themeClasses: a2, onInput: (e3) => {
    s2.value = e3.target.value;
  }, onBlur: g2, onFocus: P2, onClear: (e3) => {
    e3.stopPropagation(), P2(), un(() => {
      s2.value = null, o2("click:clear", e3);
    });
  }, handleClickPrependInner: C2, handleClickAppendInner: x2 };
} }), Cw = ["id", "value", "disabled", "aria-invalid", "autofocus"], xw = { value: "", disabled: "", hidden: "", selected: "" }, kw = ["value", "label", "disabled", "selected"], Sw = ["label"], Ew = ["value", "label", "disabled", "selected"], Iw = { key: 2, class: "cds-select__clear" }, Ow = { class: "cds-select__arrow" };
const Nw = Tm(_w, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-icon"), c2 = ro("cds-loader"), d2 = ro("cds-transition"), u2 = ro("cds-input");
  return Ss(), Ns("div", Us({ ref: "fieldRef", class: ["cds-select-field", e2.themeClasses] }, e2.rootAttrs), [e2.hasLabel ? (Ss(), Ms(a2, { key: 0, for: e2.id, text: e2.label }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["for", "text"])) : js("", true), e2.hasDescription ? (Ss(), Ms(l2, { key: 1, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true), Ps(u2, Us({ id: e2.id, ref: "validationRef", modelValue: e2.model, "onUpdate:modelValue": t2[8] || (t2[8] = (t3) => e2.model = t3), focused: e2.isFocused, class: ["cds-select", [e2.selectClasses, e2.fieldClasses, e2.iconClasses, e2.loadingClasses]] }, { ...e2.inputProps }), tr({ default: Wn(({ isDisabled: n3, isReadonly: o3, isValid: r3 }) => [e2.hasPrependInnerIcon ? (Ss(), Ns("div", { key: 0, class: "cds-select__prepend-inner", onClick: t2[0] || (t2[0] = (...t3) => e2.handleClickPrependInner && e2.handleClickPrependInner(...t3)) }, [nr(e2.$slots, "prepend-inner"), e2.prependInnerIcon ? (Ss(), Ms(i2, { key: 0, name: e2.prependInnerIcon, size: e2.size }, null, 8, ["name", "size"])) : js("", true)])) : js("", true), e2.prefix ? (Ss(), Ns("span", { key: 1, class: "cds-select__prefix", onClick: t2[1] || (t2[1] = (...t3) => e2.onFocus && e2.onFocus(...t3)) }, Q(e2.prefix), 1)) : js("", true), Ls("select", Us({ id: e2.id, ref: "selectRef", value: e2.model, disabled: n3.value, "aria-invalid": e2.error || false === r3.value, autofocus: e2.autofocus }, { ...e2.inputAttrs }, { onInput: t2[2] || (t2[2] = (...t3) => e2.onInput && e2.onInput(...t3)), onFocus: t2[3] || (t2[3] = (...t3) => e2.onFocus && e2.onFocus(...t3)), onBlur: t2[4] || (t2[4] = (...t3) => e2.onBlur && e2.onBlur(...t3)) }), [Ls("option", xw, Q(e2.placeholder), 1), (Ss(true), Ns(bs, null, er(e2.items, (t3, n4) => (Ss(), Ns(bs, { key: `${t3.value}-${n4}` }, [t3.children ? (Ss(), Ns("optgroup", { key: 1, label: t3.title }, [(Ss(true), Ns(bs, null, er(t3.children, (t4, n5) => (Ss(), Ns("option", { key: `${t4.value}-${n5}`, value: t4.value, label: t4.title, disabled: t4.props.disabled || o3.value, selected: t4.value === e2.model }, null, 8, Ew))), 128))], 8, Sw)) : (Ss(), Ns("option", { key: 0, value: t3.value, label: t3.title, disabled: t3.props.disabled || o3.value, selected: t3.value === e2.model }, null, 8, kw))], 64))), 128)), nr(e2.$slots, "default", W(Rs({ ...e2.optionProps, disabled: n3.value || o3.value })))], 16, Cw), e2.hasClear ? (Ss(), Ns("div", Iw, [Ls("button", { type: "button", title: "\u041E\u0447\u0438\u0441\u0442\u0438\u0442\u044C \u043F\u043E\u043B\u0435 \u0432\u0432\u043E\u0434\u0430", class: U(["cds-select__clear--button", { active: !!e2.model }]), onClick: t2[5] || (t2[5] = (...t3) => e2.onClear && e2.onClear(...t3)) }, [Ps(i2, { name: "remove", size: e2.size }, null, 8, ["size"])], 2)])) : js("", true), e2.suffix ? (Ss(), Ns("span", { key: 3, class: "cds-select__suffix", onClick: t2[6] || (t2[6] = (...t3) => e2.onFocus && e2.onFocus(...t3)) }, Q(e2.suffix), 1)) : js("", true), e2.hasAppendInnerIcon || e2.hasLoading ? (Ss(), Ns("div", { key: 4, class: "cds-select__append-inner", onClick: t2[7] || (t2[7] = (...t3) => e2.handleClickAppendInner && e2.handleClickAppendInner(...t3)) }, [Ps(d2, { transition: "fade-transition", role: "alert", "aria-busy": "true" }, { default: Wn(() => [e2.hasLoading ? (Ss(), Ms(c2, { key: 0, size: "xs", class: "cds-select__loader" })) : js("", true)]), _: 1 }), nr(e2.$slots, "append-inner"), e2.appendInnerIcon ? (Ss(), Ms(i2, { key: 0, name: e2.appendInnerIcon, size: e2.size }, null, 8, ["name", "size"])) : js("", true)])) : js("", true), Ls("div", Ow, [Ps(i2, { name: "chevron-down", size: e2.size }, null, 8, ["size"])])]), messages: Wn(() => [nr(e2.$slots, "messages")]), _: 2 }, [e2.hasPrepend ? { name: "prepend", fn: Wn(() => [nr(e2.$slots, "prepend")]), key: "0" } : void 0, e2.hasAppend ? { name: "append", fn: Wn(() => [nr(e2.$slots, "append")]), key: "1" } : void 0]), 1040, ["id", "modelValue", "focused", "class"])], 16);
}]]), Mw = "CdsSidebar", Aw = Lo({ name: Mw, components: { CdsButton: my, CdsTransitionExpandX: Km }, inheritAttrs: false, props: { modelValue: { type: Boolean, default: false }, disabledOverlay: { type: Boolean, default: false }, ...Xf(), ...Ff(), ...Zv() }, emits: { "update:modelValue": (e2) => true, "click:outside": (e2) => true }, setup(e2, { attrs: t2, slots: n2, emit: o2, expose: r2 }) {
  const { themeClasses: s2 } = Yv(e2), { sizeClasses: a2 } = Jf(e2, Mw), l2 = ga(() => !e2.disabledOverlay && e2.modelValue), i2 = () => {
    o2("update:modelValue", false);
  }, c2 = () => {
    o2("update:modelValue", true);
  };
  return ho(() => e2.modelValue, (e3) => {
    e3 ? c2() : i2();
  }), r2({ show: c2, hide: i2 }), { attrs: t2, sizeClasses: a2, themeClasses: s2, showOverlay: l2, hasToolbarSlot: ga(() => !!n2.toolbar), hasAppendSlot: ga(() => !!n2.append), hide: i2, onClickOutside: (e3) => {
    o2("click:outside", e3);
  } };
} }), Dw = { class: "cds-sidebar__header" }, Vw = { class: "cds-sidebar__header--title" }, Tw = { key: 0, class: "cds-sidebar__toolbar" }, Lw = { class: "cds-sidebar__content" }, Pw = { key: 1, class: "cds-sidebar__append" };
const Fw = Tm(Aw, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-button"), l2 = ro("cds-transition-expand-x");
  return Ss(), Ns(bs, null, [Ps(l2, null, { default: Wn(() => [e2.modelValue ? (Ss(), Ns("div", Us({ key: 0 }, e2.attrs, { class: ["cds-sidebar", [e2.sizeClasses, e2.themeClasses]] }), [Ls("div", Dw, [Ls("h5", Vw, Q(e2.title), 1), Ps(a2, { icon: "remove", appearance: "transparent", onClick: e2.hide }, null, 8, ["onClick"])]), e2.hasToolbarSlot ? (Ss(), Ns("div", Tw, [nr(e2.$slots, "toolbar")])) : js("", true), Ls("div", Lw, [nr(e2.$slots, "default")]), e2.hasAppendSlot ? (Ss(), Ns("div", Pw, [nr(e2.$slots, "append")])) : js("", true)], 16)) : js("", true)]), _: 3 }), e2.showOverlay ? (Ss(), Ns("div", { key: 0, class: "cds-sidebar__overlay", onClick: t2[0] || (t2[0] = (...t3) => e2.onClickOutside && e2.onClickOutside(...t3)) })) : js("", true)], 64);
}]]), Rw = { actions: "button@2", article: "heading, paragraph", avatar: "avatar", button: "button", card: "block, heading", "card-avatar": "block, list-item-avatar", chip: "chip", "date-picker": "date-picker-options, date-picker-days, list-item", "date-picker-options": "actions", "date-picker-days": "avatar@42", divider: "divider", heading: "heading", block: "block", "list-item": "text", "list-item-avatar": "avatar, text", "list-item-two-line": "sentences", "list-item-avatar-two-line": "avatar, sentences", "list-item-three-line": "paragraph", "list-item-avatar-three-line": "avatar, paragraph", paragraph: "text@3", sentences: "text@2", subtitle: "text", table: "table-heading, table-thead, table-tbody, table-tfoot", "table-heading": "chip, text", "table-thead": "heading@6", "table-tbody": "table-row-divider@6", "table-row-divider": "table-row, divider", "table-row": "text@6", "table-tfoot": "text@2, actions", text: "text" };
function $w(e2) {
  let t2 = [];
  if (!e2)
    return t2;
  const n2 = Rw[e2];
  if (e2 !== n2) {
    if (e2.includes(","))
      return jw(e2);
    if (e2.includes("@"))
      return Hw(e2);
    n2.includes(",") ? t2 = jw(n2) : n2.includes("@") ? t2 = Hw(n2) : n2 && t2.push($w(n2));
  }
  return [Bw(e2, t2)];
}
function Bw(e2, t2 = []) {
  return ba("div", { class: Cp("cds-skeleton-loader__bone", `cds-skeleton-loader__${e2}`) }, t2);
}
function Hw(e2) {
  const [t2, n2] = e2.split("@");
  return Array.from({ length: n2 }).map(() => $w(t2));
}
function jw(e2) {
  return e2.replace(/\s/g, "").split(",").map($w);
}
const Kw = Lo({ name: "CdsSkeletonLoader", props: { type: { type: [String, Array], default: "block" }, loading: { type: Boolean, default: false }, loadingText: { type: String, default: void 0 }, boilerplate: { type: Boolean, default: false }, ...Xh(), ...Zv() }, emits: {}, setup(e2, { slots: t2 }) {
  const { dimensionsStyles: n2 } = Jh(e2), { themeClasses: o2 } = Yv(e2), r2 = ga(() => $w(Td(e2.type).join(",")));
  return Oh(() => {
    var s2;
    const a2 = !t2.default || e2.loading;
    return ba("div", { class: Cp("cds-skeleton-loader", Lt(o2), { "cds-skeleton-loader--is-loading": a2, "cds-skeleton-loader--boilerplate": e2.boilerplate }), style: a2 ? Lt(n2) : {}, role: e2.boilerplate ? void 0 : "alert", "aria-busy": e2.boilerplate ? void 0 : a2, "aria-live": e2.boilerplate ? void 0 : "polite", "aria-label": e2.boilerplate ? void 0 : e2.loadingText }, a2 ? Lt(r2) : null == (s2 = t2.default) ? void 0 : s2.call(t2));
  }), {};
} }), zw = Lo({ name: "CdsStages", props: { current: { type: [String, Number], default: null }, finished: { type: Boolean, default: false }, ...dh(), ...Zv() }, setup(e2, { expose: t2 }) {
  const { items: n2 } = uh(e2), { themeClasses: o2 } = Yv(e2), r2 = ga(() => Lt(n2).findIndex((t3) => String(t3.value) === String(e2.current))), s2 = ga(() => `grid-template-columns: repeat(${Lt(n2).length}, 1fr);`), a2 = ga(() => e2.finished || "0" === String(e2.current)), l2 = (e3) => e3 === Lt(r2);
  return t2({ isCurrent: l2, isFinished: a2 }), { getStageClasses: (t3, n3) => {
    var o3;
    return { [Sd.active]: !Lt(a2) && String(t3.value) === String(e2.current), [Sd.invalid]: (null == (o3 = t3.props) ? void 0 : o3.error) && n3 <= r2.value, [Sd.checked]: Lt(a2) || n3 < r2.value };
  }, isCurrent: l2, isFinished: a2, hasError: (e3) => {
    var t3;
    return null == (t3 = e3.props) ? void 0 : t3.error;
  }, innerStyle: s2, themeClasses: o2 };
} }), Zw = { key: 0, class: "cds-stages__arrow cds-stages__arrow--before", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 11 32", width: "11", height: "32" }, Uw = { key: 0, "fill-rule": "evenodd", d: "M12 0v2H3.235l7.001 14-7.001 14H12v2H0l8-16L0 0z" }, Ww = { key: 1, d: "M12 0v1H1.618l7.5 15-7.5 15H12v1H0l8-16L0 0z" }, Yw = { class: "cds-stage__text" }, Gw = { key: 1, class: "cds-stages__arrow cds-stages__arrow--after", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 11 32", width: "11", height: "32" }, qw = { key: 0, "fill-rule": "evenodd", d: "M2.5 0l8 16-8 16h-2v-2h.765l7-14L1.264 2H.5V0z" }, Xw = { key: 1, d: "M1 0l8 16-8 16H0v-1h.383l7.499-15-7.5-15H0V0z" };
const Jw = Tm(zw, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("ul", { class: U(["cds-stages", e2.themeClasses]), style: H(e2.innerStyle) }, [(Ss(true), Ns(bs, null, er(e2.items, (t3, n3) => (Ss(), Ns("li", { key: t3.value, class: U(["cds-stage", e2.getStageClasses(t3, n3)]) }, [0 !== n3 ? (Ss(), Ns("svg", Zw, [!e2.isCurrent(n3) || e2.hasError(t3) || e2.isFinished ? (Ss(), Ns("path", Ww)) : (Ss(), Ns("path", Uw))])) : js("", true), Ls("span", Yw, Q(t3.title), 1), n3 !== e2.items.length - 1 ? (Ss(), Ns("svg", Gw, [!e2.isCurrent(n3) || e2.hasError(t3) || e2.isFinished ? (Ss(), Ns("path", Xw)) : (Ss(), Ns("path", qw))])) : js("", true)], 2))), 128))], 6);
}]]), Qw = "CdsStatus", e_ = Lo({ name: Qw, props: { ...Pf(), ...Yf(), ...lh(), ...Zv() }, setup(e2, { slots: t2 }) {
  const { themeClasses: n2 } = Yv(e2), { appearanceClasses: o2 } = Gf(e2, Qw), { statusClasses: r2 } = ih(e2, Qw);
  return !e2.text && !t2.default && _a(`[CDS] 'text' is a required prop for ${Qw}`), { themeClasses: n2, appearanceClasses: o2, statusClasses: r2 };
} }), t_ = { class: "cds-status__text" };
const n_ = Tm(e_, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("span", { class: U(["cds-status", [e2.themeClasses, e2.appearanceClasses, e2.statusClasses]]) }, [Ls("span", t_, [e2.text ? (Ss(), Ns(bs, { key: 0 }, [Hs(Q(e2.text), 1)], 64)) : nr(e2.$slots, "default", { key: 1 })])], 2);
}]]), o_ = Symbol.for("cds:steps-group"), r_ = Symbol.for("cds:steps"), s_ = Hd("20rem"), a_ = Lo({ name: "CdsSteps", directives: { Touch: $g }, props: { vertical: { type: Boolean, default: false }, mobile: { type: Boolean, default: false }, touch: { type: [Boolean, Object], default: void 0 }, ...Qf(), ...th(), ...Ov(), ...Zv(), ...jf({ prependIcon: "chevron-left", appendIcon: "chevron-right" }) }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, expose: n2 }) {
  const { themeClasses: o2 } = Yv(e2), r2 = Dv(e2, o_), s2 = At(), { mobile: a2, readonly: l2 } = $t(e2), i2 = () => {
    if (Lt(s2)) {
      const e3 = Lt(s2).scrollLeft + s_, t3 = e3 === Lt(r2.items).length * s_ ? 0 : e3;
      Lt(s2).scrollTo({ left: t3, behavior: "smooth" });
    }
  }, c2 = () => {
    if (Lt(s2)) {
      const e3 = Lt(s2).scrollLeft - s_, t3 = e3 < 0 ? Lt(r2.items).length * s_ - s_ : e3;
      Lt(s2).scrollTo({ left: t3, behavior: "smooth" });
    }
  }, d2 = ga(() => e2.mobile ? false === e2.touch ? e2.touch : { left: i2, right: c2, start: ({ originalEvent: e3 }) => {
    e3.stopPropagation();
  }, ...true === e2.touch ? {} : e2.touch } : void 0);
  return ((e3) => {
    Ir(r_, e3);
  })({ mobile: a2, readonly: l2 }), Oh(() => {
    var n3;
    const r3 = ba("ul", { ref: s2, class: Cp("cds-steps", Lt(o2), { "cds-steps--vertical": e2.vertical && !e2.mobile, "cds-steps--disabled": e2.disabled, "cds-steps--mobile": e2.mobile }) }, [null == (n3 = t2.default) ? void 0 : n3.call(t2)]), a3 = ba("span", { class: Cp("cds-steps__prev") }, t2.prev ? t2.prev({ stepPrev: c2 }) : ba(my, { icon: e2.prependIcon, appearance: "transparent", onClick: c2 })), l3 = ba("span", { class: Cp("cds-steps__next") }, t2.next ? t2.next({ stepNext: i2 }) : ba(my, { icon: e2.appendIcon, appearance: "transparent", onClick: i2 }));
    return e2.mobile ? wo(ba("div", { class: "cds-steps__mobile-container" }, [a3, l3, r3]), [[$g, Lt(d2)]]) : r3;
  }), n2({ stepsRef: s2, stepNext: i2, stepPrev: c2 }), {};
} }), l_ = {}, i_ = [Ls("svg", { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16", width: "16", height: "16" }, [Ls("path", { "fill-rule": "evenodd", d: "M8 1a7 7 0 110 14A7 7 0 018 1zm0 .933a6.067 6.067 0 100 12.134A6.067 6.067 0 008 1.933zM8 4a4 4 0 110 8 4 4 0 010-8z" })], -1)];
const c_ = Tm(l_, [["render", function(e2, t2) {
  return Ss(), Ns("i", null, i_);
}]]), d_ = {}, u_ = [Ls("svg", { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16", width: "16", height: "16" }, [Ls("path", { "fill-rule": "evenodd", d: "M8 1a7 7 0 110 14A7 7 0 018 1zm0 1a6 6 0 100 12A6 6 0 008 2z" })], -1)];
const p_ = Tm(d_, [["render", function(e2, t2) {
  return Ss(), Ns("i", null, u_);
}]]), f_ = Lo({ name: "CdsStepsItem", props: { error: { type: Boolean, default: false }, ...Nv(), ...th(), ...Qf(), ...Ff(), ...Pf() }, emits: { "group:selected": (e2) => true }, setup(e2, { slots: t2 }) {
  const { disabledClasses: n2 } = eh(e2), o2 = Av(e2, o_), r2 = Or(r_, {}), s2 = ga(() => Lt(e2.disabled)), a2 = ga(() => Lt(e2.error)), { hasTitle: l2 } = Rf(e2, t2), { hasText: i2 } = function(e3, t3) {
    return { hasText: ga(() => !!e3.text || !!t3.text) };
  }(e2, t2), c2 = ga(() => Lt(o2.isSelected)), d2 = ga(() => Lt(o2.group.items).findIndex((e3) => e3.id === o2.id)), u2 = ga(() => Lt(o2.group.items).findIndex((e3) => e3.id === Lt(o2.group.selected)[0]) > Lt(d2)), p2 = ga(() => Lt(d2) === Lt(o2.group.items).length - 1), f2 = ga(() => 0 === Lt(d2)), h2 = ga(() => ({ selected: Lt(c2), disabled: Lt(s2), complete: Lt(u2), error: Lt(a2), last: Lt(p2), first: Lt(f2) })), v2 = () => {
    Lt(r2.readonly) || e2.readonly || o2.select(true);
  };
  Oh(() => {
    const o3 = ga(() => {
      const e3 = Cp("cds-steps__item-icon");
      return Lt(c2) ? ba(c_, { class: e3 }) : Lt(a2) ? ba(Lm, { name: "error", class: e3 }) : Lt(u2) ? ba(Lm, { name: "check", class: e3 }) : ba(p_, { class: e3 });
    }), s3 = t2.icon ? t2.icon(Lt(h2)) : Lt(o3);
    return ba("li", { class: Cp("cds-steps__item", Lt(n2), { "cds-steps__item--complete": Lt(u2), "cds-steps__item--current": Lt(c2), [Sd.invalid]: Lt(a2) }), tabindex: 0, onClick: v2 }, [Lt(null == r2 ? void 0 : r2.mobile) ? null : ba("div", { class: Cp("cds-steps__item-status") }, [s3, !Lt(p2) && ba("span", { class: Cp("cds-steps__item-indicator") })]), ba("div", { class: "cds-steps__item-content" }, [l2 && ba(Lt(null == r2 ? void 0 : r2.mobile) ? "div" : "span", { class: Cp("cds-steps__item-title") }, Lt(null == r2 ? void 0 : r2.mobile) ? [ba("div", { class: Cp("cds-steps__item-mobile-title", { "cds-steps__item-mobile-title--visible": Lt(null == r2 ? void 0 : r2.mobile) }) }, t2.title ? ba("span", {}, [s3, t2.title(Lt(h2))]) : ba("span", {}, [s3, e2.title]))] : { default: () => t2.title ? t2.title(Lt(h2)) : e2.title }), i2 && ba("span", { class: Cp("cds-steps__item-text") }, { default: () => t2.text ? t2.text(Lt(h2)) : e2.text })])]);
  });
} }), h_ = "CDS_TABLE_INTERNAL_COL_DEFINE", v_ = "CDS_TABLE_INTERNAL_HOOK", m_ = tu({ id: { type: String, default: void 0 }, data: { type: Array, default: void 0 }, columns: { type: Array, default: void 0 }, rowKey: { type: [String, Function], default: "key" }, rowClassName: { type: [String, Function], default: void 0 }, tableLayout: { type: String, default: void 0 }, showHeader: { type: Boolean, default: void 0 }, sticky: { type: [Boolean, Object], default: void 0 }, loading: { type: Boolean, default: false }, hovered: { type: Boolean, default: false }, bordered: { type: Boolean, default: false }, borderless: { type: Boolean, default: false }, header: { type: Function, default: void 0 }, footer: { type: Function, default: void 0 }, components: { type: Object, default: void 0 }, customRow: { type: Function, default: void 0 }, customHeaderRow: { type: Function, default: void 0 }, pagination: { type: [Object, Boolean], default: void 0 }, scroll: { type: Object, default: void 0 }, sortDirection: { type: Array, default: void 0 }, showSorterTooltip: { type: [Boolean, Object], default: true }, expandFixed: { type: [Boolean, String], default: void 0 }, expandColumnWidth: { type: Number, default: void 0 }, expandedRowKeys: { type: Array, default: void 0 }, rowSelection: { type: Object, default: void 0 }, defaultExpandedRowKeys: { type: Array, default: void 0 }, defaultExpandAllRows: { type: Boolean, default: false }, canExpandable: { type: Boolean, default: false }, expandedRowRender: { type: Function, default: void 0 }, expandRowByClick: { type: Boolean, default: false }, expandIconColumnIndex: { type: Number, default: void 0 }, expandIcon: { type: Function, default: void 0 }, indentSize: { type: Number, default: 32 }, showExpandColumn: { type: Boolean, default: void 0 }, expandedRowClassName: { type: Function, default: void 0 }, childrenColumnName: { type: String, default: "children" }, rowExpandable: { type: Function, default: void 0 }, transformCellText: { type: Function, default: void 0 }, getPopupContainer: { type: Function, default: void 0 }, onChange: { type: Function, default: void 0 }, onResizeColumn: { type: Function, default: void 0 }, onExpand: { type: Function, default: void 0 }, onExpandedRowsChange: { type: Function, default: void 0 } }, "table");
function g_(e2, t2, n2) {
  const o2 = Dt({});
  return ho([e2, t2, n2], () => {
    const r2 = /* @__PURE__ */ new Map(), s2 = n2.value, a2 = t2.value, l2 = (e3) => {
      e3.forEach((e4, t3) => {
        const n3 = s2(e4, t3);
        r2.set(n3, e4), e4 && "object" == typeof e4 && a2 in e4 && l2(e4[a2] || []);
      });
    };
    l2(e2.value), o2.value = { kvMap: r2 };
  }, { deep: true, immediate: true }), { getRecordByKey: function(e3) {
    return o2.value.kvMap.get(e3);
  } };
}
const y_ = "CDS_TABLE_KEY";
function b_(...e2) {
  const t2 = {};
  function n2(e3, t3) {
    t3 && Object.keys(t3).forEach((o2) => {
      const r2 = t3[o2];
      r2 && "object" == typeof r2 ? (e3[o2] = e3[o2] || {}, n2(e3[o2], r2)) : e3[o2] = r2;
    });
  }
  return e2.forEach((e3) => {
    n2(t2, e3);
  }), t2;
}
function w_(e2, t2) {
  if (!t2 && "number" != typeof t2)
    return e2;
  const n2 = x_(t2);
  let o2 = e2;
  for (let e3 = 0; e3 < n2.length; e3 += 1) {
    if (!o2)
      return null;
    o2 = o2[n2[e3]];
  }
  return o2;
}
function __(e2) {
  const t2 = [], n2 = {};
  return e2.forEach((e3) => {
    const { key: o2, dataIndex: r2 } = e3 || {};
    let s2 = o2 || x_(r2).join("-") || y_;
    for (; n2[s2]; )
      s2 = `${s2}_next`;
    n2[s2] = true, t2.push(s2);
  }), t2;
}
function C_(e2) {
  return null != e2;
}
function x_(e2) {
  return null == e2 ? [] : Array.isArray(e2) ? e2 : [e2];
}
function k_(e2 = []) {
  const t2 = pu(e2), n2 = [];
  return t2.forEach((e3) => {
    var t3, o2, r2, s2;
    if (!e3)
      return;
    const a2 = e3.key, l2 = (null == (t3 = e3.props) ? void 0 : t3.style) || {}, i2 = (null == (o2 = e3.props) ? void 0 : o2.class) || "", c2 = e3.props || {};
    for (const [e4, t4] of Object.entries(c2))
      c2[N(e4)] = t4;
    const { default: d2, ...u2 } = e3.children || {}, p2 = { ...u2, ...c2, style: l2, class: i2 };
    if (a2 && (p2.key = a2), null != (r2 = e3.type) && r2.__CDS_TABLE_COLUMN_GROUP)
      p2.children = k_("function" == typeof d2 ? d2() : d2);
    else {
      const t4 = null == (s2 = e3.children) ? void 0 : s2.default;
      p2.customRender = p2.customRender || t4;
    }
    n2.push(p2);
  }), n2;
}
function S_(e2, t2) {
  return "key" in e2 && void 0 !== e2.key && null !== e2.key ? e2.key : e2.dataIndex ? Array.isArray(e2.dataIndex) ? e2.dataIndex.join(".") : e2.dataIndex : t2;
}
function E_(e2, t2) {
  return t2 ? `${t2}-${e2}` : `${e2}`;
}
function I_(e2, t2) {
  return "function" == typeof e2 ? e2(t2) : e2;
}
function O_(e2, t2, n2, o2) {
  const r2 = n2[e2] || {}, s2 = n2[t2] || {};
  let a2, l2;
  "left" === r2.fixed ? a2 = o2.left[e2] : "right" === s2.fixed && (l2 = o2.right[t2]);
  let i2 = false, c2 = false;
  const d2 = n2[t2 + 1], u2 = n2[e2 - 1];
  return void 0 !== a2 ? i2 = !(d2 && "left" === d2.fixed) : void 0 !== l2 && (c2 = !(u2 && "right" === u2.fixed)), { fixLeft: a2, fixRight: l2, lastFixLeft: i2, firstFixRight: c2, lastFixRight: false, firstFixLeft: false, isSticky: o2.isSticky };
}
function N_(e2, t2, n2, o2) {
  var r2;
  const s2 = null == (r2 = e2[t2]) ? void 0 : r2.call(e2, n2);
  return M_(s2) ? s2 : null == o2 ? void 0 : o2();
}
function M_(e2) {
  return (e2 || []).some((e3) => !As(e3) || e3.type !== _s && !(e3.type === bs && !M_(e3.children))) ? e2 : null;
}
const A_ = tu({ treeData: { type: Array, default: void 0 }, selectable: { type: Boolean, default: false }, multiple: { type: Boolean, default: false }, checkable: { type: Boolean, default: false }, draggable: { type: [Object, Boolean], default: false }, checkedKeys: { type: [Object, Array], default: void 0 }, expandedKeys: { type: Array, default: void 0 }, selectedKeys: { type: Array, default: void 0 }, autoExpandParent: { type: Boolean, default: false }, defaultExpandedKeys: { type: Array, default: () => [] }, defaultExpandParent: { type: Boolean, default: true }, defaultExpandAll: { type: Boolean, default: false }, defaultSelectedKeys: { type: Array, default: void 0 }, expandAction: { type: [Boolean, String], default: void 0 }, checkStrictly: { type: Boolean, default: false }, allowDrop: { type: Function, default: () => true }, blockNode: { type: Boolean, default: false }, fieldNames: { type: Object, default: void 0 }, showLine: { type: [Boolean, Object], default: void 0 }, showIcon: { type: Boolean, default: false }, filterTreeNode: { type: Function, default: void 0 }, loadData: { type: Function, default: void 0 }, loadedKeys: { type: Array, default: () => [] }, icon: { type: Function, default: void 0 }, switcherIcon: { type: Function, default: void 0 }, openAnimation: { type: Function, default: void 0 }, virtual: { type: Boolean, default: true }, height: { type: Number, default: void 0 } }, "tree");
function D_(e2) {
  const t2 = At(0), n2 = Dt();
  return po(() => {
    const o2 = /* @__PURE__ */ new Map(), r2 = e2.value || {};
    let s2 = 0;
    for (const e3 in r2)
      if (Object.prototype.hasOwnProperty.call(r2, e3)) {
        const t3 = r2[e3], { level: n3 } = t3;
        let a2 = o2.get(n3);
        a2 || (a2 = /* @__PURE__ */ new Set(), o2.set(n3, a2)), a2.add(t3), s2 = Math.max(s2, n3);
      }
    t2.value = s2, n2.value = o2;
  }), { maxLevel: t2, levelEntities: n2 };
}
const V_ = tu({ eventKey: { type: [String, Number], default: "" }, prefixCls: String, title: [Function, String], data: { type: Object, default: void 0 }, parent: { type: Object, default: void 0 }, isStart: { type: Array }, isEnd: { type: Array }, active: { type: Boolean, default: void 0 }, onMousemove: { type: Function }, isLeaf: { type: Boolean, default: void 0 }, checkable: { type: Boolean, default: void 0 }, selectable: { type: Boolean, default: void 0 }, disabled: { type: Boolean, default: void 0 }, disableCheckbox: { type: Boolean, default: void 0 }, icon: [Function], switcherIcon: [Function], domRef: { type: Function } }, "tree-node"), T_ = tu({ onFocus: { type: Function, default: void 0 }, onBlur: { type: Function, default: void 0 }, onKeydown: { type: Function, default: void 0 }, onContextmenu: { type: Function, default: void 0 }, onClick: { type: Function, default: void 0 }, onDblclick: { type: Function, default: void 0 }, onScroll: { type: Function, default: void 0 }, onExpand: { type: Function, default: void 0 }, onCheck: { type: Function, default: void 0 }, onSelect: { type: Function, default: void 0 }, onLoad: { type: Function, default: void 0 }, onMouseenter: { type: Function, default: void 0 }, onMouseleave: { type: Function, default: void 0 }, onRightClick: { type: Function, default: void 0 }, onDragstart: { type: Function, default: void 0 }, onDragenter: { type: Function, default: void 0 }, onDragover: { type: Function, default: void 0 }, onDragleave: { type: Function, default: void 0 }, onDragend: { type: Function, default: void 0 }, onDrop: { type: Function, default: void 0 }, onActiveChange: { type: Function, default: void 0 } }, "tree-events");
function L_(e2, { initWrapper: t2, processEntity: n2, onProcessFinished: o2, externalGetKey: r2, childrenPropName: s2, fieldNames: a2 } = {}) {
  const l2 = {}, i2 = {};
  let c2 = { posEntities: l2, keyEntities: i2 };
  return t2 && (c2 = t2(c2) || c2), function(e3, t3, n3) {
    let o3 = {};
    o3 = "object" == typeof n3 ? n3 : { externalGetKey: n3 }, o3 = o3 || {};
    const { childrenPropName: r3, externalGetKey: s3, fieldNames: a3 } = o3, { key: l3, children: i3 } = R_(a3), c3 = r3 || i3;
    let d2;
    s3 ? "string" == typeof s3 ? d2 = (e4) => e4[s3] : "function" == typeof s3 && (d2 = (e4) => s3(e4)) : d2 = (e4, t4) => K_(e4[l3], t4);
    const u2 = (n4, o4, r4, s4) => {
      const a4 = n4 ? n4[c3] : e3, l4 = n4 ? z_(r4 && r4.pos, o4) : "0", i4 = n4 ? [...s4 || [], n4] : [];
      if (n4) {
        const e4 = d2(n4, l4);
        t3({ node: n4, index: o4, pos: l4, key: e4, parentPos: r4 && r4.node ? r4.pos : null, level: r4 && r4.level + 1, nodes: i4 });
      }
      a4 && a4.forEach((e4, t4) => {
        u2(e4, t4, { node: n4, pos: l4, level: r4 ? r4.level + 1 : -1 }, i4);
      });
    };
    u2(null);
  }(e2, (e3) => {
    const { node: t3, index: o3, pos: r3, key: s3, parentPos: a3, level: d2, nodes: u2 } = e3, p2 = { node: t3, index: o3, key: s3, pos: r3, level: d2, nodes: u2 }, f2 = K_(s3, r3);
    l2[r3] = p2, i2[f2] = p2, p2.parent = l2[a3], p2.parent && (p2.parent.children = p2.parent.children || [], p2.parent.children.push(p2)), n2 && n2(p2, c2);
  }, { externalGetKey: r2, childrenPropName: s2, fieldNames: a2 }), o2 && o2(c2), c2;
}
function P_(e2) {
  return function e3(t2 = []) {
    return fu(t2).map((t3) => {
      var n2, o2, r2, s2;
      if (!function(e4) {
        return e4 && e4.type && e4.type.isTreeNode;
      }(t3))
        return null;
      const a2 = t3.children || {}, l2 = t3.key, i2 = {};
      for (const [e4, n3] of Object.entries(t3.props))
        i2[N(e4)] = n3;
      const { isLeaf: c2, checkable: d2, selectable: u2, disabled: p2, disableCheckbox: f2 } = i2, h2 = { isLeaf: c2 || "" === c2 || void 0, checkable: d2 || "" === d2 || void 0, selectable: u2 || "" === u2 || void 0, disabled: p2 || "" === p2 || void 0, disableCheckbox: f2 || "" === f2 || void 0 }, v2 = { ...i2, ...h2 }, { title: m2 = null == (n2 = a2.title) ? void 0 : n2.call(a2, v2), icon: g2 = null == (o2 = a2.icon) ? void 0 : o2.call(a2, v2), switcherIcon: y2 = null == (r2 = a2.switcherIcon) ? void 0 : r2.call(a2, v2), ...b2 } = i2, w2 = null == (s2 = a2.default) ? void 0 : s2.call(a2), _2 = { ...b2, title: m2, icon: g2, switcherIcon: y2, key: l2, ...h2 }, C2 = e3(w2);
      return C2.length && (_2.children = C2), _2;
    });
  }(e2);
}
function F_(e2, t2) {
  const n2 = /* @__PURE__ */ new Set();
  function o2(e3) {
    if (n2.has(e3))
      return;
    const r2 = t2[e3];
    if (!r2)
      return;
    n2.add(e3);
    const { parent: s2, node: a2 } = r2;
    a2.disabled || s2 && o2(s2.key);
  }
  return (e2 || []).forEach((e3) => {
    o2(e3);
  }), [...n2];
}
function R_(e2) {
  const { title: t2, _title: n2, key: o2, children: r2 } = e2 || {}, s2 = t2 || "title";
  return { title: s2, _title: n2 || [s2], key: o2 || "key", children: r2 || "children" };
}
function $_(e2, { expandedKeysSet: t2, selectedKeysSet: n2, loadedKeysSet: o2, loadingKeysSet: r2, checkedKeysSet: s2, halfCheckedKeysSet: a2, dragOverNodeKey: l2, dropPosition: i2, keyEntities: c2 }) {
  const d2 = c2[e2];
  return { eventKey: e2, expanded: t2.has(e2), selected: n2.has(e2), loaded: o2.has(e2), loading: r2.has(e2), checked: s2.has(e2), halfChecked: a2.has(e2), pos: String(d2 ? d2.pos : ""), parent: d2.parent, dragOver: l2 === e2 && 0 === i2, dragOverGapTop: l2 === e2 && -1 === i2, dragOverGapBottom: l2 === e2 && 1 === i2 };
}
function B_(e2, t2, n2, o2, r2, s2, a2, l2, i2) {
  var c2;
  const { clientX: d2, clientY: u2 } = e2, { top: p2, height: f2 } = e2.target.getBoundingClientRect(), h2 = (((null == r2 ? void 0 : r2.x) || 0) - d2 - 12) / o2;
  let v2 = l2[n2.eventKey];
  if (u2 < p2 + f2 / 2) {
    const e3 = a2.findIndex((e4) => e4.key === (null == v2 ? void 0 : v2.key)), t3 = a2[e3 <= 0 ? 0 : e3 - 1].key;
    v2 = l2[t3];
  }
  const m2 = v2.key, g2 = v2, y2 = v2.key;
  let b2 = 0, w2 = 0;
  if (!i2.has(m2))
    for (let e3 = 0; e3 < h2 && Y_(v2); e3 += 1)
      v2 = null == v2 ? void 0 : v2.parent, w2 += 1;
  const _2 = t2.eventData, C2 = null == v2 ? void 0 : v2.node;
  let x2 = true;
  return function(e3) {
    const t3 = W_(e3.pos);
    return 0 === Number(t3[t3.length - 1]);
  }(v2) && 0 === (null == v2 ? void 0 : v2.level) && u2 < p2 + f2 / 2 && s2({ dragNode: _2, dropNode: C2, dropPosition: -1 }) && v2.key === n2.eventKey ? b2 = -1 : (g2.children || []).length && i2.has(y2) ? s2({ dragNode: _2, dropNode: C2, dropPosition: 0 }) ? b2 = 0 : x2 = false : 0 === w2 ? h2 > -1.5 ? s2({ dragNode: _2, dropNode: C2, dropPosition: 1 }) ? b2 = 1 : x2 = false : s2({ dragNode: _2, dropNode: C2, dropPosition: 0 }) ? b2 = 0 : s2({ dragNode: _2, dropNode: C2, dropPosition: 1 }) ? b2 = 1 : x2 = false : s2({ dragNode: _2, dropNode: C2, dropPosition: 1 }) ? b2 = 1 : x2 = false, { dropPosition: b2, dropLevelOffset: w2, dropTargetKey: v2.key, dropTargetPos: v2.pos, dragOverNodeKey: y2, dropContainerKey: 0 === b2 ? null : (null == (c2 = null == v2 ? void 0 : v2.parent) ? void 0 : c2.key) || null, dropAllowed: x2 };
}
function H_(e2, t2) {
  if (!e2)
    return;
  const { multiple: n2 } = t2;
  return n2 ? e2.slice() : e2.length ? [e2[0]] : e2;
}
function j_(e2) {
  if (!e2)
    return null;
  let t2;
  if (Array.isArray(e2))
    t2 = { checkedKeys: e2, halfCheckedKeys: void 0 };
  else {
    if ("object" != typeof e2)
      return null;
    t2 = { checkedKeys: e2.checked || void 0, halfCheckedKeys: e2.halfChecked || void 0 };
  }
  return t2;
}
function K_(e2, t2) {
  return null != e2 ? e2 : t2;
}
function z_(e2, t2) {
  return `${e2}-${t2}`;
}
function Z_(e2, t2) {
  if (!e2)
    return [];
  const n2 = e2.slice(), o2 = n2.indexOf(t2);
  return o2 >= 0 && n2.splice(o2, 1), n2;
}
function U_(e2, t2) {
  const n2 = (e2 || []).slice();
  return -1 === n2.indexOf(t2) && n2.push(t2), n2;
}
function W_(e2) {
  return e2.split("-");
}
function Y_(e2) {
  if (null != e2 && e2.parent) {
    const t2 = W_(e2.pos);
    return Number(t2[t2.length - 1]) === e2.parent.children.length - 1;
  }
  return false;
}
function G_(e2) {
  const { data: t2, expanded: n2, selected: o2, checked: r2, loaded: s2, loading: a2, halfChecked: l2, dragOver: i2, dragOverGapTop: c2, dragOverGapBottom: d2, pos: u2, active: p2, eventKey: f2 } = e2, h2 = { dataRef: t2, ...t2, expanded: n2, selected: o2, checked: r2, loaded: s2, loading: a2, halfChecked: l2, dragOver: i2, dragOverGapTop: c2, dragOverGapBottom: d2, pos: u2, active: p2, eventKey: f2, key: f2 };
  return "props" in h2 || Object.defineProperty(h2, "props", { get: () => e2 }), h2;
}
function q_(e2) {
  const { disabled: t2, disableCheckbox: n2, checkable: o2 } = e2 || {};
  return !(!t2 && !n2) || false === o2;
}
function X_(e2, t2, n2, o2, r2, s2) {
  let a2;
  a2 = s2 || q_;
  const l2 = new Set(e2.filter((e3) => !!n2[e3]));
  let i2;
  return i2 = true === t2 ? function(e3, t3, n3, o3) {
    const r3 = new Set(e3), s3 = /* @__PURE__ */ new Set();
    for (let e4 = 0; e4 <= n3; e4 += 1)
      (t3.get(e4) || /* @__PURE__ */ new Set()).forEach((e5) => {
        const { key: t4, node: n4, children: s4 = [] } = e5;
        r3.has(t4) && !o3(n4) && s4.filter((e6) => !o3(e6.node)).forEach((e6) => {
          r3.add(e6.key);
        });
      });
    const a3 = /* @__PURE__ */ new Set();
    for (let e4 = n3; e4 >= 0; e4 -= 1)
      (t3.get(e4) || /* @__PURE__ */ new Set()).forEach((e5) => {
        if (o3(e5.node) || !e5.parent || a3.has(e5.parent.key))
          return;
        if (o3(e5.parent.node))
          return void a3.add(e5.parent.key);
        let t4 = true, n4 = false;
        (e5.parent.children || []).filter((e6) => !o3(e6.node)).forEach(({ key: e6 }) => {
          const o4 = r3.has(e6);
          t4 && !o4 && (t4 = false), !n4 && (o4 || s3.has(e6)) && (n4 = true);
        }), t4 && r3.add(e5.parent.key), n4 && s3.add(e5.parent.key), a3.add(e5.parent.key);
      });
    return { checkedKeys: Array.from(r3) || [], halfCheckedKeys: Array.from(J_(s3, r3)) || [] };
  }(l2, r2, o2, a2) : function(e3, t3, n3, o3, r3) {
    const s3 = new Set(e3);
    let a3 = new Set(t3);
    for (let e4 = 0; e4 <= o3; e4 += 1)
      (n3.get(e4) || /* @__PURE__ */ new Set()).forEach((e5) => {
        const { key: t4, node: n4, children: o4 = [] } = e5;
        !s3.has(t4) && !a3.has(t4) && !r3(n4) && o4.filter((e6) => !r3(e6.node)).forEach((e6) => {
          s3.delete(e6.key);
        });
      });
    a3 = /* @__PURE__ */ new Set();
    const l3 = /* @__PURE__ */ new Set();
    for (let e4 = o3; e4 >= 0; e4 -= 1)
      (n3.get(e4) || /* @__PURE__ */ new Set()).forEach((e5) => {
        if (r3(e5.node) || !e5.parent || l3.has(e5.parent.key))
          return;
        if (r3(e5.parent.node))
          return void l3.add(e5.parent.key);
        let t4 = true, n4 = false;
        (e5.parent.children || []).filter((e6) => !r3(e6.node)).forEach(({ key: e6 }) => {
          const o4 = s3.has(e6);
          t4 && !o4 && (t4 = false), !n4 && (o4 || a3.has(e6)) && (n4 = true);
        }), t4 || s3.delete(e5.parent.key), n4 && a3.add(e5.parent.key), l3.add(e5.parent.key);
      });
    return { checkedKeys: Array.from(s3) || [], halfCheckedKeys: Array.from(J_(a3, s3)) || [] };
  }(l2, t2.halfCheckedKeys, r2, o2, a2), i2;
}
function J_(e2, t2) {
  const n2 = /* @__PURE__ */ new Set();
  return e2.forEach((e3) => {
    t2.has(e3) || n2.add(e3);
  }), n2;
}
function Q_(e2, t2, n2) {
  const o2 = e2.findIndex((e3) => e3.key === n2), r2 = e2[o2 + 1], s2 = t2.findIndex((e3) => e3.key === n2);
  if (r2) {
    const e3 = t2.findIndex((e4) => e4.key === r2.key);
    return t2.slice(s2 + 1, e3);
  }
  return t2.slice(s2 + 1);
}
function eC({ treeData: e2, expandedKeys: t2, startKey: n2, endKey: o2, fieldNames: r2 = { title: "title", key: "key", children: "children" } }) {
  const s2 = [];
  let a2 = 0;
  if (n2 && n2 === o2)
    return [n2];
  if (!n2 || !o2)
    return [];
  return nC(e2, r2, (e3) => {
    if (null === e3 || 2 === a2)
      return false;
    if (function(e4) {
      return e4 === n2 || e4 === o2;
    }(e3)) {
      if (s2.push(e3), 0 === a2)
        a2 = 1;
      else if (1 === a2)
        return a2 = 2, false;
    } else
      1 === a2 && s2.push(e3);
    return -1 !== t2.indexOf(e3);
  }), s2;
}
function tC(e2, t2, n2) {
  const o2 = [...t2], r2 = [];
  return nC(e2, n2, (e3, t3) => {
    const n3 = o2.indexOf(e3);
    return -1 !== n3 && (r2.push(t3), o2.splice(n3, 1)), !!o2.length;
  }), r2;
}
function nC(e2, t2, n2) {
  e2.forEach(function(e3) {
    const o2 = e3[t2.key], r2 = e3[t2.children];
    false !== n2(o2, e3) && nC(r2 || [], t2, n2);
  });
}
const oC = {}, rC = "SELECT_ALL", sC = "SELECT_INVERT", aC = "SELECT_NONE", lC = {}, iC = [];
function cC(e2, t2) {
  const n2 = ga(() => {
    const t3 = e2.value || {}, { checkStrictly: n3 = true } = t3;
    return { ...t3, checkStrictly: n3 };
  }), [o2, r2] = _v(n2.value.selectedRowKeys || n2.value.defaultSelectedRowKeys || iC, { value: ga(() => n2.value.selectedRowKeys) }), s2 = Dt(/* @__PURE__ */ new Map()), a2 = (e3) => {
    if (n2.value.preserveSelectedRowKeys) {
      const n3 = /* @__PURE__ */ new Map();
      e3.forEach((e4) => {
        let o3 = t2.getRecordByKey(e4);
        !o3 && s2.value.has(e4) && (o3 = s2.value.get(e4)), n3.set(e4, o3);
      }), s2.value = n3;
    }
  };
  po(() => {
    a2(o2.value || iC);
  });
  const l2 = ga(() => n2.value.checkStrictly ? null : L_(t2.data.value, { externalGetKey: t2.getRowKey.value, childrenPropName: t2.childrenColumnName.value }).keyEntities), i2 = ga(() => dC(t2.pageData.value, t2.childrenColumnName.value)), c2 = ga(() => {
    const e3 = /* @__PURE__ */ new Map(), o3 = t2.getRowKey.value, r3 = n2.value.getCheckboxProps;
    return i2.value.forEach((t3, n3) => {
      const s3 = o3(t3, n3), a3 = (r3 ? r3(t3) : null) || {};
      e3.set(s3, a3);
    }), e3;
  }), { maxLevel: d2, levelEntities: u2 } = D_(l2), p2 = (e3) => {
    var n3;
    return !(null == (n3 = c2.value.get(t2.getRowKey.value(e3))) || !n3.disabled);
  }, f2 = ga(() => n2.value.checkStrictly ? { checkedKeys: o2.value || [], halfCheckedKeys: [] } : X_(o2.value, true, l2.value, d2.value, u2.value, p2)), h2 = ga(() => f2.value.checkedKeys), v2 = ga(() => f2.value.halfCheckedKeys), m2 = ga(() => {
    const e3 = "radio" === n2.value.type ? h2.value.slice(0, 1) : h2.value;
    return new Set(e3);
  }), g2 = ga(() => "radio" === n2.value.type ? /* @__PURE__ */ new Set() : new Set(v2.value)), [y2, b2] = wv(null), w2 = (e3) => {
    let o3, l3;
    a2(e3);
    const { preserveSelectedRowKeys: i3, onChange: c3 } = n2.value, { getRecordByKey: d3 } = t2;
    i3 ? (o3 = e3, l3 = e3.map((e4) => s2.value.get(e4))) : (o3 = [], l3 = [], e3.forEach((e4) => {
      const t3 = d3(e4);
      void 0 !== t3 && (o3.push(e4), l3.push(t3));
    })), r2(o3), null == c3 || c3(o3, l3);
  }, _2 = (e3, o3, r3, s3) => {
    const { onSelect: a3 } = n2.value, { getRecordByKey: l3 } = t2 || {};
    if (a3) {
      const t3 = r3.map((e4) => l3(e4));
      a3(l3(e3), o3, t3, s3);
    }
    w2(r3);
  }, C2 = ga(() => {
    const { onSelectNone: e3, selections: o3, hideSelectAll: r3 } = n2.value, { data: s3, pageData: a3, getRowKey: l3, locale: i3 } = t2;
    return !o3 || r3 ? null : (true === o3 ? [rC, sC, aC] : o3).map((t3) => t3 === rC ? { key: "all", text: i3.value.selectionAll, onSelect() {
      w2(s3.value.map((e4, t4) => l3.value(e4, t4)).filter((e4) => {
        const t4 = c2.value.get(e4);
        return !(null != t4 && t4.disabled) || m2.value.has(e4);
      }));
    } } : t3 === sC ? { key: "invert", text: i3.value.selectInvert, onSelect() {
      const e4 = new Set(m2.value);
      a3.value.forEach((t5, n3) => {
        const o4 = l3.value(t5, n3), r4 = c2.value.get(o4);
        null != r4 && r4.disabled || (e4.has(o4) ? e4.delete(o4) : e4.add(o4));
      });
      const t4 = Array.from(e4);
      w2(t4);
    } } : t3 === aC ? { key: "none", text: i3.value.selectNone, onSelect() {
      null == e3 || e3(), w2(Array.from(m2.value).filter((e4) => {
        const t4 = c2.value.get(e4);
        return null == t4 ? void 0 : t4.disabled;
      }));
    } } : t3);
  }), x2 = ga(() => i2.value.length);
  return { transformColumns: (o3) => {
    var r3;
    const { onSelectMultiple: s3, columnWidth: a3, type: f3, fixed: v3, renderCell: k2, hideSelectAll: S2, checkStrictly: E2 } = n2.value, { getRecordByKey: I2, getRowKey: O2, expandType: N2, getPopupContainer: M2 } = t2;
    if (!e2.value)
      return o3.includes(oC), o3.filter((e3) => e3 !== oC);
    let A2 = o3.slice();
    const D2 = new Set(m2.value), V2 = i2.value.map(O2.value).filter((e3) => !c2.value.get(e3).disabled), T2 = V2.every((e3) => D2.has(e3)), L2 = V2.some((e3) => D2.has(e3)), P2 = () => {
      T2 ? V2.forEach((e4) => {
        D2.delete(e4);
      }) : V2.forEach((e4) => {
        D2.add(e4);
      });
      const e3 = Array.from(D2);
      w2(e3);
    };
    let F2, R2;
    if ("radio" !== f3) {
      let e3 = null;
      if (C2.value) {
        const t4 = ba(zg, { openOnHover: true }, { activator: ({ props: e4 }) => ba(Lm, { name: "chevron-down", size: "sm", ...e4 }), default: () => ba(gg, {}, { default: () => C2.value && C2.value.map((e4, t5) => {
          const { key: n4, text: o5, onSelect: r5 } = e4;
          return ba(Sg, { key: n4 || t5, value: n4 || t5, onClick: () => null == r5 ? void 0 : r5(V2) }, { default: () => o5 });
        }) }) });
        e3 = ba("div", { class: "cds-table__selection--extra" }, t4);
      }
      const t3 = i2.value.map((e4, t4) => {
        const n4 = O2.value(e4, t4), o5 = c2.value.get(n4) || {};
        return { checked: D2.has(n4), ...o5 };
      }).filter(({ disabled: e4 }) => e4), n3 = !!t3.length && t3.length === x2.value, o4 = n3 && t3.every(({ modelValue: e4 }) => e4), r4 = n3 && t3.some(({ modelValue: e4 }) => e4);
      F2 = !S2 && ba("div", { class: "cds-table__selection" }, [ba(dg, { modelValue: n3 ? o4 : !!x2.value && T2, indeterminate: n3 ? !o4 && r4 : !T2 && L2, disabled: 0 === x2.value || n3, hideMessages: true, onChange: P2 }), e3]);
    }
    R2 = "radio" === f3 ? ({ record: e3, index: t3 }) => {
      const n3 = O2.value(e3, t3), o4 = D2.has(n3), r4 = c2.value.get(n3);
      return { node: ba(bw, { ...r4, checked: o4, hideMessages: true, onClick: (e4) => e4.stopPropagation(), onChange: (e4) => {
        D2.has(n3) || _2(n3, true, [n3], e4);
      } }), checked: o4 };
    } : ({ record: e3, index: t3 }) => {
      var n3;
      const o4 = O2.value(e3, t3), r4 = D2.has(o4), a4 = g2.value.has(o4), i3 = c2.value.get(o4);
      let f4;
      return "nest" === N2.value ? (f4 = a4, null == i3 || i3.indeterminate) : f4 = null != (n3 = null == i3 ? void 0 : i3.indeterminate) ? n3 : a4, { node: ba(dg, { ...i3, modelValue: r4, indeterminate: f4, hideMessages: true, onClick: (e4) => e4.stopPropagation(), onChange: (e4) => {
        const { shiftKey: t4 } = e4;
        let n4 = -1, a5 = -1;
        if (t4 && E2) {
          const e5 = /* @__PURE__ */ new Set([y2.value, o4]);
          V2.some((t5, o5) => {
            if (e5.has(t5)) {
              if (-1 !== n4)
                return a5 = o5, true;
              n4 = o5;
            }
            return false;
          });
        }
        if (-1 !== a5 && n4 !== a5 && E2) {
          const e5 = V2.slice(n4, a5 + 1), t5 = [];
          r4 ? e5.forEach((e6) => {
            D2.has(e6) && (t5.push(e6), D2.delete(e6));
          }) : e5.forEach((e6) => {
            D2.has(e6) || (t5.push(e6), D2.add(e6));
          });
          const o5 = Array.from(D2);
          null == s3 || s3(!r4, o5.map((e6) => I2(e6)), t5.map((e6) => I2(e6))), w2(o5);
        } else {
          const t5 = h2.value;
          if (E2) {
            const n5 = r4 ? function(e5, t6) {
              if (!e5)
                return [];
              const n6 = e5.slice(), o5 = n6.indexOf(t6);
              return o5 >= 0 && n6.splice(o5, 1), n6;
            }(t5, o4) : function(e5, t6) {
              const n6 = (e5 || []).slice();
              return -1 === n6.indexOf(t6) && n6.push(t6), n6;
            }(t5, o4);
            _2(o4, !r4, n5, e4);
          } else {
            const n5 = X_([...t5, o4], true, l2.value, d2.value, u2.value, p2), { checkedKeys: s4, halfCheckedKeys: a6 } = n5;
            let i4 = s4;
            if (r4) {
              const e5 = new Set(s4);
              e5.delete(o4), i4 = X_(Array.from(e5), { checked: false, halfCheckedKeys: a6 }, l2.value, d2.value, u2.value, p2).checkedKeys;
            }
            _2(o4, !r4, i4, e4);
          }
        }
        b2(o4);
      } }), checked: r4 };
    };
    if (!A2.includes(oC))
      if (0 === A2.findIndex((e3) => {
        var t3;
        return "EXPAND_COLUMN" === (null == (t3 = e3[h_]) ? void 0 : t3.columnType);
      })) {
        const [e3, ...t3] = A2;
        A2 = [e3, oC, ...t3];
      } else
        A2 = [oC, ...A2];
    const $2 = A2.indexOf(oC);
    A2.filter((e3) => e3 === oC).length, A2 = A2.filter((e3, t3) => e3 !== oC || t3 === $2);
    const B2 = A2[$2 - 1], H2 = A2[$2 + 1];
    let j2 = v3;
    void 0 === j2 && (void 0 !== (null == H2 ? void 0 : H2.fixed) ? j2 = H2.fixed : void 0 !== (null == B2 ? void 0 : B2.fixed) && (j2 = B2.fixed)), j2 && B2 && "EXPAND_COLUMN" === (null == (r3 = B2[h_]) ? void 0 : r3.columnType) && void 0 === B2.fixed && (B2.fixed = j2);
    const K2 = { fixed: j2, width: a3, class: "cds-table__selection-column", title: n2.value.columnTitle || F2, customRender: ({ record: e3, index: t3 }) => {
      const { node: n3, checked: o4 } = R2({ record: e3, index: t3 });
      return k2 ? k2(o4, e3, t3, n3) : n3;
    }, [h_]: { class: "cds-table__selection-col" } };
    return A2.map((e3) => e3 === oC ? K2 : e3);
  }, selectedKeySet: m2 };
}
function dC(e2, t2) {
  let n2 = [];
  return (e2 || []).forEach((e3) => {
    n2.push(e3), e3 && "object" == typeof e3 && t2 in e3 && (n2 = [...n2, ...dC(e3[t2], t2)]);
  }), n2;
}
function uC(e2) {
  return { filledColumns: (t2) => hC(t2, e2) };
}
function pC({ columns: e2, expandable: t2, expandedKeys: n2, getRowKey: o2, onTriggerExpand: r2, expandIcon: s2, rowExpandable: a2, expandIconColumnIndex: l2, expandRowByClick: i2, expandColumnWidth: c2, expandFixed: d2 }, u2) {
  const p2 = ga(() => {
    if (t2.value) {
      let t3 = e2.value.slice();
      if (l2 && l2.value, null == t3 || !t3.includes(lC)) {
        const e3 = (null == l2 ? void 0 : l2.value) || 0;
        e3 >= 0 && (null == t3 || t3.splice(e3, 0, lC));
      }
      t3.filter((e3) => e3 === lC).length;
      const u3 = t3.indexOf(lC);
      t3 = t3.filter((e3, t4) => e3 !== lC || t4 === u3);
      const p3 = null == e2 ? void 0 : e2.value[u3];
      let f3;
      f3 = "left" !== d2.value && !d2.value || null != l2 && l2.value ? "right" !== d2.value && !d2.value || (null == l2 ? void 0 : l2.value) !== e2.value.length ? p3 ? p3.fixed : void 0 : "right" : "left";
      const h3 = n2.value, v2 = a2.value, m2 = s2.value, g2 = i2.value, y2 = { [h_]: { class: "cds-table__expand-icon-col", columnType: "EXPAND_COLUMN" }, title: "", fixed: f3, class: "cds-table__row-expand-icon-cell", width: c2.value, customRender: ({ record: e3, index: t4 }) => {
        const n3 = o2.value(e3, t4), s3 = h3.has(n3), a3 = !v2 || v2(e3), l3 = m2({ expanded: s3, expandable: a3, record: e3, onExpand: r2 });
        return g2 ? ba("span", { onClick: (e4) => e4.stopPropagation() }, l3) : l3;
      } };
      return t3.map((e3) => e3 === lC ? y2 : e3);
    }
    return e2.value.includes(lC), e2.value.filter((e3) => e3 !== lC);
  }), f2 = ga(() => {
    let e3 = p2.value;
    return u2.value && (e3 = u2.value(e3)), e3.length || (e3 = [{ customRender: () => null }]), e3;
  }), h2 = ga(() => mC(f2.value));
  return { columns: f2, flattenColumns: h2 };
}
function fC(e2) {
  return { filledColumns: (t2) => vC(t2, e2.value) };
}
function hC(e2, t2) {
  return e2.map((e3) => {
    if (e3 === oC || e3 === lC)
      return e3;
    const n2 = { ...e3 };
    return n2.__originColumn__ = e3, t2.value.headerCell && (n2.title = N_(t2.value, "headerCell", { title: e3.title, column: e3 }, () => [e3.title])), "children" in n2 && Array.isArray(n2.children) && (n2.children = hC(n2.children, t2)), n2;
  });
}
function vC(e2, t2) {
  return e2.map((e3) => {
    const n2 = { ...e3 };
    return n2.title = I_(n2.title, t2), "children" in n2 && (n2.children = vC(n2.children, t2)), n2;
  });
}
function mC(e2) {
  return e2.reduce((e3, t2) => {
    const { fixed: n2 } = t2, o2 = true === n2 ? "left" : n2, r2 = t2.children;
    return r2 && r2.length > 0 ? [...e3, ...mC(r2).map((e4) => ({ fixed: o2, ...e4 }))] : [...e3, { ...t2, fixed: o2 }];
  }, []);
}
const gC = Symbol("cds:table"), yC = () => Or(gC, {}), bC = Symbol("cds:table:context"), wC = Symbol("cds:table:body"), _C = () => Or(wC, {}), CC = Symbol("cds:table:resize"), xC = Symbol("cds:table:slots"), kC = () => Or(xC, ga(() => ({}))), SC = At(false), EC = Symbol("cds:table:hover"), IC = Symbol("cds:table:expanded-row"), OC = Symbol("cds:table:summary"), NC = Lo({ name: "FilterSearch", inheritAttrs: false, props: ["value", "filterSearch", "locale"], setup: (e2) => () => {
  const { value: t2, onChange: n2, filterSearch: o2, locale: r2 } = e2;
  return o2 ? ba("div", { class: "cds-table__filter-dropdown-search" }, ba(ty, { value: t2, size: "sm", placeholder: r2.filterSearchPlaceholder, prependInnerIcon: "search", hideMessages: true, class: "cds-table__filter-dropdown-search-input", onChange: n2 })) : null;
} }), MC = Lo({ name: "FilterDropdownMenuWrapper", setup: (e2, { slots: t2 }) => () => {
  var e3;
  return ba("div", { onClick: (e4) => e4.stopPropagation() }, null == (e3 = t2.default) ? void 0 : e3.call(t2));
} }), AC = Lo({ name: "FilterDropdown", props: ["column", "filterState", "filterMultiple", "filterMode", "filterSearch", "columnKey", "triggerFilter", "locale", "getPopupContainer"], setup(e2, { slots: t2 }) {
  const n2 = kC(), o2 = ga(() => {
    var t3;
    return null != (t3 = e2.filterMode) ? t3 : "menu";
  }), r2 = ga(() => {
    var t3;
    return null != (t3 = e2.filterSearch) && t3;
  }), s2 = ga(() => e2.column.filterDropdownVisible), a2 = At(false), l2 = ga(() => {
    var t3;
    return !(!e2.filterState || !(null == (t3 = e2.filterState.filteredKeys) ? void 0 : t3.length) && !e2.filterState.forceFiltered);
  }), i2 = ga(() => {
    var t3;
    return TC(null == (t3 = e2.column) ? void 0 : t3.filters);
  }), c2 = ga(() => {
    const { filterDropdown: t3, customFilterDropdown: o3 } = e2.column;
    return t3 || o3 && Lt(n2).customFilterDropdown;
  }), d2 = ga(() => {
    const { filterIcon: t3 } = e2.column;
    return t3 || Lt(n2).customFilterIcon;
  }), u2 = (t3) => {
    var n3, o3;
    a2.value = t3, null == (o3 = (n3 = e2.column).onFilterDropdownVisibleChange) || o3.call(n3, t3);
  }, p2 = ga(() => "boolean" == typeof s2.value ? Lt(s2) : Lt(a2)), f2 = ga(() => {
    var t3;
    return null == (t3 = e2.filterState) ? void 0 : t3.filteredKeys;
  }), h2 = Dt([]), v2 = ({ selectedKeys: e3 }) => {
    h2.value = e3;
  };
  ho(f2, () => {
    !Lt(a2) || v2({ selectedKeys: Lt(f2) || [] });
  }, { immediate: true });
  const m2 = Dt([]), g2 = At();
  Yo(() => {
    clearTimeout(Lt(g2));
  });
  const y2 = At(""), b2 = (e3) => {
    const { value: t3 } = e3.target;
    y2.value = t3;
  };
  ho(a2, () => {
    Lt(a2) || (y2.value = "");
  }), ho(o2, (e3) => {
  }, { immediate: true });
  const w2 = (t3) => {
    const { column: n3, columnKey: o3, filterState: r3 } = e2, s3 = t3 && t3.length ? t3 : null;
    if (null === s3 && (!r3 || !r3.filteredKeys) || Vd(s3, null == r3 ? void 0 : r3.filteredKeys))
      return null;
    e2.triggerFilter({ column: n3, key: o3, filteredKeys: s3 });
  }, _2 = () => {
    u2(false), w2(Lt(h2));
  }, C2 = ({ confirm: e3, closeDropdown: t3 } = { confirm: false, closeDropdown: false }) => {
    e3 && w2([]), t3 && u2(false), y2.value = "", h2.value = [];
  }, x2 = ({ closeDropdown: e3 } = { closeDropdown: true }) => {
    e3 && u2(false), w2(Lt(h2));
  }, k2 = (e3) => {
    e3 && void 0 !== f2.value && (h2.value = f2.value || []), u2(e3), !e3 && !Lt(c2) && _2();
  }, S2 = (e3) => {
    e3.target.checked ? h2.value = Lt(i2) : h2.value = [];
  }, E2 = ({ filters: e3 }) => (e3 || []).map((e4, t3) => {
    const n3 = String(e4.value), o3 = { title: e4.text, key: void 0 !== e4.value ? n3 : t3 };
    return e4.children && (o3.children = E2({ filters: e4.children })), o3;
  });
  ga(() => E2({ filters: e2.column.filters }));
  const I2 = () => {
    const t3 = h2.value, { column: n3, locale: s3, getPopupContainer: a3, filterMultiple: l3 } = e2;
    return 0 === (n3.filters || []).length ? ba("div", { class: "cds-table__dropdown-no-filters" }, s3.filterEmptyText) : "tree" === Lt(o2) ? ba(bs, [ba(NC, { filterSearch: Lt(r2), value: Lt(y2), onChange: b2, locale: s3 }), ba("div", { class: "cds-table__filter-dropdown-tree" }, l3 ? ba(dg, { label: s3.filterCheckAll, class: "cds-table__filter-dropdown--checkall", onChange: S2, checked: t3.length === Lt(i2).length, indeterminate: t3.length > 0 && t3.length < Lt(i2).length }) : void 0), ba("div", {}, "FilterMode `tree` not supported yet.")]) : ba(bs, [ba(NC, { filterSearch: Lt(r2), value: Lt(y2), onChange: b2, locale: s3 }), ba(gg, { selected: t3, selectStrategy: l3 ? "independent" : "single-independent", "onUpdate:selected": (e3) => v2({ selectedKeys: e3 }), "onUpdate:opened": (e3) => ((e4) => {
      g2.value = setTimeout(() => {
        m2.value = e4;
      });
    })(e3) }, { default: () => DC({ filters: n3.filters || [], filterSearch: Lt(r2), filteredKeys: Lt(h2), searchValue: Lt(y2), filterMultiple: l3 }) })]);
  };
  return () => {
    var n3;
    const { locale: o3, column: r3, getPopupContainer: s3 } = e2;
    let i3;
    if ("function" == typeof c2.value)
      i3 = c2.value({ setSelectedKeys: (e3) => v2({ selectedKeys: e3 }), selectedKeys: Lt(h2), confirm: x2, clearFilters: C2, filters: r3.filters, visible: Lt(p2), column: r3.__originColumn__ });
    else if (Lt(c2))
      i3 = Lt(c2);
    else {
      const e3 = Lt(h2);
      i3 = ba(bs, [I2(), ba("div", { class: "cds-table__dropdown-buttons" }, [ba(my, { text: o3.filterReset, appearance: "secondary", disabled: 0 === e3.length, size: "sm", onClick: () => C2() }), ba(my, { text: o3.filterConfirm, appearance: "primary", size: "sm", onClick: _2 })])]);
    }
    const u3 = ba(MC, { class: "cds-table__dropdown" }, { default: () => i3 });
    let f3;
    return f3 = "function" == typeof d2.value ? d2.value({ filtered: Lt(l2), column: r3.__originColumn__ }) : Lt(d2) ? Lt(d2) : ba(Lm, { name: "funnel" }), ba("div", { class: "cds-table__filter-column" }, [ba("span", { class: "cds-table__column-title" }, null == (n3 = t2.default) ? void 0 : n3.call(t2)), ba(zg, { modelValue: Lt(a2), closeOnContentClick: false, transition: "slide-transition-y", "onUpdate:modelValue": k2 }, { activator: ({ props: e3 }) => ba(my, { appearance: "transparent", icon: true, size: "sm", tabindex: -1, class: Cp("cds-table__filter-trigger", { [Sd.active]: Lt(l2) }), ...e3 }, { prepend: () => f3 }), default: () => ba("div", { class: "cds-table__dropdown-content" }, u3) })]);
  };
} });
function DC({ filters: e2, filteredKeys: t2, filterMultiple: n2, searchValue: o2, filterSearch: r2 }) {
  return e2.map((e3, s2) => {
    const a2 = String(e3.value);
    if (e3.children)
      return ba(Ag, { key: a2 || s2, class: "cds-table__dropdown-submenu" }, { activator: ({ props: t3 }) => ba(Sg, { ...t3 }, { default: () => e3.text }), default: () => DC({ filters: e3.children || [], filteredKeys: t2, filterMultiple: n2, searchValue: o2, filterSearch: r2 }) });
    const l2 = ba(Sg, { key: void 0 !== e3.value ? a2 : s2, value: void 0 !== e3.value ? a2 : s2, selected: t2.includes(a2) }, { prepend: ({ isSelected: t3 }) => n2 ? ba(dg, { modelValue: t3, hideMessages: true, tabindex: -1 }) : ba(bw, { checked: t3, value: e3.value, hideMessages: true, tabindex: -1 }), default: () => ba("span", e3.text) });
    return o2.trim() ? "function" == typeof r2 ? r2(o2, e3) ? l2 : void 0 : function(e4, t3) {
      return ("string" == typeof t3 || "number" == typeof t3) && (null == t3 ? void 0 : t3.toString().toLowerCase().includes(e4.trim().toLowerCase()));
    }(o2, e3.text) ? l2 : void 0 : l2;
  });
}
function VC({ mergedColumns: e2, tableLocale: t2, getPopupContainer: n2, onFilterChange: o2 }) {
  const [r2, s2] = wv(FC(e2.value, true)), a2 = ga(() => {
    const t3 = FC(e2.value, false);
    return t3.every(({ filteredKeys: e3 }) => void 0 === e3) ? r2.value : (t3.every(({ filteredKeys: e3 }) => void 0 !== e3), t3);
  }), l2 = ga(() => RC(a2.value)), i2 = (e3) => {
    const t3 = a2.value.filter(({ key: t4 }) => t4 !== e3.key);
    t3.push(e3), s2(t3), o2(RC(t3), t3);
  };
  return { transformColumns: (e3) => PC(e3, a2.value, i2, null == n2 ? void 0 : n2.value, t2.value), mergedFilterStates: a2, filters: l2 };
}
function TC(e2) {
  let t2 = [];
  return (e2 || []).forEach(({ value: e3, children: n2 }) => {
    t2.push(e3), n2 && (t2 = [...t2, ...TC(n2)]);
  }), t2;
}
function LC(e2, t2) {
  return t2.reduce((e3, t3) => {
    const { column: { onFilter: n2, filters: o2 }, filteredKeys: r2 } = t3;
    return n2 && r2 && r2.length ? e3.filter((e4) => r2.some((t4) => {
      const r3 = TC(o2), s2 = r3.findIndex((e5) => String(e5) === String(t4)), a2 = -1 !== s2 ? r3[s2] : t4;
      return n2(a2, e4);
    })) : e3;
  }, e2);
}
function PC(e2, t2, n2, o2, r2, s2) {
  return e2.map((e3, a2) => {
    const l2 = E_(a2, s2), { filterMultiple: i2 = true, filterMode: c2, filterSearch: d2 } = e3;
    let u2 = e3;
    const p2 = e3.filterDropdown || (null == e3 ? void 0 : e3.customFilterDropdown);
    if (u2.filters || p2) {
      const s3 = S_(u2, l2), a3 = t2.find(({ key: e4 }) => e4 === s3);
      u2 = { ...u2, title: (t3) => ba(AC, { column: u2, columnKey: s3, filterState: a3, filterMultiple: i2, filterMode: c2, filterSearch: d2, triggerFilter: n2, locale: r2, getPopupContainer: o2 }, { default: () => I_(e3.title, t3) }) };
    }
    return "children" in u2 && (u2 = { ...u2, children: PC(u2.children, t2, n2, o2, r2, l2) }), u2;
  });
}
function FC(e2, t2, n2) {
  let o2 = [];
  return (e2 || []).forEach((e3, r2) => {
    var s2;
    const a2 = E_(r2, n2), l2 = e3.filterDropdown || (null == e3 ? void 0 : e3.customFilterDropdown);
    if (e3.filters || l2 || "onFilter" in e3)
      if ("filteredValue" in e3) {
        let t3 = e3.filteredValue;
        l2 || (t3 = null != (s2 = null == t3 ? void 0 : t3.map(String)) ? s2 : t3), o2.push({ column: e3, key: S_(e3, a2), filteredKeys: t3, forceFiltered: e3.filtered });
      } else
        o2.push({ column: e3, key: S_(e3, a2), filteredKeys: t2 && e3.defaultFilteredValue ? e3.defaultFilteredValue : void 0, forceFiltered: e3.filtered });
    "children" in e3 && (o2 = [...o2, ...FC(e3.children, t2, a2)]);
  }), o2;
}
function RC(e2) {
  const t2 = {};
  return e2.forEach(({ key: e3, filteredKeys: n2, column: o2 }) => {
    const r2 = o2.filterDropdown || (null == o2 ? void 0 : o2.customFilterDropdown), { filters: s2 } = o2;
    if (r2)
      t2[e3] = n2 || null;
    else if (Array.isArray(n2)) {
      const o3 = TC(s2);
      t2[e3] = o3.filter((e4) => n2.includes(String(e4)));
    } else
      t2[e3] = null;
  }), t2;
}
const $C = Lo({ name: "CdsTooltip", props: { id: { type: String, default: void 0 }, hideArrow: { type: Boolean, default: false }, ...Pf(), ...Zv(), ...Uh({ scrim: false, closeOnBack: false }), ...om({ openOnClick: false, openOnHover: true }), ...hm({ origin: "auto", locationStrategy: "connected", location: "top", offset: 8 }), ...Cm({ scrollStrategy: "reposition" }), ...Wh({ eager: true }), ...Xh({ minWidth: 0 }), ...zh({ transition: false }), ...Gh() }, emits: { "update:modelValue": (e2) => true }, setup(e2, { slots: t2, expose: n2 }) {
  const o2 = vh(e2, "modelValue"), { themeClasses: r2 } = Yv(e2), { scopeId: s2 } = av(), a2 = su(), l2 = ga(() => e2.id || `cds-tooltip-${a2}`), i2 = At(), c2 = ga(() => e2.location.split(" ").length > 1 ? e2.location : e2.location + " center"), d2 = ga(() => "auto" === e2.origin || "overlap" === e2.origin || e2.origin.split(" ").length > 1 || e2.location.split(" ").length > 1 ? e2.origin : e2.origin + " center"), u2 = ga(() => e2.transition ? e2.transition : o2.value ? "scale-transition" : "fade-transition"), p2 = ga(() => Us({ "aria-describedby": Lt(l2) }, e2.activatorProps));
  return Oh(() => ba(Kg, { ref: i2, ...e2, id: Lt(l2), absolute: true, persistent: true, modelValue: Lt(o2), location: Lt(c2), origin: Lt(d2), activatorProps: Lt(p2), transition: Lt(u2), class: Cp("cds-tooltip", { "cds-tooltip--hide-arrow": e2.hideArrow, [Lt(c2)]: true }, Lt(r2)), role: "tooltip", ...s2 }, { activator: t2.activator, default: (...n3) => {
    var o3, r3;
    return null != (r3 = null == (o3 = t2.default) ? void 0 : o3.call(t2, ...n3)) ? r3 : e2.text;
  } })), n2({ id: l2, overlay: i2 }), {};
} }), BC = "ascend", HC = "descend";
function jC({ mergedColumns: e2, sortDirections: t2, tableLocale: n2, showSorterTooltip: o2, onSorterChange: r2 }) {
  const [s2, a2] = wv(WC(e2.value, true)), l2 = ga(() => {
    let t3 = true;
    const n3 = WC(e2.value, false);
    if (!n3.length)
      return s2.value;
    const o3 = [], r3 = (e3) => {
      t3 ? o3.push(e3) : o3.push({ ...e3, sortOrder: null });
    };
    let a3 = null;
    return n3.forEach((e3) => {
      null === a3 ? (r3(e3), e3.sortOrder && (false === e3.multiplePriority ? t3 = false : a3 = true)) : (a3 && false !== e3.multiplePriority || (t3 = false), r3(e3));
    }), o3;
  }), i2 = ga(() => ({ sortColumns: l2.value.map(({ column: e3, sortOrder: t3 }) => ({ column: e3, order: t3 })) })), c2 = (e3) => {
    let t3;
    t3 = false !== e3.multiplePriority && l2.value.length && false !== l2.value[0].multiplePriority ? [...l2.value.filter(({ key: t4 }) => t4 !== e3.key), e3] : [e3], a2(t3), r2(GC(t3), t3);
  }, d2 = ga(() => GC(l2.value));
  return { transformColumns: (e3) => zC(e3, l2.value, c2, t2.value, null == n2 ? void 0 : n2.value, null == o2 ? void 0 : o2.value), mergedSorterStates: l2, columnTitleSorterProps: i2, sorters: d2 };
}
function KC(e2, t2, n2) {
  const o2 = t2.slice().sort((e3, t3) => t3.multiplePriority - e3.multiplePriority), r2 = e2.slice(), s2 = o2.filter(({ column: { sorter: e3 }, sortOrder: t3 }) => ZC(e3) && t3);
  return s2.length ? r2.sort((e3, t3) => {
    for (let n3 = 0; n3 < s2.length; n3 += 1) {
      const o3 = s2[n3], { column: { sorter: r3 }, sortOrder: a2 } = o3, l2 = ZC(r3);
      if (l2 && a2) {
        const n4 = l2(e3, t3, a2);
        if (0 !== n4)
          return a2 === BC ? n4 : -n4;
      }
    }
    return 0;
  }).map((e3) => {
    const t3 = e3[n2];
    return t3 ? { ...e3, [n2]: KC(t3, o2, n2) } : e3;
  }) : r2;
}
function zC(e2, t2, n2, o2, r2, s2, a2) {
  return (e2 || []).map((e3, l2) => {
    const i2 = E_(l2, a2);
    let c2 = e3;
    if (c2.sorter) {
      const a3 = c2.sortDirections || o2, l3 = void 0 === c2.showSorterTooltip ? s2 : c2.showSorterTooltip, d2 = S_(c2, i2), u2 = t2.find(({ key: e4 }) => e4 === d2), p2 = u2 ? u2.sortOrder : null, f2 = function(e4, t3) {
        return t3 ? e4[e4.indexOf(t3) + 1] : e4[0];
      }(a3, p2), h2 = a3.length && ba("div", { class: { "cds-table__column-sorter-icon": true, [Sd.active]: p2 === HC || p2 === BC } }, ba(Lm, { name: (p2 === BC ? "arrow-up" : p2 === HC && "arrow-down") || "arrows-vertical" })), { cancelSort: v2, triggerAsc: m2, triggerDesc: g2 } = r2 || {};
      let y2 = v2;
      f2 === HC ? y2 = g2 : f2 === BC && (y2 = m2);
      const b2 = "object" == typeof l3 ? l3 : { text: y2 };
      c2 = { ...c2, class: `${c2.class || ""} ${p2 ? "cds-table__column-sort" : ""}`, title: (t3) => {
        const n3 = (n4) => ba("div", { class: "cds-table__column-sorters", ...n4 }, [ba("span", { class: "cds-table__column-title" }, I_(e3.title, t3)), ba("span", { class: Cp("cds-table__column-sorter", { "cds-table__column-sorter-full": !!h2 }) }, ba("span", { class: "cds-table__column-sorter-inner" }, [h2]))]);
        return l3 ? ba($C, { ...b2, offset: 24 }, { activator: ({ props: e4 }) => n3(e4) }) : n3({});
      }, customHeaderCell: (t3) => {
        const o3 = e3.customHeaderCell && e3.customHeaderCell(t3) || {}, r3 = o3.onClick;
        return o3.onClick = (t4) => {
          n2({ column: e3, key: d2, sortOrder: f2, multiplePriority: UC(e3) }), r3 && r3(t4);
        }, o3.class = `${o3.class || ""} cds-table__column-has-sorters`, o3;
      } };
    }
    return "children" in c2 && (c2 = { ...c2, children: zC(c2.children, t2, n2, o2, r2, s2, i2) }), c2;
  });
}
function ZC(e2) {
  return "function" == typeof e2 ? e2 : !(!e2 || "object" != typeof e2 || !e2.compare) && e2.compare;
}
function UC(e2) {
  return "object" == typeof e2.sorter && "number" == typeof e2.sorter.multiple && e2.sorter.multiple;
}
function WC(e2, t2, n2) {
  let o2 = [];
  function r2(e3, t3) {
    o2.push({ column: e3, key: S_(e3, t3), multiplePriority: UC(e3), sortOrder: e3.sortOrder });
  }
  return (e2 || []).forEach((e3, s2) => {
    const a2 = E_(s2, n2);
    e3.children ? ("sortOrder" in e3 && r2(e3, a2), o2 = [...o2, ...WC(e3.children, t2, a2)]) : e3.sorter && ("sortOrder" in e3 ? r2(e3, a2) : t2 && e3.defaultSortOrder && o2.push({ column: e3, key: S_(e3, a2), multiplePriority: UC(e3), sortOrder: e3.defaultSortOrder }));
  }), o2;
}
function YC(e2) {
  const { column: t2, sortOrder: n2 } = e2;
  return { column: t2, order: n2, field: t2.dataIndex, columnKey: t2.key };
}
function GC(e2) {
  const t2 = e2.filter(({ sortOrder: e3 }) => e3).map(YC);
  return 0 === t2.length && e2.length ? { ...YC(e2[e2.length - 1]), column: void 0 } : t2.length <= 1 ? t2[0] || {} : t2;
}
const qC = 10;
function XC(e2, t2, n2, o2) {
  const r2 = ga(() => t2.value && "object" == typeof t2.value ? t2.value : {}), s2 = ga(() => r2.value.total || 0), [a2, l2] = wv(() => ({ modelValue: "modelValue" in r2.value ? r2.value.modelValue : 1, total: s2.value || qC })), i2 = ga(() => {
    const t3 = function(...e3) {
      const t4 = {};
      return e3.forEach((e4) => {
        e4 && Object.keys(e4).forEach((n3) => {
          const o4 = e4[n3];
          void 0 !== o4 && (t4[n3] = o4);
        });
      }), t4;
    }(a2.value, r2.value, { length: r2.value.length || Math.ceil(e2.value / a2.value.total), disabled: n2.value }), o3 = Math.ceil((s2.value || e2.value) / t3.length);
    return t3.modelValue > t3.length && (t3.modelValue = o3 || 1), t3;
  }), c2 = (e3, t3) => {
    false !== r2.value && l2({ modelValue: null != e3 ? e3 : 1, total: null != t3 ? t3 : i2.value.total });
  }, d2 = (e3) => {
    r2.value && (r2.value.modelValue = e3), c2(e3, a2.value.total), o2(e3, i2.value.length);
  };
  return { mergedPagination: ga(() => false === r2.value ? {} : { ...i2.value, "onUpdate:modelValue": d2 }), resetPagination: c2 };
}
function JC(e2) {
  const t2 = Dt(e2), n2 = Dt([]);
  let o2;
  return Yo(() => {
    yp.cancel(o2);
  }), [t2, function(e3) {
    n2.value.push(e3), yp.cancel(o2), o2 = yp(() => {
      const e4 = n2.value;
      n2.value = [], e4.forEach((e5) => {
        t2.value = e5(t2.value);
      });
    });
  }];
}
const QC = wd ? window : null;
const ex = {};
function tx(e2, t2) {
  const n2 = At(0), o2 = At(), r2 = At(), s2 = At(), a2 = At({ scrollWidth: 0, clientWidth: 0 }), l2 = At(), [i2, c2] = wv(false), [d2, u2] = wv(false), [p2, f2] = JC(/* @__PURE__ */ new Map()), h2 = ga(() => __(t2.value)), v2 = ga(() => h2.value.map((e3) => p2.value.get(e3))), m2 = ga(() => t2.value.length), g2 = function(e3, t3) {
    return ga(() => {
      const n3 = [], o3 = [];
      let r3 = 0, s3 = 0;
      const a3 = e3.value, l3 = t3.value;
      for (let e4 = 0; e4 < l3; e4 += 1) {
        n3[e4] = r3, r3 += a3[e4] || 0;
        const t4 = l3 - e4 - 1;
        o3[t4] = s3, s3 += a3[t4] || 0;
      }
      return { left: n3, right: o3 };
    });
  }(v2, m2), y2 = ga(() => e2.scroll && C_(e2.scroll.y)), b2 = ga(() => e2.scroll && C_(e2.scroll.x) || Boolean(e2.expandFixed)), w2 = ga(() => b2.value && t2.value.some(({ fixed: e3 }) => e3)), _2 = At(), C2 = function(e3) {
    return ga(() => {
      const { offsetHeader: t3 = 0, offsetSummary: n3 = 0, offsetScroll: o3 = 0, getContainer: r3 = () => QC } = "object" == typeof e3.value ? e3.value : {}, s3 = r3() || QC, a3 = !!e3.value;
      return { isSticky: a3, stickyClassName: a3 ? "cds-table__sticky-holder" : "", offsetHeader: t3, offsetSummary: n3, offsetScroll: o3, container: s3 };
    });
  }(jt(e2, "sticky")), x2 = vt({}), k2 = ga(() => {
    const [e3] = Object.values(x2);
    return (y2.value || C2.value.isSticky) && e3;
  }), S2 = At({}), E2 = At({}), I2 = At({});
  po(() => {
    y2.value && (E2.value = { overflowY: "scroll", maxHeight: Bd(e2.scroll.y, "px") }), b2.value && (S2.value = { overflowX: "auto" }, y2.value || (E2.value = { overflowY: "hidden" }), I2.value = { width: true === e2.scroll.x ? "auto" : Bd(e2.scroll.x, "px"), minWidth: "100%" });
  });
  const [O2, N2] = function(e3) {
    const t3 = At(e3 || null), n3 = At();
    function o3() {
      clearTimeout(n3.value);
    }
    return Yo(() => {
      o3();
    }), [function(e4) {
      t3.value = e4, o3(), n3.value = setTimeout(() => {
        t3.value = null, n3.value = void 0;
      }, 100);
    }, function() {
      return t3.value;
    }];
  }(null);
  function M2(e3, t3) {
    if (!t3)
      return;
    if ("function" == typeof t3)
      return void t3(e3);
    const n3 = t3.$el || t3;
    n3.scrollLeft !== e3 && (n3.scrollLeft = e3);
  }
  const A2 = ({ currentTarget: e3, scrollLeft: t3 }) => {
    var n3;
    const o3 = "number" == typeof t3 ? t3 : e3.scrollLeft, a3 = e3 || ex;
    if ((!N2() || N2() === a3) && (O2(a3), M2(o3, r2.value), M2(o3, s2.value), M2(o3, l2.value), M2(o3, null == (n3 = _2.value) ? void 0 : n3.setScrollLeft)), e3) {
      const { scrollWidth: t4, clientWidth: n4 } = e3;
      c2(o3 > 0), u2(o3 < t4 - n4);
    }
  }, D2 = () => {
    b2.value && s2.value ? A2({ currentTarget: s2.value }) : (c2(false), u2(false));
  };
  let V2;
  const T2 = (e3) => {
    var t3;
    e3 !== n2.value && (D2(), n2.value = o2.value ? null == (t3 = o2.value) ? void 0 : t3.offsetWidth : e3);
  };
  ho([b2, () => e2.data, () => e2.columns], () => {
    b2.value && D2();
  }, { flush: "post" });
  const [L2, P2] = wv(0);
  Zo(() => {
    SC.value = SC.value || Zu("position", "sticky");
  }), Zo(() => {
    un(() => {
      D2(), P2(function(e3) {
        if (!(wd && e3 && e3 instanceof Element))
          return { width: 0, height: 0 };
        const { width: t3, height: n3 } = getComputedStyle(e3, "::-webkit-scrollbar");
        return { width: tp(t3), height: tp(n3) };
      }(s2.value).width), a2.value = { scrollWidth: s2.value.scrollWidth || 0, clientWidth: s2.value.clientWidth || 0 };
    });
  }), Wo(() => {
    un(() => {
      const e3 = s2.value.scrollWidth || 0, t3 = s2.value.clientWidth || 0;
      (a2.value.scrollWidth !== e3 || a2.value.clientWidth !== t3) && (a2.value = { scrollWidth: e3, clientWidth: t3 });
    });
  }), po(() => {
    e2.internalHooks === v_ && e2.internalRefs && e2.onUpdateInternalRefs && e2.onUpdateInternalRefs({ body: s2.value ? s2.value.$el || s2.value : null });
  }, { flush: "post" });
  const F2 = ga(() => e2.tableLayout ? e2.tableLayout : w2.value ? "max-content" === e2.scroll.x ? "auto" : "fixed" : y2.value || C2.value.isSticky || t2.value.some(({ ellipsis: e3 }) => e3) ? "fixed" : "auto");
  return { fullTableRef: o2, scrollHeaderRef: r2, scrollBodyRef: s2, scrollSummaryRef: l2, stickyRef: _2, componentWidth: n2, fixHeader: y2, fixColumn: w2, fixFooter: k2, pingedLeft: i2, pingedRight: d2, horizonScroll: b2, mergedTableLayout: F2, stickyOffsets: g2, stickyState: C2, scrollbarSize: L2, colWidths: v2, columnCount: m2, scrollXStyle: S2, scrollYStyle: E2, scrollTableStyle: I2, scrollBodySizeInfo: a2, summaryCollect: (e3, t3) => {
    t3 ? x2[e3] = t3 : delete x2[e3];
  }, onColumnResize: (e3, t3) => {
    (function(e4) {
      if (!e4)
        return false;
      if (e4.offsetParent)
        return true;
      if (e4.getBBox) {
        const t4 = e4.getBBox();
        if (t4.width || t4.height)
          return true;
      }
      if (e4.getBoundingClientRect) {
        const t4 = e4.getBoundingClientRect();
        if (t4.width || t4.height)
          return true;
      }
      return false;
    })(o2.value) && f2((n3) => {
      if (n3.get(e3) !== t3) {
        const o3 = new Map(n3);
        return o3.set(e3, t3), o3;
      }
      return n3;
    });
  }, onFullTableResize: ({ width: e3 }) => {
    clearTimeout(V2), 0 !== n2.value ? V2 = setTimeout(() => {
      T2(e3);
    }, 100) : T2(e3);
  }, onScroll: A2 };
}
function nx(e2, t2, n2, o2, r2, s2) {
  const a2 = [];
  a2.push({ record: e2, indent: t2, index: s2 });
  const l2 = r2(e2), i2 = null == o2 ? void 0 : o2.has(l2);
  if (e2 && Array.isArray(e2[n2]) && i2)
    for (let s3 = 0; s3 < e2[n2].length; s3 += 1) {
      const l3 = nx(e2[n2][s3], t2 + 1, n2, o2, r2, s3);
      a2.push(...l3);
    }
  return a2;
}
function ox(e2, t2) {
  return function({ record: n2, expanded: o2, expandable: r2, onExpand: s2 }) {
    const a2 = "cds-table__row-expand-icon";
    return ba(my, { type: "button", appearance: "transparent", icon: "chevron-right", size: "xs" === t2 || "sm" === t2 ? "sm" : "md", class: { "cds-table__row-expand-icon": true, [`${a2}--spaced`]: !r2, [`${a2}--expanded`]: r2 && o2, [`${a2}--collapsed`]: r2 && !o2 }, ariaLabel: o2 ? e2.collapse : e2.expand, onClick: (e3) => {
      s2(n2, e3), e3.stopPropagation();
    } });
  };
}
const rx = Lo({ name: "Cell", props: ["record", "index", "renderIndex", "dataIndex", "customRender", "component", "colSpan", "rowSpan", "ellipsis", "align", "fixLeft", "fixRight", "firstFixLeft", "lastFixLeft", "firstFixRight", "lastFixRight", "appendNode", "additionalProps", "rowType", "isSticky", "column", "cellType", "transformCellText"], setup(e2, { slots: t2 }) {
  const n2 = kC(), { startRow: o2, endRow: r2, onHover: s2 } = Or(EC, { startRow: At(-1), endRow: At(-1), onHover() {
  } }), a2 = SC, l2 = ga(() => {
    var t3, n3, o3, r3;
    return null != (r3 = null != (n3 = e2.colSpan) ? n3 : null == (t3 = e2.additionalProps) ? void 0 : t3.colSpan) ? r3 : null == (o3 = e2.additionalProps) ? void 0 : o3.colspan;
  }), i2 = ga(() => {
    var t3, n3, o3, r3;
    return null != (r3 = null != (n3 = e2.rowSpan) ? n3 : null == (t3 = e2.additionalProps) ? void 0 : t3.rowSpan) ? r3 : null == (o3 = e2.additionalProps) ? void 0 : o3.rowspan;
  }), c2 = iu(() => {
    const { index: t3 } = e2;
    return function(e3 = 0, t4, n3, o3) {
      const r3 = e3 + t4 - 1;
      return e3 <= o3 && r3 >= n3;
    }(t3, i2.value || 1, o2.value, r2.value);
  }), d2 = (t3) => {
    var n3;
    const { record: o3, additionalProps: r3 } = e2;
    o3 && s2(-1, -1), null == (n3 = null == r3 ? void 0 : r3.onMouseleave) || n3.call(r3, t3);
  }, u2 = (e3) => {
    const t3 = sx(e3)[0];
    return As(t3) ? t3.type === ws ? t3.children : Array.isArray(t3.children) ? u2(t3.children) : void 0 : t3;
  };
  return () => {
    var o3, r3, p2, f2, h2;
    const { record: v2, index: m2, renderIndex: g2, dataIndex: y2, customRender: b2, component: w2 = "td", fixLeft: _2, fixRight: C2, firstFixLeft: x2, lastFixLeft: k2, firstFixRight: S2, lastFixRight: E2, appendNode: I2 = null == (o3 = t2.appendNode) ? void 0 : o3.call(t2), additionalProps: O2 = {}, ellipsis: N2, align: M2, rowType: A2, isSticky: D2, column: V2 = {}, cellType: T2 } = e2;
    let L2, P2;
    const F2 = null == (r3 = t2.default) ? void 0 : r3.call(t2);
    if (C_(F2) || "header" === T2)
      P2 = F2;
    else {
      const t3 = w_(v2, y2);
      if (P2 = t3, b2) {
        const e3 = b2({ text: t3, value: t3, record: v2, index: m2, renderIndex: g2, column: V2.__originColumn__ });
        !function(e4) {
          return e4 && "object" == typeof e4 && !Array.isArray(e4) && !As(e4);
        }(e3) ? P2 = e3 : (P2 = e3.children, L2 = e3.props);
      }
      if (!(h_ in V2) && "body" === T2 && n2.value.bodyCell) {
        const e3 = N_(n2.value, "bodyCell", { text: t3, value: t3, record: v2, index: m2, column: V2.__originColumn__ }, () => {
          const e4 = void 0 === P2 ? t3 : P2;
          return ["object" == typeof e4 && Qd(e4) || "object" != typeof e4 ? e4 : null];
        });
        P2 = pu(e3);
      }
      e2.transformCellText && (P2 = e2.transformCellText({ text: P2, record: v2, index: m2, column: V2.__originColumn__ }));
    }
    "object" == typeof P2 && !Array.isArray(P2) && !As(P2) && (P2 = null), N2 && (k2 || S2) && (P2 = ba("span", { class: "cds-table__cell-content" }, P2)), Array.isArray(P2) && 1 === (null == P2 ? void 0 : P2.length) && (P2 = P2[0]);
    const { colSpan: R2, rowSpan: $2, style: B2, class: H2, ...j2 } = L2 || {}, K2 = null != (p2 = void 0 !== R2 ? R2 : l2.value) ? p2 : 1, z2 = null != (f2 = void 0 !== $2 ? $2 : i2.value) ? f2 : 1;
    if (0 === K2 || 0 === z2)
      return null;
    const Z2 = {}, U2 = "number" == typeof _2 && a2.value, W2 = "number" == typeof C2 && a2.value;
    U2 && (Z2.position = "sticky", Z2.left = `${_2}px`), W2 && (Z2.position = "sticky", Z2.right = `${C2}px`);
    const Y2 = {};
    M2 && (Y2.textAlign = M2);
    let G2 = "";
    const q2 = true === N2 ? { showTitle: true } : N2;
    q2 && (q2.showTitle || "header" === A2) && ("string" == typeof P2 || "number" == typeof P2 ? G2 = P2.toString() : As(P2) && (G2 = u2([P2])));
    const X2 = { title: G2, ...j2, ...O2, colSpan: 1 !== K2 ? K2 : null, rowSpan: 1 !== z2 ? z2 : null, class: Cp("cds-table__cell", H2, O2.class, { "cds-table__cell--fix-left": U2 && a2.value, "cds-table__cell--fix-left-first": x2 && a2.value, "cds-table__cell--fix-left-last": k2 && a2.value, "cds-table__cell--fix-right": W2 && a2.value, "cds-table__cell--fix-right-first": S2 && a2.value, "cds-table__cell--fix-right-last": E2 && a2.value, "cds-table__cell--ellipsis": N2, "cds-table__cell--with-append": I2, "cds-table__cell--fix-sticky": (U2 || W2) && D2 && a2.value, "cds-table__cell--row-hover": !L2 && c2.value }), onMouseenter: (t3) => {
      ((t4, n3) => {
        var o4;
        const { record: r4, index: a3, additionalProps: l3 } = e2;
        r4 && s2(a3, (a3 || 0) + n3 - 1), null == (o4 = null == l3 ? void 0 : l3.onMouseenter) || o4.call(l3, t4);
      })(t3, z2);
    }, onMouseleave: d2, style: { ...Uu(O2.style), ...Y2, ...Z2, ...B2 } };
    return ba(w2, { ...X2 }, [I2, P2, null == (h2 = t2.dragHandle) ? void 0 : h2.call(t2)]);
  };
} });
function sx(e2 = []) {
  const t2 = [];
  return e2.forEach((e3) => {
    Array.isArray(e3) ? t2.push(...e3) : (null == e3 ? void 0 : e3.type) === bs ? t2.push(...sx(e3.children)) : t2.push(e3);
  }), t2.filter((e3) => !hu(e3));
}
const ax = Lo({ name: "ResizeObserver", props: ["disabled", "onResize"], emits: ["resize"], setup(e2, { slots: t2 }) {
  const n2 = vt({ width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 });
  let o2 = null, r2 = null;
  const s2 = () => {
    r2 && (r2.disconnect(), r2 = null);
  }, a2 = (t3) => {
    const { onResize: o3 } = e2, r3 = t3[0].target, { width: s3, height: a3 } = r3.getBoundingClientRect(), { offsetWidth: l3, offsetHeight: i3 } = r3, c2 = Math.floor(s3), d2 = Math.floor(a3);
    if (n2.width !== c2 || n2.height !== d2 || n2.offsetWidth !== l3 || n2.offsetHeight !== i3) {
      const e3 = { width: c2, height: d2, offsetHeight: i3, offsetWidth: l3 };
      Object.assign(n2, e3), o3 && Promise.resolve().then(() => {
        o3({ ...e3, offsetWidth: l3, offsetHeight: i3 }, r3);
      });
    }
  }, l2 = nu("ResizeObserver"), i2 = () => {
    const { disabled: t3 } = e2;
    if (t3)
      return void s2();
    const n3 = function(e3) {
      var t4;
      let n4 = (null == (t4 = null == e3 ? void 0 : e3.vnode) ? void 0 : t4.el) || e3 && (e3.$el || e3);
      for (; n4 && !n4.tagName; )
        n4 = n4.nextSibling;
      return n4;
    }(l2);
    n3 !== o2 && (s2(), o2 = n3), !r2 && n3 && (r2 = new ResizeObserver(a2), r2.observe(n3));
  };
  return Zo(() => {
    i2();
  }), Wo(() => {
    i2();
  }), Go(() => {
    s2();
  }), ho(() => e2.disabled, () => {
    i2();
  }, { flush: "post" }), () => {
    var e3;
    return null == (e3 = t2.default) ? void 0 : e3.call(t2)[0];
  };
} }), lx = Lo({ name: "MeasureCell", props: ["columnKey"], emits: ["columnResize"], setup(e2, { emit: t2 }) {
  const n2 = At();
  return Zo(() => {
    n2.value && t2("columnResize", e2.columnKey, n2.value.offsetWidth);
  }), () => ba(ax, { disabled: false, onResize: ({ offsetWidth: n3 }) => {
    t2("columnResize", e2.columnKey, n3);
  } }, { default: () => ba("td", { ref: n2, style: { padding: 0, border: 0, height: 0 } }, ba("div", { style: { height: 0, overflow: "hidden" } }, "&nbsp;")) });
} }), ix = Lo({ name: "ExpandedRow", inheritAttrs: false, props: ["component", "cellComponent", "expanded", "colSpan", "isEmpty"], setup(e2, { slots: t2, attrs: n2 }) {
  const o2 = yC(), r2 = Or(IC, {}), { fixHeader: s2, fixColumn: a2, componentWidth: l2, horizonScroll: i2 } = r2;
  return () => {
    const { component: r3, cellComponent: c2, expanded: d2, colSpan: u2, isEmpty: p2 } = e2;
    return ba(r3, { class: n2.class, style: { display: d2 ? null : "none" } }, ba(rx, { component: c2, colSpan: u2 }, { default: () => {
      var e3, n3;
      return (p2 ? i2.value : a2.value) ? ba("div", { style: { width: l2.value - (s2.value ? o2.scrollbarSize : 0) + "px", position: "sticky", left: 0, overflow: "hidden" }, class: "cds-table__expanded-row-fixed" }, null == (e3 = t2.default) ? void 0 : e3.call(t2)) : null == (n3 = t2.default) ? void 0 : n3.call(t2);
    } }));
  };
} }), cx = Lo({ name: "BodyRow", inheritAttrs: false, props: ["record", "index", "renderIndex", "recordKey", "expandedKeys", "rowComponent", "cellComponent", "customRow", "rowExpandable", "indent", "rowKey", "getRowKey", "childrenColumnName"], setup(e2, { attrs: t2 }) {
  const n2 = yC(), o2 = _C(), r2 = At(false), s2 = ga(() => e2.expandedKeys && e2.expandedKeys.has(e2.recordKey));
  po(() => {
    s2.value && (r2.value = true);
  });
  const a2 = ga(() => "row" === o2.expandableType && (!e2.rowExpandable || e2.rowExpandable(e2.record))), l2 = ga(() => "nest" === o2.expandableType), i2 = ga(() => e2.childrenColumnName && e2.record && e2.record[e2.childrenColumnName]), c2 = ga(() => a2.value || l2.value), d2 = (e3, t3) => {
    o2.onTriggerExpand(e3, t3);
  }, u2 = ga(() => {
    var t3;
    return (null == (t3 = e2.customRow) ? void 0 : t3.call(e2, e2.record, e2.index)) || {};
  }), p2 = (t3, ...n3) => {
    var r3, s3;
    o2.expandRowByClick && c2.value && d2(e2.record, t3), null == (s3 = null == (r3 = u2.value) ? void 0 : r3.onClick) || s3.call(r3, t3, ...n3);
  }, f2 = ga(() => {
    const { record: t3, index: n3, indent: r3 } = e2, { rowClassName: s3 } = o2;
    return "string" == typeof s3 ? s3 : "function" == typeof s3 ? s3(t3, n3, r3) : "";
  }), h2 = ga(() => __(o2.flattenColumns));
  return () => {
    const { class: c3, style: v2 } = t2, { record: m2, index: g2, rowKey: y2, indent: b2 = 0, rowComponent: w2, cellComponent: _2 } = e2, { fixedInfoList: C2, transformCellText: x2 } = n2, { flattenColumns: k2, expandedRowClassName: S2, indentSize: E2, expandIcon: I2, expandedRowRender: O2, expandIconColumnIndex: N2 } = o2, M2 = ba(w2, { ...u2.value, "data-row-key": y2, class: Cp(c3, f2.value, u2.value.class, "cds-table__row", `cds-table__row--level-${b2}`), style: { ...v2, ...Uu(u2.value.style) }, onClick: p2 }, k2.map((t3, n3) => {
      const { customRender: o3, dataIndex: r3, class: a3 } = t3, c4 = h2.value[n3], u3 = C2[n3];
      let p3;
      t3.customCell && (p3 = t3.customCell(m2, g2, t3));
      const f3 = n3 === (N2 || 0) && l2.value ? ba(bs, [ba("span", { style: { paddingLeft: E2 * b2 + "px" }, class: Cp("cds-table__row-indent", `cds-table__row-indent-level--${b2}`) }), I2({ expanded: s2.value, expandable: i2.value, record: m2, onExpand: d2 })]) : null;
      return ba(rx, { cellType: "body", key: c4, record: m2, index: g2, dataIndex: r3, column: t3, renderIndex: e2.renderIndex, ellipsis: t3.ellipsis, align: t3.align, component: _2, class: a3, customRender: o3, ...u3, additionalProps: p3, transformCellText: x2, appendNode: f3 });
    }));
    let A2;
    if (a2.value && (r2.value || s2.value)) {
      const e3 = O2 && O2({ record: m2, index: g2, indent: b2 + 1, expanded: s2.value }), t3 = S2 && S2(m2, g2, b2);
      A2 = ba(ix, { class: Cp("cds-table__expanded-row", `cds-table__expanded-row-level--${b2 + 1}`, t3), expanded: s2.value, component: w2, cellComponent: _2, colSpan: k2.length, isEmpty: false }, { default: () => e3 });
    }
    return ba(bs, [M2, A2]);
  };
} }), dx = Lo({ name: "Body", props: ["data", "getRowKey", "measureColumnWidth", "expandedKeys", "customRow", "rowExpandable", "childrenColumnName"], setup(e2, { slots: t2 }) {
  const n2 = Or(CC, {}), o2 = yC(), r2 = _C(), s2 = function(e3, t3, n3, o3) {
    return ga(() => {
      const r3 = t3.value, s3 = n3.value, a3 = e3.value;
      if (s3.size) {
        const e4 = [];
        for (let t4 = 0; t4 < (null == a3 ? void 0 : a3.length); t4 += 1) {
          const n4 = a3[t4];
          e4.push(...nx(n4, 0, r3, s3, o3.value, t4));
        }
        return e4;
      }
      return null == a3 ? void 0 : a3.map((e4, t4) => ({ record: e4, indent: 0, index: t4 }));
    });
  }(jt(e2, "data"), jt(e2, "childrenColumnName"), jt(e2, "expandedKeys"), jt(e2, "getRowKey")), a2 = At(-1), l2 = At(-1);
  let i2;
  return ((e3) => {
    Ir(EC, e3);
  })({ startRow: a2, endRow: l2, onHover: (e3, t3) => {
    clearTimeout(i2), i2 = setTimeout(() => {
      a2.value = e3, l2.value = t3;
    }, 100);
  } }), () => {
    const { data: a3, getRowKey: l3, measureColumnWidth: i3, expandedKeys: c2, customRow: d2, rowExpandable: u2, childrenColumnName: p2 } = e2, { onColumnResize: f2 } = n2, { getComponent: h2 } = o2, { flattenColumns: v2 } = r2, m2 = h2(["body", "wrapper"], "tbody"), g2 = h2(["body", "row"], "tr"), y2 = h2(["body", "cell"], "td");
    let b2;
    b2 = a3.length ? s2.value.map((e3, t3) => {
      const { record: n3, indent: o3, index: r3 } = e3, s3 = l3(n3, t3);
      return ba(cx, { key: s3, record: n3, renderIndex: r3, recordKey: s3, rowKey: s3, index: t3, rowComponent: g2, cellComponent: y2, expandedKeys: c2, customRow: d2, getRowKey: l3, rowExpandable: u2, childrenColumnName: p2, indent: o3 });
    }) : ba(ix, { expanded: true, isEmpty: true, class: "cds-table-placeholder", component: g2, cellComponent: y2, colSpan: v2.length }, { default: () => {
      var e3;
      return null == (e3 = t2.emptyNode) ? void 0 : e3.call(t2);
    } });
    const w2 = __(v2);
    return ba(m2, { class: "cds-table__tbody" }, [i3 && ba("tr", { ariaHidden: true, class: "cds-table__measure-row", style: { height: 0, fontSize: 0 } }, w2.map((e3) => ba(lx, { key: e3, columnKey: e3, onColumnResize: f2 }))), b2]);
  };
} }), ux = Lo({ name: "ColGroup", props: ["colWidths", "columns", "columnCount"], setup: (e2) => () => {
  const { colWidths: t2, columns: n2, columnCount: o2 } = e2, r2 = [];
  let s2 = false;
  for (let e3 = (o2 || (null == n2 ? void 0 : n2.length)) - 1; e3 >= 0; e3 -= 1) {
    const o3 = t2[e3], a2 = n2 && n2[e3], l2 = a2 && a2[h_];
    if (o3 || l2 || s2) {
      const { columnType: t3, ...n3 } = l2 || {};
      r2.unshift(ba("col", { key: e3, style: { width: "number" == typeof o3 ? `${o3}px` : o3 }, ...n3 })), s2 = true;
    }
  }
  return ba("colgroup", r2);
} }), px = Lo({ name: "FixedHolder", inheritAttrs: false, props: ["columns", "flattenColumns", "stickyOffsets", "customHeaderRow", "noData", "maxContentScroll", "colWidths", "columnCount", "fixHeader", "stickyTopOffset", "stickyBottomOffset", "stickyClassName"], emits: ["scroll"], setup(e2, { attrs: t2, slots: n2, emit: o2 }) {
  const r2 = yC(), s2 = ga(() => r2.isSticky && !e2.fixHeader ? 0 : r2.scrollbarSize), a2 = At(), l2 = At(), i2 = (e3) => {
    const { currentTarget: t3, deltaX: n3 } = e3;
    n3 && (o2("scroll", { currentTarget: t3, scrollLeft: t3.scrollLeft + n3 }), e3.preventDefault());
  };
  Zo(() => {
    un(() => {
      l2.value = mu(a2.value, "wheel", i2);
    });
  }), Yo(() => {
    var e3;
    null == (e3 = l2.value) || e3.remove();
  });
  const c2 = ga(() => e2.flattenColumns.every((e3) => e3.width && 0 !== e3.width && "0px" !== e3.width)), d2 = At([]), u2 = At([]);
  po(() => {
    const t3 = e2.flattenColumns[e2.flattenColumns.length - 1], n3 = { fixed: t3 ? t3.fixed : null, scrollbar: true, customHeaderCell: () => ({ class: "cds-table__cell--scrollbar" }) };
    d2.value = s2.value ? [...e2.columns, n3] : e2.columns, u2.value = s2.value ? [...e2.flattenColumns, n3] : e2.flattenColumns;
  });
  const p2 = ga(() => {
    const { stickyOffsets: t3 } = e2;
    return { ...t3, right: [...t3.right.map((e3) => e3 + s2.value), 0], isSticky: r2.isSticky };
  }), f2 = function(e3, t3) {
    return ga(() => {
      const n3 = [], o3 = e3.value, r3 = t3.value;
      for (let e4 = 0; e4 < r3; e4 += 1) {
        const t4 = o3[e4];
        if (void 0 === t4)
          return null;
        n3[e4] = t4;
      }
      return n3;
    });
  }(jt(e2, "colWidths"), jt(e2, "columnCount"));
  return () => {
    var o3;
    const { noData: l3, columnCount: i3, stickyTopOffset: h2, stickyBottomOffset: v2, stickyClassName: m2, maxContentScroll: g2 } = e2, { isSticky: y2 } = r2;
    return ba("div", { ref: a2, class: Cp(t2.class, { [m2]: !!m2 }), style: { overflow: "hidden", ...y2 ? { top: `${h2}px`, bottom: `${v2}px` } : {} } }, ba("table", { style: { tableLayout: "fixed", visibility: l3 || f2.value ? null : "hidden" } }, [(!l3 || !g2 || c2.value) && ba(ux, { colWidths: f2.value ? [...f2.value, s2.value] : [], columnCount: i3 + 1, columns: u2.value }), null == (o3 = n2.default) ? void 0 : o3.call(n2, { ...e2, stickyOffsets: p2.value, columns: d2.value, flattenColumns: u2.value })]));
  };
} }), fx = Lo({ name: "Footer", inheritAttrs: false, props: ["stickyOffsets", "flattenColumns"], setup: (e2, { slots: t2 }) => (((e3) => {
  Ir(OC, e3);
})(vt({ stickyOffsets: jt(e2, "stickyOffsets"), flattenColumn: jt(e2, "flattenColumns"), scrollColumnIndex: ga(() => {
  const t3 = e2.flattenColumns.length - 1, n2 = e2.flattenColumns[t3];
  return null != n2 && n2.scrollbar ? t3 : null;
}) })), () => {
  var e3;
  return ba("tfoot", { class: "cds-table__summary" }, null == (e3 = t2.default) ? void 0 : e3.call(t2));
}) }), hx = { start: "mousedown", move: "mousemove", stop: "mouseup" }, vx = { start: "touchstart", move: "touchmove", stop: "touchend" }, mx = Lo({ name: "DragHandle", props: { width: { type: Number, required: true }, minWidth: { type: Number, default: 50 }, maxWidth: { type: Number, default: 1 / 0 }, column: { type: Object, default: void 0 } }, setup(e2) {
  let t2 = 0, n2 = { remove: () => {
  } }, o2 = { remove: () => {
  } };
  const r2 = () => {
    n2.remove(), o2.remove();
  };
  Go(() => {
    r2();
  }), po(() => {
    isNaN(e2.width);
  });
  const { onResizeColumn: s2 } = Or(bC, { onResizeColumn: () => {
  } }), a2 = ga(() => "number" != typeof e2.minWidth || isNaN(e2.minWidth) ? 50 : e2.minWidth), l2 = ga(() => "number" != typeof e2.maxWidth || isNaN(e2.maxWidth) ? 1 / 0 : e2.maxWidth), i2 = nu("drag-handle"), c2 = At(false);
  let d2, u2 = 0;
  const p2 = (n3) => {
    let o3 = 0;
    o3 = n3.touches ? n3.touches.length ? n3.touches[0].pageX : n3.changedTouches[0].pageX : n3.pageX;
    const r3 = t2 - o3;
    let i3 = Math.max(u2 - r3, a2.value);
    i3 = Math.min(i3, l2.value), yp.cancel(d2), d2 = yp(() => {
      s2(i3, e2.column.__originColumn__);
    });
  }, f2 = (e3) => {
    p2(e3);
  }, h2 = (e3) => {
    c2.value = false, p2(e3), r2();
  }, v2 = (e3, s3) => {
    var a3, l3;
    c2.value = true, r2(), u2 = null == (l3 = null == (a3 = null == i2 ? void 0 : i2.vnode) ? void 0 : a3.el) ? void 0 : l3.parentNode.getBoundingClientRect().width, !(e3 instanceof MouseEvent && 1 !== e3.which) && (e3.stopPropagation && e3.stopPropagation(), t2 = e3.touches ? e3.touches[0].pageX : e3.pageX, n2 = mu(document.documentElement, s3.move, f2), o2 = mu(document.documentElement, s3.stop, h2));
  }, m2 = (e3) => {
    e3.stopPropagation(), e3.preventDefault(), v2(e3, hx);
  }, g2 = (e3) => {
    e3.stopPropagation(), e3.preventDefault();
  };
  return () => {
    const e3 = { [vu ? "onTouchstartPassive" : "onTouchstart"]: (e4) => ((e5) => {
      e5.stopPropagation(), e5.preventDefault(), v2(e5, vx);
    })(e4) };
    return ba("div", { class: Cp("cds-table__resize-handle", { [Sd.dragging]: c2.value }), onMousedown: m2, ...e3, onClick: g2 }, ba("div", { class: "cds-table__resize-handle-line" }));
  };
} }), gx = Lo({ name: "HeaderRow", props: ["cells", "flattenColumns", "stickyOffsets", "rowComponent", "cellComponent", "index", "customHeaderRow"], setup: (e2) => () => {
  const { cells: t2, stickyOffsets: n2, flattenColumns: o2, rowComponent: r2, cellComponent: s2, index: a2, customHeaderRow: l2 } = e2;
  let i2;
  l2 && (i2 = l2(t2.map((e3) => e3.column), a2));
  const c2 = __(t2.map((e3) => e3.column));
  return ba(r2, { ...i2 }, t2.map((e3, t3) => {
    var r3, a3;
    const { column: l3 } = e3, i3 = O_(e3.colStart, e3.colEnd, o2, n2);
    let d2;
    l3 && l3.customHeaderCell && (d2 = null == (a3 = (r3 = e3.column).customHeaderCell) ? void 0 : a3.call(r3, l3));
    const u2 = l3;
    return ba(rx, { ...e3, cellType: "header", ellipsis: l3.ellipsis, align: l3.align, component: s2, key: c2[t3], ...i3, rowType: "header", additionalProps: d2, column: l3 }, { default: () => l3.title, dragHandle: () => u2.resizable ? ba(mx, { column: u2, width: u2.width, minWidth: u2.minWidth, maxWidth: u2.maxWidth }) : null });
  }));
} }), yx = Lo({ name: "Header", inheritAttrs: false, props: ["columns", "flattenColumns", "stickyOffsets", "customHeaderRow"], setup(e2) {
  const t2 = yC(), n2 = ga(() => function(e3) {
    const t3 = [];
    o2(e3, 0);
    const n3 = t3.length;
    for (let e4 = 0; e4 < n3; e4 += 1)
      t3[e4].forEach((t4) => {
        !("rowSpan" in t4) && !t4.hasSubColumns && (t4.rowSpan = n3 - e4);
      });
    function o2(e4, n4, r2 = 0) {
      t3[r2] = t3[r2] || [];
      let s2 = n4;
      return e4.filter(Boolean).map((e5) => {
        const n5 = { key: e5.key, column: e5, colStart: s2, class: Cp(e5.className, e5.class) };
        let a2 = 1;
        const l2 = e5.children;
        return l2 && l2.length > 0 && (a2 = o2(l2, s2, r2 + 1).reduce((e6, t4) => e6 + t4, 0), n5.hasSubColumns = true), "colSpan" in e5 && (a2 = e5.colSpan), "rowSpan" in e5 && (n5.rowSpan = e5.rowSpan), n5.colSpan = a2, n5.colEnd = n5.colStart + a2 - 1, t3[r2].push(n5), s2 += a2, a2;
      });
    }
    return t3;
  }(e2.columns));
  return () => {
    const { getComponent: o2 } = t2, { stickyOffsets: r2, flattenColumns: s2, customHeaderRow: a2 } = e2, l2 = o2(["header", "wrapper"], "thead"), i2 = o2(["header", "row"], "tr"), c2 = o2(["header", "cell"], "th");
    return ba(l2, { class: "cds-table__thead" }, n2.value.map((e3, t3) => ba(gx, { key: t3, index: t3, cells: e3, rowComponent: i2, cellComponent: c2, stickyOffsets: r2, flattenColumns: s2, customHeaderRow: a2 })));
  };
} });
const bx = Lo({ name: "Panel", setup: (e2, { slots: t2 }) => () => {
  var e3;
  return ba("div", null == (e3 = t2.default) ? void 0 : e3.call(t2));
} }), wx = Lo({ name: "StickyScrollbar", inheritAttrs: false, props: ["scrollBodyRef", "onScroll", "offsetScroll", "container", "scrollBodySizeInfo"], emits: ["scroll"], setup(e2, { emit: t2, expose: n2 }) {
  const o2 = At(0), r2 = At(0), s2 = At(0), a2 = At(), l2 = At(false), i2 = At({ delta: 0, x: 0 }), [c2, d2] = JC({ scrollLeft: 0, isHiddenScrollbar: true });
  po(() => {
    o2.value = e2.scrollBodySizeInfo.scrollWidth || 0, r2.value = e2.scrollBodySizeInfo.clientWidth || 0, s2.value = o2.value && r2.value * (r2.value / o2.value);
  }, { flush: "post" });
  const u2 = () => {
    l2.value = false;
  }, p2 = (e3) => {
    const { buttons: n3 } = e3 || (null == window ? void 0 : window.event);
    if (!l2.value || 0 === n3)
      return void (l2.value && (l2.value = false));
    let a3 = i2.value.x + e3.pageX - i2.value.x - i2.value.delta;
    a3 <= 0 && (a3 = 0), a3 + s2.value >= r2.value && (a3 = r2.value - s2.value), t2("scroll", { scrollLeft: a3 / r2.value * (o2.value + 2) }), i2.value.x = e3.pageX;
  }, f2 = () => {
    if (!e2.scrollBodyRef.value)
      return;
    const t3 = ep(e2.scrollBodyRef.value).top, n3 = t3 + e2.scrollBodyRef.value.offsetHeight, o3 = e2.container === window ? document.documentElement.scrollTop + window.innerHeight : ep(e2.container).top + e2.container.clientHeight;
    n3 - Qu() <= o3 || t3 >= o3 - e2.offsetScroll ? d2((e3) => ({ ...e3, isHiddenScrollbar: true })) : d2((e3) => ({ ...e3, isHiddenScrollbar: false }));
  };
  let h2 = null, v2 = null, m2 = null, g2 = null;
  Zo(() => {
    h2 = mu(document.body, "mouseup", u2, false), v2 = mu(document.body, "mousemove", p2, false), m2 = mu(window, "resize", f2, false);
  }), Ro(() => {
    un(() => {
      f2();
    });
  }), Zo(() => {
    setTimeout(() => {
      ho([s2, l2], () => {
        f2();
      }, { immediate: true, flush: "post" });
    });
  }), ho(() => e2.container, () => {
    null == g2 || g2.remove(), g2 = mu(e2.container, "scroll", f2, false);
  }, { immediate: true, flush: "post" }), Yo(() => {
    null == h2 || h2.remove(), null == v2 || v2.remove(), null == g2 || g2.remove(), null == m2 || m2.remove();
  }), ho(() => ({ ...c2.value }), (t3, n3) => {
    t3.isHiddenScrollbar !== (null == n3 ? void 0 : n3.isHiddenScrollbar) && !t3.isHiddenScrollbar && d2((t4) => {
      const n4 = e2.scrollBodyRef.value;
      return n4 ? { ...t4, scrollLeft: n4.scrollLeft / n4.scrollWidth * n4.clientWidth } : t4;
    });
  }, { immediate: true });
  const y2 = Qu();
  return n2({ setScrollLeft: (e3) => {
    d2((t3) => ({ ...t3, scrollLeft: e3 / o2.value * r2.value || 0 }));
  } }), () => o2.value <= r2.value || !s2.value || c2.value.isHiddenScrollbar ? null : ba("div", { class: "cds-table__sticky-scroll", style: { height: y2 + "px", width: r2.value + "px", bottom: e2.offsetScroll + "px" } }, ba("div", { ref: a2, class: Cp("cds-table__sticky-scroll-bar", { "cds-table__sticky-scroll-bar--active": l2.value }), style: { width: s2.value + "px", transform: `translateX(${c2.value.scrollLeft}px, 0, 0)` } }));
} }), _x = [], Cx = Lo({ name: "InternalTable", inheritAttrs: false, props: { ...m_(), ...Xf(), ...Ev(), ...Zf(), ...Qf(), ...th(), internalHooks: [String], transformColumns: { type: Function }, internalRefs: { type: Object }, onUpdateInternalRefs: { type: Function } }, emits: ["change", "expand", "expandedRowsChange", "updateInternalRefs", "update:expandedRowKeys"], setup(e2, { attrs: t2, slots: n2, emit: o2 }) {
  const r2 = ga(() => e2.data || _x), s2 = ga(() => !!r2.value.length), a2 = ga(() => b_(e2.components, {})), l2 = (e3, t3) => w_(a2.value, e3) || t3, i2 = ga(() => {
    const t3 = e2.rowKey;
    return "function" == typeof t3 ? t3 : (e3) => e3 & e3[t3];
  }), c2 = ga(() => e2.expandIcon || ox(e2.locale, e2.size)), d2 = ga(() => e2.childrenColumnName || "children"), u2 = ga(() => e2.expandedRowRender ? "row" : !(!e2.canExpandable && !r2.value.some((e3) => e3 && "object" == typeof e3 && e3[d2.value])) && "nest"), p2 = Dt([]);
  po(() => {
    e2.defaultExpandAllRows && (p2.value = e2.defaultExpandedRowKeys), e2.defaultExpandAllRows && (p2.value = function(e3, t3, n3) {
      const o3 = [];
      return function e4(r3) {
        (r3 || []).forEach((r4, s3) => {
          o3.push(t3(r4, s3)), e4(r4[n3]);
        });
      }(e3), o3;
    }(r2.value, i2.value, d2.value));
  })();
  const f2 = ga(() => new Set(e2.expandedRowKeys || p2.value || [])), h2 = (e3) => {
    const t3 = i2.value(e3, r2.value.indexOf(e3));
    let n3;
    const s3 = f2.value.has(t3);
    s3 ? (f2.value.delete(t3), n3 = [...f2.value]) : n3 = [...f2.value, t3], p2.value = n3, o2("expand", !s3, e3), o2("update:expandedRowKeys", n3), o2("expandedRowsChange", n3);
  };
  e2.expandedRowRender && r2.value.some((e3) => Array.isArray(null == e3 ? void 0 : e3[d2.value]));
  const { columns: v2, flattenColumns: m2 } = pC({ columns: jt(e2, "columns", []), rowExpandable: jt(e2, "rowExpandable"), expandIcon: c2, expandRowByClick: jt(e2, "expandRowByClick"), expandColumnWidth: jt(e2, "expandColumnWidth"), expandFixed: jt(e2, "expandFixed"), expandable: ga(() => !!e2.expandedRowRender), expandedKeys: f2, getRowKey: i2, onTriggerExpand: h2 }, ga(() => e2.internalHooks === v_ ? e2.transformColumns : null)), g2 = ga(() => ({ columns: v2.value, flattenColumns: m2.value })), { fixHeader: y2, fixColumn: b2, fixFooter: w2, horizonScroll: _2, stickyOffsets: C2, stickyState: x2, scrollbarSize: k2, mergedTableLayout: S2, componentWidth: E2, colWidths: I2, columnCount: O2, pingedLeft: N2, pingedRight: M2, scrollXStyle: A2, scrollYStyle: D2, scrollTableStyle: V2, scrollBodyRef: T2, scrollHeaderRef: L2, scrollSummaryRef: P2, fullTableRef: F2, stickyRef: R2, scrollBodySizeInfo: $2, summaryCollect: B2, onColumnResize: H2, onFullTableResize: j2, onScroll: K2 } = tx(e2, m2);
  ((e3) => {
    Ir(gC, e3);
  })(vt({ ...$t(lu(e2, "transformCellText")), getComponent: l2, scrollbarSize: k2, fixedInfoList: ga(() => m2.value.map((e3, t3) => O_(t3, t3, m2.value, C2.value))), isSticky: ga(() => x2.value.isSticky), summaryCollect: B2 })), ((e3) => {
    Ir(wC, e3);
  })(vt({ ...$t(lu(e2, "rowClassName", "expandedRowClassName", "expandRowByClick", "expandedRowRender", "expandIconColumnIndex", "indentSize")), columns: v2, flattenColumns: m2, tableLayout: S2, expandIcon: c2, expandableType: u2, onTriggerExpand: h2 })), ((e3) => {
    Ir(CC, e3);
  })({ onColumnResize: H2 }), ((e3) => {
    Ir(IC, e3);
  })({ componentWidth: E2, fixHeader: y2, fixColumn: b2, horizonScroll: _2 });
  const z2 = () => ba(dx, { data: r2.value, getRowKey: i2.value, measureColumnWidth: y2.value || _2.value || x2.value.isSticky, expandedKeys: f2.value, customRow: e2.customRow, rowExpandable: e2.rowExpandable, childrenColumnName: d2.value }, { emptyNode: () => (() => {
    var e3;
    return s2.value ? null : (null == (e3 = n2.emptyText) ? void 0 : e3.call(n2)) || "No Data";
  })() }), Z2 = () => ba(ux, { colWidths: m2.value.map(({ width: e3 }) => e3), columns: m2.value });
  Oh(() => {
    var o3;
    const { id: a3, scroll: i3, tableLayout: c3, showHeader: d3, customHeaderRow: u3, header: p3 = n2.header, footer: f3 = n2.footer } = e2, h3 = { colWidths: I2.value, columnCount: m2.value.length, stickyOffsets: C2.value, customHeaderRow: u3, fixHeader: y2.value, scroll: i3 }, { isSticky: E3, offsetHeader: B3, offsetSummary: H3, offsetScroll: U2, stickyClassName: W2, container: Y2 } = x2.value, G2 = { noData: !r2.value.length, maxContentScroll: _2.value && "max-content" === (null == i3 ? void 0 : i3.x), ...h3, ...g2.value, stickyClassName: W2, onScroll: K2 }, q2 = l2(["table"], "table"), X2 = l2(["body"]), J2 = null == (o3 = n2.summary) ? void 0 : o3.call(n2, { pageData: r2.value });
    "function" == typeof X2 && s2.value && y2.value;
    let Q2 = () => null;
    if (y2.value || E3) {
      let e3 = () => null;
      "function" == typeof X2 ? (e3 = () => X2(r2.value, { scrollbarSize: k2.value, ref: T2, onScroll: K2 }), h3.colWidths = m2.value.map(({ width: e4 }, t3) => {
        const n3 = t3 === v2.value.length - 1 ? e4 - k2.value : e4;
        return "number" != typeof n3 || Number.isNaN(n3) ? 0 : n3;
      })) : e3 = () => ba("div", { ref: T2, class: "cds-table__body", style: { ...A2.value, ...D2.value }, onScroll: K2 }, ba(q2, { style: { ...V2.value, tableLayout: S2.value } }, [Z2(), z2(), !w2.value && J2 && ba(fx, { stickyOffsets: C2.value, flattenColumns: m2.value }, J2)])), Q2 = () => ba(bs, [false !== d3 && ba(px, { ...G2, ref: L2, stickyTopOffset: B3, class: "cds-table__header" }, { default: (e4) => ba(bs, [ba(yx, { ...e4 }), "top" === w2.value && ba(fx, { ...e4 }, J2)]) }), e3(), w2 && "top" !== w2.value && ba(px, { ...G2, ref: P2, stickyBottomOffset: H3, class: "cds-table__summary" }, { default: (e4) => ba(fx, { ...e4 }, J2) }), E3 && T2.value && ba(wx, { ref: R2, scrollBodySizeInfo: $2.value, offsetScroll: U2, scrollBodyRef: T2, container: Y2, onScroll: K2 })]);
    } else
      Q2 = () => ba("div", { ref: T2, class: "cds-table__content", style: { ...A2.value, ...D2.value }, onScroll: K2 }, ba(q2, { style: { ...V2.value, tableLayout: S2.value } }, [Z2(), false !== d3 && ba(yx, { ...h3, ...g2.value }), z2(), J2 && ba(fx, { stickyOffsets: C2.value, flattenColumns: m2.value }, J2)]));
    const ee2 = du(t2, { aria: true, data: true }), te2 = () => ba("div", { ...ee2, id: a3, ref: F2, class: Cp({ "cds-table": true, "cds-table--layout-fixed": "fixed" === c3, "cds-table--fixed-header": y2.value, "cds-table--fixed-column": b2.value, "cds-table--ping-left": N2.value, "cds-table--ping-right": M2.value, "cds-table--scroll-horizontal": _2.value, "cds-table--has-fix-left": m2.value[0] && m2.value[0].fixed, "cds-table--has-fix-right": m2.value[O2.value - 1] && "right" === m2.value[O2.value - 1].fixed, [t2.class]: t2.class }), style: t2.style }, [p3 && ba(bx, { class: "cds-table__title" }, { default: () => p3(r2.value) }), ba("div", { class: "cds-table__container" }, Q2()), f3 && ba(bx, { class: "cds-table__footer" }, { default: () => f3(r2.value) })]);
    return _2.value ? ba(ax, { onResize: j2 }, { default: () => te2() }) : te2();
  });
} }), xx = Lo({ name: "TableProvider", inheritAttrs: false, props: { ...m_(), ...Xf(), ...Ev(), ...Zf(), ...Qf(), ...th(), ...Zv(), contextSlots: { type: Object, required: true } }, emits: { change: (e2, t2, n2, o2) => true, expand: (e2, t2) => true, expandedRowsChange: (e2) => true, resizeColumn: (e2, t2) => true, "update:expandedRowKeys": (e2) => true }, setup(e2, { attrs: t2, slots: n2, emit: o2 }) {
  const { themeClasses: r2 } = Yv(e2);
  ((e3) => {
    Ir(xC, e3);
  })(ga(() => e2.contextSlots)), ((e3) => {
    Ir(bC, e3);
  })({ onResizeColumn: (e3, t3) => {
    o2("resizeColumn", e3, t3);
  } });
  const s2 = nv(), a2 = ga(() => {
    var t3;
    return null == (t3 = e2.columns) ? void 0 : t3.filter((e3) => !e3.responsive || e3.responsive.some((e4) => s2[e4].value));
  }), l2 = ga(() => e2.data || []), i2 = ga(() => e2.childrenColumnName || "children"), c2 = ga(() => l2.value.some((e3) => null == e3 ? void 0 : e3[i2.value]) ? "nest" : !!e2.expandedRowRender && "row"), d2 = vt({ body: null }), u2 = (e3) => {
    Object.assign(d2, e3);
  }, p2 = At(Iv(e2).table), f2 = ga(() => e2.transformCellText), h2 = ga(() => "function" == typeof e2.rowKey ? e2.rowKey : (t3) => null == t3 ? void 0 : t3[e2.rowKey]), { getRecordByKey: v2 } = g_(l2, i2, h2), m2 = {}, g2 = (t3, n3, o3 = false) => {
    const { pagination: r3, scroll: s3, onChange: a3 } = e2, c3 = { ...m2, ...t3 };
    o3 && (m2.resetPagination(), c3.pagination.modelValue && (c3.pagination.modelValue = 1), r3 && r3.modelValue && (r3.modelValue = 1, r3.length = c3.pagination.length)), s3 && false !== s3.scrollToFirstRowOnChange && d2.body && _p(0, { getContainer: () => d2.body }), null == a3 || a3(c3.pagination, c3.filters, c3.sorter, { currentData: LC(KC(l2.value, c3.sorterStates, i2.value), c3.filterStates), action: n3 });
  }, y2 = ga(() => e2.sortDirection || [BC, HC]), b2 = jt(e2, "showSorterTooltip"), { transformColumns: w2, mergedSorterStates: _2, columnTitleSorterProps: C2, sorters: x2 } = jC({ mergedColumns: a2, onSorterChange: (e3, t3) => {
    g2({ sorter: e3, sorterStates: t3 }, "sort", false);
  }, sortDirections: y2, tableLocale: p2, showSorterTooltip: b2 }), k2 = ga(() => KC(l2.value, _2.value, i2.value)), S2 = jt(e2, "getPopupContainer"), { transformColumns: E2, mergedFilterStates: I2, filters: O2 } = VC({ mergedColumns: a2, onFilterChange: (e3, t3) => {
    g2({ filters: e3, filterStates: t3 }, "filter", true);
  }, tableLocale: p2, getPopupContainer: S2 }), N2 = ga(() => LC(k2.value, I2.value)), { filledColumns: M2 } = uC(jt(e2, "contextSlots")), A2 = ga(() => ({ ...C2.value })), { filledColumns: D2 } = fC(A2), { mergedPagination: V2, resetPagination: T2 } = XC(ga(() => N2.value.length), jt(e2, "pagination"), jt(e2, "disabled"), (e3, t3) => {
    g2({ pagination: { ...m2.pagination, modelValue: e3, length: t3 } }, "paginate");
  });
  po(() => {
    m2.sorter = x2.value, m2.sorterStates = _2.value, m2.filters = O2.value, m2.filterStates = I2.value, m2.pagination = false === e2.pagination ? {} : function(e3, t3) {
      const n3 = { current: t3.modelValue, length: t3.length };
      return Object.keys(e3 && "object" == typeof e3 ? e3 : {}).forEach((e4) => {
        const o3 = t3[e4];
        "function" != typeof o3 && (n3[e4] = o3);
      }), n3;
    }(e2.pagination, V2.value), m2.resetPagination = T2;
  });
  const L2 = ga(() => {
    if (false === e2.pagination || !V2.value.length)
      return N2.value;
    const { modelValue: t3 = 1, length: n3 = qC, total: o3 } = V2.value;
    return N2.value.length < o3 ? N2.value.length > n3 ? N2.value.slice((t3 - 1) * o3, t3 * o3) : N2.value : N2.value.slice((t3 - 1) * o3, t3 * o3);
  });
  po(() => {
    un(() => {
      const { total: e3, length: t3 = qC } = V2.value;
      N2.value.length < e3 && N2.value.length;
    });
  }, { flush: "post" });
  const P2 = ga(() => false === e2.showExpandColumn ? -1 : "nest" === c2.value && void 0 === e2.expandIconColumnIndex ? e2.rowSelection ? 1 : 0 : e2.expandIconColumnIndex > 0 && e2.rowSelection ? e2.expandIconColumnIndex - 1 : e2.expandIconColumnIndex), F2 = At();
  ho(() => e2.rowSelection, () => {
    F2.value = e2.rowSelection ? { ...e2.rowSelection } : e2.rowSelection;
  }, { deep: true, immediate: true });
  const { transformColumns: R2, selectedKeySet: $2 } = cC(F2, { data: N2, pageData: L2, getRowKey: h2, getRecordByKey: v2, expandType: c2, childrenColumnName: i2, locale: p2, getPopupContainer: ga(() => e2.getPopupContainer) }), B2 = (t3, n3, o3) => {
    let r3;
    const { rowClassName: s3 } = e2;
    return r3 = Cp("function" == typeof s3 ? s3(t3, n3, o3) : s3), Cp({ "cds-table__row--selected": $2.value.has(h2.value(t3, n3)) }, r3);
  }, H2 = ga(() => "number" == typeof e2.indentSize ? e2.indentSize : 32), j2 = (e3) => D2(R2(E2(w2(M2(e3)))));
  return Oh(() => {
    const { expandIcon: o3 = n2.expandIcon || ox(p2.value, e2.size), pagination: s3, loading: l3, hovered: i3, bordered: c3, borderless: v3, disabled: m3, readonly: g3, size: y3, expandedRowKeys: b3, defaultExpandedRowKeys: w3 } = e2, _3 = Gd(e2, ["columns"]), { topPaginationNode: C3, bottomPaginationNode: x3 } = function(e3, t3) {
      var n3;
      let o4, r3;
      if (false !== t3 && (null == (n3 = e3.value) ? void 0 : n3.total)) {
        const t4 = (t5) => ba(lw, { ...e3.value, class: Cp("cds-table__pagination", `cds-table__pagination--${t5}`, e3.value.class) }), { position: n4 } = e3.value, s4 = "right";
        if (null !== n4 && Array.isArray(n4)) {
          const e4 = n4.find((e5) => -1 !== e5.indexOf("top")), a3 = n4.find((e5) => -1 !== e5.indexOf("bottom")), l4 = n4.every((e5) => "none" == `${e5}`);
          !e4 && !a3 && !l4 && (r3 = t4(s4)), e4 && (o4 = t4(e4.toLowerCase().replace("top", ""))), a3 && (r3 = t4(a3.toLowerCase().replace("bottom", "")));
        } else
          r3 = t4(s4);
      }
      return { topPaginationNode: o4, bottomPaginationNode: r3 };
    }(V2, s3);
    return ba("div", { class: Cp("cds-table-wrapper", Lt(r2), t2.class), style: t2.style }, [l3 && ba(Gg, { overlay: true, fixed: true, class: "cds-table__loading" }), C3, ba(Cx, { ...t2, ..._3, data: L2.value, columns: a2.value, rowKey: h2.value, rowClassName: B2, indentSize: H2.value, expandIconColumnIndex: P2.value, expandedRowKeys: b3, defaultExpandedRowKeys: w3, expandIcon: o3, class: Cp({ [`cds-table__size--${y3}`]: y3, "cds-table--bordered": c3, "cds-table--borderless": v3, "cds-table--hovered": i3, "cds-table--disabled": m3, "cds-table--readonly": g3 }), transformCellText: f2.value, internalHooks: v_, onUpdateInternalRefs: u2, internalRefs: d2, transformColumns: j2 }, { ...n2, emptyText: () => {
      var e3;
      return (null == (e3 = n2.emptyText) ? void 0 : e3.call(n2)) || p2.value.emptyText;
    } }), x3]);
  }), { selectedKeySet: $2 };
} }), kx = Lo({ name: "CdsTable", inheritAttrs: false, props: { ...m_(), ...Xf(), ...Ev(), ...Zf(), ...Qf(), ...th(), ...Zv() }, emits: { change: (e2, t2, n2, o2) => true, expand: (e2, t2) => true, expandedRowsChange: (e2) => true, resizeColumn: (e2, t2) => true, "update:expandedRowKeys": (e2) => true }, setup(e2, { attrs: t2, slots: n2 }) {
  const o2 = At();
  return Oh(() => {
    var r2;
    const s2 = e2.columns || k_(null == (r2 = n2.default) ? void 0 : r2.call(n2));
    return ba(xx, { ref: o2, ...t2, ...e2, columns: s2 || [], expandedRowRender: n2.expandedRowRender, contextSlots: { ...n2 } }, n2);
  }), { table: o2 };
} }), Sx = Lo({ name: "CdsTableColumn", slots: ["title", "filterIcon", "default"], render: () => null }), Ex = Lo({ name: "CdsTableColumnGroup", slots: ["title", "default"], __CDS_TABLE_COLUMN_GROUP: true, render: () => null }), Ix = "CdsTextArea", Ox = Lo({ name: Ix, components: { CdsInput: Gm, CdsInputLabel: Jm, CdsInputDescription: Qm, CdsInputCounter: eg, CdsIcon: Lm, CdsLoader: Gg, CdsTransition: Zh }, inheritAttrs: false, props: { ...jh(), ...Nh(), ...Fh(), ...jf(), ...Kf(), ...mh(), ...Qf(), ...th(), ...Zf(), ...Zv(), autoGrow: { type: Boolean, default: false }, rows: { type: [Number, String], default: 5, validator: (e2) => !isNaN(parseFloat(e2)) }, maxRows: { type: [Number, String], default: 0, validator: (e2) => !isNaN(parseFloat(e2)) }, noResize: { type: Boolean, default: false } }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true, "click:clear": (e2) => true, "click:prepend": (e2) => true, "click:append": (e2) => true, "click:prependInner": (e2) => true, "click:appendInner": (e2) => true }, setup(e2, { slots: t2, attrs: n2, emit: o2, expose: r2 }) {
  const s2 = vh(e2, "modelValue"), { themeClasses: a2 } = Yv(e2), { id: l2, hasLabel: i2, hasDescription: c2 } = Mh(e2, t2, Ix), { hasPrepend: d2, hasAppend: u2, iconClasses: p2 } = zf(e2, t2, Ix), { hasLoading: f2, loadingClasses: h2 } = Uf(e2), { isFocused: v2, focus: m2, blur: g2 } = gh(e2, Ix), { hasPrependInnerIcon: y2, hasAppendInnerIcon: b2, fieldClasses: w2, fieldProps: _2, handleClickPrependInner: C2, handleClickAppendInner: x2 } = Kh(e2, t2, Ix), { inputProps: k2 } = Ah(e2, [..._2, "modelValue", "focused", "loading", "rows", "maxRows"]), { rootAttrs: S2, inputAttrs: E2 } = Dh(n2), I2 = ga(() => e2.clearable && !e2.loading), O2 = ga(() => {
    var e3;
    return (null != (e3 = s2.value) ? e3 : "").toString().length;
  }), N2 = ga(() => e2.noResize || e2.autoGrow), M2 = At(), A2 = At(), D2 = At(), V2 = At(), T2 = At(""), L2 = ga(() => ({ "cds-text-area--has-prefix": !!e2.prefix, "cds-text-area--has-suffix": !!e2.suffix, "cds-text-area--disabled-resize": Lt(N2), "cds-text-area--auto-grow": e2.autoGrow }));
  ho(() => e2.value, (e3) => {
    e3 && (s2.value = e3);
  }, { immediate: true }), ho(s2, F2), ho(() => e2.rows, F2), ho(() => e2.maxRows, F2), Zo(F2);
  const P2 = () => {
    var e3;
    Lt(A2) !== document.activeElement && (null == (e3 = Lt(A2)) || e3.focus()), Lt(v2) || m2();
  };
  function F2() {
    !e2.autoGrow || un(() => {
      var t3;
      if (!Lt(V2) || !Lt(A2))
        return;
      const n3 = getComputedStyle(Lt(V2)), o3 = null == (t3 = Lt(V2)) ? void 0 : t3.scrollHeight, r3 = parseFloat(n3.lineHeight), s3 = Math.max(parseFloat(e2.rows) * r3), a3 = parseFloat(e2.maxRows) * r3 || 1 / 0;
      T2.value = Bd(jd(null != o3 ? o3 : 0, s3, a3));
    });
  }
  let R2;
  return ho(V2, (e3) => {
    e3 ? (R2 = new ResizeObserver(F2), R2.observe(Lt(V2))) : null == R2 || R2.disconnect();
  }), Yo(() => {
    null == R2 || R2.disconnect();
  }), r2({ id: l2, isFocused: v2, fieldRef: M2, textareaRef: A2, validationRef: D2, sizerRef: V2 }), { id: l2, model: s2, fieldRef: M2, textareaRef: A2, validationRef: D2, sizerRef: V2, isFocused: v2, rootAttrs: S2, inputAttrs: E2, inputProps: k2, hasLabel: i2, hasDescription: c2, hasPrepend: d2, hasAppend: u2, hasPrependInnerIcon: y2, hasAppendInnerIcon: b2, hasLoading: f2, hasClear: I2, counterValue: O2, iconClasses: p2, textareaClasses: L2, fieldClasses: w2, loadingClasses: h2, textareaHeight: T2, themeClasses: a2, onBlur: g2, onFocus: P2, onInput: (e3) => {
    s2.value = e3.target.value;
  }, onClear: (e3) => {
    e3.stopPropagation(), P2(), un(() => {
      s2.value = null, o2("click:clear", e3);
    });
  }, handleClickPrependInner: C2, handleClickAppendInner: x2 };
} }), Nx = ["id", "value", "placeholder", "disabled", "readonly", "aria-disabled", "aria-invalid", "autofocus", "maxlength", "rows"], Mx = { key: 3, class: "cds-text-area__clear" };
const Ax = Tm(Ox, [["render", function(e2, t2, n2, o2, r2, s2) {
  const a2 = ro("cds-input-label"), l2 = ro("cds-input-description"), i2 = ro("cds-icon"), c2 = ro("cds-loader"), d2 = ro("cds-transition"), u2 = ro("cds-input"), p2 = ro("cds-input-counter");
  return Ss(), Ns("div", Us({ ref: "fieldRef", class: ["cds-text-area-field", e2.themeClasses] }, e2.rootAttrs), [e2.hasLabel ? (Ss(), Ms(a2, { key: 0, for: e2.id, text: e2.label }, { default: Wn(() => [nr(e2.$slots, "label")]), _: 3 }, 8, ["for", "text"])) : js("", true), e2.hasDescription ? (Ss(), Ms(l2, { key: 1, text: e2.description }, { default: Wn(() => [nr(e2.$slots, "description")]), _: 3 }, 8, ["text"])) : js("", true), Ps(u2, Us({ id: e2.id, ref: "validationRef", modelValue: e2.model, "onUpdate:modelValue": t2[9] || (t2[9] = (t3) => e2.model = t3), focused: e2.isFocused, class: ["cds-text-area", [e2.textareaClasses, e2.fieldClasses, e2.iconClasses, e2.loadingClasses]], role: "textbox", style: { "--cds-textarea-height": e2.textareaHeight } }, { ...e2.inputProps }), tr({ default: Wn(({ isDisabled: n3, isReadonly: o3, isValid: r3 }) => [e2.hasPrependInnerIcon ? (Ss(), Ns("div", { key: 0, class: "cds-text-area__prepend-inner", onClick: t2[0] || (t2[0] = (...t3) => e2.handleClickPrependInner && e2.handleClickPrependInner(...t3)) }, [nr(e2.$slots, "prepend-inner"), e2.prependInnerIcon ? (Ss(), Ms(i2, { key: 0, name: e2.prependInnerIcon }, null, 8, ["name"])) : js("", true)])) : js("", true), e2.prefix ? (Ss(), Ns("span", { key: 1, class: "cds-text-area__prefix", onClick: t2[1] || (t2[1] = (...t3) => e2.onFocus && e2.onFocus(...t3)) }, Q(e2.prefix), 1)) : js("", true), Ls("textarea", Us({ id: e2.id, ref: "textareaRef", value: e2.model, placeholder: e2.placeholder, disabled: n3.value, readonly: o3.value || e2.loading, "aria-disabled": o3.value || n3.value, "aria-invalid": e2.error || false === r3.value, autofocus: e2.autofocus, maxlength: e2.limit ? +e2.limit : void 0, rows: e2.rows }, { ...e2.inputAttrs }, { onInput: t2[2] || (t2[2] = (...t3) => e2.onInput && e2.onInput(...t3)), onFocus: t2[3] || (t2[3] = (...t3) => e2.onFocus && e2.onFocus(...t3)), onBlur: t2[4] || (t2[4] = (...t3) => e2.onBlur && e2.onBlur(...t3)) }), null, 16, Nx), e2.autoGrow ? wo((Ss(), Ns("textarea", { key: 2, ref: "sizerRef", "onUpdate:modelValue": t2[5] || (t2[5] = (t3) => e2.model = t3), class: "cds-text-area__sizer", readonly: "", "aria-hidden": "true" }, null, 512)), [[kl, e2.model]]) : js("", true), e2.hasClear ? (Ss(), Ns("div", Mx, [Ls("button", { type: "button", title: "\u041E\u0447\u0438\u0441\u0442\u0438\u0442\u044C \u0442\u0435\u043A\u0441\u0442\u043E\u0432\u0443\u044E \u043E\u0431\u043B\u0430\u0441\u0442\u044C", class: U(["cds-text-area__clear--button", { active: !!e2.model }]), onClick: t2[6] || (t2[6] = (...t3) => e2.onClear && e2.onClear(...t3)) }, [Ps(i2, { name: "remove" })], 2)])) : js("", true), e2.suffix ? (Ss(), Ns("span", { key: 4, class: "cds-text-area__suffix", onClick: t2[7] || (t2[7] = (...t3) => e2.onFocus && e2.onFocus(...t3)) }, Q(e2.suffix), 1)) : js("", true), e2.hasAppendInnerIcon ? (Ss(), Ns("div", { key: 5, class: "cds-text-area__append-inner", onClick: t2[8] || (t2[8] = (...t3) => e2.handleClickAppendInner && e2.handleClickAppendInner(...t3)) }, [nr(e2.$slots, "append-inner"), e2.appendInnerIcon ? (Ss(), Ms(i2, { key: 0, name: e2.appendInnerIcon }, null, 8, ["name"])) : js("", true)])) : js("", true), Ps(d2, { transition: "fade-transition", role: "alert", "aria-busy": "true" }, { default: Wn(() => [e2.hasLoading ? (Ss(), Ms(c2, { key: 0, size: "xs", class: "cds-text-area__loader" })) : js("", true)]), _: 1 })]), messages: Wn(() => [nr(e2.$slots, "messages")]), _: 2 }, [e2.hasPrepend ? { name: "prepend", fn: Wn(() => [nr(e2.$slots, "prepend")]), key: "0" } : void 0, e2.hasAppend ? { name: "append", fn: Wn(() => [nr(e2.$slots, "append")]), key: "1" } : void 0]), 1040, ["id", "modelValue", "focused", "class", "style"]), e2.counter || e2.limit ? (Ss(), Ms(p2, { key: 2, current: e2.counterValue, max: e2.limit, class: "cds-text-area__counter" }, null, 8, ["current", "max"])) : js("", true)], 16);
}]]);
function Dx(e2, t2, n2, o2) {
  const r2 = ga(() => function() {
    if ("string" != typeof e2.defaultDate) {
      const e3 = o2(true);
      return e3.dateHash = yv(e3), e3;
    }
    return Lu(e2.defaultDate, "YYYY/MM/DD");
  }()), s2 = vh(e2, "modelValue", "", (e3) => Lu(e3, n2.value, t2.value, r2.value)), a2 = At(null === s2.value.hour || s2.value.hour < 12), l2 = ga(() => e2.format12h ? e2.format12h : !Su.date.format24h), i2 = ga(() => {
    const e3 = s2.value;
    let t3;
    null === e3.hour ? t3 = "--" : void 0 !== e3.hour && (t3 = l2.value ? a2.value ? String(0 === e3.hour ? 12 : e3.hour) : String(e3.hour > 12 ? e3.hour - 12 : e3.hour) : Wd(e3.hour));
    return { hour: t3, minute: null === e3.minute ? "--" : Wd(e3.minute), second: null === e3.second ? "--" : Wd(e3.second) };
  }), c2 = ga(() => e2.allowedHours ? (t3) => {
    var n3;
    return null == (n3 = e2.allowedHours) ? void 0 : n3.includes(t3);
  } : e2.allowedTime ? (t3) => {
    var n3;
    return null == (n3 = e2.allowedTime) ? void 0 : n3.call(e2, t3, null, null);
  } : null), d2 = ga(() => e2.allowedMinutes ? (t3) => {
    var n3;
    return null == (n3 = e2.allowedMinutes) ? void 0 : n3.includes(t3);
  } : e2.allowedTime ? (t3) => {
    var n3;
    return null == (n3 = e2.allowedTime) ? void 0 : n3.call(e2, s2.value.hour, t3, null);
  } : null), u2 = ga(() => e2.allowedSeconds ? (t3) => {
    var n3;
    return null == (n3 = e2.allowedSeconds) ? void 0 : n3.includes(t3);
  } : e2.allowedTime ? (t3) => {
    var n3;
    return null == (n3 = e2.allowedTime) ? void 0 : n3.call(e2, s2.value.hour, s2.value.minute, t3);
  } : null), p2 = ga(() => {
    if (!c2.value)
      return null;
    const e3 = v2(0, 11, c2.value), t3 = v2(12, 11, c2.value);
    return { am: e3, pm: t3, values: e3.values.concat(t3.values) };
  }), f2 = ga(() => d2.value ? v2(0, 59, d2.value) : null), h2 = ga(() => u2.value ? v2(0, 59, u2.value) : null);
  function v2(e3, t3, n3) {
    const o3 = [...new Array(t3 + 1)].map((t4, o4) => {
      const r3 = o4 + e3;
      return { index: r3, value: true === n3(r3) };
    }).filter((e4) => e4.value).map((e4) => e4.index);
    return { min: o3[0], max: o3[o3.length - 1], values: o3, threshold: t3 + 1 };
  }
  return { model: s2, isAM: a2, stringModel: i2, hourInSelection: c2, minuteInSelection: d2, secondInSelection: u2, validHours: p2, validMinutes: f2, validSeconds: h2, getCurrentTime: function() {
    const e3 = new Date();
    return { hour: e3.getHours(), minute: e3.getMinutes(), second: e3.getSeconds(), millisecond: e3.getMilliseconds() };
  } };
}
function Vx(e2, t2) {
  if (t2 > Math.max(...e2))
    return Math.min(...e2);
  if (t2 < Math.min(...e2))
    return Math.max(...e2);
  {
    let n2 = e2[0];
    for (let o2 = 1; o2 < e2.length; o2++) {
      if (!(Math.abs(e2[o2] - t2) < Math.abs(n2 - t2)))
        return n2 === t2 || (n2 = n2 < t2 ? e2[o2] : t2 > e2[o2 - 1] ? e2[o2 - 1] : e2[0]), n2;
      n2 = e2[o2], n2 > t2 && e2[e2.length - 1] === n2 && (n2 = e2[o2 - 1]);
    }
    return n2;
  }
}
const Tx = Lo({ name: "CdsTimePicker", props: { ...tu({ withSeconds: { type: Boolean, default: false }, format12h: { type: Boolean, default: false }, nowBtn: { type: Boolean, default: false }, defaultDate: { type: String, validator: (e2) => /^-?[\d]+\/[0-1]\d\/[0-3]\d$/.test(e2) }, allowedTime: { type: Function, default: void 0 }, allowedHours: { type: Array, default: void 0 }, allowedMinutes: { type: Array, default: void 0 }, allowedSeconds: { type: Array, default: void 0 } }, "time")(), ...mv({ mask: "HH:mm" }), ...Qf(), ...th(), ...Ev(), ...Zv() }, emits: { "update:modelValue": (e2, t2) => true }, setup(e2, { emit: t2 }) {
  const { tabindex: n2, getLocale: o2, getMask: r2, getCurrentDate: s2 } = gv(e2), { themeClasses: a2 } = Yv(e2), l2 = At(o2()), i2 = ga(() => e2.withSeconds ? r2() + ":ss" : r2()), { model: c2, isAM: d2, stringModel: u2, hourInSelection: p2, minuteInSelection: f2, secondInSelection: h2, validHours: v2, validMinutes: m2, validSeconds: g2, getCurrentTime: y2 } = Dx(e2, l2, i2, s2), b2 = ga(() => e2.disabled ? { "aria-disabled": "true" } : e2.readonly ? { "aria-readonly": "true" } : {}), w2 = ga(() => ({ "cds-time-picker": true, "cds-time-picker--disabled": e2.disabled, "cds-time-picker--readonly": e2.readonly }));
  function _2(n3) {
    n3 = Object.assign({ ...Lt(c2) }, n3);
    const o3 = Tu(new Date(n3.year, n3.month ? n3.month - 1 : 0, n3.day, n3.hour, n3.minute, n3.second, n3.millisecond), Lt(i2), Lt(l2), n3.year, n3.timezoneOffset);
    n3.changed = o3 !== e2.modelValue, t2("update:modelValue", o3, n3);
  }
  function C2() {
    var t3, n3, o3;
    if (!p2.value || p2.value(c2.value.hour))
      if (!f2.value || f2.value(c2.value.minute))
        if (e2.withSeconds && Lt(h2) && !Lt(h2)(Lt(c2).second)) {
          E2(Vx(null == (o3 = Lt(g2)) ? void 0 : o3.values, Lt(c2).second));
        } else
          _2();
      else {
        S2(Vx(null == (n3 = m2.value) ? void 0 : n3.values, c2.value.minute));
      }
    else {
      k2(Vx(null == (t3 = v2.value) ? void 0 : t3.values, c2.value.hour));
    }
  }
  function x2() {
    const e3 = { ...s2(), ...y2() };
    _2(e3), Object.assign(Lt(c2), e3);
  }
  function k2(e3) {
    Lt(c2).hour !== e3 && (c2.value.hour = e3, C2());
  }
  function S2(e3) {
    Lt(c2).minute !== e3 && (c2.value.minute = e3, C2());
  }
  function E2(e3) {
    Lt(c2).second !== e3 && (c2.value.second = e3, C2());
  }
  function I2() {
    Lt(d2) || (d2.value = true, null !== Lt(c2).hour && (c2.value.hour -= 12, C2()));
  }
  function O2() {
    Lt(d2) && (d2.value = false, null !== Lt(c2).hour && (c2.value.hour += 12, C2()));
  }
  return ho(() => e2.modelValue, () => {
    e2.format12h && (d2.value = Lt(c2).hour < 12);
  }), ho([i2, l2], () => {
    un(() => {
      _2();
    });
  }), Oh(() => ba("div", { class: Cp(Lt(w2), Lt(a2)), ...Lt(b2) }, [ba("div", { class: "cds-time-picker__clock" }, [ba(my, { appearance: "transparent", icon: "chevron-up", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: () => k2(Lt(c2).hour + 1) }), ba("input", { type: "number", class: "cds-time-picker__input", value: Lt(u2).hour, placeholder: "--", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onInput: (e3) => k2(+e3.target.value) }), ba(my, { appearance: "transparent", icon: "chevron-down", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: () => k2(Lt(c2).hour - 1) })]), ba("div", { class: "cds-time-picker__separator" }, ":"), ba("div", { class: "cds-time-picker__clock" }, [ba(my, { appearance: "transparent", icon: "chevron-up", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: () => S2(Lt(c2).minute + 1) }), ba("input", { type: "number", class: "cds-time-picker__input", value: Lt(u2).minute, placeholder: "--", readonly: e2.readonly, disabled: e2.disabled, onInput: (e3) => S2(+e3.target.value) }), ba(my, { appearance: "transparent", icon: "chevron-down", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: () => S2(Lt(c2).minute - 1) })]), e2.withSeconds && ba("div", { class: "cds-time-picker__separator" }, ":"), e2.withSeconds && ba("div", { class: "cds-time-picker__clock" }, [ba(my, { appearance: "transparent", icon: "chevron-up", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: () => E2(Lt(c2).second + 1) }), ba("input", { type: "number", class: "cds-time-picker__input", value: Lt(u2).second, placeholder: "--", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onInput: (e3) => E2(+e3.target.value) }), ba(my, { appearance: "transparent", icon: "chevron-down", readonly: e2.readonly, disabled: e2.disabled, onClick: () => E2(Lt(c2).second - 1) })]), e2.format12h && ba("div", { class: "cds-time-picker__ampm" }, [ba(my, { appearance: "transparent", text: "AM", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: I2 }), ba(my, { appearance: "transparent", text: "PM", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: O2 })]), e2.nowBtn && ba("div", { class: "cds-time-picker__now" }, [ba(my, { appearance: "transparent", icon: "clock", readonly: e2.readonly, disabled: e2.disabled, tabindex: Lt(n2), onClick: x2 })])])), { setNow: x2, setHour: k2, setMinute: S2, setSecond: E2, setAm: I2, setPm: O2 };
} }), Lx = "CdsToggle", Px = Lo({ name: Lx, components: { CdsTransitionSlideX: Um }, props: { ...Nh(), ...Fh(), ...mh(), ...Qf(), ...th(), ...Zv() }, emits: { "update:focused": (e2) => true, "update:modelValue": (e2) => true }, setup(e2, { slots: t2, attrs: n2, expose: o2 }) {
  const r2 = vh(e2, "modelValue"), { themeClasses: s2 } = Yv(e2), { id: a2, hasDescription: l2 } = Mh(e2, t2, "toggle"), { isFocused: i2, focus: c2, blur: d2 } = gh(e2, Lx), { inputProps: u2 } = Ah(e2, ["focused"]), { rootAttrs: p2, inputAttrs: f2 } = Dh(n2), h2 = At(), v2 = At(), m2 = ga(() => Lt(r2) ? "true" : "false"), g2 = ga(() => ({ "cds-toggle--disabled": e2.disabled, [Sd.checked]: Lt(r2) || !!n2.checked }));
  function y2(t3) {
    e2.readonly || e2.disabled || (r2.value = t3.target.checked);
  }
  function b2() {
    var t3;
    e2.readonly || e2.disabled || null == (t3 = Lt(h2)) || t3.click();
  }
  return Oh(() => {
    const n3 = !!t2.prepend, o3 = t2.label || t2.append, w2 = !!e2.label || !!o3;
    return ba(Gm, { id: Lt(a2), ref: v2, tabindex: e2.disabled ? -1 : 0, focused: Lt(i2), class: Cp("cds-toggle", Lt(g2), Lt(s2)), ...Lt(u2), ...Lt(p2) }, { default: (s3) => ba("span", { class: "cds-toggle__wrapper" }, [Lt(l2) && ba(Qm, { for: Lt(a2) }, () => [t2.description ? t2.description() : e2.description]), ba("span", { class: "cds-toggle__control" }, [n3 && ba(Jm, { for: Lt(a2), class: Cp("cds-toggle__label", "cds-toggle__prepend") }, () => {
      var e3;
      return null == (e3 = t2.prepend) ? void 0 : e3.call(t2);
    }), ba("input", { id: Lt(a2), type: "checkbox", class: "cds-toggle__input", value: e2.value, ref: h2, checked: Lt(r2), disabled: e2.disabled || e2.readonly || Lt(s3.isDisabled), tabindex: e2.disabled ? -1 : 0, "aria-checked": Lt(m2), "aria-invalid": e2.error || false === Lt(s3.isValid), "aria-disabled": e2.disabled || e2.readonly || Lt(s3.isDisabled), ...Lt(f2), onBlur: d2, onFocus: c2, onInput: y2 }), ba("span", { class: Cp("cds-toggle__switch"), onClick: b2 }), w2 && ba(Jm, { for: Lt(a2), class: "cds-toggle__label" }, () => o3 ? o3() : e2.label)])]), messages: () => {
      var e3;
      return null == (e3 = t2.messages) ? void 0 : e3.call(t2);
    } });
  }), o2({ id: a2, isFocused: i2, validationRef: v2, inputRef: h2 }), {};
} }), Fx = "CdsToolbar", Rx = Lo({ name: Fx, props: { inset: { type: Boolean, default: false }, bordered: { type: Boolean, default: false }, noGutter: { type: String, default: void 0 }, noGutters: { type: Boolean, default: false }, ...Xv(), ...nh(), ...Zv() }, setup(e2, { slots: t2 }) {
  const { themeClasses: n2 } = Yv(e2), { invertColorClasses: o2 } = oh(e2, Fx), { transparentClasses: r2 } = Jv(e2, Fx), s2 = ga(() => Cp("cds-toolbar", { "cds-toolbar--inset": e2.inset, "cds-toolbar--bordered": e2.bordered, "cds-toolbar--no-gutters": e2.noGutters, [`cds-toolbar--no-gutter-${e2.noGutter}`]: e2.noGutter }));
  return () => ba("div", { class: Cp(Lt(s2), Lt(o2), Lt(r2), Lt(n2)), role: "toolbar" }, Ep(t2.default));
} }), $x = Lo({ name: "CdsToolbarTitle", props: { shrink: { type: Boolean, default: false } }, setup(e2, { slots: t2 }) {
  const n2 = ga(() => Cp("cds-toolbar__title", { "cds-toolbar__title--shrink": e2.shrink }));
  return () => ba("div", { class: Lt(n2) }, Ep(t2.default));
} });
function Bx(e2) {
  const { dropPosition: t2, dropLevelOffset: n2, indent: o2 } = e2, r2 = "left", s2 = "right", a2 = { [r2]: -n2 * o2 + 4 + "px", [s2]: 0 };
  switch (t2) {
    case -1:
      a2.top = "-3px";
      break;
    case 1:
      a2.bottom = "-3px";
      break;
    default:
      a2.bottom = "-3px", a2[r2] = `${o2 + 4}px`;
  }
  return ba("div", { style: a2, class: Cp("cds-tree__drop-indicator") });
}
const Hx = ({ prefixCls: e2, level: t2, isStart: n2, isEnd: o2 }) => {
  const r2 = `${e2}__indent-unit`, s2 = [];
  for (let e3 = 0; e3 < t2; e3 += 1)
    s2.push(ba("span", { key: e3, class: { [r2]: true, [`${r2}--start`]: n2[e3], [`${r2}--end`]: o2[e3] } }));
  return ba("span", { "aria-hidden": true, class: `${e2}__indent` }, s2);
}, jx = Symbol("cds:tree-context"), Kx = Lo({ name: "TreeContext", props: { value: { type: Object } }, setup: (e2, { slots: t2 }) => (Ir(jx, ga(() => e2.value)), () => {
  var e3;
  return null == (e3 = t2.default) ? void 0 : e3.call(t2);
}) }), zx = () => Or(jx, ga(() => ({}))), Zx = Symbol("cds:keys-state"), Ux = () => Or(Zx, { expandedKeys: Dt([]), selectedKeys: Dt([]), loadedKeys: Dt([]), loadingKeys: Dt([]), checkedKeys: Dt([]), halfCheckedKeys: Dt([]), expandedKeysSet: ga(() => /* @__PURE__ */ new Set()), selectedKeysSet: ga(() => /* @__PURE__ */ new Set()), loadedKeysSet: ga(() => /* @__PURE__ */ new Set()), loadingKeysSet: ga(() => /* @__PURE__ */ new Set()), checkedKeysSet: ga(() => /* @__PURE__ */ new Set()), halfCheckedKeysSet: ga(() => /* @__PURE__ */ new Set()), flattenNodes: Dt([]) }), Wx = "open", Yx = "close", Gx = "cds-treenode", qx = Lo({ componentName: "CdsTreeNode", inheritAttrs: false, isTreeNode: 1, props: { ...V_() }, setup(e2, { attrs: t2, slots: n2, expose: o2 }) {
  const r2 = At(false), s2 = zx(), { expandedKeysSet: a2, selectedKeysSet: l2, loadedKeysSet: i2, loadingKeysSet: c2, checkedKeysSet: d2, halfCheckedKeysSet: u2 } = Ux(), { dragOverNodeKey: p2, dropPosition: f2, keyEntities: h2 } = Lt(s2), v2 = ga(() => $_(e2.eventKey, { expandedKeysSet: Lt(a2), selectedKeysSet: Lt(l2), loadedKeysSet: Lt(i2), loadingKeysSet: Lt(c2), checkedKeysSet: Lt(d2), halfCheckedKeysSet: Lt(u2), dragOverNodeKey: p2, dropPosition: f2, keyEntities: h2 })), m2 = iu(() => Lt(v2).expanded), g2 = iu(() => Lt(v2).selected), y2 = iu(() => Lt(v2).checked), b2 = iu(() => Lt(v2).loaded), w2 = iu(() => Lt(v2).loading), _2 = iu(() => Lt(v2).halfChecked), C2 = iu(() => Lt(v2).dragOver), x2 = iu(() => Lt(v2).dragOverGapTop), k2 = iu(() => Lt(v2).dragOverGapBottom), S2 = iu(() => Lt(v2).pos), E2 = At(), I2 = ga(() => {
    const { eventKey: t3 } = e2, { keyEntities: n3 } = Lt(s2), { children: o3 } = n3[t3] || {};
    return !!(o3 || []).length;
  }), O2 = ga(() => {
    const { isLeaf: t3 } = e2, { loadData: n3 } = Lt(s2), o3 = Lt(I2);
    return false !== t3 && (t3 || !n3 && !o3 || n3 && b2.value && !o3);
  }), N2 = ga(() => Lt(O2) ? null : Lt(m2) ? Wx : Yx), M2 = ga(() => {
    const { disabled: t3 } = e2, { disabled: n3 } = Lt(s2);
    return !(!n3 && !t3);
  }), A2 = ga(() => {
    const { checkable: t3 } = e2, { checkable: n3 } = Lt(s2);
    return !(!n3 || false === t3) && n3;
  }), D2 = ga(() => {
    const { selectable: t3 } = e2, { selectable: n3 } = Lt(s2);
    return "boolean" == typeof t3 ? t3 : n3;
  }), V2 = ga(() => {
    const { data: t3, active: n3, checkable: o3, disableCheckbox: r3, disabled: s3, selectable: a3 } = e2;
    return { active: n3, checkable: o3, disableCheckbox: r3, disabled: s3, selectable: a3, ...t3, dataRef: t3, data: t3, isLeaf: Lt(O2), checked: Lt(y2), expanded: Lt(m2), loading: Lt(w2), selected: Lt(g2), halfChecked: Lt(_2) };
  }), T2 = nu("CdsTreeNode"), L2 = ga(() => {
    const { eventKey: t3 } = e2, { keyEntities: n3 } = Lt(s2), { parent: o3 } = n3[t3] || {};
    return { ...G_(Object.assign({}, e2, Lt(v2))), parent: o3 };
  }), P2 = vt({ eventData: L2, eventKey: ga(() => e2.eventKey), selectHandle: E2, pos: S2, key: T2.vnode.key });
  o2(P2);
  const F2 = (e3) => {
    const { onNodeDoubleClick: t3 } = Lt(s2);
    t3(e3, Lt(L2));
  }, R2 = (t3) => {
    if (Lt(M2))
      return;
    const { disableCheckbox: n3 } = e2, { onNodeCheck: o3 } = Lt(s2);
    if (!Lt(A2) || n3)
      return;
    const r3 = !Lt(y2);
    o3(t3, Lt(L2), r3);
  }, $2 = (e3) => {
    const { onNodeClick: t3 } = Lt(s2);
    t3(e3, Lt(L2)), Lt(D2) ? ((e4) => {
      if (Lt(M2))
        return;
      const { onNodeSelect: t4 } = Lt(s2);
      e4.preventDefault(), t4(e4, Lt(L2));
    })(e3) : R2(e3);
  }, B2 = (e3) => {
    const { onNodeMouseEnter: t3 } = Lt(s2);
    t3(e3, Lt(L2));
  }, H2 = (e3) => {
    const { onNodeMouseLeave: t3 } = Lt(s2);
    t3(e3, Lt(L2));
  }, j2 = (e3) => {
    const { onNodeContextMenu: t3 } = Lt(s2);
    t3(e3, Lt(L2));
  }, K2 = (e3) => {
    const { onNodeDragStart: t3 } = Lt(s2);
    e3.stopPropagation(), r2.value = true, t3(e3, P2);
    try {
      e3.dataTransfer.setData("text/plain", "");
    } catch {
    }
  }, z2 = (e3) => {
    const { onNodeDragEnter: t3 } = Lt(s2);
    e3.preventDefault(), e3.stopPropagation(), t3(e3, P2);
  }, Z2 = (e3) => {
    const { onNodeDragOver: t3 } = Lt(s2);
    e3.preventDefault(), e3.stopPropagation(), t3(e3, P2);
  }, U2 = (e3) => {
    const { onNodeDragLeave: t3 } = Lt(s2);
    e3.stopPropagation(), t3(e3, P2);
  }, W2 = (e3) => {
    const { onNodeDragEnd: t3 } = Lt(s2);
    e3.stopPropagation(), r2.value = false, t3(e3, P2);
  }, Y2 = (e3) => {
    const { onNodeDrop: t3 } = Lt(s2);
    e3.preventDefault(), e3.stopPropagation(), r2.value = false, t3(e3, P2);
  }, G2 = (e3) => {
    const { onNodeExpand: t3 } = Lt(s2);
    Lt(w2) || t3(e3, Lt(L2));
  }, q2 = () => {
    const { draggable: e3 } = Lt(s2);
    return e3 && (null == e3 ? void 0 : e3.icon) ? ba(Lm, { name: "string" == typeof e3.icon ? e3.icon : "draggable", class: "cds-treenode__draggable-icon" }) : null;
  }, X2 = () => {
    const t3 = (() => {
      var t4, o4, r3;
      const { switcherIcon: a3 = n2.switcherIcon || (null == (r3 = Lt(s2).slots) ? void 0 : r3[null == (o4 = null == (t4 = e2.data) ? void 0 : t4.slots) ? void 0 : o4.switcherIcon]) } = e2, { switcherIcon: l3 } = Lt(s2), i3 = a3 || l3;
      return "function" == typeof i3 ? i3(Lt(V2)) : i3;
    })();
    if (Lt(O2))
      return false !== t3 ? ba("span", { class: Cp(`${Gx}__switcher`, `${Gx}__switcher--noop`) }, t3) : null;
    const o3 = Cp(`${Gx}__switcher`, `${Gx}__switcher--${Lt(m2) ? Wx : Yx}`);
    return false !== t3 ? ba("span", { class: o3, onClick: G2 }, t3) : null;
  }, J2 = () => {
    const { disabled: t3, eventKey: n3 } = e2, { draggable: o3, dropLevelOffset: r3, dropPosition: a3, indent: l3, dropIndicatorRender: i3, dragOverNodeKey: c3 } = Lt(s2);
    return t3 || false === o3 || c3 !== n3 ? null : i3({ dropPosition: a3, dropLevelOffset: r3, indent: l3, prefixCls: Gx });
  }, Q2 = () => {
    var t3, o3, a3, l3, i3, c3;
    const { icon: d3 = n2.icon, data: u3 } = e2, p3 = n2.title || (null == (a3 = Lt(s2).slots) ? void 0 : a3[null == (o3 = null == (t3 = e2.data) ? void 0 : t3.slots) ? void 0 : o3.title]) || (null == (l3 = Lt(s2).slots) ? void 0 : l3.title) || e2.title, { showIcon: f3, icon: h3, loadData: v3 } = Lt(s2), m3 = `${Gx}__node-content-wrapper`;
    let y3, b3;
    if (f3) {
      const e3 = d3 || (null == (c3 = Lt(s2).slots) ? void 0 : c3[null == (i3 = null == u3 ? void 0 : u3.slots) ? void 0 : i3.icon]) || h3;
      y3 = e3 ? ba("span", { className: Cp(`${Gx}__icon`, `${Gx}__icon-customize`) }, "function" == typeof e3 ? e3(Lt(V2)) : e3) : ba("span", { class: Cp(`${Gx}__icon`, `${Gx}__icon--${Lt(N2) || "docu"}`, Lt(w2) && `${Gx}__icon--loading`) });
    }
    b3 = "function" == typeof p3 ? p3(Lt(V2)) : p3, b3 = b3 || "---";
    const _3 = ba("span", { class: `${Gx}__title` }, b3);
    return ba("span", { ref: E2, title: "string" == typeof p3 ? p3 : "", class: Cp(m3, `${m3}--${Lt(N2) || "normal"}`, Lt(M2) && (Lt(g2) || Lt(r2)) && `${Gx}--node-selected`), onMouseenter: B2, onMouseleave: H2, onContextmenu: j2, onDblclick: F2, onClick: $2 }, [y3, _3, J2()]);
  }, ee2 = () => {
    const { loadData: e3, onNodeLoad: t3 } = Lt(s2);
    Lt(w2) || e3 && m2.value && !O2.value && !I2.value && !b2.value && t3(L2.value);
  };
  Zo(() => {
    ee2();
  }), Wo(() => {
    ee2();
  }), Oh(() => {
    const { eventKey: n3, isLeaf: o3, isStart: r3, isEnd: a3, domRef: l3, active: i3, data: c3, onMousemove: d3, selectable: u3, ...p3 } = { ...e2, ...t2 }, { filterTreeNode: f3, keyEntities: h3, dropContainerKey: v3, dropTargetKey: b3, draggingNodeKey: S3 } = Lt(s2), E3 = du(p3, { aria: true, data: true }), { level: I3 } = h3[n3] || {}, O3 = a3[a3.length - 1], N3 = (() => {
      const { data: t3 } = e2, { draggable: n4 } = Lt(s2);
      return n4 && (!n4.nodeDraggable || n4.nodeDraggable(t3));
    })(), D3 = !Lt(M2) && N3, V3 = S3 === n3, T3 = void 0 !== u3 ? { "aria-selected": !!u3 } : void 0, P3 = "cds-treenode";
    return ba("div", { ref: l3, class: Cp(t2.class, "cds-treenode", { [`${P3}__switcher--${Lt(m2) ? "open" : "close"}`]: !o3, [`${P3}__checkbox--checked`]: Lt(y2), [`${P3}__checkbox--indeterminate`]: Lt(_2), [`${P3}--disabled`]: Lt(M2), [`${P3}--selected`]: Lt(g2), [`${P3}--loading`]: Lt(w2), [`${P3}--active`]: i3, [`${P3}--leaf-last`]: O3, [`${P3}--draggable`]: D3, dragging: V3, "drop-target": b3 === n3, "drop-container": v3 === n3, "drag-over": !Lt(M2) && Lt(C2), "drag-over-gap-top": !Lt(M2) && Lt(x2), "drag-over-gap-bottom": !Lt(M2) && Lt(k2), "filter-node": f3 && f3(Lt(L2)) }), style: t2.style, draggable: D3, "aria-grabbed": !!V3 || void 0, onDragstart: D3 ? K2 : void 0, onDragenter: N3 ? z2 : void 0, onDragover: N3 ? Z2 : void 0, onDragleave: N3 ? U2 : void 0, onDragend: N3 ? W2 : void 0, onDrop: N3 ? Y2 : void 0, onMousemove: d3, ...T3, ...E3 }, [Hx({ prefixCls: P3, level: I3, isStart: r3, isEnd: a3 }), q2(), X2(), Lt(A2) ? ba("span", { class: Cp("cds-treenode__checkbox") }, ba(dg, { modelValue: Lt(y2), indeterminate: !Lt(y2) && Lt(_2), disabled: Lt(M2) || e2.disableCheckbox, hideMessages: true, "onUpdate:modelValue": R2 })) : null, Q2()]);
  });
} }), Xx = Lo({ name: "MotionTreeNode", inheritAttrs: false, props: { ...V_(), active: Boolean, motion: Object, motionNodes: { type: Array }, onMotionStart: Function, onMotionEnd: Function, motionType: String }, setup(e2, { attrs: t2, slots: n2 }) {
  const o2 = At(true), r2 = At(false), s2 = ga(() => e2.motion ? e2.motion : ((e3 = "cds-motion-collapse", t3 = true) => ({ name: e3, appear: t3, css: true, onBeforeEnter: (t4) => {
    t4.style.height = "0px", t4.style.opacity = "0", kp(t4, e3);
  }, onEnter: (e4) => {
    un(() => {
      e4.style.height = `${e4.scrollHeight}px`, e4.style.opacity = "1";
    });
  }, onAfterEnter: (t4) => {
    t4 && (Sp(t4, e3), t4.style.height = null, t4.style.opacity = null);
  }, onBeforeLeave: (t4) => {
    kp(t4, e3), t4.style.height = `${t4.offsetHeight}px`, t4.style.opacity = null;
  }, onLeave: (e4) => {
    setTimeout(() => {
      e4.style.height = "0px", e4.style.opacity = "0";
    });
  }, onAfterLeave: (t4) => {
    t4 && (Sp(t4, e3), t4.style && (t4.style.height = null, t4.style.opacity = null));
  } }))()), a2 = (t3, n3) => {
    var o3, a3, l2, i2;
    "appear" === n3 ? null == (a3 = null == (o3 = Lt(s2)) ? void 0 : o3.onAfterEnter) || a3.call(o3, t3) : "leave" === n3 && (null == (i2 = null == (l2 = Lt(s2)) ? void 0 : l2.onAfterLeave) || i2.call(l2, t3)), Lt(r2) || e2.onMotionEnd && e2.onMotionEnd(), r2.value = true;
  };
  ho(() => e2.motionNodes, () => {
    e2.motionNodes && "hide" === e2.motionType && o2.value && un(() => {
      o2.value = false;
    });
  }, { immediate: true, flush: "post" }), Zo(() => {
    e2.motionNodes && e2.onMotionStart && e2.onMotionStart();
  }), Yo(() => {
    e2.motionNodes && a2();
  }), Oh(() => {
    const { motion: r3, motionNodes: l2, motionType: i2, active: c2, eventKey: d2, ...u2 } = e2;
    return l2 ? ba(Oa, { ...Lt(s2), appear: "show" === i2, onAfterAppear: (e3) => a2(e3, "appear"), onAfterLeave: (e3) => a2(e3, "leave") }, { default: () => wo(ba("div", { class: "cds-treenode__motion" }, l2.map((e3) => {
      const { data: { ...t3 }, title: o3, key: r4, isStart: s3, isEnd: a3 } = e3;
      return delete t3.children, ba(qx, { ...t3, title: o3, active: c2, data: e3.data, key: r4, eventKey: r4, isStart: s3, isEnd: a3 }, n2);
    })), [[Ua, Lt(o2)]]) }) : ba(qx, { class: t2.class, style: t2.style, ...u2, active: c2, eventKey: d2 }, n2);
  });
} }), Jx = ({ setRef: e2 }, { slots: t2 }) => {
  var n2;
  const o2 = pu(null == (n2 = t2.default) ? void 0 : n2.call(t2));
  return o2 && o2.length ? $s(o2[0], { ref: e2 }) : o2;
};
Jx.props = { setRef: { type: Function, default: () => {
} } };
const Qx = ({ ref: e2, height: t2, offset: n2, prefixCls: o2, onInnerResize: r2 }, { slots: s2 }) => {
  let a2 = {}, l2 = { display: "flex", flexDirection: "column" };
  return void 0 !== n2 && (a2 = { height: `${t2}px`, position: "relative", overflow: "hidden" }, l2 = { ...l2, transform: `translateY(${n2}px)`, position: "absolute", left: 0, right: 0, top: 0 }), ba("div", { ref: e2, style: a2 }, ba(ax, { onResize: ({ offsetHeight: e3 }) => {
    e3 && r2 && r2();
  } }, { default: () => {
    var e3;
    return ba("div", { style: l2, class: Cp({ [`${o2}__holder-inner`]: o2 }) }, null == (e3 = s2.default) ? void 0 : e3.call(s2));
  } }));
};
Qx.displayName = "Filler", Qx.inheritAttrs = false, Qx.props = { prefixCls: String, height: Number, offset: Number, onInnerResize: Function };
function ek(e2) {
  return "touches" in e2 ? e2.touches[0].pageY : e2.pageY;
}
const tk = Lo({ name: "ScrollBar", inheritAttrs: false, props: { prefixCls: String, scrollTop: Number, scrollHeight: Number, height: Number, count: Number, onScroll: { type: Function }, onStartMove: { type: Function }, onStopMove: { type: Function } }, setup(e2) {
  const t2 = At(), n2 = At(), o2 = vt({ dragging: false, pageY: null, startTop: null, visible: false });
  let r2, s2 = null;
  const a2 = () => {
    clearTimeout(s2), o2.visible = true, s2 = setTimeout(() => {
      o2.visible = false;
    }, 2e3);
  }, l2 = (e3) => {
    e3.preventDefault();
  }, i2 = (e3) => {
    e3.stopPropagation(), e3.preventDefault();
  }, c2 = (t3) => {
    var r3;
    Object.assign(o2, { dragging: true, pageY: ek(t3), startTop: f2() }), null == (r3 = e2.onStartMove) || r3.call(e2), window.addEventListener("mousemove", d2), window.addEventListener("mouseup", u2), Lt(n2).addEventListener("touchmove", d2, !!vu && { passive: false }), Lt(n2).addEventListener("touchend", u2), t3.stopPropagation(), t3.preventDefault();
  }, d2 = (t3) => {
    const { dragging: n3, pageY: s3, startTop: a3 } = o2, { onScroll: l3 } = e2;
    if (yp.cancel(r2), n3) {
      const e3 = a3 + (ek(t3) - s3), n4 = v2(), o3 = m2(), i3 = o3 ? e3 / o3 : 0, c3 = Math.ceil(i3 * n4);
      r2 = yp(() => {
        l3(c3);
      });
    }
  }, u2 = () => {
    const { onStopMove: t3 } = e2;
    o2.dragging = false, t3(), p2();
  };
  function p2() {
    var e3, o3, s3, a3;
    window.removeEventListener("mousemove", d2), window.removeEventListener("mouseup", u2), null == (e3 = Lt(t2)) || e3.removeEventListener("touchstart", l2, !!vu && { passive: false }), Lt(n2) && (null == (o3 = Lt(n2)) || o3.removeEventListener("touchstart", c2, !!vu && { passive: false }), null == (s3 = Lt(n2)) || s3.removeEventListener("touchmove", d2, !!vu && { passive: false }), null == (a3 = Lt(n2)) || a3.removeEventListener("touchend", u2)), yp.cancel(r2);
  }
  function f2() {
    const { scrollTop: t3 } = e2, n3 = v2(), o3 = m2();
    return 0 === t3 || 0 === n3 ? 0 : t3 / n3 * o3;
  }
  function h2() {
    const { height: t3, scrollHeight: n3 } = e2;
    let o3 = t3 / n3 * 100;
    return o3 = Math.max(o3, 20), o3 = Math.min(o3, t3 / 2), Math.floor(o3);
  }
  function v2() {
    const { scrollHeight: t3, height: n3 } = e2;
    return t3 - n3 || 0;
  }
  function m2() {
    const { height: t3 } = e2;
    return t3 - h2() || 0;
  }
  Zo(() => {
    var e3, o3;
    null == (e3 = Lt(t2)) || e3.addEventListener("touchstart", l2, !!vu && { passive: false }), null == (o3 = Lt(n2)) || o3.addEventListener("touchstart", c2, !!vu && { passive: false });
  }), Yo(() => {
    p2(), clearTimeout(s2);
  }), ho(() => e2.scrollTop, () => {
    a2();
  }, { flush: "post" }), Oh(() => {
    const { dragging: r3, visible: s3 } = o2, { prefixCls: l3 } = e2, d3 = h2() + "px", u3 = f2() + "px", p3 = function() {
      const { height: t3, scrollHeight: n3 } = e2;
      return n3 > t3;
    }(), v3 = p3 && s3;
    return ba("div", { ref: t2, class: Cp(`${l3}__scrollbar`, { [`${l3}__scrollbar--show`]: p3 }), style: { width: "8px", top: 0, bottom: 0, right: 0, position: "absolute", display: v3 ? void 0 : "none" }, onMousedown: i2, onMousemove: a2 }, ba("div", { ref: n2, class: Cp(`${l3}__scrollbar-thumb`, { [`${l3}__scrollbar-thumb--dragging`]: r3 }), style: { width: "100%", height: d3, top: u3, left: 0, position: "absolute", background: "rgba(0, 0, 0, 0.5)", borderRadius: "99px", cursor: "pointer", userSelect: "none" }, onMousedown: c2 }));
  });
} });
function nk(e2, t2) {
  let n2 = false, o2 = null;
  return (r2, s2 = false) => {
    const a2 = r2 < 0 && Lt(e2) || r2 > 0 && Lt(t2);
    return s2 && a2 ? (clearTimeout(o2), n2 = false) : (!a2 || n2) && (clearTimeout(o2), n2 = true, o2 = setTimeout(() => {
      n2 = false;
    }, 50)), !n2 && a2;
  };
}
const ok = "object" == typeof navigator && /Firefox/i.test(navigator.userAgent);
const rk = 14 / 15;
const sk = [], ak = { overflowY: "auto", overflowAnchor: "none" }, lk = Lo({ name: "VirtualList", inheritAttrs: false, props: { prefixCls: String, data: Array, height: { type: Number, default: 0 }, itemHeight: { type: Number, default: 0 }, fullHeight: { type: Boolean, default: void 0 }, itemKey: { type: [String, Number, Function], required: true }, component: { type: [String, Object] }, virtual: { type: Boolean, default: void 0 }, children: Function, onScroll: Function, onMousedown: Function, onMouseEnter: Function, onVisibleChange: Function }, setup(e2, { slots: t2, attrs: n2, expose: o2 }) {
  const r2 = ga(() => !(false === e2.virtual || !e2.height || !e2.itemHeight)), s2 = ga(() => Lt(r2) && e2.data && e2.itemHeight * e2.data.length > e2.height), a2 = vt({ scrollTop: 0, scrollMoving: false }), l2 = ga(() => e2.data || sk), i2 = Dt([]);
  ho(l2, () => {
    i2.value = xt(Lt(l2)).slice();
  }, { immediate: true });
  const c2 = Dt((e3) => {
  });
  ho(() => e2.itemKey, (e3) => {
    c2.value = "function" == typeof e3 ? e3 : (t3) => null == t3 ? void 0 : t3[e3];
  }, { immediate: true });
  const d2 = Dt(), u2 = Dt(), p2 = Dt(), f2 = (e3) => Lt(c2)(e3), h2 = { getKey: f2 }, { setInstance: v2, collectHeight: m2, heights: g2, updatedMark: y2 } = function(e3, t3, n3, o3) {
    const r3 = /* @__PURE__ */ new Map(), s3 = /* @__PURE__ */ new Map(), a3 = At(Symbol("update"));
    let l3;
    function i3() {
      yp.cancel(l3);
    }
    function c3() {
      i3(), l3 = yp(() => {
        r3.forEach((e4, t4) => {
          if (e4 && e4.offsetParent) {
            const { offsetHeight: n4 } = e4;
            s3.get(t4) !== n4 && (a3.value = Symbol("update"), s3.set(t4, e4.offsetHeight));
          }
        });
      });
    }
    return ho(e3, () => {
      a3.value = Symbol("update");
    }), Go(() => {
      i3();
    }), { setInstance: function(e4, s4) {
      const a4 = t3(e4), l4 = r3.get(a4);
      s4 ? (r3.set(a4, s4.$el || s4), c3()) : r3.delete(a4), !l4 != !s4 && (s4 ? null == n3 || n3(e4) : null == o3 || o3(e4));
    }, collectHeight: c3, heights: s3, updatedMark: a3 };
  }(i2, f2, null, null), b2 = vt({ scrollHeight: void 0, start: 0, end: 0, offset: void 0 }), w2 = Dt(0);
  Zo(() => {
    un(() => {
      var e3;
      w2.value = (null == (e3 = Lt(u2)) ? void 0 : e3.offsetHeight) || 0;
    });
  }), Wo(() => {
    un(() => {
      var e3;
      w2.value = (null == (e3 = Lt(u2)) ? void 0 : e3.offsetHeight) || 0;
    });
  }), ho([r2, i2], () => {
    Lt(r2) || Object.assign(b2, { scrollHeight: void 0, start: 0, end: Lt(i2).length - 1, offset: void 0 });
  }, { immediate: true }), ho([r2, i2, w2, s2], () => {
    Lt(r2) && !Lt(s2) && Object.assign(b2, { scrollHeight: Lt(w2), start: 0, end: Lt(i2).length - 1, offset: void 0 }), Lt(d2) && (a2.scrollTop = Lt(d2).scrollTop);
  }, { immediate: true }), ho([s2, r2, () => a2.scrollTop, i2, y2, () => e2.height, w2], () => {
    if (!Lt(r2) || !Lt(s2))
      return;
    let t3, n3, o3, l3 = 0;
    const c3 = Lt(i2).length, d3 = Lt(i2), u3 = a2.scrollTop, { height: p3, itemHeight: h3 } = e2, v3 = u3 + p3;
    for (let e3 = 0; e3 < c3; e3++) {
      const r3 = d3[e3], s3 = f2(r3);
      let a3 = g2.get(s3);
      void 0 === a3 && (a3 = h3);
      const i3 = l3 + a3;
      void 0 === t3 && i3 >= u3 && (t3 = e3, n3 = l3), void 0 === o3 && i3 > v3 && (o3 = e3), l3 = i3;
    }
    void 0 === t3 && (t3 = 0, n3 = 0, o3 = Math.ceil(p3 / h3)), void 0 === o3 && (o3 = c3 - 1), o3 = Math.min(o3 + 1, c3), Object.assign(b2, { scrollHeight: l3, start: t3, end: o3, offset: n3 });
  }, { immediate: true });
  const _2 = ga(() => b2.scrollHeight - e2.height);
  const C2 = ga(() => a2.scrollTop <= 0), x2 = ga(() => a2.scrollTop >= Lt(_2)), k2 = nk(C2, x2);
  function S2(e3) {
    let t3;
    t3 = "function" == typeof e3 ? e3(a2.scrollTop) : e3;
    const n3 = function(e4) {
      let t4 = e4;
      return Number.isNaN(Lt(_2)) || (t4 = Math.min(t4, Lt(_2))), t4 = Math.max(t4, 0), t4;
    }(t3);
    Lt(d2) && (Lt(d2).scrollTop = n3), a2.scrollTop = n3;
  }
  function E2(e3) {
    S2(e3);
  }
  function I2(t3) {
    var n3;
    const { scrollTop: o3 } = t3.currentTarget;
    o3 !== a2.scrollTop && S2(o3), null == (n3 = e2.onScroll) || n3.call(e2, t3);
  }
  const { onWheel: O2, onFireFoxScroll: N2 } = function(e3, t3, n3, o3) {
    let r3 = 0, s3 = null, a3 = null, l3 = false;
    const i3 = nk(t3, n3);
    return { onWheel: function(t4) {
      if (!Lt(e3))
        return;
      yp.cancel(s3);
      const { deltaY: n4 } = t4;
      r3 += n4, a3 = n4, !i3(n4) && (ok || t4.preventDefault(), s3 = yp(() => {
        o3(r3 * (l3 ? 10 : 1)), r3 = 0;
      }));
    }, onFireFoxScroll: function(t4) {
      !e3.value || (l3 = t4.detail === a3);
    } };
  }(r2, C2, x2, (e3) => {
    S2((t3) => t3 + e3);
  });
  function M2(e3) {
    Lt(r2) && e3.preventDefault();
  }
  !function(e3, t3, n3) {
    let o3 = false, r3 = 0, s3 = null, a3 = null;
    const l3 = () => {
      s3 && (s3.removeEventListener("touchmove", i3), s3.removeEventListener("touchend", c3));
    };
    function i3(e4) {
      if (o3) {
        const t4 = Math.ceil(e4.touches[0].pageY);
        let o4 = r3 - t4;
        r3 = t4, n3(o4) && e4.preventDefault(), clearInterval(a3), a3 = setInterval(() => {
          o4 *= rk, (!n3(o4, true) || Math.abs(o4) <= 0.1) && clearInterval(a3);
        }, 16);
      }
    }
    function c3() {
      o3 = false, l3();
    }
    function d3(e4) {
      l3(), 1 === e4.touches.length && !o3 && (o3 = true, r3 = Math.ceil(e4.touches[0].pageY), s3 = e4.target, s3.addEventListener("touchmove", i3, { passive: false }), s3.addEventListener("touchend", c3));
    }
    Zo(() => {
      document.addEventListener("touchmove", eu, { passive: false }), ho(e3, (e4) => {
        var n4, o4;
        null == (n4 = Lt(t3)) || n4.removeEventListener("touchstart", d3), l3(), clearInterval(a3), e4 && (null == (o4 = Lt(t3)) || o4.addEventListener("touchstart", d3, { passive: false }));
      }, { immediate: true });
    }), Yo(() => {
      document.removeEventListener("touchmove", eu);
    });
  }(r2, d2, (e3, t3) => !k2(e3, t3) && (O2({ preventDefault() {
  }, deltaY: e3 }), true));
  const A2 = () => {
    var e3, t3, n3;
    Lt(d2) && (null == (e3 = Lt(d2)) || e3.removeEventListener("wheel", O2, !!vu && { passive: false }), null == (t3 = Lt(d2)) || t3.removeEventListener("DOMMouseScroll", N2), null == (n3 = Lt(d2)) || n3.removeEventListener("MozMousePixelScroll", M2));
  };
  po(() => {
    un(() => {
      var e3, t3, n3;
      Lt(d2) && (A2(), null == (e3 = Lt(d2)) || e3.addEventListener("wheel", O2, !!vu && { passive: false }), null == (t3 = Lt(d2)) || t3.addEventListener("DOMMouseScroll", N2), null == (n3 = Lt(d2)) || n3.addEventListener("MozMousePixelScroll", M2));
    });
  }), Yo(() => {
    A2();
  });
  const D2 = function(e3, t3, n3, o3, r3, s3, a3, l3) {
    let i3;
    return (c3) => {
      if (null == c3)
        return void l3();
      yp.cancel(i3);
      const d3 = t3.value, u3 = o3.itemHeight;
      if ("number" == typeof c3)
        a3(c3);
      else if (c3 && "object" == typeof c3) {
        let t4;
        const { align: o4 } = c3;
        "index" in c3 ? { index: t4 } = c3 : t4 = d3.findIndex((e4) => r3(e4) === c3.key);
        const { offset: l4 = 0 } = c3, p3 = (c4, f3) => {
          if (c4 < 0 || !e3.value)
            return;
          const h3 = e3.value.clientHeight;
          let v3 = false, m3 = f3;
          if (h3) {
            const s4 = f3 || o4;
            let i4 = 0, c5 = 0, p4 = 0;
            const g3 = Math.min(d3.length, t4);
            for (let e4 = 0; e4 <= g3; e4 += 1) {
              const o5 = r3(d3[e4]);
              c5 = i4;
              const s5 = n3.get(o5);
              p4 = c5 + (void 0 === s5 ? u3 : s5), i4 = p4, e4 === t4 && void 0 === s5 && (v3 = true);
            }
            const y3 = e3.value.scrollTop;
            let b3 = null;
            switch (s4) {
              case "top":
                b3 = c5 - l4;
                break;
              case "bottom":
                b3 = p4 - h3 + l4;
                break;
              default:
                c5 < y3 ? m3 = "top" : p4 > y3 + h3 && (m3 = "bottom");
            }
            null !== b3 && b3 !== y3 && a3(b3);
          }
          i3 = yp(() => {
            v3 && s3(), p3(c4 - 1, m3);
          }, 2);
        };
        p3(5);
      }
    };
  }(d2, i2, g2, e2, f2, m2, S2, () => {
    var e3;
    null == (e3 = Lt(p2)) || e3.syncScrollBar();
  });
  o2({ scrollTo: D2 });
  const V2 = ga(() => {
    let t3 = null;
    return e2.height && (t3 = { [e2.fullHeight ? "height" : "maxHeight"]: e2.height + "px", ...ak }, r2.value && (t3.overflowY = "hidden", a2.scrollMoving && (t3.pointerEvents = "none"))), t3;
  });
  ho([() => b2.start, () => b2.end, i2], () => {
    if (e2.onVisibleChange) {
      const t3 = Lt(i2).slice(b2.start, b2.end + 1);
      e2.onVisibleChange(t3, Lt(i2));
    }
  }, { flush: "post" }), Oh(() => {
    const { prefixCls: o3 = "cds-virtual-list", height: s3, itemHeight: l3, fullHeight: c3, data: f3, itemKey: g3, virtual: y3, component: w3 = "div", onScroll: _3, children: C3 = t2.default, ...x3 } = e2, { style: k3, class: S3 } = n2, O3 = Cp(o3, S3), { scrollTop: N3 } = a2, { scrollHeight: M3, offset: A3, start: D3, end: T2 } = b2;
    return ba("div", { style: { ...k3, position: "relative" }, class: O3, ...x3 }, [ba(w3, { ref: d2, class: `${o3}__holder`, style: Lt(V2), onScroll: I2 }, Qx({ ref: u2, prefixCls: o3, height: M3, offset: A3, onInnerResize: m2 }, { slots: { default: () => function(e3, t3, n3, o4, r3, { getKey: s4 }) {
      return e3.slice(t3, n3 + 1).map((e4, n4) => {
        const a3 = r3(e4, t3 + n4, {}), l4 = s4(e4);
        return ba(Jx, { key: l4, setRef: (t4) => o4(e4, t4) }, { default: () => a3 });
      });
    }(Lt(i2), D3, T2, v2, C3, h2) }, attrs: {}, emit: () => {
    } })), Lt(r2) && ba(tk, { ref: p2, prefixCls: o3, scrollTop: N3, height: s3, scrollHeight: M3, count: Lt(i2).length, onScroll: E2, onStarMove: () => {
      a2.scrollMoving = true;
    }, onStopMove: () => {
      a2.scrollMoving = false;
    } })]);
  });
} });
const ik = { width: 0, height: 0, display: "flex", overflow: "hidden", opacity: 0, border: 0, padding: 0, margin: 0 }, ck = `TREE_MOTION_${Math.random()}`, dk = { key: ck }, uk = { key: ck, level: 0, index: 0, pos: "0", node: dk, nodes: [dk] }, pk = { parent: null, children: [], pos: uk.pos, data: dk, title: null, key: ck, isStart: [], isEnd: [] }, fk = Lo({ name: "NodeList", inheritAttrs: false, props: { motion: { type: Object }, focusable: { type: Boolean }, activeItem: { type: Object }, focused: { type: Boolean }, tabindex: { type: Number }, checkable: { type: Boolean }, selectable: { type: Boolean }, disabled: { type: Boolean }, height: { type: Number }, itemHeight: { type: Number }, virtual: { type: Boolean }, onScroll: { type: Function }, onKeydown: { type: Function }, onFocus: { type: Function }, onBlur: { type: Function }, onActiveChange: { type: Function }, onContextmenu: { type: Function }, onListChangeStart: { type: Function }, onListChangeEnd: { type: Function } }, setup(e2, { expose: t2, attrs: n2 }) {
  const o2 = At(), r2 = At(), { expandedKeys: s2, flattenNodes: a2 } = Ux(), l2 = Dt(Lt(a2)), i2 = Dt([]), c2 = At(null);
  function d2() {
    l2.value = a2.value, i2.value = [], c2.value = null, e2.onListChangeEnd && e2.onListChangeEnd();
  }
  const u2 = zx();
  ho([() => Lt(s2).slice(), a2], ([t3, n3], [o3, r3]) => {
    const s3 = function(e3 = [], t4 = []) {
      const n4 = e3.length, o4 = t4.length;
      if (1 !== Math.abs(n4 - o4))
        return { add: false, key: null };
      function r4(e4, t5) {
        const n5 = /* @__PURE__ */ new Map();
        e4.forEach((e5) => {
          n5.set(e5, true);
        });
        const o5 = t5.filter((e5) => !n5.has(e5));
        return 1 === o5.length ? o5[0] : null;
      }
      return n4 < o4 ? { add: true, key: r4(e3, t4) } : { add: false, key: r4(t4, e3) };
    }(o3, t3);
    if (null !== s3.key) {
      const { virtual: t4, height: o4, itemHeight: a3 } = e2;
      if (s3.add) {
        const e3 = r3.findIndex(({ key: e4 }) => e4 === s3.key), d3 = hk(Q_(r3, n3, s3.key), t4, o4, a3), u3 = r3.slice();
        u3.splice(e3 + 1, 0, pk), l2.value = u3, i2.value = d3, c2.value = "show";
      } else {
        const e3 = n3.findIndex(({ key: e4 }) => e4 === s3.key), d3 = hk(Q_(n3, r3, s3.key), t4, o4, a3), u3 = n3.slice();
        u3.splice(e3 + 1, 0, pk), l2.value = u3, i2.value = d3, c2.value = "hide";
      }
    } else
      r3 !== n3 && (l2.value = n3);
  }), ho(() => Lt(u2).dragging, (e3) => {
    e3 || d2();
  });
  const p2 = ga(() => void 0 === e2.motion ? Lt(l2) : Lt(a2)), f2 = () => {
    e2.onActiveChange && e2.onActiveChange(null);
  };
  Oh(() => {
    const { selectable: t3, checkable: s3, disabled: a3, motion: l3, height: u3, itemHeight: h2, virtual: v2, focusable: m2, activeItem: g2, focused: y2, tabindex: b2, onKeydown: w2, onFocus: _2, onBlur: C2, onListChangeStart: x2, onListChangeEnd: k2, ...S2 } = { ...e2, ...n2 };
    return ba(bs, [y2 && g2 && ba("span", { style: ik, "aria-live": "assertive" }, [mk(g2)]), ba("div", [ba("input", { style: ik, disabled: false === y2 || a3, tabindex: false !== m2 ? b2 : null, value: "", "aria-label": "for screen reader", onKeydown: w2, onFocus: _2, onChange: eu })]), ba("div", { "aria-hidden": "true", class: "cds-treenode", style: { position: "absolute", pointerEvents: "none", visibility: "hidden", height: 0, overflow: "hidden" } }, ba("div", { class: "cds-treenode__indent" }, ba("div", { ref: r2, class: "cds-treenode__indent-unit" }))), ba(lk, { ...Gd(S2, ["onActiveChange"]), prefixCls: "cds-tree-list", ref: o2, data: Lt(p2), height: u3, fullHeight: false, virtual: v2, itemKey: vk, itemHeight: h2, onVisibleChange: (e3, t4) => {
      const n3 = new Set(e3);
      t4.filter((e4) => !n3.has(e4)).some((e4) => vk(e4) === ck) && d2();
    } }, { default: (e3) => {
      const { pos: t4, data: { ...n3 }, title: o3, key: r3, isStart: s4, isEnd: a4 } = e3, u4 = K_(r3, t4);
      return delete n3.key, delete n3.children, ba(Xx, { ...n3, eventKey: u4, title: o3, active: !!g2 && r3 === g2.key, data: e3.data, isStart: s4, isEnd: a4, motion: l3, motionNodes: r3 === ck ? Lt(i2) : null, motionType: Lt(c2), onMotionStart: x2, onMotionEnd: d2, onMousemove: f2 });
    } })]);
  }), t2({ scrollTo: (e3) => {
    Lt(o2).scrollTo(e3);
  }, getIndentWidth: () => Lt(r2).offsetWidth });
} });
function hk(e2, t2, n2, o2) {
  return t2 && n2 ? e2.slice(0, Math.ceil(n2 / o2) + 1) : e2;
}
function vk(e2) {
  const { key: t2, pos: n2 } = e2;
  return K_(t2, n2);
}
function mk(e2) {
  let t2 = String(e2.key), n2 = e2;
  for (; n2.parent; )
    n2 = n2.parent, t2 = `${n2.key} > ${t2}`;
  return t2;
}
const gk = Lo({ name: "TreeProvider", inheritAttrs: false, props: { ...A_(), ...Qf(), ...T_(), children: [Object, Array], activeKey: { type: [Number, String] }, focusable: { type: Boolean, default: void 0 }, tabindex: { type: Number, default: void 0 }, defaultCheckedKeys: { type: Array, default: () => [] }, dropIndicatorRender: { type: Function, default: Bx }, itemHeight: { type: Number, default: 28 }, replaceFields: { type: Object, default: void 0 } }, setup(e2, { attrs: t2, slots: n2, expose: o2 }) {
  const r2 = At(false), s2 = At(void 0), a2 = Dt([]), l2 = Dt([]), i2 = Dt([]), c2 = Dt([]), d2 = Dt([]), u2 = Dt([]), p2 = {}, f2 = Dt([]), h2 = Dt({}), v2 = At(false), m2 = At(null), g2 = At(false), y2 = ga(() => R_(e2.fieldNames)), b2 = At();
  let w2 = {}, _2 = null, C2 = null, x2 = null;
  const k2 = vt({ draggingNodeKey: null, dragChildrenKeys: [], dropTargetKey: null, dropPosition: null, dropContainerKey: null, dropLevelOffset: null, dropTargetPos: null, dropAllowed: true, dragOverNodeKey: null }), S2 = ga(() => new Set(Lt(u2))), E2 = ga(() => new Set(Lt(a2))), I2 = ga(() => new Set(Lt(c2))), O2 = ga(() => new Set(Lt(d2))), N2 = ga(() => new Set(Lt(l2))), M2 = ga(() => new Set(Lt(i2))), A2 = ga(() => ({ expandedKeysSet: Lt(S2), selectedKeysSet: Lt(E2), loadedKeysSet: Lt(I2), loadingKeysSet: Lt(O2), checkedKeysSet: Lt(N2), halfCheckedKeysSet: Lt(M2), dragOverNodeKey: k2.dragOverNodeKey, dropPosition: k2.dropPosition, keyEntities: Lt(h2) })), D2 = ga(() => null === m2.value ? null : T2.value.find(({ key: e3 }) => e3 === m2.value) || null);
  ho([() => e2.treeData, () => e2.children], () => {
    f2.value = void 0 !== e2.treeData ? xt(e2.treeData).slice() : P_(xt(e2.children));
  }, { immediate: true, deep: true }), po(() => {
    if (Lt(f2)) {
      const e3 = L_(Lt(f2), { fieldNames: Lt(y2) });
      h2.value = { [ck]: uk, ...e3.keyEntities };
    }
  });
  let V2 = false;
  ho([() => e2.expandedKeys, () => e2.autoExpandParent, h2], ([t3, n3], [o3, r3]) => {
    let s3 = Lt(u2);
    if (void 0 !== e2.expandedKeys || V2 && n3 !== r3)
      s3 = e2.autoExpandParent || !V2 && e2.defaultExpandParent ? F_(e2.expandedKeys, Lt(h2)) : e2.expandedKeys;
    else if (!V2 && e2.defaultExpandAll) {
      const e3 = { ...Lt(h2) };
      delete e3[ck], s3 = Object.keys(e3).map((t4) => e3[t4].key);
    } else
      !V2 && e2.defaultExpandedKeys && (s3 = e2.autoExpandParent || e2.defaultExpandParent ? F_(e2.defaultExpandedKeys, Lt(h2)) : e2.defaultExpandedKeys);
    s3 && (u2.value = s3), V2 = true;
  }, { immediate: true });
  const T2 = Dt([]);
  po(() => {
    T2.value = function(e3, t3, n3) {
      const { _title: o3, key: r3, children: s3 } = R_(n3), a3 = new Set(true === t3 ? [] : t3), l3 = [];
      return function e4(n4, i3 = null) {
        return n4.map((c3, d3) => {
          const u3 = z_(i3 ? i3.pos : "0", d3), p3 = K_(c3[r3], u3);
          let f3;
          for (let e5 = 0; e5 < o3.length; e5 += 1) {
            const t4 = o3[e5];
            if (void 0 !== c3[t4]) {
              f3 = c3[t4];
              break;
            }
          }
          const h3 = { ...Gd(c3, [...o3, r3, s3]), title: f3, key: p3, parent: i3, pos: u3, children: null, data: c3, isStart: [...i3 ? i3.isStart : [], 0 === d3], isEnd: [...i3 ? i3.isEnd : [], d3 === n4.length - 1] };
          return l3.push(h3), true === t3 || a3.has(p3) ? h3.children = e4(c3[s3] || [], h3) : h3.children = [], h3;
        });
      }(e3), l3;
    }(Lt(f2), Lt(u2), Lt(y2));
  }), po(() => {
    e2.selectable && (void 0 !== e2.selectedKeys ? a2.value = H_(e2.selectedKeys, e2) : !V2 && e2.defaultSelectedKeys && (a2.value = H_(e2.defaultSelectedKeys, e2)));
  });
  const { maxLevel: L2, levelEntities: P2 } = D_(h2);
  po(() => {
    if (e2.checkable) {
      let t3;
      if (void 0 !== e2.checkedKeys ? t3 = j_(e2.checkedKeys) || {} : !V2 && e2.defaultCheckedKeys ? t3 = j_(e2.defaultCheckedKeys) || {} : Lt(f2) && (t3 = j_(e2.checkedKeys) || { checkedKeys: Lt(l2), halfCheckedKeys: Lt(i2) }), t3) {
        let { checkedKeys: n3 = [], halfCheckedKeys: o3 = [] } = t3;
        e2.checkStrictly || ({ checkedKeys: n3, halfCheckedKeys: o3 } = X_(n3, true, Lt(h2), Lt(L2), Lt(P2))), l2.value = n3, i2.value = o3;
      }
    }
  }), po(() => {
    e2.loadedKeys && (d2.value = e2.loadedKeys);
  });
  const F2 = () => {
    Object.assign(k2, { dragOverNodeKey: null, dropPosition: null, dropLevelOffset: null, dropTargetKey: null, dropContainerKey: null, dropTargetPos: null, dropAllowed: false });
  }, R2 = (e3) => {
    b2.value.scrollTo(e3);
  };
  ho(() => e2.activeKey, () => {
    void 0 !== e2.activeKey && (m2.value = e2.activeKey);
  }, { immediate: true }), ho(m2, (e3) => {
    un(() => {
      null !== e3 && R2({ key: e3 });
    });
  }, { immediate: true, flush: "post" });
  const $2 = (t3) => {
    void 0 === e2.expandedKeys && (u2.value = t3);
  }, B2 = () => {
    null !== k2.draggingNodeKey && Object.assign(k2, { draggingNodeKey: null, dropPosition: null, dropContainerKey: null, dropTargetKey: null, dropLevelOffset: null, dropAllowed: true, dragOverNodeKey: null }), _2 = null, x2 = null;
  }, H2 = (t3, n3) => {
    const { onDragend: o3 } = e2;
    k2.dragOverNodeKey = null, B2(), null == o3 || o3({ event: t3, node: n3.eventData }), C2 = null;
  }, j2 = (e3) => {
    H2(e3, null), window.removeEventListener("dragend", j2);
  }, K2 = (t3, n3) => {
    const { onDragstart: o3 } = e2, { eventKey: r3, eventData: a3 } = n3;
    C2 = n3, _2 = { x: t3.clientX, y: t3.clientY };
    const l3 = Z_(Lt(u2), r3);
    k2.draggingNodeKey = r3, k2.dragChildrenKeys = function(e3, t4) {
      const n4 = [];
      return function e4(t5 = []) {
        t5.forEach(({ key: t6, children: o4 }) => {
          n4.push(t6), e4(o4);
        });
      }(t4[e3].children), n4;
    }(r3, Lt(h2)), s2.value = Lt(b2).getIndentWidth(), $2(l3), window.addEventListener("dragend", j2), o3 && o3({ event: t3, node: a3 });
  }, z2 = (t3, n3) => {
    const { onDragenter: o3, onExpand: r3, allowDrop: a3 } = e2, { pos: l3, eventKey: i3 } = n3;
    if (x2 !== i3 && (x2 = i3), !C2)
      return void F2();
    const { dropPosition: c3, dropLevelOffset: d3, dropTargetKey: p3, dropContainerKey: f3, dropTargetPos: v3, dropAllowed: m3, dragOverNodeKey: g3 } = B_(t3, C2, n3, Lt(s2), _2, a3, Lt(T2), Lt(h2), Lt(S2));
    -1 === k2.dragChildrenKeys.indexOf(p3) && m3 ? (w2 || (w2 = {}), Object.keys(w2).forEach((e3) => {
      clearTimeout(w2[e3]);
    }), C2.eventKey !== n3.eventKey && (w2[l3] = window.setTimeout(() => {
      if (null === k2.draggingNodeKey)
        return;
      let e3 = Lt(u2).slice();
      const o4 = Lt(h2)[n3.eventKey];
      o4 && (o4.children || []).length && (e3 = U_(Lt(u2), n3.eventKey)), $2(e3), r3 && r3(e3, { node: n3.eventData, expanded: true, nativeEvent: t3 });
    }, 800)), C2.eventKey !== p3 || 0 !== d3 ? (Object.assign(k2, { dragOverNodeKey: g3, dropPosition: c3, dropLevelOffset: d3, dropTargetKey: p3, dropContainerKey: f3, dropTargetPos: v3, dropAllowed: m3 }), o3 && o3({ event: t3, node: n3.eventData, expandedKeys: Lt(u2) })) : F2()) : F2();
  }, Z2 = (t3, n3) => {
    const { onDragover: o3, allowDrop: r3 } = e2;
    if (!C2)
      return;
    const { dropPosition: a3, dropLevelOffset: l3, dropTargetKey: i3, dropContainerKey: c3, dropAllowed: d3, dropTargetPos: u3, dragOverNodeKey: p3 } = B_(t3, C2, n3, Lt(s2) || 0, _2, r3, T2.value, h2.value, S2.value);
    -1 !== k2.dragChildrenKeys.indexOf(i3) || !d3 || (C2.eventKey === i3 && 0 === l3 ? null === k2.dropPosition && null === k2.dropLevelOffset && null === k2.dropTargetKey && null === k2.dropContainerKey && null === k2.dropTargetPos && false === k2.dropAllowed && null === k2.dragOverNodeKey || F2() : a3 === k2.dropPosition && l3 === k2.dropLevelOffset && i3 === k2.dropTargetKey && c3 === k2.dropContainerKey && u3 === k2.dropTargetPos && d3 === k2.dropAllowed && p3 === k2.dragOverNodeKey || Object.assign(k2, { dropPosition: a3, dropLevelOffset: l3, dropTargetKey: i3, dropContainerKey: c3, dropTargetPos: u3, dropAllowed: d3, dragOverNodeKey: p3 }), o3 && o3({ event: t3, node: n3.eventData }));
  }, U2 = (t3, n3) => {
    x2 === n3.eventKey && !t3.currentTarget.contains(t3.relatedTarget) && (F2(), x2 = null);
    const { onDragleave: o3 } = e2;
    o3 && o3({ event: t3, node: n3.eventData });
  }, W2 = (t3, n3, o3 = false) => {
    var r3;
    const { dragChildrenKeys: s3, dropPosition: a3, dropTargetKey: l3, dropTargetPos: i3, dropAllowed: c3 } = k2;
    if (!c3)
      return;
    const { onDrop: d3 } = e2;
    if (k2.dragOverNodeKey = null, B2(), null === l3)
      return;
    const u3 = { ...$_(l3, xt(Lt(A2))), active: (null == (r3 = Lt(D2)) ? void 0 : r3.key) === l3, data: Lt(h2)[l3].node };
    s3.indexOf(l3);
    const p3 = W_(i3), f3 = { event: t3, node: G_(u3), dragNode: C2 ? C2.eventData : null, dragNodesKeys: [C2.eventKey].concat(s3), dropToGap: 0 !== a3, dropPosition: a3 + Number(p3[p3.length - 1]) };
    o3 || null == d3 || d3(f3), C2 = null;
  }, Y2 = (t3, n3) => {
    const { onClick: o3 } = e2;
    o3 && o3(t3, n3);
  }, G2 = (t3, n3) => {
    const { onDblclick: o3 } = e2;
    o3 && o3(t3, n3);
  }, q2 = (t3, n3) => {
    let o3 = Lt(a2);
    const { onSelect: r3, multiple: s3 } = e2, { selected: l3 } = n3, i3 = n3[y2.value.key], c3 = !l3;
    o3 = c3 ? s3 ? U_(o3, i3) : [i3] : Z_(o3, i3);
    const d3 = Lt(h2), u3 = o3.map((e3) => {
      const t4 = d3[e3];
      return t4 ? t4.node : null;
    }).filter((e3) => e3);
    void 0 === e2.selectedKeys && (a2.value = o3), r3 && r3(o3, { event: "select", selected: c3, node: n3, selectedNodes: u3, nativeEvent: t3 });
  }, X2 = (t3, n3, o3) => {
    const { checkStrictly: r3, onCheck: s3 } = e2, a3 = n3[Lt(y2).key];
    let c3;
    const d3 = { event: "check", node: n3, checked: o3, nativeEvent: t3 }, u3 = Lt(h2);
    if (r3) {
      const t4 = o3 ? U_(Lt(l2), a3) : Z_(Lt(l2), a3);
      c3 = { checked: t4, halfChecked: Z_(Lt(i2), a3) }, d3.checkedNodes = t4.map((e3) => u3[e3]).filter((e3) => e3).map((e3) => e3.node), void 0 === e2.checkedKeys && (l2.value = t4);
    } else {
      let { checkedKeys: t4, halfCheckedKeys: n4 } = X_([...Lt(l2), a3], true, u3, Lt(L2), Lt(P2));
      if (!o3) {
        const e3 = new Set(t4);
        e3.delete(a3), { checkedKeys: t4, halfCheckedKeys: n4 } = X_(Array.from(e3), { checked: false, halfCheckedKeys: n4 }, u3, Lt(L2), Lt(P2));
      }
      c3 = t4, d3.checkedNodes = [], d3.checkedNodesPositions = [], d3.halfCheckedKeys = n4, t4.forEach((e3) => {
        const t5 = u3[e3];
        if (!t5)
          return;
        const { node: n5, pos: o4 } = t5;
        d3.checkedNodes.push(n5), d3.checkedNodesPositions.push({ node: n5, pos: o4 });
      }), void 0 === e2.checkedKeys && (l2.value = t4, i2.value = n4);
    }
    s3 && s3(c3, d3);
  }, J2 = (t3) => {
    const n3 = t3[y2.value.key], o3 = new Promise((o4, r3) => {
      const { loadData: s3, onLoad: a3 } = e2;
      if (!s3 || Lt(I2).has(n3) || Lt(O2).has(n3))
        return null;
      s3(t3).then(() => {
        const r4 = U_(Lt(c2), n3), s4 = Z_(Lt(d2), n3);
        a3 && a3(r4, { event: "load", node: t3 }), void 0 === e2.loadedKeys && (c2.value = r4), d2.value = s4, o4();
      }).catch((t4) => {
        if (d2.value = Z_(d2.value, n3), p2[n3] = (p2[n3] || 0) + 1, p2[n3] >= 10) {
          const t5 = U_(c2.value, n3);
          void 0 === e2.loadedKeys && (c2.value = t5), o4();
        }
        r3(t4);
      }), d2.value = U_(d2.value, n3);
    });
    return o3.catch(() => {
    }), o3;
  }, Q2 = (t3, n3) => {
    const { onMouseenter: o3 } = e2;
    o3 && o3({ event: t3, node: n3 });
  }, ee2 = (t3, n3) => {
    const { onMouseleave: o3 } = e2;
    o3 && o3({ event: t3, node: n3 });
  }, te2 = (t3, n3) => {
    const { onRightClick: o3 } = e2;
    o3 && (t3.preventDefault(), o3({ event: t3, node: n3 }));
  }, ne2 = (t3, n3) => {
    let o3 = Lt(u2);
    const { onExpand: r3, loadData: s3 } = e2, { expanded: a3 } = n3, l3 = n3[Lt(y2).key];
    if (Lt(g2))
      return;
    o3.indexOf(l3);
    const i3 = !a3;
    if (o3 = i3 ? U_(o3, l3) : Z_(o3, l3), $2(o3), r3 && r3(o3, { node: n3, expanded: i3, nativeEvent: t3 }), i3 && s3) {
      const e3 = J2(n3);
      e3 && e3.then(() => {
      }).catch((e4) => {
        const t4 = Z_(u2.value, l3);
        $2(t4), Promise.reject(e4);
      });
    }
  }, oe2 = () => {
    g2.value = true;
  }, re2 = () => {
    setTimeout(() => {
      g2.value = false;
    });
  }, se2 = (t3) => {
    const { onActiveChange: n3 } = e2;
    Lt(m2) !== t3 && (void 0 !== e2.activeKey && (m2.value = t3), null !== t3 && R2({ key: t3 }), n3 && n3(t3));
  }, ae2 = (t3) => {
    const { onFocus: n3 } = e2;
    v2.value = true, n3 && n3(t3);
  }, le2 = (t3) => {
    const { onBlur: n3 } = e2;
    v2.value = false, se2(null), n3 && n3(t3);
  }, ie2 = (e3) => {
    let t3 = Lt(T2).findIndex(({ key: e4 }) => e4 === Lt(m2));
    -1 === t3 && e3 < 0 && (t3 = Lt(T2).length), t3 = (t3 + e3 + Lt(T2).length) % Lt(T2).length;
    const n3 = Lt(T2)[t3];
    if (n3) {
      const { key: e4 } = n3;
      se2(e4);
    } else
      se2(null);
  }, ce2 = ga(() => G_({ ...$_(Lt(m2), A2.value), data: Lt(D2).data, active: true })), de2 = (t3) => {
    const { onKeydown: n3, checkable: o3, selectable: r3 } = e2;
    switch (t3.key) {
      case kd.up:
        ie2(-1), t3.preventDefault();
        break;
      case kd.down:
        ie2(1), t3.preventDefault();
    }
    const s3 = Lt(D2);
    if (s3 && s3.data) {
      const e3 = false === s3.data.isLeaf || !!(s3.data.children || []).length, n4 = Lt(ce2);
      switch (t3.key) {
        case kd.left:
          e3 && S2.value.has(Lt(m2)) ? ne2({}, n4) : s3.parent && se2(s3.parent.key), t3.preventDefault();
          break;
        case kd.right:
          e3 && !S2.value.has(Lt(m2)) ? ne2({}, n4) : s3.children && s3.children.length && se2(s3.children[0].key), t3.preventDefault();
          break;
        case kd.enter:
        case kd.space:
          !o3 || n4.disabled || false === n4.checkable || n4.disableCheckbox ? !o3 && r3 && !n4.disabled && false !== n4.selectable && q2({}, n4) : X2({}, n4, !N2.value.has(Lt(m2)));
      }
    }
    n3 && n3(t3);
  };
  return ((e3) => {
    Ir(Zx, e3);
  })({ expandedKeys: u2, selectedKeys: a2, loadedKeys: c2, loadingKeys: d2, checkedKeys: l2, halfCheckedKeys: i2, expandedKeysSet: S2, selectedKeysSet: E2, loadedKeysSet: I2, loadingKeysSet: O2, checkedKeysSet: N2, halfCheckedKeysSet: M2, flattenNodes: T2 }), Go(() => {
    window.removeEventListener("dragend", j2), r2.value = true;
  }), Oh(() => {
    const { draggingNodeKey: o3, dropLevelOffset: r3, dropContainerKey: a3, dropTargetKey: l3, dropPosition: i3, dragOverNodeKey: c3 } = k2, { showLine: d3, focusable: u3, tabindex: p3 = 0, selectable: f3, showIcon: g3, icon: y3 = n2.icon, switcherIcon: w3, draggable: _3, checkable: C3, checkStrictly: x3, disabled: S3, loadData: E3, openAnimation: I3, filterTreeNode: O3, height: N3, itemHeight: M3, virtual: A3, dropIndicatorRender: V3, onContextmenu: T3, onScroll: L3 } = e2, { class: P3, style: F3 } = t2, R3 = du({ ...e2, ...t2 }, { aria: true, data: true });
    let $3;
    return _3 && ($3 = "object" == typeof _3 ? _3 : "function" == typeof _3 ? { nodeDraggable: _3 } : {}), ba(Kx, { value: { selectable: f3, showIcon: g3, icon: y3, switcherIcon: w3, draggable: $3, draggingNodeKey: o3, checkable: C3, customCheckable: n2.checkable, checkStrictly: x3, disabled: S3, keyEntities: h2.value, dropLevelOffset: r3, dropContainerKey: a3, dropTargetKey: l3, dropPosition: i3, dragOverNodeKey: c3, dragging: null !== o3, indent: s2.value, dropIndicatorRender: V3, loadData: E3, filterTreeNode: O3, onNodeClick: Y2, onNodeDoubleClick: G2, onNodeExpand: ne2, onNodeSelect: q2, onNodeCheck: X2, onNodeLoad: J2, onNodeMouseEnter: Q2, onNodeMouseLeave: ee2, onNodeContextMenu: te2, onNodeDragStart: K2, onNodeDragEnter: z2, onNodeDragOver: Z2, onNodeDragLeave: U2, onNodeDragEnd: H2, onNodeDrop: W2, slots: n2 } }, { default: () => ba("div", { role: "tree", class: Cp("cds-tree", P3, { "cds-tree--show-line": d3, "cds-tree--focused": Lt(v2), "cds-tree--active-focused": null !== Lt(m2) }) }, ba(fk, { ref: b2, style: F3, disabled: S3, selectable: f3, checkable: !!C3, motion: I3, virtual: A3, height: N3, itemHeight: M3, focusable: u3, focused: Lt(v2), tabindex: p3, activeItem: Lt(D2), onFocus: ae2, onBlur: le2, onKeydown: de2, onActiveChange: se2, onListChangeStart: oe2, onListChangeEnd: re2, onContextmenu: T3, onScroll: L3, ...R3 })) });
  }), o2({ onNodeExpand: ne2, scrollTo: R2, onKeydown: de2, selectedKeys: ga(() => a2.value), checkedKeys: ga(() => l2.value), halfCheckedKeys: ga(() => i2.value), loadedKeys: ga(() => c2.value), loadingKeys: ga(() => d2.value), expandedKeys: ga(() => u2.value) }), {};
} });
const yk = Lo({ name: "CdsTree", inheritAttrs: false, props: { ...A_(), ...T_(), ...Qf() }, emits: { "update:selectedKeys": (e2) => true, "update:checkedKeys": (e2) => true, "update:expandedKeys": (e2) => true, click: (e2, t2) => true, check: (e2, t2) => true, dragstart: (e2) => true, dragend: (e2) => true, dragenter: (e2) => true, dragleave: (e2) => true, dragover: (e2) => true, drop: (e2) => true, expand: (e2, t2) => true, select: (e2, t2) => true, load: (e2) => true, rightClick: (e2) => true, dblclick: (e2, t2) => true, doubleclick: (e2, t2) => true }, setup(e2, { attrs: t2, expose: n2, emit: o2, slots: r2 }) {
  const s2 = At(), a2 = (t3, n3) => {
    var r3;
    "click" === e2.expandAction && (null == (r3 = Lt(s2)) || r3.onNodeExpand(t3, n3)), o2("click", t3, n3);
  }, l2 = (t3, n3) => {
    var r3;
    ("dblclick" === e2.expandAction || "doubleclick" === e2.expandAction) && (null == (r3 = Lt(s2)) || r3.onNodeExpand(t3, n3)), o2("dblclick", t3, n3), o2("doubleclick", t3, n3);
  }, i2 = (e3, t3) => {
    o2("update:checkedKeys", e3), o2("check", e3, t3);
  }, c2 = (e3, t3) => {
    o2("update:expandedKeys", e3), o2("expand", e3, t3);
  }, d2 = (e3, t3) => {
    o2("update:selectedKeys", e3), o2("select", e3, t3);
  };
  return Oh(() => {
    const { showIcon: n3, showLine: o3, blockNode: u2, checkable: p2, selectable: f2, fieldNames: h2, openAnimation: v2, virtual: m2, icon: g2 = r2.icon, switcherIcon: y2 = r2.switcherIcon } = e2, b2 = r2.default ? fu(r2.default()) : void 0;
    return ba(gk, { ...t2, ...e2, ref: s2, showLine: !!o3, fieldNames: h2, icon: g2, virtual: m2, openAnimation: v2, checkable: p2, selectable: f2, children: b2, class: Cp({ "cds-tree--icon-hide": !n3, "cds-tree--block-node": u2, "cds-tree--unselectable": !f2, "cds-tree--uncheckable": !p2, "cds-tree--clickable-node": !!e2.expandAction }, t2.class), dropIndicatorRender: Bx, switcherIcon: (e3) => function(e4, t3, n4) {
      const { isLeaf: o4, expanded: r3, loading: s3 } = n4;
      let a3 = e4;
      if (s3)
        return ba(Gg, { class: "cds-tree__switcher-loading-icon", size: "xs" });
      let l3 = false;
      t3 && "object" == typeof t3 && (l3 = t3.showLeafIcon);
      let i3 = null;
      const c3 = "cds-tree__switcher-icon";
      return o4 ? (t3 && (i3 = "object" == typeof t3 && l3 ? ba(Lm, { name: "string" == typeof l3 ? l3 : "doc" }) : ba("span", { class: "cds-tree__switcher-leaf-line" })), i3) : (i3 = ba(Lm, { name: "chevron-down", class: c3 }), "function" == typeof e4 ? a3 = e4({ ...n4, defaultIcon: i3, switcherCls: c3 }) : Qd(a3) && (a3 = $s(a3, { class: c3 })), a3 || i3);
    }(y2, o3, e3), onClick: a2, onCheck: i2, onExpand: c2, onSelect: d2, onDblclick: l2 }, { ...r2, checkable: () => ba("span", { class: Cp("cds-tree__checkbox-inner") }) });
  }), n2({ treeRef: s2, onNodeExpand: (...e3) => {
    var t3;
    null == (t3 = Lt(s2)) || t3.onNodeExpand(...e3);
  }, scrollTo: (e3) => {
    var t3;
    null == (t3 = Lt(s2)) || t3.scrollTo(e3);
  }, selectedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(s2)) ? void 0 : e3.selectedKeys;
  }), checkedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(s2)) ? void 0 : e3.checkedKeys;
  }), halfCheckedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(s2)) ? void 0 : e3.halfCheckedKeys;
  }), expandedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(s2)) ? void 0 : e3.expandedKeys;
  }), loadingKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(s2)) ? void 0 : e3.loadingKeys;
  }), loadedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(s2)) ? void 0 : e3.loadedKeys;
  }) }), {};
} });
function bk(e2) {
  const { isLeaf: t2, expanded: n2 } = e2;
  return ba(Lm, t2 ? { name: "doc" } : n2 ? { name: "directory-open" } : { name: "directory" });
}
const wk = Lo({ name: "CdsTreeDirectory", inheritAttrs: false, props: { ...A_({ showIcon: true, expandAction: "click" }) }, emits: { "update:selectedKeys": (e2) => true, "update:checkedKeys": (e2) => true, "update:expandedKeys": (e2) => true, click: (e2, t2) => true, check: (e2, t2) => true, dragstart: (e2) => true, dragend: (e2) => true, dragenter: (e2) => true, dragleave: (e2) => true, dragover: (e2) => true, drop: (e2) => true, expand: (e2, t2) => true, select: (e2, t2) => true, load: (e2) => true, rightClick: (e2) => true, dblclick: (e2, t2) => true, doubleclick: (e2, t2) => true }, setup(e2, { attrs: t2, slots: n2, emit: o2, expose: r2 }) {
  var s2;
  const a2 = At(e2.treeData || P_(fu(null == (s2 = n2.default) ? void 0 : s2.call(n2))));
  ho(() => e2.treeData, () => {
    a2.value = e2.treeData;
  }), Wo(() => {
    un(() => {
      var t3;
      void 0 !== e2.treeData && n2.default && (a2.value = P_(fu(null == (t3 = n2.default) ? void 0 : t3.call(n2))));
    });
  });
  const l2 = At(), i2 = At(), c2 = ga(() => R_(e2.fieldNames)), d2 = At(), u2 = At(e2.selectedKeys || e2.defaultSelectedKeys || []), p2 = At((() => {
    const { keyEntities: t3 } = L_(Lt(a2), { fieldNames: Lt(c2) });
    let n3;
    return n3 = e2.defaultExpandAll ? Object.keys(t3) : e2.defaultExpandParent ? F_(e2.expandedKeys || e2.defaultExpandedKeys || [], t3) : e2.expandedKeys || e2.defaultExpandedKeys, n3;
  })());
  ho(() => e2.selectedKeys, () => {
    void 0 !== e2.selectedKeys && (u2.value = e2.selectedKeys);
  }, { immediate: true }), ho(() => e2.expandedKeys, () => {
    void 0 !== e2.expandedKeys && (p2.value = e2.expandedKeys);
  }, { immediate: true });
  const f2 = function(e3, t3) {
    let n3 = 0;
    return (...o3) => {
      clearTimeout(n3), n3 = setTimeout(() => e3(...o3), t3);
    };
  }((e3, t3) => {
    const { isLeaf: n3 } = t3;
    n3 || e3.shiftKey || e3.metaKey || e3.ctrlKey || Lt(d2).onNodeExpand(e3, t3);
  }, 200), h2 = (t3, n3) => {
    void 0 === e2.expandedKeys && (p2.value = t3), o2("update:expandedKeys", t3), o2("expand", t3, n3);
  }, v2 = (t3, n3) => {
    const { expandAction: r3 } = e2;
    "click" === r3 && f2(t3, n3), o2("click", t3, n3);
  }, m2 = (t3, n3) => {
    const { expandAction: r3 } = e2;
    ("dblclick" === r3 || "doubleclick" === r3) && f2(t3, n3), o2("doubleclick", t3, n3), o2("dblclick", t3, n3);
  }, g2 = (t3, n3) => {
    const { node: r3, nativeEvent: s3 } = n3, d3 = r3[Lt(c2).key], f3 = { ...n3, selected: true }, h3 = (null == s3 ? void 0 : s3.ctrlKey) || (null == s3 ? void 0 : s3.metaKey), v3 = null == s3 ? void 0 : s3.shiftKey;
    let m3;
    e2.multiple && h3 ? (m3 = t3, l2.value = d3, i2.value = m3, f3.selectedNodes = tC(Lt(a2), m3, Lt(c2))) : e2.multiple && v3 ? (m3 = Array.from(/* @__PURE__ */ new Set([...Lt(i2) || [], ...eC({ treeData: Lt(a2), expandedKeys: Lt(p2), startKey: d3, endKey: Lt(l2), fieldNames: Lt(c2) })])), f3.selectedNodes = tC(Lt(a2), m3, Lt(c2))) : (m3 = [d3], l2.value = d3, i2.value = m3, f3.selectedNodes = tC(Lt(a2), m3, Lt(c2))), o2("update:selectedKeys", m3), o2("select", m3, f3), void 0 === e2.selectedKeys && (u2.value = m3);
  }, y2 = (e3, t3) => {
    o2("update:checkedKeys", e3), o2("check", e3, t3);
  };
  return Oh(() => {
    const o3 = Cp("cds-tree-directory", t2.class), { icon: r3 = n2.icon, blockNode: s3 = true, ...a3 } = e2;
    return ba(yk, { ...t2, ref: d2, icon: r3 || bk, blockNode: s3, ...a3, selectable: true, class: o3, expandedKeys: Lt(p2), selectedKeys: Lt(u2), onDblclick: m2, onSelect: g2, onClick: v2, onExpand: h2, onCheck: y2 }, n2);
  }), r2({ scrollTo: (e3) => {
    var t3;
    null == (t3 = Lt(d2)) || t3.scrollTo(e3);
  }, selectedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(d2)) ? void 0 : e3.selectedKeys;
  }), checkedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(d2)) ? void 0 : e3.checkedKeys;
  }), halfCheckedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(d2)) ? void 0 : e3.halfCheckedKeys;
  }), loadedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(d2)) ? void 0 : e3.loadedKeys;
  }), loadingKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(d2)) ? void 0 : e3.loadingKeys;
  }), expandedKeys: ga(() => {
    var e3;
    return null == (e3 = Lt(d2)) ? void 0 : e3.expandedKeys;
  }) }), {};
} }), _k = tu({ name: { type: String, default: void 0 }, capture: { type: [Boolean, String], default: void 0 }, multiple: { type: Boolean, default: true }, action: { type: [String, Function], default: void 0 }, method: { type: String, default: void 0 }, directory: { type: Boolean, default: void 0 }, defaultFileList: { type: Array, default: void 0 }, showUploadList: { type: [Boolean, Object], default: void 0 }, itemRender: { type: Function, default: void 0 }, listType: { type: String, default: void 0 }, data: { type: [Object, Function], default: void 0 }, headers: { type: Object, default: void 0 }, accept: { type: [String, Array], default: void 0 }, customRequest: { type: Function, default: void 0 }, withCredentials: { type: Boolean, default: void 0 }, openFileDialogOnClick: { type: Boolean, default: void 0 }, beforeUpload: { type: Function, default: void 0 }, maxCount: { type: Number, default: void 0 }, isImageUrl: { type: Function, default: void 0 }, showTooltipWithError: { type: Boolean, default: false }, removeIcon: { type: String, default: "remove", validator: (e2) => Hf.includes(e2) }, downloadIcon: { type: String, default: "download", validator: (e2) => Hf.includes(e2) }, uploadIcon: { type: String, default: "upload", validator: (e2) => Hf.includes(e2) } }, "upload"), Ck = tu({ items: { type: Array, default: void 0 }, listType: { type: String, default: void 0 }, itemRender: { type: Function, default: void 0 }, showRemoveIcon: { type: Boolean, default: void 0 }, showDownloadIcon: { type: Boolean, default: void 0 }, showPreviewIcon: { type: Boolean, default: void 0 }, showTooltipWithError: { type: Boolean, default: false }, removeIcon: { type: String, default: "remove", validator: (e2) => Hf.includes(e2) }, downloadIcon: { type: String, default: "download", validator: (e2) => Hf.includes(e2) }, uploadIcon: { type: String, default: "upload", validator: (e2) => Hf.includes(e2) }, isImageUrl: { type: Function, default: void 0 }, progress: { type: Object, default: void 0 }, onDownload: { type: Function, default: void 0 }, onPreview: { type: Function, default: void 0 }, onRemove: { type: Function, default: void 0 } }, "upload-list");
function xk() {
  function e2(e3) {
    const t2 = e3.responseText || e3.response;
    if (!t2)
      return t2;
    try {
      return JSON.parse(t2);
    } catch {
      return t2;
    }
  }
  return function(t2) {
    const n2 = new XMLHttpRequest();
    t2.onProgress && n2.upload && (n2.upload.onprogress = function(e3) {
      e3.total > 0 && (e3.percent = e3.loaded / e3.total * 100), t2.onProgress(e3);
    });
    const o2 = new FormData();
    t2.data && Object.keys(t2.data).forEach((e3) => {
      const n3 = t2.data[e3];
      Array.isArray(n3) ? n3.forEach((t3) => {
        o2.append(`${e3}[]`, t3);
      }) : o2.append(e3, n3);
    }), t2.file instanceof Blob ? o2.append(t2.filename || "file", t2.file, t2.file.name) : o2.append(t2.filename || "file", t2.file), n2.onerror = function(e3) {
      t2.onError(e3);
    }, n2.onload = function() {
      return n2.status < 200 || n2.status >= 300 ? t2.onError(function(e3, t3) {
        const n3 = `fail to ${e3.method} ${e3.action} ${t3.status}'`, o3 = new Error(n3);
        return o3.status = t3.status, o3.method = e3.method, o3.url = e3.action, o3;
      }(t2, n2), e2(n2)) : t2.onSuccess(e2(n2), n2);
    }, n2.open(t2.method || "post", t2.action, true), t2.withCredentials && "withCredentials" in n2 && (n2.withCredentials = true);
    const r2 = t2.headers || {};
    return null !== r2["X-Requested-With"] && n2.setRequestHeader("X-Requested-With", "XMLHttpRequest"), Object.keys(r2).forEach((e3) => {
      null !== r2[e3] && n2.setRequestHeader(e3, r2[e3]);
    }), n2.send(o2), { abort() {
      n2.abort();
    } };
  };
}
const kk = Lo({ name: "CdsUploadListItem", inheritAttrs: false, props: { file: { type: Object, default: void 0 }, ...Ck(), ...Ev() }, setup(e2, { slots: t2, attrs: n2 }) {
  var o2;
  const { uploader: r2 } = Iv(e2), s2 = Dt(false), a2 = Dt(), l2 = Dt(0);
  Zo(() => {
    a2.value = setTimeout(() => {
      s2.value = true;
    }, 300);
  }), Yo(() => {
    clearTimeout(Lt(a2));
  });
  const i2 = Dt(null == (o2 = e2.file) ? void 0 : o2.status);
  return ho(() => {
    var t3;
    return null == (t3 = e2.file) ? void 0 : t3.status;
  }, (e3) => {
    "removed" !== e3 && (i2.value = e3);
  }), Oh(() => {
    var n3, o3;
    const { itemRender: a3 = t2.itemRender, items: c2, file: d2, showDownloadIcon: u2, showRemoveIcon: p2, showTooltipWithError: f2, onDownload: h2, onPreview: v2, onRemove: m2 } = e2;
    let g2 = "link";
    "error" === Lt(i2) ? g2 = "info" : "done" === Lt(i2) && (g2 = null != d2 && d2.url ? "link" : "check");
    const y2 = (e3 = {}) => ba("div", { class: Cp("cds-uploader-list-item__icon"), ...e3 }, "uploading" === Lt(i2) ? ba(Gg, { size: "xs" }) : ba(Lm, { name: g2, class: Cp({ [`cds-uploader-list-item__icon--${(null == d2 ? void 0 : d2.url) && "done" === Lt(i2) ? "link" : Lt(i2)}`]: !!Lt(i2) }) })), b2 = p2 && ba($C, { text: r2.removeFile }, { activator: ({ props: t3 }) => ba(my, { icon: e2.removeIcon, appearance: "transparent", size: "sm", onClick: () => null == m2 ? void 0 : m2(d2), ...t3 }) }), w2 = u2 && (null == d2 ? void 0 : d2.url) && "done" === Lt(i2) && ba($C, { text: r2.downloadFile }, { activator: ({ props: t3 }) => ba(my, { icon: e2.downloadIcon, appearance: "transparent", size: "sm", onClick: () => null == h2 ? void 0 : h2(d2), ...t3 }) }), _2 = ba("span", { key: "download-delete", class: "cds-uploader-list-item__actions" }, "uploading" === Lt(i2) ? `${Lt(l2)}%` : [w2, b2]), C2 = "string" == typeof (null == d2 ? void 0 : d2.linkProps) ? JSON.parse(null == d2 ? void 0 : d2.linkProps) : null == d2 ? void 0 : d2.linkProps, x2 = Cp("cds-uploader-list-item__name", { [`cds-uploader-list-item__name--${Lt(i2)}`]: !!Lt(i2) }), k2 = null != d2 && d2.url ? ba(bs, {}, [ba(ly, { key: "view", target: "_blank", rel: "noopener noreferrer", title: null == d2 ? void 0 : d2.name, class: x2, ...C2, href: null == d2 ? void 0 : d2.url, onClick: () => null == v2 ? void 0 : v2(d2) }, { default: () => null == d2 ? void 0 : d2.name }), _2]) : ba(bs, {}, [ba("span", { key: "view", title: null == d2 ? void 0 : d2.name, class: x2, onClick: () => null == v2 ? void 0 : v2(d2) }, null == d2 ? void 0 : d2.name), _2]), S2 = (null == d2 ? void 0 : d2.response) && "string" == typeof d2.response ? d2.response : (null == (n3 = null == d2 ? void 0 : d2.error) ? void 0 : n3.statusText) || (null == (o3 = null == d2 ? void 0 : d2.error) ? void 0 : o3.message) || r2.uploadError, E2 = ba("div", { class: Cp("cds-uploader-list-item__container") }, ["error" === Lt(i2) && f2 ? ba($C, { text: S2 }, { activator: ({ props: e3 }) => y2(e3) }) : y2(), Lt(s2) && k2, ba(zm, { tag: "div" }, { default: () => wo(ba("div", { class: Cp("cds-uploader-list-item__progress") }, "percent" in d2 ? [ba(cw, { absolute: true, height: 2, modelValue: null == d2 ? void 0 : d2.percent, "onUpdate:modelValue": (e3) => l2.value = e3 })] : void 0), [[Ua, "uploading" === Lt(i2)]]) })]);
    return ba("div", { class: Cp("cds-uploader-list-item") }, a3 ? a3({ originNode: E2, file: d2, fileList: c2, actions: { download: null == h2 ? void 0 : h2.bind(null, d2), preview: null == v2 ? void 0 : v2.bind(null, d2), remove: null == m2 ? void 0 : m2.bind(null, d2) } }) : E2);
  }), {};
} }), Sk = Lo({ name: "CdsUploadList", inheritAttrs: false, props: { previewFile: { type: Function, default: void 0 }, ...Ck(), ...Ev() }, setup(e2, { slots: t2 }) {
  const n2 = Dt(false), o2 = nu("upload-list");
  Zo(() => {
    n2.value = true;
  }), po(() => {
    "picture" !== e2.listType && "picture-card" !== e2.listType || (e2.items || []).forEach((t3) => {
      typeof document > "u" || typeof window > "u" || !window.FileReader || !window.File || !(t3.originFileObj instanceof File || t3.originFileObj instanceof Blob) || void 0 !== t3.thumbUrl || (t3.thumbUrl = "", e2.previewFile && e2.previewFile(t3.originFileObj).then((e3) => {
        t3.thumbUrl = e3 || "", o2.update();
      }));
    });
  });
  const r2 = (t3, n3) => {
    if (e2.onPreview)
      return null == n3 || n3.preventDefault(), e2.onPreview(t3);
  }, s2 = (e3) => {
    const t3 = document.createElement("a");
    document.body.appendChild(t3), t3.download = e3.name, t3.href = e3.url, t3.rel = "noopener noreferrer", t3.target = "_blank", t3.click(), document.body.removeChild(t3);
  }, a2 = (t3) => {
    var n3;
    null == (n3 = e2.onRemove) || n3.call(e2, t3);
  };
  return Oh(() => {
    const { items: n3 = [], locale: o3, progress: l2, listType: i2, showRemoveIcon: c2, showDownloadIcon: d2, showPreviewIcon: u2, showTooltipWithError: p2, itemRender: f2, removeIcon: h2, downloadIcon: v2 } = e2;
    return ba(ml, { tag: "div", name: "slide-transition-y", class: "cds-uploader-list" }, { default: () => n3.map((e3) => {
      const { uid: m2 } = e3;
      return ba(kk, { key: m2, locale: o3, file: e3, items: n3, progress: l2, listType: i2, showPreviewIcon: u2, showDownloadIcon: d2, showRemoveIcon: c2, showTooltipWithError: p2, itemRender: f2, onPreview: r2, onDownload: s2, onRemove: a2, removeIcon: h2, downloadIcon: v2 }, { itemRender: t2.itemRender });
    }) });
  }), { handlePreview: r2, handleDownload: s2 };
} });
function Ek(e2) {
  return e2 ? { ...e2, lastModified: e2.lastModified, lastModifiedDate: e2.lastModifiedDate, name: e2.name, size: e2.size, type: e2.type, uid: e2.uid, percent: 0, originFileObj: e2 } : null;
}
function Ik(e2, t2) {
  const n2 = [...t2], o2 = n2.findIndex(({ uid: t3 }) => t3 === e2.uid);
  return -1 === o2 ? n2.push(e2) : n2[o2] = e2, n2;
}
function Ok(e2, t2) {
  const n2 = void 0 !== e2.uid ? "uid" : "name";
  return t2.filter((t3) => t3[n2] === e2[n2])[0];
}
function Nk(e2, t2) {
  if (e2 && t2) {
    const n2 = Array.isArray(t2) ? t2 : t2.split(","), o2 = e2.name || "", r2 = e2.type || "", s2 = r2.replace(/\/.*$/, "");
    return n2.some((e3) => {
      const t3 = e3.trim();
      if (/^\*(\/\*)?$/.test(e3))
        return true;
      if ("." === t3.charAt(0)) {
        const e4 = o2.toLowerCase(), n3 = t3.toLowerCase();
        let r3 = [n3];
        return (".jpg" === n3 || ".jpeg" === n3) && (r3 = [".jpg", ".jpeg"]), r3.some((t4) => e4.endsWith(t4));
      }
      return /\/\*$/.test(t3) ? s2 === t3.replace(/\/.*$/, "") : !(r2 !== t3 && !/^\w+$/.test(t3));
    });
  }
  return true;
}
function Mk(e2, t2, n2) {
  const o2 = (e3, r2) => {
    e3.path = r2 || "", e3.isFile ? e3.file((o3) => {
      n2(o3) && (e3.fullPath && !o3.webkitRelativePath && (Object.defineProperties(o3, { webkitRelativePath: { writable: true } }), o3.webkitRelativePath = e3.fullPath.replace(/^\//, ""), Object.defineProperties(o3, { webkitRelativePath: { writable: false } })), t2([o3]));
    }) : e3.isDirectory && function(e4, t3) {
      const n3 = e4.createReader();
      let o3 = [];
      function r3() {
        n3.readEntries((e5) => {
          const n4 = Array.prototype.slice.apply(e5);
          o3 = o3.concat(n4), n4.length ? r3() : t3(o3);
        });
      }
      r3();
    }(e3, (t3) => {
      t3.forEach((t4) => {
        o2(t4, `${r2}${e3.name}/`);
      });
    });
  };
  e2.forEach((e3) => {
    o2(e3.webkitGetAsEntry());
  });
}
const Ak = +new Date();
let Dk = 0;
function Vk() {
  return `cds-uploader-${Ak}-${++Dk}`;
}
const Tk = Lo({ name: "CdsAjaxUploader", inheritAttrs: false, props: { id: { type: String, default: void 0 }, ..._k({ data: {}, headers: {}, name: "file", multiple: false, customRequest: null, withCredentials: false, beforeUpload: null, openFileDialogOnClick: true }), ...Qf(), ...th(), ...ch({ tag: "span" }) }, emits: { mouseenter: (e2) => true, mouseleave: (e2) => true, click: (e2) => true, batchStart: (e2) => true, reject: (e2) => true, start: (e2) => true, error: (e2, t2, n2) => true, success: (e2, t2, n2) => true, progress: (e2, t2) => true }, setup(e2, { slots: t2, attrs: n2, emit: o2 }) {
  const r2 = At(Vk()), s2 = xk(), a2 = {};
  let l2 = false;
  const i2 = At();
  function c2(e3) {
    if (e3) {
      const t3 = e3.uid ? e3.uid : e3;
      a2[t3] && a2[t3].abort && a2[t3].abort(), delete a2[t3];
    } else
      Object.keys(a2).forEach((e4) => {
        a2[e4] && a2[e4].abort && a2[e4].abort(), delete a2[e4];
      });
  }
  function d2(t3) {
    const n3 = [...t3], r3 = n3.map((t4) => (t4.uid = String(Vk()), async function(t5, n4) {
      const { beforeUpload: o3 } = e2;
      let r4 = t5;
      if (o3) {
        try {
          r4 = await o3(t5, n4);
        } catch {
          r4 = false;
        }
        if (false === r4)
          return { origin: t5, parsedFile: null, action: null, data: null };
      }
      const { action: s3 } = e2;
      let a3;
      a3 = "function" == typeof s3 ? await s3(t5) : s3;
      const { data: l3 } = e2;
      let i3;
      i3 = "function" == typeof l3 ? await l3(t5) : l3;
      const c3 = "object" != typeof r4 && "string" != typeof r4 || !r4 ? t5 : r4;
      let d3;
      d3 = c3 instanceof File ? c3 : new File([c3], t5.name, { type: t5.type });
      const u3 = d3;
      return u3.uid = t5.uid, { origin: t5, data: i3, parsedFile: u3, action: a3 };
    }(t4, n3)));
    Promise.all(r3).then((t4) => {
      o2("batchStart", t4.map(({ origin: e3, parsedFile: t5 }) => ({ file: e3, parsedFile: t5 }))), t4.filter((e3) => null !== e3.parsedFile).forEach((t5) => {
        !function({ data: t6, origin: n4, action: r4, parsedFile: i3 }) {
          if (!l2)
            return;
          const { name: c3, headers: d3, withCredentials: u3, method: p3, customRequest: f3, onStart: h3 } = e2, { uid: v2 } = n4, m2 = f3 || s2, g2 = { action: r4, filename: c3, data: t6, file: i3, headers: d3, withCredentials: u3, method: p3 || "post", onProgress: (e3) => {
            o2("progress", e3, i3);
          }, onSuccess: (e3, t7) => {
            o2("success", e3, i3, t7), delete a2[v2];
          }, onError: (e3, t7) => {
            o2("error", e3, t7, i3), delete a2[v2];
          } };
          o2("start", n4), a2[v2] = m2(g2);
        }(t5);
      });
    });
  }
  function u2(t3) {
    const { accept: n3, directory: o3 } = e2, { files: s3 } = t3.target, a3 = [...s3].filter((e3) => !o3 || Nk(e3, n3));
    d2(a3), r2.value = Vk();
  }
  function p2(t3) {
    const n3 = Lt(i2);
    if (!n3)
      return;
    const { onClick: o3 } = e2;
    n3.click(), o3 && o3(t3);
  }
  function f2(e3) {
    e3.key === kd.enter && p2(e3);
  }
  function h2(t3) {
    var n3, o3;
    const { multiple: r3, directory: s3, onReject: a3 } = e2;
    if (t3.preventDefault(), "dragover" !== t3.type)
      if (s3)
        Mk(Array.prototype.slice.call(null == (n3 = t3.dataTransfer) ? void 0 : n3.items), d2, (t4) => Nk(t4, e2.accept));
      else {
        const n4 = function(e3, t4) {
          const n5 = [[], []];
          return Array.isArray(e3) && e3.forEach((e4) => {
            n5[t4(e4) ? 0 : 1].push(e4);
          }), n5;
        }(Array.prototype.slice.call(null == (o3 = t3.dataTransfer) ? void 0 : o3.files), (t4) => Nk(t4, e2.accept));
        let s4 = n4[0];
        const l3 = n4[1];
        false === r3 && (s4 = s4.slice(0, 1)), d2(s4), l3.length && a3 && a3(l3);
      }
  }
  return Zo(() => {
    l2 = true;
  }), Yo(() => {
    l2 = false, c2();
  }), Oh(() => {
    var o3;
    const { tag: s3, disabled: a3, readonly: l3, id: c3, multiple: d3, accept: v2, capture: m2, directory: g2, openFileDialogOnClick: y2, onMouseenter: b2, onMouseleave: w2, ..._2 } = e2, C2 = l3 || a3 ? {} : { onClick: e2.openFileDialogOnClick ? p2 : () => {
    }, onKeydown: e2.openFileDialogOnClick ? f2 : () => {
    }, onMouseenter: e2.onMouseenter, onMouseleave: e2.onMouseleave, onDrop: h2, onDragover: h2, tabindex: "0" }, x2 = g2 ? { directory: "directory", webkitdirectory: "webkitdirectory" } : {};
    return ba(e2.tag, { role: "button", style: n2.style, class: { [Sd.disabled]: a3, [Sd.readonly]: l3, [n2.class]: !!n2.class }, ...C2 }, [ba("input", { ...du(_2, { aria: true, data: true }), id: c3, ref: i2, type: "file", key: Lt(r2), accept: v2, multiple: d3, disabled: a3, readonly: l3, style: { display: "none" }, onChange: u2, onClick: (e3) => e3.stopPropagation(), ...x2, ...null != e2.capture ? { capture: e2.capture } : {} }), null == (o3 = t2.default) ? void 0 : o3.call(t2)]);
  }), { fileInput: i2, abort: c2 };
} }), Lk = `__LIST_IGNORE_${Date.now()}__`, Pk = "CdsUploader", Fk = Lo({ name: Pk, inheritAttrs: false, props: { ..._k({ action: "", accept: "", showUploadList: true, data: {}, type: "select", listType: "text", multiple: true }), ...Nh(), ...Fh(), ...Qf(), ...th(), ...Ev(), ...jf(), ...Zv(), ...ch({ tag: "span" }) }, emits: { "update:modelValue": (e2) => true, change: (e2) => true, progress: (e2, t2) => true, success: (e2, t2) => true, error: (e2, t2, n2) => true, remove: (e2) => true, drop: (e2) => true, reject: (e2) => true, download: (e2) => true, preview: (e2) => true }, setup(e2, { attrs: t2, slots: n2, emit: o2, expose: r2 }) {
  const s2 = vh(e2, "modelValue"), { themeClasses: a2 } = Yv(e2), { id: l2, hasLabel: i2, hasDescription: c2 } = Mh(e2, n2, Pk), { uploader: d2 } = Iv(e2), { hasPrepend: u2, hasAppend: p2 } = zf(e2, n2, Pk), [f2, h2] = _v(e2.defaultFileList || [], { value: s2, postState: (e3) => {
    const t3 = Date.now();
    return (null != e3 ? e3 : []).map((e4, n3) => (!e4.uid && !Object.isFrozen(e4) && (e4.uid = `__AUTO__${t3}_${n3}__`), e4));
  } });
  At("drop");
  const v2 = At(), m2 = At(), g2 = At();
  function y2(t3, n3, r3) {
    let s3 = [...n3];
    1 === e2.maxCount ? s3 = s3.slice(-1) : e2.maxCount && (s3 = s3.slice(0, e2.maxCount)), h2(s3);
    const a3 = { file: t3, fileList: s3 };
    r3 && (a3.event = r3), o2("update:modelValue", a3.fileList), o2("change", a3);
  }
  async function b2(t3, n3) {
    const { beforeUpload: o3 } = e2;
    let r3 = t3;
    if (o3) {
      const e3 = await o3(t3, n3);
      if (false === e3)
        return false;
      if (delete t3[Lk], e3 === Lk)
        return Object.defineProperty(t3, Lk, { value: true, configurable: true }), false;
      "object" == typeof e3 && e3 && (r3 = e3);
    }
    return r3;
  }
  function w2(e3) {
    const t3 = e3.filter((e4) => !e4.file[Lk]);
    if (!t3.length)
      return;
    const n3 = t3.map((e4) => Ek(e4.file));
    let o3 = [...Lt(f2)];
    n3.forEach((e4) => {
      !e4 || (o3 = Ik(e4, o3));
    }), n3.forEach((e4, n4) => {
      if (!e4)
        return;
      let r3 = e4;
      if (t3[n4].parsedFile)
        e4.status = "uploading";
      else {
        const { originFileObj: t4 } = e4;
        let n5;
        try {
          n5 = new File([t4], t4.name, { type: t4.type });
        } catch {
          n5 = new Blob([t4], { type: t4.type }), n5.name = t4.name, n5.lastModifiedDate = new Date(), n5.lastModified = new Date().getTime();
        }
        n5.uid = e4.uid, r3 = n5;
      }
      y2(r3, o3);
    });
  }
  function _2(e3, t3, n3) {
    try {
      "string" == typeof e3 && (e3 = JSON.parse(e3));
    } catch {
    }
    if (!Ok(t3, Lt(f2)))
      return;
    const r3 = Ek(t3);
    if (!r3)
      return;
    r3.status = "done", r3.percent = 100, r3.response = e3, r3.xhr = n3;
    y2(r3, Ik(r3, Lt(f2))), o2("success", e3, r3);
  }
  function C2(e3, t3) {
    if (!t3 || !Ok(t3, Lt(f2)))
      return;
    const n3 = Ek(t3);
    if (!n3)
      return;
    n3.status = "uploading", n3.percent = e3.percent;
    y2(n3, Ik(n3, Lt(f2)), e3), o2("progress", e3, n3);
  }
  function x2(e3, t3, n3) {
    if (!n3 || !Ok(n3, Lt(f2)))
      return;
    const r3 = Ek(n3);
    if (!r3)
      return;
    r3.error = e3, r3.response = t3, r3.status = "error";
    y2(r3, Ik(r3, Lt(f2))), o2("error", e3, t3, r3);
  }
  function k2(e3) {
    o2("download", e3);
  }
  function S2(e3) {
    let t3;
    Promise.resolve(o2("remove", e3)).then((n3) => {
      var o3, r3;
      if (false === n3)
        return;
      const s3 = function(e4, t4) {
        const n4 = void 0 !== e4.uid ? "uid" : "name", o4 = t4.filter((t5) => t5[n4] !== e4[n4]);
        return o4.length === t4.length ? null : o4;
      }(e3, Lt(f2));
      s3 && (t3 = { ...e3, status: "removed" }, null == (o3 = Lt(f2)) || o3.forEach((e4) => {
        const n4 = void 0 !== t3.uid ? "uid" : "name";
        e4[n4] === t3[n4] && !Object.isFrozen(e4) && (e4.status = "removed");
      }), null == (r3 = Lt(g2)) || r3.abort(t3), y2(t3, s3));
    });
  }
  function E2(e3) {
    o2("preview", e3);
  }
  return Oh(() => {
    const { class: o3, style: r3, ...h3 } = t2, y3 = { ...e2, ...h3, id: Lt(l2), beforeUpload: b2, onBatchStart: w2, onError: x2, onProgress: C2, onSuccess: _2, onChange: void 0 }, I2 = { appearance: "secondary", text: d2.uploadFile, prependIcon: e2.uploadIcon, disabled: e2.disabled, readonly: e2.readonly }, O2 = Lt(u2) ? { prepend: () => {
      var e3;
      return null == (e3 = n2.prepend) ? void 0 : e3.call(n2);
    } } : {}, N2 = Lt(p2) ? { append: () => {
      var e3;
      return null == (e3 = n2.append) ? void 0 : e3.call(n2);
    } } : {};
    return ba("div", { ref: v2, class: Cp("cds-uploader-field", Lt(a2)) }, [Lt(i2) && ba(Jm, { for: Lt(l2), text: e2.label }, { default: n2.label }), Lt(c2) && ba(Qm, { text: e2.description }, { default: n2.description }), ba(Gm, { ...t2, ref: m2, id: Lt(l2), modelValue: s2, disabled: e2.disabled, readonly: e2.readonly, errorMessages: e2.errorMessages, messages: e2.messages, maxErrors: e2.maxErrors, name: e2.name, rules: e2.rules, validateOn: e2.validateOn, validationValue: e2.validationValue, hideMessages: e2.hideMessages, persistentMessages: e2.persistentMessages, prependIcon: e2.prependIcon, appendIcon: e2.appendIcon, class: Cp("cds-uploader", t2.class), "onUpdate:modelValue": (e3) => {
      s2.value = e3;
    } }, { ...O2, default: () => [ba(Tk, { ...y3, ref: g2 }, { default: () => {
      var e3;
      return n2.default ? null == (e3 = n2.default) ? void 0 : e3.call(n2, { props: I2 }) : ba(my, I2);
    } })], messages: () => {
      var e3;
      return null == (e3 = n2.messages) ? void 0 : e3.call(n2);
    }, ...N2 }), (() => {
      const { listType: t3, locale: o4, disabled: r4, removeIcon: s3, downloadIcon: a3, isImageUrl: l3, showUploadList: i3, itemRender: c3, showTooltipWithError: d3 } = e2, { showDownloadIcon: u3, showRemoveIcon: p3 } = "boolean" == typeof i3 ? {} : i3;
      return i3 && ba(Sk, { listType: t3, items: Lt(f2), itemRender: c3, showRemoveIcon: !r4 && p3, showDownloadIcon: u3, removeIcon: s3, downloadIcon: a3, isImageUrl: l3, showUploadList: i3, showTooltipWithError: d3, locale: o4, onPreview: E2, onDownload: k2, onRemove: S2 }, { itemRender: n2.itemRender });
    })()]);
  }), r2({ onBatchStart: w2, onSuccess: _2, onProgress: C2, onError: x2, fileList: f2, uploadRef: g2 }), {};
} }), Rk = Object.freeze(Object.defineProperty({ __proto__: null, CdsAutocomplete: oy, CdsBadge: sy, CdsBreadcrumbs: cy, CdsBreadcrumbsItem: iy, CdsButton: my, CdsCheckbox: dg, CdsCheckboxGroup: ag, CdsCombobox: Ey, CdsContainer: My, CdsContainerItem: Ay, CdsContentSwitcher: Hy, CdsContentSwitcherItem: jy, CdsDatePicker: Yy, CdsDrawer: sb, CdsDropdown: db, CdsEmptyState: bb, CdsExpandablePanel: Nb, CdsExpandablePanelContent: Eb, CdsExpandablePanels: Sb, CdsExpandablePanelTitle: Ob, CdsFooter: Lb, CdsForm: Pb, CdsCol: Fb, CdsGrid: Rb, CdsRow: $b, CdsHeader: Hb, CdsIcon: Lm, CdsInput: Gm, CdsInputCounter: eg, CdsInputDescription: Qm, CdsInputLabel: Jm, CdsInputMessages: Wm, CdsLayout: jb, CdsLink: ly, CdsList: gg, CdsListGroup: Ag, CdsListItem: Sg, CdsLoader: Gg, CdsMain: zb, CdsMenu: zg, CdsModal: Ub, CdsNotification: Gb, CdsNotificationAlert: sw, CdsOverlay: Kg, CdsPage: aw, CdsPagination: lw, CdsProgress: cw, CdsRadio: bw, CdsRadioButton: vw, CdsSelect: Nw, CdsSidebar: Fw, CdsSkeletonLoader: Kw, CdsSlideGroup: Ly, CdsSlideGroupItem: Py, CdsStages: Jw, CdsStatus: n_, CdsSteps: a_, CdsStepsItem: f_, CdsTable: kx, CdsTableColumn: Sx, CdsTableColumnGroup: Ex, CdsTab: By, CdsTabs: $y, CdsTag: _y, CdsTextArea: Ax, CdsTextInput: ty, CdsTimePicker: Tx, CdsToggle: Px, CdsToolbar: Rx, CdsToolbarTitle: $x, CdsTooltip: $C, CdsTree: yk, CdsTreeDirectory: wk, CdsTreeNode: qx, CdsUploader: Fk, makeTableProps: m_, useLazyKVMap: g_, SELECTION_COLUMN: oC, SELECTION_ALL: rC, SELECTION_INVERT: sC, SELECTION_NONE: aC, EXPAND_COLUMN: lC, useSelection: cC }, Symbol.toStringTag, { value: "Module" })), $k = Lo({ name: "IconAdd" }), Bk = (e2, t2) => {
  const n2 = e2.__vccOpts || e2;
  for (const [e3, o2] of t2)
    n2[e3] = o2;
  return n2;
}, Hk = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, jk = [Ls("g", { id: "add", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 1a6 6 0 1 0 0 12A6 6 0 0 0 8 2Zm.5 2v3.5H12v1H8.5V12h-1V8.499L4 8.5v-1l3.5-.001V4h1Z" })], -1)];
const Kk = Bk($k, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", Hk, jk);
}]]), zk = Lo({ name: "IconAdministrator" }), Zk = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, Uk = [Ls("path", { "fill-rule": "evenodd", d: "M7 8a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Zm0 1a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM10.964 13.5c.036.307.036.5.036.5v.5H3V14s0-.193.036-.5c.136-1.172.793-4 3.964-4 3.17 0 3.828 2.828 3.964 4Zm-6.92 0c.048-.355.152-.853.37-1.344.202-.454.484-.86.873-1.153.378-.283.914-.503 1.713-.503s1.335.22 1.713.503c.39.293.671.7.873 1.153a5.045 5.045 0 0 1 .37 1.344H4.044ZM11.5 4.388a.888.888 0 1 1 0-1.775.888.888 0 0 1 0 1.775Zm1.451-.101.294.382a.123.123 0 0 1 0 .15 2.2 2.2 0 0 1-.426.427.123.123 0 0 1-.15 0l-.382-.295c-.1.055-.207.1-.317.132l-.062.478a.123.123 0 0 1-.105.106 2.16 2.16 0 0 1-.605 0 .123.123 0 0 1-.105-.106l-.062-.478a1.64 1.64 0 0 1-.318-.131l-.382.294a.123.123 0 0 1-.15 0 2.218 2.218 0 0 1-.427-.427.123.123 0 0 1 .001-.149l.294-.382a1.64 1.64 0 0 1-.132-.318l-.478-.063a.123.123 0 0 1-.106-.105 2.214 2.214 0 0 1 0-.604.123.123 0 0 1 .106-.105l.478-.062a1.64 1.64 0 0 1 .132-.318l-.294-.383a.123.123 0 0 1 0-.149c.121-.16.268-.307.427-.427a.123.123 0 0 1 .149 0l.382.295c.101-.055.207-.1.318-.132l.062-.478a.123.123 0 0 1 .105-.105c.205-.029.4-.029.605 0 .054.007.097.05.105.106l.062.478c.11.032.217.076.317.131l.382-.294a.123.123 0 0 1 .15 0 2.2 2.2 0 0 1 .427.427.123.123 0 0 1 0 .15l-.295.382c.055.1.099.206.132.317l.477.062c.055.007.099.05.106.105.029.206.028.4 0 .604a.123.123 0 0 1-.106.105l-.477.063c-.033.11-.077.216-.132.317Z", "clip-rule": "evenodd" }, null, -1)];
const Wk = Bk(zk, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", Zk, Uk);
}]]), Yk = Lo({ name: "IconAlertFill" }), Gk = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, qk = [Ls("g", { id: "CDS-Visual-language/Icons/16/alert-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1ZM7 3h2v6H7V3Zm1 7.25a1 1 0 1 1 0 2 1 1 0 0 1 0-2Z" })], -1)];
const Xk = Bk(Yk, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", Gk, qk);
}]]), Jk = Lo({ name: "IconAlert" }), Qk = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, eS = [Ls("g", { id: "alert", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 13A6 6 0 1 0 8 2a6 6 0 0 0 0 12ZM7.5 4h1v5.5h-1V4Zm.5 6.5A.75.75 0 1 1 8 12a.75.75 0 0 1 0-1.5Z" })], -1)];
const tS = Bk(Jk, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", Qk, eS);
}]]), nS = Lo({ name: "IconAnalog" }), oS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, rS = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/analog", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "M3.5 3.001v5.006l.007.144A1.5 1.5 0 0 0 4.85 9.5l.144.007 6.629.023-.002-.031L9.33 7.207l.707-.707 3.536 3.536-3.536 3.535-.707-.707 2.332-2.334-6.67-.023-.165-.006a2.5 2.5 0 0 1-2.322-2.33L2.5 8.007V3.001h1Z" })], -1)];
const sS = Bk(nS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", oS, rS);
}]]), aS = Lo({ name: "IconArrowDown" }), lS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, iS = [Ls("g", { id: "arrow-down", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path-Copy", d: "m8 1 5 5.257-.707.743L8.5 3.012V15h-1V3.012L3.707 7 3 6.257z", transform: "rotate(-180 8 8)" })], -1)];
const cS = Bk(aS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", lS, iS);
}]]), dS = Lo({ name: "IconArrowLeftBottom" }), uS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, pS = [Ls("g", { id: "CDS-Visual-language/Icons/16/navigation/arrow-left-bottom", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "m12.743 2.55.707.707L4.707 12 10 12v1H3V6h1v5.292z" })], -1)];
const fS = Bk(dS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", uS, pS);
}]]), hS = Lo({ name: "IconArrowLeftTop" }), vS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, mS = [Ls("g", { id: "CDS-Visual-language/Icons/16/navigation/arrow-left-top", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "m12.743 13 .707-.707L4.707 3.55H10v-1H3v7h1V4.257z" })], -1)];
const gS = Bk(hS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", vS, mS);
}]]), yS = Lo({ name: "IconArrowLeft" }), bS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, wS = [Ls("g", { id: "arrow-left", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path-Copy", d: "m8 1 5 5.257-.707.743L8.5 3.012V15h-1V3.012L3.707 7 3 6.257z", transform: "rotate(-90 8 8)" })], -1)];
const _S = Bk(yS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", bS, wS);
}]]), CS = Lo({ name: "IconArrowRightBottom" }), xS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, kS = [Ls("g", { id: "CDS-Visual-language/Icons/16/navigation/arrow-right-bottom", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "M3.707 2.55 3 3.257 11.743 12 6.45 12v1h7V6h-1v5.292z" })], -1)];
const SS = Bk(CS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", xS, kS);
}]]), ES = Lo({ name: "IconArrowRightTop" }), IS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, OS = [Ls("g", { id: "CDS-Visual-language/Icons/16/navigation/arrow-right-top", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "M3.707 13 3 12.293l8.743-8.742H6.45v-1h7v7h-1V4.257z" })], -1)];
const NS = Bk(ES, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", IS, OS);
}]]), MS = Lo({ name: "IconArrowRight" }), AS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, DS = [Ls("g", { id: "arrow-right", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path-Copy-2", d: "m8 1 5 5.257-.707.743L8.5 3.012V15h-1V3.012L3.707 7 3 6.257z", transform: "rotate(90 8 8)" })], -1)];
const VS = Bk(MS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", AS, DS);
}]]), TS = Lo({ name: "IconArrowUp" }), LS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, PS = [Ls("g", { id: "arrow-up", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "m8 1 5 5.257-.707.743L8.5 3.012V15h-1V3.012L3.707 7 3 6.257z" })], -1)];
const FS = Bk(TS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", LS, PS);
}]]), RS = Lo({ name: "IconArrowsVertical" }), $S = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, BS = [Ls("g", { id: "arrows-vertical", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "m4.5 2 3 3.2-.75.8L5 4.134V14H4V4.133L2.25 6l-.75-.8 3-3.2ZM12 2l-.001 9.867L13.75 10l.75.8-3 3.2-3-3.2.75-.8 1.749 1.865L11 2h1Z" })], -1)];
const HS = Bk(RS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", $S, BS);
}]]), jS = Lo({ name: "IconCalendar" }), KS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, zS = [Ls("g", { id: "calendar", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M11 2h1a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h1v-.5a.5.5 0 0 1 1 0V2h4v-.5a.5.5 0 1 1 1 0V2Zm-1 1H6v.5a.5.5 0 0 1-1 0V3H4a1 1 0 0 0-1 1v1h10v1H3v6a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1h-1v.5a.5.5 0 1 1-1 0V3Z" })], -1)];
const ZS = Bk(jS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", KS, zS);
}]]), US = Lo({ name: "IconCardBank" }), WS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, YS = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/card-bank", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M13.5 3.5a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1h-11a1 1 0 0 1-1-1v-7a1 1 0 0 1 1-1h11Zm0 3h-11v5h11v-5Zm-5 3.5v1h-5v-1h5Zm5-5.5h-11v1h11v-1Z" })], -1)];
const GS = Bk(US, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", WS, YS);
}]]), qS = Lo({ name: "IconCategory" }), XS = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, JS = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/category", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M12 9H8.5v2h1v3h-3v-3h1V9H4v2h1v3H2v-3h1V8h4.5V6H6V2h4v4H8.5v2H13v3h1v3h-3v-3h1V9Zm-3.5 3h-1v1h1v-1Zm4.5 0h-1v1h1v-1Zm-9 0H3v1h1v-1Zm5-9H7v2h2V3Z" })], -1)];
const QS = Bk(qS, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", XS, JS);
}]]), eE = Lo({ name: "IconCheckFill" }), tE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, nE = [Ls("g", { id: "CDS-Visual-language/Icons/16/check-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm2.927 4.58L7.318 9.229 5.495 7.7l-.973 1.223 2.87 2.56 4.584-4.827-1.049-1.075Z" })], -1)];
const oE = Bk(eE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", tE, nE);
}]]), rE = Lo({ name: "IconCheck" }), sE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, aE = [Ls("g", { id: "check", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 13A6 6 0 1 0 8 2a6 6 0 0 0 0 12ZM5.188 8.176l2.147 1.916 3.916-4.124.725.688-4.583 4.827-2.87-2.56.665-.747Z" })], -1)];
const lE = Bk(rE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", sE, aE);
}]]), iE = Lo({ name: "IconCheckboxFill" }), cE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, dE = [Ls("g", { id: "checkbox-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M13 1H2.977C1.853 1 1 2.001 1 2.985v10.03C1 14.213 1.944 15 2.977 15H13c1.114 0 2-.89 2-1.986V2.984C15 2.097 14.27 1 13 1Zm-1.966 4.15.884.884-4.884 4.884L4.15 8.034l.884-.884 2 2 4-4Z" })], -1)];
const uE = Bk(iE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", cE, dE);
}]]), pE = Lo({ name: "IconCheckbox" }), fE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, hE = [Ls("g", { id: "checkbox", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10Zm0 1H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1Zm-2 3.293.707.707L7 10.707 4.293 8 5 7.293l2 2 4-4Z" })], -1)];
const vE = Bk(pE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", fE, hE);
}]]), mE = Lo({ name: "IconChevronDown" }), gE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, yE = [Ls("g", { id: "Chevron-down", "stroke-width": "1" }, [Ls("mask", { id: "mask-2" }, [Ls("path", { d: "M3 5.743 3.707 5 8 9.513 12.293 5l.707.743L8 11z" })]), Ls("path", { d: "M3 5.743 3.707 5 8 9.513 12.293 5l.707.743L8 11z" })], -1)];
const bE = Bk(mE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", gE, yE);
}]]), wE = Lo({ name: "IconChevronLeft" }), _E = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, CE = [Ls("g", { id: "Chevron-left", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "arrow", d: "m12.275 11 .725-.699L7.998 5 3 10.301l.725.699 4.273-4.533z", transform: "rotate(-90 8 8)" })], -1)];
const xE = Bk(wE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", _E, CE);
}]]), kE = Lo({ name: "IconChevronRight" }), SE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, EE = [Ls("g", { id: "Chevron-right", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "arrow", d: "m12.275 11 .725-.699L7.998 5 3 10.301l.725.699 4.273-4.533z", transform: "rotate(90 8 8)" })], -1)];
const IE = Bk(kE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", SE, EE);
}]]), OE = Lo({ name: "IconChevronUp" }), NE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, ME = [Ls("g", { id: "Chevron-up", "stroke-width": "1" }, [Ls("mask", { id: "mask-2" }, [Ls("path", { d: "M3 10.257 8 5l5 5.257-.707.743L8 6.487 3.707 11z" })]), Ls("path", { d: "M3 10.257 8 5l5 5.257-.707.743L8 6.487 3.707 11z" })], -1)];
const AE = Bk(OE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", NE, ME);
}]]), DE = Lo({ name: "IconChocholateMenu" }), VE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, TE = [Ls("g", { id: "chocholate-menu", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M14 12v2h-2v-2h2Zm-5 0v2H7v-2h2Zm-5 0v2H2v-2h2Zm0-5v2H2V7h2Zm10 0v2h-2V7h2ZM9 7v2H7V7h2ZM4 2v2H2V2h2Zm10 0v2h-2V2h2ZM9 2v2H7V2h2Z" })], -1)];
const LE = Bk(DE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", VE, TE);
}]]), PE = Lo({ name: "IconChocolateMenu" }), FE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, RE = [Ls("g", { id: "chocholate-menu", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M14 12v2h-2v-2h2Zm-5 0v2H7v-2h2Zm-5 0v2H2v-2h2Zm0-5v2H2V7h2Zm10 0v2h-2V7h2ZM9 7v2H7V7h2ZM4 2v2H2V2h2Zm10 0v2h-2V2h2ZM9 2v2H7V2h2Z" })], -1)];
const $E = Bk(PE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", FE, RE);
}]]), BE = Lo({ name: "IconClock" }), HE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, jE = [Ls("g", { id: "CDS-Visual-language/Icons/16/time/\u0441lock", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 1a6 6 0 1 0 0 12A6 6 0 0 0 8 2Zm0 2v4h4v1H7V4h1Z" })], -1)];
const KE = Bk(BE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", HE, jE);
}]]), zE = Lo({ name: "IconCollapse" }), ZE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, UE = [Ls("path", { "fill-rule": "evenodd", d: "M1 3v10h1V3H1Zm2 5.017.71.705L6 11l.705-.71L4.92 8.516 15.001 8.5l-.002-1-10.09.015 1.8-1.81L6 5 3.705 7.308 3 8.018Z", "clip-rule": "evenodd" }, null, -1)];
const WE = Bk(zE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", ZE, UE);
}]]), YE = Lo({ name: "IconComment" }), GE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, qE = [Ls("g", { id: "comment", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M14 2a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1h-1.001L13 13.793a.5.5 0 0 1-.095.293l-.051.06a.5.5 0 0 1-.638.058l-.07-.058L9 11H2a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h12Zm0 1H2v7h7.413L12 12.59 11.999 10H14V3ZM9 7v1H4V7h5Zm3-2v1H4V5h8Z" })], -1)];
const XE = Bk(YE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", GE, qE);
}]]), JE = Lo({ name: "IconCompany" }), QE = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, eI = [Ls("g", { id: "company", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "company", d: "M1 15v-1h1V2h7v2h5v10h1v1H1ZM8 3H3v11h5V3Zm5 2H9v1h3v1H9v1h3v1H9v1h3v1H9v1h3v1H9v1h4V5Zm-8 7v1H4v-1h1Zm2 0v1H6v-1h1Zm-2-2v1H4v-1h1Zm2 0v1H6v-1h1ZM5 8v1H4V8h1Zm2 0v1H6V8h1ZM5 6v1H4V6h1Zm2 0v1H6V6h1ZM4 4h1v1H4V4Zm2 0h1v1H6V4Z" })], -1)];
const tI = Bk(JE, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", QE, eI);
}]]), nI = Lo({ name: "IconCompare" }), oI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, rI = [Ls("g", { id: "CDS-Visual-language/Icons/16/operations/compare", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M4 9v5H3V9h1Zm3-7v12H6V2h1Zm3 2v10H9V4h1Zm3 3v7h-1V7h1Z" })], -1)];
const sI = Bk(nI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", oI, rI);
}]]), aI = Lo({ name: "IconCopy" }), lI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, iI = [Ls("g", { id: "\u0441opy", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M13.75 5c.69 0 1.25.56 1.25 1.25v7.5c0 .69-.56 1.25-1.25 1.25h-7.5C5.56 15 5 14.44 5 13.75v-7.5C5 5.56 5.56 5 6.25 5h7.5ZM14 6H6v8h8V6ZM9.75 1c.69 0 1.25.56 1.25 1.25V4h-1V2H2v8h2v1H2.25C1.56 11 1 10.44 1 9.75v-7.5C1 1.56 1.56 1 2.25 1h7.5Z" })], -1)];
const cI = Bk(aI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", lI, iI);
}]]), dI = Lo({ name: "IconDataCards" }), uI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, pI = [Ls("g", { id: "CDS-Visual-language/Icons/16/operations/data-cards", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M15 9v4H1V9h14Zm-1 1H2v2h12v-2Zm1-7v4H1V3h14Zm-1 1H2v2h12V4Z" })], -1)];
const fI = Bk(dI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", uI, pI);
}]]), hI = Lo({ name: "IconDelete" }), vI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, mI = [Ls("g", { id: "delete", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M10 2H6v1h4V2Zm2 2H4v10h8V4Zm-2 1v8H9V5h1ZM7 5v8H6V5h1Zm7-2v1h-1v11H3V4H2V3h3V1h6v2h3Z" })], -1)];
const gI = Bk(hI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", vI, mI);
}]]), yI = Lo({ name: "IconDirectoryOpen" }), bI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, wI = [Ls("g", { "clip-path": "url(#clip0_2734_854)" }, [Ls("path", { "fill-rule": "evenodd", d: "M1.034 2.548a.5.5 0 0 1 .41-.492l.09-.008h2.96a.5.5 0 0 1 .392.188l.05.077.928 1.739h8.62a.5.5 0 0 1 .491.41l.008.09V7h.517a.5.5 0 0 1 .493.582l-1 6A.5.5 0 0 1 14.5 14H2l-.004-.003h-.462a.5.5 0 0 1-.492-.411l-.008-.09V2.548ZM3 13v-.004h-.859L3.391 8H14.91l-.834 5H3Zm10.983-7.948L14 7H3a.5.5 0 0 0-.485.379l-.482 1.928V3.047h2.161L5.25 5.023v.03l8.733-.001Z", "clip-rule": "evenodd" })], -1), Ls("defs", null, [Ls("clipPath", { id: "clip0_2734_854" }, [Ls("path", { d: "M0 0h16v16H0z" })])], -1)];
const _I = Bk(yI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", bI, wI);
}]]), CI = Lo({ name: "IconDirectory" }), xI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, kI = [Ls("g", { id: "CDS-Visual-language/Icons/16/file/directory", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M1.034 2.548a.5.5 0 0 1 .41-.492l.09-.008h2.96a.5.5 0 0 1 .392.188l.05.077.928 1.739h8.62a.5.5 0 0 1 .491.41l.008.09v9a.5.5 0 0 1-.41.492l-.09.008H1.533a.5.5 0 0 1-.491-.41l-.008-.09V2.548Zm.999 10.504h11.95v-8H5.249v-.029L4.195 3.048H2.033v10.004Z" })], -1)];
const SI = Bk(CI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", xI, kI);
}]]), EI = Lo({ name: "IconDocDownload" }), II = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, OI = [Ls("g", { id: "doc-download", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "doc-download", d: "M11 8v5.156l2.364-2.363.707.707-3.535 3.536L7 11.5l.707-.707L10 13.086V8h1ZM9 1l3 3v3h-1V5H8V2H3v12h3v1H2V1h7Zm0 1.5v1.499L10.5 4 9 2.5Z" })], -1)];
const NI = Bk(EI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", II, OI);
}]]), MI = Lo({ name: "IconDoc" }), AI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, DI = [Ls("g", { id: "CDS-Visual-language/Icons/16/file/doc", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M13 15H3V1h7.018L13 3.991V15ZM9 2H4v12h8V5H9V2Zm1 .397V4h1.597L10 2.397Z" })], -1)];
const VI = Bk(MI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", AI, DI);
}]]), TI = Lo({ name: "IconDownload" }), LI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, PI = [Ls("g", { id: "download", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Download", d: "m2 12-.001 2H14v-2h1v3H1v-3h1ZM8.5 1v8.156l2.364-2.363.707.707-3.535 3.536L4.5 7.5l.707-.707L7.5 9.086V1h1Z" })], -1)];
const FI = Bk(TI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", LI, PI);
}]]), RI = Lo({ name: "IconDraggable" }), $I = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, BI = [Ls("g", { id: "CDS-Visual-language/Icons/16/navigation/draggable", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M7 11v2H5v-2h2Zm4 0v2H9v-2h2ZM7 7v2H5V7h2Zm4 0v2H9V7h2ZM7 3v2H5V3h2Zm4 0v2H9V3h2Z" })], -1)];
const HI = Bk(RI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", $I, BI);
}]]), jI = Lo({ name: "IconEdit" }), KI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, zI = [Ls("g", { id: "edit", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "m12.88 1.364 1.793 1.793a1 1 0 0 1 .083 1.32l-.083.094L4.207 14.96H1v-3.206L11.466 1.364c.33-.33.834-.38 1.218-.153l.101.07.095.083ZM3.25 10.92 2 12.169v1.793h1.793l1.252-1.249-1.794-1.793Zm10.228-7.717-8.272 8.258.546.546 8.284-8.246-.558-.558Zm-1.236-1.235L3.96 10.215l.539.539 8.272-8.259-.528-.527Z" })], -1)];
const ZI = Bk(jI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", KI, zI);
}]]), UI = Lo({ name: "IconEnter" }), WI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, YI = [Ls("g", { id: "enter", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "enter", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 1a6 6 0 1 0 0 12A6 6 0 0 0 8 2Zm1 3.172L11.828 8 9 10.828l-.707-.707L9.914 8.5H4v-1h5.914L8.293 5.879 9 5.172Z" })], -1)];
const GI = Bk(UI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", WI, YI);
}]]), qI = Lo({ name: "IconErrorFill" }), XI = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, JI = [Ls("g", { id: "CDS-Visual-language/Icons/16/error-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1ZM5.172 3.757 3.757 5.172l7.071 7.07 1.415-1.414-7.071-7.07Z" })], -1)];
const QI = Bk(qI, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", XI, JI);
}]]), eO = Lo({ name: "IconError" }), tO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, nO = [Ls("g", { id: "error", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M1 8c0-3.85 3.15-7 7-7s7 3.15 7 7-3.15 7-7 7-7-3.15-7-7Zm11.55 3.85C13.5 10.8 14 9.4 14 8c0-3.3-2.7-6-6-6-1.4 0-2.8.5-3.85 1.45l8.4 8.4Zm-8.45.75C5.2 13.5 6.6 14 8 14s2.8-.5 3.85-1.4L3.4 4.15c-2.1 2.5-1.8 6.3.7 8.45Z" })], -1)];
const oO = Bk(eO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", tO, nO);
}]]), rO = Lo({ name: "IconExpand" }), sO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, aO = [Ls("path", { "fill-rule": "evenodd", d: "M2 13V3H1v10h1Zm13-5.017-.71-.705L12 5l-.705.71 1.786 1.775L2.999 7.5l.002 1 10.09-.015-1.8 1.81L12 11l2.295-2.308.705-.71Z", "clip-rule": "evenodd" }, null, -1)];
const lO = Bk(rO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", sO, aO);
}]]), iO = Lo({ name: "IconExternal" }), cO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, dO = [Ls("g", { id: "external", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M7 2v1H3v10h10V9h1v4.143a.857.857 0 0 1-.857.857H2.857A.857.857 0 0 1 2 13.143V2.857C2 2.384 2.384 2 2.857 2H7Zm8-1v6.116h-1.036V2.767L7.732 9 7 8.268l6.232-6.233H8.884V1H15Z" })], -1)];
const uO = Bk(iO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", cO, dO);
}]]), pO = Lo({ name: "IconFacebookFill" }), fO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, hO = [Ls("g", { id: "CDS-Visual-language/Icons/16/social/facebook-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M3 2a1 1 0 0 0-.993.883L2 13a1 1 0 0 0 .883.993l3.99.007V9.955H4.96V7.842h1.914V6.098c-.036-.914.297-1.667 1-2.256.702-.589 1.875-.743 3.52-.462v1.83l-.828.016a7.33 7.33 0 0 0-.773.09c-.12.04-.321.132-.454.3-.139.176-.19.447-.202.622l.024 1.55h2.233l-.482 2.167H9.134L9.158 14H13a1 1 0 0 0 .993-.883L14 3a1 1 0 0 0-.883-.993L3 2Z" })], -1)];
const vO = Bk(pO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", fO, hO);
}]]), mO = Lo({ name: "IconFacebook" }), gO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, yO = [Ls("g", { id: "facebook", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M13 1a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h10ZM3 2a1 1 0 0 0-.993.883L2 13a1 1 0 0 0 .883.993l3.99.007V9.955H4.96V7.842h1.914V6.098c-.036-.914.297-1.667 1-2.256.702-.589 1.875-.743 3.52-.462v1.83l-.828.016a7.33 7.33 0 0 0-.773.09c-.12.04-.321.132-.454.3-.139.176-.19.447-.202.622l.024 1.55h2.233l-.482 2.167H9.134L9.158 14H13a1 1 0 0 0 .993-.883L14 3a1 1 0 0 0-.883-.993L3 2Z" })], -1)];
const bO = Bk(mO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", gO, yO);
}]]), wO = Lo({ name: "IconFilters" }), _O = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, CO = [Ls("g", { id: "CDS-Visual-language/Icons/16/operations/filter", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M5 11a2 2 0 0 1 1.937 1.5H15v1l-8.063.001a2 2 0 0 1-3.874 0L1 13.5v-1h2.063A2 2 0 0 1 5 11Zm0 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm6-6a2 2 0 0 1 1.937 1.5H15v1l-2.063.001a2 2 0 0 1-3.874 0L1 8.5v-1h8.063A2 2 0 0 1 11 6Zm0 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2ZM5 1a2 2 0 0 1 1.937 1.5H15v1l-8.063.001a2 2 0 0 1-3.874 0L1 3.5v-1h2.063A2 2 0 0 1 5 1Zm0 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2Z" })], -1)];
const xO = Bk(wO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", _O, CO);
}]]), kO = Lo({ name: "IconFlag" }), SO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, EO = [Ls("g", { id: "CDS-Visual-language/Icons/16/toggle/flag", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", "fill-rule": "nonzero", d: "m13.5 2-1.865 3.5L13.5 9h-10v5h-1V2z" })], -1)];
const IO = Bk(kO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", SO, EO);
}]]), OO = Lo({ name: "IconFunnel" }), NO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, MO = [Ls("g", { id: "funnel", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Rectangle", "fill-rule": "nonzero", d: "M14.005 2v2.12a1 1 0 0 1-.248.659l-3.74 4.263v4.966h-4V9.042L2.251 4.78A1 1 0 0 1 2 4.117V2h12.005Zm-1 1H3v1.117L6.767 8.38l.25.284v4.344h1.999V8.666l.249-.283 3.74-4.264V3Z" })], -1)];
const AO = Bk(OO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", NO, MO);
}]]), DO = Lo({ name: "IconGlobe" }), VO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, TO = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/workspace/globe", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a6.992 6.992 0 0 1 7 7 7 7 0 1 1-7-7ZM4.85 11.344c-.523.168-.772.403-.95.641l-.111.158-.052.079A5.979 5.979 0 0 0 8 14c1.12 0 2.167-.307 3.064-.84l.054.049c-.677-.65-1.486-.926-2.873-1.061l-.285-.026-.148-.015-.151-.026-.168-.037-.311-.086-.265-.082-1.009-.322-.313-.09c-.355-.125-.603-.165-.745-.12ZM8 2a6.034 6.034 0 0 0-1.437.173c.468.85.685 1.718.417 2.1-.28.4-.565.75-.913 1.044-.675.57-1.41.662-1.872-.104-.243-.404-.25-.798-.12-1.489l.062-.315A5.987 5.987 0 0 0 2 8c0 1.283.403 2.473 1.09 3.448l-.027.03c.309-.366.745-.705 1.31-1.012.342-.186.721-.176 1.398.006l.192.053.397.121.532.172.658.205.175.047.141.031.12.019.339.03c1.661.162 2.688.528 3.566 1.415a5.986 5.986 0 0 0 1.85-2.818l-.115-.303-.128-.304-.118-.251a4.786 4.786 0 0 0-.053-.105c-.064-.151-.109-.227-.134-.227-.019 0-.039.022-.06.066l-.175.382-.104.244-.198.485-.152.35c-.24.523-.443.787-.805.83l-.114.007-.583.01c-1.338.013-1.924-.298-2.352-1.6l-.09-.286c-.07-.21-.119-.272-.181-.286l-.033-.003h-.05l-.047.005-.09.022-.09.031-.698.279c-.677.276-1.186.41-1.774.41-.841 0-1.204-.689-.88-1.428.25-.57.543-.865 1.35-1.255l.262-.118c.282-.135.668-.375 1.042-.974.056-.109.1-.229.116-.374l.006-.114c.063-1.791.99-2.773 2.609-2.663l.183.016.427.055A5.956 5.956 0 0 0 8 2Zm.5 3.388c-.021.281-.124.482-.195.6l-.056.087c-.452.723-.755 1-1.08 1.192l-.166.09-.18.09-.177.09-.213.125a5.56 5.56 0 0 0-.108.068l-.213.144c-.343.247-.61.51-.494.628.1.102.347.05.614-.041l.231-.086.326-.127.822-.331.192-.066c.199-.062.372-.095.543-.095.592 0 .99.385 1.205 1.105.241.812.403 1.029 1.04 1.066l.135.006h.273c.273 0 .435-.018.484-.053a.208.208 0 0 0 .072-.102l.052-.138.048-.108.367-.884c.418-.97.697-1.285 1.333-1.135.231.055.457.189.644.404L14 8a5.98 5.98 0 0 0-1.67-4.153l-.936-.102-.693-.084-.538-.076c-1.17-.134-1.54.208-1.663 1.803Zm-2.898-2.89-.037.017c-.106.047-.21.097-.313.15v.071a4.53 4.53 0 0 1-.1.714l-.09.44c-.086.454-.086.681-.01.807l.032.047c.035.036.08.036.197-.06l.142-.131c.043-.043.09-.083.14-.12l.225-.165c.194-.148.348-.318.29-.642-.047-.262-.082-.38-.122-.466l-.15-.28a6.357 6.357 0 0 0-.204-.383Z" })], -1)];
const LO = Bk(DO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", VO, TO);
}]]), PO = Lo({ name: "IconInfinity" }), FO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, RO = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/infinity", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M12.002 4.793C13.935 4.793 15 6.343 15 8c0 1.657-1.065 3.274-2.998 3.274-1.127 0-2.461-.835-4.002-2.505-1.54 1.67-2.875 2.505-4.002 2.505C2.065 11.274 1 9.657 1 8s1.065-3.207 2.998-3.207c1.127 0 2.461.818 4.002 2.453 1.54-1.635 2.875-2.453 4.002-2.453Zm0 1-.117.005c-.75.06-1.76.695-2.978 1.946l-.237.25.005.006-.011.012.238.258c1.215 1.279 2.224 1.93 2.97 1.998l.13.006C13.198 10.274 14 9.309 14 8c0-1.232-.718-2.126-1.842-2.201l-.156-.006Zm-8.004 0C2.785 5.793 2 6.713 2 8c0 1.309.802 2.274 1.998 2.274.757 0 1.814-.65 3.1-2.004l.237-.258L7.325 8l.005-.006-.02-.022c-1.314-1.403-2.401-2.11-3.195-2.174l-.117-.005Z" })], -1)];
const $O = Bk(PO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", FO, RO);
}]]), BO = Lo({ name: "IconInfoFill" }), HO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, jO = [Ls("g", { id: "CDS-Visual-language/Icons/16/info-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm1 5H7v6h2V6Zm0-3H7v2h2V3Z" })], -1)];
const KO = Bk(BO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", HO, jO);
}]]), zO = Lo({ name: "IconInfo" }), ZO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, UO = [Ls("g", { id: "Info", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 1a6 6 0 1 0 0 12A6 6 0 0 0 8 2Zm.668 4.06V12h-.972V6.06h.972Zm0-2.412v.972h-.972v-.972h.972Z" })], -1)];
const WO = Bk(zO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", ZO, UO);
}]]), YO = Lo({ name: "IconInsert" }), GO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, qO = [Ls("path", { "fill-rule": "evenodd", d: "M8 2.261C7.683 2.117 7.316 2 7 2c-.316 0-.683.117-1 .261V3a1 1 0 0 1-1 1v1h4V4a1 1 0 0 1-1-1v-.739ZM3 4h1v1a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V4h1v2h1V4a1 1 0 0 0-1-1H9v-.798c0-.335-.166-.649-.467-.795C8.148 1.22 7.574 1 7 1s-1.148.22-1.533.407c-.3.146-.467.46-.467.795V3H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h4v-1H3V4Zm6 10V8h4v6H9ZM8 8a1 1 0 0 1 1-1h4a1 1 0 0 1 1 1v6a1 1 0 0 1-1 1H9a1 1 0 0 1-1-1V8Zm2 1.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 0 1h-1a.5.5 0 0 1-.5-.5Zm.5 1.5a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1h-1Z", "clip-rule": "evenodd" }, null, -1)];
const XO = Bk(YO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", GO, qO);
}]]), JO = Lo({ name: "IconLink" }), QO = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, eN = [Ls("g", { id: "link", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "m7.895 5.895.701.703-1.739 1.738-.1.105c-.894.967-1.013 2.318-.265 3.067.747.747 2.088.627 3.052-.25l.105-.101 3.494-3.493.1-.105c.894-.967 1.013-2.318.265-3.067-.46-.459-1.142-.59-1.824-.42l-.791-.792c1.16-.495 2.467-.339 3.316.51 1.127 1.128 1.035 3.06-.183 4.388l-.117.123-3.608 3.608c-1.329 1.328-3.348 1.463-4.51.3-1.127-1.127-1.035-3.06.183-4.387l.117-.123 1.804-1.804ZM5.7 4.091c1.329-1.328 3.348-1.463 4.51-.3 1.127 1.127 1.035 3.06-.183 4.387l-.117.123-1.805 1.804-.702-.701 1.74-1.74.101-.105c.894-.967 1.013-2.318.265-3.067-.747-.747-2.088-.627-3.052.25l-.105.101-3.494 3.493-.1.105c-.894.967-1.013 2.318-.265 3.067.46.458 1.142.59 1.823.42l.791.792c-1.16.495-2.466.338-3.315-.51-1.127-1.128-1.035-3.06.183-4.388l.117-.123L5.7 4.091Z" })], -1)];
const tN = Bk(JO, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", QO, eN);
}]]), nN = Lo({ name: "IconListSearch" }), oN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, rN = [Ls("g", { id: "list-search", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M11 8a3 3 0 0 1 2.445 4.738l2.176 2.176-.707.707-2.176-2.176A3 3 0 1 1 11 8Zm-3.874 4c.092.356.23.692.41 1.001L4 13v-1h3.126ZM2 12v1H1v-1h1Zm9-3a2 2 0 1 0 0 4 2 2 0 0 0 0-4ZM7.535 9c-.179.31-.318.645-.409 1H4V9h3.535ZM2 9v1H1V9h1Zm13-3v1H4V6h11ZM2 6v1H1V6h1Zm13-3v1H4V3h11ZM2 3v1H1V3h1Z" })], -1)];
const sN = Bk(nN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", oN, rN);
}]]), aN = Lo({ name: "IconList" }), lN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, iN = [Ls("g", { id: "list", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M15 12v1H4v-1h11ZM2 12v1H1v-1h1Zm13-3v1H4V9h11ZM2 9v1H1V9h1Zm13-3v1H4V6h11ZM2 6v1H1V6h1Zm13-3v1H4V3h11ZM2 3v1H1V3h1Z" })], -1)];
const cN = Bk(aN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", lN, iN);
}]]), dN = Lo({ name: "IconLoader" }), uN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, pN = [Ls("g", { id: "Loader", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M15 8a7 7 0 1 1-7-7v2a5 5 0 1 0 3.536 1.464L12.95 3.05A7 7 0 0 1 15 8Z" })], -1)];
const fN = Bk(dN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", uN, pN);
}]]), hN = Lo({ name: "IconLocation" }), vN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, mN = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/location", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a5 5 0 0 1 5 5c0 1.418-.89 3.16-1.975 4.824l-.369.552-.187.272-.38.536-.415.571-.409.55-.94 1.254L8 15l-.324-.436-1.142-1.517-.413-.56-.416-.58-.375-.542C4.09 9.545 3 7.57 3 6a5 5 0 0 1 5-5Zm0 1a4 4 0 0 0-4 4c0 .825.42 1.955 1.22 3.329.353.606.77 1.244 1.264 1.946l.446.621.529.714.538.719 1.08-1.452.182-.25c.606-.843 1.1-1.586 1.509-2.285C11.576 7.962 12 6.83 12 6a4 4 0 0 0-4-4Zm0 2a2 2 0 1 1 0 4 2 2 0 0 1 0-4Zm0 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2Z" })], -1)];
const gN = Bk(hN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", vN, mN);
}]]), yN = Lo({ name: "IconLocked" }), bN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, wN = [Ls("g", { id: "CDS-Visual-language/Icons/16/toggle/locked", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 2a3 3 0 0 1 3 3v2h1a1 1 0 0 1 1 1v6a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h1V5a3 3 0 0 1 3-3Zm4 6H4v6h8V8Zm-4 2a1 1 0 1 1 0 2 1 1 0 0 1 0-2Zm0-7a2 2 0 0 0-1.995 1.85L6 5v2h4V5a2 2 0 0 0-1.85-1.995L8 3Z" })], -1)];
const _N = Bk(yN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", bN, wN);
}]]), CN = Lo({ name: "IconLogin" }), xN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, kN = [Ls("g", { id: "login", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "login", d: "M14 1v14H6v-2h1v1h6V2H7v1H6V1h8ZM7.536 4.5l3.535 3.536-3.535 3.535-.708-.707 2.365-2.365-7.157.001v-1l7.085-.001-2.293-2.292.708-.707Z" })], -1)];
const SN = Bk(CN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", xN, kN);
}]]), EN = Lo({ name: "IconLogout" }), IN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, ON = [Ls("g", { id: "logout", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "logout", d: "M10 1v2H9V2H3v12h6v-1h1v2H2V1h8Zm.536 3.5 3.535 3.536-3.535 3.535-.708-.707 2.365-2.365-7.157.001v-1l7.085-.001-2.293-2.292.708-.707Z" })], -1)];
const NN = Bk(EN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", IN, ON);
}]]), MN = Lo({ name: "IconMenu" }), AN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, DN = [Ls("g", { id: "Menu", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "menu", d: "M16 12v2H0v-2h16Zm0-5v2H0V7h16Zm0-5v2H0V2h16Z" })], -1)];
const VN = Bk(MN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", AN, DN);
}]]), TN = Lo({ name: "IconMinus" }), LN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, PN = [Ls("g", { id: "minus", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "M2 8h12v1H2z" })], -1)];
const FN = Bk(TN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", LN, PN);
}]]), RN = Lo({ name: "IconNotification" }), $N = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, BN = [Ls("g", { id: "notification", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "notification", d: "M2 12.5v-2L3 9V7c0-2.817 1.973-4.752 4.5-4.978V1h1v1.025a5 5 0 0 1 4.495 4.758L13 7v2l1 1.5v2h-3.5a2.5 2.5 0 1 1-5 0H2Zm4.5 0a1.5 1.5 0 0 0 3 0h-3ZM8 3a4 4 0 0 0-3.995 3.8L4 7v2.3l-1 1.467v.733h10v-.733L12 9.3V7l-.005-.2A4 4 0 0 0 8 3Z" })], -1)];
const HN = Bk(RN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", $N, BN);
}]]), jN = Lo({ name: "IconOverflowMenuHorizontal" }), KN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, zN = [Ls("g", { id: "Overflow-menu-horizontal", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "overflow-menu-vertical", d: "M8 12a1 1 0 1 1 0 2 1 1 0 0 1 0-2Zm0-5a1 1 0 1 1 0 2 1 1 0 0 1 0-2Zm0-5a1 1 0 1 1 0 2 1 1 0 0 1 0-2Z", transform: "rotate(90 8 8)" })], -1)];
const ZN = Bk(jN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", KN, zN);
}]]), UN = Lo({ name: "IconOverflowMenuVertical" }), WN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, YN = [Ls("g", { id: "Overflow-menu-vertical", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "overflow-menu-vertical", d: "M8 12a1 1 0 1 1 0 2 1 1 0 0 1 0-2Zm0-5a1 1 0 1 1 0 2 1 1 0 0 1 0-2Zm0-5a1 1 0 1 1 0 2 1 1 0 0 1 0-2Z" })], -1)];
const GN = Bk(UN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", WN, YN);
}]]), qN = Lo({ name: "IconPassword" }), XN = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, JN = [Ls("g", { id: "password", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "password", d: "m11.818 1.546 2.636 2.636a1.864 1.864 0 0 1 0 2.636l-2.636 2.636a1.864 1.864 0 0 1-2.536.093l-1.45 1.452L7 11v.831L5.83 13 5 13v.83L3.83 15H1.002L1 12l5.374-5.38a1.864 1.864 0 0 1 .172-2.438l2.636-2.636a1.864 1.864 0 0 1 2.636 0ZM7.068 7.34l-5.069 5.073.002 1.585h1.414l.584-.584.002-1.414 1.414-.002.584-.584.002-1.414 1.415-.002 1.156-1.154-1.504-1.504Zm2.854-5.208-.081.072-2.636 2.636a.932.932 0 0 0-.072 1.237l.072.081 2.636 2.636a.932.932 0 0 0 1.237.072l.081-.072 2.636-2.636a.932.932 0 0 0 .072-1.237l-.072-.081-2.636-2.636a.932.932 0 0 0-1.237-.072Zm1.237 1.39 1.318 1.318a.466.466 0 0 1-.659.659L10.5 4.182a.466.466 0 0 1 .659-.659Z" })], -1)];
const QN = Bk(qN, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", XN, JN);
}]]), eM = Lo({ name: "IconPin" }), tM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, nM = [Ls("g", { id: "pin", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M9.506 2.432a.5.5 0 0 1 .779-.333l.065.054 3.546 3.477a.5.5 0 0 1-.162.82l-.086.027-1.719.36L9.48 9.211v2.869a.501.501 0 0 1-.72.45l-.071-.043-.066-.057-1.63-1.662-2.645 2.647a.5.5 0 0 1-.275.14l-.078.007H2.992a.5.5 0 0 1-.492-.41l-.008-.09v-.982a.5.5 0 0 1 .094-.292l.05-.06L5.298 9.04l-1.66-1.691a.5.5 0 0 1 .272-.844l.086-.007h2.8l2.446-2.401.265-1.665ZM5.997 9.754l-2.505 2.531v.276h.295l2.503-2.506-.122-.127-.17-.174Zm4.344-6.209-.138.873a.5.5 0 0 1-.091.218l-.053.06-2.709 2.66a.5.5 0 0 1-.253.133L7 7.499l-1.815-.001 1.094 1.113.048.035.045.042.575.606 1.533 1.562V9a.5.5 0 0 1 .057-.23l.042-.068.053-.061 2.704-2.622a.5.5 0 0 1 .156-.102l.09-.028.946-.199-2.187-2.145Z" })], -1)];
const oM = Bk(eM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", tM, nM);
}]]), rM = Lo({ name: "IconPlus" }), sM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, aM = [Ls("g", { id: "plus", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M8.5 2v5.5H14v1H8.5V14h-1V8.5H2v-1h5.5V2z" })], -1)];
const lM = Bk(rM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", sM, aM);
}]]), iM = Lo({ name: "IconPrinter" }), cM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, dM = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/printer", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M12 2v3h2a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1h-2v2H4v-2H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h2V2h8Zm-1 7.01H5.02V12H5v1h6V9.01ZM14 6H2v5h2V8h8l.002.848.006.046.007.116V11H14V6Zm-3-3H5v2h6V3Z" })], -1)];
const uM = Bk(iM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", cM, dM);
}]]), pM = Lo({ name: "IconQuestionFill" }), fM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, hM = [Ls("g", { id: "CDS-Visual-language/Icons/16/question-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm2.404 4.576c0 .215-.014.408-.042.581a2.365 2.365 0 0 1-.329.882c-.08.126-.166.259-.259.399a7.103 7.103 0 0 1-.364.497 7.77 7.77 0 0 0-.371.504 3.08 3.08 0 0 0-.294.546c-.08.191-.119.39-.119.595v.63H7.058v-.56c0-.355.033-.663.098-.924s.191-.523.378-.784c.15-.224.296-.434.441-.63.145-.196.27-.383.378-.56.107-.177.196-.35.266-.518.07-.168.105-.336.105-.504 0-.13-.012-.268-.035-.413a1.082 1.082 0 0 0-.147-.399.888.888 0 0 0-.315-.301c-.135-.08-.315-.119-.539-.119a.984.984 0 0 0-.805.371c-.2.247-.31.576-.329.987a59.858 59.858 0 0 0-.77-.105A30.583 30.583 0 0 0 5 5.66c.037-.439.138-.824.301-1.155.163-.331.366-.609.609-.833.243-.224.523-.392.84-.504A3.049 3.049 0 0 1 7.772 3c.868 0 1.524.233 1.967.7.443.467.665 1.092.665 1.876Zm-3.36 7.238V11.26h1.568v1.554H7.044Z" })], -1)];
const vM = Bk(pM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", fM, hM);
}]]), mM = Lo({ name: "IconQuestion" }), gM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, yM = [Ls("g", { id: "question", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 13A6 6 0 1 0 8 2a6 6 0 0 0 0 12Zm2.232-8.3c0 .192-.014.364-.042.516a2.238 2.238 0 0 1-.312.804c-.076.12-.162.252-.258.396a4.866 4.866 0 0 1-.3.39c-.112.132-.218.27-.318.414a2.74 2.74 0 0 0-.252.444 1.109 1.109 0 0 0-.102.456v.72h-.996v-.564c0-.288.034-.544.102-.768a2.03 2.03 0 0 1 .342-.648l.756-1.02c.208-.28.32-.564.336-.852 0-.168-.012-.338-.036-.51a1.294 1.294 0 0 0-.156-.468.922.922 0 0 0-.348-.342c-.152-.088-.352-.132-.6-.132-.184 0-.35.038-.498.114a1.14 1.14 0 0 0-.372.3c-.1.124-.178.266-.234.426-.056.16-.088.32-.096.48l-.996-.144c.048-.36.14-.674.276-.942a2.2 2.2 0 0 1 .504-.666c.2-.176.426-.306.678-.39.252-.084.518-.126.798-.126.696 0 1.224.192 1.584.576.36.384.54.896.54 1.536ZM7.628 12v-1.068h1.008V12H7.628Z" })], -1)];
const bM = Bk(mM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", gM, yM);
}]]), wM = Lo({ name: "IconRefresh" }), _M = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, CM = [Ls("g", { id: "refresh", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M14.93 7A7 7 0 0 1 3 12.898V15H2v-4h4v1l-2.471.001A6 6 0 0 0 13.917 7h1.012Zm-1.001-6v4h-4V4h2.47A6 6 0 0 0 2.012 9.001L1 9a7 7 0 0 1 11.93-5.898V1h1Z" })], -1)];
const xM = Bk(wM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", _M, CM);
}]]), kM = Lo({ name: "IconRemove" }), SM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, EM = [Ls("g", { id: "remove", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Path", d: "m8.01 7.345 3.324-3.324.665.665L8.675 8.01l3.304 3.304-.665.665L8.01 8.675 4.685 12l-.665-.665L7.345 8.01 4 4.665 4.665 4 8.01 7.345z" })], -1)];
const IM = Bk(kM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", SM, EM);
}]]), OM = Lo({ name: "IconSandglass" }), NM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, MM = [Ls("g", { id: "sandglass", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M11.992 1v1l-.117.007a1 1 0 0 0-.883.993v2l-.005.157a2.013 2.013 0 0 1-.935 1.57l-.176.103c-.498.302-.882.673-.882 1.17l.001.057c.028.469.402.822.881 1.113l.117.068c.534.303.915.868.987 1.531l.007.074.005.157v2l.007.117a1 1 0 0 0 .764.857l.112.02.117.006v1H3.997v-1l.117-.007a1 1 0 0 0 .876-.876L4.997 13v-2l.005-.157.007-.074a2.015 2.015 0 0 1 .856-1.45l.13-.08.118-.07c.43-.26.777-.573.862-.975l.018-.137L6.995 8c0-.452-.318-.8-.75-1.085l-.132-.085-.176-.103a2.005 2.005 0 0 1-.916-1.405l-.019-.165L4.997 5V3a1 1 0 0 0-.883-.993L3.997 2V1h7.995ZM7.678 13.105l-1.812.606c-.038.1-.085.198-.138.29h4.533c-.053-.092-.1-.19-.138-.29l-1.813-.606a.999.999 0 0 0-.632 0ZM7.994 8v.074a2 2 0 0 1-.999 1.658c-.537.312-.852.6-.956.972l-.027.127-.007.052L6 11l-.001 1.666 1.68-.56a.999.999 0 0 1 .632 0l1.68.56V11l-.006-.117-.007-.052c-.063-.434-.386-.753-.983-1.099a2 2 0 0 1-.998-1.658L7.994 8ZM9.99 5H6l.006.117.007.052c.063.434.386.753.983 1.099a2 2 0 0 1 .992 1.563L7.994 8a2 2 0 0 1 1-1.732c.597-.346.92-.665.983-1.099l.007-.052L9.99 5Zm.27-3H5.728c.17.294.268.636.268 1h.003l-.001 1H9.99V3h.003c0-.364.097-.706.267-1Z" })], -1)];
const AM = Bk(OM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", NM, MM);
}]]), DM = Lo({ name: "IconSave" }), VM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, TM = [Ls("g", { id: "Save", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", "fill-rule": "nonzero", d: "M2 14V2h9.026L14 5.001V14H2ZM5 3H3v10h2V8h6v5h2V5.413l-2-2.019V6H5V3Zm5 6H6v4h4V9Zm0-6H6v2h4V3Z" })], -1)];
const LM = Bk(DM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", VM, TM);
}]]), PM = Lo({ name: "IconSearch" }), FM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, RM = [Ls("g", { id: "search", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M6.5 1a5.5 5.5 0 0 1 4.187 9.067L15 14.5l-.5.5-4.433-4.313A5.5 5.5 0 1 1 6.5 1Zm0 1a4.5 4.5 0 1 0 0 9 4.5 4.5 0 0 0 0-9Z" })], -1)];
const $M = Bk(PM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", FM, RM);
}]]), BM = Lo({ name: "IconSettings" }), HM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, jM = [Ls("g", { id: "settings", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M8 10.841a2.84 2.84 0 1 1 0-5.68 2.84 2.84 0 0 1 0 5.68Zm0-.786a2.054 2.054 0 1 0 0-4.108 2.054 2.054 0 0 0 0 4.108Zm4.644.463.94 1.223c.109.14.11.336.003.477-.39.517-.851.977-1.367 1.368a.393.393 0 0 1-.477-.002l-1.224-.94a5.248 5.248 0 0 1-1.015.421l-.2 1.53a.393.393 0 0 1-.336.338c-.655.091-1.278.091-1.933.001a.393.393 0 0 1-.336-.339l-.2-1.528a5.249 5.249 0 0 1-1.017-.421l-1.224.94a.393.393 0 0 1-.477.002 7.098 7.098 0 0 1-1.367-1.366.393.393 0 0 1 .002-.477l.94-1.224a5.248 5.248 0 0 1-.422-1.018l-1.53-.2a.393.393 0 0 1-.338-.336 7.086 7.086 0 0 1 0-1.932.393.393 0 0 1 .339-.336l1.529-.2c.105-.355.246-.695.422-1.019l-.94-1.223a.393.393 0 0 1-.002-.477c.39-.516.86-.983 1.367-1.366a.393.393 0 0 1 .477.002l1.224.94a5.248 5.248 0 0 1 1.017-.421l.2-1.529a.393.393 0 0 1 .336-.338 6.915 6.915 0 0 1 1.933 0 .393.393 0 0 1 .336.339l.2 1.53c.353.104.693.245 1.015.42l1.224-.94a.393.393 0 0 1 .477-.001c.516.39.976.85 1.367 1.367a.393.393 0 0 1-.002.477l-.94 1.224c.174.322.315.661.42 1.014l1.528.2c.176.023.315.16.34.336.09.658.09 1.282-.001 1.934a.393.393 0 0 1-.339.336l-1.529.2a5.248 5.248 0 0 1-.42 1.014Zm-.784.27a.393.393 0 0 1-.023-.444c.244-.399.424-.832.533-1.288a.393.393 0 0 1 .332-.298l1.485-.194a6.043 6.043 0 0 0 0-1.126l-1.485-.194a.393.393 0 0 1-.332-.298c-.11-.456-.29-.89-.533-1.288a.393.393 0 0 1 .023-.445l.914-1.189a6.253 6.253 0 0 0-.796-.796l-1.188.914a.393.393 0 0 1-.445.023 4.465 4.465 0 0 0-1.289-.535.393.393 0 0 1-.298-.331l-.194-1.486a6.046 6.046 0 0 0-1.125 0l-.194 1.484a.393.393 0 0 1-.298.332c-.458.11-.892.29-1.291.535a.393.393 0 0 1-.445-.024l-1.188-.913a6.348 6.348 0 0 0-.796.795l.913 1.187c.1.13.109.307.024.446-.245.4-.426.834-.536 1.291a.393.393 0 0 1-.332.298l-1.484.195a6.317 6.317 0 0 0 0 1.124l1.484.194c.162.021.294.14.332.298.11.458.29.892.536 1.292a.393.393 0 0 1-.024.445l-.913 1.188c.239.286.507.554.796.795l1.188-.913c.129-.1.306-.109.445-.024.4.245.834.425 1.29.535.16.038.278.17.299.331l.194 1.485c.378.035.747.035 1.125 0l.194-1.486a.393.393 0 0 1 .298-.331c.457-.11.89-.291 1.289-.535a.393.393 0 0 1 .445.023l1.188.913c.289-.24.555-.507.796-.796l-.914-1.188Z" })], -1)];
const KM = Bk(BM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", HM, jM);
}]]), zM = Lo({ name: "IconShoppingCart" }), ZM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, UM = [Ls("g", { id: "shopping-cart", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M5.95 12a1 1 0 1 1 0 2 1 1 0 0 1 0-2Zm6 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2ZM2.5 2h1.285l.083.007a.5.5 0 0 1 .552.324l.023.087.268 1.599c.063-.012.122-.017.181-.017h8.877a1 1 0 0 1 .987 1.164l-.667 4a1 1 0 0 1-.986.836H5.707l.167 1h6.576a.5.5 0 0 1 .09.992l-.09.008-6.918-.007-.069.007H5.45a.503.503 0 0 1-.47-.33l-.023-.088L3.526 3H2.5a.5.5 0 1 1 0-1Zm11.27 3H4.891l.718 4h7.493l.666-4Z" })], -1)];
const WM = Bk(zM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", ZM, UM);
}]]), YM = Lo({ name: "IconSortDate" }), GM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, qM = [Ls("g", { id: "CDS-Visual-language/Icons/16/operations/sort-date", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M12 8v5.156l2.364-2.363.707.707-3.535 3.536L8 11.5l.707-.707L11 13.086V8h1ZM9.5 1a.5.5 0 0 1 .492.41L10 1.5v.502h1.002a2 2 0 0 1 1.994 1.852l.006.149v2.995h-1L12 6H1.999L2 12.026a1 1 0 0 0 .883.993l.117.007h4v1H3a2 2 0 0 1-1.995-1.85L1 12.025V4.003a2 2 0 0 1 1.85-1.995L3 2.003l1-.001V1.5a.5.5 0 0 1 .992-.09L5 1.5v.502h4V1.5a.5.5 0 0 1 .5-.5ZM9 3.002H5V3.5a.5.5 0 0 1-.992.09L4 3.5v-.498H3a1 1 0 0 0-.993.884L2 4.003 1.999 5h10.002v-.997a1 1 0 0 0-.882-.993l-.117-.007L10 3.002V3.5a.5.5 0 0 1-.992.09L9 3.5v-.498Z" })], -1)];
const XM = Bk(YM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", GM, qM);
}]]), JM = Lo({ name: "IconSource" }), QM = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, eA = [Ls("path", { "fill-rule": "evenodd", d: "m9.015 1.879-3 12 .97.242 3-12-.97-.242Zm2.38 1.813-.79.615 2.947 3.782-2.94 3.594.775.633 3.445-4.212-3.438-4.412Zm-6.798.013.806.59L2.625 8.09l2.771 3.606-.792.61-3.229-4.202 3.222-4.398Z", "clip-rule": "evenodd" }, null, -1)];
const tA = Bk(JM, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", QM, eA);
}]]), nA = Lo({ name: "IconStarFill" }), oA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, rA = [Ls("g", { id: "CDS-Visual-language/Icons/16/toggle/star-fill", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Star", "fill-rule": "nonzero", d: "m6.066 5.938-4.448.092-.086.008a.5.5 0 0 0-.206.89L4.87 9.616l-1.288 4.26-.018.084a.5.5 0 0 0 .782.47L8 11.89l3.653 2.542.074.043a.5.5 0 0 0 .69-.598l-1.289-4.26 3.546-2.688.064-.057a.5.5 0 0 0-.356-.841l-4.449-.092-1.46-4.202a.5.5 0 0 0-.945 0L6.066 5.938Z" })], -1)];
const sA = Bk(nA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", oA, rA);
}]]), aA = Lo({ name: "IconStar" }), lA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, iA = [Ls("g", { id: "CDS-Visual-language/Icons/16/toggle/star", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Star", "fill-rule": "nonzero", d: "m6.066 5.938-4.448.092-.086.008a.5.5 0 0 0-.206.89L4.87 9.616l-1.288 4.26-.018.084a.5.5 0 0 0 .782.47L8 11.89l3.653 2.542.074.043a.5.5 0 0 0 .69-.598l-1.289-4.26 3.546-2.688.064-.057a.5.5 0 0 0-.356-.841l-4.449-.092-1.46-4.202a.5.5 0 0 0-.945 0L6.066 5.938ZM8 3.423l1.103 3.173.036.08a.5.5 0 0 0 .426.256L12.924 7l-2.677 2.03-.066.06a.5.5 0 0 0-.11.483l.972 3.215-2.757-1.918-.077-.044a.5.5 0 0 0-.495.044l-2.758 1.918.974-3.215.018-.087a.5.5 0 0 0-.195-.456L3.075 7l3.36-.068a.5.5 0 0 0 .462-.336L8 3.423Z" })], -1)];
const cA = Bk(aA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", lA, iA);
}]]), dA = Lo({ name: "IconTelegram" }), uA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, pA = [Ls("g", { id: "CDS-Visual-language/Icons/16/social/telegram", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M14.76 2.158c.2.172.276.493.224.96l-2.12 9.961c-.105.495-.293.787-.564.876-.271.09-.6.045-.984-.134l-3.723-2.746c-.517-.36-.776-.67-.776-.932 0-.261.203-.551.608-.87l4.02-4.027c.04-.116.028-.21-.033-.28-.06-.071-.176-.071-.345 0L5.37 8.72c-.239.178-.447.296-.626.354-.178.058-.399.058-.661 0l-2.513-.878C1.19 8.066 1 7.86 1 7.581c0-.28.14-.493.417-.641l12.431-4.876c.407-.11.71-.078.912.094Z" })], -1)];
const fA = Bk(dA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", uA, pA);
}]]), hA = Lo({ name: "IconTimer" }), vA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, mA = [Ls("g", { id: "timer", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "timer", d: "M8 1a7 7 0 1 1-5 11.898V15H2v-4h4v1l-2.471.001A6 6 0 1 0 8 2V1Zm0 3v4.593l2.536 2.535-.708.708L7 9.007 7.007 9H7V4h1ZM1.037 7.257l.995.104a6.055 6.055 0 0 0 .03 1.496l.037.219-.984.18L1.073 9a7.1 7.1 0 0 1-.06-1.442l.024-.3Zm1.525-3.674.776.63c-.29.357-.537.746-.737 1.158l-.142.314-.923-.385a7.01 7.01 0 0 1 .822-1.453l.204-.264Zm3.297-2.26.305.953c-.438.14-.857.33-1.25.566l-.289.186-.563-.827c.456-.31.951-.569 1.478-.766l.32-.111Z" })], -1)];
const gA = Bk(hA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", vA, mA);
}]]), yA = Lo({ name: "IconTool" }), bA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, wA = [Ls("g", { id: "tool", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M10.886 5.71a5.216 5.216 0 0 1-.44 1.854c.084.059.163.127.239.202l3.758 3.759a2 2 0 0 1 0 2.828l-.099.1a2 2 0 0 1-2.828 0l-3.76-3.76a2.015 2.015 0 0 1-.206-.242 5.191 5.191 0 0 1-1.84.435c-2.728.13-4.834-1.977-4.704-4.705a5.291 5.291 0 0 1 .765-2.48L4.63 6.559a1 1 0 0 0 1.32.083l.094-.083.782-.782a1 1 0 0 0 .083-1.32l-.083-.094L4.043 1.58a5.249 5.249 0 0 1 2.138-.575c2.728-.13 4.835 1.976 4.705 4.705ZM6.422 2l-.193.005c-.086.004-.17.01-.256.02l-.064.008 1.665 1.668.124.142a2 2 0 0 1-.047 2.51l-.119.131-.827.824-.142.123a2 2 0 0 1-2.51-.046l-.131-.12-1.807-1.807-.052.247a4.132 4.132 0 0 0-.037.26l-.021.264c-.102 2.139 1.519 3.76 3.658 3.658.425-.02.844-.107 1.246-.255l.24-.096.738-.324.473.652.05.063.054.06 3.759 3.758a1 1 0 0 0 1.32.083l.094-.083.1-.099a1 1 0 0 0 .082-1.32l-.083-.094-3.759-3.759-.055-.051-.055-.043-.664-.472.328-.746c.21-.479.332-.985.356-1.498C9.986 3.59 8.465 2.001 6.422 2Z" })], -1)];
const _A = Bk(yA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", bA, wA);
}]]), CA = Lo({ name: "IconTruck" }), xA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, kA = [Ls("g", { id: "CDS-Visual-language/Icons/16/other/truck", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "M6 12a2 2 0 1 1-4 0h-.5a.5.5 0 0 1-.492-.41L1 11.5v-9l.008-.09L1 2.498a.5.5 0 0 1 .41-.492l.09-.008h9a.5.5 0 0 1 .492.41l.008.09V3h1.673a.5.5 0 0 1 .393.191l.05.078 1.827 3.499a.5.5 0 0 1 .05.151L15 7V11.5a.5.5 0 0 1-.41.492L14.5 12h-2a2 2 0 1 1-4 0H6Zm-2-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm6.5 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2ZM10 2.998H2v8.001h.268a2 2 0 0 1 3.464 0h3.036A2.004 2.004 0 0 1 10 10.064V2.998Zm4 5.001h-3l.001 2.064c.525.136.966.479 1.231.937H14V8ZM12.369 4H11v3h2.936l-1.567-3Z" })], -1)];
const SA = Bk(CA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", xA, kA);
}]]), EA = Lo({ name: "IconUnlocked" }), IA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, OA = [Ls("g", { id: "CDS-Visual-language/Icons/16/toggle/unlocked", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 2a3 3 0 0 1 3 3h-1a2 2 0 0 0-1.85-1.995L8 3a2 2 0 0 0-1.995 1.85L6 5v2h6a1 1 0 0 1 1 1v6a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h1V5a3 3 0 0 1 3-3Zm4 6H4v6h8V8Zm-4 2a1 1 0 1 1 0 2 1 1 0 0 1 0-2Z" })], -1)];
const NA = Bk(EA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", IA, OA);
}]]), MA = Lo({ name: "IconUpload" }), AA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, DA = [Ls("g", { id: "CDS-Visual-language/Icons/16/file/upload", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Combined-Shape", d: "m8.036 5 3.535 3.536-.707.707L8.5 6.88v8.156h-1V6.95L5.207 9.243 4.5 8.536 8.036 5ZM15 1v3h-1l.001-2H2v2H1V1h14Z" })], -1)];
const VA = Bk(MA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", AA, DA);
}]]), TA = Lo({ name: "IconUserAvatar" }), LA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, PA = [Ls("g", { id: "user-avatar", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "user", d: "M8 1a7 7 0 1 1 0 14A7 7 0 0 1 8 1Zm0 9.5a3 3 0 0 0-2.986 2.705c.88.506 1.9.795 2.986.795 1.087 0 2.107-.29 2.986-.795A3 3 0 0 0 8 10.5ZM8 2a6 6 0 0 0-3.878 10.579l.002-.07a4.002 4.002 0 0 1 7.755.011.172.172 0 0 1 .004.054A6 6 0 0 0 8 2Zm0 2a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Zm0 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3Z" })], -1)];
const FA = Bk(TA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", LA, PA);
}]]), RA = Lo({ name: "IconUserGroup" }), $A = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, BA = [Ls("g", { id: "user-group", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "icon", d: "M8 7a4 4 0 0 1 3.564 2.182A2.5 2.5 0 0 1 15 11.5V15H1v-3.5a2.5 2.5 0 0 1 3.437-2.318A3.998 3.998 0 0 1 8 7Zm0 1a3 3 0 0 0-3 3v3h5.999v-3.029l-.004-.147A3 3 0 0 0 8 8Zm4.5 2c-.174 0-.342.03-.5.085V14h2v-2.5a1.5 1.5 0 0 0-1.5-1.5Zm-9 0a1.5 1.5 0 0 0-1.493 1.356L2 11.5V14h2v-3.915A1.499 1.499 0 0 0 3.5 10ZM13 4a2 2 0 1 1 0 4 2 2 0 0 1 0-4ZM3 4a2 2 0 1 1 0 4 2 2 0 0 1 0-4Zm10 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2ZM3 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm5-4a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Zm0 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3Z" })], -1)];
const HA = Bk(RA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", $A, BA);
}]]), jA = Lo({ name: "IconVisibilityOff" }), KA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, zA = [Ls("g", { id: "view-off", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "m13.732 2.267.75.75-1.749 1.75C14.195 5.972 15 7.447 15 8c0 1-2.625 5-7 5a7.064 7.064 0 0 1-2.885-.616L3.017 14.48l-.75-.75L13.732 2.267Zm-1.713 3.214-1.285 1.283a3 3 0 0 1-3.97 3.97l-.883.883A6.04 6.04 0 0 0 8 12c1.71 0 3.25-.713 4.493-1.899a7.19 7.19 0 0 0 1.192-1.47l.123-.218.093-.186.083-.186L14 8l-.036-.088-.079-.173a4.868 4.868 0 0 0-.2-.37 7.19 7.19 0 0 0-1.192-1.47 7.816 7.816 0 0 0-.474-.418ZM8 3c1.123 0 2.131.264 3.009.673l-.761.76A6.047 6.047 0 0 0 8 4c-1.71 0-3.25.713-4.493 1.899a7.19 7.19 0 0 0-1.192 1.47l-.123.218-.093.186-.083.186L2 8l.036.088.079.173c.05.104.117.232.2.37a7.19 7.19 0 0 0 1.192 1.47c.185.177.377.343.575.498l-.715.715C1.842 10.094 1 8.566 1 8c0-1 2.625-5 7-5Zm1.95 4.551-2.4 2.398A2 2 0 0 0 9.95 7.551ZM8 5c.488 0 .95.117 1.357.324l-.765.765a2 2 0 0 0-2.503 2.503l-.765.765A3 3 0 0 1 8 5Z" })], -1)];
const ZA = Bk(jA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", KA, zA);
}]]), UA = Lo({ name: "IconVisibilityOn" }), WA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, YA = [Ls("g", { id: "view", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M8 3c4.375 0 7 4 7 5s-2.625 5-7 5-7-4-7-5 2.625-5 7-5Zm0 1c-1.71 0-3.25.713-4.493 1.899a7.19 7.19 0 0 0-1.192 1.47l-.123.218C2.066 7.826 2 7.997 2 8l.036.088.079.173c.05.104.117.232.2.37a7.19 7.19 0 0 0 1.192 1.47C4.75 11.287 6.29 12 8 12c1.71 0 3.25-.713 4.493-1.899a7.19 7.19 0 0 0 1.192-1.47l.123-.218c.126-.239.192-.41.192-.413l-.036-.088a4.75 4.75 0 0 0-.279-.543 7.19 7.19 0 0 0-1.192-1.47C11.25 4.713 9.71 4 8 4Zm0 1a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 1a2 2 0 1 0 0 4 2 2 0 0 0 0-4Z" })], -1)];
const GA = Bk(UA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", WA, YA);
}]]), qA = Lo({ name: "IconVk" }), XA = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, JA = [Ls("g", { id: "CDS-Visual-language/Icons/16/social/vk", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "M3.272 4c0 1.705.161 2.854.809 3.934.647 1.08 1.752 1.92 2.696 2.162V4h2.335v3.599c.95-.48 1.619-.937 2.006-1.371.388-.434.778-1.177 1.172-2.228h2.271c-.184 1.074-.56 2.034-1.128 2.879-.567.845-1.18 1.369-1.837 1.572 1.107.809 1.88 1.505 2.315 2.088.437.583.8 1.403 1.089 2.461h-2.418c-.588-1.176-1.076-1.94-1.464-2.29-.387-.35-1.056-.63-2.006-.843V13c-2.631-.046-4.64-.866-6.03-2.461C1.695 8.944 1 6.764 1 4h2.272Z" })], -1)];
const QA = Bk(qA, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", XA, JA);
}]]), eD = Lo({ name: "IconWinner" }), tD = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, nD = [Ls("g", { id: "winner", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "winner", d: "M3 7a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h1V1h8v1h1a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1a3 3 0 0 1-3 3h-.5v4H11v1H5v-1h2.5v-4H7a3 3 0 0 1-3-3H3Zm8-5H5v5a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2V2ZM4 3H3v3h1V3Zm9 0h-1v3h1V3Z" })], -1)];
const oD = Bk(eD, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", tD, nD);
}]]), rD = Lo({ name: "IconWorkspace" }), sD = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, aD = [Ls("g", { id: "workspace", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", d: "M14 8a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H9a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h5Zm0 1H9v5h5V9ZM6 8a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h4Zm0 1H2v3h4V9Zm8-8a1 1 0 0 1 1 1v4a1 1 0 0 1-1 1h-3a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h3ZM8 1a1 1 0 0 1 1 1v4a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6Zm6 1h-3v4h3V2ZM8 2H2v4h6V2Z" })], -1)];
const lD = Bk(rD, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", sD, aD);
}]]), iD = Lo({ name: "IconYoutube" }), cD = { xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 16 16" }, dD = [Ls("g", { id: "youtube", "fill-rule": "evenodd", "stroke-width": "1" }, [Ls("path", { id: "Shape", "fill-rule": "nonzero", d: "m9.399 3.019 1.507.076c1.555.12 2.593.354 3.114.7C14.673 4.22 15 5.62 15 8s-.327 3.802-.98 4.268c-.757.4-2.05.654-3.881.758l-.669.03c-.23.009-.467.015-.713.02L8 13.08l-.757-.006c-.245-.004-.483-.01-.713-.018l-.667-.03c-1.828-.105-3.116-.358-3.863-.759-.667-.464-1-1.851-1-4.162s.345-3.748 1.036-4.31c.467-.327 1.4-.553 2.798-.678l1.606-.094L9.4 3.019ZM6.452 4.025l-1.536.087-.43.042-.896.126c-.456.085-.768.184-.923.291-.36.294-.64 1.396-.665 3.251L2 8.106c0 1.811.227 2.92.49 3.262l.047.051.049.024c.555.258 1.5.446 2.81.549l1.715.08h1.78l1.43-.06c1.464-.096 2.511-.293 3.116-.571l.038-.02c.263-.269.501-1.345.523-3.146a40.66 40.66 0 0 0-.012-1.06l-.103-1.27c-.103-.77-.262-1.216-.417-1.317-.219-.145-.633-.272-1.23-.373l-.38-.057-.653-.073-1.02-.073-1.186-.042L8 4l-1.548.025Zm.012 2.08L10.192 8 6.464 9.948V6.105Z" })], -1)];
const uD = Bk(iD, [["render", function(e2, t2, n2, o2, r2, s2) {
  return Ss(), Ns("svg", cD, dD);
}]]), pD = Object.freeze(Object.defineProperty({ __proto__: null, IconAdd: Kk, IconAdministrator: Wk, IconAlertFill: Xk, IconAlert: tS, IconAnalog: sS, IconArrowDown: cS, IconArrowLeftBottom: fS, IconArrowLeftTop: gS, IconArrowLeft: _S, IconArrowRightBottom: SS, IconArrowRightTop: NS, IconArrowRight: VS, IconArrowUp: FS, IconArrowsVertical: HS, IconCalendar: ZS, IconCardBank: GS, IconCategory: QS, IconCheckFill: oE, IconCheck: lE, IconCheckboxFill: uE, IconCheckbox: vE, IconChevronDown: bE, IconChevronLeft: xE, IconChevronRight: IE, IconChevronUp: AE, IconChocholateMenu: LE, IconChocolateMenu: $E, IconClock: KE, IconCollapse: WE, IconComment: XE, IconCompany: tI, IconCompare: sI, IconCopy: cI, IconDataCards: fI, IconDelete: gI, IconDirectoryOpen: _I, IconDirectory: SI, IconDocDownload: NI, IconDoc: VI, IconDownload: FI, IconDraggable: HI, IconEdit: ZI, IconEnter: GI, IconErrorFill: QI, IconError: oO, IconExpand: lO, IconExternal: uO, IconFacebookFill: vO, IconFacebook: bO, IconFilters: xO, IconFlag: IO, IconFunnel: AO, IconGlobe: LO, IconInfinity: $O, IconInfoFill: KO, IconInfo: WO, IconInsert: XO, IconLink: tN, IconListSearch: sN, IconList: cN, IconLoader: fN, IconLocation: gN, IconLocked: _N, IconLogin: SN, IconLogout: NN, IconMenu: VN, IconMinus: FN, IconNotification: HN, IconOverflowMenuHorizontal: ZN, IconOverflowMenuVertical: GN, IconPassword: QN, IconPin: oM, IconPlus: lM, IconPrinter: uM, IconQuestionFill: vM, IconQuestion: bM, IconRefresh: xM, IconRemove: IM, IconSandglass: AM, IconSave: LM, IconSearch: $M, IconSettings: KM, IconShoppingCart: WM, IconSortDate: XM, IconSource: tA, IconStarFill: sA, IconStar: cA, IconTelegram: fA, IconTimer: gA, IconTool: _A, IconTruck: SA, IconUnlocked: NA, IconUpload: VA, IconUserAvatar: FA, IconUserGroup: HA, IconVisibilityOff: ZA, IconVisibilityOn: GA, IconVk: QA, IconWinner: oD, IconWorkspace: lD, IconYoutube: uD }, Symbol.toStringTag, { value: "Module" }));
var fD = {};
Object.defineProperty(fD, "__esModule", { value: true });
var hD = fD.sendFromIframe = fD.sendToIframe = fD.receiveCommandFromIframe = mD = fD.receiveCommandToIframe = void 0, vD = function(e2, t2, n2) {
  return function(o2) {
    if ("string" == typeof o2.data && o2.data.includes("command") && o2.data.includes("widget")) {
      var r2 = null;
      try {
        var s2 = JSON.parse(o2.data);
        r2 = { widget: s2.widget, command: s2.command, data: s2.data }, s2.widget === e2 && t2(r2);
      } catch (e3) {
        console.log(e3), n2(e3);
      }
    }
  };
}, mD = fD.receiveCommandToIframe = function(e2, t2, n2) {
  window.addEventListener("message", vD(e2, t2, n2));
};
fD.receiveCommandFromIframe = function(e2, t2, n2) {
  window.onmessage = vD(e2, t2, n2);
};
fD.sendToIframe = function(e2, t2) {
  var n2;
  null === (n2 = e2.contentWindow) || void 0 === n2 || n2.postMessage(JSON.stringify(t2), "*");
};
hD = fD.sendFromIframe = function(e2) {
  var t2;
  null === (t2 = window.top) || void 0 === t2 || t2.postMessage(JSON.stringify(e2), "*");
};
var gD = {};
Object.defineProperty(gD, "__esModule", { value: true });
var yD, bD = gD.EWidgets = void 0;
!function(e2) {
  e2.Test = "Test", e2.B2BCenterSignWidget = "B2BCenterSignWidget";
}(yD || (bD = gD.EWidgets = yD = {})), gD.default = { Test: { path: "/test", width: "100%", height: "100%" }, B2BCenterSignWidget: { path: "", width: "100%", height: "100%", loading: "eager" } };
var wD = {};
Object.defineProperty(wD, "__esModule", { value: true }), wD.useWidget = void 0;
var _D = gD, CD = fD, xD = "undefined" == typeof window;
function kD(e2) {
  console.log("receiver in client: ", e2), mD(bD.B2BCenterSignWidget, (t2) => {
    console.log("received command in widget from parent $edsWidgetPostMessagingCommunication", t2), e2({ name: t2.command, data: t2.data });
  }, (e3) => {
    throw new Error(e3);
  });
}
function SD(e2) {
  console.log("send event from $sendEdsPostMessageEvent to parent", e2);
  const t2 = { widget: bD.B2BCenterSignWidget, command: e2.name, data: e2.data };
  hD(t2);
}
wD.useWidget = function(e2) {
  if (xD)
    return { onLoad: function(e3) {
    }, onError: function(e3) {
    }, init: function(e3) {
    }, sendCommand: function(e3) {
    }, receiveData: function(e3, t3) {
    }, reload: function() {
    } };
  var t2 = _D.default[e2.widget];
  t2.baseUrl = e2.baseUrl;
  var n2 = function(e3) {
    var t3 = document.createElement("iframe");
    if (t3.style.minHeight = e3.height, t3.style.width = e3.width, t3.style.border = "none", t3.style.margin = "0", t3.style.padding = "0", t3.src = e3.baseUrl + e3.path, t3.loading = (null == e3 ? void 0 : e3.loading) || "eager", null == e3 ? void 0 : e3.attrs)
      for (var n3 in e3.attrs)
        t3[n3] = e3.attrs[n3];
    return t3;
  }(t2);
  return { onLoad: function(e3) {
    n2.onload = e3;
  }, onError: function(e3) {
    n2.onerror = e3;
  }, sendCommand: function(t3) {
    t3.widget = e2.widget, (0, CD.sendToIframe)(n2, t3);
  }, receiveData: function(t3, n3) {
    (0, CD.receiveCommandFromIframe)(e2.widget, t3, n3);
  }, init: function(t3) {
    void 0 === t3 && (t3 = {});
    for (var o2 = document.getElementById(e2.elementId); null == o2 ? void 0 : o2.firstChild; )
      o2.removeChild(o2.firstChild);
    Object.keys(t3).length && (n2.src = n2.src + "?" + new URLSearchParams(t3).toString()), null == o2 || o2.append(n2);
  }, reload: function() {
    var e3;
    null === (e3 = n2.contentWindow) || void 0 === e3 || e3.location.reload();
  } };
};
const ED = { install(e2, t2) {
  console.log("register $edsWidgetPostMessagingCommunication", t2), console.log("register $edsWidgetPostMessagingCommunication"), e2.provide("subscribeEdsPostMessagingCommands", kD), e2.provide("sendEdsPostMessagingEvent", SD);
} }, ID = function(e2 = {}) {
  e2 = { ...Am, ...e2 };
  const { components: t2 = {}, directives: n2 = {}, icons: o2 = {}, illustrations: r2 = {} } = e2, s2 = tv(e2.display, e2.ssr), a2 = Wv(e2.theme);
  return { install: (l2) => {
    if (Object.entries({ ...t2 }).forEach(([e3, t3]) => {
      l2.component(e3, t3);
    }), Object.entries({ ...n2 }).forEach(([e3, t3]) => {
      l2.directive(e3, t3);
    }), Object.entries({ ...o2 }).forEach(([e3, t3]) => {
      l2.component(e3, t3);
    }), Object.entries({ ...r2 }).forEach(([e3, t3]) => {
      l2.component(e3, t3);
    }), a2.install(l2), l2.provide(ev, s2), l2.provide(zv, a2), l2.provide(Sm, e2.cssGrid), l2.provide(Sv, e2.locale), l2.provide(Tf, e2.teleport), wd && e2.ssr)
      if (l2.$nuxt)
        l2.$nuxt.hook("app:suspense:resolve", () => {
          s2.update();
        });
      else {
        const { mount: e3 } = l2;
        l2.mount = (...t3) => {
          const n3 = e3(...t3);
          return un(() => s2.update()), l2.mount = e3, n3;
        };
      }
    su.reset(), l2.mixin({ computed: { $cds() {
      return vt({ display: Dm.call(this, ev) });
    } } });
  }, display: s2, theme: a2 };
}({ components: Rk, directives: jg, icons: pD });
function OD(e2, t2) {
  console.log("RUN APP ", t2);
  const n2 = Dl(gd, t2 != null ? t2 : {});
  n2.use(function() {
    const e3 = ae(true), t3 = e3.run(() => At({}));
    let n3 = [], o2 = [];
    const r2 = kt({ install(e4) {
      Zl(r2), r2._a = e4, e4.provide(Ul, r2), e4.config.globalProperties.$pinia = r2, ql && xi(e4, r2), o2.forEach((e5) => n3.push(e5)), o2 = [];
    }, use(e4) {
      return this._a || Vl ? n3.push(e4) : o2.push(e4), this;
    }, _p: n3, _a: null, _e: e3, _s: /* @__PURE__ */ new Map(), state: t3 });
    return ql && "undefined" != typeof Proxy && r2.use(Ii), r2;
  }()), n2.use(ID), n2.use(ED, {}), n2.mount(e2);
}
function ND(e2) {
  const t2 = At(false), n2 = At(null), o2 = At(null);
  return { signedDoc: n2, fetchSignedDoc: async (r2) => {
    t2.value = true;
    try {
      const o3 = await e2.fetchSignedDoc(r2);
      t2.value = false, n2.value = o3;
    } catch (e3) {
      o2.value = new Error("\u041E\u0448\u0438\u0431\u043A\u0430 \u043F\u0440\u0438 \u043F\u043E\u043F\u044B\u0442\u043A\u0435 \u043F\u043E\u043B\u0443\u0447\u0438\u0442\u044C \u043F\u043E\u0434\u043F\u0438\u0441\u0430\u043D\u043D\u044B\u0439 \u0434\u043E\u043A\u0443\u043C\u0435\u043D\u0442"), t2.value = false;
    }
  }, isSignedDocFetching: t2, signedDocFetchError: o2 };
}
function MD() {
  console.log("TEst module log");
}
export {
  e as SomeClass,
  OD as runApp,
  MD as someFn,
  ND as useSignedDoc
};
