export type EdsIframeOptions = {
    url: string;
    elId: string;
};
export type TSize2d = {
    width: number;
    height: number;
};
export type TInitOptions = {
    communicationType: 'postMessaging';
    secret: string;
};
type TEdsWidgetBaseCommand = {
    name: string;
    data?: any;
};
type TEdsWidgetCommandInit = TEdsWidgetBaseCommand & {
    name: 'init';
    data: TInitOptions;
};
export type TEdsWidgetCommand = TEdsWidgetBaseCommand | TEdsWidgetCommandInit;
type TEdsWidgetEventBase = {
    name: string;
    data: unknown;
};
export type TEdsWidgetEventError = TEdsWidgetEventBase & {
    name: 'failed';
    data: {
        error: Error;
    };
};
export type TEdWidgetEventSigned = TEdsWidgetEventBase & {
    name: 'signed';
    data: {
        signature: string;
    };
};
export type TEdWidgetEventDocCreated = TEdsWidgetEventBase & {
    name: 'doc_created';
    data: {
        doc_id: number;
    };
};
export type TEdsWidgetHTMLUpdated = TEdsWidgetEventBase & {
    name: 'html_updated';
    data: TSize2d;
};
export type TEdsWidgetEvent = TEdsWidgetEventError | TEdWidgetEventSigned | TEdsWidgetHTMLUpdated | TEdWidgetEventDocCreated;
export declare function getWidget(o: EdsIframeOptions): {
    connect: (config?: Record<string, string>) => Promise<{
        execCommand: (command: TEdsWidgetCommand) => void;
        subscribe: (onEvent: (eventData: {
            e: string;
            d: any;
        }) => void, onError: (e: string) => void) => void;
    }>;
};
export declare function someTh(): void;
export declare function receiveCommandToIframeWidget(handler: (cmd: TEdsWidgetCommand) => void): void;
export declare function sendFromIframeWidget(e: TEdsWidgetEvent): void;
export {};
