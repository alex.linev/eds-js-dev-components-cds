var lib = {};

Object.defineProperty(lib, "__esModule", { value: true });
var sendFromIframe_1 = lib.sendFromIframe = lib.sendToIframe = lib.receiveCommandFromIframe = receiveCommandToIframe_1 = lib.receiveCommandToIframe = void 0;
var receiver = function (widget, resolve, reject) {
    return function (event) {
        // @ts-ignore
        if (typeof event.data !== 'string' || !event.data.includes('command') || !event.data.includes('widget')) {
            return;
        }
        var response = null;
        try {
            var result = JSON.parse(event.data);
            response = {
                widget: result.widget,
                command: result.command,
                data: result.data
            };
            if (result.widget === widget) {
                resolve(response);
            }
        }
        catch (e) {
            console.log(e);
            reject(e);
        }
    };
};
var receiveCommandToIframe = function (widget, resolve, reject) {
    window.addEventListener('message', receiver(widget, resolve, reject));
};
var receiveCommandToIframe_1 = lib.receiveCommandToIframe = receiveCommandToIframe;
var receiveCommandFromIframe = function (widget, resolve, reject) {
    window.onmessage = receiver(widget, resolve, reject);
};
lib.receiveCommandFromIframe = receiveCommandFromIframe;
var sendToIframe = function (iframe, data) {
    var _a;
    (_a = iframe.contentWindow) === null || _a === void 0 ? void 0 : _a.postMessage(JSON.stringify(data), '*');
};
lib.sendToIframe = sendToIframe;
var sendFromIframe = function (data) {
    var _a;
    (_a = window.top) === null || _a === void 0 ? void 0 : _a.postMessage(JSON.stringify(data), '*');
};
sendFromIframe_1 = lib.sendFromIframe = sendFromIframe;

var widgets = {};

Object.defineProperty(widgets, "__esModule", { value: true });
var EWidgets_1 = widgets.EWidgets = void 0;
var EWidgets;
(function (EWidgets) {
    EWidgets["Test"] = "Test";
    EWidgets["B2BCenterSignWidget"] = "B2BCenterSignWidget";
})(EWidgets || (EWidgets_1 = widgets.EWidgets = EWidgets = {}));
widgets.default = {
    Test: {
        path: '/test',
        width: '100%',
        height: '100%'
    },
    B2BCenterSignWidget: {
        path: '', // fixme: Надо от этого избавиться - хуйня какая то
        width: '100%',
        height: '100%',
        loading: 'eager'
    }
};

var useIframe = {};

Object.defineProperty(useIframe, "__esModule", { value: true });
var useWidget_1 = useIframe.useWidget = void 0;
var widgets_1 = widgets;
var lib_1 = lib;
var IS_SSR = typeof window === 'undefined';
var createIframe = function (options) {
    var iframe = document.createElement('iframe');
    iframe.style.minHeight = options.height;
    iframe.style.width = options.width;
    iframe.style.border = 'none';
    iframe.style.margin = '0';
    iframe.style.padding = '0';
    iframe.src = options.baseUrl + options.path;
    // @ts-ignore
    iframe.loading = (options === null || options === void 0 ? void 0 : options.loading) || 'eager';
    if (options === null || options === void 0 ? void 0 : options.attrs) {
        for (var attr in options.attrs) {
            // @ts-ignore
            iframe[attr] = options.attrs[attr];
        }
    }
    return iframe;
};
var useWidget = function (options) {
    if (IS_SSR) {
        return {
            onLoad: function (clb) { },
            onError: function (clb) { },
            init: function (config) {
            },
            sendCommand: function (data) { },
            receiveData: function (resolve, reject) { },
            reload: function () { }
        };
    }
    var widget = widgets_1.default[options.widget];
    widget.baseUrl = options.baseUrl;
    var iframe = createIframe(widget);
    return {
        onLoad: function (clb) {
            iframe.onload = clb;
        },
        onError: function (clb) {
            iframe.onerror = clb;
        },
        sendCommand: function (data) {
            data.widget = options.widget;
            (0, lib_1.sendToIframe)(iframe, data);
        },
        receiveData: function (resolve, reject) {
            (0, lib_1.receiveCommandFromIframe)(options.widget, resolve, reject);
        },
        init: function (config) {
            if (config === void 0) { config = {}; }
            var element = document.getElementById(options.elementId);
            while (element === null || element === void 0 ? void 0 : element.firstChild) {
                element.removeChild(element.firstChild);
            }
            if (Object.keys(config).length) {
                iframe.src = iframe.src + '?' + new URLSearchParams(config).toString();
            }
            element === null || element === void 0 ? void 0 : element.append(iframe);
        },
        reload: function () {
            var _a;
            (_a = iframe.contentWindow) === null || _a === void 0 ? void 0 : _a.location.reload();
        }
    };
};
useWidget_1 = useIframe.useWidget = useWidget;

const secret = '12345';
// type TSignWidgetSubscribe = (onEvent: (eventData: {e: string, d: any}) => void, onError: (e: string) => void) => void;
// todo: добавить обработчики для перехвата изменения высоты html
// todo: а может и не надо отдавать ничего в return? сразу пытаться его вставить с одним конфигом на все?
function getWidget(o) {
    const opts = {
        widget: EWidgets_1.B2BCenterSignWidget,
        elementId: o.elId,
        baseUrl: o.url
    };
    const widget = useWidget_1(opts); // todo Проверить на чистом, будет ли установка vue тут. Если она есть, тогда можно тоже использовать реактивные штуки
    const execCommand = (command) => {
        console.info('execCommand in client: ', command);
        return widget.sendCommand({
            widget: EWidgets_1.B2BCenterSignWidget,
            command: command.name,
            data: command.data,
        });
    };
    const subscribe = (onEvent, onError) => {
        const resolver = (data) => {
            console.info('resolver in client: ', data);
            if (data.widget === EWidgets_1.B2BCenterSignWidget) {
                onEvent({ e: data.command, d: data.data });
            }
        };
        widget.receiveData(resolver, onError);
    };
    // todo тут надо передавать конфиг для процесса ЭП
    const connectFn = (config) => {
        console.log('connection starts');
        // const initCfg = {
        //     token: 'jwt_token_here',
        //     opId: 'OPERATION_ID_UUID'
        // };
        return new Promise((resolve, reject) => {
            widget.onError(() => {
                console.error('IFRAME connect ERROR');
                reject(new Error('Не удалось подключить iframe'));
            });
            widget.onLoad(() => {
                console.log('widget loaded 123');
                resolve({ execCommand, subscribe });
                const o = { communicationType: 'postMessaging', secret };
                execCommand({ name: 'init', data: o });
            });
            widget.init(config ?? {}); //todo: а вот тут непонятно, надо ли за сессию отдавать uuid и работать только с одним подписанием или тут может быть серия???
        });
    };
    return {
        connect: connectFn,
    };
}
function someTh() {
    console.log();
}
function receiveCommandToIframeWidget(handler) {
    console.log('receiver in client: ', handler);
    receiveCommandToIframe_1(EWidgets_1.B2BCenterSignWidget, (d) => {
        console.log('received command in widget from parent $edsWidgetPostMessagingCommunication', d);
        handler({ name: d.command, data: d.data });
    }, (e) => {
        throw new Error(e);
    });
}
function sendFromIframeWidget(e) {
    console.log('send event from $sendEdsPostMessageEvent to parent', e);
    const d = {
        widget: EWidgets_1.B2BCenterSignWidget,
        command: e.name,
        data: e.data
    };
    sendFromIframe_1(d);
}

export { getWidget, receiveCommandToIframeWidget, sendFromIframeWidget, someTh };
//# sourceMappingURL=index.js.map
