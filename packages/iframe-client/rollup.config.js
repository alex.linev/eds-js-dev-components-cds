// rollup.config.js
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import typescript from '@rollup/plugin-typescript';

export default {
    input: 'src/index.ts', // Входной файл вашего проекта
    output: {
        file: 'dist/index.js', // Выходной файл
        format: 'esm', // Формат выходного файла (например, cjs, esm)
        sourcemap: true, // Генерация sourcemap
    },
    // plugins: [
    //      typescript(),
    //     //typescript({ compilerOptions: {lib: ["es5", "es6", "dom"], target: "es5"}})
    // ]
    plugins: [
        // Преобразование TypeScript в JavaScript
        nodeResolve({
            // browser: true,
            preferBuiltins: true,
            extensions: ['.js', '.ts'],
        }),
        // Решение путей для внешних зависимостей
        typescript({
            tsconfig: './tsconfig.json', // Путь к вашему tsconfig.json
            // Опционально: настройки TypeScript
        }),
        // Преобразование CommonJS модулей в ES6
        commonjs(),
        // // Минификация кода
        // terser(),
    ],
    // external: ['node:fs', 'node:stream'], // Внешние зависимости, которые не должны быть включены в бандл
};
