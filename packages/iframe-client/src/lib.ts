import {receiveCommandToIframe, sendFromIframe} from 'edsjs-test-pkg-widget-client/dist/lib';
import {TWidgetData} from 'edsjs-test-pkg-widget-client/types';
import {EWidgets} from 'edsjs-test-pkg-widget-client/dist/widgets';
import {useWidget} from 'edsjs-test-pkg-widget-client/dist/composables/useIframe';
import BackendRestJsApiProvider from 'edsjs-test-pkg-core/dist/providers/BackendRestJsApiProvider';

export type EdsIframeOptions = {
    url: string;
    elId: string;
}

///// CORE

// THTMLOffsetSize
export type TSize2d = {width: number, height: number};

export type TInitOptions = {
    communicationType: 'postMessaging';
    secret: string;
}


type TEdsWidgetBaseCommand = {
    name: string;
    data?: any
};

type TEdsWidgetCommandInit = TEdsWidgetBaseCommand & {
    name: 'init';
    data: TInitOptions;
}

export type TEdsWidgetCommand = TEdsWidgetBaseCommand | TEdsWidgetCommandInit;

type TEdsWidgetEventBase = {
    name: string;
    data: unknown;
}

export type TEdsWidgetEventError = TEdsWidgetEventBase & {
    name: 'failed';
    data: {
        error: Error
    };
}

export type TEdWidgetEventSigned = TEdsWidgetEventBase & {
    name: 'signed';
    data: {
        signature: string
    };
}

export type TEdWidgetEventDocCreated = TEdsWidgetEventBase & {
    name: 'doc_created';
    data: {
        doc_id: number
    };
}

export type TEdsWidgetHTMLUpdated = TEdsWidgetEventBase & {
    name: 'html_updated',
    data: TSize2d
}

export type TEdsWidgetEvent = TEdsWidgetEventError | TEdWidgetEventSigned | TEdsWidgetHTMLUpdated | TEdWidgetEventDocCreated;

const secret = '12345';

// type TSignWidgetSubscribe = (onEvent: (eventData: {e: string, d: any}) => void, onError: (e: string) => void) => void;

// todo: добавить обработчики для перехвата изменения высоты html
// todo: а может и не надо отдавать ничего в return? сразу пытаться его вставить с одним конфигом на все?
export function getWidget(o: EdsIframeOptions) {
    const opts = {
        widget: EWidgets.B2BCenterSignWidget,
        elementId: o.elId,
        baseUrl: o.url
    };
    const widget = useWidget(opts); // todo Проверить на чистом, будет ли установка vue тут. Если она есть, тогда можно тоже использовать реактивные штуки

    const execCommand = (command: TEdsWidgetCommand): void => {
        console.info('execCommand in client: ', command);
        return widget.sendCommand({
            widget: EWidgets.B2BCenterSignWidget,
            command: command.name,
            data: command.data,
        });
    }

    const subscribe = (
        onEvent: (eventData: {
            e: string,
            d: any
        }) => void,
        onError: (e: string) => void
    ): void => { // todo return unsubscribe, secret checking
        const resolver = (data: TWidgetData): void => {
            console.info('resolver in client: ', data);
            if (data.widget === EWidgets.B2BCenterSignWidget) {
                onEvent({e: data.command, d: data.data});
            }
        };
        widget.receiveData(resolver, onError);
    }

    // todo тут надо передавать конфиг для процесса ЭП
    const connectFn = (config?: Record<string, string>): Promise<{
        execCommand: (command: TEdsWidgetCommand) => void,
        subscribe: (onEvent: (eventData: {
            e: string,
            d: any
        }) => void,
        onError: (e: string) => void) => void
    }> => {
        console.log('connection starts');
        // const initCfg = {
        //     token: 'jwt_token_here',
        //     opId: 'OPERATION_ID_UUID'
        // };
        return new Promise((resolve, reject) => {
            widget.onError(() => {
                console.error('IFRAME connect ERROR');
                reject(new Error('Не удалось подключить iframe'));
            });
            widget.onLoad(() => {
                console.log('widget loaded 123');
                resolve({execCommand, subscribe});
                const o: TInitOptions = {communicationType: 'postMessaging', secret};
                execCommand({name: 'init', data: o});
            });
            widget.init(config ?? {}); //todo: а вот тут непонятно, надо ли за сессию отдавать uuid и работать только с одним подписанием или тут может быть серия???
        });
    }

    return {
        connect: connectFn,
    }
}

export function someTh() {
    console.log();
}

export function receiveCommandToIframeWidget(
    handler: (cmd: TEdsWidgetCommand) => void
) { // todo: unsubscribe
    console.log('receiver in client: ', handler);
    receiveCommandToIframe(
        EWidgets.B2BCenterSignWidget,
        (d: TWidgetData) => {
            console.log('received command in widget from parent $edsWidgetPostMessagingCommunication', d);
            handler({name: d.command, data: d.data});
        },
        (e: string) => {
            throw new Error(e);
        }
    )
}

export function sendFromIframeWidget(e: TEdsWidgetEvent) {
    console.log('send event from $sendEdsPostMessageEvent to parent', e);
    const d: TWidgetData = {
        widget: EWidgets.B2BCenterSignWidget,
        command: e.name,
        data: e.data
    };
    sendFromIframe(d);
}