import type { TIframeOptions } from './types';

export enum EWidgets {
  Test = 'Test',
  B2BCenterSignWidget = 'B2BCenterSignWidget'
}

export default {
  Test: {
    path: '/test',
    width: '100%',
    height: '100%'
  },
  B2BCenterSignWidget: {
    path: '', // fixme: Надо от этого избавиться - хуйня какая то
    width: '100%',
    height: '100%',
    loading: 'eager'
  }
} as Record<EWidgets, TIframeOptions>;
