import widgets from '../widgets';
import type { IWidgetClient, TIframeOptions, TWidgetData, TWidgetCreate } from '../types';
import { receiveCommandFromIframe, sendToIframe } from '../lib';

const IS_SSR = typeof window === 'undefined';

const createIframe = (options: TIframeOptions): HTMLIFrameElement => {
  const iframe = document.createElement('iframe');
  iframe.style.minHeight = options.height;
  iframe.style.width = options.width;
  iframe.style.border = 'none';
  iframe.style.margin = '0';
  iframe.style.padding = '0';
  iframe.src = options.baseUrl + options.path;
  // @ts-ignore
  iframe.loading = options?.loading || 'eager';

  if (options?.attrs) {
    for (const attr in options.attrs) {
      // @ts-ignore
      iframe[attr] = options.attrs[attr];
    }
  }

  return iframe;
};

export const useWidget = (options: TWidgetCreate): IWidgetClient => {
  if (IS_SSR) {
    return {
      onLoad: (clb: () => void) => {},
      onError: (clb: () => void) => {},
      init: (config: Record<string, string> = {}) => {},
      sendCommand: (data: TWidgetData) => {},
      receiveData: (resolve: (data: TWidgetData) => void, reject: (error: string) => void) => {},
      reload: () => {}
    };
  }

  // todo: рассмотреть второй вариант подгрузки седержимого
  // const xhr = new XMLHttpRequest();
  // xhr.open('GET', 'https://example.com/iframe-content', true);
  // xhr.onreadystatechange = () => {
  //   if (xhr.readyState === 4 && xhr.status === 200) {
  //     // Сервер передал содержимое для iframe
  //     iframe.src = 'data:text/html;charset=utf-8,' + encodeURIComponent(xhr.responseText);
  //   } else if (xhr.readyState === 4) {
  //     console.log('Ошибка загрузки iframe');
  //   }
  // };
  // xhr.send();

  const widget = widgets[options.widget];
  widget.baseUrl = options.baseUrl;
  const iframe = createIframe(widget);
  let timeoutId = null;
  // todo: missed error object
  let onErrorFn = () => {};

  return {
    onLoad: (clb: () => void) => {
      // iframe.onload = clb;
      iframe.addEventListener('load', () => {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
        console.log('iframe loaded event')
        clb()
      });
    },
    onError: (clb: () => void) => {
      onErrorFn = clb;
      console.info('iframe onerror handler is:', onErrorFn);
      // iframe.onerror = clb;
      iframe.addEventListener('error', onErrorFn)
    },
    sendCommand: (data: TWidgetData) => {
      data.widget = options.widget;
      sendToIframe(iframe, data);
    },
    receiveData: (resolve: (data: TWidgetData) => void, reject: (error: string) => void) => {
      receiveCommandFromIframe(options.widget, resolve, reject);
    },
    init: (config: Record<string, string> = {}) => {
      console.log('init iframe client method invoked');
      const element = document.getElementById(options.elementId);
      while (element?.firstChild) {
        element.removeChild(element.firstChild);
      }

      if (Object.keys(config).length) {
        iframe.src = iframe.src + '?' + new URLSearchParams(config).toString();
      }
      element?.append(iframe);
      console.debug('Appended iframe ', iframe);
      timeoutId = setTimeout(onErrorFn, 1000)
    },
    reload: () => {
      iframe.contentWindow?.location.reload();
    }
  };
};
