"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useWidget = void 0;
var widgets_1 = require("../widgets");
var lib_1 = require("../lib");
var IS_SSR = typeof window === 'undefined';
var createIframe = function (options) {
    var iframe = document.createElement('iframe');
    iframe.style.minHeight = options.height;
    iframe.style.width = options.width;
    iframe.style.border = 'none';
    iframe.style.margin = '0';
    iframe.style.padding = '0';
    iframe.src = options.baseUrl + options.path;
    // @ts-ignore
    iframe.loading = (options === null || options === void 0 ? void 0 : options.loading) || 'eager';
    if (options === null || options === void 0 ? void 0 : options.attrs) {
        for (var attr in options.attrs) {
            // @ts-ignore
            iframe[attr] = options.attrs[attr];
        }
    }
    return iframe;
};
var useWidget = function (options) {
    if (IS_SSR) {
        return {
            onLoad: function (clb) { },
            onError: function (clb) { },
            init: function (config) {
                if (config === void 0) { config = {}; }
            },
            sendCommand: function (data) { },
            receiveData: function (resolve, reject) { },
            reload: function () { }
        };
    }
    var widget = widgets_1.default[options.widget];
    widget.baseUrl = options.baseUrl;
    var iframe = createIframe(widget);
    return {
        onLoad: function (clb) {
            iframe.onload = clb;
        },
        onError: function (clb) {
            iframe.onerror = clb;
        },
        sendCommand: function (data) {
            data.widget = options.widget;
            (0, lib_1.sendToIframe)(iframe, data);
        },
        receiveData: function (resolve, reject) {
            (0, lib_1.receiveCommandFromIframe)(options.widget, resolve, reject);
        },
        init: function (config) {
            if (config === void 0) { config = {}; }
            var element = document.getElementById(options.elementId);
            while (element === null || element === void 0 ? void 0 : element.firstChild) {
                element.removeChild(element.firstChild);
            }
            if (Object.keys(config).length) {
                iframe.src = iframe.src + '?' + new URLSearchParams(config).toString();
            }
            element === null || element === void 0 ? void 0 : element.append(iframe);
        },
        reload: function () {
            var _a;
            (_a = iframe.contentWindow) === null || _a === void 0 ? void 0 : _a.location.reload();
        }
    };
};
exports.useWidget = useWidget;
