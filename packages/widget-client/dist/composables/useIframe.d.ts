import type { IWidgetClient, TWidgetCreate } from '../types';
export declare const useWidget: (options: TWidgetCreate) => IWidgetClient;
