"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendFromIframe = exports.sendToIframe = exports.receiveCommandFromIframe = exports.receiveCommandToIframe = void 0;
var receiver = function (widget, resolve, reject) {
    return function (event) {
        // @ts-ignore
        if (typeof event.data !== 'string' || !event.data.includes('command') || !event.data.includes('widget')) {
            return;
        }
        var response = null;
        try {
            var result = JSON.parse(event.data);
            response = {
                widget: result.widget,
                command: result.command,
                data: result.data
            };
            if (result.widget === widget) {
                resolve(response);
            }
        }
        catch (e) {
            console.log(e);
            reject(e);
        }
    };
};
var receiveCommandToIframe = function (widget, resolve, reject) {
    window.addEventListener('message', receiver(widget, resolve, reject));
};
exports.receiveCommandToIframe = receiveCommandToIframe;
var receiveCommandFromIframe = function (widget, resolve, reject) {
    window.onmessage = receiver(widget, resolve, reject);
};
exports.receiveCommandFromIframe = receiveCommandFromIframe;
var sendToIframe = function (iframe, data) {
    var _a;
    (_a = iframe.contentWindow) === null || _a === void 0 ? void 0 : _a.postMessage(JSON.stringify(data), '*');
};
exports.sendToIframe = sendToIframe;
var sendFromIframe = function (data) {
    var _a;
    (_a = window.top) === null || _a === void 0 ? void 0 : _a.postMessage(JSON.stringify(data), '*');
};
exports.sendFromIframe = sendFromIframe;
