import type { EWidgets } from '../widgets';
import type { TWidgetData } from '../types';
export declare const receiveCommandToIframe: (widget: EWidgets, resolve: (data: TWidgetData) => void, reject: (error: any) => void) => void;
export declare const receiveCommandFromIframe: (widget: EWidgets, resolve: (data: TWidgetData) => void, reject: (error: any) => void) => void;
export declare const sendToIframe: (iframe: HTMLIFrameElement, data: TWidgetData) => void;
export declare const sendFromIframe: (data: TWidgetData) => void;
