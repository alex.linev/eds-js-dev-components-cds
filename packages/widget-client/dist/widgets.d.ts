import type { TIframeOptions } from './types';
export declare enum EWidgets {
    Test = "Test",
    B2BCenterSignWidget = "B2BCenterSignWidget"
}
declare const _default: Record<EWidgets, TIframeOptions>;
export default _default;
