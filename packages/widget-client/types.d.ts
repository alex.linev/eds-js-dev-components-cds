import { EWidgets } from './widgets';

export type TIframeOptions = {
  path: string;
  height: string;
  width: string;
  baseUrl?: string;
  loading?: 'lazy' | 'eager';
  attrs?: Record<string, string>;
};

export type TWidgetData = {
  widget?: string;
  command: string;
  data: any;
};

export type TWidgetCreate = {
  widget: EWidgets;
  elementId: string;
  baseUrl: string;
};

export interface IWidgetClient {
  onLoad: (clb: () => void) => void;
  onError: (clb: () => void) => void;
  init: (config: Record<string, string>) => void;
  sendCommand: (data: TWidgetData) => void;
  receiveData: (resolve: (data: TWidgetData) => void, reject: (error: string) => void) => void;
  reload: () => void;
}
