import type { EWidgets } from '../widgets';
import type { TWidgetData } from '../types';

type TEvent = {
  command: string;
  widget: string;
  data: any;
};

const receiver = (widget: EWidgets, resolve: (data: TWidgetData) => void, reject: (error: any) => void) => {
  return (event: MessageEvent<TEvent>) => {
    // @ts-ignore
    if (typeof event.data !== 'string' || !event.data.includes('command') || !event.data.includes('widget')) {
      return;
    }

    let response: TWidgetData | null = null;
    try {
      const result = JSON.parse(event.data) as TEvent;
      response = {
        widget: result.widget,
        command: result.command,
        data: result.data
      };

      if (result.widget === widget) {
        resolve(response);
      }
    } catch (e) {
      console.log(e);
      reject(e);
    }
  };
};

export const receiveCommandToIframe = (
  widget: EWidgets,
  resolve: (data: TWidgetData) => void,
  reject: (error: any) => void
) => {
  window.addEventListener('message', receiver(widget, resolve, reject));
};

export const receiveCommandFromIframe = (
  widget: EWidgets,
  resolve: (data: TWidgetData) => void,
  reject: (error: any) => void
) => {
  window.onmessage = receiver(widget, resolve, reject);
};

export const sendToIframe = (iframe: HTMLIFrameElement, data: TWidgetData) => {
  iframe.contentWindow?.postMessage(JSON.stringify(data), '*');
};

export const sendFromIframe = (data: TWidgetData) => {
  window.top?.postMessage(JSON.stringify(data), '*');
};
